create or replace function svc_process_recurrent_fees(in_batch_limit bigint) returns json
    security definer
    language plpgsql
as $$
declare

    /**
      This will apply pending recurrent fees
      the in_batch_limit will determine how many records to process at a time
     */
    cursor_pending_fees cursor (batch_size integer) for
        SELECT
            id, description
        FROM
            institution_fees_due
        WHERE
                approval_status = TRUE
          AND recurrent = TRUE
          AND effective_date <= NOW()
          AND end_date >= NOW()
          AND next_apply_date <= NOW()
          LIMIT batch_size;

    fee_record  record;


begin

    open cursor_pending_fees(batch_size:=in_batch_limit);
    loop

        fetch cursor_pending_fees into fee_record;
        exit when not found;

        raise notice '% - Applying recurrent fee % - by creating period fee', now(), fee_record.description;
        perform fee_create_period_child_fee(fee_record.id);
    end loop;
    close cursor_pending_fees;

    -- Return details as json
    -- Parameter validations
    return jsonb_build_object('returnCode', 0, 'returnMessage', 'Recurrent fee run has been completed successfully');
end;
$$;

create or replace function svc_process_pending_fee_group_associations(in_batch_limit bigint) returns json
    security definer
    language plpgsql
as $$
declare

    /**
      This will apply pending fee class associations
      the in_batch_limit will determine how many records to process at a time
     */
    student_record          record;
    cursor_fee_association cursor (batch_size integer) for
        SELECT
            ifga.id AS gpFeeId,
            ifd.id AS feeId,
            sgi.id AS stdgpid ,
            ifd.child_of_recurrent,
            ifd.due_amount ,
            ifd.description,
            ifd.parent_fee_id,
            ifga.credit_hours,
            sgi.group_name
        FROM
            institution_fees_due ifd
                INNER JOIN institution_fee_group_association ifga ON
                    ifga.fee_id = ifd.id
                INNER JOIN student_group_information sgi ON
                    sgi.id = ifga.group_id
        WHERE
                ifga.applied = FALSE
          AND ifd.approval_status = TRUE
          AND ifd.effective_date <= current_date
          AND ifd.recurrent = FALSE
        LIMIT batch_size;
    association_record  record;

    group_student_fetch_query varchar = 'SELECT cs.* FROM core_student cs inner join student_group_student sgs on sgs.student_id = cs.id WHERE sgs.group_id = $1';
    cursor_group_students refcursor;

    fee_id_to_consider_for_exemption bigint;

    apply_student_fee_request json;

begin

    raise notice '% - %', now(), group_student_fetch_query;
    open cursor_fee_association(batch_size:=in_batch_limit);
    loop

        fetch cursor_fee_association into association_record;
        exit when not found;

        raise notice '% - Applying group fee % to %', now(), association_record.description, association_record.group_name;

        fee_id_to_consider_for_exemption = association_record.feeId;

        if association_record.child_of_recurrent
        then
            fee_id_to_consider_for_exemption = association_record.parent_fee_id;
        end if;


        open cursor_group_students for execute group_student_fetch_query using association_record.stdgpid;
        loop
            fetch cursor_group_students into student_record;
            exit when not found;

            raise notice '% - Applying fee % to student %', now(), association_record.description, student_record.first_name;


            --check if student is exempted
            if exists (SELECT id FROM fees_due_student_exemption WHERE student_number = student_record.student_code and fee_id = fee_id_to_consider_for_exemption)
            then
                raise notice '% - Student % is exempted from fee %', now(), student_record.student_code, fee_id_to_consider_for_exemption;
                continue;
            end if;


            apply_student_fee_request = jsonb_build_object('studentId', student_record.id,
                                                           'feeId', association_record.feeId,
                                                           'webUserId', 0,
                                                           'webUserName', 'SYSTEM',
                                                           'creditHours', association_record.credit_hours,
                                                           'ipAdress', ''
                );

            raise notice '% - %', now(), cast(apply_student_fee_request as text);

            perform apply_fee_to_student(cast(apply_student_fee_request as text));

        end loop;
        close cursor_group_students;

        UPDATE institution_fee_group_association SET applied = true, date_applied = now() WHERE id = association_record.gpFeeId;


        raise notice '% - Fee association % applied to Group %', now(), association_record.description, association_record.group_name;

    end loop;
    close cursor_fee_association;

    -- Return details as json
    -- Parameter validations
    return jsonb_build_object('returnCode', 0, 'returnMessage', 'Group Fees have been applied successfully');
end;
$$;

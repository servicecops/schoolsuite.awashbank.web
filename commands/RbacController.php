<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use Yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author John Mark
 * @since 2.0
 */
class RbacController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
//    public function actionSetRule()
////    {
////    	$auth = Yii::$app->authManager;
////    	// add the rule
////		$rule = new \app\rbac\SchAccessRule();
////		$auth->add($rule);
////        echo "Rule is set" . "\n";
////    }

    /*
     * This sets the uneb payments access rule for bank users
     */

//    public function actionUnebmmRule()
//    {
//        $auth = Yii::$app->authManager;
//        // add the rule
//        $rule = new \app\rbac\UnebPaymentsAccess();
//        $auth->add($rule);
//        echo "Uneb MM rule is set" . "\n";
//    }

    public function actionUpdatePerms(){
        $auth = Yii::$app->authManager;
        $perms = $this->perms();

        foreach($perms as $k=>$v){
            $perm = $auth->getPermission($k);
            if($perm){
                $perm->name = $k;
                $perm->description = $v;
                $perm->type =2;
                $auth->update($v, $perm);
                echo $v." updated" . "\n";
            } else {
                $perm = $auth->createPermission($k);
                $perm->description = $v;
                $auth->add($perm);
                echo $v." added" . "\n";
            }
        }
    }


    public function actionUpdateRole(){
        $auth = Yii::$app->authManager;
        $roles = $this->roles();

        foreach($roles as $k=>$v){
            $role = $auth->getRole($k);
            if($role){
                $role->name = $k;
                $role->description = $v;
                $role->type =1;
                $auth->update($v, $role);
                echo $v." updated" . "\n";
            } else {
                $role = $auth->createRole($k);
                $role->description = $v;
                $auth->add($role);
                echo $v." added" . "\n";
            }
        }
    }


    public function actionAddPerm(){
        $name = readline("Enter permission name: ");
        $description = readline("Enter description: ");

        if(!$name || !$description){
            echo "Both name and description are required. Please try again";
            return false;
        }

        $auth = Yii::$app->authManager;

        $perm = $auth->getPermission($name);
        if($perm){
            $perm->name = $name;
            $perm->description = $description;
            $perm->type =2;
            $auth->update($description, $perm);
            echo $description." updated" . "\n";
        } else {
            $perm = $auth->createPermission($name);
            $perm->description = $description;
            $auth->add($perm);
            echo $description." added" . "\n";
        }
    }


    public function actionSchRoles(){
    	$auth = Yii::$app->authManager;
    	$rule = $auth->getRule('sch_access');

    	$perms = ['r_student', 'rw_student', 'r_class', 'rw_class', 'r_sch', 'rw_sch', 'del_student',
            'del_class', 'own_sch', 'r_msg_cr', 'w_msg_cr', 'r_msg', 'w_msg', 'r_outbox', 'w_outbox'];

    	foreach($perms as $v){
	    	$role = $auth->getPermission($v);
	    	if($role){
	    		$role->ruleName = $rule->name;
	    		$auth->update($v, $role);
	    		echo $v." updated" . "\n";
	    	}
	    }  	
    }

    public function actionUnebmnoRoles(){
        $auth = Yii::$app->authManager;
        $rule = $auth->getRule('uneb_payment_access');

        $perms = ['uneb_mno'];

        foreach($perms as $v){
            $role = $auth->getPermission($v);
            if($role){
                $role->ruleName = $rule->name;
                $auth->update($v, $role);
                echo $v." updated" . "\n";
            }
        }
    }


    private function perms(){
        return [
            'r_loan_request'=>'Read Loan Request', 'w_loan_request'=>'Write Loan Request',
            'r_loan'=>'Read Loan', 'w_loan'=>'Write Loan', 'r_sacco_member'=>'Read Sacco Member',
            'w_sacco_member'=>'Write Sacco Member', 'r_sacco_trans'=>'Read Sacco Transaction',
            'r_loan_provider'=>'Read Loan Provider', 'w_loan_provider'=>'Write Loan Provider',
            'r_loan_trans'=>'Read Loan Transaction',
            'r_msg_cr'=>'Read Message Credits', 'w_msg_cr'=>'Write Message Credits',
            'r_msg'=>'Read Messages','w_msg'=>'Write Messages',
            'r_outbox'=>'Read Outbox', 'w_outbox'=>'Write Outbox'

        ];
    }

    private function roles(){
        return [

        ];
    }
}

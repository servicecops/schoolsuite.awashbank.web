<?php


namespace app\commands;
use Yii;
use yii\console\Controller;

class PermsController extends Controller
{
    public function actionAdd()
    {
        $auth = Yii::$app->authManager;
//        $auth->removeAll();

        // add "edit user" permission
        $perm = $auth->createPermission('edit_class');
        $perm->description = 'Edit class information';
        $auth->add($perm);


//        // add "author" role and give this role the "createPost" permission
//        $author = $auth->createRole('author');
//        $auth->add($author);
//        $auth->addChild($author, $createPost);
//
//        // add "admin" role and give this role the "updatePost" permission
//        // as well as the permissions of the "author" role
//        $admin = $auth->createRole('admin');
//        $auth->add($admin);
//        $auth->addChild($admin, $updatePost);
//        $auth->addChild($admin, $author);
//
//        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
//        // usually implemented in your User model.
//        $auth->assign($author, 2);
//        $auth->assign($admin, 1);
    }
}

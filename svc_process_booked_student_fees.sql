create or replace function svc_process_booked_student_fees(in_batch_limit bigint) returns json
    security definer
    language plpgsql
as $$
declare

    /**
      This will apply pending booked student fees
      the in_batch_limit will determine how many records to process at a time
     */
    cursor_pending_fees cursor (batch_size integer) for
        SELECT
            *
        FROM
            institution_fee_student_association
        WHERE
                applied = false
          AND due_date <= current_date
        LIMIT batch_size;

    fee_record  record;
    apply_student_fee_request json;
begin

    open cursor_pending_fees(batch_size:=in_batch_limit);
    loop

        fetch cursor_pending_fees into fee_record;
        exit when not found;

        raise notice '% - Applying booked student fee % to student %', now(), fee_record.fee_id, fee_record.student_id;
        apply_student_fee_request = jsonb_build_object('studentId', fee_record.student_id,
                                                       'feeId', fee_record.fee_id,
                                                       'webUserId', 0,
                                                       'webUserName', 'SYSTEM',
                                                       'creditHours', fee_record.credit_hours,
                                                       'ipAdress', ''
            );

        raise notice '% - %', now(), cast(apply_student_fee_request as text);

        perform apply_fee_to_student(cast(apply_student_fee_request as text));
    end loop;
    close cursor_pending_fees;

    -- Return details as json
    -- Parameter validations
    return jsonb_build_object('returnCode', 0, 'returnMessage', 'Recurrent fee run has been completed successfully');
end;
$$;

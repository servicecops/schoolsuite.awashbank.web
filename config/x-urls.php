<?php
return [
        '@awashLoginUrl'=>'http://10.10.102.7:9180/bfweb/services/LoginService?wsdl',
        '@awashStatementUrl'=>'http://10.10.102.7:9180/bfweb/retail/v1/accounts',
        '@awashSmsUrl'=>'http://10.36.30.27:9509/api',
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
    '@uploads'   => 'uploads/videos',
    '@uploaded'   => 'uploads/videos',
    '@mtnPaymentApi'=>'https://schoolpay.co.ug/uatpaymentapi/AndroidRS/RequestSchoolpayPaymentForChannel',
    '@css'=>'@app/web/vendors/bootstrap/dist/css/bootstrap.min.css',
    ];

?>

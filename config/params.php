<?php
return [
    'adminEmail' => '',
    'senderEmail' => '',
    'supportEmail' => 'no-reply@awashbank.com',
    'senderName' => 'Awash Eschool Support Team',

    'user.passwordResetTokenExpire' => 3600,
    'bsVersion' => '4.x',
    'awashStatementApiUser' => 'intuser',
    'awashStatementApiPassword' => 'P@ssw0rd',
    'awashSmsApiUsername' => 'eschool',
    'awashSmsApiPassword' => 'Eschool@123',
    'ratePerMessage' => 5,
    'reservedRegistrationNumberFormats'=>[ //These formats should be valid regexes, with no deliters of ^ and $
        '^\d{6}', //Reserved for ICPAU
    ],
    'fileUploadedEmailRecipients'=>[ //These are the email addresses which will receive emails when a file is uploaded
//        'support@schoolpay.co.zm',
    ],
    'bankTransactionsConfig'=>[
        'AWASH'=>[
            'internalChannelCodes'=>['AWASH'],
        ],

    ],
    'teleconferencingAppUrl'=>'https://elearning.awashbank.com/bigbluebutton/',
    'teleconferencingApiKey'=>'7wt1NYpRl3kIsyWipJEblerGlK16UW54PtPe9NKI',
    'EmailSenderAddress'=>'no-reply@awashbank.com',
    'sessionTimeout' => 300

];


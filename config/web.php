<?php

use app\modules\schoolcore\models\CoreStudent;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$sessionTimeout = isset($params['sessionTimeout']) ? $params['sessionTimeout'] : 300; //default to 10 minutes

Yii::trace('timeout is ' . $sessionTimeout);
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/home-page',
    'modules' => [
        'schoolcore' => [
            'class' => 'app\modules\schoolcore\SchoolCoreModule',
        ],
        'library' => [
            'class' => 'app\modules\library\LibraryModule',
        ],
        'feesdue' => [
            'class' => 'app\modules\feesdue\FeesCoreModule',
        ],


        'gradingcore' => [
            'class' => 'app\modules\gradingcore\GradingCoreModule',
        ],



        'e_learning' => [
            'class' => 'app\modules\e_learning\ElearningModule',
        ],
        'online_reg' => [
            'class' => 'app\modules\online_reg\OnlineRegistrationModule',
        ],
        'paymentscore' => [
            'class' => 'app\modules\paymentscore\PaymentsCoreModule',
        ],
        'studentreport' => [
            'class' => 'app\modules\studentreport\StudentReportModule',
        ],
        'synccore' => [
            'class' => 'app\modules\synccore\SyncCoreModule',
        ],
       /* 'suiteapp' => [
            'class' => 'app\modules\suiteapp\OpenLearningModule',
        ],*/

         'suiteapp' => [
           'class' => 'app\modules\suiteapp\SuiteAppModule',
       ],
        'plan' => [
            'class' => 'app\modules\payment_plan\Module',
        ],
        'logs' => [
            'class' => 'app\modules\logs\logs',
        ],

        'paymentsplan' => [
            'class' => 'app\modules\paymentplan\PaymentPlanModule',
        ],

        'import' => [
            'class' => 'app\modules\import\import',
        ],
        'messages' => [
            'class' => 'app\modules\messages\messages',
        ],

        'attendance' => [
            'class' => 'app\modules\attendance\AttendanceModule',
        ],
        'reports' => [
            'class' => 'app\modules\reports\student',
        ],
        'banks' => [
            'class' => 'app\modules\banks\banks',
        ],

        'disciplinary' => [
            'class' => 'app\modules\disciplinary\DisciplinaryModule',
        ],

            'gridview' => ['class' => 'kartik\grid\Module'],
        'payfees' => [
            'class' => 'app\modules\payfees\payfees',
        ],
        'planner' => [
            'class' => 'app\modules\planner\PlannerModule'
        ],

        'timetable' => [
            'class' => 'app\modules\timetable\TimeTableModule'
        ],

        'workflow' => [
            'class' => 'app\modules\workflow\WorkflowModule',
        ],
        'recon' => [
            'class' => 'app\modules\recon\ReconModule',
        ],
        'lessonplan' => [
            'class' => 'app\modules\lessonplan\LessonPlanModule'

        ],
        'dissertation' => [
           'class' => 'app\modules\dissertation\DissertationModule'
        ],
        'notice' => [
           'class' => 'app\modules\noticeboard\NoticeModule'
        ],
        'supplementary' => [
           'class' => 'app\modules\supplementaryfees\SupplementaryFeesModule'
        ],

    ],
   'aliases' => require(__DIR__ . '/x-urls.php'),



    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'QTNGGxRB4imok7obsySNU25sTAdCq3V1',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'geoip' => ['class' => 'lysenkobv\GeoIP\GeoIP'],
        'user' => [
            'class'=>'yii\web\User',
           'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'authTimeout' => $sessionTimeout,

        ],
//        'session' => [
//
//            'timeout' => 120,
//
//        ],
        'pdf'=>[
            'class'=>'app\components\ExportToPdf',
        ],
        'excel'=>[
            'class'=>'app\components\ExportToExcel',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'mailer' => require(__DIR__ . '/mailer.php'),

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'db2' => require(__DIR__ . '/db2.php'),


//        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => true,
//            'rules' => [
//            ],
//        ],
//        'urlManager' => [
//            'class' => 'yii\web\UrlManager',
//            // Disable index.php
//            'showScriptName' => false,
//            // Disable r= routes
//            'enablePrettyUrl' => true,
//            'rules' =>[
//                'class' => 'yii\rest\UrlRule',
//                'controller' =>[ 'paymentscore/post-payment'],
//                'tokens'=>['{id}' => '<id:\\w+>'],
//            ],
//        ],
        'response' => [
            // ...
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                    // ...
                ],
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                'm/<i:\w+>' => 'site/m', //Alias for receipt context ActionM
                '_<i:\w+>' => 'site/m', //Alias for receipt context ActionM
                '-<i:\w+>' => 'site/m', //Alias for receipt context ActionM
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/e/<mod:[\w-]+>' => '<module>/default/index',
                '<module:\w+>/e/<mod:[\w-]+>/<id:\d+>' => '<module>/default/view',
                '<module:\w+>/e/<mod:[\w-]+>/<action:\w+>/' => '<module>/default/<action>',
                '<module:\w+>/e/<mod:[\w-]+>/<action:\w+>/<id:\d+>' => '<module>/default/<action>',
            ],
        ],


    ],
    'on beforeRequest' => function ($event) {
//            $user = Yii::$app->user;
        Yii::trace('before request');
//            if (!$user->isGuest) {
//
//            }

    },
    'params' => $params,
    'container' => [
        'definitions' => [
            \yii\widgets\LinkPager::class => \yii\bootstrap4\LinkPager::class,
            'yii\bootstrap4\LinkPager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel'  => 'Last'
            ]
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1','192.168.43.21', '192.168.43.135','192.168.43.1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}


return $config;

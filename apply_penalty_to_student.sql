CREATE OR REPLACE FUNCTION public.apply_student_penalty(in_fee_student_association_id bigint)
    RETURNS text
    LANGUAGE plpgsql
    SECURITY DEFINER
AS $function$

declare
    --temps
    temp_amount              numeric;
    balance_before              numeric;
    balance_after              numeric;
    this_penalty_next_apply_date date;
    record_fee_association record;
    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    penalty_applied boolean = false;
    this_student_end_balance numeric;
    new_outstanding_balance numeric;
    varying_amount numeric;
    varying_amount_json json;

begin

    -- Open row cursor
    select student.id as student_id, student.student_account_id, penalty_type, penalty_frequency,
           student.student_code , penalty_amount, fee_outstanding_balance, last_penalty_apply_date,
           next_penalty_apply_date, penalties_closed, penalties_outstanding_balance, assoc.id as associd,
           penalty_apply_frequency_unit, penalty_apply_frequency_term,  fee.description as fee_description,
           fee.id as fee_id, penalty_expiry_date, maximum_penalty_amount, maximum_penalty_application_count,
           assoc.penalty_application_count, student_gl.account_balance as student_account_balance,
           penalty.penalty_variable_amount

    from institution_fee_student_association assoc
             inner join core_student student on assoc.student_id=student.id
             inner join institution_fees_due fee on assoc.fee_id=fee.id
             inner join institution_fees_due_penalty penalty on fee.penalty_id = penalty.id
             inner join student_account_gl student_gl on student.student_account_id = student_gl.id
    where assoc.id = in_fee_student_association_id
      and assoc.penalties_closed=false
      and assoc.next_penalty_apply_date <= current_date
      and fee_outstanding_balance < 0
      and assoc.has_penalty = true
    into record_fee_association;

    if record_fee_association.associd is null
    then
        RETURN row_to_json(data) FROM ( VALUES (0, 'No penalty to apply was found')) data (returncode, returnmessage);
    end if;


    --if penalty expiry date has passed or penalty_amount for this record has been exceeded

    -- then close the penalty and continue

    IF cast(record_fee_association.penalty_expiry_date as date) < current_date

    then
        raise notice '% - Penalty for fee association id % has reached the expiry date %', now(), record_fee_association.associd, cast(record_fee_association.penalty_expiry_date as varchar);
        penalty_applied = false;
        UPDATE institution_fee_student_association set
            penalties_closed = true
        where id = record_fee_association.associd;
        RETURN row_to_json(data) FROM ( VALUES (0, format('Penalty with id %s has reached the expiry date', record_fee_association.associd))) data (returncode, returnmessage);
    end if;



    -- if record_fee_association.maximum_penalty_amount <> 0 and penalty has exceeded limit them leave it

    IF abs(record_fee_association.penalties_outstanding_balance)>=abs(record_fee_association.maximum_penalty_amount) and record_fee_association.maximum_penalty_amount <>0

    then
        raise notice '% - Penalty for fee association id % has reached the maximum penalty amount constraints %', now(), record_fee_association.associd, cast(record_fee_association.maximum_penalty_amount as varchar);
        penalty_applied = false;
        UPDATE institution_fee_student_association set
            penalties_closed = true
        where id = record_fee_association.associd;
        RETURN row_to_json(data) FROM ( VALUES (0, format('Penalty with id %s has reached the maximum penalty amount constraints', record_fee_association.associd))) data (returncode, returnmessage);
    end if;



    -- if record_fee_association.maximum_penalty_application_count <> 0 and penalty application count has exceeded the maximum

    IF abs(record_fee_association.penalty_application_count)>=abs(record_fee_association.maximum_penalty_application_count) and record_fee_association.maximum_penalty_application_count <>0
    then
        raise notice '% - Penalty for fee association id % has reached the maximum number of applications %', now(), record_fee_association.associd, cast(record_fee_association.maximum_penalty_application_count as varchar);
        penalty_applied = false;
        UPDATE institution_fee_student_association set
            penalties_closed = true
        where id = record_fee_association.associd;
        RETURN row_to_json(data) FROM ( VALUES (0, format('Penalty with id %s has reached the maximum number of applications', record_fee_association.associd))) data (returncode, returnmessage);
    end if;



    if record_fee_association.student_account_balance >= 0

    then
        -- Student has a penalty but for some reason the balance is not negative
-- This is an opportunity to block further penalties
        raise notice '% - Penalty for fee association id % is being skipped due to positive student balance %', now(), record_fee_association.associd, cast(record_fee_association.student_account_balance as varchar);
        UPDATE institution_fee_student_association set
            penalties_closed = true
        where id = record_fee_association.associd;
        RETURN row_to_json(data) FROM ( VALUES (0, format('Penalty fee with id %s is being skipped due to positive student balance', record_fee_association.associd))) data (returncode, returnmessage);
    end if;


    IF record_fee_association.penalty_type = 'FIXED'
    then
        temp_amount = record_fee_association.penalty_amount;
    elsiF record_fee_association.penalty_type = 'PERCENTAGE'
    then
        --Compute as percantage of outstanding
        temp_amount = (record_fee_association.penalty_amount * abs(record_fee_association.fee_outstanding_balance)) / 100.0;
    elsiF record_fee_association.penalty_type = 'VARYING_AMOUNT'
    then
        --Lookup the next application amount
        --Using the number of applications so far, lookup the next index and if not found use the last amount in the list
        varying_amount_json = record_fee_association.penalty_variable_amount;
        varying_amount = coalesce(varying_amount_json->>record_fee_association.penalty_application_count,
                                  varying_amount_json->>(json_array_length(varying_amount_json)-1));
        temp_amount = varying_amount;
    elsiF record_fee_association.penalty_type = 'VARYING_PERCENTAGE'
    then
        --Lookup the next application percentage
        --Using the number of applications so far, lookup the next index and if not found use the last amount in the list
        --then use this as the percentage
        varying_amount_json = record_fee_association.penalty_variable_amount;
        varying_amount = coalesce(varying_amount_json->>record_fee_association.penalty_application_count,
                                  varying_amount_json->>(json_array_length(varying_amount_json)-1));
        temp_amount = varying_amount;
        temp_amount = (temp_amount * abs(record_fee_association.fee_outstanding_balance)) / 100.0;
    end if;


    if temp_amount is null or temp_amount = 0
    then
        raise notice '% - Penalty for fee association id % is being skipped due null or zero penalty amount', now(), record_fee_association.associd;
        RETURN row_to_json(data) FROM ( VALUES (0, 'Penalty amount is null or zero')) data (returncode, returnmessage);
    end if;





    --Adjust and set balance to balance minus adjustment amount

    UPDATE institution_fee_student_association set
        penalties_outstanding_balance = penalties_outstanding_balance - temp_amount
    where id = record_fee_association.associd;

    -- if fixed penalty
    if record_fee_association.penalty_frequency = 'ONETIME'
    then
        UPDATE institution_fee_student_association set
            penalties_closed = true
        where id = record_fee_association.associd;
    end if;



    -- Create fee performance th record

    INSERT INTO institution_fees_due_student_payment_transaction_history
    ("date_created",
     "payment_id",
     "fee_id",
     "fee_student_association_id",
     "student_id",
     "amount",
     "balance_before",
     "balance_after",
     "description",
     "penalty_amount",
     "penalty_balance_before",
     "penalty_balance_after"
    )
    VALUES (
               now(),
               0,
               record_fee_association.fee_id,
               record_fee_association.associd,
               record_fee_association.student_id,
               0,
               record_fee_association.fee_outstanding_balance, -- balances havent changed
               record_fee_association.fee_outstanding_balance, -- balances havent changed
               format('PENALTY - %s', temp_amount),
               temp_amount * -1,
               record_fee_association.penalties_outstanding_balance,
               record_fee_association.penalties_outstanding_balance - temp_amount
           );


    -- Updating student balances too
    SELECT
        account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
    FROM
        student_account_gl
    WHERE
            ID = record_fee_association.student_account_id FOR SHARE ;


    -- Adjust the new balance
    this_student_end_balance = this_student_current_balance - temp_amount;
    new_outstanding_balance = this_student_current_outstanding_balance - temp_amount;

    IF new_outstanding_balance > 0 THEN
        new_outstanding_balance = 0;
    END IF;

    -- update balance
    UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = record_fee_association.student_account_id;
    -- Create th record
    INSERT INTO student_account_transaction_history (
        date_created,
        amount,
        description,
        payment_id,
        trans_type,
        account_id,
        reversal_flag,
        reversed,
        date_reversed,
        balance_after,
        associated_fee_due,
        outstanding_balance_after,
        fee_association_id
    )
    VALUES
    (
        now(),
        - 1 * temp_amount,
        'PENALTY ON - ' || record_fee_association.fee_description,
        NULL,
        'PENALTY',
        record_fee_association.student_account_id,
        FALSE,
        FALSE,
        NULL,
        this_student_end_balance,
        record_fee_association.fee_id,
        new_outstanding_balance,
        record_fee_association.associd
    ) ;

    -- Adjust next apply date
    -- If penalty, we are to save the penalty apply date
    this_penalty_next_apply_date = null;
    if record_fee_association.penalty_apply_frequency_unit = 'D'
    then
        this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 day' * record_fee_association.penalty_apply_frequency_term;
    elsif record_fee_association.penalty_apply_frequency_unit = 'W' then
        this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 week' * record_fee_association.penalty_apply_frequency_term;
    else
        this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 month' * record_fee_association.penalty_apply_frequency_term;
    end if;

    -- update next apply date, last apply date to today select * from institution_fee_student_association
    update institution_fee_student_association
    set last_penalty_apply_date = current_date,
                                                    next_penalty_apply_date = this_penalty_next_apply_date,
                                                   penalties_accrued=(penalties_accrued + abs(temp_amount)),
                                                   penalty_application_count=penalty_application_count+1  where id = record_fee_association.associd;







    raise notice '% - Applied penalty for association id  % ', now(), record_fee_association.associd;
    penalty_applied = true;

    if penalty_applied = false
    then
        RETURN row_to_json(data) FROM ( VALUES (0, 'No penalty to apply was found')) data (returncode, returnmessage);
    end if;

    RETURN row_to_json(data) FROM ( VALUES (0, 'Penalty applied')) data (returncode, returnmessage);

end;
$function$
;

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FullCalendarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/web';
    public $css = [
        'vendors/bootstrap/dist/css/bootstrap.min.css',
        'vendors/font-awesome/css/font-awesome.min.css',
        //full calendar
        'vendor/full-calendar/css/main.min.css',
        'vendor/full-calendar/css/daygrid.main.min.css',
        'vendor/full-calendar/css/timegrid.main.min.css'        
    ];
    public $js = [

        'vendors/bootstrap/dist/js/bootstrap.bundle.min.js',
        'vendor/full-calendar/js/fullcalendar_core.js',
        'vendor/full-calendar/js/fullcalendar_interaction.js',
        'vendor/full-calendar/js/daygrid.js',
        'vendor/full-calendar/js/timegrid.js',
        'vendor/full-calendar/js/moment.min.js',
        'vendor/full-calendar/js/timetable.js'

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
    public $cssOptions = ['type'=>'text/css'];

    /*   public $jsOptions = [
            'position' => \yii\web\View::POS_HEAD
        ];*/
}

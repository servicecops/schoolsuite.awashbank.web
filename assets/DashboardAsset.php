<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/web';
    public $css = [
        'vendors/bootstrap/dist/css/bootstrap.min.css',
        'vendors/font-awesome/css/font-awesome.min.css',
//        'assets/all.min.css',
        'build/css/custom.css',
        'css/croppie.css',
        'css/schpayDash.css',
        'css/UserProfile.css',
        'css/qr.css',


    ];
    public $js = [

        'vendors/bootstrap/dist/js/bootstrap.bundle.min.js',
        'build/js/custom.min.js',
        'js/UserProfile.js'




    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
    public $cssOptions = ['type'=>'text/css'];

// public $jsOptions = [
//            'position' => \yii\web\View::POS_HEAD
//        ];
}

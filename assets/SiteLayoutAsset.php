<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SiteLayoutAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/web';
    public $css = [

        'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i',
        'website/assets/vendor/bootstrap/css/bootstrap.min.css',
        'website/assets/vendor/icofont/icofont.min.css',
        'website/assets/vendor/remixicon/remixicon.css',
        'website/assets/vendor/boxicons/css/boxicons.min.css',
        'website/assets/vendor/owl.carousel/assets/owl.carousel.min.css',
        'website/assets/vendor/venobox/venobox.css',
        'website/assets/vendor/aos/aos.css',
        'website/assets/css/style.css',


    ];
    public $js = [
        'website/assets/vendor/jquery/jquery.min.js',
        'website/assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
        'website/assets/vendor/jquery.easing/jquery.easing.min.js',
        'website/assets/vendor/php-email-form/validate.js',
        'website/assets/vendor/waypoints/jquery.waypoints.min.js',
        'website/assets/vendor/counterup/counterup.min.js',
        'website/assets/vendor/owl.carousel/owl.carousel.min.js',
        'website/assets/vendor/isotope-layout/isotope.pkgd.min.js',
        'website/assets/vendor/venobox/venobox.min.js',
        'website/assets/vendor/aos/aos.js',
        'website/assets/js/main.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/web';
    public $css = [

       'website/assets/vendor/icofont/icofont.min.css',
        'website/assets/vendor/boxicons/css/boxicons.min.css',
        'website/assets/vendor/venobox/venobox.css',
        'website/assets/vendor/owl.carousel/assets/owl.carousel.min.css',
        'website/assets/css/style.css',
        'website/assets/fonts/fonts_awash.css',
        'vendors/font-awesome/css/font-awesome.min.css',


    ];
    public $js = [
        'website/assets/vendor/jquery/jquery.min.js',
        'website/assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
        'website/assets/vendor/jquery.easing/jquery.easing.min.js',
        'website/assets/vendor/php-email-form/validate.js',
        'website/assets/vendor/waypoints/jquery.waypoints.min.js',
        'website/assets/vendor/counterup/counterup.min.js',
        'website/assets/vendor/owl.carousel/owl.carousel.min.js',
        'website/assets/vendor/isotope-layout/isotope.pkgd.min.js',
        'website/assets/vendor/venobox/venobox.min.js',
        'css/schpayDash.css',
        'website/assets/js/main.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $cssOptions = ['type'=>'text/css'];

}

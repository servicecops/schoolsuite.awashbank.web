<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CroppieAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/web';
    public $css = [
        'vendors/bootstrap/dist/css/bootstrap.min.css',
        'vendors/font-awesome/css/font-awesome.min.css',
//        'assets/all.min.css',
        'build/css/custom.css',
        'css/croppie.css',
        'css/schpayDash.css',
        'css/UserProfile.css',

/*//        'css/style.css',

//        'css/schpayDash.css',*/


    ];
    public $js = [

        'vendors/jquery/dist/jquery.min.js',
        'build/js/custom.min.js',
        'js/croppie.min.js',
     //   'js/UserProfile.js',



    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

   public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}

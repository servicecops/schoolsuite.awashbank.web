<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PrintAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/web';
    public $css = [
        'css/UserProfile.css',
        'css/print.css',
    ];
    public $js = [
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\web\YiiAsset',

        //'yii\bootstrap\BootstrapPluginAsset',
    ];

     public $jsOptions = [
            'position' => \yii\web\View::POS_HEAD
    ];
    public $cssOptions = ['type'=>'text/css'];







}

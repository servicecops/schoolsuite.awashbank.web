<?php

namespace app\modules\import\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\import\models\StudentPaymentCodeRequests;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

if (!Yii::$app->session->isActive){
          session_start();  
      }
/**
 * StudentPaymentCodeRequestsSearch represents the model behind the search form about `app\modules\import\models\StudentPaymentCodeRequests`.
 */
class StudentPaymentCodeRequestsSearch extends StudentPaymentCodeRequests
{
    public $modelSearch, $schsearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['modelSearch'], 'trim'],
            [['first_name', 'middle_name', 'last_name', 'school_student_registration_number', 'student_phone', 'gurdian_name', 'gurdian_phone', 'student_class', 'registration_mode', 'modelSearch', 'schsearch', 'approved'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['si.id', 'si.date_created', 'si.first_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_phone', 'si.gurdian_name', 'si.gurdian_phone', 'si.approved'])
            ->from('student_payment_code_requests si')
            ->innerJoin('institution_student_classes cls', 'cls.id=si.student_class')
            ->innerJoin('school_information sch', 'sch.id=si.school_id');
            if($this->student_class && $this->student_class!=0){
                $query->andWhere(['si.student_class'=>intVal($this->student_class)]);
            } elseif($this->schsearch){
                $query->andWhere(['sch.id'=>$this->schsearch]);
            }

        if($this->school_student_registration_number){
            $query->andWhere(['ilike', 'school_student_registration_number', $this->school_student_registration_number]);
        }
        if(!empty($this->approved)){
            $query->andWhere(['approved'=>$this->approved]);
        }

        if($this->modelSearch){
                $query = $this->nameLogic($query, $this->modelSearch);
            }

            $pages = new Pagination(['totalCount' => $query->count()]);
            $sort = new Sort([
                'attributes' => [
                    'date_created', 'first_name', 'last_name',  'school_name', 'class_code', 'gurdian_name', 'student_phone', 'gurdian_phone'
                    ],
            ]);
            ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.date_created DESC');
            unset($_SESSION['findData']);
            $_SESSION['findData'] = $query;
            $query->offset($pages->offset)->limit($pages->limit);
            return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];

    }

    public static function getExportQuery() 
    {
    $data = [
            'data'=>$_SESSION['findData'],
            'labels'=>['first_name', 'last_name', 'class_code', 'school_name', 'student_phone', 'gurdian_name', 'gurdian_phone', 'approved'],
            'fileName'=>'Self_enrolled', 
            'title'=>'Self Enrolled List',
            'exportFile'=>'@app/modules/import/views/import/exportPdfExcel',
        ];

        return $data;
    }

    private function nameLogic($query, $seachName){
        $search_words = preg_split("/[\s,]+/", $seachName);
        $search_words = array_filter($search_words);
        $words = array();
          if(count($search_words) > 1) {
            foreach($search_words as $word) {
                $words[] = $word;
            }
                if(isset($words[0])){
                    $name = str_replace(' ', '', $words[0]);
                    $query->andFilterWhere(['or', 
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name] 
                            ]);
                }
                if(isset($words[1])){
                    $name = str_replace(' ', '', $words[1]);
                    $query->andFilterWhere(['or', 
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name] 
                            ]);
                }
                if(isset($words[2])){
                    $name = str_replace(' ', '', $words[2]);
                    $query->andFilterWhere(['or', 
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name] 
                            ]);
                }
            
            }
            else{
                $name = str_replace(' ', '', $seachName);
                $query->andFilterWhere(['or', 
                        ['ilike', 'first_name', $name], 
                        ['ilike', 'middle_name', $name],
                        ['ilike', 'last_name', $name],

                    ]);
                }
        return $query;

    }
}

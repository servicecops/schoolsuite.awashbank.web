<?php

namespace app\modules\import\models;

use Yii;
use yii\db\Query;
use app\models\Classes;
use app\models\SchoolInformation;

/**
 * This is the model class for table "student_payment_code_requests".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $school_student_registration_number
 * @property string $gender
 * @property string $date_of_birth
 * @property string $date_created
 * @property integer $student_class
 * @property boolean $active
 * @property string $student_email
 * @property string $student_phone
 * @property string $gurdian_name
 * @property string $guardian_relation
 * @property string $gurdian_email
 * @property string $gurdian_phone
 * @property string $date_modified
 * @property boolean $allow_part_payments
 * @property integer $student_image
 * @property string $nationality
 * @property integer $approved_student_id
 * @property integer $school_id
 * @property string $day_boarding
 * @property string $registration_mode
 * @property string $requested_by
 * @property string $device_id
 * @property string $device_name
 * @property string $cell_info
 * @property string $network_mode
 * @property string $status
 * @property boolean $approved
 */
class StudentPaymentCodeRequests extends \yii\db\ActiveRecord
{
    public $captcha;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_payment_code_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'gender', 'date_of_birth', 'school_id', 'student_class', 'day_boarding'], 'required'],
            [['first_name', 'middle_name', 'last_name', 'school_student_registration_number', 'student_email', 'student_phone', 'gurdian_name', 'guardian_relation', 'gurdian_email', 'gurdian_phone', 'nationality', 'registration_mode', 'requested_by', 'device_id', 'device_name', 'cell_info', 'network_mode', 'status'], 'string'],
            [['gurdian_email', 'student_email'], 'email'],
            [['date_of_birth', 'date_created', 'date_modified', 'approved_student_id'], 'safe'],
            [['student_class', 'student_image', 'school_id', 'approved_student_id'], 'integer', 'message'=>'Please select from dropdown'],
            [['active', 'allow_part_payments', 'approved'], 'boolean'],
            [['school_student_registration_number'], 'uniqueRegNo', 'skipOnEmpty' => true, 'skipOnError' => false],
            [['gurdian_phone'], 'onePhoneNumber', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['first_name', 'last_name'], 'validatenames', 'skipOnEmpty' => true, 'skipOnError' => false],
            [['middle_name', 'gurdian_name'], 'nameWithSpaces', 'skipOnEmpty' => true, 'skipOnError' => false],
            [['gurdian_phone', 'student_phone'], 'validePhone', 'skipOnEmpty' => true, 'skipOnError' => false], 
            [['gurdian_name'], 'validateGurdian', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['gender', 'day_boarding'], 'string', 'max' => 1],
            [['captcha'], 'captcha', 'except'=>'update']
        ];
    }

    // public function scenarios()
    // {
    //     $scenarios = parent::scenarios();
    //     $scenarios['update'] = ['username', 'first_name', 'last_name', 'mobile_number', 'user_level', 'school_id', 'bank_id', 'email_address','password', 'password2'];
    //     return $scenarios;
    // }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'first_name' => 'First Name',
            'approved_student_id' => 'Student Id',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'school_student_registration_number' => 'School Student Registration Number',
            'gender' => 'Gender',
            'date_of_birth' => 'Date Of Birth',
            'date_created' => 'Date Created',
            'student_class' => 'Student Class',
            'active' => 'Active',
            'student_email' => 'Student Email',
            'student_phone' => 'Student Phone',
            'gurdian_name' => 'Guardian Name',
            'guardian_relation' => 'Guardian Relation',
            'gurdian_email' => 'Guardian Email',
            'gurdian_phone' => 'Guardian Phone',
            'date_modified' => 'Date Modified',
            'allow_part_payments' => 'Allow Part Payments',
            'student_image' => 'Student Image',
            'nationality' => 'Nationality',
            'day_boarding' => 'Day Boarding',
            'registration_mode' => 'Registration Mode',
            'requested_by' => 'Requested By',
            'device_id' => 'Device ID',
            'device_name' => 'Device Name',
            'cell_info' => 'Cell Info',
            'network_mode' => 'Network Mode',
            'status' => 'Status',
            'approved' => 'Approved',
        ];
    }

    public function getFullname()
    {
        $fn = $this->first_name ? $this->first_name : '';
        $mn = $this->middle_name ? $this->middle_name : '';
        $ln = $this->last_name ? $this->last_name : '';
        return $fn. ' '.$mn .' '.$ln;
    }

    public function getStudentClass()
    {
        return $this->hasOne(Classes::className(), ['id' => 'student_class']);
    }

    public function getSchoolId()
    {
        return $this->hasOne(SchoolInformation::className(), ['id' => 'school_id']);
    }

    public function uniqueRegNo($attribute,$params)
    {   
        $query = (new Query())->select(['school_student_registration_number', 'payment_code'])->from('student_information')->where(['school_id'=>$this->school_id, 'school_student_registration_number'=>trim(strtoupper($this->school_student_registration_number))])->limit(1)->one();
        $invalid = $query['school_student_registration_number'] ? true : false;

        if($invalid) {
              $this->addError($attribute, 'Student with this REG NO already exits');  
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $query = (new Query())->select(['default_part_payment_behaviour'])->from('school_information')->where(['id'=>$this->school_id])->limit(1)->one();
            $this->allow_part_payments = $query['default_part_payment_behaviour'];
            $this->active = true;
            return true;
        } else {
            return false;
        }
    }

    public function validateGurdian($attribute, $params)
    {
        if ($this->gurdian_phone && !$this->gurdian_name) {
             $this->addError($attribute, 'Provide Guardian Name for '.$this->gurdian_phone);
        }
    }

    public function onePhoneNumber($attribute,$params)
    {
        if(!$this->gurdian_phone && !$this->student_phone){
            $this->addError($attribute, 'Enter atleast one Phone Number (Guardian or Student)');
        }
    }

    public function validePhone($attribute,$params)
    {
        if (!preg_match('/^(0|256)(7[178095234]|39)\d\d\d\d\d\d\d$/i', $this->$attribute)) {
             $this->addError($attribute, 'Please Enter a valid Phone Number');
        }
    }

    public function validatenames($attribute, $params)
    {
        if (!preg_match('/^[a-zA-Z]+[a-zA-Z]$/i', $this->$attribute)) {
             $this->addError($attribute, 'Should not contain spaces, numbers or special characters');
        }
    }

    public function nameWithSpaces($attribute, $params)
    {
        if (!preg_match('/^[\p{L} ]+$/i', $this->$attribute)) {
             $this->addError($attribute, 'Should not Contain numbers or special characters');
        }
    }


}

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\SchoolInformation;
use app\models\Classes;
use kartik\select2\Select2;
use yii\jui\DatePicker;
use yii\web\JsExpression;
?>

<div class="self-enrolled hd-title" data-title="<?= $model->fullname ?>">
<div class="row">
<div class="col-xs-12">
    <div class="row">
  <?php $form = ActiveForm::begin([
  	'id'=>'auto_enrollment_form',
  ]); ?>
    <div class="row">
    <div class="col-md-12" style="text-align:center; color:#565656;"><h3>Student /Personal Information</h3>
        <hr class="l_header">
    </div>

    <div class="col-md-12 no-padding">
        <div class="col-md-4 col-sm-6 col-xs-12">
        <?php          
            $url = Url::to(['/school-information/schoollist']);
            $selectedSchool = empty($model->school_id) ? '' : SchoolInformation::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Find School', 
                    'id'=>'school_search',
                    'class'=>'form-control',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ])->label(''); ?>
        </div>
    <div class="col-md-4 col-sm-6 col-xs-12"><?= $form->field($model, 'student_class', ['inputOptions'=>[ 'class'=>'form-control', 'id'=>'student_class_drop']])->dropDownList(
        ['prompt'=>'Filter class'])->label('') ?></div>

        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'day_boarding', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Day /Boarding'] ])->dropDownList([''=>'Select Day /Boarding', 'D'=>'Day', 'B'=>'Boarding'])->label('') ?>
        </div>
        </div>

    <div class="col-md-12 no-padding">
        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'first_name', ['inputOptions'=>[ 'class'=>'form-control student_name', 'placeholder'=> 'First Name'] ])->textInput()->label('') ?>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'middle_name', ['inputOptions'=>[ 'class'=>'form-control student_name', 'placeholder'=> 'Middle Name'] ])->textInput()->label('') ?>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'last_name', ['inputOptions'=>[ 'class'=>'form-control student_name', 'placeholder'=> 'Last Name'] ])->textInput()->label('') ?>
        </div>
    </div>

    <div class="col-md-12 no-padding">
        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'gender')->dropDownList(['M'=>'Male', 'F'=>'Female'], ['prompt'=>'Gender'])->label('') ?>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'school_student_registration_number', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Registration Number'] ])->textInput()->label('') ?>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'student_email', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Student Email'] ])->textInput()->label('') ?>
        </div>
    </div>

    <div class="col-md-12 no-padding">
        <div class="col-md-4 col-sm-6 col-xs-12">
        <?= $form->field($model, 'student_phone', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Student Phone'] ])->textInput()->label('')
         ?>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 no-pading">
        <?= $form->field($model, 'date_of_birth')->widget(DatePicker::className(),
                    [
            'model'=>$model,
            'attribute'=>'date_of_birth',
                        'clientOptions' =>[
                        'changeMonth'=> true,
                        'changeYear'=> true,
                        'yearRange'=>'1900:'.(date('Y')+1),
                        'autoSize'=>true,
                        ],
                        'options'=>[
                            'class'=>'form-control',
                            'placeholder'=>'Date of Birth'
                         ],])->label('') ?>
        </div>
    </div>
    
    <div class="col-md-12" style="text-align:center; color:#565656;"><h3>Guardian Information</h3>
        <hr class="l_header">
    </div>

 
   <div class="col-xs-12 col-lg-12 no-padding" >
        <div class="col-sm-6">
        <?= $form->field($model, 'gurdian_name', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Guardian Name'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
        <?= $form->field($model, 'gurdian_email', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Guardian Email'] ])->textInput()->label('') ?>
        </div>
   </div>


    <div class="col-xs-12 no-padding">
        <div class="col-sm-6">
        <?= $form->field($model, 'gurdian_phone', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Guardian Phone'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
        </div>
   </div>

    
    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding" style="margin-top:20px;">
        <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => 'btn btn-block btn-info']) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
     </div>

    <?php ActiveForm::end(); ?>

</div>
 </div>
 </div>
 </div>
 </div>
<?php 
$class_url = Url::to(['/classes/lists']);
$class = $model->student_class ? $model->student_class : 0;
$script = <<< JS
$("document").ready(function(){ 
    $("#home").removeClass('active');
    $("#request_pc").addClass('active');
    var class_val = $class;
    if($('#school_search').val() !==''){
        var school_id = $('#school_search').val();
       $.get( '$class_url', {id : school_id}, function( data ) {
                $( 'select#student_class_drop' ).html(data);
                $( 'select#student_class_drop' ).val(class_val);
            }); 

    }
    $('#school_search').change(function(){
        var school_id = $(this).val();
        $.get( '$class_url', {id : school_id}, function( data ) {
                $( 'select#student_class_drop' ).html(data);
            });
    });
   
  });
JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\import\models\StudentPaymentCodeRequests */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-payment-code-requests-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput() ?>

    <?= $form->field($model, 'middle_name')->textInput() ?>

    <?= $form->field($model, 'last_name')->textInput() ?>

    <?= $form->field($model, 'school_student_registration_number')->textInput() ?>

    <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_of_birth')->textInput() ?>

    <?= $form->field($model, 'date_created')->textInput() ?>

    <?= $form->field($model, 'student_class')->textInput() ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <?= $form->field($model, 'student_email')->textInput() ?>

    <?= $form->field($model, 'student_phone')->textInput() ?>

    <?= $form->field($model, 'gurdian_name')->textInput() ?>

    <?= $form->field($model, 'guardian_relation')->textInput() ?>

    <?= $form->field($model, 'gurdian_email')->textInput() ?>

    <?= $form->field($model, 'gurdian_phone')->textInput() ?>

    <?= $form->field($model, 'date_modified')->textInput() ?>

    <?= $form->field($model, 'allow_part_payments')->checkbox() ?>

    <?= $form->field($model, 'student_image')->textInput() ?>

    <?= $form->field($model, 'nationality')->textInput() ?>

    <?= $form->field($model, 'school_id')->textInput() ?>

    <?= $form->field($model, 'day_boarding')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'registration_mode')->textInput() ?>

    <?= $form->field($model, 'requested_by')->textInput() ?>

    <?= $form->field($model, 'device_id')->textInput() ?>

    <?= $form->field($model, 'device_name')->textInput() ?>

    <?= $form->field($model, 'cell_info')->textInput() ?>

    <?= $form->field($model, 'network_mode')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'approved')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

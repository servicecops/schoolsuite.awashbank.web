<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\import\models\StudentPaymentCodeRequests */

$this->title = 'Create Student Payment Code Requests';
$this->params['breadcrumbs'][] = ['label' => 'Student Payment Code Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-payment-code-requests-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

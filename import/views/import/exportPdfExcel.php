<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php  
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Mobile User List</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <th>First Name</th><th>Last Name</th><th>School Name</th><th>Class code</th><th>Student Phone</th> <th>Guardian Name</th> <th>Guardian Phone</th></tr>
        </thead>
        <tbody>
            <?php 
            if($query) :
            foreach($query as $k=>$v) : ?>
                <tr <?= ($v['approved']) ? '' :"style='background-color:#f9dbdb;'" ?> >
                    <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['gurdian_name']) ? $v['gurdian_name'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><?= ($v['gurdian_phone']) ? $v['gurdian_phone'] : '<span class="not-set">(not set) </span>'?></td>
                </tr>
            <?php endforeach; 
            else :?>
            <tr><td colspan="8">No student found</td></tr>
        <?php endif; ?>
        </tbody>
        </table>

</div>
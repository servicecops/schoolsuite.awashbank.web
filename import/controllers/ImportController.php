<?php

namespace app\modules\import\controllers;

use Yii;
use app\modules\import\models\StudentPaymentCodeRequests;
use app\modules\messages\models\MessageOutbox;
use app\modules\import\models\StudentPaymentCodeRequestsSearch;
use app\models\Student;
use yii\web\Controller;
use app\models\ImageBank;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * ImportController implements the CRUD actions for StudentPaymentCodeRequests model.
 */
class ImportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view','delete','update','create', 'image-link'],
                'rules' => [
                    [
                        'actions' => ['index', 'view','delete','update','create', 'image-link'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentPaymentCodeRequests models.
     * @return mixed
     */

    public function actionIndex()
    {
        if(Yii::$app->user->can('approve_students')){
            $request = Yii::$app->request;
            $searchModel = new StudentPaymentCodeRequestsSearch();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages'=>$data['pages'], 'page'=>$data['cpage']];
            $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ];

            return ($request->isAjax) ? $this->renderAjax('index', $res) : $this->render('index', $res);
        } else {
        throw new ForbiddenHttpException('No permissions to view Self Enrolled List.');
        }
    }

    public function actionImageLink($id)
    {

        $data = (new Query())->select(['image_base64'])->from('image_bank')->where(['id'=>$id])->limit(1)->one();
        $data = base64_decode($data['image_base64']);

        $im = imagecreatefromstring($data);
        if ($im !== false) {
            header('Content-Type: image/jpeg');
            header('Pragma: public');
            header('Cache-Control: max-age=3600');
            header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
            imagejpeg($im);
            imagedestroy($im);
        }
        else {
            echo '';
        }

    }
    public function actionImageLink2($id, $h='')
    {
        //h should be base4(image$id)
        if($h != base64_encode('image' . $id))
            throw new NotFoundHttpException("Resource not found");

        $data = (new Query())->select(['image_base64'])->from('image_bank')->where(['id'=>$id])->limit(1)->one(Yii::$app->db2);
        $data = base64_decode($data['image_base64']);

        $im = imagecreatefromstring($data);
        if ($im !== false) {
            header('Content-Type: image/jpeg');
            header('Pragma: public');
            header('Cache-Control: max-age=3600');
            header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
            imagejpeg($im);
            imagedestroy($im);
        }
        else {
            echo 'An error occurred.';
        }

    }

    /**
     * Displays a single StudentPaymentCodeRequests model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('approve_students')){
        $request = Yii::$app->request;
        $res = ['model' => $this->findModel($id)];
        return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);
        } else {
        throw new ForbiddenHttpException('No permissions to view Self Enrolled Student details.');
        }
    }



    /**
     * Updates an existing StudentPaymentCodeRequests model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('approve_students')){
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model->scenario = 'update';
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->first_name = ucfirst($model->first_name);
                $model->last_name = ucfirst($model->last_name);
                $model->middle_name = ucfirst($model->middle_name);
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }
            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to update Self Enrolled Student details.');
        }
    }


    public function actionApprove($id)
    {
        if(Yii::$app->user->can('approve_students')){
            $model = $this->findModel($id);
            $model->approved= true;
            $query = (new Query())->select(['school_student_registration_number', 'payment_code'])->from('student_information')->where(['school_id'=>$model->school_id, 'school_student_registration_number'=>$model->school_student_registration_number])->limit(1)->one();
            if(!$query['school_student_registration_number']){
                $model2 = new Student();

                $model2->first_name = $model->first_name;
                $model2->last_name = $model->last_name;
                $model2->middle_name = $model->middle_name;
                $model2->school_id = $model->school_id;
                $model2->student_class = $model->student_class;
                $model2->date_of_birth = $model->date_of_birth;
                $model2->school_student_registration_number = $model->school_student_registration_number;
                $model2->gender = $model->gender;
                $model2->day_boarding = $model->day_boarding;
                $model2->student_email = $model->student_email;
                $model2->student_phone = $model->student_phone;
                $model2->gurdian_name = $model->gurdian_name;
                $model2->gurdian_phone = $model->gurdian_phone;
                $model2->gurdian_email = $model->gurdian_email;
                $model2->guardian_relation = 'Guardian';
                $model2->active = true;
                $model2->allow_part_payments = true;
                $model2->nationality = 'Uganda';

                $model2->save(false);
                $model->approved_student_id =$model2->id;
                $model->save(false);
                $this->createMessage($model2->id);

                return $this->redirect(['index']);
            } else {
                \Yii::$app->session->setFlash('failedApproval');
                return Yii::$app->runAction('/import/import/view', ['id'=>$id]);
            }

        } else {

            throw new ForbiddenHttpException('No permissions to approve item');
        }
    }

    /**
     * Deletes an existing StudentPaymentCodeRequests model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    private function createMessage($id){
        $model = Student::find()->where(['id'=>$id])->limit(1)->one();
        $reg = ($model->school_student_registration_number) ? 'Reg No: '.$model->school_student_registration_number : '';
        $phones = [];
        if($model->student_phone){
            $phones[] = $model->student_phone;
        }
        if($model->gurdian_phone){
            $phones[] = $model->gurdian_phone;
        }
        foreach ($phones as $k=>$v) {
            $message = new MessageOutbox();

            $message->message_text = "SchoolPay: Payment details for ".$model->fullname." Payment Code: ".$model->payment_code."; ".$reg."\n in ".$model->schoolId->school_name;
            $message->email_message = false;
            $message->flash_message = false;
            $message->message_status = 'PENDING';
            $message->recipient_number = $v;
            $message->save(false);
        }

    }

    /**
     * Finds the StudentPaymentCodeRequests model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentPaymentCodeRequests the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentPaymentCodeRequests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

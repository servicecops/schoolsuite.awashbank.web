create or replace function svc_process_pending_fee_class_associations(in_batch_limit bigint) returns json
    security definer
    language plpgsql
as $$
declare

    /**
      This will apply pending fee class associations
      the in_batch_limit will determine how many records to process at a time
     */
    student_record          record;
    cursor_fee_association cursor (batch_size integer) for
        SELECT
            assoc.id AS assocId,
            fees.id AS feeId,
            classes.id AS classId,
            classes.class_description,
            assoc.credit_hours,
            fees.child_of_recurrent,
            fees.due_amount,
            fees.description,
            fees.parent_fee_id
        FROM
            institution_fees_due fees
                INNER JOIN institution_fee_class_association assoc ON
                    fees.id = assoc.fee_id
                INNER JOIN core_school_class classes ON
                    classes.id = assoc.class_id
        WHERE
                assoc.applied = FALSE
          AND fees.approval_status = TRUE
          AND fees.effective_date <= now()
          AND assoc.class_id = classes.id
          AND fees.recurrent = FALSE
        LIMIT batch_size;
    association_record  record;

    class_student_fetch_query varchar = 'SELECT id, student_code, first_name, middle_name, last_name FROM core_student WHERE class_id = $1';
    cursor_class_students refcursor;

    fee_id_to_consider_for_exemption bigint;

    apply_student_fee_request json;

begin

    raise notice '% - %', now(), class_student_fetch_query;
    open cursor_fee_association(batch_size:=in_batch_limit);
    loop

        fetch cursor_fee_association into association_record;
        exit when not found;

        raise notice '% - Applying fee % to %', now(), association_record.description, association_record.class_description;

        fee_id_to_consider_for_exemption = association_record.feeId;

        if association_record.child_of_recurrent
        then
            fee_id_to_consider_for_exemption = association_record.parent_fee_id;
        end if;


        open cursor_class_students for execute class_student_fetch_query using association_record.classId;
        loop
            fetch cursor_class_students into student_record;
            exit when not found;

            raise notice '% - Applying fee % to student % - %', now(), association_record.description, student_record.student_code, student_record.first_name;


            --check if student is exempted
            if exists (SELECT id FROM fees_due_student_exemption WHERE student_number = student_record.student_code and fee_id = fee_id_to_consider_for_exemption)
            then
                raise notice '% - Student % is exempted from fee %', now(), student_record.student_code, fee_id_to_consider_for_exemption;
                continue;
            end if;


            apply_student_fee_request = jsonb_build_object('studentId', student_record.id,
                                                           'feeId', association_record.feeId,
                                                           'webUserId', 0,
                                                           'webUserName', 'SYSTEM',
                                                           'creditHours', association_record.credit_hours,
                                                           'ipAdress', ''
                );

            raise notice '% - Executing student fee application with %', now(), cast(apply_student_fee_request as text);

            perform apply_fee_to_student(cast(apply_student_fee_request as text));

        end loop;
        close cursor_class_students;

        UPDATE institution_fee_class_association SET applied = true, date_applied = now() WHERE id = association_record.assocId;


        raise notice '% - Fee association % applied to class %', now(), association_record.description, association_record.class_description;

    end loop;
    close cursor_fee_association;

    -- Return details as json
    -- Parameter validations
    return jsonb_build_object('returnCode', 0, 'returnMessage', 'Fees have been applied successfully');
end;
$$;

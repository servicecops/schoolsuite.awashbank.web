<?php

use kartik\export\ExportMenu;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\Html;

/*
 * @var $dataProvider yii\data\ActiveDataProvider ;
 */


$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";

if(!isset($requiresExport)){
    $requiresExport =true;
}
?>

<div class="letters">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <br>


    <div class="float-right">
        <!--        data download or export widget-->
        <!--    --><?php
        /*    echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $columns,
                'dropdownOptions' => [
                    'label' => 'Export',
                ]

            ])
            */ ?>

        <?php
        if($requiresExport){
            echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-sm btn-danger',
                'target' => '_blank',
                'data-toggle' => 'tooltip',
                'title' => 'Will open the generated PDF file in a new window'
            ]);
            echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-sm btn-success',
                'target' => '_blank'
            ]);
        }

        ?>
    </div>
    <div class="float-left">
        <div class="col-md-12 "><span style="font-size:20px;color:#2c3844">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $title ?> </span></div>
        <br/>
        <br/>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
        'columns' => $columns,
        'resizableColumns' => true,
//    'floatHeader'=>true,
        'options' => [
            'class' => 'bg-colorz',
        ],
        'responsive' => true,
        'responsiveWrap' => false,
        'hover' => true,
        'bordered' => false,
        'striped' => true,
    ]); ?>

</div>

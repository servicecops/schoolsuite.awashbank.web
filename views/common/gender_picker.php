<?PHP
/**
 * This component will provide a search of a school using select 2
 */


use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '_gender_picker';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

 echo   $form->field($model, 'gender')->dropDownList(['M'=>'Male', 'F'=>'Female'], ['prompt'=>'Gender'])->label($label); ?>

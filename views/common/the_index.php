<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sacco\models\LoanRequestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $data['title'];
$this->params['breadcrumbs'][] = $this->title;
$searchCols = $data['searchCols'];
$scw = $data['search_col_w'];
?>

<div style="background: white;
    padding: 20px;">
<?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
    <div class="notify notify-success">
        <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('successAlert'); ?>
        </div>
    </div>
<?php endif; ?>
<?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>



    <div class="row " >

        <div class="col-md-<?= $scw[0] ?>"><span style="font-size:22px;">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-md-<?= $scw[1] ?> "><?php echo $this->render('_thesearch', ['model' => $searchModel, 'cols' => $searchCols, 'mod' => $mod]); ?></div>
        <div class="col-md-<?= $scw[2] ?>">
            <ul class="col-md-12 pull-right">
                <li style="    float: left; margin-right: 10px;list-style: none;"><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target' => '_blank']) ?>
                </li>
                <li style="    float: left; margin-right: 10px;list-style: none;">
                    <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model' => get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target' => '_blank']) ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        <?= $this->render('_table', ['data' => $data, 'mod' => $mod]) ?>
        </div>
    </div>
    <div style="clear: both"></div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

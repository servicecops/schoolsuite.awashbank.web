<?PHP
/**
 * This component will provide a search of a school using select 2
 */


use app\modules\schoolcore\models\CoreStaff;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '__subject_id_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Teacher';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

$types = CoreStaff::find()->orderBy(['id'=>'ASC'])->all();

echo $form
    ->field($model, $attribute)
    ->dropDownList(ArrayHelper::map($types, 'id', 'first_name'), [
        'placeholder'=>$placeHolder,
        'prompt'=>$prompt,
        'id'=>'subject_teacher_id',
    ])
    ->label($label); ?>

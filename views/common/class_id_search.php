<?PHP
/**
 * This component will provide a search of a school using select 2
 */


use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '__school_id_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Class';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

$types = CoreSchoolClass::find()->orderBy(['class_description'=>'ASC'])->all();

echo $form
    ->field($model, $attribute);
//    ->dropDownList(ArrayHelper::map($types, 'id', 'class_description'), [
//        'placeholder'=>$placeHolder,
//        'prompt'=>$prompt,
//        'id'=>'class_id',
//    ])
        $form->field($model, 'class_id', ['inputOptions'=>[ 'class'=>'form-control', 'id'=>'student_class_drop']])
            ->dropDownList(
            ['prompt'=>'Filter class'])->label($label); ?>

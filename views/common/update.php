<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sacco\models\SaccoLoanRequests */

$this->title = 'Edit '.$model->ViewTitle;
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$formOptions = isset($model->formOptions) ? $model->formOptions : [];
?>
<div class= "hd-title" data-title="<?= Html::encode($this->title) ?>">
    <div class="letters">
        <div class="row">
            <div class="<?=$mod?>-form">
                <?php $form = ActiveForm::begin($formOptions);
                echo $this->render('_form', ['model' => $model, 'form'=>$form
                ]) ?>
                <div class="col-xs-12 form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Edit', ['class' =>'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>

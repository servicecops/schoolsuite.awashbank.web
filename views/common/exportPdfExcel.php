<?php
use yii\helpers\Html;

$session = $subSessionIndex ? $_SESSION['findData'][$subSessionIndex] : $_SESSION['findData'];
$cols = $session['cols'];
unset($cols['actions']);
$title = $session['title'];
$initOptions = ['type'=>'string', 'label'=>" ", 'pdf'=>true];
if($type == 'Excel') {
    echo "<table><tr> <th colspan={}><h3>".$title."</h3> </th> </tr> </table>";
}
?>

<table class="table" width="100%">
    <thead>
    <tr>
        <?php
        $col_count = 0;
        foreach ($cols as $col=>$options){
            $opt = array_merge($initOptions, $options);
            if($opt['pdf']){
                echo "<th>{$opt['label']}</th>";
                $col_count++;
            }
        }
        ?>
    </tr>
    </thead>
    <tbody>
    <?php
    if($query) :
        foreach($query as $k=>$v) : ?>
            <tr >
                <?php foreach ($cols as $col=>$options) :
                    $opt = array_merge($initOptions, $options);
                    if($opt['pdf']) :
                    $val = null;
                    switch ($opt['type']) {
                        case 'number':
                            $val = number_format($v[$col]);
                            break;
                        case 'date':
                            $val = date('d/m/Y H:i',strtotime($v[$col]));
                            break;
                        case 'merge':
                            if(in_array('cols', $opt)){
                                $fields = [];
                                foreach($opt['cols'] as $c){
                                    $fields[] = $v[$c];
                                }
                                $val = implode($opt['sep'], $fields);
                            }
                            break;
                        default:
                            $val = $v[$col];
                            break;
                    } ?>

                    <td><?= $v[$col] ? $val : '<span class="not-set">(not set) </span>' ?></td>
                    <?php endif;?>
                <?php endforeach; ?>
            </tr>
        <?php endforeach;
    else :?>
        <tr><td colspan=<?= $col_count;?>>No records found</td></tr>
    <?php endif; ?>
    </tbody>
</table>

<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStaffType;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreTest;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

?>
<?php

if (!isset($inputId)) $inputId = '__staff_type_id';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Type';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);



$thetypes = CoreStaffType::find()->orderBy(['id'=>'ASC'])->all();

echo $form
    ->field($model, $attribute)
    ->dropDownList(ArrayHelper::map($thetypes, 'id', 'staff_description'), [
        'placeholder'=>$placeHolder,
        'prompt'=>$prompt,
        'id'=>'staff_type',
    ])
    ->label($label);?>



<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

?>
<?php

if (!isset($inputId)) $inputId = '__school_id_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Class';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

//$types = CoreSchool::find()->orderBy(['id'=>'ASC'])->all();

echo $form
    ->field($model, $attribute);
   $url = Url::to(['core-school/active-schoollist']);
$selectedSchool = empty($model->school) ? '' : CoreSchool::findOne($model->school)->school_name;
echo $form->field($model, 'school')->widget(Select2::classname(), [
    'initValueText' => $selectedSchool, // set the initial display text
    'size' => 'sm',
    'theme' => Select2::THEME_BOOTSTRAP,
    'options' => [
        'placeholder' => 'Filter School',
        'id'=>'school_search',
        'onchange'=> '$.post( "'.Url::to(['core-school-class/lists']).'?id="+$(this).val(), function( data ) {
                        $( "select#dynamicmodel-student_class" ).html(data);
                    });'
    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 3,
        'language' => [
            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
        ],
        'ajax' => [
            'url' => $url,
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],

    ],
])->label($label); ?>



<?php

use app\models\SchoolInformation;
use app\modules\sacco\models\LoanProviders;
use app\modules\sacco\models\Member;
use app\modules\sacco\models\SaccoLoanProviders;
use app\modules\sacco\models\SaccoMember;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sacco\models\SaccoLoanRequests */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$i = 0;
foreach ($model->formFields() as $section) :
$i++;
?>
<div class="col-md-12">
    <h3><i class="<?= $section['header_icon'] ?>"></i>&nbsp;&nbsp;<?= $section['heading'] ?></h3>
    <hr class="l_header">
    <br/>
</div>
<div id="form_fields_section_<?= $i; ?>" style="width:100%" >
<?php
    $row =0;
    $n = 12/$section['col_n'];
    $initOptions = ['type'=>'string', 'label'=>false, 'placeholder'=>""];
    $no_of_fields = count($section['fields']);
    foreach($section['fields'] as $field=>$options) {
        $options = array_merge($initOptions, $options);
        $placeholder =$options['placeholder'];
        $label = $options['label'];
        $size = isset($section['size']) ? $section['size'] : 'md';
        $same_div = (isset($options['same_div']) && $options['same_div']) ? $options['same_div'] : null;
        if ($row == 0) {
            $row = $section['col_n'];
            if($same_div == 'first' || !$same_div ) echo "<div class='col-md-12 no-padding'>";
        }
        if($same_div == 'first' || !$same_div) echo "<div class='col-md-$n'>";
        switch ($options['type']) {
            case 'number':
                echo $form->field($model, $field)->textInput(['class' => "form-control input-{$size}", 'type' => 'number', 'placeholder' => $placeholder])->label($label);
                break;
            case 'checkbox':
                echo "<br>";
                echo $form->field($model, $field)->checkbox();
                break;
            case 'date':
                echo $form->field($model, $field)->widget(DatePicker::className(),
                    [
                        'clientOptions' => [
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                        'options' => [
                            'class' => "form-control input-{$size}",
                            'id'=>$field."_datepicker",
                            'placeholder' => $placeholder,
                        ],
                    ])->label($label);
                break;
            case 'datetime':
                echo $form->field($model, $field)->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => $placeholder],
                    'removeButton'=>false,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayBtn' => true,
                    ]
                ]);
                break;
            case 'dropdown':
                $select_values = isset($options['options']) ? $options['options'] : [''=>'Nothing to select'];
                $input_options = isset($options['inputOptions']) ? $options['inputOptions'] : [];
                echo $form->field($model, $field, ['inputOptions'=>['class'=>'form-control input-sm']])->dropDownList($select_values, $input_options);
                break;
            case 'select':
                $data = isset($options['data']) ? $options['data'] : [];
                $multiple = (isset($options['multiple']) && $options['multiple']) ? $options['multiple'] : false;
                echo $form->field($model, $field)->widget(Select2::classname(), [
                    'data'=>$data,
                    'theme'=>Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' =>$placeholder,
                        'multiple' => $multiple,
                        'id'=>$field.'_selector'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                break;
            case 'search':
                $url = '';
                $selected = '';
                $mode = isset($options['mode']) ? $options['mode'] : '';
                if($mode=='sacco_member'){
                    $url = Url::to(['/sacco/default/find-members']);
                    $selected = empty($model->{$field}) ? '' : Member::findOne($model->{$field})->name;
                } else if($mode=='school'){
                    $url = Url::to(['/schoolcore/core-school/schoollist']);
                    $selected = empty($model->{$field}) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->{$field})->school_name;
                } else if($mode=='provider'){
                    $url = Url::to(['/sacco/default/find-providers']);
                    $selected = empty($model->{$field}) ? '' : LoanProviders::findOne($model->{$field})->name;
                } else{
                    //continue;
                }

                echo $form->field($model, $field)->widget(Select2::classname(), [
                    'initValueText' => $selected, // set the initial display text
                    'theme'=>Select2::THEME_BOOTSTRAP,
                    'size'=>'sm',
                    'options' => [
                        'placeholder' => $options['label'],
                        'id'=>'search_'.$field,
                        'class'=>'form-control',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                    ],
                ])->label($label);
                break;

            case 'file':
                echo $form->field($model, $field)->fileInput();
                break;
            case 'textarea':
                $r = isset($options['rows']) ? $options['rows'] : 2;
                $maxlength = isset($options['length']) ? $options['length'] : 1000;
                echo $form->field($model, $field)->textarea(['rows' => $r, 'class' => "form-control input-{$size}", 'placeholder' => $placeholder, 'maxlength'=>$maxlength])->label($label);
                break;
            default:
                echo $form->field($model, $field)->textInput(['class' => "form-control input-{$size}", 'placeholder' => $placeholder])->label($label);
                break;
        }
        if($same_div == 'last' || !$same_div ) echo "</div>";
        $row--; $no_of_fields--;
        if ($row == 0 || $no_of_fields == 0) {
            if($same_div == 'last' || !$same_div ) echo "</div>";
        }
    }
    endforeach;
    ?>
</div>


<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreTestGrouping;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSubject;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '_assessment_type';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Assessment Type';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

$types = CoreTestGrouping::find()->orderBy(['id' => 'ASC'])->all();

echo $form
    ->field($model, $attribute)
    ->dropDownList(ArrayHelper::map($types, 'id', 'name'), [
        'placeholder' => $placeHolder,
        'prompt' => $prompt,
        'id' => 'test_grouping_id',
    ])
    ->label($label); ?>

<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStaffLevels;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreTest;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

?>
<?php

if (!isset($inputId)) $inputId = '__staff_levels_id';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Level';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);




$levels = CoreStaffLevels::find()->orderBy(['id'=>'ASC'])->all();

echo

$form->field($model, 'user_level',['labelOptions'=>['style'=>'color:#041f4a']])->dropDownList(
    [],
    ['id'=>'user_level_id', 'prompt'=>'Type of User']
)->label('Type Of User')


?>


<?php
$url = Url::to(['/user/level-options']);
$user_level = $model->user_level;
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#user").addClass('active');
    $("#user_create").addClass('active');
    $('#school_div').hide();
    $('#bank_div').hide();
    $.get('$url', function( data ) {
        $('select#user_level_id').html(data);
        $('select#user_level_id').val('$user_level');
        
        if($('#user_level_id').val()){
            var profile =$('select#user_level_id option[value="' + $('#user_level_id').val() + '"]').attr("data-profile");
            if(profile == 'school_guy'){
                $('#school_div').show();
            }
            $('#school_div').show();
        }
        if($('#user_level_id').val() == 'bank_user'){
            $('#bank_div').show();
        }
    });
    
    $('#user_level_id').change(function(){
        var profile =$('select#user_level_id option[value="' + $(this).val() + '"]').attr("data-profile");
        if(profile == 'school_guy'){
            $('#bank_div').hide();
            $('#school_div').show();
        }
        else if($(this).val() == 'bank_user'){
             $('#school_div').hide();
            $('#bank_div').show();
        }
        else {
            $('#school_id').prop('selectedIndex', 0);
            $('#school_div').hide();
        }
    });
});
JS;
$this->registerJs($script);
?>




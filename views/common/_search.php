<?php

use app\modules\schoolcore\models\CoreSchoolSearch;
use app\modules\schoolcore\models\CoreStudentSearch;


/* @var $studentModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $schoolModel app\modules\schoolcore\models\CoreSchoolSearch */

$numberOfColumns = isset($numberOfColumns) ? $numberOfColumns : 2;
$studentModel = new  CoreStudentSearch();
$schoolModel = new CoreSchoolSearch();
//$this->fieldSpanClass = $numberOfColumns == 3 ? 'col-md-4' : 'col-md-6';
?>

<div class="core-school-form form-horizontal">

           <?php
           Yii::trace($searchModel);
            if ($searchModel == $studentModel)
                echo $this->render('_search_stds', ['studentModel' => $studentModel]);
            else
                echo $this->render('_search_school', ['schoolModel' => $schoolModel]);


            ?>


        </div>


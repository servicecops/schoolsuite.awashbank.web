<?php

use kartik\datetime\DateTimePicker;

if (!isset($inputId)) $inputId = '_date_picker';
$attribute = isset($attribute) ? $attribute : '';
$placeHolder = isset($placeHolder) ? $placeHolder : '';
$dateFormat = isset($dateFormat) ? $dateFormat : 'yyyy-MM-dd';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);
?>



<?= $form->field($model, $attribute)->widget(DateTimePicker::className(), [
    'attribute' => $attribute,
    'model' => $model,
    'class' => 'form-control',
    'options' => [
        'class' => 'form-control' . ($model->hasErrors('term_ends') ? ' is-invalid' : ''),
        'placeholder' => $placeHolder
    ],
    'type' => DateTimePicker::TYPE_INPUT
])->label($label); ?>

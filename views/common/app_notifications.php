<?PHP
Yii::trace(\Yii::$app->session);
?>
<?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
    <div class="notify notify-success">
        <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('successAlert'); ?>
        </div>
    </div>
<?php endif; ?>
<?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>

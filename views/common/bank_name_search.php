<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSubject;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '_bank_name_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Bank';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

$types = CoreBankDetails::find()->orderBy(['id'=>'ASC'])->all();

echo $form
    ->field($model, $attribute)
    ->dropDownList(ArrayHelper::map($types, 'id', 'bank_name'), [
        'placeholder'=>$placeHolder,
        'prompt'=>$prompt,
        'id'=>'bank_id',
    ])
    ->label($label); ?>

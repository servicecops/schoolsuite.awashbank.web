<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\sacco\models\SaccoLoanRequests */

$this->title = $model->ViewTitle;
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="hd-title" data-title="<?= $this->title ?>">
<div class="row">
    <div class="col-md-12">
        <div class="letters">
            <div class="row">
                <div class='col-md-7'> <h3><?= Html::encode($this->title) ?></h3></div>
                <div class='col-md-5' style="padding-top:27px;">
<span class="pull-right">

    <?php if(method_exists($model, 'getViewActions')) :
        foreach ($model->viewActions as $k => $v) {
            $title = isset($v['title']) ? $v['title'] : '';
            $link = isset($v['link']) ? $v['link']: '#';
            $htmlOptions = isset($v['htmlOptions']) ? $v['htmlOptions'] : [];
            echo '&nbsp;'.Html::a($title, $link, $htmlOptions);
        }
    else : ?>
    <?= Html::a('<i class="fa fa-plus"></i>&nbsp;&nbsp; Add New', ['create', 'mod'=>$mod], ['class' => 'aclink btn  btn-info btn-md']) ?>
    <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'mod'=>$mod, 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-md']) ?>
    <?= Html::a('<i class="fa fa-remove"></i> Delete', ['delete', 'mod'=>$mod, 'id' => $model->id], [
        'class' => 'btn btn-danger btn-md',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
    <?php endif; ?>

    </span>
                </div>
            </div>
            <hr class="l_header">
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => 'table  detail-view'],
                'attributes' => $model->viewFields,
            ]) ?>

        </div>
    </div>
</div>
</div>

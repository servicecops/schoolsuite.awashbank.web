<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use yii\data\ActiveDataProvider;

/**
 * @var $dataProvider ActiveDataProvider
 * <? foreach ($dataProvider->getPagination()->page)
 */
$pagination = $dataProvider->getPagination();
$currentPage = $dataProvider->getPagination()->page + 1; //Since it uses zero index
$pageCount = $dataProvider->getPagination()->pageCount;

$previousPage = $currentPage - 1;
$nextPage = $currentPage + 1;

$listBaseUrl = isset($listBaseUrl) ? $listBaseUrl : '.';
$pageListCount = isset($pageListCount) ? $pageListCount : 10;

$startPage = $currentPage - floor($pageListCount / 2);
//Start page should never be less than 1
$startPage = $startPage>0 ? $startPage : 1;

?>
<nav>
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link" href="<?= $listBaseUrl ?>?page=1"><i class="fa fa-fast-backward"></i></a>
        </li>

        <li class="page-item">
            <a class="page-link" href="<?= $listBaseUrl ?>?page=<?= $previousPage ?>"><i class="fa fa-backward"></i></a>
        </li>

        <?php for ($i = $startPage; $i <= $pageCount && $i < ($startPage + $pageListCount); $i++): ?>
            <li class="page-item <?php if ($i == $currentPage) echo 'active'; ?>">
                <a class="page-link" href="<?= $listBaseUrl ?>?page=<?= $i ?>"><?= $i ?></a>
            </li>
        <?php endfor; ?>

        <li class="page-item">
            <a class="page-link" href="<?= $listBaseUrl ?>?page=<?= $nextPage ?>"><i class="fa fa-forward"></i></a>
        </li>
        <li class="page-item">
            <a class="page-link" href="<?= $listBaseUrl ?>?page=<?= $pageCount ?>"><i class="fa fa-fast-forward"></i></a>
        </li>
    </ul>
</nav>

<?php

use dosamigos\datetimepicker\DateTimePicker;
use yii\jui\DatePicker;
if (!isset($inputId)) $inputId = '_date_picker';
$attribute = isset($attribute) ? $attribute : '';
$placeHolder = isset($placeHolder) ? $placeHolder : '';
$dateFormat = isset($dateFormat) ? $dateFormat : 'yyyy-MM-dd';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);
?>
<?=  $form->field($model, $attribute)->widget(DatePicker::className(),
    [
        'model'=>$model,
        'attribute'=>$attribute,
        'dateFormat'=>$dateFormat,
        'clientOptions' =>[
            'changeMonth'=> true,
            'changeYear'=> true,
            'yearRange'=>'1980:'.(date('Y')),
            'autoSize'=>true,
            'dateFormat'=>$dateFormat,
        ],
        'options'=>[
            'class' => 'form-control' . ($model->hasErrors('term_ends') ? ' is-invalid' : ''),
            'placeholder'=>$placeHolder
        ],])->label($label) ?>

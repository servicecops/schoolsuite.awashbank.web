<?PHP
/**
 * This component will provide a search of a school using select 2
 */


use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '__active_boolean';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Class';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

echo $form
    ->field($model, 'active')->radioList( [0=>'false', 1 => 'true'], ['unselect' => null] );

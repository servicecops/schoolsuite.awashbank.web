<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/*
 */
$numberOfColumns = isset($numberOfColumns) ? $numberOfColumns : 2;
//$this->fieldSpanClass = $numberOfColumns == 3 ? 'col-md-4' : 'col-md-6';
?>
<div class="core-school-form form-horizontal">

    <div class="letters">
        <div class="row">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>
    <div class="row card">
        <div class="card-header">
            <strong><?= isset($formTitle) ? $formTitle : "" ?></strong>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin([
                'fieldConfig' => function ($model, $attribute) {
                    return [
//                            'template' => "{label}{input}\n{hint}\n{error}",
                        'options' => ['class' => "col-sm-12 col-md-4"],
                        'labelOptions' => ['class' => ' col-form-label-sm'],
                        'inputOptions' => ['class' => 'form-control']
                    ];
                },
                'options' => ['class' => 'row'] //Form options
            ]);

            //Render fields
            if (isset($formConfig) && is_array($formConfig)) {
                Yii::trace($formConfig);
                foreach ($formConfig as $fieldConfig) {
                    if ($fieldConfig['controlType'] == 'date_picker')
                        echo $this->render('date_picker',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);
                    elseif ($fieldConfig['controlType'] == 'date_time_picker')
                        echo $this->render('date_time_picker',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);
                    else if ($fieldConfig['controlType'] == 'school_type_picker')
                        echo $this->render('school_type_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);


                    else if ($fieldConfig['controlType'] == 'school_id_picker')
                        echo $this->render('school_id_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);

                    else if ($fieldConfig['controlType'] == 'school_id_picker_subject')
                        echo $this->render('school_id_picker_subject',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);
                    else if ($fieldConfig['controlType'] == 'assessment_type')
                        echo $this->render('_assessment_type',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);

//                    else if ($fieldConfig['controlType'] == 'test_grouping')
//                        echo $this->render('_test_grouping',
//                            [
//                                'form' => $form,
//                                'model' => $formModel,
//                                'attribute' => $fieldConfig['attributeName'],
//                                'placeholder' => $fieldConfig['placeholder'],
//                                'prompt' => $fieldConfig['prompt']
//                            ]);

                    else if ($fieldConfig['controlType'] == 'class_id_picker')
                        echo $this->render('class_id_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);
                    else if ($fieldConfig['controlType'] == 'subject_id_picker')
                        echo $this->render('subject_id_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);
                    else if ($fieldConfig['controlType'] == 'teacher_id_picker')
                        echo $this->render('teacher_id_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);
                    else if ($fieldConfig['controlType'] == 'term_id_picker')
                        echo $this->render('term_id_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                                'placeholder' => $fieldConfig['placeholder'],
                                'prompt' => $fieldConfig['prompt']
                            ]);

                    else if ($fieldConfig['controlType'] == 'gender_picker')
                        echo $this->render('gender_picker',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);
                    else if ($fieldConfig['controlType'] == 'school_id_only')
                        echo $this->render('school_id_only',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);
                    else if ($fieldConfig['controlType'] == 'staff_levels_id')
                        echo $this->render('staff_levels',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);
                    else if ($fieldConfig['controlType'] == 'staff_type_id')
                        echo $this->render('staff_type',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);


                    else if ($fieldConfig['controlType'] == 'bank_name_search')
                        echo $this->render('bank_name_search',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);

                    else if ($fieldConfig['controlType'] == 'boolean')
                        echo $this->render('active_boolean',
                            [
                                'form' => $form,
                                'model' => $formModel,
                                'attribute' => $fieldConfig['attributeName'],
                            ]);

                    else
                        echo $form->field($formModel, $fieldConfig['attributeName'])->textInput();
                }
            }

            ?>


        </div>
        <div class="card-footer">

            <?= Html::submitButton($formModel->isNewRecord ? '<i class="fa fa-save"></i> Save' : '<i class="fa fa-save"></i> Update', ['class' => 'btn btn-md btn-primary']) ?>

        </div>


        <?php ActiveForm::end(); ?>
    </div>
    </div>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sacco\models\LoanRequestsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $data['title'];
$this->params['breadcrumbs'][] = $this->title;
$searchCols = $data['searchCols'];
$scw = $data['search_col_w'];
?>

<?php  if (\Yii::$app->session->hasFlash('successAlert')) : ?>
    <div class="notify notify-success">
        <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('successAlert'); ?>
        </div>
    </div>
<?php endif; ?>
<?php  if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>

<div class= "row hd-title" data-title="<?= Html::encode($this->title) ?>">

    <div class="col-xs-12">
        <div>
            <div class="col-sm-<?= $scw[0]?> col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
           <div class="col-sm-<?= $scw[1]?> col-xs-12 no-padding-right" ><?php echo $this->render('_search', ['model' => $searchModel, 'cols'=>$searchCols, 'mod'=>$mod]); ?></div>
            <div class="col-sm-<?= $scw[2]?> col-xs-12 no-padding">
                <ul class="menu-list pull-right">
                    <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']) ?>
                    </li>
                    <li>
                        <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
   <?= $this->render('_table', ['data'=>$data, 'mod'=>$mod])?>
    </div>

</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>
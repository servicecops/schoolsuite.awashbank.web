<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\models\SchoolInformation;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

?>
<?php

$baseLogoUrl = Url::to(['/import/import/image-link2']);

if(!isset($theme)) $theme = Select2::THEME_BOOTSTRAP;
if(!isset($size)) $size = Select2::MEDIUM;
if(!isset($inputId)) $inputId = '__school_search';
if(!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if(!isset($allowClear)) $allowClear = true;
if(!isset($maximumInputLength)) $maximumInputLength = 3; //Default to 3
if(!isset($modelAttribute)) throw new Exception('School attribute is required for search component');
if(!isset($form)) throw new Exception('Component should be part of a form');
if(!isset($inputClass)) $inputClass = 'form-control';
if(!isset($label)) $label = false;
if(!isset($showLogoAfterSelect)) $showLogoAfterSelect = false;
if(!isset($primaryBank)) $primaryBank = '';

$searchJS = <<< JS
//This will format school results
var baseLogo =  '$baseLogoUrl';
var showLogoAfterSelect = '$showLogoAfterSelect';

var ajaxCall = function(params) { 
    return {q:params.term, bank:'$primaryBank'}; 
}

 var formatSchoolResult = function(repo) {
       if (repo.loading) {
        return repo.text;
    }
    
    return formatSchoolWithLogo(repo)
    }
    
    var formatSchoolSelection = function (repo) {
     if(!showLogoAfterSelect) return repo.text;
    return formatSchoolWithLogo(repo)
    }
    
    var formatSchoolWithLogo = function(repo) {
     var logo = '';   
    if(repo.school_logo) {
        logo = baseLogo + '?id=' + repo.school_logo;
    }   
    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + logo + '" class="img-rounded" style="width:30px" />' +
        '<b style="margin-left:5px">' + repo.text + '</b>' + 
    '</div>' +
   
'</div>';
    
    return '<div style="overflow:hidden;">' + markup + '</div>';
    }
JS;

$this->registerJs($searchJS, View::POS_HEAD);



$url = Url::toRoute(['/core-school/schoollist']);
$selectedSchool = empty($model->$modelAttribute) ? '' : SchoolInformation::findOne($model->$modelAttribute)->school_name;
echo $form->field($model, $modelAttribute)->widget(Select2::classname(), [
    'initValueText' => $selectedSchool, // set the initial display text
    'theme'=>$theme,
    'size'=>$size,
    'options' => [
        'placeholder' => $placeHolder,
        'id'=> $inputId,'class'=>$inputClass
    ],
    'pluginOptions' => [
        'placeholder' => $placeHolder,
        'allowClear' => $allowClear,
        'minimumInputLength' => $maximumInputLength,
        'language' => [
            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
        ],
        'ajax' => [
            'url' => $url,
            'dataType' => 'json',
            'data' => new JsExpression('ajaxCall')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('formatSchoolResult'),
        'templateSelection' => new JsExpression('formatSchoolSelection'),

    ],
])->label($label); ?>

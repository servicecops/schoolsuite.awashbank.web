<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreTerm;
use kartik\select2\Select2;use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

?>
<?php

if (!isset($inputId)) $inputId = '__subject_id_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Subject';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

$subjectModel = new  CoreSubject();
$termModel= new CoreTerm();
if($model = $subjectModel){
    $theId = 'coresubject';
}
elseif ($model = $termModel){
    $theId = 'coreterm';
}
else{
    $theId =null;
}

$types = CoreSubject::find()->orderBy(['id'=>'ASC'])->all();


        $url = Url::to(['core-school/active-schoollist']);
        $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
        echo $form->field($model, 'school_id')->widget(Select2::classname(), [
            'initValueText' => $selectedSchool, // set the initial display text
            'size' => 'sm',
            'theme' => Select2::THEME_BOOTSTRAP,
            'class' => 'form-control  input-sm inputRequired',
            'options' => [
                'placeholder' => 'Filter School',
                'id' => 'school_search',
                'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#'.$theId.'-class_id" ).html(data);
                        });'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label('School'); ?>



    <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class',
        'onchange' => '$.post( "' . Url::to(['core-subject/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#'.$theId.'-id" ).html(data);
                        });'])->label('Class') ?>

<?= $form->field($model, 'id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label('Class') ?>


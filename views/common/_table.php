<?php

use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

$sort = $data['sort'];
$cols = $data['cols'];
$searchCols = $data['searchCols'];
$pages = $data['pages'];
$records = $data['query'];
$scw = $data['search_col_w'];
$striped = (isset($data['striped']) && !$data['striped']) ? false : true;
$initOptions = ['type'=>'string', 'label'=>" ", 'val'=>null];
?>

<div class="row">
    <div class="col-md-12">
        <div class="">
            <table class="table table-striped">
                <thead>
                <?php
                $col_count = 0;
                foreach ($cols as $col=>$options) :
                    $opt = array_merge($initOptions, $options);
                    if($sort->hasAttribute($col)): ?>
                        <th><?= $sort->link($col, ['label'=>$opt['label']]) ?></th>
                    <?php else : ?>
                        <th><?= $opt['label'] ?></th>
                    <?php endif; ?>
                    <?php
                    $col_count++;
                endforeach; ?>
                </thead>
                <tbody>
                <?php
                if($records) :
                    foreach($records as $k=>$v) : ?>
                        <tr>
                            <?php foreach ($cols as $col=>$options) :
                                $opt = array_merge($initOptions, $options);
                                $val = null;
                                $not_set = "(not-set)";
                                $cssClass = (isset($opt['cssClass']) && $opt['cssClass']) ? $opt['cssClass'] : '';
                                switch ($opt['type']) {
                                    case 'number':
                                        $val = number_format($v[$col]);
                                        $not_set =0;
                                        break;
                                    case 'date':
                                        $format = isset($opt['format']) ? $opt['format'] : 'Y-m-d H:i';
                                        $val = date($format, strtotime($v[$col]));
                                        break;
                                    case 'url':
                                        $link = isset($opt['url']['link']) ? [ $opt['url']['link'] ] : ['#'];
                                        $descParam = isset($opt['url']['desc']) ? $opt['url']['desc'] : "";

                                        if($params = $opt['url']['params']){
                                            foreach($params as $id=>$param){
                                                $link[$id] = $id=='mod' ? $param : $v[$param];
                                            }
                                        }
                                        $description = isset($v[$descParam]) ? $v[$descParam] : $opt['desc'];
                                        $val = "<a class='aclink' href='".Url::to($link)."'>".$description."</a>";

                                        break;
                                    case 'merge':
                                        if(in_array('cols', $opt)){
                                            $fmt=[];
                                            foreach($opt['cols'] as $n=>$m){
                                                $fmt[$n] = $v[$m];
                                            }
                                            $val = $opt['format'] ;
                                        }
                                        break;
                                    case 'action':
                                        $links = $opt['links'];
                                        $urls = [];
//                                    'params'=>['id'=>'id', 'mod'=>'mod'],

                                        foreach($links as $link){
                                            $lnk = isset($link['url']) ? [ $link['url'] ] : ['#'];
                                            $params = isset($link['params']) ? $link['params'] : ['mod'=>$mod, 'id'=>'id'];
                                            $htmlOptions = isset($link['options']) ? $link['options'] : '';
                                            $class = isset($link['class']) ? $link['class'] : "aclink";

                                            foreach($params as $id=>$param){
                                                $lnk[$id] = ($id=='mod') ? $param : $v[$param];
                                            }
                                            $linkDesc = isset($link['desc']) ? $link['desc'] : "";
                                            if(isset($link['modal_link'])){
                                                $class = isset($link['class']) ? $link['class'] : "modal_link";
                                                $urls[] = "<a class='".$class."' href='javascript:;' data-href='".Url::to($lnk)."' data-title='".$link['title']."' ".$htmlOptions."><i class='".$link['icon']."'>".$linkDesc."</i></a>";
                                            } else{
                                                $urls[] = "<a class='".$class."' href='".Url::to($lnk)."' title='".$link['title']."' ".$htmlOptions."><i class='".$link['icon']."'>".$linkDesc."</i></a>";
                                            }
                                        }
                                        $val = implode("&nbsp;&nbsp;&nbsp;", $urls);
                                        break;
                                    default:
                                        $val = $v[$col];
                                        break;
                                }

                                if(isset($opt['has-error']) && $v[$opt['has-error']]){
                                    $cssClass = 'text-danger';
                                }
                                $exceptions = ['action', 'url'];
                                ?>

                                <td><?= (in_array($opt['type'], $exceptions) || $v[$col]) ? "<span class='".$cssClass."'>".$val."</span>" : "<span class='not-set'> {$not_set}</span>" ?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach;
                else :
                    ?>
                    <tr><td colspan=<?= $col_count+1;?>>No records found</td></tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages,
            'options' => [
                'id'=>'pg_'.$mod,
                'class'=>'pagination',
            ]
        ]);?>

    </div>
</div>


<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\banks\models\BankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use yii\helpers\ArrayHelper;

?>
<?php

if (!isset($inputId)) $inputId = '__school_type_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select School Type';
$label = isset($label) ? $label : $model->getAttributeLabel($attribute);

$types = CoreControlSchoolTypes::find()->orderBy(['description'=>'ASC'])->all();

echo $form
    ->field($model, $attribute)
    ->dropDownList(ArrayHelper::map($types, 'id', 'description'), [
        'placeholder'=>$placeHolder,
        'prompt'=>$prompt,
        'id'=>'school_id',
    ])
    ->label($label); ?>

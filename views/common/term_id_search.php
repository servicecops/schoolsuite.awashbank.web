<?PHP
///**
// * This component will provide a search of a school using select 2
// */
//
//
//use app\modules\schoolcore\models\CoreTerm;
//use yii\helpers\ArrayHelper;
//
//?>
<?php
//
//if (!isset($inputId)) $inputId = '__term_id_search';
//if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
//if (!isset($attribute)) throw new Exception('Attribute is required for search component');
//if (!isset($form)) throw new Exception('Component should be part of a form');
//if (!isset($inputClass)) $inputClass = 'form-control';
//if (!isset($prompt)) $prompt = 'Select Term';
//$label = isset($label) ? $label : $model->getAttributeLabel($attribute);
//
//$types = CoreTerm::find()->where([])->orderBy(['id'=>'ASC'])->all();
//
//echo $form
//    ->field($model, $attribute)
//    ->dropDownList(ArrayHelper::map($types, 'id', 'term_description'), [
//        'placeholder'=>$placeHolder,
//        'prompt'=>$prompt,
//        'id'=>'term_id',
//    ])
//    ->label($label); ?>


<?PHP
/**
 * This component will provide a search of a school using select 2
 */

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreTest;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

?>
<?php

$testModel = new  CoreTest();
$termModel = new CoreTerm();
$subjectModel = new CoreSubject();

if (!isset($inputId)) $inputId = '__school_id_search';
if (!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if (!isset($attribute)) throw new Exception('Attribute is required for search component');
if (!isset($form)) throw new Exception('Component should be part of a form');
if (!isset($inputClass)) $inputClass = 'form-control';
if (!isset($prompt)) $prompt = 'Select Class';



if ($model == $subjectModel)$theId= 'coresubject';

elseif ($model == $testModel) $theId= 'coretest';
elseif ($model == $termModel) $theId= 'coreterm';
else $theId= 'null';

?>


<?php if (\app\components\ToWords::isSchoolUser()) : ?>


    <?php
    $data = CoreTerm::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

    echo $form->field($model, 'term_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($data, 'id', 'term_description'),
        'language' => 'en',
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Find Term'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label('Term *'); ?>


<?php endif; ?>
<?php if (Yii::$app->user->can('schoolpay_admin')) : ?>


    <?php
    $url = Url::to(['core-school/active-schoollist']);
    $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
    echo $form->field($model, 'school_id')->widget(Select2::classname(), [
        'initValueText' => $selectedSchool, // set the initial display text
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => [
            'placeholder' => 'Filter School',
            'id' => 'school_search',
            'class' => 'form-control',
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
            ],
            'ajax' => [
                'url' => $url,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
        ],
    ])->label(); ?>




    <?= $form->field($model, 'term_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
        ['prompt' => 'Filter term'])->label() ?>

<?php endif; ?>

<?php
$url = Url::to(['core-term/lists']);
$cls = $model->class_id;

$campusurl = Url::to(['/school-information/campuslists']);

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
    
    }
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>

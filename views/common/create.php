<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\sacco\models\SaccoLoanRequests */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sacco Loan Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$formOptions = isset($model->formOptions) ? $model->formOptions : [];
?>
<div class="hd-title" data-title="<?= Html::encode($this->title) ?>">



            <?php $form = ActiveForm::begin($formOptions);?>
    <div class="row">
           <?php echo $this->render('_form', ['model' => $model, 'form' => $form]) ?>
    </div>


            <div class="col-md-12 form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Edit', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

</div>


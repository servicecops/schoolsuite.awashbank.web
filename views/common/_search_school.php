<?php

use app\modules\schoolcore\models\CoreBankDetails;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card-body">
<?php $form = ActiveForm::begin([
    'action' => ['search'],
    'method' => 'get',
    'options'=>['class'=>'formprocess'],
]); ?>
<div class="row">

        <div class="col-md-3 no-padding">
            <?= $form->field($schoolModel, 'school_code',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Code'])->label(false) ?>
        </div>
        <div class="col-md-4 no-padding">
            <?= $form->field($schoolModel, 'school_name',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Name'])->label(false) ?>
        </div>

        <div class="col-md-3 no-padding"><?= $form->field($schoolModel, 'bank_name', ['inputOptions'=>[ 'class'=>'form-control input-sm']] )->dropDownList(ArrayHelper::map(CoreBankDetails::findAllNominatedBanks(), 'id', 'bank_name'),
                ['prompt'=>'Primary Bank'])->label(false) ?></div>

        <div class="col-md-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>

</div>
<?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>
<div class='col-sm-12'>

    <?php $form = ActiveForm::begin(['options'=>['class'=>'formprocess']]); ?>

    <?= $form->field($model, 'name')->textInput()->label('New Role Name') ?>

    <?= $form->field($model, 'description')->textInput() ?>
    <div class="row">
        <div class="form-group col-xs-12 no-padding">
            <div class="col-xs-6">
                <?= Html::submitButton($model->isNewRecord ? 'Add Role' : 'Edit Role', ['class' => 'btn btn-block btn-info']) ?>
            </div>
            <div class="col-xs-6">
                <?= Html::resetButton('Reset Form', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

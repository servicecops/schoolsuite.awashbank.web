<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row" data-title="Add Roles">

    <div class="col-xs-12">
      <div class="col-lg-8 col-sm-8 col-xs-12 no-padding"><h3 class="box-title"><i class="fab fa-critical-role"></i> <?php echo $this->title ?></h3></div>
        <div class="col-xs-4 no-padding" style="padding-top: 15px !important;">
            <?= Html::a('Add Role <i class="fa fa-plus"></i>', ['/roles/create'], ['class' => 'btn btn-info pull-right aclink']) ?>

        </div>
      </div>
    <div class='col-md-12'>
    <div class="box-body table table-responsive no-padding">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        //'filterModel' => $searchModel,
        'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            [
            'header'=>'Sr.No',
            'class' => 'yii\grid\SerialColumn'
            ],

            'name',
            [
                'attribute'=>'type',
                'value'=>function($model){
                    return ($model->type == 1) ? 'Role' : 'Permission';
                }

            ],
            'description:ntext',
            [
                'options'=>['style'=>'width:15px;'],
                'value'=> function($model){
                    return Html::a('<i class="fa fa-exchange"></i>', ['perms', 'name'=>$model->name], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],

            [
                'options'=>['style'=>'width:10px;'],
                'value'=> function($model){
                    return Html::a('<i class="fa fa-eye"></i>', ['view', 'id'=>$model->name], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
//            [
//                'options'=>['style'=>'width:15px;'],
//                'value'=> function($model){
//                    return (count($model->authAssignments) == 0 && $model->name !='super_admin') ? Html::a('<i class="fa fa-remove"></i>', ['/roles/delete', 'id'=>$model->name], ['class'=>'aclink', 'data'=>['method'=>'post', 'confirm'=>"Are you sure you want to delete this record"]]) : false;
//
//                    // '<a class="aclink" data-confirm="Are you sure you want to delete this record"> <i class="fa fa-remove" href="'.Url::to(["/roles/delete", "id"=>$model->name]).'"></i></a>' : false;
//                },
//                'format'=>'html',
//            ],
        ],
    ]); ?>
    </div>
</div>
</div>


<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */

$this->title = $model->name;

?>
<div class="container bg-white">

    <h1 class="text-center" style="color: #000"><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="text-white"><i class="fa fa-edit"></i>Update</span>', ['update', 'id' => $model->name], ['class' => 'btn bg-gradient-primary']) ?>
        <?= Html::a('<span class="text-white"><i class="fa fa-trash"></i>Delete</span>', ['delete', 'id' => $model->name], [
            'class' => 'btn bg-gradient-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
            'name',
            'type',
            'description:ntext',
            'rule_name',
            'data:ntext',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>



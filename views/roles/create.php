<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */

$this->title = 'Create New User Role';

?>

<div class="col container">
    <div class="letter">
        <div class="row">
            <div class="col-md-12" style="text-align:center; color:#565656;"><h3><i class="fa fa-pen"></i>&nbsp;
                    &nbsp; <?= Html::encode($this->title) ?></h3></h3>
                <hr class="style14">
            </div>

            <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
                <div class="notify notify-danger">
                    <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
                    <div class="notify-content">
                        <?= \Yii::$app->session->getFlash('viewError'); ?>
                    </div>
                </div>
            <?php endif; ?>
            <p>&nbsp;</p>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>



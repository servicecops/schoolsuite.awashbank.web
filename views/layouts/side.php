<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

$isSchoolpayAdmin = \Yii::$app->user->can('schoolpay_admin');
$isSchoolUser = \app\components\ToWords::isSchoolUser();

$isBankUser = Yii::$app->user->can('bank_user')
?>
<?php
// $role = Yii::$app->user->identity->user_level;
// if($this->beginCache('side_menu_'.$role, ['duration' => 3600] )) :
?>
<!-- <div class="main-sidebar"> -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar fixed">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::$app->request->baseUrl ?>/web/img/avatar.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= (!\Yii::$app->user->isGuest) ? Yii::$app->user->identity->fullname : "Guest User"; ?></p>
                <a href="javascript:;"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <?php $form = ActiveForm::begin([
            'action' => ['/student/index'],
            'method' => 'get',
            'options' => ['class' => 'formprocess sidebar-form', 'id' => 'global_search_student_name'],
        ]); ?>
        <div class="input-group">
            <!-- <input type="text" name="q" class="form-control" placeholder="Search..." /> -->
            <input type="text" class="form-control no-padding-right" name="StudentSearch[modelSearch]"
                   placeholder="Search Name / Code"/>
            <span class="input-group-btn">
                <button type="submit" name="search" id="studentsearch-btn" class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
            </span>
        </div>
        <?php ActiveForm::end(); ?>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="clink treeview" id="home">
                <a href="<?= Url::to(['/site/index']); ?>">
                    <i class="fa fa-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview" id="my_account">
                <a href="#">
                    <i class="fa fa-user"></i> <span>My Account</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="clink" id="my_profile"><a href="<?= Url::to(['/user/myaccountdetails']); ?>"><i
                                    class="far fa-circle"></i>Account</a></li>
                    <li class="clink" id="reset_password">
                        <a href="<?= Url::to(['/user/change', 'id' => \Yii::$app->user->id]); ?>">
                            <i class="far fa-circle"></i><span style="margin-left: 5px">Reset Password</span></a>
                    </li>
                </ul>
            </li>
            <?php if (\Yii::$app->user->can('rw_user')) : ?>
                <li id="user" class="treeview">
                    <a href="#">
                        <i class="fa fa-user-plus"></i>
                        <span>Users Panel</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="clink" id="user_index"><a href="<?= Url::to(['/user/index']); ?>"><i
                                        class="fa fa-user-times"></i>System Users</a></li>
                        <?php if (\Yii::$app->user->can('manage_mobile_user')) : ?>
                            <li class="clink" id="mobile_users"><a href="<?= Url::to(['/banks/mobile-access']); ?>"><i
                                            class="fa fa-phone-square"></i>Mobile Users</a></li>
                        <?php endif; ?>
                        <li class="clink" id="user_create"><a href="<?= Url::to(['/user/create']); ?>"><i
                                        class="fa fa-user-plus"></i>Add User</a></li>
                        <?php if (\Yii::$app->user->can('rw_role')) : ?>
                            <li class="clink" id="user_create"><a href="<?= Url::to(['/roles']); ?>"><i
                                            class="fa fa-gears"></i>Roles & Permissions</a></li>
                            <li class="clink" id="user_create"><a href="<?= Url::to(['/roles/create']); ?>"><i
                                            class="fa fa-gear"></i>Add New Role</a></li>
                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>

            <?php if (\Yii::$app->user->can('r_sch')) : ?>
                <li class="treeview" id="schools">
                    <a href="#">
                        <i class="fa fa-institution"></i> <span>Schools</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>

                    <ul class="treeview-menu">
                        <?php if (\Yii::$app->user->can('schoolsuite_admin')) :

                            ?>
                            <li class="clink" id="school_index"><a
                                        href="<?= Url::to(['/schoolcore/core-school/index']); ?>"><i
                                            class="fa fa-sun-o"></i>Manage Schools</a>
                            </li>
                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('rw_sch')) : ?>
                            <li class="clink" id="school_create"><a
                                        href="<?= Url::to(['/schoolcore/core-school/create']); ?>"><i
                                            class="fa fa-sun-o"></i>Add
                                    School</a>
                            </li>

                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('schoolsuite_admin')) :

                            ?>
                            <li class="clink" id="school_providers"><a
                                        href="<?= Url::to(['/schoolcore/core-school/sync-report']); ?>"><i
                                            class="fa fa-sun-o"></i>Schoolpay sync report</a>
                            </li>
                        <?php endif; ?>
                        <?php if (\app\components\ToWords::isSchoolUser()) :
                            $schoolId = Yii::$app->user->identity->school_id;
                            ?>
                            <li class="clink" id="classes_create"><a
                                        href="<?= Url::to(['/schoolcore/core-school/view', 'id' => $schoolId]); ?>"><i
                                            class="fa fa-sun-o"></i>View school</a></li>
                        <?php endif; ?>

                    </ul>

                </li>
            <?php endif; ?>

            <li class="treeview" id="workflow">
                <a href="#">
                    <i class="fa fa-flag"></i> <span>Workflow</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                    <li class="clink" id="school_index"><a href="<?= Url::to(['/workflow']); ?>"><i
                                    class="fa fa-circle-o"></i>Workflow & Approvals</a></li>
                </ul>

            </li>


            <?php if (\Yii::$app->user->can('r_student')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Students</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="clink" id="student_index"><a
                                    href="<?= Url::to(['/schoolcore/core-student/index']); ?>"><i
                                        class="fa fa-users"></i>Students List</a></li>
                        <?php if (\Yii::$app->user->can('rw_student')) : ?>
                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-student/create']); ?>"><i
                                            class="fa fa-user-plus"></i>Add New Student</a></li>
                            <li class="clink" id="upload_excel"><a
                                        href="<?= Url::to(['/schoolcore/core-student/upload-excel']); ?>"><i
                                            class="fa fa-file-excel-o"></i>Import Students</a></li>
                            <li class="clink" id="uploaded_files"><a
                                        href="<?= Url::to(['/schoolcore/core-student/uploaded-files']); ?>"><i
                                            class="fa fa-folder"></i>Uploaded files</a></li>
                            <li class="clink" id="upload_excel"><a
                                        href="<?= Url::to(['/schoolcore/core-student/archived']); ?>"><i
                                            class="fa fa-lock"></i>Archived Students</a></li>
                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>



            <?php if (\Yii::$app->user->can('r_term')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Terms</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="clink" id="student_index"><a
                                    href="<?= Url::to(['/schoolcore/core-term/create']); ?>"><i
                                        class="fa fa-users"></i>Creat term</a></li>

                        <li class="clink" id="student_create"><a
                                    href="<?= Url::to(['/schoolcore/core-term/index']); ?>"><i
                                        class="fa fa-user-plus"></i>Manage term</a></li>


                    </ul>
                </li>
            <?php endif; ?>

            <?php if (\Yii::$app->user->can('r_attendance')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Attendance</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Yii::$app->user->can('w_attendance')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/attendance/rollcall/display-class']); ?>"><i
                                            class="fa fa-users"></i>Subject Students Rollcall</a></li>

                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/attendance/rollcall/classes']); ?>"><i
                                            class="fa fa-user-plus"></i>General Student Rollcall</a></li>

                        <?php endif; ?>


                        <li class="clink" id="student_index"><a
                                    href="<?= Url::to(['/attendance/rollcall/view-student-attendance']); ?>"><i
                                        class="fa fa-users"></i>Student Subject Attendance</a></li>

                        <li class="clink" id="student_create"><a
                                    href="<?= Url::to(['/attendance/rollcall/view-gen-student-attendance']); ?>"><i
                                        class="fa fa-user-plus"></i>General Student Attendance</a></li>


                    </ul>
                </li>
            <?php endif; ?>

            <?php if (\Yii::$app->user->can('r_class')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Classes</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Yii::$app->user->can('w_attendance')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-school-class/index']); ?>"><i
                                            class="fa fa-users"></i>View classes</a></li>

                            <?php if (\Yii::$app->user->can('rw_class')) : ?>

                                <li class="clink" id="student_create"><a
                                            href="<?= Url::to(['/schoolcore/core-school-class/create']); ?>"><i
                                                class="fa fa-user-plus"></i>Create class</a></li>

                            <?php endif; ?>


                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-school-class/upload-excel']); ?>"><i
                                            class="fa fa-users"></i>Import classes</a></li>

                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-school-class/promotion-studio']); ?>"><i
                                            class="fa fa-user-plus"></i>Promotion studio</a></li>

                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>


            <?php if (\Yii::$app->user->can('r_staff')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Staff</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                        <li class="clink" id="student_index"><a
                                    href="<?= Url::to(['/schoolcore/core-staff/index']); ?>"><i
                                        class="fa fa-users"></i>View staff</a></li>

                        <?php if (\Yii::$app->user->can('rw_class')) : ?>

                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-staff/create']); ?>"><i
                                            class="fa fa-user-plus"></i>Create staff</a></li>


                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-staff/upload-excel']); ?>"><i
                                            class="fa fa-users"></i>Import staff</a></li>

                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-staff/uploaded-files']); ?>"><i
                                            class="fa fa-user-plus"></i>Uploaded files</a></li>
                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('rw_classcddff')) : ?>
                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-staff/email']); ?>"><i
                                            class="fa fa-user-plus"></i>Write email</a></li>


                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>


            <?php if (\Yii::$app->user->can('r_subject') || !Yii::$app->user->can('non_student')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Subject</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Yii::$app->user->can('r_subject')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-subject/display-class']); ?>"><i
                                            class="fa fa-users"></i>View subjects</a></li>
                        <?php endif; ?>

                        <?php if (!Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-subject/student-subject']); ?>"><i
                                            class="fa fa-user-plus"></i>My subject</a></li>

                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('rw_subject')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-subject/create']); ?>"><i
                                            class="fa fa-users"></i>Add
                                    subject</a></li>

                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-subject/upload-excel']); ?>"><i
                                            class="fa fa-user-plus"></i>Import subjects</a></li>

                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (\Yii::$app->user->can('r_test') || !Yii::$app->user->can('non_student')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Tests/exams</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Yii::$app->user->can('rw_test')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-test/display-class']); ?>"><i
                                            class="fa fa-users"></i>Add test/exam</a></li>
                        <?php endif; ?>

                        <?php if (\Yii::$app->user->can('r_test')) : ?>
                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-test/display-class-view']); ?>"><i
                                            class="fa fa-user-plus"></i>View test/exam</a></li>

                        <?php endif; ?>
                        <?php if (!Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-test/student-tests']); ?>"><i
                                            class="fa fa-users"></i>Class tests</a></li>


                        <?php endif; ?>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (\Yii::$app->user->can('r_results') || !Yii::$app->user->can('non_student')) : ?>
                <li class="treeview" id="students">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Results</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if (\Yii::$app->user->can('rw_results')) : ?>
                            <li class="clink" id="student_index"><a
                                        href="<?= Url::to(['/schoolcore/core-marks/tests']); ?>"><i
                                            class="fa fa-users"></i>Manage results</a></li>
                        <?php endif; ?>

                        <?php if (!Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="student_create"><a
                                        href="<?= Url::to(['/schoolcore/core-marks/student-results']); ?>"><i
                                            class="fa fa-user-plus"></i>My results</a></li>

                        <?php endif; ?>

                    </ul>
                </li>
            <?php endif; ?>

            <?php if (\Yii::$app->user->can('rw_banks')) : ?>
                <li class="treeview" id="banks">
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>Banks</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="clink" id="bank-index"><a
                                    href="<?= Url::to(['/schoolcore/core-bank-details/index']); ?>"><i
                                        class="fa fa-circle-o"></i>Banks</a></li>
                        <li class="clink" id="bank-create"><a
                                    href="<?= Url::to(['/schoolcore/core-bank-details/create']); ?>"><i
                                        class="fa fa-circle-o"></i>Add Bank</a></li>
                    </ul>
                </li>
            <?php endif; ?>


            <?php if (\Yii::$app->user->can('w_msg_cr')) : ?>
                <li class="treeview" id="banks">
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>Messages</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="clink" id="bank-index"><a href="<?= Url::to(['/messages/e/sch-credits']); ?>"><i
                                        class="fa fa-circle-o"></i>Account history</a></li>
                        <li class="clink" id="bank-create"><a href="<?= Url::to(['/messages/e/sch-messages']); ?>"><i
                                        class="fa fa-circle-o"></i>Messages</a></li>
                        <li class="clink" id="bank-create"><a href="<?= Url::to(['/messages/e/sch-outbox']); ?>"><i
                                        class="fa fa-circle-o"></i>Message Outbox</a></li>
                    </ul>
                </li>
            <?php endif; ?>


            <?php if (\app\components\ToWords::isSchoolUser()) : ?>
                <?php if (Yii::$app->user->identity->school->hasModule('SCHOOLPAY_MESSAGES')) : ?>
                    <li class="treeview" id="banks">
                        <a href="#">
                            <i class="fa fa-building"></i>
                            <span>Messages</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="clink" id="bank-index"><a
                                        href="<?= Url::to(['/messages/e/sch-messages/create']); ?>"><i
                                            class="fa fa-circle-o"></i>New school message</a></li>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/messages/e/sch-credits/view', 'id' => Yii::$app->user->identity->school_id]); ?>"><i
                                            class="fa fa-circle-o"></i>Message Account</a></li>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/messages/e/sch-messages']); ?>"><i
                                            class="fa fa-circle-o"></i>Messages</a></li>
                            <li class="clink" id="bank-create"><a href="<?= Url::to(['/messages/e/sch-outbox']); ?>"><i
                                            class="fa fa-circle-o"></i>Message outbox</a></li>
                        </ul>
                    </li>
                <?php endif; ?>

            <?php endif; ?>



            <?php if (\Yii::$app->user->can('r_grade') || !Yii::$app->user->can('non_student')) : ?>
                <li class="treeview" id="banks">
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>Grading</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">.
                        <?php if (\Yii::$app->user->can('rw_grade')) : ?>
                            <li class="clink" id="bank-index"><a
                                        href="<?= Url::to(['/schoolcore/core-school-class/add-grades']); ?>"><i
                                            class="fa fa-circle-o"></i>Add grades</a></li>
                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('r_grade')) : ?>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/gradingcore/grading/index']); ?>"><i
                                            class="fa fa-circle-o"></i>Manage grades</a></li>
                        <?php endif; ?>
                        <?php if (!Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/gradingcore/grading/student-class-grades']); ?>"><i
                                            class="fa fa-circle-o"></i>Class grades</a></li>
                        <?php endif; ?>

                    </ul>
                </li>


            <?php endif; ?>


            <?php if (\Yii::$app->user->can('r_classwork') || !\Yii::$app->user->can('non_student')) : ?>
                <li class="treeview" id="banks">
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>E-learning</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">.
                        <?php if (\Yii::$app->user->can('rw_classwork')) : ?>
                            <li class="clink" id="bank-index"><a
                                        href="<?= Url::to(['/e_learning/elearning/display-class']); ?>"><i
                                            class="fa fa-circle-o"></i>View class work</a></li>
                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('r_classwork') || \Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/e_learning/elearning/display-teachers-class']); ?>"><i
                                            class="fa fa-circle-o"></i>Add class work</a></li>
                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('r_classwork') || \Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/e_learning/elearning/display-teachers-class-submission']); ?>"><i
                                            class="fa fa-circle-o"></i>Student submission</a></li>
                        <?php endif; ?>
                        <?php if (!\Yii::$app->user->can('non_student')) : ?>
                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/e_learning/elearning/student-class-subject-submission']); ?>"><i
                                            class="fa fa-circle-o"></i>Submit students work</a></li>


                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/e_learning/elearning/student-class-subject']); ?>"><i
                                            class="fa fa-circle-o"></i>View class work</a></li>

                            <li class="clink" id="bank-create"><a
                                        href="<?= Url::to(['/e_learning/elearning/student-view-subject-submission']); ?>"><i
                                            class="fa fa-circle-o"></i>My submissions</a></li>
                        <?php endif; ?>

                    </ul>
                </li>


            <?php endif; ?>


            <li>
                <p>&nbsp;</p>
                <br>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<!-- </div> -->

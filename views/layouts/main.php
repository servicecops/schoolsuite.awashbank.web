<?php

/* @var $this View */

/* @var $content string */

use app\assets\DashboardAsset;
use app\assets\LoginAsset;
use app\components\Helpers;
use app\modules\attendance\models\TeacherDailyAttendance;
use app\modules\schoolcore\models\CoreStudent;
use app\widgets\Alert;
use kartik\ipinfo\IpInfo;
use kartik\popover\PopoverX;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use yii\web\View;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

DashboardAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody();
//

?>

<body class="nav-md menu_fixed">
<div class="container body ">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?= Url::to(['/site/index']); ?>" class="site_title">
                        <?= Html::img('@web/web/website/assets/img/index_logo.png', ['alt' => 'logo', 'class' => 'light-logo',]); ?></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <?= Html::img('@web/web/img/user.png', ['alt' => 'user', 'class' => 'img-circle profile_img']); ?>
                    </div>
                    <div class="profile_info">
                        <p style="color: white"><?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->fullname : "Guest User"; ?></p>
                        <a style="color: white" href="javascript:"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br/>
                <!-- /.search form -->
                <?php $form = ActiveForm::begin([
                    'action' => ['/schoolcore/core-student/index'],
                    'method' => 'get',
                    'options' => ['id' => 'global_search_student_name'],
                ]); ?>

                <div class="input-group container d-none d-lg-block">

                    <div class="wrap">
                        <div class="search">
                            <input type="text" class="searchTerm" name="CoreStudentSearch[modelSearch]"
                                   placeholder="Enter student code/name">
                            <button type="submit" class="searchButton" id="studentsearch-btn">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <?php ActiveForm::end(); ?>

                <!-- /.search form -->
                <!-- sidebar menu: : style can be found in custom.less -->
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu ">
                    <div class="menu_section">

                        <ul class="nav side-menu">
                            <li><a href="<?= Url::to(['/site/index']); ?>"><i class="fa fa-home"></i> Home</a></li>

                            <!--                        Accounts and security completed-->
                            <li>
                                <a>
                                    <i class="fa fa-users"></i> Accounts & security <span
                                            class="fa fa-chevron-down"></span> </i>
                                </a>
                                <ul class="nav child_menu">

                                    <li><a href="<?= Url::to(['/user/my-account-profile']); ?>">Account</a></li>
                                    <li><a href="<?= Url::to(['/user/change', 'id' => \Yii::$app->user->id]); ?>">Reset
                                            My password</a></li>


                                    <?php if (Helpers::is('rw_role')) : ?>
                                        <li><a href="<?= Url::to(['/roles/index']); ?>">Roles and permissions</a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if (Helpers::is('rw_user')) : ?>
                                        <li><a href="<?= Url::to(['/user/index']); ?>"> User management</a></li>
                                        <li><a href="<?= Url::to(['/user/archived']); ?>"> Archived Users</a></li>
                                    <?php endif; ?>
                                    <?php if (Yii::$app->user->can('view_logs')) : ?>
                                        <li><a href="<?= Url::to(['/logs/default/index']); ?>">System logs</a></li>
                                    <?php endif; ?>
                                </ul>
                            </li>

                            <!--                        school center-->
                            <?php if (Helpers::is('r_sch') || !Helpers::is('non_student'))   : ?>
                                <li><a href="<?= Url::to(['/schoolcore/core-school/menu']) ?>"><i
                                                class="fa fa-university"></i> School center </i></a>
                                </li>
                            <?php endif; ?>
                            <!-- end school center -->


                            <!--messages-->
                            <?php if (Yii::$app->user->can('w_msg_cr')) : ?>
                                <li><a><i class="fa fa-comment"></i> Messages<span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?= Url::to(['/messages/e/sch-credits']); ?>">Account
                                                history</a>
                                        </li>
                                        <li><a href="<?= Url::to(['/messages/e/sch-messages']); ?>">Messages</a>
                                        </li>
                                        <li><a href="<?= Url::to(['/messages/e/sch-outbox']); ?>">Message outbox</a>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>

                            <?php if (\app\components\ToWords::isSchoolUser()) : ?>


                                <?php if (Yii::$app->user->identity->school->hasModule('SCHOOLPAY_MESSAGES')): ?>
                                    <li><a><i class="fa fa-comment"></i> Messages <span
                                                    class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="<?= Url::to(['/messages/e/sch-messages/create']); ?>">New
                                                    sch
                                                    message</a></li>
                                            <li>
                                                <a href="<?= Url::to(['/messages/e/sch-credits/view', 'id' => Yii::$app->user->identity->school_id]); ?>">Message
                                                    account</a></li>
                                            <li><a href="<?= Url::to(['/messages/e/sch-messages']); ?>">Messages</a>
                                            </li>
                                            <li><a href="<?= Url::to(['/messages/e/sch-outbox']); ?>">Message
                                                    outbox</a>
                                            </li>

                                        </ul>
                                    </li>

                                <?php endif; ?>
                            <?php endif; ?>


                            <!--                                finance and payments center-->
                            <?php if (Helpers::is('rw_fd') || Helpers::is('r_payments') || Helpers::is('rw_banks')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/site/finance-menu']) ?>"><i class="fa fa-dollar"></i>
                                        Finance and Payments </i></a>
                                </li>
                            <?php endif; ?>


                            <!--bank user-->
                            <?php if (Yii::$app->user->can('bank_reports')) : ?>
                                <li><a><i class="fa fa-bug"></i> Reconciliation <span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <?php if (Yii::$app->user->can('rw_banks')) : ?>
                                            <li><a href="<?= Url::to(['/banks/reconciliation']); ?>">Bank
                                                    Destination</a></li>
                                        <?php endif; ?>
                                        <li><a href="<?= Url::to(['/banks/reconciliation/chn-sch']); ?>">Channel -
                                                Sch</a></li>
                                        <li><a href="<?= Url::to(['/banks/reconciliation/sch-channel']); ?>">Sch -
                                                Channel</a></li>
                                    </ul>
                                </li>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can('bank_user')) : ?>


                                <li><a><i class="fa fa-bug"></i> Bank Reports <span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="<?= Url::to(['/banks/reports/all-trans']); ?>"> Realized
                                                Transactions</a></li>

                                    </ul>
                                </li>
                            <?php endif; ?>

                            <!--report center-->
                            <?php if (Yii::$app->user->can('r_report')) : ?>
                                <li><a><i class="fa fa-bar-chart-o"></i> Report center <span
                                                class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <?php if (\app\components\ToWords::isSchoolUser()) : ?>
                                            <li><a href="<?= Url::to(['/reports']) ?>">Channel graphs</a></li>
                                            <li><a href="<?= Url::to(['/reports/default/clas']); ?>">Class
                                                    graphs</a></li>
                                            <li><a href="<?= Url::to(['/reports/timetable/index']) ?>"> Timetable
                                                    Reports</a>
                                            </li>
                                        <?php endif; ?>

                                        <li><a href="<?= Url::to(['/reports/students/stu-info']) ?>">Students
                                                Report</a></li>

                                        <li>

                                            <a href="<?= Url::to(['/schoolcore/school-information/penalty-report']) ?>">
                                                Penalty Report</a>
                                        </li>

                                        <?php if (\Yii::$app->user->can('view_transaction_report') ): ?>

                                            <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                                            <li><a href="<?= Url::to(['/schoolcore/school-information/branch-performance']) ?>">
                                                    School Branch Report</a>
                                            </li>
                                                <li><a href="<?= Url::to(['/messages/outbox/index']) ?>">Message Outbox</a>
                                                </li>

                                            <li><a href="<?= Url::to(['/reports/transactions/schools-report-data']) ?>">
                                                    School Transaction Report</a>
                                            </li>

                                            <li><a href="<?= Url::to(['/schoolcore/school-information/schoolsstatus']) ?>">
                                                    School Status</a>
                                            </li>

                                            <li><a href="<?= Url::to(['/reports/transactions/schools-without-fees']) ?>">
                                                    Fee Not Assigned Schools</a>
                                            </li>
                                            <li><a href="<?= Url::to(['/reports/transactions/schools-with-fees']) ?>">
                                                    Fee Assigned Schools</a>
                                            </li>
                                            <li><a href="<?= Url::to(['/schoolcore/school-information/school-users']) ?>">
                                                    School Users</a>
                                            </li>
                                            <li><a href="<?= Url::to(['/schoolcore/school-information/student-users']) ?>">
                                                    Student Users</a>
                                            </li>
                                            <li><a href="<?= Url::to(['/reports/transactions/schools-without-student-data']) ?>">
                                                    Student Data Not Uploaded Schools</a>
                                            </li>
                                            <li><a href="<?= Url::to(['/reports/transactions/schools-with-student-data']) ?>">
                                                    Student Data Uploaded Schools</a>
                                            </li>
                                         <?php endif; ?>
                                            <li><a href="<?= Url::to(['/reports/transactions/trans']) ?>">Transaction
                                                    Report</a></li>
                                          <li><a href="<?= Url::to(['/reports/transactions/supplementary-trans']) ?>">Supplementary Transaction
                                                                                            Report</a></li>

                                            <li><a href="<?= Url::to(['/reports/transactions/channel']) ?>">Channel
                                                    Summary</a></li>
                                            <li><a href="<?= Url::to(['/reports/transactions/fees-performance']) ?>">Detailed
                                                    Fees Report</a></li>
                                            <li>
                                                <a href="<?= Url::to(['/reports/transactions/balance-adjustment-report']) ?>">Balance
                                                    Adjustment Report</a></li>



                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>


                        </ul>
                    </div>


                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout"
                       href="<?= Url::to(['/site/logout']); ?>" data-method="post">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu box-shadow">
                <div class="nav toggle">
                    <div class="pull-left">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                </div>
                <nav class="nav navbar-nav">

                    <ul class=" navbar-right">

                        <li class="nav-item dropdown open" style="padding-left: 15px;">
                            <a href="javascript:" class="user-profile dropdown-toggle" aria-haspopup="true"
                               id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">

                                <?= Html::img('@web/web/img/user.png', ['alt' => 'user']); ?>
                                <?= (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->fullname : Yii::$app->runAction('/site/logout'); ?>
                            </a>
                            <div class="dropdown-menu dropdown-usermenu pull-right"
                                 aria-labelledby="navbarDropdown">

                                <a class="dropdown-item" href="<?= Url::to(['/user/my-account-profile']); ?>">
                                    Profile</a>


                                <a class="dropdown-item" href="<?= Url::to(['/user/change']); ?>">Change
                                    password</a>


                                <a class="dropdown-item" href="<?= Url::to(['/site/logout']); ?>"
                                   data-method="post"><i
                                            class="fa fa-sign-out pull-right"></i> Log Out</a>
                            </div>
                        </li>
                        <li  id="back-arrow"  onclick="window.history.back()"><div><i class="nav-arrow  fa fa-arrow-circle-o-left"></i></div></li>
                        <li  id="next-arrow"  onclick="window.history.forward()"><div><i class="nav-arrow  fa fa-arrow-circle-o-right"></i></div></li>
                        <li  id="reload"  onclick="clink(location.href, reload)"><div><i class="nav-repeat  fa fa-repeat"></i></div></li>


                    </ul>

                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!--        <!-- page content -->
        <!--        --><? //= Alert::widget() ?>
        <!--        <div class="right_col" role="main" id="view_content">-->
        <!--            <div>-->
        <!--                <p style="color: #F1F4F6">SchoolSuite</p>-->
        <!--            </div>-->
        <!--            <div class="letter">-->
        <!--                --><? //= $content ?>
        <!---->
        <!--            </div>-->
        <!--        </div>-->
        <!--        -->
        <!--        -->

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- *************************************************************** -->
                <!-- Start First Cards -->
                <!-- *************************************************************** -->

                <!-- *************************************************************** -->
                <!-- End First Cards -->
                <!-- *************************************************************** -->
                <!-- *************************************************************** -->
                <!-- Start Sales Charts Section -->
                <!-- *************************************************************** -->
                <!-- Main content -->
                <div class="right_col" style="padding: 5px">
                    <?= Alert::widget() ?>
                    <div id="loader">
                    </div>

                    <div class="notify notify-success" id="workflowNotificationContainer" style="display: none">
                        <a href="javascript:" class='close-notify' data-flash='workflowInfo'>&times;</a>
                        <div class="notify-content" id="workflowNotificationContent">
                            ...
                        </div>
                    </div>

                    <div class="content">
                        <div class="letter" id="view_content">
                            <?= $content ?>
                        </div>
                    </div>

                </div>
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
<!--                <p class="text-center">Powered By Service Cops Limited --><?//= date('Y') ?><!--</p>-->

                <p class="text-center">&copy; Awash Bank <?= date('Y') ?></p>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>


</body>


<?php
$workflowPollUrl = Url::to(['/workflow/workflow/poll']);
$workflowItemsUrl = Url::to(['/workflow/workflow']);
$sessionTimeout = isset($params['sessionTimeout']) ? $params['sessionTimeout'] : 300;
$workflowInterval = ($sessionTimeout + 5) * 1000;
$main15 = <<< JS


var _workflowInterval;
var _pollInterval = "$workflowInterval";
    $(document).ready(function(){
        $('.close-notify').click(function(){
          $(".notify").hide();
          
        });
        
        let checkWorkflowItems = function() {
            let urlval = '$workflowPollUrl';
            $.get( urlval, function( data ) {
                    if(data && data.code === '0' && data.count) {
                        var msg;
                        if(data.count === 1) {
                            msg = 'You have <b>'+data.count+'</b> pending approval. Click here to view it';
                        }else {
                            msg = 'You have <b>'+data.count+'</b> pending approvals. Click here to view them';
                        }
                        $("#workflowNotificationContent").html('<div><a href="$workflowItemsUrl" style="color:white">'+msg+'</a></div>');
                        $("#workflowNotificationContainer").show();
                    }
                    else {
                        $("#workflowNotificationContainer").hide();
                    }
                    
                });
        }
        
        _workflowInterval = window.setTimeout(function(){
          checkWorkflowItems();
        }, 5000);
   });
JS;
$this->registerJs($main15);
?>

<?php $this->endBody() ?>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" role="dialog" data-backdrop="false" tabindex="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="col-10">
                        <h3 class="modal-title"></h3>
                    </div>
                </div>


            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="border:none;">
            </div>
        </div>

    </div>
</div>

</body>
</html>
<?php $this->endPage() ?>

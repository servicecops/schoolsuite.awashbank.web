<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\LoginAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

\app\assets\LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('successAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('viewError'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?= $content ?>
    </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

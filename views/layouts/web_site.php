<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */

\app\assets\SiteAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to('favicon.ico?v=2')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="School fees payments platform, Ethiopia. Pay fees conveniently over mobile money.">
    <meta name="keywords"
        content="Schoolsuite Ethopia, school pay Ethiopia, schoolpay, school fees, Ethiopia, best, school fees payments Ethiopia, centenary, fees, dfcu, mtn, airtel, payway, mobile money">
    <meta name="author" content="ServiceCops Uganda">
    <!--    <link rel="shortcut icon" href="/favicon.ico?v=1" type="image/x-icon" />-->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="site-page">
    <?php $this->beginBody() ?>




    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">

            <nav class="navbar navbar-expand-lg navbar-light shadow-sm bg-white fixed-top">
                <div class="container"> <a class="navbar-brand d-flex align-items-center" href="#">
                        <?= Html::img('@web/web/website/assets/img/new_logo_home.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '50px']); ?>
<!--                        <span class="ml-3 font-weight-bold " style="color:#1168af !important;">Schoolsuite<span>-->
                    </a> <button class="navbar-toggler navbar-toggler-right border-0" type="button"
                        data-toggle="collapse" data-target="#navbar4">
                        <span class="navbar-toggler-icon"></span>
                    </button>



                    <div class="collapse navbar-collapse" id="navbar4">
                        <ul class="navbar-nav ml-auto pl-lg-4">
                            <li class="nav-item"> <a class="nav-link " style="color:#fff"
                                    href="<?= Url::to(['/site/home-page']); ?>"> <span
                                        class="d-inline-block d-lg-none icon-width"><i
                                            class="fas fa-home"></i></span>Home</a> </li>

<!--                            <li class="nav-item "> <a id="find-student" class="nav-link" style="color:#1168af"-->
<!--                                    href="--><?//= Url::to(['/site/stusearch']); ?><!--"><span-->
<!--                                        class="d-inline-block d-lg-none icon-width"><i-->
<!--                                            class="far fa-user"></i></i></span>Find Student</a> </li>-->
                            <li class="nav-item "> <a id="student-login" class="nav-link" style="color:#fff"
                                    href="<?= Url::to(['/site/login']); ?>"><span
                                        class="d-inline-block d-lg-none icon-width"><i
                                            class="far fa-user"></i></i></span>Student Login</a> </li>
                            <li class="nav-item "> <a class="nav-link" style="color:#fff"
                                    href="<?= Url::to(['/site/login']); ?>"><span
                                        class="d-inline-block d-lg-none icon-width"><i
                                            class="far fa-user"></i></i></span>School Login</a> </li>
                            <li class="nav-item "> <a id="schools_and_channels" class="nav-link" style="color:#fff"
                                    href="<?= Url::to(['/site/about']); ?>"><span
                                        class="d-inline-block d-lg-none icon-width"><i
                                            class="far fa-user"></i></i></span>About Us</a> </li>
                            <li class="nav-item "> <a id="contact" class="nav-link"
                                                      style="font-weight: 400;

                    border-radius: 4px;
                    padding: 12px ;
                    color: #fff;
                    background: #F99134 !important;"
                                    href="<?= Url::to(['/site/contact']); ?>"><span
                                        class="d-inline-block d-lg-none icon-width"><i
                                            class="far fa-user"></i></i></span>Contact Us</a> </li>
                        </ul>

                    </div>
                    <div>
                        <span style="margin-right:7px"></span>
                        <a style="
                    font-weight: 400;
                    font-size: 13px;
                    letter-spacing: 2px;
                    display: inline-block;
                    border-radius: 4px;
                    padding: 12px ;
                    transition: ease-in-out 0.3s;
                    color: #fff;
                    background: #F99134;
                    text-transform: uppercase;" href="#contact" class="btn-get-started scrollto"><i
                                class="fa fa-phone"></i>8980</a>

                    </div>

                </div>
            </nav>



        </div>
    </header><!-- End Header -->


    <main id="main">

        <div id="content_view" style="background: white" data-web="<?= \Yii::getAlias('@web'); ?>">
            <?= $content ?>
        </div>
        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact section-bg" style="background-color: #283895d9">
            <div class="container">

                <div class="section-title">
                    <h2 class="text-white">Contact</h2>

                </div>

                <div class="row mt-5 justify-content-center">

                    <div class="col-lg-10">

                        <div class="info-wrap">
                            <div class="row">
                                <div class="col-lg-4 info">
                                    <i class="icofont-google-map"></i>
                                    <h4>Location:</h4>
                                    <p>Awash Towers, Right Side</p>
                                </div>

                                <div class="col-lg-4 info mt-4 mt-lg-0">
                                    <i class="icofont-envelope"></i>
                                    <h4>Email:</h4>
                                    <p>contactcenter@awashbank.com</p>
                                </div>

                                <div class="col-lg-4 info mt-4 mt-lg-0">
                                    <i class="icofont-phone"></i>
                                    <h4>Call:</h4>
                                    <p>0115-57-12-54</p>
                                    <p>0115-57-13-24</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>



            </div>
        </section><!-- End Contact Section -->
    </main>
    <!-- ======= Footer ======= -->
    <footer id="footer" style="color:#fff; background: rgb(39,56,145)">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Awash E-School</h4>
                        <p>
                            Awash Towers, Right Side<br>
                            <strong>Phone:</strong> 0115-57-13-24<br>
                            <strong>Email:</strong> contactcenter@awashbank.com<br>
                        </p>
                    </div>

                    <div class="col-lg-1 col-md-6 footer-links">

                    </div>

                    <div class="col-lg-2 col-md-6 footer-links">
                    </div>

                    <div class="col-lg-5 col-md-6 footer-links">
                        <h4>Our Social Networks</h4>
                        <div class="social-links mt-3">
                            <a href="https://twitter.com/bankawash" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="https://www.facebook.com/bankawash" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="https://www.instagram.com/awash_bank/" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="https://www.youtube.com/awashbank" class="google-plus"><i class="bx bxl-youtube"></i></a>
                            <a href="https://www.linkedin.com/company/awashban/" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                            <a href="https://t.me/awash_bank_official" class="-telegram"><i class="bx bxl-telegram"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container d-md-flex py-4">

            <div class="mr-md-auto text-center text-md-left">
                <div class="copyright">
                    Copyright © 2021 Property of Awash Bank. All rights reserved
                </div>
                <div class="credits">

                </div>
            </div>

        </div>
    </footer><!-- End Footer -->
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
    <div id="ad-features">
        <span class="ad-btn1">
            <i class="fa fa-times" aria-hidden="true"></i>
            <!--        <button aria-hidden="true"></button>-->
        </span>


    </div>

</footer><!-- End Footer -->
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <?php $this->endBody() ?>
</body>
<?php $this->endPage() ?>

<?php
$script = <<< JS
    $(document).ready(function(){

        $("#ad-features").fadeIn()
        .animate({bottom:0,right:0}, 1500, function() {
            //callback
        });      

        $(".ad-btn1").click(function(e){
            e.preventDefault();
            e.stopPropagation()
            $('#ad-features').fadeOut();
        });
      
    });
JS;
$this->registerJs($script);
?>

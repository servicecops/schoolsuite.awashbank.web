   <?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

   <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::to(['/site/index']); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?= Yii::$app->request->baseUrl ?>/web/img/icon.png" alt="icon" width="40" height="40" /></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img  src="<?= \Yii::getAlias('@web') ?>/web/img/PNG1.png" width="100" height="43"/></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <ul class="nav navbar-nav">
          <li id="title-header" class="title-head" ></li>
          </ul>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav pull-right">
          <li id="back-arrow"  onclick="window.history.back()"><i class="nav-arrow  fa fa-arrow-circle-o-left"></i></li>
          <li id="next-arrow"  onclick="window.history.forward()"><i class="nav-arrow  fa fa-arrow-circle-o-right"></i></li>
          <li id="reload"  onclick="clink(location.href, reload)"><i class="nav-repeat  fa fa-repeat"></i></li>

              <!-- User Account: style can be found in dropdown.less -->
              <?php if(!\Yii::$app->user->isGuest): ?>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?= Yii::$app->request->baseUrl ?>/web/img/user.png" class="user-image" alt="User" />
                  <span class="hidden-xs">
                    <?= Yii::$app->user->identity->username; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <?= Html::img('@web/web/img/user.png', ['class'=>'img-circle', 'alt' => 'User Image']) ?>
                    <p>
                     <?php echo (\app\components\ToWords::isSchoolUser()) ? 'School Admin' : 'System Admin';
                       echo ': '.Yii::$app->user->identity->fullname; ?>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-8 text-center">
                      <a href="<?= Url::to(['/user/change', 'id'=>Yii::$app->user->identity->id ]) ?>">Reset Password</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?= Url::to(['/user/view', 'id'=>Yii::$app->user->identity->id ]) ?>" class="btn btn-default">Profile</a>
                    </div>
                    <div class="pull-right">
                      <?= Html::a('Sign Out', Url::toRoute(['/site/logout']), ['data-method' => 'post', 'class'=>'btn btn-default'] ) ?>
                    </div>
                  </li>
                </ul>
              </li>
              <?php else: ?>
              <li>
                <a href="<?= Url::to(['/site/login']) ?>">Sign in </a>
              </li>
              <?php endif;?>
            </ul>
          </div>
        </nav>
      </header>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'New User';
?>

<div class="container card">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="card-body">
        <div class="row">

            <div class="col-sm-6" id="user_level">
                <?= $form->field($model, 'user_level', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                    [],
                    ['id' => 'user_level_id', 'prompt' => 'Type of User']
                )->label('Type Of User') ?>
            </div>
            <div class="col">
                <?= $form->field($model, 'firstname', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput(['autofocus' => true])->label('First Name') ?>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?= $form->field($model, 'lastname', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Last Name') ?>

            </div>
            <?php if ($model->isNewRecord) : ?>
                <div class="col">
                    <?= $form->field($model, 'username', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput(['autofocus' => true]) ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col">
                <?= $form->field($model, 'mobile_phone', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Mobile Number') ?>

            </div>
            <div class="col">
                <?= $form->field($model, 'email', ['labelOptions' => ['style' => 'color:#041f4a']]) ?>
            </div>
        </div>


        <div class="row">

            <?php if ($model->isNewRecord): ?>
                <div class="col-sm-6">
                    <?= $form->field($model, 'password', ['labelOptions' => ['style' => 'color:#041f4a']])->widget(PasswordInput::classname(), []); ?>

                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'password2', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Confirm Password']])->passwordInput()->label('Confirm Password') ?>

                </div>

            <?php endif; ?>

            <div class="col" id="school_div">


                <!--                    --><?php
                //                    $url = Url::to(['/schoolcore/core-school/sch-list']);
                //                    $cityDesc = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                //                                    $selectedSchool = $cityDesc;
                //                    echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                //                        'initValueText' => $selectedSchool, // set the initial display text
                //                        'theme'=>Select2::THEME_BOOTSTRAP,
                //                        'options' => [
                //                            'placeholder' => 'Filter School',
                //                            'id'=>'school_id',
                //                            'multiple'=>true,
                //                            'class'=>'form-control',
                //                        ],
                //                        'pluginOptions' => [
                //                            'allowClear' => true,
                //                            'minimumInputLength' => 3,
                //                            'language' => [
                //                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                //                            ],
                //                            'ajax' => [
                //                                'url' => $url,
                //                                'dataType' => 'json',
                //                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                //                            ],
                //                        ],
                //                    ])->label('school'); ?>
                <?= $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(CoreSchool::find()->all(), 'id', 'school_name'),
                    'options' => ['placeholder' => 'Find School', 'id' => 'business_id', 'multiple' => true],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Schools'); ?>

            </div>


            <div class="col" id="bank_div">
                <?= $form->field($model, 'bank_id', ['labelOptions' => ['style' => 'color:#041f4a']])->widget(Select2::class, [
                    'data' => ArrayHelper::map(CoreBankDetails::find()->all(), 'id', 'bank_name'),
                    'options' => ['placeholder' => 'Select bank ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false
                    ],
                ])->label('Bank'); ?>

            </div>
            <div class="col" id="branch_div">
                <?= $form->field($model, 'branch_id', ['labelOptions' => ['style' => 'color:#041f4a']])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\modules\schoolcore\models\AwashBranches::find()->all(), 'id', 'branch_name'),
                    'options' => ['placeholder' => 'Select bank ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false
                    ],
                ])->label('Bank Branch'); ?>

            </div>
            <div class="col" id="region_div">
                <?= $form->field($model, 'region_id', ['labelOptions' => ['style' => 'color:#041f4a']])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\modules\schoolcore\models\AwashBankRegion::find()->all(), 'id', 'bank_region_name'),
                    'options' => ['placeholder' => 'Select Region ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false
                    ],
                ])->label('Bank region'); ?>

            </div>

        </div>
        <div class="row">


            <?php if (!$model->isNewRecord && Yii::$app->user->can('schoolsuite_admin')): ?>
                <div class="col no-padding">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'locked', ['labelOptions' => ['style' => 'color:#041f4a']])->checkbox() ?>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            <?php endif; ?>

        </div>
        <hr>
        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create <i class="fa fa-plus"></i>' : 'Update <i class="fa fa-edit"></i>', ['class' => 'btn btn-primary']) ?>
                    <?php if ($model->isNewRecord): ?>
                        <?= Html::resetButton('Reset Form <i class="fa fa-undo"></i>', ['class' => 'btn btn-danger']) ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col">

            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$url = Url::to(['/user/level-options']);
$user_level = $model->user_level;
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#user").addClass('active');
    $("#user_create").addClass('active');
    $('#school_div').hide();
    $('#bank_div').hide();
    $('#region_div').hide();
    $('#branch_div').hide();
    $.get('$url', function( data ) {
      
        $('select#user_level_id').html(data);
        $('select#user_level_id').val('$user_level');
        
        
        if($('#user_level_id').val()){
            var profile =$('select#user_level_id option[value="' + $('#user_level_id').val() + '"]').attr("data-profile");
            if(profile == 'school_guy'){
                $('#school_div').show();
                   $('#region_div').hide();
                    $('#branch_div').hide();
            }
              if(profile == 'region_guy'){
                   $('#region_div').show();
                $('#school_div').hide();
                
                    $('#branch_div').hide();
            }
            // $('#region_guy').show();
            // $('#school_div').show();
        } 
      
        
        if($('#user_level_id').val() == 'bank_user' ){
            $('#bank_div').show();
               $('#region_div').hide();
                  $('#branch_div').hide();
        } 
        
        if($('#user_level_id').val() == 'branch_chekers' || $('#user_level_id').val() == 'branch_makers'){
            $('#branch_div').show();
               $('#school_div').hide();
            $('#bank_div').hide();
        }
       
        if($('#user_level_id').val() == 'branch_chekers' || $('#user_level_id').val() == 'branch_makers'){
            $('#branch_div').show();
               $('#school_div').hide();
            $('#bank_div').hide();
        }
      
        
        
    });
    
    $('#user_level_id').change(function(){
        var profile =$('select#user_level_id option[value="' + $(this).val() + '"]').attr("data-profile");
        if(profile == 'school_guy'){
            $('#bank_div').hide();
            $('#school_div').show();
            $('#branch_div').hide();
            $('#region_div').hide();
        } 
         else if(profile == 'region_guy'){
               $('#bank_div').hide();
            $('#school_div').hide();
            $('#branch_div').hide();
            $('#region_div').show();
        }
        else if($(this).val() == 'bank_user'){
            
            $('#bank_div').show();
            $('#school_div').hide();
            $('#branch_div').hide();
            $('#region_div').hide();
        }  
        else if($(this).val() == 'branch_chekers' || $(this).val() == 'branch_makers' ){
             $('#school_div').hide();
            $('#region_div').hide();
            $('#bank_div').hide();
            $('#branch_div').show();
        }
        else {
            $('#school_id').prop('selectedIndex', 0);
            $('#school_div').hide();
        }
    });
});
JS;
$this->registerJs($script);
?>

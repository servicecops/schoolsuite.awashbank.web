<?php

use app\models\ImageBank;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="letter">
    <div class="row hd-title" data-title="Upload Student Photo">
        <?php $form = ActiveForm::begin(['id'=>'student_image_upload', 'options'=>['enctype'=>'multipart/form-data']]); ?>
        <div class="row">
            <div class="col">
                <div id="upload-demo" style="padding-top:0;padding-left:0;"></div>
                <input type="hidden" id="imagebase64" name="ImageBank[image_base64]">
            </div>
            <div class="coltext-center" style="padding-top:10px;">
                <div>
                    <h3 style="padding:0px;line-height:10px;"><?= $model->fullname." (".$model->username.")"; ?><h3>
                            <hr class="l_header">
                            <div id="crop_result" style="padding:50px 100px;">
                                <?php if($model->profile_pic) : ?>
                                    <img class="img-thumbnail" src="data:image/jpeg;base64,<?= $model->image->image_base64 ?>" height="210" width="200" />'
                                <?php endif; ?>
                            </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="col-md-6">
                <label class="btn btn-primary btn-file">
                    Upload<input type="file" id="upload" style="display:none;" value="Choose a file"/>
                </label>
                <span class="btn btn-info upload-result">Crop</span>
                <?= Html::submitButton('Save Logo', ['class' => 'btn btn-info save_cropped_photo', 'style'=>'display:none;']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    });
                    $('.upload-demo').addClass('ready');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 192,
                height: 200,
            },
            boundary: {
                width: 400,
                height: 400
            }
        });
        $('.cr-viewport').html("<div class='text-center' style='color: rgba(23,67,88,0.5);padding:75px 10px;font-size:22px;'>Click Upload Button Below</div>");

        $('#upload').on('change', function () { $('.cr-viewport').html(''); readFile(this); });
        $('.upload-result').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport',
                format:'jpeg',
                quality:0.8,
            }).then(function (resp) {
                $('#imagebase64').val(resp);
                $('#crop_result').html('<img class="img-thumbnail" src="'+resp+'" height="210" width="200" />');
                $('.save_cropped_photo').css('display', 'inline');
                return false;
            });
        });

    });
</script>

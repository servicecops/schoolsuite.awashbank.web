<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BackOfficeUser */

$this->title = 'Add user';

?>
<div class="">

    <div class="">
        <h5 class="text-center" style="color: #041f4a"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h5><hr class="style14">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>

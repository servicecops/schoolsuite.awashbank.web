<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->firstname . ' ' . $model->lastname . ' - ' . $model->user_level;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class=" row">
<div class=" col-md-12">

    <div class="d-flex justify-content-between">
        <div class="display-4">
            <i class="fa fa-user"></i>&nbsp;&nbsp;
            <?= Html::encode($this->title) ?>
        </div>
    </div>
    <?php if (Yii::$app->session->hasFlash('success')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('error')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
            <div class="notify-content">
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        </div>
    <?php endif; ?>


</div>

<div class="col-md-12 ">

<p class="float-right" >
    <?php if (Yii::$app->user->can('view_by_branch')) : ?>
        <?= Html::a('<i class="fa fa-edit" ></i> <span >Update Branch</span>', ['update-branch', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary ']) ?>
    <?php endif; ?>
</p>


</div>


<div class="col-md-12">
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;">
            <b><?= $model->getAttributeLabel('username') ?></b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= $model->username ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>User
                Level</b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= $model->user_level ?></div>
    </div>


    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;">
            <b><?= $model->getAttributeLabel('firstname') ?></b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= $model->firstname ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;">
            <b><?= $model->getAttributeLabel('lastname') ?></b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= $model->lastname ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;">
            <b><?= $model->getAttributeLabel('middlename') ?></b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= $model->lastname ?></div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Branch</b>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->branch_id) ? \app\modules\schoolcore\models\AwashBranches::findOne(['id' => $model->branch_id])->branch_name : "--" ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Region
                Address</b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->region_id) ? \app\modules\schoolcore\models\AwashBankRegion::findOne(['id' => $model->region_id])->bank_region_name : "--" ?></div>
    </div>
    <?php if ($model->user_level == 'sch_admin') : ?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>School</b>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->school_id) ? $model->school->school_name : "--" ?></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>School
                    Address</b></div>
            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->school_id) ? $model->school->village : "--" ?></div>
        </div>

    <?php elseif ($model->user_level == 'bank_user') : ?>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Bank</b>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->bank_id) ? CoreBankDetails::findOne(['id' => $model->bank_id])->bank_name : "--" ?></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Bank
                    Address</b></div>
            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->bank_id) ? CoreBankDetails::findOne(['id' => $model->bank_id])->bank_address : "--" ?></div>
        </div>
    <?php endif; ?>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Joining
                Date</b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->created_at) ? date('M Y, H:i', strtotime($model->created_at)) : "--" ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Last
                Updated</b></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->updated_at) ? date('M Y, H:i', strtotime($model->updated_at)) : "--" ?></div>
    </div>

</div>
</div>


</div>

<?php
$script = <<< JS
  $(".active").removeClass('active');
    $("#my_account").addClass('active');
    $("#my_profile").addClass('active');

    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

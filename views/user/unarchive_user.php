<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'action' => ['unarchive-user', 'id'=>$model->id],
        'method' => 'post',
        'options'=>['class'=>'formprocess', 'id'=>'archive_user_form_modal']
    ]); ?>
<div class="row">
        <div class="col-sm-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Only archive if <b><?= $model->fullname ?></b> is no longer with the school. If archived, user won't be able to access the system.</div>

        <div class="col-sm-12">
        <?= $form->field($model2, 'reason', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Give Reason'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
            <?= Html::submitButton('Unarchive', ['class' => 'btn btn-block btn-info', 'data-confirm'=>'Are you sure you want archive this user']) ?>
        </div>
        <div class="col-sm-6">
            <?php if ($model->isNewRecord): ?>
                <?= Html::resetButton('Reset Form <i class="fa fa-undo"></i>', ['class' => 'btn btn-danger']) ?>
            <?php endif; ?>        </div>
   </div>

<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
        $('form#archive_user_form_modal').yiiActiveForm('validate');
          $("form#archive_user_form_modal").on("afterValidate", function (event, messages) {
              // if($(this).find('.has-error').length) {
              //           return false;
              //   } else {
              //       $('.modal').modal('hide');
              //   }
            });
   });
JS;
$this->registerJs($script);
?>

<?php

use app\models\ImageBank;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="card front">
  <div class="blue"></div>
  <div class="yellow"></div>
  <div class="pink"></div>
  <div class="dots"></div>
  <div class="personal-intro">
    <p>Krista Stone</p>
    <p>Photographer Maker Doer</p>
  </div>
</div>
<div class="card back">
  <div class="yellow"></div>
  <div class="top dots"></div>
  <div class="personal-info">
    <p>Krista Stone</p>
    <p>Photographer. Maker. Doer.</p>
    <p>123 Address St</p>
    <p>Sacramento, CA 14234</p>
    <p>567.890.1234</p>
    <p>www.kristastone.com</p>
    <p>@kristastone</p>
  </div>
  <div class="bottom dots"></div>
  <div class="pink"></div>
</div>
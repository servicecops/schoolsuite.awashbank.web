<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Edit: ' . $model->fullname;

?>
<div class="">

    <div class="bg-white">
        <h5 class="text-center" style="color: #000"><i class="fa fa-edit"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h5><hr class="style14">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>

<?php

use app\models\UserSearch;
use app\models\User;
use kartik\editable\Editable;
use kartik\export\ExportMenu;
use kartik\ipinfo\IpInfo;
use kartik\popover\PopoverX;
use yii\bootstrap4\LinkPager;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use yii\widgets\Pjax;

use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $model app\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Users';

?>



<!--new-->
    <div class="row">
        <div class="col-md-12">
        </div>

        <?php

        //Yii::trace($model);
        Yii::trace($dataProvider);
        $columns = [
            ['class' => 'kartik\grid\SerialColumn',],
            'username',
         'email',
            'user_level',
            [
                'attribute'=>'school_id',
                'value'=>'school.school_name',
            ],




            [
                'attribute'=>'Reset',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-unlock"></i>', ['user/reset', 'id'=>$model->id], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'View',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-eye"></i>', ['user/view', 'id'=>$model['id']]);
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'Update',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-pencil"></i>', ['user/update', 'id'=>$model['id']]);
                },
                'format'=>'html',
            ],

            [
                'attribute'=>'Archive user',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-lock"></i>', ['user/archive-user', 'id'=>$model['id']]);
                },
                'format'=>'html',
            ],


        ];
        ?>


        <div class="col-md-2">
            <span class="mb-3" style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span>


        </div>
        <div class="col-md-8">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <div class="col-md-2">

            <div>
                <?php if (Yii::$app->user->can('rw_user')) : ?>
                    <?= Html::a('Add User <i class=" fa fa-user-plus"></i>', ['create'], ['class' => 'btn btn-md btn-primary  float-right']) ?>

                <?php endif; ?>
            </div>
        </div>

        <div class="col-sm-12 col-md-12 no-padding" style="padding-top: 20px !important;">
            <div class="pull-right">


                <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                    <?php
                    echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                        'class' => 'btn btn-sm btn-danger',
                        'target' => '_blank',
                        'data-toggle' => 'tooltip',
                        'title' => 'Will open the generated PDF file in a new window'
                    ]);
                    echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                        'class' => 'btn btn-sm btn-success',
                        'target' => '_blank'
                    ]);
                    ?>
                </ul>

            </div>

        </div>



        <table class="table table-striped">
            <thead>
            <tr>

                <th class='clink'>Username</th>
                <th class='clink'>Email</th>
                <th class='clink'>User Level</th>
                <th class='clink'>School Name</th>
                <th class='clink'>Reset</th>
                <th class='clink'>View</th>
                <th class='clink'>Update</th>
                <th class='clink'>Archive User</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">
                        <td class="clink"><?= ($v['username']) ? $v['username']: '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['email']) ? $v['email'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['user_level']) ? $v['user_level'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>

                        <td>

                            <?= Html::a('<i class="fa fa-unlock"></i>', ['user/reset',  'id' => $v['id']]); ?>
                        </td>
                        <td>
                            <?= Html::a('<i class="fa  fa-eye"></i>', ['user/view', 'id' => $v['id']], ['class' => 'aclink']) ?>
                        </td>
                        <td>
                            <?= Html::a('<i class="fa  fa-pencil"></i>', ['user/update', 'id' => $v['id']], ['class' => 'aclink']) ?>
                        </td>
                        <td>
                            <?= Html::a('<i class="fa  fa-lock"></i>', ['user/archive-user', 'id' => $v['id']], ['class' => 'aclink']) ?>
                        </td>
                    </tr>
                <?php endforeach;
            else :?>
                <tr>
                    <td colspan="8">No student found</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

        <?= LinkPager::widget([
            'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ]); ?>


    </div>

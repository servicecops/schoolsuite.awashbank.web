<?php

use app\models\UserSearch;
use app\models\User;
use kartik\editable\Editable;
use kartik\export\ExportMenu;
use kartik\ipinfo\IpInfo;
use kartik\popover\PopoverX;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use yii\widgets\Pjax;

use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $model app\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Archived Users';

?>



<!--new-->
    <div class="row">
        <div class="col-md-12">
        </div>

        <?php


        $columns = [
            ['class' => 'kartik\grid\SerialColumn',],
            'username',
         'email',
            'user_level',
            [
                'attribute'=>'school_id',
                'value'=>'school.school_name',
            ],




            [
                'attribute'=>'Reset',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-unlock"></i>', ['user/reset', 'id'=>$model->id], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'View',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-eye"></i>', ['user/view', 'id'=>$model->id]);
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'Update',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-pencil"></i>', ['user/update', 'id'=>$model->id]);
                },
                'format'=>'html',
            ],

            [
                'attribute'=>'UnArchive user',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-unlock"></i>', ['user/unarchive-user', 'id'=>$model->id]);
                },
                'format'=>'html',
            ],


        ];
        ?>


        <div class="col-md-2">
            <span class="mb-3" style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span>


        </div>
        <div class="col-md-8">
        <?php echo $this->render('_unarchive_search', ['model' => $searchModel]); ?>
        </div>
        <div class="col-md-2">

            <div>

            </div>
        </div>



        <div class="table-responsive mt-3">
            <div class="float-right">

                <?php


                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'target' => '_blank',
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_EXCEL => false,

                    ],
                    'dropdownOptions' => [
                        'label' => 'Export users',
                        'class' => 'btn btn-outline-secondary'
                    ]
                ])
                ?>
            </div>
            <?= GridView::widget([
                'dataProvider'=> $dataProvider,
                //    'filterModel' => $searchModel,
                'columns' => $columns,
                'resizableColumns'=>true,
                'responsiveWrap' => false,
                //    'floatHeader'=>true,

                'responsive'=>false,
                'bordered' => false,
                'striped' => true,
            ]); ?>

        </div>


    </div>

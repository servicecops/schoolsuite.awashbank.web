<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'Update Branch';
?>
<div class="">
    <h5 class="text-center" style="color: #041f4a"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h5><hr class="style14">


<div class="container card">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="card-body">


        <p>Please add branch you belong to proceed</p>
            <div class="col" id="school_div">




            <div class="col" id="bank_div">
                <?= $form->field($model, 'branch_id', ['labelOptions' => ['style' => 'color:#041f4a']])->widget(Select2::class, [
                    'data' => ArrayHelper::map(\app\modules\schoolcore\models\AwashBranches::find()->all(), 'id', 'branch_name'),
                    'options' => ['placeholder' => 'Select branch ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false
                    ],
                ])->label('Select Branch'); ?>

            </div>

        </div>

        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <div class="form-group">
                    <?= Html::submitButton( 'Update <i class="fa fa-edit"></i>', ['class' => 'btn btn-primary']) ?>
                    <?php if ($model->isNewRecord): ?>
                        <?= Html::resetButton('Reset Form <i class="fa fa-undo"></i>', ['class' => 'btn btn-danger']) ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col">

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>

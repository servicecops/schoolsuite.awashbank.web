<?php

use app\models\AuthItem;
use app\models\UserSearch;
use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>



    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess']
    ]); ?>
<div class="row text-center">
    <div class="col-md-12">
        <div class="col-md-3 ">
            <?= $form->field($model, 'username', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Username'])->label(false) ?>

        </div>
        <div class="col-md-4">
            <?php
            $url = Url::to(['/schoolcore/core-school/sch-list']);
            $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'options' => [
                    'placeholder' => 'Filter School',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'user_level', ['inputOptions' => ['class' => 'form-control input-sm']])->dropDownList(
                ArrayHelper::map(AuthItem::getUserLevelsICanAdminister(), 'name', 'description'), ['id' => 'user_level_id', 'prompt' => 'Type of User']
            )->label(false) ?>
        </div>

        <div class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-md btn-primary']) ?></div>
    </div>

</div>

<?php ActiveForm::end(); ?>


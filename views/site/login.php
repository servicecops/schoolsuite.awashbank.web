<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */
/* @var $stdmodel app\modules\schoolcore\models\CoreStudent */


use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

use app\models\LoginForm;
use yii\web\View;
\app\assets\LoginAsset::register($this);
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="wrapper">

    <div class="inner-warpper text-center">
        <p class="login-box-msg">  <?= Html::img('@web/web/website/assets/img/login_logo.png', ['alt' => 'logo', 'class' => 'light-logo', ]); ?></p>

        <p style="min-height: 10px"></p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options'=>["autocomplete"=>"off"]
        ]); ?>


        <div class="social-auth-links text-center mt-2 mb-3">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="social-auth-links text-center mt-2 mb-3">
            <?= $form->field($model, 'password')->passwordInput()?>
        </div>
        <div class="social-auth-links text-center mt-2 mb-3">
            <?= $form->field($model, 'captcha', ['options'=>['id'=>'loginform-captcha-id'
            ]])->widget(Captcha::className(), ['options'=>['class'=>'form-control', 'placeholder'=>'Enter Above code']])->label(false) ?>
        </div>
        <!--            <button type="submit" id="login">Login</button>-->
        <?= Html::submitButton('Login', ['class' => 'btn buttonz_login w-100 mt-3']) ?>
        <div class="clearfix supporter">

            <span class="pull-right" style="color: #1068af">
                       <?= Html::a('I forgot my password ?', ['site/request-password-reset']) ?>
                </span>

        </div>

        <?php ActiveForm::end(); ?>

       
    </div>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>


    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="fade-up">
                <h2 class="text-danger">Payment code</h2>
                <p>search for student by entering the student payment code</p>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">

                </div>



                <div class="col-lg-5 col-md-12" data-aos="fade-up" data-aos-delay="300">
                    <?php $form = ActiveForm::begin([
                        'action' => ['my-trans'],
                        'method' => 'get',
                    ]); ?>
                    <div class="form-group">
                        <?= $form->field($searchModel, 'pc_rgnumber', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Payment code /Registration Number'] ])->textInput()->label(false); ?>
                        <div class="validate"></div>

                        <?php if($request) : ?>
                            <div>

                                <span style="color:red">No student with payment code: <b><?= $request ?></b> found</span><br><br>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>
            <div class="row col-md-offset-4">
                <div class="col-md-12">
                    <br>
                    <a href="<?= Url::to(['/site/stusearch']);?>" >Forgot your payment code </a>
                    <p><br></p>
                </div>
            </div>
        </div>

    </section><!-- End Contact Section -->
<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".bg-danger").removeClass('bg-danger');
    $("#student-login").addClass('bg-danger');
    $("#student-login").addClass('text-white');
  });
JS;
$this->registerJs($script);
?>
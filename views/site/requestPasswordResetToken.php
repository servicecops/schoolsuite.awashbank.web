<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row mt-5">

        <?php if (\Yii::$app->session->hasFlash('password')) : ?>
                <div class="notify notify-success">
                    <a href="javascript:;" class='close-notify' data-flash='password'>&times;</a>
                    <div class="notify-content">
                        <?= \Yii::$app->session->getFlash('password'); ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (\Yii::$app->session->hasFlash('error')) : ?>
                <div class="notify notify-danger">
                    <a href="javascript:;" class='close-notify' data-flash='error'>&times;</a>
                    <div class="notify-content">
                        <?= \Yii::$app->session->getFlash('error'); ?>
                    </div>
                </div>
            <?php endif; ?>




        <div class="card text-dark " style="margin-left: auto; margin-right: auto;">
            <div class="card-body">
                <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr>

                <p>Please fill out your username. <u><strong style="font-weight: bold; color:green">An email will be sent to you to complete the reset process.</strong></u></p>
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

</div>
<?php
$script = <<< JS
    $(document).ready(function(){
      
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

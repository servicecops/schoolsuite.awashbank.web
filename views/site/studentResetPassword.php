<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\StudentResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col">
    </div>
    <div class="col-6">
        <div class="card text-dark">
            <div class="card-body">
                <h3 class="text-center"><?= Html::encode($this->title) ?></h3><hr>
                <p>Please choose your new password:</p>
                <?php $form = ActiveForm::begin(['id' => 'student-reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col">

    </div>
</div>

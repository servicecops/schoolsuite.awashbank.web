<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row" style="padding-bottom: 20px; margin-bottom: 20px"></div>

<div class="site-contact">
    <div class="row" style="padding-bottom: 20px; margin-bottom: 20px"></div>
    <div class="blog contact">
        <div class="container">
            <div class="blog-post pull-left">
                <div class="blog-header">
                    <div class="ribbon-stitches-bottom"></div>

                    <div class="ribbon-stitches-bottom"></div>

                </div>
            </div><!-- End blog-post -->
        </div><!-- End container -->
    </div>

    <div class="comment" style="margin-bottom:0px;">
        <div class="container col-md-offset-2">
            <article class="post-1682 page type-page status-publish entry" aria-label="Company Profile" itemscope="" itemtype="https://schema.org/CreativeWork"><header class="entry-header"></br><h3 class="entry-title" itemprop="headline">About Us</h3></br>
                </header>
                <div class="entry-content" itemprop="text"><p style="text-align: justify;"><span style="color: #21205f;"><strong>Awash Bank</strong></span> was established by 486 founding shareholders with a paid-up capital of Birr 24.2 million and started banking operations on Feb. 13, 1995.The number of shareholders and its paid-up capital increased to over 5400 and Birr 10.3 billion, respectively. Our total assets reached Birr 173 billion with over 730 branches found across the country, Awash Bank continues to be leading private commercial Bank in Ethiopia.</p>
                    <p style="text-align: justify;">Having grown from humble beginnings with visionary intent, we are the first private bank in Ethiopia to exceed a billion profit mark in the history of Ethiopian Private Banks in the financial year 2019 since inception, we have expanded through a combination of substantial organic growth and an enhanced customer service experience.</p>
                    <p style="text-align: justify;">Our strategic goals and objectives are based on the aspiration of:-</p>
                    <p style="text-align: justify;"><strong>Vision</strong><strong>:</strong></p>
                    <p style="text-align: justify;">“To be the First Choice World Class Bank”</p>
                    <p style="text-align: justify;"><strong>Mission</strong><strong>:</strong></p>
                    <p style="text-align: justify;">“To provide Innovative, Competitive and Diversified banking services accessible to the society with qualified and committed staff in a profitable and socially responsible manner”</p>
                    <p style="text-align: justify;"><strong>Our Core Values</strong></p>
                    <p style="text-align: justify;"><strong>A</strong>&nbsp;&nbsp;&nbsp;&nbsp;–&nbsp;Accessibility</p>
                    <p style="text-align: justify;"><strong>W&nbsp;</strong>&nbsp;&nbsp;–&nbsp;Wisdom</p>
                    <p style="text-align: justify;"><strong>A&nbsp;</strong>&nbsp;&nbsp;&nbsp;–&nbsp;Accountability</p>
                    <p style="text-align: justify;"><strong>S&nbsp;</strong>&nbsp;&nbsp;&nbsp;–&nbsp;Socially Responsible</p>
                    <p style="text-align: justify;"><strong>H&nbsp;</strong>&nbsp;&nbsp;– Honesty</p>
                    <div width="742">
                        <p style="text-align: justify;"><strong>Quick Links</strong><strong>:</strong></p>

                        <a style="text-decoration: underline"  target="_blank" href="https://www.awashbank.com/company-profile/">  <p><strong>Find Out More About Awash Bank</strong></p></a>
                        <a style="text-decoration: underline" target="_blank"  href="https://www.awashbank.com/branch-address/">  <p><strong>Branch Address By Region</strong></p></a>
                    </div>
                    <p style="text-align: justify;">
                    </p></div></article>
        </div>
    </div>
</div>

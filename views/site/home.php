<?php

// @var $this yii\web\View /
use
    kartik\editable\Editable;
use miloschuman\highcharts\Highcharts;
use yii\bootstrap4\Progress;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

$this->title = 'Awash E-School';
?>

<body >

<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">


        <nav class="navbar navbar-expand-lg navbar-light shadow-sm bg-white fixed-top" style="border-bottom: 1px solid #6db3ff;">
            <div class="container"> <a class="navbar-brand d-flex align-items-center" href="<?= Url::to(['/site/home-page']); ?>">

                    <?= Html::img('@web/web/website/assets/img/new_logo_home.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '50px']); ?>
<!--                    <span class="ml-3 font-weight-bold " style="color:#1168af !important;">Schoolsuite<span>-->

                </a> <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar4">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbar4">
                    <ul class="navbar-nav ml-auto pl-lg-4">
                        <li class="nav-item" style="background-color: #F99134; color:white;border-radius: 7px;"> <a class="nav-link text-white" href="<?= Url::to(['/site/home-page']); ?>"> <span class="d-inline-block d-lg-none icon-width"><i class="fas fa-home"></i></span>Home</a> </li>
<!--                        <li class="nav-item "> <a class="nav-link" style="color:#1168af" href="--><?//= Url::to(['/site/stusearch']); ?><!--"><span class="d-inline-block d-lg-none icon-width"><i class="far fa-user"></i></i></span>Find Student</a> </li>-->
                        <li class="nav-item "> <a class="nav-link" style="color:#fff" href="<?= Url::to(['/site/login']); ?>"><span class="d-inline-block d-lg-none icon-width"><i class="far fa-user"></i></i></span>Student Login</a> </li>
                        <li class="nav-item "> <a class="nav-link" style="color:#fff" href="<?= Url::to(['/site/login']); ?>"><span class="d-inline-block d-lg-none icon-width"><i class="far fa-user"></i></i></span>School Login</a> </li>
                        <li class="nav-item "> <a class="nav-link" style="color:#fff" href="<?= Url::to(['/site/about']); ?>"><span class="d-inline-block d-lg-none icon-width"><i class="far fa-user"></i></i></span>About Us</a> </li>
                        <li class="nav-item "> <a class="nav-link"style="color:#fff" href="<?= Url::to(['/site/contact']); ?>"><span class="d-inline-block d-lg-none icon-width"><i class="far fa-user"></i></i></span>Contact Us</a> </li>
                    </ul>

                </div>
                <div>
                    <span style="margin-right:7px"></span>
                    <a style="
                    font-weight: 400;
                    font-size: 13px;
                    letter-spacing: 2px;
                    display: inline-block;
                    border-radius: 7px;
                    padding: 12px ;
                    transition: ease-in-out 0.3s;
                    color: #fff;
                    background: #F99134;
                    text-transform: uppercase;" href="#contact" class="btn-get-started scrollto"><i class="fa fa-phone"></i>8980</a>

                </div>

            </div>


        </nav>


    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
<!--    <div class="container text-right text-md-right" data-aos="fade-up">-->
<!--        <h1 style="color:#273891">Welcome to <span>Schoolsuite</span></h1>-->
<!--        <h2 class="text-primary">Automate all your school Operations</h2>-->
<!--        <a href="--><?//= Url::to(['/site/contact']); ?><!-- " class="btn-get-started scrollto">Contact Us</a>-->
<!--    </div>-->

</section><!-- End Hero -->

<main id="main">

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg" style="background-color: #fff">
        <div class="container">


            <div class="section-title">
                <h2>Features</h2>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="icon-box">
                        <i class="icofont-computer"></i>
                        <h4><a href="#">
                                E-library  Management </a></h4>
                        <p>Easily manage library items and track borrowing of both teachers and students.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-lg-0">
                    <div class="icon-box">
                        <i class="icofont-chart-bar-graph"></i>
                        <h4><a href="#">Real time reports</a></h4>
                        <p>

                            The schools can easily view all the necessary reports concerning fees, academics, attendance and many more with just a click of a button.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="icon-box">
                        <i class="icofont-image"></i>
                        <h4><a href="#"> E-learning and Grading</a></h4>
                        <p>

                            Effortlessly conduct and reset the online examination. Enter and update the marks, customized
                            report card and instantly share it with parents and students.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="icon-box">
                        <i class="icofont-settings"></i>
                        <h4><a href="#"> Paperless Admission</a></h4>
                        <p>

                            Verify original documents digitally and collect admission fee in a click. Automatic allotment of batches & subjects to students for every academic year.
                        </p>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="icon-box">
                        <i class="icofont-earth"></i>
                        <h4><a href="#"> Fees management</a></h4>
                        <p>

                            Automatic fees collection, calculate tax on the types of fee transactions. Imply fine on late fees, add
                            instant discounts, and monitor fee defaulters.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4">
                    <div class="icon-box">
                        <i class="icofont-tasks-alt"></i>
                        <h4><a href="#">Online classroom</a></h4>
                        <p>  With the future lifestyle becoming more online. Our aim is to prepare teachers, students, and the whole school to interact from anywhere at any time.</p>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Services Section -->



    <!-- ======= What We Do Section ======= -->
    <section id="what-we-do" class="what-we-do" style="background-color: #2c3c93">
        <div class="container">

            <div class="section-title">
                <h2 style="color: #fff">Payment Channels</h2>

            </div>

            <div class="row">
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <div class="icon-box">
                        <div class="icon">
                            <?= Html::img('@web/web/img/Branch.jpg', ['alt' => '', 'class' => 'img-fluid']); ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                    <div class="icon-box">
                        <div class="icon">
                            <?= Html::img('@web/web/img/USSD.jpg', ['alt' => '', 'class' => 'img-fluid']); ?>
                        </div>

                    </div>
                </div>

                <div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
                    <div class="icon-box">
                        <div class="icon">
                            <?= Html::img('@web/web/img/Awashbirr.jpg', ['alt' => '', 'class' => 'img-fluid']); ?>

                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
                    <div class="icon-box">
                        <div class="icon">
                            <?= Html::img('@web/web/img/Agent.jpg', ['alt' => '', 'class' => 'img-fluid']); ?>

                        </div>

                    </div>
                </div>

            </div>

        </div>
    </section><!-- End What We Do Section -->


    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
        <div class="container">


        </div>
    </section><!-- End Counts Section -->




    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg" >
        <div class="container">

            <div class="section-title">
                <h2>Contact</h2>

            </div>

            <div class="row mt-5 justify-content-center">

                <div class="col-lg-10">

                    <div class="info-wrap">
                        <div class="row">
                            <div class="col-lg-4 info">
                                <i class="icofont-google-map"></i>
                                <h4>Location:</h4>
                                <p> Awash Towers, Right Side</p>
                            </div>


                            <div class="col-lg-4 info mt-4 mt-lg-0">
                                <i class="icofont-envelope"></i>
                                <h4>Email:</h4>
                                <p>contactcenter@awashbank.com</p>
                            </div>

                            <div class="col-lg-4 info mt-4 mt-lg-0">
                                <i class="icofont-phone"></i>
                                <h4>Call:</h4>
                                <p>0115-57-13-24</p>
                                <p>0115-57-13-24</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>



        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->



<!-- ======= Footer ======= -->
<footer id="footer" style="color:#fff; background: rgb(39,56,145)" >
    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Awash E-school</h4>
                    <p>
                        Awash Towers, Right Side<br>
                        <strong>Phone:</strong> 0115-57-13-24<br>
                        <strong>Email:</strong> contactcenter@awashbank.com<br>
                    </p>
                </div>

                <div class="col-lg-1 col-md-6 footer-links">

                </div>

                <div class="col-lg-2 col-md-6 footer-links">

                </div>

                <div class="col-lg-5 col-md-6 footer-links">
                    <h4>Our Social Networks</h4>
                    <div class="social-links mt-3">
                        <a href="https://twitter.com/bankawash" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="https://www.facebook.com/bankawash" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="https://www.instagram.com/awash_bank/" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="https://www.youtube.com/awashbank" class="google-plus"><i class="bx bxl-youtube"></i></a>
                        <a href="https://www.linkedin.com/company/awashban/" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        <a href="https://t.me/awash_bank_official" class="-telegram"><i class="bx bxl-telegram"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div  class="container d-md-flex py-4">

        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                <?= Html::img('@web/web/img/awash_logo2.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '50px']); ?>

                Copyright © <?php date('Y')?> Property of Awash Bank. All rights reserved
            </div>
            <div class="credits">

            </div>
        </div>

    </div>
</footer><!-- End Footer -->

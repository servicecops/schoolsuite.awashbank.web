<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\web\JsExpression;

?>



    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="fade-up">
                <h2 class="text-danger">Find student</h2>

                <div class="alert alert-info">
                    You can use this section to retrieve your payment code in case you lost it
                </div>

            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">

                </div>



                <div class="col-lg-5 col-md-12" data-aos="fade-up">
                    <?php $form = ActiveForm::begin([
                        'id'=>'adv_search_form',
                        'action' => ['stusearch'],
                        'method' => 'get',
                    ]); ?>
                    <div class="form-group">
                        <?php if($request) : ?>
                            <?php if($search_error) : ?> <span style="color:red"><?= $search_error ?></span> <?php endif; ?>
                            <br>
                        <?php endif; ?>
                        <?= $this->render('/reusables/school_search',
                            ['form'=>$form,
                                'model'=>$searchModel,
                                'modelAttribute'=>'schsearch',
                                'size'=>Select2::LARGE,
                                'label'=>'School',
                                'showLogoAfterSelect'=>true,
                                'inputId'=>'schoolId',
                            ]
                        ); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($searchModel,
                            'class_id', ['inputOptions'=>['class'=>'form-control input-lg']])->dropDownList(['prompt'=>'Select Class'] )->label('Class'); ?>
                    </div>
                    <div class="form-group">
                        <div id="name_dob">
                            <?= $form->field($searchModel, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Student name(s)'] ])->textInput()->label('Student Name'); ?>
                        </div>

                    </div>

                    <a href="javascript:" style="display: none;" id="use_reg_number"><b>Use Registration Number</b> </a>
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <hr>
                    <a style="text-align:right" href="<?= Url::to(['/site/my-trans']); ?>"><b>Use payment code</b> </a>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>

        </div>
    </section>
<?php
$class_url = Url::to(['/schoolcore/core-school-class/lists']);
$class_val = $searchModel->class_id ? $searchModel->class_id : 0;
$script = <<< JS
$("document").ready(function(){ 
    $(".bg-danger").removeClass('bg-danger');
    $("#find-student").addClass('bg-danger');
    $("#find-student").addClass('text-white');
    if($('#schoolId').val() !==''){
        var school_id = $('#schoolId').val();
       $.get( '$class_url', {id : school_id}, function( data ) {
                $( 'select#corestudentsearch-class_id' ).html(data);
                if($class_val){
                    $( 'select#corestudentsearch-class_id' ).val($class_val);
                }
            }); 

    }
    $('#schoolId').change(function(){
        var school_id = $(this).val();
        $.get( '$class_url', {id : school_id}, function( data ) {
                $( 'select#corestudentsearch-class_id' ).html(data);
            });
    });

     $('#use_name_dob').click(function(){
        $('input#studentsearch-school_student_registration_number').val('');
        $(this).css('display', 'none');
        $("#use_reg_number").css('display', 'block');
        $("#reg_number").css('display', 'none');
        $("#name_dob").css('display', 'block');
    });

    $('#use_reg_number').click(function(){
        $('input#studentsearch-last_name').val('');
        $('input#studentsearch-date_of_birth').val('');
        $(this).css('display', 'none');
        $("#use_name_dob").css('display', 'block');
        $("#reg_number").css('display', 'block');
        $("#name_dob").css('display', 'none');
    });
   
  });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

<section id="contact" class="contact" >
    <div class="section-title" data-aos="fade-up">
        <h2 class="text-danger"><?= $record->fullname  ?></h2>
    </div>
<div class="container">    
<div style="text-align:center">  
 
  <h3><?= 'Payment code: '.$record->student_code ?></h3>
  <h3>Outstanding balance: <?= ($record->studentAccount->account_balance < 0) ? number_format(abs($record->studentAccount->account_balance), 2) : 0; ?></h3>
</div>
<br>
<hr style="border-top: dashed 1px;" /> 

  <div class="profile-data">

        <ul class="nav nav-tabs responsive" id = "profileTab">
            <li style ="padding:10px; border-right:1px solid #4287f5" class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-inr"></i> Personal</a></li>
             <li style ="padding:10px" id = "fees-tab"><a href="#fees" data-toggle="tab"><i class="fa fa-inr"></i> Payments</a></li>
        </ul>

         <div id='content' class="tab-content responsive">

            <div class="tab-pane active" id="personal">
                <?= $this->render('_tab_stu_info', [ 'model' => $record]) ?> 
            </div>
            
            <div class="tab-pane" id="fees">
                <?= $this->render('_tab_payments', ['model'=>$record]) ?>    
            </div>

        </div>
  </div>
  <p><br></p>


</div>
</section>
<?php 
$script = <<< JS
$("document").ready(function(){ 
    $(".bg-danger").removeClass('bg-danger');
    $("#student-login").addClass('bg-danger');
    $("#student-login").addClass('text-white');
  });
JS;
$this->registerJs($script);
?>

    
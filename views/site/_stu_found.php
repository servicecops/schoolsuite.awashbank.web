<?php

use app\components\ToWords;
use yii\helpers\Html;

?>

<section id="contact" class="contact" >
    <div class="section-title" data-aos="fade-up">
        <h2 class="text-danger">Results</h2>
    </div>
 <div class="container">

<table class="table table-striped">
    <tr>
        <th>Payment Code</th>
        <th>School Name</th>
        <th>Student Name</th>
        <th>Class</th>
        <th>Guardian Name</th>
        <th>Guardian Phone</th>
    </tr>
<?php foreach($record as $stu): ?>
<tr>
   <td><?= $stu->student_code ?></td>
   <td><?= $stu->schoolId->school_name ?></td>
    <td><?= Html::a($stu->fullname, ['site/stu', 'id'=>ToWords::encrypt($stu->id)]); ?></td>
    <td><?= $stu->studentClass->class_description ?></td>
    <td><?= $stu->guardian_name ? $stu->guardian_name : '-'  ?></td>
    <td><?= $stu->guardian_phone ? $stu->guardian_phone : '-' ?></td>
</tr>
<?php endforeach; ?>
</table>

</div>
</section>
<?php 
$script = <<< JS
$("document").ready(function(){ 
    $(".bg-danger").removeClass('bg-danger');
    $("#find-student").addClass('bg-danger');
    $("#find-student").addClass('text-white');
  });
JS;
$this->registerJs($script);
?>

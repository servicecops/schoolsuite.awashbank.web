<?php

// @var $this yii\web\View /
use kartik\editable\Editable;
use miloschuman\highcharts\Highcharts;
use yii\bootstrap4\Progress;
use yii\web\JsExpression;
use yii\web\View;

$this->title = 'Awash E-School';
?>
<div class="page_index">

    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card border-0 mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading"><span style="color: #293895">Active schools</span></div>
                        <div class="widget-subheading"><span style="color: #293895">Total Active Schools</span></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers "><span style="color: #293895"><?= number_format($schCount['active']) ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card border-0 mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading"><span style="color: #293895">Active students</span></div>
                        <div class="widget-subheading"><span style="color: #293895">Total active students</span></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span style="color: #293895"><?= number_format($stuCount['active']) ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card border-0 mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading"><span style="color: #293895">Archived/Inactive students</span></div>
                        <div class="widget-subheading"><span style="color: #293895">Total Archived/Inactive students</span></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span style="color: #293895"><?= number_format($stuCount['inactive']) ?></span></div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card border-0 mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading"><span style="color: #293895">Teachers</span></div>
                        <div class="widget-subheading"><span style="color: #293895">Total Teachers Enrolled</span></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span style="color: #293895"><?= number_format($trCount['active']) ?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-4">
            <div class="card border-0 mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading"><span style="color: #293895">Attendance</span></div>
                        <div class="widget-subheading"><span style="color: #293895">Total Schools Using attendance</span></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span style="color: #293895"><?= number_format($trCount['schools_attendance']) ?></span></div>
                    </div>
                </div>
            </div>
        </div>


    </div>


        <div class="card border-0">
            <div class="card-body">
                <?php
                ?>
                <div id="chart_count_content">
                    <?= Highcharts::widget([
                        'options' => [
                            'credits'=>[
                                'enabled'=>false
                            ],
                            'chart'=> [
                                'backgroundColor'=>'#fff',
                            ],
                            'colors'=>['#293895','#F78F33', '#FFE600', '#48CFF4', '#AB0E96', '#E8BB1A'],
                            'title' => ['text' => 'Monthly Channel Performance'],
                            'xAxis' => [
                                'categories' => $months
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Number of Transactions'],
                                'plotLines'=> [[
                                    'value'=> 0,
                                    'width'=> 1,
                                    'color'=> '#808080'
                                ]]
                            ],
                            'legend' => [
                                'layout'=> 'horizontal',
                                'align'=> 'center',
                                'verticalAlign'=> 'bottom',
                                'borderWidth'=> 0
                            ],
                            'plotOptions'=> [
                                'line'=> [
                                    'dataLabels'=> [
                                        'enabled'=> true
                                    ],
                                    'enableMouseTracking'=> true
                                ]
                            ],
                            'series' =>$transCount
                        ]
                    ]); ?>
                </div>
            </div>
        </div>


   <!-- <div class="main-content card">
        <div class="">
            <div class="card-body">
                <div id="chart_amount_content" >
                    <?/*= Highcharts::widget([
                        'scripts' => [
                            //'highcharts-more',
                            'modules/exporting',
                            // 'themes/gray'
                        ],
                        'options' => [
                            'exporting'=>[
                                'enabled'=>true
                            ],
                            'credits'=>[
                                'enabled'=>false
                            ],
                            'chart'=> [
                                'backgroundColor'=>'#fff',
                            ],
                            'colors'=>['#046DC9','#FE2626', '#FFE600', '#48CFF4', '#AB0E96', '#E8BB1A'],
                            'title' => ['text' => '<b>Monthly Channel Performance (Amount Birr)</b>'],
                            'xAxis' => [
                                'categories' => $months
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Total Monthly Amount'],
                                'plotLines'=> [
                                    [
                                        'value'=> 0,
                                        'width'=> 2,
                                        'color'=> '#808080'
                                    ]]
                            ],
                            'legend' => [
                                'layout'=> 'horizontal',
                                'align'=> 'center',
                                'verticalAlign'=> 'bottom',
                                'borderWidth'=> 0
                            ],
                            'plotOptions'=> [
                                'line'=> [
                                    'dataLabels'=> [
                                        'enabled'=> true
                                    ],
                                    'enableMouseTracking'=> true
                                ]
                            ],
                            'series' => $monTrans
                        ]
                    ]); */?>
                </div>
            </div>
        </div>
    </div>-->
<!--piecharts-->
    <div class="row mb-5 mt-5">
        <div class="col">
            <?php
            echo Highcharts::widget([
                'scripts' => [
                    'highcharts-3d',
                    //'themes/grid-light',
                ],
                'options' => [
                    'exporting'=>[
                        'enabled'=>false
                    ],
                    'legend'=>[
                        'align'=>'left',
                        'verticalAlign'=>'bottom',
                        'layout'=>'horizontal',
                    ],
                    'credits'=>[
                        'enabled'=>false
                    ],
                    'colors'=>["#293895", '#F78F33', "#F17CB0", '#2B908F', '#808080', '#7999d5', '#ffff36', '#F45B5B'],
                    'chart'=> [
                        'backgroundColor'=>'#fff',
                        'type'=>'pie',
                        'options3d'=>[
                            'enabled'=>true,

                            'alpha'=>45,
                            'beta'=>7
                        ]
                    ],
                    'title'=>[
                        'text'=>'<b class="mt-3">Total Schools ('.$ttle["sch"].')</b>',
                        'margin'=>0,
                    ],
                    'plotOptions'=>[
                        'pie'=>[
                            'allowPointSelect'=> true,
                            'depth'=>20,
                            'innerSize'=>40,
                            'dataLabels'=>[
                                'enabled'=>false
                            ],
                            'showInLegend'=>true,
                        ],

                    ],
                    'series'=> [
                        [
                            'name'=>'Schools',
                            'data' => $schTypes,
                        ],
                    ]
                ],
            ]);
            ?>
        </div>
        <div class="col">
            <?php
            echo Highcharts::widget([
                'scripts' => [
                    'highcharts-3d',
                    //'themes/grid-light',
                ],
                'options' => [
                    'exporting'=>[
                        'enabled'=>false
                    ],
                    'legend'=>[
                        'align'=>'left',
                        'verticalAlign'=>'bottom',
                        'layout'=>'horizontal',
                    ],
                    'credits'=>[
                        'enabled'=>false
                    ],
                    'colors'=>["#293895", '#F78F33', '#F45B5B',  "#43C6DB", '#2B908F', '#808080', '#7999d5', '#ffff36'],
                    'chart'=> [
                        'backgroundColor'=>'#fff',
                        'type'=>'pie',
                        'options3d'=>[
                            'enabled'=>true,

                            'alpha'=>45,
                            'beta'=>7
                        ]
                    ],
                    'title'=>[
                        'text'=>'<b class="mt-3">Total Users ('.$ttle["usr"].')</b>',
                        'margin'=>0,
                    ],
                    'plotOptions'=>[
                        'pie'=>[
                            'allowPointSelect'=> true,
                            'depth'=>20,
                            'innerSize'=>40,
                            'dataLabels'=>[
                                'enabled'=>false
                            ],
                            'showInLegend'=>true,
                            // 'startAngle'=> -90,
                            // 'endAngle'=> 90,
                        ],

                    ],
                    'series' => [
                        [
                            'name'=>'Users',
                            'data'=>$usrTypes,
                        ],
                    ]
                ],
            ]);
            ?>
        </div>
    </div>
</div>
<?php
$script = <<< JS

$("document").ready(function(){ 
	$('#chart_amount_content').css('display', 'none');
    $("#chart_count_trans").click(function(){
    	$('#chart_amount_trans').css('background-color', '');
    	$('#chart_count_trans').css('background-color', '#fbfbfb');
    	$('#chart_amount_content').css('display', 'none');
		$('#chart_count_content').css('display', 'block');
    	
    });

    $("#chart_amount_trans").click(function(){
    	$('#chart_count_trans').css('background-color', '');
    	$('#chart_amount_trans').css('background-color', '#fbfbfb');
    	$('#chart_count_content').css('display', 'none');
    	$('#chart_amount_content').css('display', 'block');

    });
  });
JS;
$this->registerJs($script);
?>

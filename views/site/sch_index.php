<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;
use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

$this->title = 'Home';
if(!isset($searchModel)) $searchModel = new DynamicModel(['start_date', 'end_date']);

?>


<!--cards-->
<div class="row">
    <?php $form = ActiveForm::begin([
        'action' => ['/index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess']
    ]); ?>

    <?php ActiveForm::end(); ?>


</div>

<div class="row">
    <div class="col-md-4">
        <a class="datcard my-3" href="">
            <span style="color:#293895;" class="h4">Total active students</span>
            <p class="text-center">
                <span style="color:#293895;" class="h4"><?= number_format($stuCount['active']) ?></span>
            </p>

            <div class="go-corner">
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a class="datcard my-3" href="">
            <span style="color:#293895;" class="h4">Archived /Inactive Students</span>
            <p class="text-center">
                <span style="color:#293895;" class="h4"><?= number_format($stuCount['inactive']) ?></span>
            </p>

            <div class="go-corner">
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a class="datcard my-3" href="">
            <span style="color:#293895;" class="h4">Total Teachers enrolled</span>
            <p class="text-center">
                <span style="color:#293895;" class="h4 text-center"><?= number_format($trCount['active']) ?></span>
            </p>

            <div class="go-corner">
            </div>
        </a>
    </div>
</div>



<!--four-->
        <div class="row mt-5 letter">
            <div class="col">

                    <div class="link-box2 bg-teal">
                        <span class=""><i class="fa  fa-arrow-circle-up"></i></span>
                        <span style="font-size:20px;"> Highest Paying Class </span>
                    </div>
                    <table class='table table-striped col-md-6'>
                        <thead>
                        <tr>
                            <th> Time</th>
                            <th> Class</th>
                            <th> Count</th>
                            <th> Sum</th>
                        </tr>
                        </thead>
                        <tr>
                            <td> Today</td>
                            <td><?= ($class_rank['Today']['h']['sum'] != 0) ? $class_rank['Today']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Today']['h']) ? number_format($class_rank['Today']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Today']['h']) ? number_format($class_rank['Today']['h']['sum'], 2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Week</td>
                            <td><?= ($class_rank['Week']['h']['sum'] != 0) ? $class_rank['Week']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Week']['h']) ? number_format($class_rank['Week']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Week']['h']) ? number_format($class_rank['Week']['h']['sum'], 2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Month</td>
                            <td><?= ($class_rank['Month']['h']['sum'] != 0) ? $class_rank['Month']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Month']['h']) ? number_format($class_rank['Month']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Month']['h']) ? number_format($class_rank['Month']['h']['sum'],2 ) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Older</td>
                            <td><?= ($class_rank['Older']['h']['sum'] != 0) ? $class_rank['Older']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Older']['h']) ? number_format($class_rank['Older']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Older']['h']) ? number_format($class_rank['Older']['h']['sum'], 2) : '0' ?> </td>
                        </tr>
                    </table>

            </div>
            <div class="col">

                <div class="link-box2 bg-teal">
                    <span class=""><i class="fa  fa-arrow-circle-up"></i></span>
                    <span style="font-size:20px;"> Highest Collection Channel  </span>
                </div>
                    <table class='table table-striped col-md-6'>
                        <thead>
                        <tr>
                            <th> Time</th>
                            <th> Channel</th>
                            <th> Count</th>
                            <th> Sum</th>
                        </tr>
                        </thead>
                        <tr>
                            <td> Today</td>
                            <td><?= ($channel_rank['Today']['h']['sum'] != 0) ? $channel_rank['Today']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Today']['h']) ? number_format($channel_rank['Today']['h']['count'] ) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Today']['h']) ? number_format($channel_rank['Today']['h']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Week</td>
                            <td><?= ($channel_rank['Week']['h']['sum'] != 0) ? $channel_rank['Week']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Week']['h']) ? number_format($channel_rank['Week']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Week']['h']) ? number_format($channel_rank['Week']['h']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Month</td>
                            <td><?= ($channel_rank['Month']['h']['sum'] != 0) ? $channel_rank['Month']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Month']['h']) ? number_format($channel_rank['Month']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Month']['h']) ? number_format($channel_rank['Month']['h']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Older</td>
                            <td><?= ($channel_rank['Older']['h']['sum'] != 0) ? $channel_rank['Older']['h']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Older']['h']) ? number_format($channel_rank['Older']['h']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Older']['h']) ? number_format($channel_rank['Older']['h']['sum'],2) : '0' ?> </td>
                        </tr>
                    </table>

            </div>
            <div class="col">

                <div class="link-box2 bg-teal">
                    <span class=""><i class="fa  fa-arrow-circle-down"></i></span>
                    <span style="font-size:20px;"> Lowest Paying Class  </span>
                </div>
                    <table class='table table-striped col-md-6'>
                        <thead>
                        <tr>
                            <th> Time</th>
                            <th> Class</th>
                            <th> Count</th>
                            <th> Sum</th>
                        </tr>
                        </thead>
                        <tr>
                            <td> Today</td>
                            <td><?= ($class_rank['Today']['l']['sum'] != 0) ? $class_rank['Today']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Today']['l']) ? number_format($class_rank['Today']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Today']['l']) ? number_format($class_rank['Today']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Week</td>
                            <td><?= ($class_rank['Week']['l']['sum'] != 0) ? $class_rank['Week']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Week']['l']) ? number_format($class_rank['Week']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Week']['l']) ? number_format($class_rank['Week']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Month</td>
                            <td><?= ($class_rank['Month']['l']['sum'] != 0) ? $class_rank['Month']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Month']['l']) ? number_format($class_rank['Month']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Month']['l']) ? number_format($class_rank['Month']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Older</td>
                            <td><?= ($class_rank['Older']['l']['sum'] != 0) ? $class_rank['Older']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Older']['l']) ? number_format($class_rank['Older']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($class_rank['Older']['l']) ? number_format($class_rank['Older']['l']['sum'],2) : '0' ?> </td>
                        </tr>

                    </table>


            </div>
            <div class="col">

                <div class="link-box2 bg-teal">
                    <span class=""><i class="fa  fa-arrow-circle-down"></i></span>
                    <span style="font-size:20px;"> Lowest Collection Channel  </span>
                </div>
                    <table class='table table-striped col-md-6'>
                        <thead>
                        <tr>
                            <th> Time</th>
                            <th> Channel</th>
                            <th> Count</th>
                            <th> Sum</th>
                        </tr>
                        </thead>
                        <tr>
                            <td> Today</td>
                            <td><?= ($channel_rank['Today']['l']['sum'] != 0) ? $channel_rank['Today']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Today']['l']) ? number_format($channel_rank['Today']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Today']['l']) ? number_format($channel_rank['Today']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Week</td>
                            <td><?= ($channel_rank['Week']['l']['sum'] != 0) ? $channel_rank['Week']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Week']['l']) ? number_format($channel_rank['Week']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Week']['l']) ? number_format($channel_rank['Week']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Month</td>
                            <td><?= ($channel_rank['Month']['l']['sum'] != 0) ? $channel_rank['Month']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Month']['l']) ? number_format($channel_rank['Month']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Month']['l']) ? number_format($channel_rank['Month']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                        <tr>
                            <td> Older</td>
                            <td><?= ($channel_rank['Older']['l']['sum'] != 0) ? $channel_rank['Older']['l']['cdes'] : '--' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Older']['l']) ? number_format($channel_rank['Older']['l']['count']) : '0' ?> </td>
                            <td style="text-align: right;"><?= ($channel_rank['Older']['l']) ? number_format($channel_rank['Older']['l']['sum'],2) : '0' ?> </td>
                        </tr>
                    </table>
                </div>
        </div>

<!--    charts-->

    <div class="row mb-5 mt-5">
        <div class="col">
                <div>
                    <?php
                    echo Highcharts::widget([
                        'scripts' => [
                            'highcharts-3d',
                            //'themes/grid-light',
                        ],
                        'options' => [
                            'exporting' => [
                                'enabled' => false
                            ],
                            'legend' => [
                                'align' => 'left',
                                'verticalAlign' => 'bottom',
                                'layout' => 'horizontal',
                            ],
                            'credits' => [
                                'enabled' => false
                            ],
                            'colors' => ["#233184", "#F78F33"],
                           // 'colors' => ["#43C6DB", "#F17CB0"],
                            'chart' => [
//                                'backgroundColor' => '#E3EEF1',
                                'type' => 'pie',
                                'options3d' => [
                                    'enabled' => true,

                                    'alpha' => 45,
                                    'beta' => 7
                                ]
                            ],
                            'title' => [
                                'text' => 'Total Student by Gender',
                                'margin' => 0,
                            ],
                            'plotOptions' => [
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'depth' => 20,
                                    'innerSize' => 40,
                                    'dataLabels' => [
                                        'enabled' => false
                                    ],
                                    'showInLegend' => true,
                                ],

                            ],
                            'series' => [
                                [
                                    'name' => 'Total Student',
                                    'data' => $gender,
                                ],
                            ]
                        ],
                    ]);
                    ?>
                </div>
        </div>

        <div class="col">
                <div>
                    <?php
                    echo Highcharts::widget([
                        'scripts' => [
                            'highcharts-3d',
                            //'themes/grid-light',
                        ],
                        'options' => [
                            'exporting' => [
                                'enabled' => false
                            ],
                            'legend' => [
                                'align' => 'left',
                                'verticalAlign' => 'bottom',
                                'layout' => 'horizontal',
                            ],
                            'credits' => [
                                'enabled' => false
                            ],
                          //  'colors' => ["#43C6DB", "#F17CB0"],
                            'colors' => ["#233184", "#F78F33"],
                            'chart' => [
//                                'backgroundColor' => '#E3EEF1',
                                'type' => 'pie',
                                'options3d' => [
                                    'enabled' => true,

                                    'alpha' => 45,
                                    'beta' => 7
                                ]
                            ],
                            'title' => [
                                'text' => 'Total Student by Class',
                                'margin' => 0,
                            ],
                            'plotOptions' => [
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'depth' => 20,
                                    'innerSize' => 40,
                                    'dataLabels' => [
                                        'enabled' => false
                                    ],
                                    'showInLegend' => true,
                                ],

                            ],
                            'series' => [
                                [
                                    'name' => 'Total Student',
                                    'data' => $classes,
                                ],
                            ]
                        ],
                    ]);
                    ?>
                </div>
        </div>
        <div class="col-6">
                <div>

                    <?php
                    echo Highcharts::widget([
                        'scripts' => [
                            'highcharts-3d',
                            //'themes/grid-light',
                        ],
                        'options' => [
                            'displayErrors'=>true,
                            'exporting' => [
                                'enabled' => false
                            ],
                            'legend' => [
                                'align' => 'left',
                                'verticalAlign' => 'bottom',
                                'layout' => 'horizontal',
                            ],
                            'credits' => [
                                'enabled' => false
                            ],
                          //  'colors' => ["#43C6DB", "#F17CB0"],
                            'colors' => ["#233184", "#F78F33"],
                            'chart' => [
//                                'backgroundColor' => '#E3EEF1',
                                'type' => 'pie',
                                'options3d' => [
                                    'enabled' => true,

                                    'alpha' => 45,
                                    'beta' => 7
                                ]
                            ],
                            'title' => [
                                'text' => 'Amount Paid / UnPaid',
                                'margin' => 0,
                            ],
                            'plotOptions' => [
                                'pie' => [
                                    'allowPointSelect' => true,
                                    'depth' => 20,
                                    'innerSize' => 40,
                                    'dataLabels' => [
                                        'enabled' => false
                                    ],
                                    'showInLegend' => true,
                                    // 'startAngle'=> -90,
                                    // 'endAngle'=> 90,
                                ],

                            ],
                            'series' => [
                                [
                                    'name' => 'Paid /UnPaid',
                                    'data' => $paidUnpaid,
                                ],
                            ]
                        ],
                    ]);
                    ?>
                </div>
        </div>
    </div>
    <?php
    $script = <<< JS
    $('ul.treeview-menu.menu-open').slideUp().removeClass('menu-open');
    $(".active").removeClass('active');
    $("#home").addClass('active');
JS;
    $this->registerJs($script);
    ?>

<div id="ad-features">
    <span class="ad-btn1">
        <i class="fa fa-times" aria-hidden="true"></i>
        <!--        <button aria-hidden="true"></button>-->
    </span>

    <!--REPLACE THIS IMAGE LINK-->
    <a href="#" target="_blank">
        <?= $this->render('all_school_notices.php') ?>

    </a>
</div>
<?php
$script = <<< JS
    $(document).ready(function(){

        $("#ad-features").fadeIn()
        .animate({bottom:0,right:0}, 1500, function() {
            //callback
        });      

        $(".ad-btn1").click(function(e){
            e.preventDefault();
            e.stopPropagation()
            $('#ad-features').fadeOut();
        });
      
    });
JS;
$this->registerJs($script);
?>

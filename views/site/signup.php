<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\SignupForm */

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\View;
use app\models\AuthItem;
$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="row">

        <div class="col-sm-6" id="user_level">
            <?= $form->field($model, 'user_level')->dropDownList(
               \yii\helpers\ArrayHelper::map(AuthItem::find()->where(['type' => 1])->all(),'name','name'),['prompt'=>'select user level']
            ) ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'firstname')->textInput(['autofocus' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'lastname', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Last Name'] ])->textInput()->label('Last Name') ?>

        </div>
        <div class="col">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'mobile_phone', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Mobile Number'] ])->textInput()->label('Mobile Number') ?>

        </div>
        <div class="col">
            <?= $form->field($model, 'email') ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'password')->passwordInput() ?>

        </div>
        <div class="col">

        </div>

    </div>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <div class="form-group">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>        </div>
        <div class="col">

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

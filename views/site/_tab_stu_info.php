<?php
use yii\helpers\Html;
?>




<div class="row">

    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->getAttributeLabel('fullname') ?></div>
                <div class="col-md-6 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><?= ($model->fullname) ? $model->fullname : "--" ?></div>

        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->getAttributeLabel('first_name') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><?= ($model->first_name) ? $model->first_name : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->getAttributeLabel('last_name') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><?= ($model->last_name) ? $model->last_name : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->getAttributeLabel('middle_name') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><b><?= ($model->middle_name) ? $model->middle_name : "--" ?></b></div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->getAttributeLabel('student_code') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><b><?= ($model->student_code) ? $model->student_code : "--" ?></b></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;">Reg Number</div>
            <div class="col-lg-6 col-xs-9 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><b><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?></b></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;">School</div>
            <div class="col-lg-6 col-xs-9 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->studentClass->institution0->school_name ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;padding: 12px;border-bottom: 1px solid #ddd;"><?= $model->getAttributeLabel('class_id') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text" style="padding: 12px;border-bottom: 1px solid #ddd;"><b><?= ($model->class_id) ? $model->studentClass->class_code : "--" ?></b></div>
        </div>
    </div>
</div>

























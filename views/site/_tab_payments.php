<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>

<div class="row">
    <div class="col-md-12">
    <div class="box">
    <div class="box-header with-border">
            <h3 class="box-title">Account Transaction History </h3>
            
        </div>
  <?php // Pjax::begin(); ?>
   <?= GridView::widget([
        'dataProvider' => $model->accountHistories,
        //'filterModel' => $searchModel,

        'layout' =>"{items}\n{pager}",
        'columns' => [
            [
              'attribute'=>'date_created',
              'value'=>function($model){
                return date('d/m/y H:i', strtotime($model->date_created));
              }

            ],
            [
              'attribute'=>'Channel',
              'value'=>function($model){
                return '<img src="'.Url::to(['/import/import/image-link2', 'id'=>$model->payment->paymentChannel->payment_channel_logo]).'" width="25px" /> &nbsp;'.$model->payment->paymentChannel->channel_name;
              },
              'format'=>'html',
            ],
            [
              'attribute'=>'description',
              'value'=>function($model){
                return $model->description;
              },
              'format'=>'html',

            ],
            [
              'attribute'=>'amount',
              'value'=>function($model){
                if($model->amount < 0)
                    return '<span style="color:red">'.number_format($model->amount, 2).'</span>';
                else
                    return number_format($model->amount, 2);
              },
              'format'=>'html',
            ],
            [
              'attribute'=>'print',
              'value'=>function($model){
                return $model->payment_id ? Html::a('<i class="glyphicon glyphicon-print"></i> Receipt', Url::to(['/r', 'i'=>base64_encode($model->payment_id)]), ['target'=>'_blank']) : '';
              },
              'format'=>'raw',
            ]
           

        ],
    ]); ?>
  <?php //Pjax::end(); ?>

</div>
</div>
</div>
<?php
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\SchoolInformation;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
?>
<?php

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup ='<tr>';
    if(repo.img){
    	markup += '<td>' + '<img src="data:image/jpeg;base64, ' + repo.img + '" class="img-rounded" style="width:30px" /></td>' ;
    } else {
    	markup +='<td style="width:30px">&nbsp;</td>';
    }
    markup +='<td style="font-size:12px; padding-left:5px;"> '+ repo.text + '</td>' + '</tr>';

    return '<table style="overflow:hidden;">' + markup + '</table>';
};

JS;
 
// Register the formatting script
$this->registerJs($formatJs, \yii\web\View::POS_HEAD);
?>

<section id="contact" class="contact" >

<div class="container">

<div class="row">
<div class="col-sm-6 no-padding divider" >
    <div class="section-title mt-5" data-aos="fade-up">
        <h2 class="text-danger">Enrolled Schools</h2>
    </div>
<div id="school-loader">
 </div>
<br>

<?php $form = ActiveForm::begin(); ?>
<div class="col-xs-12 no-padding">
<div class="col-sm-6 col-xs-9 no-padding">
 <?php      
            $url = Url::to(['/schoolcore/core-school/template-list']);
            $selectedSchool = empty($model->school) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size'=>'sm',
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => ' Search School', 
                    'id'=>'site_school_search'
                ],
                'pluginOptions' => [
                    'allowClear' => false,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        			'templateResult' => new JsExpression('formatRepo'),
                ],
            ])->label(''); ?>
            </div>
            </div>
 <?php $form = ActiveForm::end(); ?>
 <div id='sch_container' class="col-xs-12 no-padding" style="margin-bottom:10px;display:none;">
 </div>
 <div id="tb_site_schools_channels">
<?php 
$pg = $pages->page;
$snr = ($pg * 20) + 1;
$dgts = 0;
if(in_array($pg, range(0,3))){
	$dgts = 2;
}elseif(in_array($pg, range(4,33))){
	$dgts= 3;
}elseif(in_array($pg, range(34,333))){
	$dgts = 4;
}
foreach($schools as $k=>$v){
	echo str_pad($snr, $dgts, '0', STR_PAD_LEFT).".  ".$v['school_name']."<br>";
	$snr++;
	} ?>
	<?php
		echo LinkPager::widget([
		    'pagination' => $pages,
		    'options'=>['class'=>'pagination', 
		                'data-for'=>'tb_site_schools_channels',
		                'id'=>'pg_tb_site_schools_channels']
	]);?>
</div>
</div>
<div class="col-sm-6 no-padding-right" style="padding-right: 0;" >
    <div class="section-title mt-5" data-aos="fade-up">
        <h2 class="text-danger">Payment channels</h2>
    </div>
	<h3><b>How to pay</b> (click on icon to view instructions)</h3>

	<?php foreach($channels as $k=>$v) :  $val = Html::decode($v['instructions']) ?>
    <?php if($v['payment_channel_logo']): ?>
		<div class='col-sm-4 col-md-3 ' data-html="true" data-toggle="popover" title="<b><span style='font-size:20px;align:center;margin-top:100px;'>Instructions</span></b>" data-placement="bottom" data-content="<?= $val ?>">


                 
                 <img src="<?= Url::to(['/import/import/image-link2', 'id'=>$v['payment_channel_logo']]) ?>" height="100" width="100" />
                 <br>

  	<?= "<div style='height:50px'>".$v['channel_name']."</div>"; ?>
	</div>
  <?php endif; ?>
	 <?php endforeach; ?>


</div>
</div>
</div>
</section>
<?php 
$url = Url::to(['/site/school']);
$script = <<< JS
$(document).ready(function(){
	 $(".bg-danger").removeClass('bg-danger');
    $("#schools_and_channels").addClass('bg-danger');
    $("#schools_and_channels").addClass('text-white');
    $('[data-toggle="popover"]').popover(); 
     $('#site_school_search').change(function(){
     	$('#sch_container').css('display', 'block');
     	$('#sch_container').html("<p><b>Loading..</b></p>");
     	var sch = $(this).val();
     	$.get('$url', {id : sch}, function( data ) {
            $('#sch_container').html(data);
        });
     })

    $('html').on('click', function(e) {
	  if (typeof $(e.target).data('original-title') == 'undefined' &&
	     !$(e.target).parents().is('[data-toggle="popover"]')) {
	    $('[data-original-title]').popover('hide');
	  }
	});
	
 });
JS;
$this->registerJs($script);
?>

 
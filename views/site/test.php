<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */
/* @var $stdmodel app\modules\schoolcore\models\CoreStudent */


use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

use app\models\LoginForm;
use yii\web\View;
\app\assets\LoginAsset::register($this);
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="login-block">
    <div class="login-container">
        <div class="row">
            <div class="col-md-5 login-sec">


                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'options'=>["autocomplete"=>"off"]
                ]); ?>
                <form class="login-form">
                    <div class="form-group">
                        <label for="exampleInputEmail1" >Username</label>
                        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <?= $form->field($model, 'password')->passwordInput()->label(false)?>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'captcha', ['options'=>['id'=>'loginform-captcha-id'
                        ]])->widget(Captcha::className(), ['options'=>['class'=>'form-control pull-left', 'placeholder'=>'Enter Above code']])->label(false) ?>
                    </div>


                    <div class="form-check">

                        <div class="mt-2">
                            <?= Html::submitButton('Login', ['class' => 'btn submit-btn text-white w-100 mt-3']) ?>
                            <pre id='log'></pre>
                        </div>
                    </div>

                </form>
                <?php ActiveForm::end(); ?>


                <div class="copy-text">
                    Forgot your password? <?= Html::a(' Click here to reset ?', ['site/request-password-reset']) ?>
                </div>
            </div>
            <div class="col-md-7 banner-sec cliente">

                  <span class="text-center">
                                 <a href="<?= Url::to(['/site/home-page']); ?>">
                                     <?= Html::img('@web/web/img/PNG1.png', ['alt'=>'logo', 'class'=>'img-fluid logo mt-3']);?>
                                 </a>
                             </span>
                <div id="carouselExampleIndicators" class="carousel slide carousel-fade " data-ride="carousel">

                    <div class="carousel-inner">
                        <div class="carousel-item active mt-5">

                            <div class="alert alert-info advert-space" style="width: 100%">
                                <p style="text-align: center"><strong>GOOD NEWS!!</strong></p>The School Suite website has been <span style="font_weight:700 !important; text-decoration: underline">zero rated</span>. This means you can access and use the platform at no data cost with MTN.
                                <hr>Remember to Stay home and Stay safe. <br><br>COVID-19 is real, Keep alert.

                            </div>
                            <div id="ad-features">
    <span class="ad-btn1">
        <i class="fa fa-times" aria-hidden="true"></i>
        <!--        <button aria-hidden="true"></button>-->
    </span>

                                <!--REPLACE THIS IMAGE LINK-->
                                <a href="#" target="_blank">
                                    <?= Html::img('@web/web/img/advert20.gif', ['alt'=>'logo', 'class'=>'img-fluid']);?>
                                </a>
                            </div>

                            <!--                            <img class="d-block w-100" src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide">-->

                        </div>
                    </div>

                </div>
            </div>
        </div>


</section>




<?php
$script = <<< JS
    $(document).ready(function(){

        $("#ad-features").fadeIn()
        .animate({bottom:0,right:0}, 1500, function() {
            //callback
        });      

        $(".ad-btn1").click(function(e){
            e.preventDefault();
            e.stopPropagation()
            $('#ad-features').fadeOut();
        });
      
    });
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */
$this->title = 'Reset Password';

?>

<div class="wrapper">


    <div class="inner-warpper text-center">
        <div>
            <h5 class="box-title text-center" style="color: #fff"><b><i class="fa  fa-user-lock"></i> Reset password
                    for <?= $model->username ?> </b></h5>
            <hr class="style14">
        </div>
        <p style="min-height: 10px"></p>
        <?php $form = ActiveForm::begin([
            'id' => 'change-password-form',
            'fieldConfig' => [
                'template' => "{label}{input}{error}",
            ],
            'action'=>''
        ]); ?>

        <div class="social-auth-links text-center mt-2 mb-3">
            <?= $form->field($model, 'current_pass', ['options'=>[
                'class'=>'form-group field-loginform-password has-feedback required'
            ],
                'template'=>'{label}{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    {error}{hint}',

                'labelOptions' => ['style' => 'color:#fff'],
                ])->passwordInput(['maxlength' => 60, 'placeholder' => 'Enter Current Password'])->label('Current Password') ?>
        </div>
        <div class="social-auth-links text-center mt-2 mb-3">
            <?= $form->field($model, 'new_pass', ['options'=>[
                        'class'=>'form-group field-loginform-password has-feedback required'
                        ],
                        'template'=>'{label}{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    {error}{hint}',

                'labelOptions' => ['style' => 'color:#fff'],
                   ]
                )->passwordInput(['maxlength' => 60, 'placeholder' => 'New Password'])->label('New Password') ?>
        </div>
        <div class="social-auth-links text-center mt-2 mb-3">
            <?= $form->field($model, 'password2', ['options'=>[
                'class'=>'form-group field-loginform-password has-feedback required'
            ],
                'template'=>'{label}{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    {error}{hint}',

                'labelOptions' => ['style' => 'color:#fff'],
                ])->passwordInput(['maxlength' => 60, 'placeholder' => 'Confirm New Password'])->label('Re-enter New password') ?>
        </div>        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>


        <!--            <button type="submit" id="login">Login</button>-->


        <?php ActiveForm::end(); ?>

    </div>
</div>

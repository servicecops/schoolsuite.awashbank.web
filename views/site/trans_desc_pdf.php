<?php
use app\components\ToWords;
use yii\helpers\Url;

?>
<table align="center">
	<tr>
	<td><img src="<?=  Url::to('@web/web/img/receipt_logo.png', true) ?>" height="60" width="94" /></td>
	<td style="color:red;font-size:22px;" align="right" width="85.8%"><tt><?= $trans['reciept_number'] ?></tt></td>
	</tr>
</table>
<div class="row" style="margin-top:-80px;">
	<div style="text-align:center;">
		<?php if($trans['school_logo']) : ?>
		<img src="<?= Url::to(['/import/import/image-link2', 'id'=>$trans['school_logo']], true) ?>" height="65" width="65" />
		<?php else :?>
		<img src="<?= Url::to('@web/web/img/sch_logo_icon.jpg', true) ?>" height=65, width=65 />
		<?php endif; ?>
		<h2><?= $trans['school_name'] ?></h2>
		<h4>Transaction Receipt</h4>
	</div>
</div>

<table style="font-size:9pt;padding:0 25px 0 25px;" align="center">
	<tr>
		<td><tt><b>Payment Code:</b></tt></td><td style="padding-left:15px;"><tt><b><?= $trans['payment_code'] ?></b></tt></td>
	</tr>
	<tr>
		<td><tt>Registration Number:</tt></td><td style="padding-left:15px;"><tt><?= $trans['reg_no'] ?></tt></td>
	</tr>
	<tr>
		<td><tt>Student Name:</tt></td><td style="padding-left:15px;"><tt><?= $trans['first_name'].' '.$trans['middle_name'].' '.$trans['last_name'] ?></tt></td>
	</tr>
	<tr>
		<td><tt><b>Amount:</b></tt></td><td style="padding-left:15px;"><b><tt><?= number_format($trans['amount'], 2) ?></tt></b></td>
	</tr>
	<tr>
		<td><tt>Amount in words:</tt></td><td style="padding-left:15px;"><tt><?= $trans['amount'] ? ucwords(ToWords::convertNumberToWord($trans['amount'])).' Birr Only' : ''; ?></tt></td>
	</tr>
	<tr>
		<td><tt>Date:</tt></td><td style="padding-left:15px;"><tt><?= date('d M, Y  h:i a', strtotime($trans['date_created'])) ?></tt></td>
	</tr>
	<tr>
		<td><tt>Channel:</tt></td><td style="padding-left:15px;"><tt><?= $trans['channel_name'] ?></tt></td>
	</tr>
	<tr>
		<td><tt>Description:</tt></td><td style="padding-left:15px;"><tt><?= $trans['description']?></tt></td>
	</tr>
	<tr>
		<td><tt>Trans Type:</tt></td><td style="padding-left:15px;"><tt><?= $trans['trans_type'] ?></tt></td>
	</tr>
	<tr>
		<td><tt>Channel Depositor Name:</tt></td><td style="padding-left:15px;"><tt><?= $trans['channel_depositor_name'] ?></tt></td>
	</tr>
	<tr>
		<td><tt>Channel Depositor Branch:</tt></td><td style="padding-left:15px;"><tt><?= $trans['channel_depositor_branch'] ?></tt></td>
	</tr>
	<tr>
		<td><tt>Channel Memo:</tt></td><td style="padding-left:15px;"><tt><?= $trans['channel_memo'] ?></tt></td>
	</tr>
</table>

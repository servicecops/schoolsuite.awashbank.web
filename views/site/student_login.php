<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\StudentLoginForm */


use app\models\LoginForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card mx-auto bg-gradient-success" style="max-width: 400px;">
    <div class="site-login" style="padding-top: 10px">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options'=>['style'=>'width:300px;margin:0px auto;']
        ]); ?>

        <div class="card-body">
            <form class="form">

                <div class="form-group text-dark">
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true])?>
                </div>
                <div class="form-group text-dark">
                    <?= $form->field($model, 'password')->passwordInput()?>
                </div>
                <div class="form-group text-dark">
                    <?= $form->field($model, 'rememberMe')->checkbox([
                    ]) ?>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-1 col-lg-11">
                        <?= Html::submitButton('submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>
            </form>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


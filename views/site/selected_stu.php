<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */
$this->title = 'Select School';
?>

<div class="site-login" style="background: white;
padding: 15px;
margin-top: 30px;">
<div class="login-box">
    <div class="login-logo">
        <br>
        <br>
        <img style="text-align:center; padding-left: 15px;" src="<?= \Yii::getAlias('@web') ?>/web/website/assets/img/index_logo.png"/><br>

      </div>

        <div class="login-box-body" style="text-align: justify;">
            <p style="text-align:center; padding-left: 15px;"><b>Select school</b></p>
            <hr>
            <?php foreach($model as $k=>$v): ?>
            <span class="list_item"><a href="<?= Url::to(['/site/assign-sch', 'id'=>$v['school_id']]) ?>"><?= $v['school_name'] ?></a></span><br>
        <?php endforeach; ?>

    </div>
</div>
 </div>

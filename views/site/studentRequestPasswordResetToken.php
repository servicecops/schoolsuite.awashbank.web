<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\StudentPasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col">

    </div>
    <div class="col-6">
        <div class="card border-0">
            <div class="card-body text-dark">
                <h3 class="card-title text-dark text-center"><?= Html::encode($this->title) ?></h3><hr>
                <p class="card-text text-dark">Please fill out your email. A link to reset password will be sent there.</p>
                <?php $form = ActiveForm::begin(['id' => 'student-request-password-reset-form']); ?>

                <?= $form->field($model, 'student_email')->textInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>

    </div>
    <div class="col">

    </div>
</div>

<?php

// @var $this yii\web\View /
use kartik\editable\Editable;
use miloschuman\highcharts\Highcharts;
use yii\bootstrap4\Progress;
use yii\web\JsExpression;
use yii\web\View;

$this->title = 'Awash E-School';
?>
<div class="page_index">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Home</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

<div class="row ">
<!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Schools</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php
                            $count = \app\modules\schoolcore\models\CoreSchool::find()->count();
                            echo $count;
                            ?></p></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-home fa-2x" style="color: #ec2027"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Students</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"> <?php
                            $count = \app\modules\schoolcore\models\CoreStudent::find()->count();
                            echo $count;
                            ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-graduation-cap fa-2x" style="color: #ec2027"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Teachers</div>
                        <div class="row no-gutters align-items-center">
                            <div class="col-auto">
                                <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"> <?php
                                    $count = \app\modules\schoolcore\models\CoreStaff::find()->count();
                                    echo $count;
                                    ?></div>
                            </div>
<!--                            <div class="col">-->
<!--                                <div class="progress progress-sm mr-2">-->
<!--                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-chalkboard-teacher fa-2x" style="color: #ec2027"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Requests Card Example -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Inactive Users</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php $count = \app\models\User::find()->where(['status' =>9])->count();
                            echo $count;
                            ?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x" style="color: #ec2027"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content card">
    <div class="">
        <div class="card-body">
            <?php
            echo Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Graph'],
                    'xAxis' => [
                        'categories' => ['equity', 'Centenary', 'Stanbic','Opportunity', 'Finance Trust', 'Housing Finance']
                    ],
                    'yAxis' => [
                        'title' => ['text' => 'Schools enrolled']
                    ],
                    'series' => [
                        ['name' => 'Nursery & Kindergaten', 'data' => [1, 0, 4,14, 5, 9]],
                        ['name' => 'Primary', 'data' => [5, 7, 3,7, 8, 13]],
                        ['name' => 'Secondary', 'data' => [4, 0, 8,19, 2, 5]],
                        ['name' => 'University', 'data' => [1, 2, 4,10, 8, 14]],

                    ]
                ]
            ]);
            ?>
        </div>
    </div>
</div>
<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#user").addClass('active');
    $("#page_index").addClass('active');_
  });
JS;
$this->registerJs($script);
?>
</div>

<?php

use app\components\Helpers;
use yii\helpers\Url;

$this->title = "Finance menu";
?>
<!-- Bank management-->
<?php if (Helpers::is('rw_banks')) : ?>
    <div class="row card my-1 bg-light">
        <h3 class="card-header ">Bank management</h3>
        <div class="row card-body justify-content-center">
            <?php if (Helpers::is('rw_banks')) : ?>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/schoolcore/core-bank-details/index']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-bank"></i> <span>Manage banks</span></div>
                </a>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/schoolcore/core-bank-details/create']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-plus-circle"></i> <span>Add bank</span></div>
                </a>
            <?php endif; ?>
            <?php if (Helpers::is('rw_branches')) : ?>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/schoolcore/awash-branches/index']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-th-list"></i> <span>Manage Branches</span></div>
                </a>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/schoolcore/awash-bank-regions/index']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-list"></i> <span>Manage Regions</span></div>
                </a>

            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>


<!-- Payments management-->
<?php if (Helpers::is('r_payments') || Helpers::is('rw_chn')) : ?>
    <div class="row card my-1 bg-light">
        <h3 class="card-header ">Payments</h3>
        <div class="row card-body justify-content-center">
            <?php if (Helpers::is('r_payments')) : ?>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/schoolcore/school-information/accounthistory']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-history" aria-hidden="true"></i> <br><span>School Payment History</span>
                    </div>
                </a>

                <?php if (Helpers::is('view_statement')) : ?>
                    <a class="cardz col-md-3 "
                       href="<?= Url::to(['/recon/postings/statement']); ?>">
                        <div class="display-4 text-center"><i class="fa fa-file-text" aria-hidden="true"></i> <br>
                            <span>Bank statement</span></div>
                    </a>

                    <a class="cardz col-md-3 "
                       href="<?= Url::to(['/recon/reconciliation/fetch-statement']); ?>">
                        <div class="display-4 text-center"><i class="fa fa-folder" aria-hidden="true"></i><br> <span>Statement Reconciliation</span>
                        </div>
                    </a>

                <?php endif; ?>

                    <a class="cardz col-md-3 "
                       href="<?= Url::to(['/schoolcore/school-information/supplementaryhistory']); ?>">
                        <div class="display-4 text-center"><i class="fa fa-history" aria-hidden="true"></i> <br><span>Supplementary Fees Payment History</span>
                        </div>
                    </a>

            <?php endif; ?>

        </div>
    </div>
<?php endif; ?>


<!-- Fees management -->
<?php if (Helpers::is('rw_fd')) : ?>
    <div class="row card my-1 bg-light">
        <h3 class="card-header">Fees/Tuition management</h3>
        <div class="row card-body justify-content-center">
            <a class="cardz col-md-3 " href="<?= Url::to(['/feesdue/fees-due/index']); ?>">
                <div class="display-4 text-center"><i class="fa fa-rub" aria-hidden="true"></i> <span>Fees Due</span>
                </div>
            </a>
            <?php if (Helpers::is('own_sch')) : ?>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/feesdue/fees-due/fees-priority']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-tags"></i> <span>Priority</span></div>
                </a>
            <?php endif; ?>
                <a class="cardz col-md-3 "
                   href="<?= Url::to(['/supplementary/supplementary-fees/index']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-rub" aria-hidden="true"></i>
                        <span>Supplementary Fees</span></div>
                </a>
        </div>
    </div>
<?php endif; ?>


<!-- Bank management-->
<?php if (Helpers::is('bank_reports')) : ?>
    <div class="card my-1 bg-light">
        <h3 class="card-header ">Reconciliations</h3>
        <div class="row card-body justify-content-center">
            <?php if (Helpers::is('rw_banks')) : ?>
                <a class="cardz col-md-3 " href="<?= Url::to(['/banks/reconciliation']); ?>">
                    <div class="display-4 text-center"><i class="fa fa-circle-o"></i> <span>Bank destination</span>
                    </div>
                </a>
            <?php endif; ?>
            <a class="cardz col-md-3 " href="<?= Url::to(['/banks/reconciliation/chn-sch']); ?>">
                <div class="display-4 text-center"><i class="fa fa-chain-broken"></i> <span>Channel - Sch</span></div>
            </a>

            <a class="cardz col-md-3 "
               href="<?= Url::to(['/banks/reconciliation/sch-channel']); ?>">
                <div class="display-4 text-center"><i class="fa fa-chain"></i> <span>Sch - Channel</span></div>
            </a>

            <?php if (Helpers::is('bank_user')) : ?>
                <a class="cardz col-md-3 " href="<?= Url::to(['/banks/reports/all-trans']) ?>">
                    <div class="display-4 text-center"><i class="fa fa-file"></i> <span> Bank Reports Realized Transactions</span>
                    </div>
                </a>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>



<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row" style="padding-bottom: 20px; margin-bottom: 20px"></div>

<div class="site-contact">
    <div class="row" style="padding-bottom: 20px; margin-bottom: 20px"></div>
    <div class="blog contact">
        <div class="container">
            <div class="blog-post pull-left">
                <div class="blog-header">
                    <div class="ribbon-stitches-bottom"></div>

                    <div class="ribbon-stitches-bottom"></div>

                </div>
            </div><!-- End blog-post -->
        </div><!-- End container -->
    </div>

    <div class="comment" style="margin-bottom:0px;">
        <div class="container col-md-offset-2">
            <p><br></p>

            <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

                <div class="alert alert-success">
                    Thank you for contacting us. We will respond to you as soon as possible.
                    <br>
                </div>

                <p>
                    &nbsp;
                </p>
            <?php elseif (Yii::$app->session->hasFlash('contactFormFailed')): ?>
                <div class="alert alert-danger">
                    Our mail server is being worked on. Please try later.
                    We apologize for this inconvenience.
                    <br>
                </div>
            <?php else: ?>


        </div>
        <div class="container col-md-offset-3">

                <h3>Contact Us</h3>

        </div>
        <div class="container col-md-offset-3">
            <i>
                If you have business inquiries or other questions, please fill out the following form and we will get back to you as soon as possible.

            </i>
            <br>
            <br>
        </div>
        <div class="container col-md-offset-3">

            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'email') ?>
                    <?= $form->field($model, 'subject') ?>
                    <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                    <?= $form->field($model, 'captcha')->widget(Captcha::className(), ['options' => ['class' => 'form-control', 'placeholder' => 'Enter Above code']])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn buttonz_login w-100 mt-3', 'name' => 'contact-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <?php endif; ?>
        </div>
    </div>
    <div style="clear: both"> </div>
</div>

<?php
$script = <<< JS
$("document").ready(function(){
    $(".bg-danger").removeClass('bg-danger');
    $("#contact").addClass('bg-danger');
    $("#contact").addClass('text-white');
  });
JS;
$this->registerJs($script);
?>

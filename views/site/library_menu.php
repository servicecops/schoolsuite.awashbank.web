<?php
use app\components\Helpers;
use yii\helpers\Url;
$this->title = "Library menu";
?>


<!-- Fees management -->
<?php if (Helpers::is('r_library') || ( !Helpers::is('non_student') ) ) : ?>
    <div class="card my-1 bg-light">
        <h3 class="card-header display-4">School library center</h3>
        <div class="row card-body justify-content-center">
            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/publisher/']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-send" aria-hidden="true"></i> <span>Publisher</span></div>
            </a>
            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/author/']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-user" aria-hidden="true"></i> <span>Author</span></div>
            </a>

            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/bookshelf/']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-lock" aria-hidden="true"></i> <span>Book shelf</span></div>
            </a>

            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/category/']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-outdent" aria-hidden="true"></i> <span>Category</span></div>
            </a>

            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/catalogue/']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-list" aria-hidden="true"></i> <span>Catalogue</span></div>
            </a>

            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/catalogue/borrowinghistory']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-history" aria-hidden="true"></i> <span>Borrower history</span></div>
            </a>
            <?php if (Helpers::is('rw_library') ) : ?>

            <a class="card col-md-3 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/library/catalogue/borrowingtracker']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-map-marker" aria-hidden="true"></i> <span>Borrower tracker</span></div>
            </a>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>



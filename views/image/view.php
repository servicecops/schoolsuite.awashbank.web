<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Image Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-bank-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
   
    <img src="data:image/jpeg;base64,<?= $model->image_base64 ?>" height="200" width="200" />
    <br>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'description',
        ],
    ]) ?>

</div>

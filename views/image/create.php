<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */

$this->title = 'Create Image Bank';
$this->params['breadcrumbs'][] = ['label' => 'Image Banks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-bank-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

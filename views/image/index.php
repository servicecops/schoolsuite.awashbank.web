<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ImageBankSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Image Banks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-bank-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Image Bank', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'description',
            'image_base64',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

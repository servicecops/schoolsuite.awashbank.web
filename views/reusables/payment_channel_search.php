<?PHP
/**
 * This component will provide a search of a school using select 2
 */


use app\modules\paymentscore\models\PaymentChannels;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

?>
<?php

$baseLogoUrl = Url::to(['/import/import/image-link2']);

if(!isset($theme)) $theme = Select2::THEME_BOOTSTRAP;
if(!isset($size)) $size = Select2::MEDIUM;
if(!isset($inputId)) $inputId = '__payment_channel_search';
if(!isset($placeHolder)) $placeHolder = ' '; //Default to empty space
if(!isset($allowClear)) $allowClear = true;
if(!isset($minimumInputLength)) $minimumInputLength = 3; //Default to 3
if(!isset($modelAttribute)) throw new Exception('School attribute is required for search component');
if(!isset($form)) throw new Exception('Component should be part of a form');
if(!isset($inputClass)) $inputClass = 'form-control';
if(!isset($label)) $label = false;
if(!isset($showLogoAfterSelect)) $showLogoAfterSelect = false;
if(!isset($channelType)) $channelType = '';

$searchJS = <<< JS
//This will format school results
var baseLogo =  '$baseLogoUrl';
var showLogoAfterSelect = '$showLogoAfterSelect';

 var ajaxParams = function(params) { 
     return {q:params.term, type: '$channelType'}; 
 }

 var formatChannelResult = function(repo) {
       if (repo.loading) {
        return repo.text;
    }
    
    return formatChannelWithLogo(repo)
    }
    
    var formatChannelSelection = function (repo) {
     if(!showLogoAfterSelect) return repo.text;
    return formatChannelWithLogo(repo)
    }
    
    var formatChannelWithLogo = function(repo) {
     var logo = '';   
    if(repo.payment_channel_logo) {
        logo = baseLogo + '?id=' + repo.payment_channel_logo;
    }   
    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<img src="' + logo + '" class="img-rounded" style="width:30px" />' +
        '<b style="margin-left:5px">' + repo.text + '</b>' + 
    '</div>' +
   
'</div>';
    
    return '<div style="overflow:hidden;">' + markup + '</div>';
    }
JS;

$this->registerJs($searchJS, View::POS_HEAD);



$url = Url::toRoute(['/paymentscore/payment-channels/payment-channels-list']);
$selectedChannel = empty($model->$modelAttribute) ? '' : PaymentChannels::findOne($model->$modelAttribute)->channel_name;
echo $form->field($model, $modelAttribute)->widget(Select2::classname(), [
    'initValueText' => $selectedChannel, // set the initial display text
    'theme'=>$theme,
    'size'=>$size,
    'options' => [
        'placeholder' => $placeHolder,
        'id'=> $inputId,'class'=>$inputClass
    ],
    'pluginOptions' => [
        'placeholder' => $placeHolder,
        'allowClear' => $allowClear,
        'minimumInputLength' => $minimumInputLength,
        'language' => [
            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
        ],
        'ajax' => [
            'url' => $url,
            'dataType' => 'json',
            'data' => new JsExpression('ajaxParams')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('formatChannelResult'),
        'templateSelection' => new JsExpression('formatChannelSelection'),

    ],
])->label($label); ?>

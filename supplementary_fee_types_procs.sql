create or replace function sp_student_inquiry(in_payment_channel bigint, in_student_code character varying, in_amount numeric, in_school_code character varying) returns json
	security definer
	language plpgsql
as $$
declare

studentPaymentCode varchar;
student_record record;
section_record record;
bank_record record;
bank_account_records record;
payment_channel_account_record record;
school_details_record record;
mtn_hosted_service_record record;
mtn_ova varchar;

fetch_cursor refcursor;

associated_fees json;
minimum_amount numeric;
percentage_lock_record record;
class_code varchar;

supplementary_fees json;
begin

	studentPaymentCode = upper(trim(replace(in_student_code, ' ', '')));

	-- Get the student
		IF studentPaymentCode ~ '^((3)\d{9})$' -- Payment code or not
		then
			OPEN fetch_cursor for
select
    si.id,
    si.first_name,
    si.middle_name,
    si.last_name,
    si.guardian_phone ,
    si.school_id,
    si.active,
    si.student_account_id,
    si.student_code,
    isc.class_code ,
    isc.class_description,
    si.class_id,
    si.archived,
    sch.school_code,
    sch.school_name,
    sch.active as school_active,
    sch.default_part_payment_behaviour,
    sag.account_balance,
    si.school_student_registration_number,
    si.date_of_birth,
    case when sag.outstanding_balance > 0 then 0 else abs(sag.outstanding_balance) end as outstanding_balance,
    case when sch.default_part_payment_behaviour = true then true else si.allow_part_payments end as student_part_payments,
    format('%s%s%s', first_name,
           case
               when middle_name is not null then ' ' || middle_name
               else ''
               end,
           case
               when last_name is not null then ' ' || last_name
               else ''
               end) as student_name,
    case
        when si.allow_part_payments is null then true
        else si.allow_part_payments
        end as part_payments
from
    core_student si
        inner join core_school sch on
            sch.id = si.school_id
        inner join core_school_class isc on
            si.class_id = isc.id
        inner join student_account_gl sag on sag.id = si.student_account_id
where
        student_code = cast(studentPaymentCode as int8)
  and sch.external_school_code = in_school_code
    limit 1;
FETCH fetch_cursor INTO student_record;
close fetch_cursor;
ELSE
		studentPaymentCode = upper(studentPaymentCode);
		-- How many students have this reg no?
--		IF (SELECT count(id) from core_student where school_student_registration_number=studentPaymentCode) > 1
--		THEN
--		RETURN  row_to_json(data) FROM (VALUES (407, 'Student selection failed. Use the 10 digit payment code to select specific student')) data("returnCode", "returnMessage");
--		END IF;

OPEN fetch_cursor for
select
    si.id,
    si.first_name,
    si.middle_name,
    si.last_name,
    si.guardian_phone,
    si.school_id,
    si.active,
    si.student_account_id,
    si.student_code,
    isc.class_code ,
    isc.class_description,
    si.class_id,
    si.archived,
    sch.school_code,
    sch.school_name,
    sch.active as school_active,
    sch.default_part_payment_behaviour,
    sag.account_balance,
    si.school_student_registration_number,
    si.date_of_birth,
    case when sag.outstanding_balance > 0 then 0 else abs(sag.outstanding_balance) end as outstanding_balance,
    case when sch.default_part_payment_behaviour = true then true else si.allow_part_payments end as student_part_payments,
    format('%s%s%s', first_name,
           case
               when middle_name is not null then ' ' || middle_name
               else ''
               end,
           case
               when last_name is not null then ' ' || last_name
               else ''
               end) as student_name,
    case when si.allow_part_payments is null then true else si.allow_part_payments end as part_payments
from
    core_student si
        inner join core_school sch on sch.id = si.school_id
        inner join core_school_class isc on
            si.class_id  = isc.id
        inner join student_account_gl sag on sag.id = si.student_account_id
where
        school_student_registration_number = studentPaymentCode
  and sch.external_school_code = in_school_code
    limit 1;


FETCH fetch_cursor INTO student_record;
close fetch_cursor;
END IF;

		-- Check if we have something atleast
		IF student_record.id IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (404, 'No student was found with the specified payment code or registration number.')) data("returnCode", "returnMessage");
END IF;

	-- Get the school details
select * from sp_resolve_student_school_details(student_record.id) into school_details_record;

if school_details_record.return_code <> 0
	then
	RETURN  row_to_json(data) FROM (VALUES (school_details_record.return_code, school_details_record.return_message)) data("returnCode", "returnMessage");
end if;

	--get account number

select * from core_bank_account_details cbad into bank_account_records where bank_id =school_details_record.bank_id  and school_id =student_record.school_id;
-- Get payment channel
select * from sp_resolve_payment_channel_account(in_payment_channel, school_details_record.bank_id) into payment_channel_account_record;
if payment_channel_account_record.return_code <> 0
	then
	RETURN  row_to_json(data) FROM (VALUES (payment_channel_account_record.return_code, payment_channel_account_record.return_message)) data("returnCode", "returnMessage");
end if;

	-- DOES THE PAYMENT CHANNEL HAVE ENOUGH BALANCE
		IF  in_amount is not null and in_amount<>0 and  (payment_channel_account_record.account_balance - in_amount) < payment_channel_account_record.account_limit
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (309, 'INSUFFICIENT BALANCE. PAYMENT CHANNEL DOESN''T HAVE SUFFICIENT BALANCE FOR THIS OPERATION')) data("returnCode", "returnMessage");
END IF;

		-- CHECK IF STUDENT IS NOT ACTIVE
		IF student_record.active = false and student_record.archived = false
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (502, 'INACTIVE: STUDENT IS NOT ACTIVE. ALL FUTURE REQUESTS FOR STUDENT WILL FAIL.')) data("returnCode", "returnMessage");
END IF;



		-- is school active
		IF student_record.school_active = FALSE
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (501, 'INACTIVE: THE ASSOCIATED SCHOOL / INSTITUTION IS NOT ACTIVE')) data("returnCode", "returnMessage");
END IF;

   --School and payment channel should be associated
   if not exists (select 1 from institution_payment_channel_association ipca where ipca.institution = student_record.school_id and ipca.payment_channel = in_payment_channel)
   then
   RETURN  row_to_json(data) FROM (VALUES (511, 'NOT ALLOWED. School ' || student_record.school_name || ' does not accept payments from ' || payment_channel_account_record.channel_name )) data("returnCode", "returnMessage");
end if;

  -- CHECK IF WE SHOULD ALLOW PART PAYMENTS select * from student_account_gl limit 10;
		IF 	student_record.default_part_payment_behaviour = false and in_amount is not null and  in_amount<>0
		THEN
			IF student_record.part_payments = FALSE AND in_amount < ABS(student_record.outstanding_balance)
			THEN
			--ITS A PART PAYMENT
			RETURN  row_to_json(data) FROM (VALUES (204, 'Sorry, Student ' || student_record.student_name || ' is not allowed to make part payments. Minimum amount allowed is ' || (ABS(student_record.outstanding_balance)) )) data("returnCode", "returnMessage");
END IF;
end if;



	--If mtn channel, return the ova as well
	if 	payment_channel_account_record.channel_code = 'MTN_UG'
	then
			OPEN fetch_cursor for
select * from mtn_hosted_services mhs where mhs.service_id = format('SCHOOLPAY-%s', school_details_record.bank_code);
FETCH fetch_cursor INTO mtn_hosted_service_record;
close fetch_cursor;

if mtn_hosted_service_record.id is null
	    then
	    	RETURN  row_to_json(data) FROM (VALUES (1204, 'MTN Payment channel is not correctly configured on the Schoolpay Interface')) data("returnCode", "returnMessage");
end if;
	   mtn_ova = mtn_hosted_service_record.mtn_ova;
end if;


select
    json_agg(c)
from
    (
        select
            ifd.description as "feeDescription",
            ifd.due_amount as "feeAmount",
            ifsa.id as "feeId",
            ifd.effective_date as "feeDueFromDate",
            ifd.end_date as "feeDueToDate"

        from
            institution_fee_student_association ifsa
                inner join institution_fees_due ifd on
                    ifsa.fee_id = ifd.id
        where ifsa.student_id = student_record.id
          and ifd.end_date >= current_date) c
    into associated_fees;

if associated_fees is null
		then
		associated_fees = '[]'::json;
end if;

	--Compute minimum amount
	minimum_amount = 0;
    if student_record.student_part_payments <> true
    then
    minimum_amount = abs(student_record.outstanding_balance);
end if;


   --Validate percentage locks as well
select * from school_minimum_fee_percentage_rules smfpr
where smfpr.school_id = student_record.school_id and smfpr.minimum_percentage > 0 and smfpr.minimum_percentage < 100
    into percentage_lock_record;

if percentage_lock_record is not null
	then
		minimum_amount = abs(floor(percentage_lock_record.minimum_percentage * student_record.outstanding_balance / 100));
		if in_amount is not null and  in_amount<>0  and in_amount < minimum_amount
		then
			RETURN  row_to_json(data) FROM (VALUES (907, 'School requires atleast ' || percentage_lock_record.minimum_percentage || ' percent of the outstanding balance. ' || 'Pay atleast UGX ' || trim(to_char(minimum_amount, '999,999,999,999,999')) )) data("returnCode", "returnMessage");
end if;
end if;


	--For mobile channels use shorter class code
	if payment_channel_account_record.channel_type = 'Mobile'
	then
		class_code = trim(left(student_record.class_code, 15));
else
		class_code = trim(format('%s - %s', student_record.class_code, student_record.class_description));
end if;


-- Get supplementary fees
select * from sp_inquire_student_supplementary_fees(student_record.id) into supplementary_fees;

RETURN row_to_json (DATA)
    FROM
	(

		VALUES
			(
				0,
				student_record.student_name,
				school_details_record.external_school_code,
				upper(school_details_record.school_name),
				student_record.first_name,
				student_record.middle_name,
				student_record.last_name,
				class_code,
				school_details_record.bank_code,
				mtn_ova,
				associated_fees,
				student_record.student_code,
				student_record.student_part_payments,
				student_record.outstanding_balance,
				minimum_amount,
				student_record.school_student_registration_number,
				student_record.date_of_birth,
				bank_account_records.account_number,
                supplementary_fees

			)
	) DATA ("returnCode", "returnMessage", "schoolCode", "schoolName", "firstName", "middleName", "lastName", "studentClass", "bankId", "mtnOva", "associatedFees", "paymentCode", "allowPartPayments", "outstandingAmount", "minimumAmount", "registrationNumber", "dateOfBirth","accountNumber", "supplementaryFees") ;

end;
$$;


create or replace function sp_inquire_student_supplementary_fees(in_student_id bigint) returns jsonb
	security definer
	language plpgsql
as $$
declare
associated_fees json;
student_record record;
begin

select * from core_student where id = in_student_id into student_record;

select
    json_agg(c)
from
    (
        select
            isf.description as "feeDescription",
            sfc.category_description as "feeType",
            isf.id as "feeId",
            isf.due_amount as "feeAmount"
        from institution_supplementary_fees isf
                 inner join supplementary_fee_category sfc on sfc.id = isf.supplementary_fee_category_id
                 inner join supplementary_fee_class_association sfca on sfca.fee_id = isf.id
        where sfca.class_id = student_record.class_id
          and isf.school_id = student_record.school_id
          and isf.active = true
          and isf.start_date<= current_date and isf.end_date>=current_date
        order by isf.description
    ) c
    into associated_fees;

if associated_fees is null
    then
        associated_fees = '[]'::json;
end if;

return associated_fees;
end;
$$;


create or replace function sp_process_student_supplementary_payment(in_payment_channel bigint, in_payment_json text) returns json
	security definer
	language plpgsql
as $$
declare
request_json json;

student_record record;
school_record record;
payment_channel_record record;
supplementary_fee_record record;

student_account_record record;
school_account_record record;
payment_channel_account_record record;

payment_record record;
bank_record record;
effective_school_details_record record;
amount numeric;
paymentType varchar;


fetch_cursor refcursor;
studentPaymentCode varchar;
schoolCode varchar;


sms_message_template varchar;
sms_text varchar;

message_fields_json json;
receipt_base varchar;


begin
		-- Parse INPUT
		request_json = in_payment_json::json;
		amount = cast(request_json ->> 'amount' as numeric);
		studentPaymentCode = request_json ->> 'studentPaymentCode';
	    schoolCode = request_json ->> 'schoolCode';
		studentPaymentCode = upper(trim(replace(studentPaymentCode, ' ', '')));
		paymentType = request_json ->> 'paymentType';


		IF studentPaymentCode IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (100, 'STUDENT PAYMENT CODE IS REQUIRED BUT WAS NOT SUPPLIED')) data("returnCode", "returnMessage");
END IF;

		IF schoolCode IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (1002, 'SCHOOL CODE IS REQUIRED BUT WAS NOT SUPPLIED')) data("returnCode", "returnMessage");
END IF;

		IF amount IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (101, 'AMOUNT IS REQUIRED BUT WAS NOT SUPPLIED')) data("returnCode", "returnMessage");
END IF;

		IF amount <= 0
		THEN
			RETURN  row_to_json(data) FROM (VALUES (109, 'AMOUNT SHOULD BE GREATER THAN ZERO')) data("returnCode", "returnMessage");
END IF;

		IF in_payment_channel IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (105, 'PAYMENT CHANNEL IS NOT SUPPLIED')) data("returnCode", "returnMessage");
END IF;

		IF request_json ->> 'channelTransactionId' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (106, 'CHANNEL TRANS ID IS REQUIRED AND WAS NOT SPECIFIED')) data("returnCode", "returnMessage");
END IF;

		IF request_json ->> 'channelMemo' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (107, 'CHANNEL MEMO IS REQUIRED AND WAS NOT SPECIFIED')) data("returnCode", "returnMessage");
END IF;

		IF request_json ->> 'processDate' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (108, 'PROCESS DATE IS REQUIRED AND WAS NOT SPECIFIED')) data("returnCode", "returnMessage");
END IF;

		IF request_json ->> 'feeId' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (1108, 'Supplementary fee id to pay is required and was not supplied')) data("returnCode", "returnMessage");
END IF;



		-- Avoid blank payment types
		IF paymentType IS NULL
		THEN
		paymentType = 'CASH';
END IF;
		-- Work on fee associations

		-- Get the PAYMENT CHANNEL
OPEN fetch_cursor for
select * from payment_channels where id = in_payment_channel limit 1;
FETCH fetch_cursor INTO payment_channel_record;
close fetch_cursor;


IF payment_channel_record.id IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (199, 'Payment channel was not found')) data("returnCode", "returnMessage");
END IF;

		-- Get the student
		IF studentPaymentCode ~ '^((33)\d{8})$' -- Payment code or not
		then
			OPEN fetch_cursor for
select
    si.id,
    si.first_name,
    si.middle_name,
    si.last_name,
    si.guardian_phone,
    si.school_id,
    si.active,
    si.student_account_id,
    si.student_code,
    isc.class_code ,
    isc.class_description,
    si.school_student_registration_number,
    si.class_id,
    si.archived,
    format('%s%s%s', first_name,
           case
               when middle_name is not null then ' ' || middle_name
               else ''
               end,
           case
               when last_name is not null then ' ' || last_name
               else ''
               end) as student_name,
    case when si.allow_part_payments is null then true else si.allow_part_payments end as part_payments
from
    core_student si
        inner join core_school_class isc on
            si.class_id = isc.id
        inner join core_school cs on cs.id = si.school_id
where
        student_code = cast(studentPaymentCode as int8)
  and cs.external_school_code = schoolCode
    limit 1;
FETCH fetch_cursor INTO student_record;
close fetch_cursor;
ELSE
		studentPaymentCode = upper(studentPaymentCode);
		-- How many students have this reg no?
		IF (SELECT count(cs.id) from core_student cs inner join core_school cs2 on cs.school_id = cs2.id where school_student_registration_number=studentPaymentCode and cs2.external_school_code = schoolCode) > 1
		THEN
		RETURN  row_to_json(data) FROM (VALUES (407, 'Student selection failed. Use the 10 digit payment code to select specific student')) data("returnCode", "returnMessage");
END IF;


OPEN fetch_cursor for
select
    si.id,
    si.first_name,
    si.middle_name,
    si.last_name,
    si.guardian_phone,
    si.school_id,
    si.active,
    si.student_account_id,
    si.student_code,
    isc.class_code ,
    isc.class_description,
    si.school_student_registration_number,
    si.class_id,
    si.archived,
    format('%s%s%s', first_name,
           case
               when middle_name is not null then ' ' || middle_name
               else ''
               end,
           case
               when last_name is not null then ' ' || last_name
               else ''
               end) as student_name,
    case when si.allow_part_payments is null then true else si.allow_part_payments end as part_payments
from
    core_student si
        inner join core_school_class isc on
            si.class_id = isc.id
        inner join core_school cs on cs.id = si.school_id
where
        school_student_registration_number = studentPaymentCode
  and cs.external_school_code = schoolCode
    limit 1;

FETCH fetch_cursor INTO student_record;
close fetch_cursor;
END IF;

		-- Check if we have something atleast
		IF student_record.id IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (404, 'No student was found with the specified payment code or registration number')) data("returnCode", "returnMessage");
END IF;



		-- Get the school
select * from core_school where id = student_record.school_id into school_record;

IF school_record.id IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, student does not belong to a valid school or institution')) data("returnCode", "returnMessage");
END IF;

OPEN fetch_cursor for
select * from nominated_bank_details nbd where id = school_record.bank_name limit 1;
FETCH fetch_cursor INTO bank_record;
close fetch_cursor;

select * from sp_resolve_student_school_details(student_record.id) into effective_school_details_record;

--Resolve the payment channel account to use
select * from sp_resolve_payment_channel_account(payment_channel_record.id, effective_school_details_record.bank_id) into payment_channel_account_record;
-- DOES THE PAYMENT CHANNEL HAVE ENOUGH BALANCE
IF (payment_channel_account_record.account_balance - amount) < payment_channel_account_record.account_limit
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (309, 'INSUFFICIENT BALANCE. PAYMENT CHANNEL DOESN''T HAVE SUFFICIENT BALANCE FOR THIS OPERATION')) data("returnCode", "returnMessage");
END IF;

		-- CHECK IF STUDENT IS NOT ACTIVE
		IF student_record.active = false and student_record.archived = false
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (502, 'INACTIVE: STUDENT IS NOT ACTIVE. ALL FUTURE REQUESTS FOR STUDENT WILL FAIL.')) data("returnCode", "returnMessage");
END IF;



		-- is school active
		IF school_record.active = FALSE
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (501, 'INACTIVE: THE ASSOCIATED SCHOOL / INSTITUTION IS NOT ACTIVE')) data("returnCode", "returnMessage");
END IF;



		-- CHECK IF SCHOOL ALLOWS PAYMENTS FROM THIS CHANNEL
		IF NOT EXISTS (SELECT 1 FROM institution_payment_channel_association WHERE institution = school_record.id AND payment_channel = in_payment_channel)
		THEN
		RETURN  row_to_json(data) FROM (VALUES (511, 'NOT ALLOWED. School ' || school_record.school_name || ' does not accept payments from ' || payment_channel_record.channel_name )) data("returnCode", "returnMessage");
END IF;


		-- Check if payment payment for trans id EXISTS
OPEN fetch_cursor for
select * from payments_received where payment_channel = payment_channel_record.id and channel_trans_id = request_json->>'channelTransactionId' LIMIT 1;
FETCH fetch_cursor INTO payment_record;
close fetch_cursor;

IF payment_record.id IS NOT NULL
			THEN
                RETURN  row_to_json(data) FROM (VALUES (456, 'DUPLICATE TRANS ID: EACH TRANS ID SHOULD BE UNIQUE FOR A PAYMENT CHANNEL. '|| (request_json->>'channelTransactionId') ||' WAS ALREADY RECEIVED FOR A COMPLETELY DIFFERENT PAYMENT.')) data("returnCode", "returnMessage");
END IF;


		-- Check if payment payment for trans id EXISTS in supplementary payments
OPEN fetch_cursor for
select * from supplementary_fee_payments_received where payment_channel = payment_channel_record.id and channel_trans_id = request_json->>'channelTransactionId' LIMIT 1;
FETCH fetch_cursor INTO payment_record;
close fetch_cursor;

IF payment_record.id IS NOT NULL
			THEN
			--WE ALREADY HAVE A PAYMENT FOR THIS TRANSID AND FROM THIS CHANNEL
						IF student_record.id = payment_record.student_id AND payment_record.amount = amount
						THEN
						--IF AMOUNT IS SAME AND STUDENT IS THE SAME THEN ITS A REPOSTING, SO RETURN DETAILS WITH RETURN CODE 0

						RETURN row_to_json (DATA)
						FROM
							(

								VALUES
									(
										0,
										format('Supplementary fees payment was already processed on %s. Reference:%s', payment_record.date_created, payment_record.reciept_number),
										effective_school_details_record.external_school_code,
										school_record.school_name,
										student_record.first_name,
										student_record.middle_name,
										student_record.last_name,
										format('%s - %s', student_record.class_code, student_record.class_description),
										payment_record.reciept_number,
										-- to_json(student_record),
										payment_record.id,
										student_record.student_name
									)
							) DATA ("returnCode", "returnMessage", "schoolCode", "schoolName",
								"firstName", "middleName", "lastName", "studentClass", "receiptNumber",
--								"studentDetails",
								"paymentId", "studentFullNames") ;


END IF;

			-- An attempt to reuse a trans id should fail immediately
RETURN  row_to_json(data) FROM (VALUES (456, 'DUPLICATE TRANS ID: EACH TRANS ID SHOULD BE UNIQUE FOR A PAYMENT CHANNEL. '|| (request_json->>'channelTransactionId') ||' WAS ALREADY RECEIVED FOR A COMPLETELY DIFFERENT PAYMENT.')) data("returnCode", "returnMessage");
END IF;

		-- get the supplementary fee record
select * from institution_supplementary_fees where id = cast(request_json ->> 'feeId' as bigint)
                                               and school_id = school_record.id
    into supplementary_fee_record;

IF (supplementary_fee_record.id is null)
        THEN
            -- There was a problem getting the school
            RETURN  row_to_json(data) FROM (VALUES (1309, 'SUPPLEMENTARY FEE WAS NOT FOUND FOR PAYMENT')) data("returnCode", "returnMessage");
END IF;

		-- Now we are going to start updating


		-- Create the payment channel record first
INSERT INTO supplementary_fee_payments_received (
    date_created,
    student_id,
    school_id,
    payment_channel,
    channel_trans_id,
    channel_memo,
    amount,
    channel_process_date,
    channel_payment_type,
    channel_depositor_name,
    channel_depositor_phone,
    channel_depositor_branch,
    channel_teal_id,
    reversed,
    date_reversed,
    reversal_payment_id,
    reversal,
    payment_reference,
    fee_id
)
VALUES
(
    NOW(),
    student_record.id,
    school_record.id,
    in_payment_channel,
    request_json->>'channelTransactionId',
    FORMAT('%s %s', request_json->>'channelMemo', supplementary_fee_record.description),
    amount,
    cast(request_json->>'processDate' as timestamp),
    paymentType,
    request_json->>'channelDepositorName',
    request_json->>'channelDepositorPhone',
    request_json->>'channelDepositorBranch',
    request_json ->>'channelTealId',
    false, --reversed
    null, --date reversed
    null, --reversal payment id
    false, --reversal,
    studentPaymentCode,
    supplementary_fee_record.id
) RETURNING * INTO payment_record;




-- Create payment channel TH and update its balance
update
    payment_channel_account_gl
set
    balance = (payment_channel_account_record.account_balance - amount),
    last_transaction_date = NOW()
where
        id = payment_channel_account_record.account_id returning *
into
    payment_channel_account_record;

INSERT INTO payment_channel_account_transaction_history(
    date_created,
    amount,
    description,
    payment_id,
    trans_type,
    account_id,
    reversal_flag,
    reversed,
    date_reversed,
    balance_after,
    balance_before,
    channel_transaction_processed_date
)
VALUES
(
    NOW(),
    amount * -1, -- Amount is a debit
    format('SUPPLEMENTARY_PAYMENT_RECV-%s - %s - %s', school_record.school_name, student_record.student_code, student_record.student_name),
    payment_record.id,
    'SUPPLEMENTARY_PAYMENT', -- Trans type
    payment_channel_account_record.id, -- Account id
    false, -- Reversal flag
    false, -- Reversed
    null, -- Date Reversed
    payment_channel_account_record.balance, -- Balance after
    (payment_channel_account_record.balance + amount), -- Balance before it was debited
    payment_record.channel_process_date -- Channel process date
) ;

-- Generate messages
select message_template from message_templates where id = 'SUPPLEMENTARY_FEE_PAYMENT_PROCESSED_SMS_TEMPLATE' limit 1 into sms_message_template;
if sms_message_template is null
        then
            sms_message_template = 'You have paid #AMOUNT# BIRR for #STUDENT_CODE# - #STUDENT_NAME# for #FEE# in #SCHOOL_NAME# via #CHANNEL#. Your receipt: #RECEIPT#';
end if;

select message_template from message_templates where id = 'RECEIPT_BASE_URL' limit 1 into receipt_base;
if receipt_base is null
        then
            receipt_base = 'https://eschooltest.awashbank.com/-';
end if;

        --Build template

        message_fields_json = row_to_json (DATA)
                              FROM
                                  (

                                      VALUES
                                      (
                                          trim(to_char(payment_record.amount, '999,999,999,999,999,999')),
                                          student_record.school_student_registration_number ,
                                          format('%s %s', student_record.first_name, student_record.last_name),
                                          school_record.school_name,
                                          to_char(now(), 'dd/mm/yyyy HH24:mi:ss'),
                                          payment_channel_record.channel_code,
                                          format('%s%s', receipt_base, fn_base36_encode(payment_record.id)),
                                          supplementary_fee_record.description
                                      )
                                  ) DATA ("AMOUNT", "STUDENT_CODE", "STUDENT_NAME", "SCHOOL_NAME", "DATE", "CHANNEL", "RECEIPT", "FEE") ;
select * from mtp_template_replace(sms_message_template, cast(message_fields_json as text)) into sms_text;

-- Create message
if payment_record.channel_depositor_phone ~ '^(0|251)\d{9}$'
	     then

	            INSERT
	            INTO
	                public.message_outbox (message_text,
	                                       time_generated,
	                                       recipient_number,
	                                       message_status,
	                                       flash_message,
	                                       email_message)
	            VALUES(sms_text,
	                   now(),
	                   payment_record.channel_depositor_phone,
	                   'PENDING'::CHARACTER VARYING,
	                   FALSE,
	                   FALSE);
end if;

		-- Return details as json
		-- Parameter validations
RETURN row_to_json (DATA)
    FROM
	(

		VALUES
			(
				0,
				format('School supplementary fee payment has been processed succesfully. Reference:%s', payment_record.reciept_number),
				effective_school_details_record.external_school_code,
				school_record.school_name,
				student_record.first_name,
				student_record.middle_name,
				student_record.last_name,
				format('%s - %s', student_record.class_code, student_record.class_description),
				payment_record.reciept_number,
--				to_json(student_record),
				payment_record.id,
				student_record.student_name,
			    supplementary_fee_record.description
			)
	) DATA ("returnCode", "returnMessage", "schoolCode", "schoolName", "firstName", "middleName", "lastName",
			"studentClass", "receiptNumber",
--			"studentDetails",
			"paymentId", "studentFullNames", "feeDescription") ;

end;
$$;



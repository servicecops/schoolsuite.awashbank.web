CREATE OR REPLACE FUNCTION public.adjust_student_balance(this_student_payment_code bigint, this_new_balance numeric, posting_user_id numeric, posting_user character varying, reason character varying, ip character varying, adjustment_mode character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
 this_student_id integer;
this_student_account_id integer;
amount_to_post numeric;
new_outstanding_balance numeric;
student_th_record_id integer;
student_name varchar;
account_record_before record;
account_record record;
begin
		--Fetch the student account using payment code 
		if NOT exists (select 1 from core_student cs  where student_code = this_student_payment_code) THEN
				RAISE EXCEPTION 'STUDENT NOT FOUND WITH THE SPECIFIED PAYMENT CODE';
		end if;

		-- Select the student payment code
		SELECT id, student_account_id,
		format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) 
	into this_student_id, this_student_account_id, student_name from core_student cs  where student_code  = this_student_payment_code;
		
		--Select account for ID
		SELECT * from student_account_gl where id = this_student_account_id into account_record_before for update;
		--Determine what amount to POST
		amount_to_post = this_new_balance - account_record_before.account_balance;
		
		if adjustment_mode = 'BY'
		then 
		amount_to_post = this_new_balance;
		end if;
	
		-- if amount to post is zero, do NOTHING
		IF amount_to_post = 0 AND account_record_before.outstanding_balance = least(account_record_before.account_balance+amount_to_post, 0) THEN
			RAISE NOTICE 'NO AMOUNT TO PASS';
			RETURN false;
		END IF;
	
		update student_account_gl set account_balance = account_balance+amount_to_post, outstanding_balance=least(account_balance+amount_to_post, 0) where id = this_student_account_id returning * into account_record;

	
		
		--Now post by inserting into student th, and updating balance accordingly
		INSERT INTO student_account_transaction_history(
            date_created, amount, posting_user, description, 
            trans_type, account_id, reversal_flag, reversed, 
            balance_after, outstanding_balance_after, balance_before, outstanding_balance_before)
    VALUES (now(), amount_to_post, posting_user, reason,  
            'BAL-ADJUSTMENT', this_student_account_id, false, false,
            account_record.account_balance, account_record.outstanding_balance, account_record_before.account_balance, account_record_before.outstanding_balance) returning id into student_th_record_id;

		
		
		
		-- If amount is positive, apportion it to fees
		if amount_to_post > 0
		then
			perform apportion_payment_amount_to_fees(this_student_id, 0, amount_to_post);
		end if;
		
		
		-- Log to web console log
	INSERT INTO web_console_log ( "ip_address", "user_name", "event_date", "event_action", "affected_student_id", "web_user_id", "event_action_id", "student_th_id" )
	VALUES
		( ip, posting_user, now(), format('%s''s balance adjusted to %s', student_name, this_new_balance), this_student_id, posting_user_id, 'ADJUST_BALANCE', student_th_record_id );
		
		RETURN true;
end;
$function$
;



CREATE OR REPLACE FUNCTION public.adjust_student_balance(this_student_payment_code bigint, this_new_balance numeric, posting_user_id numeric, posting_user character varying, reason character varying, ip character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$



declare

 this_student_id integer;

this_student_account_id integer;

this_student_account_balance numeric;

this_student_outstanding_balance numeric;

amount_to_post numeric;

new_outstanding_balance numeric;

student_th_record_id integer;

student_name varchar;

begin

		--Fetch the student account using payment code 

		if NOT exists (select 1 from core_student where student_code = this_student_payment_code) THEN

				RAISE EXCEPTION 'STUDENT NOT FOUND WITH THE SPECIFIED PAYMENT CODE';

		end if;



		-- Select the student payment code

		SELECT id, student_account_id,

		format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) 

	into this_student_id, this_student_account_id, student_name from core_student where student_code = this_student_payment_code;

		

		--Select account for ID

		SELECT account_balance, outstanding_balance into this_student_account_balance, this_student_outstanding_balance from student_account_gl where id = this_student_account_id for update;

		--Determine what amount to POST

		amount_to_post = this_new_balance - this_student_account_balance;

		--Now update the balance

		new_outstanding_balance = this_new_balance;	

				

		IF new_outstanding_balance > 0 THEN

			new_outstanding_balance = 0;

		END IF;



		-- if amount to post is zero, do NOTHING

		IF amount_to_post = 0 AND new_outstanding_balance = this_student_outstanding_balance THEN

			RAISE NOTICE 'NO AMOUNT TO PASS';

			RETURN false;

		END IF;



		



		--Now post by inserting into student th, and updating balance accordingly

		INSERT INTO student_account_transaction_history(

            date_created, amount, posting_user, description, 

            trans_type, account_id, reversal_flag, reversed, 

            balance_after, outstanding_balance_after)

    VALUES (now(), amount_to_post, posting_user, reason,  

            'BAL-ADJUSTMENT', this_student_account_id, false, false,

            this_new_balance, new_outstanding_balance) returning id into student_th_record_id;



		

		if this_new_balance < 0

		then

		new_outstanding_balance = this_new_balance;

		end if;



		update student_account_gl set account_balance = this_new_balance, outstanding_balance = new_outstanding_balance where id = this_student_account_id;

		

		-- If amount is positive, apportion it to fees

		if amount_to_post > 0

		then

			perform apportion_payment_amount_to_fees(this_student_id, 0, amount_to_post);

		end if;

		

		

		-- Log to web console log

	INSERT INTO web_console_log ( "ip_address", "user_name", "event_date", "event_action", "affected_student_id", "web_user_id","student_th_id"  )

	VALUES

		( ip, posting_user, now(), format('%s''s balance adjusted to %s', student_name, this_new_balance), this_student_id, posting_user_id,student_th_record_id );

		

		RETURN true;

end;

$function$
;


CREATE OR REPLACE FUNCTION public.apply_current_fees_to_student_on_creation()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

declare 



student_school_id int = null;

student_class_id int = null;

this_student_id int = NEW.id;

the_fee_id int;



BEGIN



-- FIND fees associated to the student's class and are valid

-- and apply the fee to this student



	FOR the_fee_id IN

				select ifd.id as the_fee_id from institution_fees_due ifd 

		inner join institution_fee_class_association fda on ifd.id = fda.fee_id

		inner join core_student si on si.class_id = fda.class_id

		where si.id = this_student_id 

		and ifd.approval_status = true

		and ifd.effective_date <= current_date

		and ifd.end_date >= current_date

		and ifd.recurrent = false

   LOOP

      --apply school to payment channel

				PERFORM apply_fee_to_student(this_student_id, the_fee_id, 0, 'SYSTEM', '');

   END LOOP;

		





RETURN NEW;

END;



$function$
;


CREATE OR REPLACE FUNCTION public.apply_fee_to_group(in_fee_id bigint, in_group_id bigint, in_user_id character varying, OUT association_id bigint, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare



fetch_cursor refcursor;
fee_due_record record;
group_record record;
fee_association_record record;
active_child_fee_record record;

begin
	-- Get the fee
		OPEN fetch_cursor for 
			select * from institution_fees_due ifd 
			where ifd.id = in_fee_id limit 1;
			FETCH fetch_cursor INTO fee_due_record;
		close fetch_cursor;	
	
	    if fee_due_record.id is null
	    then
	    	return_code = 404;
	    	return_message = 'Fee not found';
		    return;
	    end if;
	   
	   if fee_due_record.end_date < current_date
	   then
	   		return_code = 504;
	    	return_message = 'Fee end date is in the past';
		    return;
	   end if;
	  
	  if fee_due_record.recurrent = false and fee_due_record.child_of_recurrent = false
	   then
	   		return_code = 505;
	    	return_message = 'Only recurrent fees or child of recurrent fees can be applied to groups';
		    return;
	   end if;
	
	  --get the group	
	   OPEN fetch_cursor for 
			select * from student_group_information sgi 
			where sgi.id = in_group_id limit 1;
			FETCH fetch_cursor INTO group_record;
		close fetch_cursor;	
	
		if group_record.id is null
	    then
	    	return_code = 404;
	    	return_message = 'Group not found';
		    return;
	    end if;
	   
	   if group_record.school_id <> fee_due_record.school_id
	   then
	   		return_code = 1404;
	    	return_message = 'Group and fees should belong to the same school';
		    return;
	   end if;
	
	  	--Check if already associated
	 	OPEN fetch_cursor for 
			select * from institution_fee_group_association assoc 
			where assoc.fee_id = fee_due_record.id and group_id = group_record.id limit 1;
			FETCH fetch_cursor INTO fee_association_record;
		close fetch_cursor;	
		
		if fee_association_record.id is not null
	    then
	    	return_code = 0;
	    	return_message = 'Already associated';
	    	association_id = fee_association_record.id;
		    return;
	    end if;
	   
	   
	   --Associate now
	   
	   insert
		into
		public.institution_fee_group_association (date_created,
		fee_id,
		group_id,
		applied,
		created_by)
		values(now(),
		fee_due_record.id,
		group_record.id,
		false,
		in_user_id);

	
	    -- if this fee has current child fee which is active, then associate it with the group too but with applied false
	    select * from institution_fees_due ifd where parent_fee_id = fee_due_record.id and ifd.effective_date<=current_date and ifd.end_date>=current_date limit 1 into active_child_fee_record;
	   	if active_child_fee_record.id is not null
	    	then
	    	insert
			into
			public.institution_fee_group_association (date_created,
			fee_id,
			group_id,
			applied,
			created_by)
			values(now(),
			active_child_fee_record.id,
			group_record.id,
			false,
			in_user_id);
		end if;
	    
	   
	   	return_code = 0;
	    return_message = 'Associated';
	    association_id = fee_association_record.id;
	
end;
$function$
;


CREATE OR REPLACE FUNCTION public.apply_fee_to_student(this_student_id bigint, this_fee_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_school_id int8;
    this_student_payment_code int8;

    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    this_student_end_balance numeric;
    this_student_account_id int8;
    this_fee_association_id int8;
    new_outstanding_balance numeric;

    fee_record record;

    this_existing_association_id int8;
    this_existing_association_applied boolean;

    this_fee_start_date date;
    this_penalty_next_apply_date date;
		
		fee_amount numeric;
		narration varchar;
		consession_record numeric;
		concession_amount numeric;
		
		consession_remainder numeric;
	the_next_reminder_date date;

begin

    SELECT student_code into this_student_payment_code FROM core_student WHERE ID = this_student_id;

    IF this_student_payment_code IS NULL THEN
        RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id ;
        RETURN FALSE ;
    end if;


    -- Get the fee things select * from institution_fees_due
    SELECT
        *
    INTO fee_record
    FROM
        institution_fees_due
    WHERE
            ID = this_fee_id ;


    if fee_record.id IS NULL THEN
        --Fee record does not exist
        RAISE EXCEPTION 'SPECIFIED FEE % WAS NOT FOUND', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.recurrent = true THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS RECURRENT AND HENCE CANNOT BE APPLIED DIRECTLY TO STUDENTS', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.approval_status = false THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS NOT YET APPROVED', this_fee_id;
    end if;



--If fee due date is past, then this_fee_start_date should be today
    if fee_record.effective_date < current_date then
        this_fee_start_date = current_date;
    else
        this_fee_start_date = fee_record.effective_date;
    end if;


    -- Select the student account id
    SELECT
        student_account_id,
        school_id INTO this_student_account_id,
        this_student_school_id
    FROM
        core_student
    WHERE
            ID = this_student_id ;


    -- School for fee and student should be the same

    IF fee_record.school_id <> this_student_school_id THEN
        RAISE EXCEPTION 'THE SCHOOL FOR THE FEE AND STUDENT MUST BE THE SAME';
    END
        IF ;


    -- Check if fee is exempted for this
    IF EXISTS (SELECT 1
               FROM
                   fees_due_student_exemption
               WHERE
                       fee_id = fee_record.id
                 AND student_number = this_student_payment_code
        ) THEN
        RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE %',
            this_student_id,
            fee_record.id;
        return false;
    END
        IF ;


    --if fee is child of recurrent, check for exemption from parent fee
    if fee_record.child_of_recurrent then
        IF EXISTS (SELECT 1 FROM fees_due_student_exemption WHERE fee_id = fee_record.parent_fee_id AND student_number = this_student_payment_code )
        THEN
            RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE''s PARENT FEE %',
                this_student_id,
                fee_record.id;
            return false;
        END
            IF ;
    end if;

		fee_amount = fee_record.due_amount;
		narration ='FEE DUE - ' || fee_record.description;
		
		
	--check if fee has concessions;
		SELECT
        *
    INTO consession_record
    FROM
        fee_concessions
    WHERE
    fee_id = this_fee_id ;
		
		
		    -- Check if student has consession for this
    IF EXISTS (SELECT 1
               FROM
                   student_fee_concessions
               WHERE
                       fee_id = fee_record.id
                 AND student_id = this_student_id
        ) THEN
        
				--compute amount to pay
				select fc.percentage  INTO consession_record from fee_concessions  fc INNER JOIN student_fee_concessions sfc on sfc.concessions_id = fc.id where sfc.student_id =this_student_id and fc.fee_id =fee_record.id;
				concession_amount= fee_amount *(consession_record/100);
			fee_amount = fee_amount-concession_amount;
			
			consession_remainder = 100-consession_record;
			narration= consession_remainder||'% FEE DUE - ' || fee_record.description;
			
			
				
    END  IF ;
		
	
	
		
   
      
   --if fee is child of recurrent, check for exemption from parent fee
   if fee_record.child_of_recurrent then
   		IF EXISTS (SELECT 1 FROM student_fee_concessions WHERE fee_id = fee_record.parent_fee_id AND student_id = this_student_id ) 
   		THEN
     
				--compute amount to pay
				select fc.percentage  INTO consession_record from fee_concessions  fc INNER JOIN student_fee_concessions sfc on sfc.concessions_id = fc.id where sfc.student_id =this_student_id and fc.fee_id = fee_record.parent_fee_id;
				
				concession_amount= fee_amount *(consession_record/100);
			fee_amount = fee_amount-concession_amount;
			narration= consession_record||'% FEE DUE - ' || fee_record.description;

			
			
    END
    IF ;
   end if;
  
  
  --check if fee has reminders
   

if fee_record.enable_reminders then
	the_next_reminder_date = fee_record.effective_date +  (fee_record.send_msg_after_effective_date_in_days||' day')::interval;


end if;


    -- Get possible existing record
    select id, applied  into this_existing_association_id , this_existing_association_applied
    from institution_fee_student_association
    WHERE
            fee_id = this_fee_id
      AND student_id = this_student_id;

    -- Check if fee is already applied to this student
    IF this_existing_association_applied = TRUE
    THEN
        RAISE NOTICE 'THIS FEE % IS ALREADY APPLIED TO THIS STUDENT %',
            fee_record.id,
            this_student_id ;
        return false;
    END IF;
   
   
   
   


    -- IF FEE EFFECTIVE DATE IS IN FUTURE
    -- CHECK IF ALREADY BOOKED AND RETURN
    -- ELSE BOOK AND RETURN
    IF fee_record.effective_date > current_date
    THEN

        if this_existing_association_id is not null and this_existing_association_applied = false
        then
            RAISE NOTICE 'THIS FEE % IS ALREADY BOOKED TO A FUTURE DATE FOR THIS STUDENT %',
                this_fee_id,
                this_student_id ;
            return false;
        end if;

        -- Book fee now and return
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id,
            number_sent_reminders,
            next_reminder_date,
            reminders_closed
        )
        VALUES
        (
            now(),
            fee_record.id,
            this_student_id,
            FALSE,  --Applied as false
            web_user_name,
            now(),
            fee_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id,
           
            0,
            the_next_reminder_date,
            false
        ) RETURNING ID INTO this_fee_association_id ;

        RAISE NOTICE 'THIS FEE % HAS NOW BEEN BOOKED FOR THIS STUDENT %',
            fee_record.id,
            this_student_id ;

        return true;

    END IF;



    -- Get the current balance for UPDATE
    SELECT
        account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
    FROM
        student_account_gl
    WHERE
            ID = this_student_account_id FOR SHARE ;

    -- Adjust the new balance
    this_student_end_balance = this_student_current_balance - fee_amount;
    new_outstanding_balance = this_student_current_outstanding_balance - fee_amount;

    IF new_outstanding_balance > 0 THEN
        new_outstanding_balance = 0;
    END IF;

    -- If penalty, we are to save the penalty apply date
    -- Compute this from the this_fee_start_date - which can be effective date or current date depending on whether fee effective date is past
    this_penalty_next_apply_date = null;
    if fee_record.has_penalty = true
    then
        if fee_record.arrears_after_units = 'D'
        then
            this_penalty_next_apply_date = fee_record.effective_date + interval '1 day' * fee_record.arrears_after_count;
        elsif fee_record.arrears_after_units = 'W' then
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 week' * fee_record.arrears_after_count;
        else
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 month' * fee_record.arrears_after_count;
        end if;
    end if;




    -- Create association now select now() + interval '1 day' *5format('+ %s day', this_fee_arrears_after_count);
    -- IF association exists, just update it to applied, and set the next penanly apply date else create it
    if this_existing_association_id is not null and this_existing_association_applied = false
    then
        update institution_fee_student_association set applied = true,next_penalty_apply_date=this_penalty_next_apply_date  where id = this_existing_association_id;
        --Set the assoc id to the existing record for future use
        this_fee_association_id = this_existing_association_id;
    else
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id,
            number_sent_reminders,
            next_reminder_date,
            reminders_closed
        )
        VALUES
        (
            now(),
            this_fee_id,
            this_student_id,
            TRUE,
            web_user_name,
            now(),
            fee_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id,
             0,
            the_next_reminder_date,
        
            false
        ) RETURNING ID INTO this_fee_association_id ;
    end if;



    IF this_student_end_balance < 0 THEN
        new_outstanding_balance = this_student_end_balance;
    else
        new_outstanding_balance = 0;
        --if the student end balance is greater than zero or eqaul to zero, then it means that the fee has applied while student has sufficient balance
        -- then update the association record and close the penalty
        update institution_fee_student_association set penalties_closed = true, fee_outstanding_balance=0 where id = this_fee_association_id ;
    END IF;


    -- update balance
    UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = this_student_account_id;

    -- Create th record
    INSERT INTO student_account_transaction_history (
        date_created,
        amount,
        posting_user,
        description,
        fee_association_id,
        payment_id,
        trans_type,
        account_id,
        reversal_flag,
        reversed,
        date_reversed,
        balance_after,
        associated_fee_due,
        outstanding_balance_after
    )
    VALUES
    (
        now(),
        - 1 * fee_amount,
        web_user_name,
        narration,
        this_fee_association_id,
        NULL,
        'FEE DUE INV.',
        this_student_account_id,
        FALSE,
        FALSE,
        NULL,
        this_student_end_balance,
        fee_record.id,
        new_outstanding_balance
    ) ;


    INSERT INTO institution_fees_due_student_payment_transaction_history
    ("date_created",
     "payment_id",
     "fee_id",
     "fee_student_association_id",
     "student_id",
     "amount",
     "balance_before",
     "balance_after",
     "description"
    )
    VALUES (
               now(),
               0,
               fee_record.id,
               this_fee_association_id,
               this_student_id,
               fee_amount*-1,
               0,
               fee_amount*-1,
               format('FEE APPLIED')
           );

    -- Log now
    INSERT INTO web_console_log (
        ip_address,
        user_name,
        event_date,
        event_action,
        affected_student_id,
        errors,
        web_user_id
    )
    VALUES
    (
        ip_address,
        web_user_name,
        now(),
        'Apply fee ' || fee_record.id,
        this_student_id,
        '',
        web_user_id
    ) ;
    RETURN true;
end;
$function$
;


CREATE OR REPLACE FUNCTION public.apply_fee_to_student0(this_student_id bigint, this_fee_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_school_id int8;
    this_student_payment_code int8;

    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    this_student_end_balance numeric;
    this_student_account_id int8;
    this_fee_association_id int8;
    new_outstanding_balance numeric;

    fee_record record;

    this_existing_association_id int8;
    this_existing_association_applied boolean;

    this_fee_start_date date;
    this_penalty_next_apply_date date;

begin

    SELECT student_code into this_student_payment_code FROM core_student WHERE ID = this_student_id;

    IF this_student_payment_code IS NULL THEN
        RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id ;
        RETURN FALSE ;
    end if;


    -- Get the fee things select * from institution_fees_due
    SELECT
        *
    INTO fee_record
    FROM
        institution_fees_due
    WHERE
            ID = this_fee_id ;


    if fee_record.id IS NULL THEN
        --Fee record does not exist
        RAISE EXCEPTION 'SPECIFIED FEE % WAS NOT FOUND', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.recurrent = true THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS RECURRENT AND HENCE CANNOT BE APPLIED DIRECTLY TO STUDENTS', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.approval_status = false THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS NOT YET APPROVED', this_fee_id;
    end if;



--If fee due date is past, then this_fee_start_date should be today
    if fee_record.effective_date < current_date then
        this_fee_start_date = current_date;
    else
        this_fee_start_date = fee_record.effective_date;
    end if;


    -- Select the student account id
    SELECT
        student_account_id,
        school_id INTO this_student_account_id,
        this_student_school_id
    FROM
        core_student
    WHERE
            ID = this_student_id ;


    -- School for fee and student should be the same

    IF fee_record.school_id <> this_student_school_id THEN
        RAISE EXCEPTION 'THE SCHOOL FOR THE FEE AND STUDENT MUST BE THE SAME';
    END
        IF ;


    -- Check if fee is exempted for this
    IF EXISTS (SELECT 1
               FROM
                   fees_due_student_exemption
               WHERE
                       fee_id = fee_record.id
                 AND student_number = this_student_payment_code
        ) THEN
        RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE %',
            this_student_id,
            fee_record.id;
        return false;
    END
        IF ;


    --if fee is child of recurrent, check for exemption from parent fee
    if fee_record.child_of_recurrent then
        IF EXISTS (SELECT 1 FROM fees_due_student_exemption WHERE fee_id = fee_record.parent_fee_id AND student_number = this_student_payment_code )
        THEN
            RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE''s PARENT FEE %',
                this_student_id,
                fee_record.id;
            return false;
        END
            IF ;
    end if;


    -- Get possible existing record
    select id, applied  into this_existing_association_id , this_existing_association_applied
    from institution_fee_student_association
    WHERE
            fee_id = this_fee_id
      AND student_id = this_student_id;

    -- Check if fee is already applied to this student
    IF this_existing_association_applied = TRUE
    THEN
        RAISE NOTICE 'THIS FEE % IS ALREADY APPLIED TO THIS STUDENT %',
            fee_record.id,
            this_student_id ;
        return false;
    END IF;


    -- IF FEE EFFECTIVE DATE IS IN FUTURE
    -- CHECK IF ALREADY BOOKED AND RETURN
    -- ELSE BOOK AND RETURN
    IF fee_record.effective_date > current_date
    THEN

        if this_existing_association_id is not null and this_existing_association_applied = false
        then
            RAISE NOTICE 'THIS FEE % IS ALREADY BOOKED TO A FUTURE DATE FOR THIS STUDENT %',
                this_fee_id,
                this_student_id ;
            return false;
        end if;

        -- Book fee now and return
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id
        )
        VALUES
        (
            now(),
            fee_record.id,
            this_student_id,
            FALSE,  --Applied as false
            web_user_name,
            now(),
            fee_record.due_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id
        ) RETURNING ID INTO this_fee_association_id ;

        RAISE NOTICE 'THIS FEE % HAS NOW BEEN BOOKED FOR THIS STUDENT %',
            fee_record.id,
            this_student_id ;

        return true;

    END IF;



    -- Get the current balance for UPDATE
    SELECT
        account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
    FROM
        student_account_gl
    WHERE
            ID = this_student_account_id FOR SHARE ;

    -- Adjust the new balance
    this_student_end_balance = this_student_current_balance - fee_record.due_amount;
    new_outstanding_balance = this_student_current_outstanding_balance - fee_record.due_amount;

    IF new_outstanding_balance > 0 THEN
        new_outstanding_balance = 0;
    END IF;

    -- If penalty, we are to save the penalty apply date
    -- Compute this from the this_fee_start_date - which can be effective date or current date depending on whether fee effective date is past
    this_penalty_next_apply_date = null;
    if fee_record.has_penalty = true
    then
        if fee_record.arrears_after_units = 'D'
        then
            this_penalty_next_apply_date = fee_record.effective_date + interval '1 day' * fee_record.arrears_after_count;
        elsif fee_record.arrears_after_units = 'W' then
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 week' * fee_record.arrears_after_count;
        else
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 month' * fee_record.arrears_after_count;
        end if;
    end if;




    -- Create association now select now() + interval '1 day' *5format('+ %s day', this_fee_arrears_after_count);
    -- IF association exists, just update it to applied, and set the next penanly apply date else create it
    if this_existing_association_id is not null and this_existing_association_applied = false
    then
        update institution_fee_student_association set applied = true,next_penalty_apply_date=this_penalty_next_apply_date  where id = this_existing_association_id;
        --Set the assoc id to the existing record for future use
        this_fee_association_id = this_existing_association_id;
    else
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id
        )
        VALUES
        (
            now(),
            this_fee_id,
            this_student_id,
            TRUE,
            web_user_name,
            now(),
            fee_record.due_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id
        ) RETURNING ID INTO this_fee_association_id ;
    end if;



    IF this_student_end_balance < 0 THEN
        new_outstanding_balance = this_student_end_balance;
    else
        new_outstanding_balance = 0;
        --if the student end balance is greater than zero or eqaul to zero, then it means that the fee has applied while student has sufficient balance
        -- then update the association record and close the penalty
        update institution_fee_student_association set penalties_closed = true, fee_outstanding_balance=0 where id = this_fee_association_id ;
    END IF;


    -- update balance
    UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = this_student_account_id;

    -- Create th record
    INSERT INTO student_account_transaction_history (
        date_created,
        amount,
        posting_user,
        description,
        fee_association_id,
        payment_id,
        trans_type,
        account_id,
        reversal_flag,
        reversed,
        date_reversed,
        balance_after,
        associated_fee_due,
        outstanding_balance_after
    )
    VALUES
    (
        now(),
        - 1 * fee_record.due_amount,
        web_user_name,
        'FEE DUE - ' || fee_record.description,
        this_fee_association_id,
        NULL,
        'FEE DUE INV.',
        this_student_account_id,
        FALSE,
        FALSE,
        NULL,
        this_student_end_balance,
        fee_record.id,
        new_outstanding_balance
    ) ;


    INSERT INTO institution_fees_due_student_payment_transaction_history
    ("date_created",
     "payment_id",
     "fee_id",
     "fee_student_association_id",
     "student_id",
     "amount",
     "balance_before",
     "balance_after",
     "description"
    )
    VALUES (
               now(),
               0,
               fee_record.id,
               this_fee_association_id,
               this_student_id,
               fee_record.due_amount*-1,
               0,
               fee_record.due_amount*-1,
               format('FEE APPLIED')
           );

    -- Log now
    INSERT INTO web_console_log (
        ip_address,
        user_name,
        event_date,
        event_action,
        affected_student_id,
        errors,
        web_user_id
    )
    VALUES
    (
        ip_address,
        web_user_name,
        now(),
        'Apply fee ' || fee_record.id,
        this_student_id,
        '',
        web_user_id
    ) ;
    RETURN true;
end;
$function$
;


CREATE OR REPLACE FUNCTION public.apply_student_penalty(in_fee_student_association_id bigint)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare





    record_student          json;

    cursor_fee_association     cursor for

        select student.id as student_id, student.student_account_id, penalty_type, penalty_frequency,

               student.payment_code , penalty_amount, fee_outstanding_balance, last_penalty_apply_date,

               next_penalty_apply_date, penalties_closed, penalties_outstanding_balance, assoc.id as associd,

               penalty_apply_frequency_unit, penalty_apply_frequency_term,  fee.description as fee_description,

               fee.id as fee_id, penalty_expiry_date, maximum_penalty_amount, maximum_penalty_application_count,

               assoc.penalty_application_count, student_gl.account_balance as student_account_balance

        from institution_fee_student_association assoc

                 inner join core_student student on assoc.student_id=student.id

                 inner join institution_fees_due fee on assoc.fee_id=fee.id

                 inner join institution_fees_due_penalty penalty on fee.penalty_id = penalty.id

 inner join student_account_gl student_gl on student.student_account_id = student_gl.id

        where assoc.id = in_fee_student_association_id

          and assoc.penalties_closed=false

          and assoc.next_penalty_apply_date <= current_date

          and fee_outstanding_balance < 0

          and assoc.has_penalty = true;



    --temps

    temp_amount              numeric;

    balance_before              numeric;

    balance_after              numeric;

    this_penalty_next_apply_date date;

    record_fee_association record;

    this_student_current_balance numeric;

    this_student_current_outstanding_balance numeric;

    penalty_applied boolean = false;

    this_student_end_balance numeric;

    new_outstanding_balance numeric;



begin

    -- Open row cursor

    open cursor_fee_association;

    loop

        fetch cursor_fee_association into record_fee_association;

        exit when not found;



        --if penalty expiry date has passed or penalty_amount for this record has been exceeded

        -- then close the penalty and continue

        IF cast(record_fee_association.penalty_expiry_date as date) < current_date

        then

            raise notice 'Penalty with id % has reached the expiry date', record_fee_association.associd;

            penalty_applied = false;

            UPDATE institution_fee_student_association set

                penalties_closed = true

            where id = record_fee_association.associd;

            continue;

        end if;



        -- if record_fee_association.maximum_penalty_amount <> 0 and penalty has exceeded limit them leave it

        IF abs(record_fee_association.penalties_outstanding_balance)>=abs(record_fee_association.maximum_penalty_amount) and record_fee_association.maximum_penalty_amount <>0

        then

            raise notice 'Penalty with id % has reached the maximum penalty amount constraints', record_fee_association.associd;

            penalty_applied = false;

            UPDATE institution_fee_student_association set

                penalties_closed = true

            where id = record_fee_association.associd;

            continue;

        end if;



        -- if record_fee_association.maximum_penalty_application_count <> 0 and penalty application count has exceeded the maximum

        IF abs(record_fee_association.penalty_application_count)>=abs(record_fee_association.maximum_penalty_application_count) and record_fee_association.maximum_penalty_application_count <>0

        then

            raise notice 'Penalty with id % has reached the maximum number of applications', record_fee_association.associd;

            penalty_applied = false;

            UPDATE institution_fee_student_association set

                penalties_closed = true

            where id = record_fee_association.associd;

            continue;

        end if;



if record_fee_association.student_account_balance >= 0

then

-- Student has a penalty but for some reason the balance is not negative

-- This is an opportunity to block further penalties

 raise notice 'Penalty fee with id % is being skipped due to positive student balance', record_fee_association.associd;

UPDATE institution_fee_student_association set

                penalties_closed = true

            where id = record_fee_association.associd;

            continue;

end if;





        IF record_fee_association.penalty_type = 'FIXED'

        then

            temp_amount = record_fee_association.penalty_amount;

        else

            --Compute as percantage of outstanding

            temp_amount = (record_fee_association.penalty_amount * abs(record_fee_association.fee_outstanding_balance)) / 100.0;

        end if;









        --Adjust and set balance to balance minus adjustment amount

        UPDATE institution_fee_student_association set

            penalties_outstanding_balance = penalties_outstanding_balance - temp_amount

        where id = record_fee_association.associd;



        -- if fixed penalty

        if record_fee_association.penalty_frequency = 'ONETIME'

        then

            UPDATE institution_fee_student_association set

                penalties_closed = true

            where id = record_fee_association.associd;

        end if;







        -- Create fee performance th record

        INSERT INTO institution_fees_due_student_payment_transaction_history

        ("date_created",

         "payment_id",

         "fee_id",

         "fee_student_association_id",

         "student_id",

         "amount",

         "balance_before",

         "balance_after",

         "description",

         "penalty_amount",

         "penalty_balance_before",

         "penalty_balance_after"

        )

        VALUES (

                   now(),

                   0,

                   record_fee_association.fee_id,

                   record_fee_association.associd,

                   record_fee_association.student_id,

                   0,

                   record_fee_association.fee_outstanding_balance, -- balances havent changed

                   record_fee_association.fee_outstanding_balance, -- balances havent changed

                   format('PENALTY - %s', temp_amount),

                   temp_amount * -1,

                   record_fee_association.penalties_outstanding_balance,

                   record_fee_association.penalties_outstanding_balance - temp_amount

               );









        -- Updating student balances too

        SELECT

            account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance

        FROM

            student_account_gl

        WHERE

                ID = record_fee_association.student_account_id FOR SHARE ;



        -- Adjust the new balance

        this_student_end_balance = this_student_current_balance - temp_amount;

        new_outstanding_balance = this_student_current_outstanding_balance - temp_amount;



        IF new_outstanding_balance > 0 THEN

            new_outstanding_balance = 0;

        END IF;





        -- update balance

        UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = record_fee_association.student_account_id;



        -- Create th record

        INSERT INTO student_account_transaction_history (

            date_created,

            amount,

            description,

            payment_id,

            trans_type,

            account_id,

            reversal_flag,

            reversed,

            date_reversed,

            balance_after,

            associated_fee_due,

            outstanding_balance_after,

            fee_association_id

        )

        VALUES

        (

            now(),

            - 1 * temp_amount,

            'PENALTY ON - ' || record_fee_association.fee_description,

            NULL,

            'PENALTY',

            record_fee_association.student_account_id,

            FALSE,

            FALSE,

            NULL,

            this_student_end_balance,

            record_fee_association.fee_id,

            new_outstanding_balance,

            record_fee_association.associd

        ) ;









        -- Adjust next apply date

        -- If penalty, we are to save the penalty apply date

        this_penalty_next_apply_date = null;

        if record_fee_association.penalty_apply_frequency_unit = 'D'

        then

            this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 day' * record_fee_association.penalty_apply_frequency_term;

        elsif record_fee_association.penalty_apply_frequency_unit = 'W' then

            this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 week' * record_fee_association.penalty_apply_frequency_term;

        else

            this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 month' * record_fee_association.penalty_apply_frequency_term;

        end if;



        -- update next apply date, last apply date to today select * from institution_fee_student_association

        update institution_fee_student_association set last_penalty_apply_date = current_date, next_penalty_apply_date = this_penalty_next_apply_date,penalties_accrued=(penalties_accrued + abs(temp_amount)),penalty_application_count=penalty_application_count+1  where id = record_fee_association.associd;







        raise notice 'Applied penalty for association id  % ', record_fee_association.associd;

        penalty_applied = true;



    end loop;

    close cursor_fee_association;





    if penalty_applied = false

    then

        RETURN row_to_json(data) FROM ( VALUES (0, 'No penalty to apply was found')) data (returncode, returnmessage);

    end if;



    RETURN row_to_json(data) FROM ( VALUES (0, 'Penalty applied')) data (returncode, returnmessage);

end;

$function$
;


CREATE OR REPLACE FUNCTION public.apportion_payment_amount_to_fees(in_student_id bigint, in_payment_id bigint, in_payment_amount numeric)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare





    record_student          json;

    record_association          json;

    cursor_fee_association     cursor for

        select assoc.* from institution_fee_student_association assoc

                                inner join institution_fees_due fee on assoc.fee_id  = fee.id

        where student_id = in_student_id and fee_outstanding_balance<0 order by fee.priority, fee.id;

    record_fee_association record;



    --temps

    temp_amount              numeric;

    temp_balance              numeric;

    balance_before              numeric;

    balance_after              numeric;

    payment_date date;

		student_account_balance numeric;



begin

    -- Get student

    select get_record_by_int8(in_student_id, 'core_student', 'id')

    into record_student;

    IF (record_student ->> 'id') IS NULL

    THEN

        -- There was a problem getting the school

        RETURN row_to_json(data) FROM ( VALUES (199, 'Student was not found')) data (returncode, returnmessage);

    END IF;





    IF NOT EXISTS (select 1 from institution_fee_student_association where student_id = in_student_id and fee_outstanding_balance<0)

    then

        -- No schedules updatable

        RETURN row_to_json(data) FROM ( VALUES (0, 'No outstanding updatable fees for this student')) data (returncode, returnmessage);

    end if;



    if in_payment_amount <= 0 then

        RETURN row_to_json(data) FROM ( VALUES (100, 'Payment amount can not be less than 0')) data (returncode, returnmessage);

    end if;





    -- Open row cursor

    temp_balance := in_payment_amount;

    open cursor_fee_association;

    loop

        fetch cursor_fee_association into record_fee_association;

        exit when not found;

        exit when temp_balance <= 0;



        --get the payment date

        select date_created into payment_date from payments_received where id = in_payment_id;





        balance_before = record_fee_association.fee_outstanding_balance;



        temp_amount := abs(record_fee_association.fee_outstanding_balance); --the amount to apportion



        if temp_balance < temp_amount then

            -- Amount remaining is less than fee balance

            temp_amount = temp_balance;

        end if;



        balance_after = balance_before + temp_amount;





        --Adjust and set balance to balance minus adjustment amount

        UPDATE institution_fee_student_association set

            fee_outstanding_balance = fee_outstanding_balance + temp_amount

        where id = record_fee_association.id;



        -- If amount for this fee has been paid in full, set penalties closed as true

        UPDATE institution_fee_student_association set

            penalties_closed = true

        where id = record_fee_association.id and fee_outstanding_balance>=0;



        temp_balance := temp_balance - temp_amount;



        -- Create fee performance th record

        -- select * from institution_fee_student_association

        -- select * from institution_fees_due_student_payment_transaction_history





        INSERT INTO institution_fees_due_student_payment_transaction_history

        ("date_created",

         "payment_id",

         "fee_id",

         "fee_student_association_id",

         "student_id",

         "amount",

         "balance_before",

         "balance_after",

         "description",

         "penalty_amount",

         "penalty_balance_before",

         "penalty_balance_after"

        )

        VALUES (

                   payment_date,

                   in_payment_id,

                   record_fee_association.fee_id,

                   record_fee_association.id,

                   record_fee_association.student_id,

                   temp_amount,

                   balance_before,

                   balance_after,

                   format('PAYMENT MADE - %s', temp_amount),

                   0,

                   record_fee_association.penalties_outstanding_balance,

                   record_fee_association.penalties_outstanding_balance

               );









        -- Now also apportion to penalty if theres a penalty

        -- if no penalty, continue the loop

        IF record_fee_association.has_penalty = false

        then

            raise notice 'Association has no penalty';

            continue;

        end if;



        IF record_fee_association.penalties_outstanding_balance >= 0

        then

            raise notice 'No penalty outstanding for this association';

            continue;

        end if;





        -- At this point there's a penalty to be applied



        balance_before = record_fee_association.penalties_outstanding_balance;



        temp_amount := abs(record_fee_association.penalties_outstanding_balance); --the amount to apportion



        if temp_balance < temp_amount then

            -- Amount remaining is less than fee balance

            temp_amount = temp_balance;

        end if;



        balance_after = balance_before + temp_amount;





        --Adjust and set balance to balance minus adjustment amount

        UPDATE institution_fee_student_association set

            penalties_outstanding_balance = penalties_outstanding_balance + temp_amount

        where id = record_fee_association.id;



        temp_balance := temp_balance - temp_amount;



        -- Create fee performance th record

        -- select * from institution_fee_student_association

        -- select * from institution_fees_due_student_payment_transaction_history



        -- This record will help us get the fee balances for penalty th update

        select get_record_by_int8(record_fee_association.id, 'institution_fee_student_association', 'id')

        into record_association;



        INSERT INTO institution_fees_due_student_payment_transaction_history

        ("date_created",

         "payment_id",

         "fee_id",

         "fee_student_association_id",

         "student_id",

         "amount",

         "balance_before",

         "balance_after",

         "description",

         "penalty_amount",

         "penalty_balance_before",

         "penalty_balance_after"

        )

        VALUES (

                   payment_date,

                   in_payment_id,

                   record_fee_association.fee_id,

                   record_fee_association.id,

                   record_fee_association.student_id,

                   temp_amount, -- amt is zero record_student ->> 'id'

                   cast((record_association->>'fee_outstanding_balance') as numeric),

                   cast((record_association->>'fee_outstanding_balance') as numeric),

                   format('PENALTY PAYMENT - %s', temp_amount),

                   0,

                   balance_before,

                   balance_after

               );











        raise notice 'Apportioned % to fee association with id ', record_fee_association.id;

				

				-- Get the student account balance, if its greater than or equal to zero,

				-- Set all fees associations as closed and balances to zero

				select account_balance into student_account_balance from student_account_gl where id = (select student_account_id from student_information where id = in_student_id);

				if student_account_balance >=0 then

				 raise notice 'Since student balance is zero, clearing outstanding balances as well now';

				update institution_fee_student_association set fee_outstanding_balance=0, penalties_outstanding_balance=0, penalties_closed = true where student_id = in_student_id and fee_outstanding_balance<0;

				end if;

				



    end loop;

    close cursor_fee_association;







    RETURN row_to_json(data) FROM ( VALUES (0, 'Fees updated')) data (returncode, returnmessage);

end;

$function$
;


CREATE OR REPLACE FUNCTION public.apportion_payment_amount_to_plans(in_student_id bigint, in_payment_amount bigint)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare


  record_student          json;
  cursor_schedule     cursor for
    select * from payment_plan_student_payment_schedule where student_id = in_student_id and schedule_balance>0 order by id;
  record_schedule record;

  --temps
  temp_amount              int8;
  temp_balance              int8;

begin
  -- Get student
  select get_record_by_int8(in_student_id, 'student_information', 'id')
  into record_student;
  IF (record_student ->> 'id') IS NULL
  THEN
    -- There was a problem getting the school
    RETURN row_to_json(data) FROM ( VALUES (199, 'Student was not found')) data (returncode, returnmessage);
  END IF;


  IF NOT EXISTS (select 1 from payment_plan_student_payment_schedule where student_id = in_student_id and schedule_balance>0)
    then
    -- No schedules updatable
    RETURN row_to_json(data) FROM ( VALUES (0, 'No outstanding updatable schedules')) data (returncode, returnmessage);
  end if;

  if in_payment_amount <= 0 then
    RETURN row_to_json(data) FROM ( VALUES (100, 'Payment amount can not be less than 0')) data (returncode, returnmessage);
  end if;


  -- Open row cursor
  temp_balance := in_payment_amount;
  open cursor_schedule;
  loop
    fetch cursor_schedule into record_schedule;
    exit when not found;
    exit when temp_balance <= 0;

    temp_amount := record_schedule.schedule_balance; --the amount to apportion

    if temp_balance < temp_amount then
      -- Amount remaining is less than schedule balance
      temp_amount = temp_balance;
    end if;

    --Adjust and set balance to balance minus adjustment amount
    UPDATE payment_plan_student_payment_schedule set
      schedule_balance = schedule_balance - temp_amount,
      schedule_paid_so_far = schedule_paid_so_far + temp_amount
      where id = record_schedule.id;

    temp_balance := temp_balance - temp_amount;

    raise notice '%', record_schedule.description;

  end loop;
  close cursor_schedule;



  RETURN row_to_json(data) FROM ( VALUES (0, 'Schedules updated')) data (returncode, returnmessage);
end;
$function$
;
CREATE OR REPLACE FUNCTION public.archive_student(this_student_id bigint, this_reason_for_archive character varying, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$



declare

this_student_school_id int8;

this_student_current_class_id int8;

this_student_new_class_id int8;

current_archive_status boolean;





begin



		if NOT exists (select 1 from core_student where id = this_student_id) THEN

			RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id;

				return false;

		end if;





		-- Select the student class ID

		SELECT class_id, school_id, archived INTO this_student_current_class_id, this_student_school_id, current_archive_status FROM core_student where id = this_student_id;



		IF current_archive_status = true THEN

			RAISE NOTICE 'STUDENT % IS ALREADY ARCHIVED' , this_student_id;

			RETURN false;

		END IF;



		-- Check if this school as an archive class 

		SELECT id into this_student_new_class_id FROM core_school_class WHERE school_id = this_student_school_id AND class_code = '__ARCHIVE__';

		IF this_student_new_class_id IS NULL THEN

		RAISE NOTICE 'CREATING NON EXISTENT ARCHIVE CLASS FOR SCHOOL %', this_student_school_id;

		INSERT INTO core_school_class (school_id, class_code, class_description, date_created) 

			values(this_student_school_id, '__ARCHIVE__', 'Archive Class', now()) RETURNING id into this_student_new_class_id;

		END IF;



		-- LEts update now

		update core_student set class_id = this_student_new_class_id , active = FALSE,

		archived = true, date_archived=now(), archive_reason=this_reason_for_archive, class_when_archived = this_student_current_class_id

		where id = this_student_id;

		



		-- Log now

		INSERT INTO web_console_log(

            ip_address, user_name, event_date, event_action, affected_student_id, 

            errors, web_user_id)

    VALUES (ip_address, web_user_name, now(), 

		'Archive student ', this_student_id, 

            '', web_user_id);

		RETURN true;

end;

$function$
;

CREATE OR REPLACE FUNCTION public.archive_user(this_user_id bigint, this_reason_for_archive character varying, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
current_archive_status boolean;


begin

		if NOT exists (select 1 from "user" where id = this_user_id) THEN
			RAISE NOTICE 'USER WITH ID % NOT FOUND', this_user_id;
				return false;
		end if;


	

		IF current_archive_status = true THEN
			RAISE NOTICE 'USER % IS ALREADY ARCHIVED' , this_user_id;
			RETURN false;
		END IF;

		-- LEts update now
		update "user" set archived = true, date_archived=now(), archive_reason=this_reason_for_archive
		where id = this_user_id;
		

		-- Log now
		INSERT INTO web_console_log(
            ip_address, user_name, event_date, event_action, affected_student_id, 
            errors, web_user_id)
    VALUES (ip_address, web_user_name, now(), 
		'Archive user ', this_user_id, 
            '', web_user_id);
		RETURN true;
end;
$function$
;

CREATE OR REPLACE FUNCTION public.associate_school_with_default_payment_channels(this_school_id numeric)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$



declare

payment_channel_id numeric;

school_bank_id numeric;

begin



		-- Get the school bank id

		SELECT bank_name into school_bank_id from core_school where id = this_school_id;



		FOR payment_channel_id IN

      SELECT payment_channel from bank_payment_channel_association WHERE bank_id = school_bank_id

   LOOP

      --apply school to payment channel

				PERFORM associate_school_with_payment_channel(this_school_id, payment_channel_id);

   END LOOP;

		RETURN true;

end;

$function$
;

CREATE OR REPLACE FUNCTION public.associate_school_with_payment_channel(this_school_id numeric, this_payment_channel_id numeric)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$



declare



begin

		IF NOT EXISTS (SELECT 1 FROM core_school WHERE id = this_school_id) THEN

			RAISE NOTICE 'SCHOOL NOT FOUND';

			return false;

		END IF;



		IF NOT EXISTS (SELECT 1 FROM payment_channels WHERE id = this_payment_channel_id) THEN

			RAISE NOTICE 'PAYMENT CHANNEL NOT FOUND';

			return false;	

		END IF;

		

		--Fetch the student account using payment code 

		if NOT exists (select 1 from institution_payment_channel_association where institution = this_school_id and payment_channel=this_payment_channel_id) THEN

				INSERT INTO institution_payment_channel_association(institution, payment_channel) VALUES(this_school_id, this_payment_channel_id);

		end if;



	

		RETURN true;

end;

$function$
;


CREATE OR REPLACE FUNCTION public.change_student_class(this_student_id bigint, this_new_class_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$



declare

this_student_school_id int8;

this_student_current_class_id int8;

this_student_current_class_description varchar;

this_student_new_class_description varchar;

this_student_new_class_school_id int8;



begin

		--Fetch the student account using payment code 

		if NOT exists (select 1 from core_school_class where id = this_new_class_id) THEN

				RAISE EXCEPTION 'NEW CLASS DOES NOT EXIST';

		end if;



		if NOT exists (select 1 from core_student where id = this_student_id) THEN

			RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id;

				return false;

		end if;





		-- Select the student class ID

		SELECT class_id, school_id INTO this_student_current_class_id, this_student_school_id FROM core_student where id = this_student_id;



		IF this_student_current_class_id = this_new_class_id THEN

			RETURN false;

		END IF;



		--Get the current class description for logging purposes

		SELECT class_description into this_student_current_class_description from core_school_class where id = this_student_current_class_id; 

		

		-- Get the new class details

		SELECT class_description, school_id into this_student_new_class_description, this_student_new_class_school_id from core_school_class where id = this_new_class_id; 



		if this_student_new_class_school_id <> this_student_school_id THEN

			RAISE EXCEPTION 'STUDENT CLASS SCHOOL MISMATCH';

		end if;



		-- LEts update now

		update core_student set class_id = this_new_class_id where id = this_student_id;

		



		-- Log now

		INSERT INTO web_console_log(

            ip_address, user_name, event_date, event_action, affected_student_id, 

            errors, web_user_id)

    VALUES (ip_address, web_user_name, now(), 

		'Change class from ' || this_student_current_class_description || ' to ' || this_student_new_class_description, this_student_id, 

            '', web_user_id);

		RETURN true;

end;

$function$
;


CREATE OR REPLACE FUNCTION public.check_applied_status_before_delete_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

BEGIN

		IF OLD.applied = true

		THEN

		RAISE EXCEPTION 'FEE CLASS ASSOCIATION CAN NOT BE UNDONE BECAUSE FEE ASSOCIATION HAS BEEN APPLIED TO ALL STUDENTS';

		END IF;

    RETURN OLD;

END;

$function$
;


CREATE OR REPLACE FUNCTION public.check_fee_and_class_school_for_association_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare 

fee_school_id int = null;
class_school_id int = null;

BEGIN
-- Get the school ids for the fee and CLASS
SELECT school_id INTO class_school_id FROM core_school_class where id = NEW.class_id;
SELECT school_id INTO fee_school_id FROM institution_fees_due where id = NEW.fee_id;

IF fee_school_id <> class_school_id THEN
-- fail since school id for class and fee aint the same
raise EXCEPTION 'FEE AND CLASS ASSOCIATIONS ARE ONLY POSSIBLE FOR A FEE AND CLASS BELONGING TO THE SAME SCHOOL'; 
END IF;
RETURN NEW;
END;

$function$
;


CREATE OR REPLACE FUNCTION public.create_payment_channel_account_on_payment_channel_creation_trig()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$



declare 



created_account_id int = null;







BEGIN



-- If account id is not passed then add it 







IF NEW.payment_channel_account_id IS NULL THEN



-- try to create the account id



INSERT INTO payment_channel_account_gl (description, balance)



VALUES (NEW.channel_name || ' - Payment Channel A/C', 999999999999999999) returning id into created_account_id;



NEW.payment_channel_account_id = created_account_id;



RETURN NEW;



END IF;







RETURN NEW;



END;







$function$
;



CREATE OR REPLACE FUNCTION public.create_school_account_on_school_creation_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$















declare 































created_account_id int = null;































BEGIN















-- If account id is not passed then add it 































IF NEW.school_account_id IS NULL THEN















-- try to create the account id















INSERT INTO school_account_gl (account_title, account_balance)















VALUES (NEW.school_name || ' - School A/C', 0) returning id into created_account_id;















NEW.school_account_id = created_account_id;















RETURN NEW;















END IF;































RETURN NEW;















END;































$function$
;



CREATE OR REPLACE FUNCTION public.create_school_class(this_school_id numeric, this_class_code text, this_class_description text)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$



declare



begin

		--Fetch the student account using payment code 

		IF NOT exists (select 1 from core_school where id = this_school_id) THEN

				RAISE EXCEPTION 'SCHOOL NOT FOUND WITH THE SPECIFIED SCHOOL ID';

		end if;





		-- Check if student has transacted atleast once ,if so then fail

		If exists (select 1 from core_school_class where class_code = this_class_code and institution=this_school_id) THEN

				RAISE NOTICE 'CLASS WITH THIS CODE IS ALREADY EXISTENT FOR THIS SCHOOL';

				RETURN false;

		end if;



		--CREATE THE CLASS

		INSERT INTO core_school_class (institution, class_code, class_description, date_created)

			VALUES (this_school_id, this_class_code, this_class_description, now());



		RAISE NOTICE 'CLASS % HAS BEEN CREATED', this_class_code;

		

		RETURN true;

end;

$function$
;
CREATE OR REPLACE FUNCTION public.create_student_account_on_student_creation_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$



declare 







created_account_id int = null;







BEGIN



-- If account id is not passed then add it 







IF NEW.student_account_id IS NULL THEN



-- try to create the account id



INSERT INTO student_account_gl (account_title, account_balance)



VALUES (NEW.first_name || ' ' || NEW.last_name || ' - Student A/C', 0) returning id into created_account_id;



NEW.student_account_id = created_account_id;



RETURN NEW;



END IF;







RETURN NEW;



END;







$function$
;

CREATE OR REPLACE FUNCTION public.datediff(units character varying, start_t timestamp without time zone, end_t timestamp without time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$

   DECLARE

     diff_interval INTERVAL; 

     diff INT = 0;

     years_diff INT = 0;

   BEGIN

     IF units IN ('yy', 'yyyy', 'year', 'mm', 'm', 'month') THEN

       years_diff = DATE_PART('year', end_t) - DATE_PART('year', start_t);

 

       IF units IN ('yy', 'yyyy', 'year') THEN

         -- SQL Server does not count full years passed (only difference between year parts)

         RETURN years_diff;

       ELSE

         -- If end month is less than start month it will subtracted

         RETURN years_diff * 12 + (DATE_PART('month', end_t) - DATE_PART('month', start_t)); 

       END IF;

     END IF;

 

     -- Minus operator returns interval 'DDD days HH:MI:SS'  

     diff_interval = end_t - start_t;

 

     diff = diff + DATE_PART('day', diff_interval);

 

     IF units IN ('wk', 'ww', 'week') THEN

       diff = diff/7;

       RETURN diff;

     END IF;

 

     IF units IN ('dd', 'd', 'day') THEN

       RETURN diff;

     END IF;

 

     diff = diff * 24 + DATE_PART('hour', diff_interval); 

 

     IF units IN ('hh', 'hour') THEN

        RETURN diff;

     END IF;

 

     diff = diff * 60 + DATE_PART('minute', diff_interval);

 

     IF units IN ('mi', 'n', 'minute') THEN

        RETURN diff;

     END IF;

 

     diff = diff * 60 + DATE_PART('second', diff_interval);

 

     RETURN diff;

   END;

   $function$
;



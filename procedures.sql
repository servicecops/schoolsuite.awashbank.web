CREATE OR REPLACE FUNCTION public.associate_school_with_payment_channel(this_school_id numeric, this_payment_channel_id numeric)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare

begin
		IF NOT EXISTS (SELECT 1 FROM core_school WHERE id = this_school_id) THEN
			RAISE NOTICE 'SCHOOL NOT FOUND';
			return false;
		END IF;

		IF NOT EXISTS (SELECT 1 FROM payment_channels WHERE id = this_payment_channel_id) THEN
			RAISE NOTICE 'PAYMENT CHANNEL NOT FOUND';
			return false;	
		END IF;
		
		--Fetch the student account using payment code 
		if NOT exists (select 1 from institution_payment_channel_association where institution = this_school_id and payment_channel=this_payment_channel_id) THEN
				INSERT INTO institution_payment_channel_association(institution, payment_channel) VALUES(this_school_id, this_payment_channel_id);
		end if;

	
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.process_fee_reminders()
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_school_id int8;
    this_student_payment_code int8;

    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    this_student_end_balance numeric;
    this_student_account_id int8;
    this_fee_association_id int8;
    new_outstanding_balance numeric;
   

    fee_record record;

    this_existing_association_id int8;
    this_existing_association_applied boolean;

    this_fee_start_date date;
    this_penalty_next_apply_date date;
		
		fee_amount numeric;
		narration varchar;
		consession_record numeric;
		concession_amount numeric;
		
		consession_remainder numeric;
	this_student_id int8;

  record_association          json;
this_student_association record;
 r institution_fee_student_association%rowtype;
student_record record;
school_record record;
class_record record;
accountgl_record record;

the_next_reminder_date date;
new_id numeric;
the_fee_id numeric;
student_name text;
the_account_balance text;
the_gurdian_name text;
the_payment_code text;
the_school_name text;
the_class_description text;


     cursor_fee_association     cursor for
          select assoc.*, fee.sent_reminders,fee.id as feeId,fee.stop_sending_reminders, fee.reminder_frequency_in_days , fee.effective_date ,fee.message_template from institution_fee_student_association assoc
                                inner join institution_fees_due fee on assoc.fee_id  = fee.id
        where fee.enable_reminders = true and  assoc.reminders_closed = false 
        and ( assoc.fee_outstanding_balance <0 
        or fee.sent_reminders <  fee.stop_sending_reminders) and assoc.next_reminder_date <= current_date ;
    record_fee_association record;
   
   
   
    cursor_closed_fee_association     cursor for
         select assoc.*, fee.sent_reminders,fee.id as feeId,fee.stop_sending_reminders, fee.reminder_frequency_in_days , fee.effective_date ,fee.message_template from institution_fee_student_association assoc
                                inner join institution_fees_due fee on assoc.fee_id  = fee.id
        where fee.enable_reminders = true   and  assoc.number_sent_reminders  >= fee.stop_sending_reminders and assoc .reminders_closed =false;
          
       record_fee_closed_association record;

       
       
   
   
   msg_text varchar;

begin

--get association whose sms needs to be closed
	
	 open cursor_closed_fee_association;
    loop
        fetch cursor_closed_fee_association into record_fee_closed_association;
        exit when not found;
       
      -- the_fee_id = cast((record_fee_association.student_id) as numeric;
       
   	
			
			  UPDATE institution_fee_student_association set
	            reminders_closed = true
	        		where id = record_fee_closed_association.id;
	        	RAISE notice  'MAXMUM NUBMBER OF SMS FOR FEE % HAVE ALREAY BEEN SENT TO PARENT', record_fee_closed_association.feeId ;
	        
		
       		end loop;	       
		       
    close cursor_closed_fee_association;

      
   --get student association
 
   
   open cursor_fee_association;
    loop
        fetch cursor_fee_association into record_fee_association;
        exit when not found;
       
      -- the_fee_id = cast((record_fee_association.student_id) as numeric;
       
   	 -- Get the fee things select * from institution_fees_due
		    SELECT
		        *
		    INTO fee_record
		    FROM
		        institution_fees_due
		    WHERE
		            ID = record_fee_association.feeId ;
		

		    if fee_record.id IS NULL THEN
		        --Fee record does not exist
		        RAISE notice 'SPECIFIED FEE % WAS NOT FOUND', record_fee_association.feeId ;
		    end if;
		
		 	    if fee_record.message_template IS NULL THEN
		        --Fee record does not exist
		        RAISE notice 'SPECIFIED FEE % HAS NO MESSAGE TEMPLATE', record_fee_association.feeId ;
		    end if;
		    
    
			If record_fee_association.sent_reminders >=  record_fee_association.stop_sending_reminders then
			
			  UPDATE institution_fee_student_association set
	            reminders_closed = true
	        where id = record_fee_association.id;
	        	RAISE notice  'MAXMUM NUBMBER OF SMS FOR FEE % HAVE ALREAY BEEN SENT TO PARENT', record_fee_association.feeId ;
	        
		end if;
       
	

        --get the std, sch and class details
			select * into student_record from core_student si where id =  cast((record_fee_association.student_id) as numeric);
			select * into accountgl_record from student_account_gl sag  where id =  cast((student_record.student_account_id) as numeric);

			select * into school_record from core_school si2  where id =  cast((student_record.school_id) as numeric);
			select * into class_record from core_school_class csc  where id =  cast((student_record.class_id) as numeric);
			 
     
     
		     msg_text = cast((fee_record.message_template) as text);
		     student_name = concat((cast((student_record.last_name) as text)), ' ', cast((student_record.first_name) as text), ' ',cast((student_record.middle_name) as text));
		     raise notice 'message os %',msg_text;
		    
		    --check if message template has data
		    
		    if accountgl_record.account_balance is null then
		    	the_account_balance ='';
		    else
		    	the_account_balance =	cast((accountgl_record.account_balance) as text);
		    end if;	
	
		    if student_record.guardian_name is null then
		    	the_gurdian_name ='';
		    else
		    	the_gurdian_name =	cast((student_record.guardian_name) as text);
		    end if;	
		   
		     if student_record.student_code is null then
		    	the_payment_code ='';
		    else
		    	the_payment_code =	cast((student_record.student_code) as text);
		    end if;	
		   
		     if school_record.school_name is null then
		    	the_school_name ='';
		    else
		    	the_school_name =	cast((school_record.school_name) as text);
		    end if;	
		   
		     if class_record.class_description is null then
		    	the_class_description ='';
		    else
		    	the_class_description =	cast((class_record.class_description) as text);
		    end if;	
		   
		   
		   
		   
		   
				msg_text = REPLACE(msg_text,'{OUTSTANDING_BALANCE}',the_account_balance );
				msg_text = REPLACE(msg_text,'{GUARDIAN_NAME}', the_gurdian_name);
				msg_text = REPLACE(msg_text,'{STUDENT_PAYMENT_CODE}',the_payment_code);
				msg_text = REPLACE(msg_text,'{SCHOOL_NAME}', the_school_name);
				msg_text = REPLACE(msg_text,'{STUDENT_CLASS}', the_class_description);
				msg_text = REPLACE(msg_text,'{STUDENT_NAME}', student_name);
				
			   

				--insert into msg outbox
--   
				INSERT INTO message_outbox (
					message_text,
					
					recipient_number)
				
				VALUES
					(
						msg_text,
						cast((student_record.guardian_phone) as text)
		
						
						
					) ;--RETURNING id into new_id;
		
 				raise notice 'new_id os %',new_id;
        
        
		        
		        
		        --UPDATE institution_fee_student_association 

		    		     raise notice 'next_reminder_date os %',record_fee_association.next_reminder_date;

		       --next_date to send sms
		       the_next_reminder_date = record_fee_association.next_reminder_date +  (record_fee_association.reminder_frequency_in_days||' day')::interval;
		      
		           

		       
		           UPDATE institution_fee_student_association set
		            next_reminder_date = the_next_reminder_date 
		        	where id = record_fee_association.id ;
		       
		       
		     	  --ipdate sent reminders
		           UPDATE institution_fee_student_association set
		            number_sent_reminders = cast((record_fee_association.number_sent_reminders) as numeric) + 1
		        	where id = record_fee_association.id;
		
		      	  -- If sent reminders are equal to number of times to stop sending reminders, set reminders closed as true
		        
		        	if record_fee_association.sent_reminders >=record_fee_association.stop_sending_reminders then
		       		 UPDATE institution_fee_student_association set
		            reminders_closed = true
		       		 where id = record_fee_association.id;
					end if;
		       		
		       				   --  raise notice 'record_fee_association.stop_sending_reminder os %',record_fee_association.stop_sending_reminders;
		     --raise notice 'record_fee_association.sent_remindersos %',record_fee_association.sent_reminders;

		    		    -- raise notice 'message os %',the_next_reminder_date;


					end loop;

			
		      
		 
		       
		       
    close cursor_fee_association;
   
   
    
    RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.apply_fee_to_student0(this_student_id bigint, this_fee_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_school_id int8;
    this_student_payment_code int8;

    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    this_student_end_balance numeric;
    this_student_account_id int8;
    this_fee_association_id int8;
    new_outstanding_balance numeric;

    fee_record record;

    this_existing_association_id int8;
    this_existing_association_applied boolean;

    this_fee_start_date date;
    this_penalty_next_apply_date date;

begin

    SELECT student_code into this_student_payment_code FROM core_student WHERE ID = this_student_id;

    IF this_student_payment_code IS NULL THEN
        RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id ;
        RETURN FALSE ;
    end if;


    -- Get the fee things select * from institution_fees_due
    SELECT
        *
    INTO fee_record
    FROM
        institution_fees_due
    WHERE
            ID = this_fee_id ;


    if fee_record.id IS NULL THEN
        --Fee record does not exist
        RAISE EXCEPTION 'SPECIFIED FEE % WAS NOT FOUND', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.recurrent = true THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS RECURRENT AND HENCE CANNOT BE APPLIED DIRECTLY TO STUDENTS', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.approval_status = false THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS NOT YET APPROVED', this_fee_id;
    end if;



--If fee due date is past, then this_fee_start_date should be today
    if fee_record.effective_date < current_date then
        this_fee_start_date = current_date;
    else
        this_fee_start_date = fee_record.effective_date;
    end if;


    -- Select the student account id
    SELECT
        student_account_id,
        school_id INTO this_student_account_id,
        this_student_school_id
    FROM
        core_student
    WHERE
            ID = this_student_id ;


    -- School for fee and student should be the same

    IF fee_record.school_id <> this_student_school_id THEN
        RAISE EXCEPTION 'THE SCHOOL FOR THE FEE AND STUDENT MUST BE THE SAME';
    END
        IF ;


    -- Check if fee is exempted for this
    IF EXISTS (SELECT 1
               FROM
                   fees_due_student_exemption
               WHERE
                       fee_id = fee_record.id
                 AND student_number = this_student_payment_code
        ) THEN
        RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE %',
            this_student_id,
            fee_record.id;
        return false;
    END
        IF ;


    --if fee is child of recurrent, check for exemption from parent fee
    if fee_record.child_of_recurrent then
        IF EXISTS (SELECT 1 FROM fees_due_student_exemption WHERE fee_id = fee_record.parent_fee_id AND student_number = this_student_payment_code )
        THEN
            RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE''s PARENT FEE %',
                this_student_id,
                fee_record.id;
            return false;
        END
            IF ;
    end if;


    -- Get possible existing record
    select id, applied  into this_existing_association_id , this_existing_association_applied
    from institution_fee_student_association
    WHERE
            fee_id = this_fee_id
      AND student_id = this_student_id;

    -- Check if fee is already applied to this student
    IF this_existing_association_applied = TRUE
    THEN
        RAISE NOTICE 'THIS FEE % IS ALREADY APPLIED TO THIS STUDENT %',
            fee_record.id,
            this_student_id ;
        return false;
    END IF;


    -- IF FEE EFFECTIVE DATE IS IN FUTURE
    -- CHECK IF ALREADY BOOKED AND RETURN
    -- ELSE BOOK AND RETURN
    IF fee_record.effective_date > current_date
    THEN

        if this_existing_association_id is not null and this_existing_association_applied = false
        then
            RAISE NOTICE 'THIS FEE % IS ALREADY BOOKED TO A FUTURE DATE FOR THIS STUDENT %',
                this_fee_id,
                this_student_id ;
            return false;
        end if;

        -- Book fee now and return
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id
        )
        VALUES
        (
            now(),
            fee_record.id,
            this_student_id,
            FALSE,  --Applied as false
            web_user_name,
            now(),
            fee_record.due_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id
        ) RETURNING ID INTO this_fee_association_id ;

        RAISE NOTICE 'THIS FEE % HAS NOW BEEN BOOKED FOR THIS STUDENT %',
            fee_record.id,
            this_student_id ;

        return true;

    END IF;



    -- Get the current balance for UPDATE
    SELECT
        account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
    FROM
        student_account_gl
    WHERE
            ID = this_student_account_id FOR SHARE ;

    -- Adjust the new balance
    this_student_end_balance = this_student_current_balance - fee_record.due_amount;
    new_outstanding_balance = this_student_current_outstanding_balance - fee_record.due_amount;

    IF new_outstanding_balance > 0 THEN
        new_outstanding_balance = 0;
    END IF;

    -- If penalty, we are to save the penalty apply date
    -- Compute this from the this_fee_start_date - which can be effective date or current date depending on whether fee effective date is past
    this_penalty_next_apply_date = null;
    if fee_record.has_penalty = true
    then
        if fee_record.arrears_after_units = 'D'
        then
            this_penalty_next_apply_date = fee_record.effective_date + interval '1 day' * fee_record.arrears_after_count;
        elsif fee_record.arrears_after_units = 'W' then
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 week' * fee_record.arrears_after_count;
        else
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 month' * fee_record.arrears_after_count;
        end if;
    end if;




    -- Create association now select now() + interval '1 day' *5format('+ %s day', this_fee_arrears_after_count);
    -- IF association exists, just update it to applied, and set the next penanly apply date else create it
    if this_existing_association_id is not null and this_existing_association_applied = false
    then
        update institution_fee_student_association set applied = true,next_penalty_apply_date=this_penalty_next_apply_date  where id = this_existing_association_id;
        --Set the assoc id to the existing record for future use
        this_fee_association_id = this_existing_association_id;
    else
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id
        )
        VALUES
        (
            now(),
            this_fee_id,
            this_student_id,
            TRUE,
            web_user_name,
            now(),
            fee_record.due_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id
        ) RETURNING ID INTO this_fee_association_id ;
    end if;



    IF this_student_end_balance < 0 THEN
        new_outstanding_balance = this_student_end_balance;
    else
        new_outstanding_balance = 0;
        --if the student end balance is greater than zero or eqaul to zero, then it means that the fee has applied while student has sufficient balance
        -- then update the association record and close the penalty
        update institution_fee_student_association set penalties_closed = true, fee_outstanding_balance=0 where id = this_fee_association_id ;
    END IF;


    -- update balance
    UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = this_student_account_id;

    -- Create th record
    INSERT INTO student_account_transaction_history (
        date_created,
        amount,
        posting_user,
        description,
        fee_association_id,
        payment_id,
        trans_type,
        account_id,
        reversal_flag,
        reversed,
        date_reversed,
        balance_after,
        associated_fee_due,
        outstanding_balance_after
    )
    VALUES
    (
        now(),
        - 1 * fee_record.due_amount,
        web_user_name,
        'FEE DUE - ' || fee_record.description,
        this_fee_association_id,
        NULL,
        'FEE DUE INV.',
        this_student_account_id,
        FALSE,
        FALSE,
        NULL,
        this_student_end_balance,
        fee_record.id,
        new_outstanding_balance
    ) ;


    INSERT INTO institution_fees_due_student_payment_transaction_history
    ("date_created",
     "payment_id",
     "fee_id",
     "fee_student_association_id",
     "student_id",
     "amount",
     "balance_before",
     "balance_after",
     "description"
    )
    VALUES (
               now(),
               0,
               fee_record.id,
               this_fee_association_id,
               this_student_id,
               fee_record.due_amount*-1,
               0,
               fee_record.due_amount*-1,
               format('FEE APPLIED')
           );

    -- Log now
    INSERT INTO web_console_log (
        ip_address,
        user_name,
        event_date,
        event_action,
        affected_student_id,
        errors,
        web_user_id
    )
    VALUES
    (
        ip_address,
        web_user_name,
        now(),
        'Apply fee ' || fee_record.id,
        this_student_id,
        '',
        web_user_id
    ) ;
    RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.adjust_student_balance(this_student_payment_code bigint, this_new_balance numeric, posting_user_id numeric, posting_user character varying, reason character varying, ip character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
 this_student_id integer;
this_student_account_id integer;
this_student_account_balance numeric;
this_student_outstanding_balance numeric;
amount_to_post numeric;
new_outstanding_balance numeric;
student_th_record_id integer;
student_name varchar;
begin
		--Fetch the student account using payment code 
		if NOT exists (select 1 from core_student where student_code = this_student_payment_code) THEN
				RAISE EXCEPTION 'STUDENT NOT FOUND WITH THE SPECIFIED PAYMENT CODE';
		end if;

		-- Select the student payment code
		SELECT id, student_account_id,
		format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) 
	into this_student_id, this_student_account_id, student_name from core_student where student_code = this_student_payment_code;
		
		--Select account for ID
		SELECT account_balance, outstanding_balance into this_student_account_balance, this_student_outstanding_balance from student_account_gl where id = this_student_account_id for update;
		--Determine what amount to POST
		amount_to_post = this_new_balance - this_student_account_balance;
		--Now update the balance
		new_outstanding_balance = this_new_balance;	
				
		IF new_outstanding_balance > 0 THEN
			new_outstanding_balance = 0;
		END IF;

		-- if amount to post is zero, do NOTHING
		IF amount_to_post = 0 AND new_outstanding_balance = this_student_outstanding_balance THEN
			RAISE NOTICE 'NO AMOUNT TO PASS';
			RETURN false;
		END IF;

		

		--Now post by inserting into student th, and updating balance accordingly
		INSERT INTO student_account_transaction_history(
            date_created, amount, posting_user, description, 
            trans_type, account_id, reversal_flag, reversed, 
            balance_after, outstanding_balance_after)
    VALUES (now(), amount_to_post, posting_user, reason,  
            'BAL-ADJUSTMENT', this_student_account_id, false, false,
            this_new_balance, new_outstanding_balance) returning id into student_th_record_id;

		
		if this_new_balance < 0
		then
		new_outstanding_balance = this_new_balance;
		end if;

		update student_account_gl set account_balance = this_new_balance, outstanding_balance = new_outstanding_balance where id = this_student_account_id;
		
		-- If amount is positive, apportion it to fees
		if amount_to_post > 0
		then
			perform apportion_payment_amount_to_fees(this_student_id, 0, amount_to_post);
		end if;
		
		
		-- Log to web console log
	INSERT INTO web_console_log ( "ip_address", "user_name", "event_date", "event_action", "affected_student_id", "web_user_id","student_th_id"  )
	VALUES
		( ip, posting_user, now(), format('%s''s balance adjusted to %s', student_name, this_new_balance), this_student_id, posting_user_id,student_th_record_id );
		
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.apply_fee_to_student(this_student_id bigint, this_fee_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_school_id int8;
    this_student_payment_code int8;

    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    this_student_end_balance numeric;
    this_student_account_id int8;
    this_fee_association_id int8;
    new_outstanding_balance numeric;

    fee_record record;

    this_existing_association_id int8;
    this_existing_association_applied boolean;

    this_fee_start_date date;
    this_penalty_next_apply_date date;
		
		fee_amount numeric;
		narration varchar;
		consession_record numeric;
		concession_amount numeric;
		
		consession_remainder numeric;
	the_next_reminder_date date;

begin

    SELECT student_code into this_student_payment_code FROM core_student WHERE ID = this_student_id;

    IF this_student_payment_code IS NULL THEN
        RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id ;
        RETURN FALSE ;
    end if;


    -- Get the fee things select * from institution_fees_due
    SELECT
        *
    INTO fee_record
    FROM
        institution_fees_due
    WHERE
            ID = this_fee_id ;


    if fee_record.id IS NULL THEN
        --Fee record does not exist
        RAISE EXCEPTION 'SPECIFIED FEE % WAS NOT FOUND', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.recurrent = true THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS RECURRENT AND HENCE CANNOT BE APPLIED DIRECTLY TO STUDENTS', this_fee_id;
    end if;

    -- Fee should be approved
    IF fee_record.approval_status = false THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS NOT YET APPROVED', this_fee_id;
    end if;



--If fee due date is past, then this_fee_start_date should be today
    if fee_record.effective_date < current_date then
        this_fee_start_date = current_date;
    else
        this_fee_start_date = fee_record.effective_date;
    end if;


    -- Select the student account id
    SELECT
        student_account_id,
        school_id INTO this_student_account_id,
        this_student_school_id
    FROM
        core_student
    WHERE
            ID = this_student_id ;


    -- School for fee and student should be the same

    IF fee_record.school_id <> this_student_school_id THEN
        RAISE EXCEPTION 'THE SCHOOL FOR THE FEE AND STUDENT MUST BE THE SAME';
    END
        IF ;


    -- Check if fee is exempted for this
    IF EXISTS (SELECT 1
               FROM
                   fees_due_student_exemption
               WHERE
                       fee_id = fee_record.id
                 AND student_number = this_student_payment_code
        ) THEN
        RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE %',
            this_student_id,
            fee_record.id;
        return false;
    END
        IF ;


    --if fee is child of recurrent, check for exemption from parent fee
    if fee_record.child_of_recurrent then
        IF EXISTS (SELECT 1 FROM fees_due_student_exemption WHERE fee_id = fee_record.parent_fee_id AND student_number = this_student_payment_code )
        THEN
            RAISE NOTICE 'THIS STUDENT % IS EXEMPTED FROM THIS FEE''s PARENT FEE %',
                this_student_id,
                fee_record.id;
            return false;
        END
            IF ;
    end if;

		fee_amount = fee_record.due_amount;
		narration ='FEE DUE - ' || fee_record.description;
		
		
	--check if fee has concessions;
		SELECT
        *
    INTO consession_record
    FROM
        fee_concessions
    WHERE
    fee_id = this_fee_id ;
		
		
		    -- Check if student has consession for this
    IF EXISTS (SELECT 1
               FROM
                   student_fee_concessions
               WHERE
                       fee_id = fee_record.id
                 AND student_id = this_student_id
        ) THEN
        
				--compute amount to pay
				select fc.percentage  INTO consession_record from fee_concessions  fc INNER JOIN student_fee_concessions sfc on sfc.concessions_id = fc.id where sfc.student_id =this_student_id and fc.fee_id =fee_record.id;
				concession_amount= fee_amount *(consession_record/100);
			fee_amount = fee_amount-concession_amount;
			
			consession_remainder = 100-consession_record;
			narration= consession_remainder||'% FEE DUE - ' || fee_record.description;
			
			
				
    END  IF ;
		
	
	
		
   
      
   --if fee is child of recurrent, check for exemption from parent fee
   if fee_record.child_of_recurrent then
   		IF EXISTS (SELECT 1 FROM student_fee_concessions WHERE fee_id = fee_record.parent_fee_id AND student_id = this_student_id ) 
   		THEN
     
				--compute amount to pay
				select fc.percentage  INTO consession_record from fee_concessions  fc INNER JOIN student_fee_concessions sfc on sfc.concessions_id = fc.id where sfc.student_id =this_student_id and fc.fee_id = fee_record.parent_fee_id;
				
				concession_amount= fee_amount *(consession_record/100);
			fee_amount = fee_amount-concession_amount;
			narration= consession_record||'% FEE DUE - ' || fee_record.description;

			
			
    END
    IF ;
   end if;
  
  
  --check if fee has reminders
   

if fee_record.enable_reminders then
	the_next_reminder_date = fee_record.effective_date +  (fee_record.send_msg_after_effective_date_in_days||' day')::interval;


end if;


    -- Get possible existing record
    select id, applied  into this_existing_association_id , this_existing_association_applied
    from institution_fee_student_association
    WHERE
            fee_id = this_fee_id
      AND student_id = this_student_id;

    -- Check if fee is already applied to this student
    IF this_existing_association_applied = TRUE
    THEN
        RAISE NOTICE 'THIS FEE % IS ALREADY APPLIED TO THIS STUDENT %',
            fee_record.id,
            this_student_id ;
        return false;
    END IF;
   
   
   
   


    -- IF FEE EFFECTIVE DATE IS IN FUTURE
    -- CHECK IF ALREADY BOOKED AND RETURN
    -- ELSE BOOK AND RETURN
    IF fee_record.effective_date > current_date
    THEN

        if this_existing_association_id is not null and this_existing_association_applied = false
        then
            RAISE NOTICE 'THIS FEE % IS ALREADY BOOKED TO A FUTURE DATE FOR THIS STUDENT %',
                this_fee_id,
                this_student_id ;
            return false;
        end if;

        -- Book fee now and return
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id,
            number_sent_reminders,
            next_reminder_date,
            reminders_closed
        )
        VALUES
        (
            now(),
            fee_record.id,
            this_student_id,
            FALSE,  --Applied as false
            web_user_name,
            now(),
            fee_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id,
           
            0,
            the_next_reminder_date,
            false
        ) RETURNING ID INTO this_fee_association_id ;

        RAISE NOTICE 'THIS FEE % HAS NOW BEEN BOOKED FOR THIS STUDENT %',
            fee_record.id,
            this_student_id ;

        return true;

    END IF;



    -- Get the current balance for UPDATE
    SELECT
        account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
    FROM
        student_account_gl
    WHERE
            ID = this_student_account_id FOR SHARE ;

    -- Adjust the new balance
    this_student_end_balance = this_student_current_balance - fee_amount;
    new_outstanding_balance = this_student_current_outstanding_balance - fee_amount;

    IF new_outstanding_balance > 0 THEN
        new_outstanding_balance = 0;
    END IF;

    -- If penalty, we are to save the penalty apply date
    -- Compute this from the this_fee_start_date - which can be effective date or current date depending on whether fee effective date is past
    this_penalty_next_apply_date = null;
    if fee_record.has_penalty = true
    then
        if fee_record.arrears_after_units = 'D'
        then
            this_penalty_next_apply_date = fee_record.effective_date + interval '1 day' * fee_record.arrears_after_count;
        elsif fee_record.arrears_after_units = 'W' then
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 week' * fee_record.arrears_after_count;
        else
            this_penalty_next_apply_date = fee_record.effective_date +  interval '1 month' * fee_record.arrears_after_count;
        end if;
    end if;




    -- Create association now select now() + interval '1 day' *5format('+ %s day', this_fee_arrears_after_count);
    -- IF association exists, just update it to applied, and set the next penanly apply date else create it
    if this_existing_association_id is not null and this_existing_association_applied = false
    then
        update institution_fee_student_association set applied = true,next_penalty_apply_date=this_penalty_next_apply_date  where id = this_existing_association_id;
        --Set the assoc id to the existing record for future use
        this_fee_association_id = this_existing_association_id;
    else
        INSERT INTO institution_fee_student_association (
            date_created,
            fee_id,
            student_id,
            applied,
            created_by,
            date_applied,
            fee_outstanding_balance,
            has_penalty,
            next_penalty_apply_date,
            due_date,
            penalty_id,
            number_sent_reminders,
            next_reminder_date,
            reminders_closed
        )
        VALUES
        (
            now(),
            this_fee_id,
            this_student_id,
            TRUE,
            web_user_name,
            now(),
            fee_amount*-1,
            fee_record.has_penalty,
            this_penalty_next_apply_date,
            fee_record.effective_date,
            fee_record.penalty_id,
             0,
            the_next_reminder_date,
        
            false
        ) RETURNING ID INTO this_fee_association_id ;
    end if;



    IF this_student_end_balance < 0 THEN
        new_outstanding_balance = this_student_end_balance;
    else
        new_outstanding_balance = 0;
        --if the student end balance is greater than zero or eqaul to zero, then it means that the fee has applied while student has sufficient balance
        -- then update the association record and close the penalty
        update institution_fee_student_association set penalties_closed = true, fee_outstanding_balance=0 where id = this_fee_association_id ;
    END IF;


    -- update balance
    UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = this_student_account_id;

    -- Create th record
    INSERT INTO student_account_transaction_history (
        date_created,
        amount,
        posting_user,
        description,
        fee_association_id,
        payment_id,
        trans_type,
        account_id,
        reversal_flag,
        reversed,
        date_reversed,
        balance_after,
        associated_fee_due,
        outstanding_balance_after
    )
    VALUES
    (
        now(),
        - 1 * fee_amount,
        web_user_name,
        narration,
        this_fee_association_id,
        NULL,
        'FEE DUE INV.',
        this_student_account_id,
        FALSE,
        FALSE,
        NULL,
        this_student_end_balance,
        fee_record.id,
        new_outstanding_balance
    ) ;


    INSERT INTO institution_fees_due_student_payment_transaction_history
    ("date_created",
     "payment_id",
     "fee_id",
     "fee_student_association_id",
     "student_id",
     "amount",
     "balance_before",
     "balance_after",
     "description"
    )
    VALUES (
               now(),
               0,
               fee_record.id,
               this_fee_association_id,
               this_student_id,
               fee_amount*-1,
               0,
               fee_amount*-1,
               format('FEE APPLIED')
           );

    -- Log now
    INSERT INTO web_console_log (
        ip_address,
        user_name,
        event_date,
        event_action,
        affected_student_id,
        errors,
        web_user_id
    )
    VALUES
    (
        ip_address,
        web_user_name,
        now(),
        'Apply fee ' || fee_record.id,
        this_student_id,
        '',
        web_user_id
    ) ;
    RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.sp_process_student_payment(in_payment_channel bigint, in_payment_json text)
 RETURNS json
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare
request_json json;
feeAssociations json;
feeAssocRecord json;

student_record record;
school_record record;
payment_channel_record record;

student_account_record record;
school_account_record record;
payment_channel_account_record record;
payment_channel_secondary_account_record record;

payment_record record;
bank_record record;
provider_record record;
amount numeric;
paymentType varchar;

feeId int8;
feeMemo varchar;
tenderedAmount numeric;
totalFeeAmount numeric = 0;

fetch_cursor refcursor;
studentPaymentCode varchar;

associated_fee_due_id int8;
student_outstanding_balance_after numeric;
student_outstanding_balance_before numeric;
student_balance_before numeric;
student_balance_after numeric;


effective_school_details_record record;


begin
		-- Parse INPUT
		request_json = in_payment_json::json;
		amount = cast(request_json ->> 'amount' as numeric);

		studentPaymentCode = request_json ->> 'studentPaymentCode';
		studentPaymentCode = upper(trim(replace(studentPaymentCode, ' ', '')));
		paymentType = request_json ->> 'paymentType';
		feeAssociations = (request_json ->> 'feeAssociations') :: json ;
		
		
		IF studentPaymentCode IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (100, 'STUDENT PAYMENT CODE IS REQUIRED BUT WAS NOT SUPPLIED')) data("returnCode", "returnMessage");
		END IF;

		IF amount IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (101, 'AMOUNT IS REQUIRED BUT WAS NOT SUPPLIED')) data("returnCode", "returnMessage");
		END IF;

		IF amount <= 0
		THEN
			RETURN  row_to_json(data) FROM (VALUES (109, 'AMOUNT SHOULD BE GREATER THAN ZERO')) data("returnCode", "returnMessage");
		END IF;

		IF in_payment_channel IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (105, 'PAYMENT CHANNEL IS NOT SUPPLIED')) data("returnCode", "returnMessage");
		END IF;

		IF request_json ->> 'channelTransactionId' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (106, 'CHANNEL TRANS ID IS REQUIRED AND WAS NOT SPECIFIED')) data("returnCode", "returnMessage");
		END IF;

		IF request_json ->> 'channelMemo' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (107, 'CHANNEL MEMO IS REQUIRED AND WAS NOT SPECIFIED')) data("returnCode", "returnMessage");
		END IF;
		IF request_json ->> 'processDate' IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (108, 'PROCESS DATE IS REQUIRED AND WAS NOT SPECIFIED')) data("returnCode", "returnMessage");
		END IF;
	
		IF feeAssociations IS NOT NULL AND json_array_length(feeAssociations)>0
		THEN
			FOR feeAssocRecord IN SELECT * FROM json_array_elements(feeAssociations)
			LOOP
				feeId = cast(feeAssocRecord::json->>'feeId' as int8);
				tenderedAmount = cast(feeAssocRecord::json->>'tenderedAmount' as numeric);
				feeMemo = feeAssocRecord::json->>'feeMemo';

					IF tenderedAmount IS NULL 
					THEN
						RETURN  row_to_json(data) FROM (VALUES (110, 'INVALID FEE BREAKDOWN AMOUNT WITH ID '|| feeId ||' SPECIFY A VALID FEE BREAKDOWN AMOUNT AND TRY AGAIN')) data("returnCode", "returnMessage");
					END IF;
				totalFeeAmount = totalFeeAmount + tenderedAmount;
			END LOOP;

			-- Check that fee associations sum is equal to payment amount
			IF amount <> totalFeeAmount 
			THEN
				RETURN  row_to_json(data) FROM (VALUES (143, 'FEES AMOUNT MISMATCH. TOTAL OF FEES BREAKDOWNS MUST TOTAL TO THE TOTAL AMOUNT TENDERED.')) data("returnCode", "returnMessage");
			END IF;
			
		END IF;

		-- Avoid blank payment types
		IF paymentType IS NULL
		THEN
		paymentType = 'CASH';
		END IF;
		-- Work on fee associations
		
		-- Get the PAYMENT CHANNEL
		OPEN fetch_cursor for 
		select * from payment_channels where id = in_payment_channel limit 1;
		FETCH fetch_cursor INTO payment_channel_record;
		close fetch_cursor;	
	
	
		IF payment_channel_record.id IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (199, 'Payment channel was not found')) data("returnCode", "returnMessage");
		END IF;

		-- Get the student
		IF studentPaymentCode ~ '^((99)\d{8})$' -- Payment code or not
		then
			OPEN fetch_cursor for 
				select
					si.id,
					si.first_name,
					si.middle_name,
					si.last_name,
					si.guardian_phone,
					si.school_id,
					si.active,
					si.student_account_id,
					si.student_code,
					isc.class_code ,
					isc.class_description,
					si.class_id,
					si.archived,
					format('%s%s%s', first_name,
					case
						when middle_name is not null then ' ' || middle_name
						else ''
					end,
					case
						when last_name is not null then ' ' || last_name
						else ''
					end) as student_name,
					case when si.allow_part_payments is null then true else si.allow_part_payments end as part_payments
				from
					core_student si
				inner join core_school_class isc on
					si.class_id = isc.id
				where
					student_code = cast(studentPaymentCode as int8)
				limit 1;
			FETCH fetch_cursor INTO student_record;
			close fetch_cursor;	
		ELSE
		studentPaymentCode = upper(studentPaymentCode);
		-- How many students have this reg no?			
		IF (SELECT count(id) from core_student where school_student_registration_number=studentPaymentCode) > 1
		THEN
		RETURN  row_to_json(data) FROM (VALUES (407, 'Student selection failed. Use the 10 digit payment code to select specific student')) data("returnCode", "returnMessage");
		END IF;
	
	
			OPEN fetch_cursor for 
				select
					si.id,
					si.first_name,
					si.middle_name,
					si.last_name,
					si.guardian_phone,
					si.school_id,
					si.active,
					si.student_account_id,
					si.student_code,
					isc.class_code ,
					isc.class_description,
					si.class_id,
					si.archived,
					format('%s%s%s', first_name,
					case
						when middle_name is not null then ' ' || middle_name
						else ''
					end,
					case
						when last_name is not null then ' ' || last_name
						else ''
					end) as student_name,
					case when si.allow_part_payments is null then true else si.allow_part_payments end as part_payments
				from
					core_student si
				inner join core_school_class isc on
					si.class_id = isc.id
				where
					school_student_registration_number = studentPaymentCode
				limit 1;
			
			FETCH fetch_cursor INTO student_record;
			close fetch_cursor;	
		END IF;
		
		-- Check if we have something atleast
		IF student_record.id IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (404, 'No student was found with the specified payment code or registration number')) data("returnCode", "returnMessage");
		END IF;	
	

		
		-- Get the school
		OPEN fetch_cursor for 
			select
				*, 
				case when si.default_part_payment_behaviour is null then true else si.default_part_payment_behaviour end as part_payments
			from
				core_school si
			where
				id = student_record.school_id
			limit 1;
			FETCH fetch_cursor INTO school_record;
		close fetch_cursor;
		
		IF school_record.id IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, student does not belong to a valid school or institution')) data("returnCode", "returnMessage");
		END IF;

		OPEN fetch_cursor for 
			select * from nominated_bank_details nbd where id = school_record.bank_name limit 1;
			FETCH fetch_cursor INTO bank_record;
		close fetch_cursor;
	
	    select * from sp_resolve_student_school_details(student_record.id) into effective_school_details_record; 
	
		--Resolve the payment channel account to use	
		select * from sp_resolve_payment_channel_account(payment_channel_record.id, effective_school_details_record.bank_id) into payment_channel_account_record;
		-- DOES THE PAYMENT CHANNEL HAVE ENOUGH BALANCE 
		IF (payment_channel_account_record.account_balance - amount) < payment_channel_account_record.account_limit
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (309, 'INSUFFICIENT BALANCE. PAYMENT CHANNEL DOESN''T HAVE SUFFICIENT BALANCE FOR THIS OPERATION')) data("returnCode", "returnMessage");
		END IF;
		
		-- CHECK IF STUDENT IS NOT ACTIVE
		IF student_record.active = false and student_record.archived = false
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (502, 'INACTIVE: STUDENT IS NOT ACTIVE. ALL FUTURE REQUESTS FOR STUDENT WILL FAIL.')) data("returnCode", "returnMessage");
		END IF;



		-- is school active
		IF school_record.active = FALSE
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (501, 'INACTIVE: THE ASSOCIATED SCHOOL / INSTITUTION IS NOT ACTIVE')) data("returnCode", "returnMessage");
		END IF;
		
		

		-- CHECK IF SCHOOL ALLOWS PAYMENTS FROM THIS CHANNEL 
		IF NOT EXISTS (SELECT 1 FROM institution_payment_channel_association WHERE institution = school_record.id AND payment_channel = in_payment_channel)
		THEN
		RETURN  row_to_json(data) FROM (VALUES (511, 'NOT ALLOWED. School ' || school_record.school_name || ' does not accept payments from ' || payment_channel_record.channel_name )) data("returnCode", "returnMessage");
		END IF;

		-- Get school account gl record
		OPEN fetch_cursor for 
			select * from school_account_gl sag where id = school_record.school_account_id limit 1 for update;
			FETCH fetch_cursor INTO school_account_record;
		close fetch_cursor;		
		
		IF school_account_record.id IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, institution gl information is missing, contact support for assistance' )) data("returnCode", "returnMessage");
		END IF;
		
		
		-- Get student account gl record
		OPEN fetch_cursor for 
			select * from student_account_gl sag where id = student_record.student_account_id limit 1 for update;
			FETCH fetch_cursor INTO student_account_record;
		close fetch_cursor;		
	
		IF student_account_record.id IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, student gl information is missing, contact support for assistance' )) data("returnCode", "returnMessage");
		END IF;
		

		-- CHECK IF WE SHOULD ALLOW PART PAYMENTS select * from student_account_gl limit 10;
		IF 	school_record.part_payments = FALSE
		THEN
			IF student_record.part_payments = FALSE AND amount < ABS(student_account_record.outstanding_balance)
			THEN
			--ITS A PART PAYMENT
			RETURN  row_to_json(data) FROM (VALUES (204, 'Sorry, Student ' || student_record.student_name || ' is not allowed to make part payments. Minimum amount allowed is ' || (ABS(student_account_record.outstanding_balance)) )) data("returnCode", "returnMessage");
			END IF;
		end if;


		-- Check if payment payment for trans id EXISTS	
		OPEN fetch_cursor for 
			select * from payments_received where payment_channel = payment_channel_record.id and channel_trans_id = request_json->>'channelTransactionId' LIMIT 1;
			FETCH fetch_cursor INTO payment_record;
		close fetch_cursor;			
	
		IF payment_record.id IS NOT NULL
			THEN
			--WE ALREADY HAVE A PAYMENT FOR THIS TRANSID AND FROM THIS CHANNEL
						IF student_record.id = payment_record.student_id AND payment_record.amount = amount
						THEN
						--IF AMOUNT IS SAME AND STUDENT IS THE SAME THEN ITS A REPOSTING, SO RETURN DETAILS WITH RETURN CODE 0
						
						RETURN row_to_json (DATA)
						FROM
							(
						
								VALUES
									(
										0,
										format('School fees payment was already processed on %s. Reference:%s', payment_record.date_created, payment_record.reciept_number),
										school_record.school_code,
										school_record.school_name,
										student_record.first_name,
										student_record.middle_name,
										student_record.last_name,
										format('%s - %s', student_record.class_code, student_record.class_description),
										payment_record.reciept_number,
										to_json(student_record),
										payment_record.id,
										student_record.student_name
									)
							) DATA ("returnCode", "returnMessage", "schoolCode", "schoolName", "firstName", "middleName", "lastName", "studentClass", "receiptNumber", "studentDetails", "paymentId", "studentFullNames") ;

						
						END IF;

			-- An attempt to reuse a trans id should fail immediately
			RETURN  row_to_json(data) FROM (VALUES (456, 'DUPLICATE TRANS ID: EACH TRANS ID SHOULD BE UNIQUE FOR A PAYMENT CHANNEL. '|| (request_json->>'channelTransactionId') ||' WAS ALREADY RECEIVED FOR A COMPLETELY DIFFERENT PAYMENT.')) data("returnCode", "returnMessage");
		END IF;

		


		-- Check if fee associations are passed, then fetch them and try to validate it now
		IF feeAssociations IS NOT NULL AND json_array_length(feeAssociations)>0
		THEN
			FOR feeAssocRecord IN SELECT * FROM json_array_elements(feeAssociations)
			LOOP
				
				feeId = cast(feeAssocRecord::json->>'feeId' as int8);
				tenderedAmount = cast(feeAssocRecord::json->>'tenderedAmount' as int8);
				feeMemo = feeAssocRecord::json->>'feeMemo';
					-- This feeId must exist and associated with student
					IF NOT EXISTS (SELECT 1 FROM institution_fee_student_association WHERE id = feeId AND student_id = student_record.id)
					THEN
						RETURN  row_to_json(data) FROM (VALUES (458, format('FEE %s & STUDENT MISMATCH. SPECIFIED FEE IS NOT ASSOCIATED WITH THE STUDENT. OPERATION ABORTED.', feeId))) data("returnCode", "returnMessage");
					END IF;
			END LOOP;
		END IF;



		-- Now we are going to start updating
		

		-- Create the payment channel record first
				INSERT INTO payments_received (
					date_created,
					student_id,
					school_id,
					payment_channel,
					channel_trans_id,
					channel_memo,
					amount,
					channel_process_date,
					channel_payment_type,
					channel_depositor_name,
					channel_depositor_phone,
					channel_depositor_branch,
					channel_teal_id,
					reversed,
					date_reversed,
					reversal_payment_id,
					reversal,
					payment_reference 
				)
				VALUES
					(
						NOW(),
						student_record.id,
						school_record.id,
						in_payment_channel,
						request_json->>'channelTransactionId',
						request_json->>'channelMemo',
						amount,
						cast(request_json->>'processDate' as timestamp),
						paymentType,
						request_json->>'channelDepositorName',
						request_json->>'channelDepositorPhone',
						request_json->>'channelDepositorBranch',
						request_json ->>'channelTealId',
						false, --reversed
						null, --date reversed
						null, --reversal payment id
						false, --reversal,
						studentPaymentCode
					) RETURNING * INTO payment_record;
		


		
			-- Create payment channel TH and update its balance
			 update
				payment_channel_account_gl
			set
				balance = (payment_channel_account_record.account_balance - amount),
				last_transaction_date = NOW()
			where
				id = payment_channel_account_record.account_id returning *
			into
				payment_channel_account_record;
			
			INSERT INTO payment_channel_account_transaction_history(
				date_created,
				amount,
				description,
				payment_id,
				trans_type,
				account_id,
				reversal_flag,
				reversed,
				date_reversed,
				balance_after,
				balance_before,
				channel_transaction_processed_date
			)
			VALUES
				(
					NOW(), 
					amount * -1, -- Amount is a debit
					format('PMNT_RECV-%s - %s - %s', school_record.school_name, student_record.student_code, student_record.student_name), 
					payment_record.id, 
					'PAYMENT PROCESSED', -- Trans type 
					payment_channel_account_record.id, -- Account id
					false, -- Reversal flag
					false, -- Reversed
					null, -- Date Reversed
					payment_channel_account_record.balance, -- Balance after
					(payment_channel_account_record.balance + amount), -- Balance before it was debited
					payment_record.channel_process_date -- Channel process date
				) ;

		
		
		-- Work on student txns
		student_balance_before = student_account_record.account_balance;
		student_outstanding_balance_before = student_account_record.outstanding_balance;
		
			
		-- If with break down, each to be created separately
		IF feeAssociations IS NOT NULL AND json_array_length(feeAssociations)>0
		THEN
			FOR feeAssocRecord IN SELECT * FROM json_array_elements(feeAssociations)
			LOOP
				
				feeId = cast(feeAssocRecord::json->>'feeId' as int8);
				tenderedAmount = cast(feeAssocRecord::json->>'tenderedAmount' as int8);
				feeMemo = feeAssocRecord::json->>'feeMemo';

				-- Get the fee due ID	 select * from institution_fee_student_association
				SELECT fee_id INTO associated_fee_due_id from institution_fee_student_association WHERE id = feeId;

				student_outstanding_balance_after = student_outstanding_balance_before + tenderedAmount;
				student_balance_after = student_balance_before + tenderedAmount;
				IF student_outstanding_balance_after > 0
				THEN
				-- Set to zero if its greater than 0
				student_outstanding_balance_after = 0;
				END IF;
				


				-- Update the GL and Create TH REcord, indicating fee -- select * from student_account_gl limit 10;
				UPDATE student_account_gl SET account_balance = student_balance_after, outstanding_balance = student_outstanding_balance_after,  last_transaction_date = NOW() WHERE id = student_account_record.id;
				-- Create TH Record For student
				-- Fee description
				if feeMemo IS NULL
				THEN
				feeMemo = format('PAY FEE - %s - %s', payment_channel_record.channel_code, request_json->>'channelTransactionId');
				ELSE
				feeMemo = format('PAY FEE - %s - %s', payment_channel_record.channel_code, feeMemo);
				END IF;
					INSERT INTO student_account_transaction_history (
						date_created,
						amount,
						description,
						fee_association_id,
						payment_id,
						trans_type,
						account_id,
						reversal_flag,
						reversed,
						date_reversed,
						balance_after,
						associated_fee_due,
						outstanding_balance_after
					)
					VALUES
						(
							NOW(),
							tenderedAmount,
							feeMemo,
							feeId,
							payment_record.id,
							'FEE PAYMENT RECEIVED',
							student_account_record.id,
							false, --Reversal
							false, --Reversed
							null, --Date Reversed
							student_balance_after, --Balance after
							associated_fee_due_id, --Associated fee due id
							student_outstanding_balance_after --Associated fee due id
						) ;
				student_outstanding_balance_before = student_outstanding_balance_after; -- Set the before for the next iteration
				student_balance_before = student_balance_after; -- Set the before for the next iteration
					
			END LOOP;

		ELSE
		-- Save only one
				student_outstanding_balance_after = student_outstanding_balance_before + amount;
				student_balance_after = student_balance_before + amount;
			
				IF student_outstanding_balance_after > 0
				THEN
				-- Set to zero if its greater than 0
				student_outstanding_balance_after = 0;
				END IF;
				
				-- Update the GL and Create TH REcord, indicating fee -- select * from student_account_gl limit 10;
				UPDATE student_account_gl SET account_balance = student_balance_after, outstanding_balance = student_outstanding_balance_after,  last_transaction_date = NOW() WHERE id = student_account_record.id returning * into student_account_record;
				-- Create TH Record
				-- 'PAYMENT RECEIVED - " + paymentChannel.getChannelCode() + " - " + paymentRequestWrapper.getChannelTransactionId()
				INSERT INTO student_account_transaction_history (
						date_created,
						amount,
						description,
						fee_association_id,
						payment_id,
						trans_type,
						account_id,
						reversal_flag,
						reversed,
						date_reversed,
						balance_after,
						associated_fee_due,
						outstanding_balance_after
					)
					VALUES
						(
							NOW(),
							amount,
							format('PAYMENT RECEIVED %s - %s', payment_channel_record.channel_code, request_json->>'channelTransactionId'),
							null, -- fee id
							payment_record.id,
							'PAYMENT RECEIVED',
							student_account_record.id,
							false, --Reversal
							false, --Reversed
							null, --Date Reversed
							student_balance_after, --Balance after
							null, --Associated fee due id
							student_outstanding_balance_after --Associated fee due id
						) ;
		
		END IF;


		-- School GL
		UPDATE school_account_gl SET account_balance = (account_balance+amount), last_transaction_date = now() where id = school_account_record.id returning * into school_account_record;
		--School TH
				INSERT INTO school_account_transaction_history (
					date_created,
					amount,
					posting_user,
					description,
					payment_id,
					trans_type,
					account_id,
					reversal_flag,
					reversed,
					date_reversed,
					balance_after
				)
				VALUES
					(
					NOW(),
					amount,
					null, -- posting user
					format('PMNT_RECV-%s - %s - %s', payment_channel_record.channel_code, student_record.student_code, student_record.student_name), -- description "PMNT_RECV-" + paymentChannel.getChannelCode() + " - " + studentInformation.getPaymentCode() + " - " + studentInformation.getFirstName() + " " + studentInformation.getLastName()
					payment_record.id,
					'PAYMENT RECEIVED',
					school_account_record.id,
					false, -- REversal
					false, -- reversed
					null, -- date reversed
					school_account_record.account_balance
					) ;


			-- If we should log for auto settlement select * from payment_channels
			IF payment_channel_record.log_for_auto_settlement IS TRUE	
			THEN 
				
			-- Log for auto settlement
						INSERT INTO auto_settlement_requests (
							date_created,
							source_payment_channel,
							source_payment_channel_code,
							student_code,
							student_name,
							school_name,
							source_channel_trans_id,
							schoolpay_reciept_number,
							payment_id,
							amount,
							payer_phone,
							school_code,
							settled,
							settlement_bank_id
						)
						VALUES
							(
								NOW(),
								payment_channel_record.id,
								payment_channel_record.channel_code,
								student_record.student_code,
								student_record.student_name,
								effective_school_details_record.school_name,
								request_json->>'channelTransactionId',
								payment_record.reciept_number,
								payment_record.id,
								amount,
								request_json->>'channelDepositorPhone',
								effective_school_details_record.school_code, 
								false, -- Settled 
								effective_school_details_record.bank_id
							) ;

			
			END IF;
		
		
--		
--			--Log for external school notification
--		 open fetch_cursor for
--			select
--				tsp.*
--			from
--				thirdparty_provider_school_association tpsa
--				inner join thirdparty_school_providers tsp on tpsa.provider_id = tsp.id 
--			where
--				tpsa.school_id = school_record.id
--			limit 1;
--				FETCH fetch_cursor INTO provider_record;
--		close fetch_cursor;			
--		
--		if provider_record.id is not null
--		then
--			insert
--				into
--				thirdparty_provider_payment_notification_queue (
--				payment_id,
--				provider_id,
--				sent_to_provider,
--				provider_notification_reference)
--			values(
--			payment_record.id,
--			provider_record.id,
--			false,
--			false);
--		end if;
		

	-- Check if payment plans are enabled and if so apportion the amounts
	if exists (select 1 from school_module_association sma inner join schoolpay_modules sm on sm.id = sma.module_id where sm.module_code  = 'PAYMENT_PLANS_MODULE' and sma.school_id = student_record.school_id)
	then 
		perform apportion_payment_amount_to_plans(student_record.id, amount);
	end if;

--fee association priority;

		perform apportion_payment_amount_to_fees(student_record.id, payment_record.id, amount);

	
		-- Return details as json
		-- Parameter validations
RETURN row_to_json (DATA)
FROM
	(

		VALUES
			(
				0,
				format('School fees payment has been processed succesfully. Reference:%s', payment_record.reciept_number),
				school_record.school_code,
				school_record.school_name,
				student_record.first_name,
				student_record.middle_name,
				student_record.last_name,
				format('%s - %s', student_record.class_code, student_record.class_description),
				payment_record.reciept_number,
				to_json(student_record),
				payment_record.id,
				student_record.student_name
			)
	) DATA ("returnCode", "returnMessage", "schoolCode", "schoolName", "firstName", "middleName", "lastName", "studentClass", "receiptNumber", "studentDetails", "paymentId", "studentFullNames") ;

end;
$function$

CREATE OR REPLACE FUNCTION public.sp_student_inquiry(in_payment_channel bigint, in_student_code character varying, in_amount numeric)
 RETURNS json
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

studentPaymentCode varchar;
student_record record;
section_record record;
bank_record record;
bank_account_records record;
payment_channel_account_record record;
school_details_record record;
mtn_hosted_service_record record;
mtn_ova varchar;

fetch_cursor refcursor;

associated_fees json;
minimum_amount numeric;
percentage_lock_record record;
class_code varchar;

begin
	
	studentPaymentCode = upper(trim(replace(in_student_code, ' ', '')));
	
	-- Get the student
		IF studentPaymentCode ~ '^((9)\d{9})$' -- Payment code or not
		then
			OPEN fetch_cursor for 
				select
				si.id,
				si.first_name,
				si.middle_name,
				si.last_name,
				si.guardian_phone ,
				si.school_id,
				si.active,
				si.student_account_id,
				si.student_code,
				isc.class_code ,
				isc.class_description,
				si.class_id,
				si.archived,
				sch.school_code,
				sch.school_name,
				sch.active as school_active,
				sch.default_part_payment_behaviour,
				sag.account_balance,
				si.school_student_registration_number,
				si.date_of_birth,
				case when sag.outstanding_balance > 0 then 0 else abs(sag.outstanding_balance) end as outstanding_balance,
				case when sch.default_part_payment_behaviour = true then true else si.allow_part_payments end as student_part_payments,
				format('%s%s%s', first_name,
				case
					when middle_name is not null then ' ' || middle_name
					else ''
				end,
				case
					when last_name is not null then ' ' || last_name
					else ''
				end) as student_name,
				case
					when si.allow_part_payments is null then true
					else si.allow_part_payments
				end as part_payments
			from
				core_student si
			inner join core_school sch on
				sch.id = si.school_id
			inner join core_school_class isc on
				si.class_id = isc.id
			inner join student_account_gl sag on sag.id = si.student_account_id 
			where
				student_code = cast(studentPaymentCode as int8)
			limit 1;
			FETCH fetch_cursor INTO student_record;
			close fetch_cursor;	
		ELSE
		studentPaymentCode = upper(studentPaymentCode);
		-- How many students have this reg no?			
		IF (SELECT count(id) from core_student where school_student_registration_number=studentPaymentCode) > 1
		THEN
		RETURN  row_to_json(data) FROM (VALUES (407, 'Student selection failed. Use the 10 digit payment code to select specific student')) data("returnCode", "returnMessage");
		END IF;
			OPEN fetch_cursor for 
			select
					si.id,
					si.first_name,
					si.middle_name,
					si.last_name,
					si.guardian_phone,
					si.school_id,
					si.active,
					si.student_account_id,
					si.student_code,
					isc.class_code ,
					isc.class_description,
					si.class_id,
					si.archived,
					sch.school_code,
					sch.school_name,
					sch.active as school_active,
					sch.default_part_payment_behaviour,
					sag.account_balance,
					si.school_student_registration_number,
					si.date_of_birth,
					case when sag.outstanding_balance > 0 then 0 else abs(sag.outstanding_balance) end as outstanding_balance,
					case when sch.default_part_payment_behaviour = true then true else si.allow_part_payments end as student_part_payments,
					format('%s%s%s', first_name,
					case
					when middle_name is not null then ' ' || middle_name
					else ''
					end,
					case
						when last_name is not null then ' ' || last_name
						else ''
					end) as student_name,
					case when si.allow_part_payments is null then true else si.allow_part_payments end as part_payments
				from
					core_student si
					inner join core_school sch on sch.id = si.school_id 
				inner join core_school_class isc on
					si.class_id  = isc.id
				inner join student_account_gl sag on sag.id = si.student_account_id 
				where
					school_student_registration_number = studentPaymentCode
				limit 1;
			
			
			FETCH fetch_cursor INTO student_record;
			close fetch_cursor;	
		END IF;
	
		-- Check if we have something atleast
		IF student_record.id IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (404, 'No student was found with the specified payment code or registration number.')) data("returnCode", "returnMessage");
		END IF;	

	-- Get the school details
	select * from sp_resolve_student_school_details(student_record.id) into school_details_record;
	
	if school_details_record.return_code <> 0
	then
	RETURN  row_to_json(data) FROM (VALUES (school_details_record.return_code, school_details_record.return_message)) data("returnCode", "returnMessage");
	end if;

	--get account number 

	select * from core_bank_account_details cbad into bank_account_records where bank_id =school_details_record.bank_id  and school_id =student_record.school_id;
	-- Get payment channel
	select * from sp_resolve_payment_channel_account(in_payment_channel, school_details_record.bank_id) into payment_channel_account_record;
	if payment_channel_account_record.return_code <> 0
	then
	RETURN  row_to_json(data) FROM (VALUES (payment_channel_account_record.return_code, payment_channel_account_record.return_message)) data("returnCode", "returnMessage");
	end if;

	-- DOES THE PAYMENT CHANNEL HAVE ENOUGH BALANCE 
		IF  in_amount is not null and in_amount<>0 and  (payment_channel_account_record.account_balance - in_amount) < payment_channel_account_record.account_limit
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (309, 'INSUFFICIENT BALANCE. PAYMENT CHANNEL DOESN''T HAVE SUFFICIENT BALANCE FOR THIS OPERATION')) data("returnCode", "returnMessage");
		END IF;
		
		-- CHECK IF STUDENT IS NOT ACTIVE
		IF student_record.active = false and student_record.archived = false
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (502, 'INACTIVE: STUDENT IS NOT ACTIVE. ALL FUTURE REQUESTS FOR STUDENT WILL FAIL.')) data("returnCode", "returnMessage");
		END IF;



		-- is school active
		IF student_record.school_active = FALSE
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (501, 'INACTIVE: THE ASSOCIATED SCHOOL / INSTITUTION IS NOT ACTIVE')) data("returnCode", "returnMessage");
		END IF;

   --School and payment channel should be associated
   if not exists (select 1 from institution_payment_channel_association ipca where ipca.institution = student_record.school_id and ipca.payment_channel = in_payment_channel)
   then 
   RETURN  row_to_json(data) FROM (VALUES (511, 'NOT ALLOWED. School ' || student_record.school_name || ' does not accept payments from ' || payment_channel_account_record.channel_name )) data("returnCode", "returnMessage");
   end if;
  
  -- CHECK IF WE SHOULD ALLOW PART PAYMENTS select * from student_account_gl limit 10;
		IF 	student_record.default_part_payment_behaviour = false and in_amount is not null and  in_amount<>0 
		THEN
			IF student_record.part_payments = FALSE AND in_amount < ABS(student_record.outstanding_balance)
			THEN
			--ITS A PART PAYMENT
			RETURN  row_to_json(data) FROM (VALUES (204, 'Sorry, Student ' || student_record.student_name || ' is not allowed to make part payments. Minimum amount allowed is ' || (ABS(student_record.outstanding_balance)) )) data("returnCode", "returnMessage");
			END IF;
		end if;
		
		

	--If mtn channel, return the ova as well
	if 	payment_channel_account_record.channel_code = 'MTN_UG'
	then
			OPEN fetch_cursor for 
				select * from mtn_hosted_services mhs where mhs.service_id = format('SCHOOLPAY-%s', school_details_record.bank_code);
			FETCH fetch_cursor INTO mtn_hosted_service_record;
			close fetch_cursor;	
		
		if mtn_hosted_service_record.id is null
	    then
	    	RETURN  row_to_json(data) FROM (VALUES (1204, 'MTN Payment channel is not correctly configured on the Schoolpay Interface')) data("returnCode", "returnMessage");
	    end if;
	   mtn_ova = mtn_hosted_service_record.mtn_ova;
	end if;
  

	select
		json_agg(c)
	from
		(
		select
			ifd.description as "feeDescription",
			ifd.due_amount as "feeAmount",
			ifsa.id as "feeId",
			ifd.effective_date as "feeDueFromDate",
			ifd.end_date as "feeDueToDate"
			
		from
			institution_fee_student_association ifsa
		inner join institution_fees_due ifd on
			ifsa.fee_id = ifd.id 
			where ifsa.student_id = student_record.id 
			and ifd.end_date >= current_date) c
			into associated_fees;
    
		if associated_fees is null
		then
		associated_fees = '[]'::json;
		end if;

	--Compute minimum amount
	minimum_amount = 0;
    if student_record.student_part_payments <> true
    then
    minimum_amount = abs(student_record.outstanding_balance);
    end if;
   
   
   --Validate percentage locks as well
	select * from school_minimum_fee_percentage_rules smfpr 
	where smfpr.school_id = student_record.school_id and smfpr.minimum_percentage > 0 and smfpr.minimum_percentage < 100 
	into percentage_lock_record;
	
	if percentage_lock_record is not null
	then
		minimum_amount = abs(floor(percentage_lock_record.minimum_percentage * student_record.outstanding_balance / 100));
		if in_amount is not null and  in_amount<>0  and in_amount < minimum_amount
		then
			RETURN  row_to_json(data) FROM (VALUES (907, 'School requires atleast ' || percentage_lock_record.minimum_percentage || ' percent of the outstanding balance. ' || 'Pay atleast UGX ' || trim(to_char(minimum_amount, '999,999,999,999,999')) )) data("returnCode", "returnMessage");
		end if;
	end if;


	--For mobile channels use shorter class code
	if payment_channel_account_record.channel_type = 'Mobile'
	then
		class_code = trim(left(student_record.class_code, 15));
	else
		class_code = trim(format('%s - %s', student_record.class_code, student_record.class_description));
	end if;

RETURN row_to_json (DATA)
FROM
	(

		VALUES
			(
				0,
				student_record.student_name,
				school_details_record.school_code,
				upper(school_details_record.school_name),
				student_record.first_name,
				student_record.middle_name,
				student_record.last_name,
				class_code,
				school_details_record.bank_code,
				mtn_ova,
				associated_fees,
				student_record.student_code,
				student_record.student_part_payments,
				student_record.outstanding_balance,
				minimum_amount,
				student_record.school_student_registration_number,
				student_record.date_of_birth,
				bank_account_records.account_number
				
			)
	) DATA ("returnCode", "returnMessage", "schoolCode", "schoolName", "firstName", "middleName", "lastName", "studentClass", "bankId", "mtnOva", "associatedFees", "paymentCode", "allowPartPayments", "outstandingAmount", "minimumAmount", "registrationNumber", "dateOfBirth","accountNumber") ;

end;
$function$

CREATE OR REPLACE FUNCTION public.apply_current_fees_to_student_on_creation()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare 

student_school_id int = null;
student_class_id int = null;
this_student_id int = NEW.id;
the_fee_id int;

BEGIN

-- FIND fees associated to the student's class and are valid
-- and apply the fee to this student

	FOR the_fee_id IN
				select ifd.id as the_fee_id from institution_fees_due ifd 
		inner join institution_fee_class_association fda on ifd.id = fda.fee_id
		inner join core_student si on si.class_id = fda.class_id
		where si.id = this_student_id 
		and ifd.approval_status = true
		and ifd.effective_date <= current_date
		and ifd.end_date >= current_date
		and ifd.recurrent = false
   LOOP
      --apply school to payment channel
				PERFORM apply_fee_to_student(this_student_id, the_fee_id, 0, 'SYSTEM', '');
   END LOOP;
		


RETURN NEW;
END;

$function$

CREATE OR REPLACE FUNCTION public.apply_student_penalty(in_fee_student_association_id bigint)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare


    record_student          json;
    cursor_fee_association     cursor for
        select student.id as student_id, student.student_account_id, penalty_type, penalty_frequency,
               student.payment_code , penalty_amount, fee_outstanding_balance, last_penalty_apply_date,
               next_penalty_apply_date, penalties_closed, penalties_outstanding_balance, assoc.id as associd,
               penalty_apply_frequency_unit, penalty_apply_frequency_term,  fee.description as fee_description,
               fee.id as fee_id, penalty_expiry_date, maximum_penalty_amount, maximum_penalty_application_count,
               assoc.penalty_application_count, student_gl.account_balance as student_account_balance
        from institution_fee_student_association assoc
                 inner join core_student student on assoc.student_id=student.id
                 inner join institution_fees_due fee on assoc.fee_id=fee.id
                 inner join institution_fees_due_penalty penalty on fee.penalty_id = penalty.id
 inner join student_account_gl student_gl on student.student_account_id = student_gl.id
        where assoc.id = in_fee_student_association_id
          and assoc.penalties_closed=false
          and assoc.next_penalty_apply_date <= current_date
          and fee_outstanding_balance < 0
          and assoc.has_penalty = true;

    --temps
    temp_amount              numeric;
    balance_before              numeric;
    balance_after              numeric;
    this_penalty_next_apply_date date;
    record_fee_association record;
    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    penalty_applied boolean = false;
    this_student_end_balance numeric;
    new_outstanding_balance numeric;

begin
    -- Open row cursor
    open cursor_fee_association;
    loop
        fetch cursor_fee_association into record_fee_association;
        exit when not found;

        --if penalty expiry date has passed or penalty_amount for this record has been exceeded
        -- then close the penalty and continue
        IF cast(record_fee_association.penalty_expiry_date as date) < current_date
        then
            raise notice 'Penalty with id % has reached the expiry date', record_fee_association.associd;
            penalty_applied = false;
            UPDATE institution_fee_student_association set
                penalties_closed = true
            where id = record_fee_association.associd;
            continue;
        end if;

        -- if record_fee_association.maximum_penalty_amount <> 0 and penalty has exceeded limit them leave it
        IF abs(record_fee_association.penalties_outstanding_balance)>=abs(record_fee_association.maximum_penalty_amount) and record_fee_association.maximum_penalty_amount <>0
        then
            raise notice 'Penalty with id % has reached the maximum penalty amount constraints', record_fee_association.associd;
            penalty_applied = false;
            UPDATE institution_fee_student_association set
                penalties_closed = true
            where id = record_fee_association.associd;
            continue;
        end if;

        -- if record_fee_association.maximum_penalty_application_count <> 0 and penalty application count has exceeded the maximum
        IF abs(record_fee_association.penalty_application_count)>=abs(record_fee_association.maximum_penalty_application_count) and record_fee_association.maximum_penalty_application_count <>0
        then
            raise notice 'Penalty with id % has reached the maximum number of applications', record_fee_association.associd;
            penalty_applied = false;
            UPDATE institution_fee_student_association set
                penalties_closed = true
            where id = record_fee_association.associd;
            continue;
        end if;

if record_fee_association.student_account_balance >= 0
then
-- Student has a penalty but for some reason the balance is not negative
-- This is an opportunity to block further penalties
 raise notice 'Penalty fee with id % is being skipped due to positive student balance', record_fee_association.associd;
UPDATE institution_fee_student_association set
                penalties_closed = true
            where id = record_fee_association.associd;
            continue;
end if;


        IF record_fee_association.penalty_type = 'FIXED'
        then
            temp_amount = record_fee_association.penalty_amount;
        else
            --Compute as percantage of outstanding
            temp_amount = (record_fee_association.penalty_amount * abs(record_fee_association.fee_outstanding_balance)) / 100.0;
        end if;




        --Adjust and set balance to balance minus adjustment amount
        UPDATE institution_fee_student_association set
            penalties_outstanding_balance = penalties_outstanding_balance - temp_amount
        where id = record_fee_association.associd;

        -- if fixed penalty
        if record_fee_association.penalty_frequency = 'ONETIME'
        then
            UPDATE institution_fee_student_association set
                penalties_closed = true
            where id = record_fee_association.associd;
        end if;



        -- Create fee performance th record
        INSERT INTO institution_fees_due_student_payment_transaction_history
        ("date_created",
         "payment_id",
         "fee_id",
         "fee_student_association_id",
         "student_id",
         "amount",
         "balance_before",
         "balance_after",
         "description",
         "penalty_amount",
         "penalty_balance_before",
         "penalty_balance_after"
        )
        VALUES (
                   now(),
                   0,
                   record_fee_association.fee_id,
                   record_fee_association.associd,
                   record_fee_association.student_id,
                   0,
                   record_fee_association.fee_outstanding_balance, -- balances havent changed
                   record_fee_association.fee_outstanding_balance, -- balances havent changed
                   format('PENALTY - %s', temp_amount),
                   temp_amount * -1,
                   record_fee_association.penalties_outstanding_balance,
                   record_fee_association.penalties_outstanding_balance - temp_amount
               );




        -- Updating student balances too
        SELECT
            account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
        FROM
            student_account_gl
        WHERE
                ID = record_fee_association.student_account_id FOR SHARE ;

        -- Adjust the new balance
        this_student_end_balance = this_student_current_balance - temp_amount;
        new_outstanding_balance = this_student_current_outstanding_balance - temp_amount;

        IF new_outstanding_balance > 0 THEN
            new_outstanding_balance = 0;
        END IF;


        -- update balance
        UPDATE student_account_gl SET account_balance = this_student_end_balance, outstanding_balance = new_outstanding_balance WHERE id = record_fee_association.student_account_id;

        -- Create th record
        INSERT INTO student_account_transaction_history (
            date_created,
            amount,
            description,
            payment_id,
            trans_type,
            account_id,
            reversal_flag,
            reversed,
            date_reversed,
            balance_after,
            associated_fee_due,
            outstanding_balance_after,
            fee_association_id
        )
        VALUES
        (
            now(),
            - 1 * temp_amount,
            'PENALTY ON - ' || record_fee_association.fee_description,
            NULL,
            'PENALTY',
            record_fee_association.student_account_id,
            FALSE,
            FALSE,
            NULL,
            this_student_end_balance,
            record_fee_association.fee_id,
            new_outstanding_balance,
            record_fee_association.associd
        ) ;




        -- Adjust next apply date
        -- If penalty, we are to save the penalty apply date
        this_penalty_next_apply_date = null;
        if record_fee_association.penalty_apply_frequency_unit = 'D'
        then
            this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 day' * record_fee_association.penalty_apply_frequency_term;
        elsif record_fee_association.penalty_apply_frequency_unit = 'W' then
            this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 week' * record_fee_association.penalty_apply_frequency_term;
        else
            this_penalty_next_apply_date = record_fee_association.next_penalty_apply_date + interval '1 month' * record_fee_association.penalty_apply_frequency_term;
        end if;

        -- update next apply date, last apply date to today select * from institution_fee_student_association
        update institution_fee_student_association set last_penalty_apply_date = current_date, next_penalty_apply_date = this_penalty_next_apply_date,penalties_accrued=(penalties_accrued + abs(temp_amount)),penalty_application_count=penalty_application_count+1  where id = record_fee_association.associd;



        raise notice 'Applied penalty for association id  % ', record_fee_association.associd;
        penalty_applied = true;

    end loop;
    close cursor_fee_association;


    if penalty_applied = false
    then
        RETURN row_to_json(data) FROM ( VALUES (0, 'No penalty to apply was found')) data (returncode, returnmessage);
    end if;

    RETURN row_to_json(data) FROM ( VALUES (0, 'Penalty applied')) data (returncode, returnmessage);
end;
$function$

CREATE OR REPLACE FUNCTION public.apportion_payment_amount_to_fees(in_student_id bigint, in_payment_id bigint, in_payment_amount numeric)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare


    record_student          json;
    record_association          json;
    cursor_fee_association     cursor for
        select assoc.* from institution_fee_student_association assoc
                                inner join institution_fees_due fee on assoc.fee_id  = fee.id
        where student_id = in_student_id and fee_outstanding_balance<0 order by fee.priority, fee.id;
    record_fee_association record;

    --temps
    temp_amount              numeric;
    temp_balance              numeric;
    balance_before              numeric;
    balance_after              numeric;
    payment_date date;
		student_account_balance numeric;

begin
    -- Get student
    select get_record_by_int8(in_student_id, 'core_student', 'id')
    into record_student;
    IF (record_student ->> 'id') IS NULL
    THEN
        -- There was a problem getting the school
        RETURN row_to_json(data) FROM ( VALUES (199, 'Student was not found')) data (returncode, returnmessage);
    END IF;


    IF NOT EXISTS (select 1 from institution_fee_student_association where student_id = in_student_id and fee_outstanding_balance<0)
    then
        -- No schedules updatable
        RETURN row_to_json(data) FROM ( VALUES (0, 'No outstanding updatable fees for this student')) data (returncode, returnmessage);
    end if;

    if in_payment_amount <= 0 then
        RETURN row_to_json(data) FROM ( VALUES (100, 'Payment amount can not be less than 0')) data (returncode, returnmessage);
    end if;


    -- Open row cursor
    temp_balance := in_payment_amount;
    open cursor_fee_association;
    loop
        fetch cursor_fee_association into record_fee_association;
        exit when not found;
        exit when temp_balance <= 0;

        --get the payment date
        select date_created into payment_date from payments_received where id = in_payment_id;


        balance_before = record_fee_association.fee_outstanding_balance;

        temp_amount := abs(record_fee_association.fee_outstanding_balance); --the amount to apportion

        if temp_balance < temp_amount then
            -- Amount remaining is less than fee balance
            temp_amount = temp_balance;
        end if;

        balance_after = balance_before + temp_amount;


        --Adjust and set balance to balance minus adjustment amount
        UPDATE institution_fee_student_association set
            fee_outstanding_balance = fee_outstanding_balance + temp_amount
        where id = record_fee_association.id;

        -- If amount for this fee has been paid in full, set penalties closed as true
        UPDATE institution_fee_student_association set
            penalties_closed = true
        where id = record_fee_association.id and fee_outstanding_balance>=0;

        temp_balance := temp_balance - temp_amount;

        -- Create fee performance th record
        -- select * from institution_fee_student_association
        -- select * from institution_fees_due_student_payment_transaction_history


        INSERT INTO institution_fees_due_student_payment_transaction_history
        ("date_created",
         "payment_id",
         "fee_id",
         "fee_student_association_id",
         "student_id",
         "amount",
         "balance_before",
         "balance_after",
         "description",
         "penalty_amount",
         "penalty_balance_before",
         "penalty_balance_after"
        )
        VALUES (
                   payment_date,
                   in_payment_id,
                   record_fee_association.fee_id,
                   record_fee_association.id,
                   record_fee_association.student_id,
                   temp_amount,
                   balance_before,
                   balance_after,
                   format('PAYMENT MADE - %s', temp_amount),
                   0,
                   record_fee_association.penalties_outstanding_balance,
                   record_fee_association.penalties_outstanding_balance
               );




        -- Now also apportion to penalty if theres a penalty
        -- if no penalty, continue the loop
        IF record_fee_association.has_penalty = false
        then
            raise notice 'Association has no penalty';
            continue;
        end if;

        IF record_fee_association.penalties_outstanding_balance >= 0
        then
            raise notice 'No penalty outstanding for this association';
            continue;
        end if;


        -- At this point there's a penalty to be applied

        balance_before = record_fee_association.penalties_outstanding_balance;

        temp_amount := abs(record_fee_association.penalties_outstanding_balance); --the amount to apportion

        if temp_balance < temp_amount then
            -- Amount remaining is less than fee balance
            temp_amount = temp_balance;
        end if;

        balance_after = balance_before + temp_amount;


        --Adjust and set balance to balance minus adjustment amount
        UPDATE institution_fee_student_association set
            penalties_outstanding_balance = penalties_outstanding_balance + temp_amount
        where id = record_fee_association.id;

        temp_balance := temp_balance - temp_amount;

        -- Create fee performance th record
        -- select * from institution_fee_student_association
        -- select * from institution_fees_due_student_payment_transaction_history

        -- This record will help us get the fee balances for penalty th update
        select get_record_by_int8(record_fee_association.id, 'institution_fee_student_association', 'id')
        into record_association;

        INSERT INTO institution_fees_due_student_payment_transaction_history
        ("date_created",
         "payment_id",
         "fee_id",
         "fee_student_association_id",
         "student_id",
         "amount",
         "balance_before",
         "balance_after",
         "description",
         "penalty_amount",
         "penalty_balance_before",
         "penalty_balance_after"
        )
        VALUES (
                   payment_date,
                   in_payment_id,
                   record_fee_association.fee_id,
                   record_fee_association.id,
                   record_fee_association.student_id,
                   temp_amount, -- amt is zero record_student ->> 'id'
                   cast((record_association->>'fee_outstanding_balance') as numeric),
                   cast((record_association->>'fee_outstanding_balance') as numeric),
                   format('PENALTY PAYMENT - %s', temp_amount),
                   0,
                   balance_before,
                   balance_after
               );





        raise notice 'Apportioned % to fee association with id ', record_fee_association.id;
				
				-- Get the student account balance, if its greater than or equal to zero,
				-- Set all fees associations as closed and balances to zero
				select account_balance into student_account_balance from student_account_gl where id = (select student_account_id from student_information where id = in_student_id);
				if student_account_balance >=0 then
				 raise notice 'Since student balance is zero, clearing outstanding balances as well now';
				update institution_fee_student_association set fee_outstanding_balance=0, penalties_outstanding_balance=0, penalties_closed = true where student_id = in_student_id and fee_outstanding_balance<0;
				end if;
				

    end loop;
    close cursor_fee_association;



    RETURN row_to_json(data) FROM ( VALUES (0, 'Fees updated')) data (returncode, returnmessage);
end;
$function$

CREATE OR REPLACE FUNCTION public.archive_student(this_student_id bigint, this_reason_for_archive character varying, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
this_student_school_id int8;
this_student_current_class_id int8;
this_student_new_class_id int8;
current_archive_status boolean;


begin

		if NOT exists (select 1 from core_student where id = this_student_id) THEN
			RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id;
				return false;
		end if;


		-- Select the student class ID
		SELECT class_id, school_id, archived INTO this_student_current_class_id, this_student_school_id, current_archive_status FROM core_student where id = this_student_id;

		IF current_archive_status = true THEN
			RAISE NOTICE 'STUDENT % IS ALREADY ARCHIVED' , this_student_id;
			RETURN false;
		END IF;

		-- Check if this school as an archive class 
		SELECT id into this_student_new_class_id FROM core_school_class WHERE school_id = this_student_school_id AND class_code = '__ARCHIVE__';
		IF this_student_new_class_id IS NULL THEN
		RAISE NOTICE 'CREATING NON EXISTENT ARCHIVE CLASS FOR SCHOOL %', this_student_school_id;
		INSERT INTO core_school_class (school_id, class_code, class_description, date_created) 
			values(this_student_school_id, '__ARCHIVE__', 'Archive Class', now()) RETURNING id into this_student_new_class_id;
		END IF;

		-- LEts update now
		update core_student set class_id = this_student_new_class_id , active = FALSE,
		archived = true, date_archived=now(), archive_reason=this_reason_for_archive, class_when_archived = this_student_current_class_id
		where id = this_student_id;
		

		-- Log now
		INSERT INTO web_console_log(
            ip_address, user_name, event_date, event_action, affected_student_id, 
            errors, web_user_id)
    VALUES (ip_address, web_user_name, now(), 
		'Archive student ', this_student_id, 
            '', web_user_id);
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.archive_user(this_user_id bigint, this_reason_for_archive character varying, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
current_archive_status boolean;


begin

		if NOT exists (select 1 from "user" where id = this_user_id) THEN
			RAISE NOTICE 'USER WITH ID % NOT FOUND', this_user_id;
				return false;
		end if;


	

		IF current_archive_status = true THEN
			RAISE NOTICE 'USER % IS ALREADY ARCHIVED' , this_user_id;
			RETURN false;
		END IF;

		-- LEts update now
		update "user" set archived = true, date_archived=now(), archive_reason=this_reason_for_archive
		where id = this_user_id;
		

		-- Log now
		INSERT INTO web_console_log(
            ip_address, user_name, event_date, event_action, affected_student_id, 
            errors, web_user_id)
    VALUES (ip_address, web_user_name, now(), 
		'Archive user ', this_user_id, 
            '', web_user_id);
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.associate_school_with_default_payment_channels(this_school_id numeric)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
payment_channel_id numeric;
school_bank_id numeric;
begin

		-- Get the school bank id
		SELECT bank_name into school_bank_id from core_school where id = this_school_id;

		FOR payment_channel_id IN
      SELECT payment_channel from bank_payment_channel_association WHERE bank_id = school_bank_id
   LOOP
      --apply school to payment channel
				PERFORM associate_school_with_payment_channel(this_school_id, payment_channel_id);
   END LOOP;
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.change_student_class(this_student_id bigint, this_new_class_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
this_student_school_id int8;
this_student_current_class_id int8;
this_student_current_class_description varchar;
this_student_new_class_description varchar;
this_student_new_class_school_id int8;

begin
		--Fetch the student account using payment code 
		if NOT exists (select 1 from core_school_class where id = this_new_class_id) THEN
				RAISE EXCEPTION 'NEW CLASS DOES NOT EXIST';
		end if;

		if NOT exists (select 1 from core_student where id = this_student_id) THEN
			RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id;
				return false;
		end if;


		-- Select the student class ID
		SELECT class_id, school_id INTO this_student_current_class_id, this_student_school_id FROM core_student where id = this_student_id;

		IF this_student_current_class_id = this_new_class_id THEN
			RETURN false;
		END IF;

		--Get the current class description for logging purposes
		SELECT class_description into this_student_current_class_description from core_school_class where id = this_student_current_class_id; 
		
		-- Get the new class details
		SELECT class_description, school_id into this_student_new_class_description, this_student_new_class_school_id from core_school_class where id = this_new_class_id; 

		if this_student_new_class_school_id <> this_student_school_id THEN
			RAISE EXCEPTION 'STUDENT CLASS SCHOOL MISMATCH';
		end if;

		-- LEts update now
		update core_student set class_id = this_new_class_id where id = this_student_id;
		

		-- Log now
		INSERT INTO web_console_log(
            ip_address, user_name, event_date, event_action, affected_student_id, 
            errors, web_user_id)
    VALUES (ip_address, web_user_name, now(), 
		'Change class from ' || this_student_current_class_description || ' to ' || this_student_new_class_description, this_student_id, 
            '', web_user_id);
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.check_applied_status_before_delete_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
		IF OLD.applied = true
		THEN
		RAISE EXCEPTION 'FEE CLASS ASSOCIATION CAN NOT BE UNDONE BECAUSE FEE ASSOCIATION HAS BEEN APPLIED TO ALL STUDENTS';
		END IF;
    RETURN OLD;
END;
$function$

CREATE OR REPLACE FUNCTION public.check_fee_and_class_school_for_association_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare 

fee_school_id int = null;
class_school_id int = null;

BEGIN
-- Get the school ids for the fee and CLASS
SELECT school_id INTO class_school_id FROM core_school_class where id = NEW.class_id;
SELECT school_id INTO fee_school_id FROM institution_fees_due where id = NEW.fee_id;

IF fee_school_id <> class_school_id THEN
-- fail since school id for class and fee aint the same
raise EXCEPTION 'FEE AND CLASS ASSOCIATIONS ARE ONLY POSSIBLE FOR A FEE AND CLASS BELONGING TO THE SAME SCHOOL'; 
END IF;
RETURN NEW;
END;

$function$

CREATE OR REPLACE FUNCTION public.create_payment_channel_account_on_payment_channel_creation_trig()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

declare 

created_account_id int = null;



BEGIN

-- If account id is not passed then add it 



IF NEW.payment_channel_account_id IS NULL THEN

-- try to create the account id

INSERT INTO payment_channel_account_gl (description, balance)

VALUES (NEW.channel_name || ' - Payment Channel A/C', 999999999999999999) returning id into created_account_id;

NEW.payment_channel_account_id = created_account_id;

RETURN NEW;

END IF;



RETURN NEW;

END;



$function$

CREATE OR REPLACE FUNCTION public.create_school_account_on_school_creation_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$







declare 















created_account_id int = null;















BEGIN







-- If account id is not passed then add it 















IF NEW.school_account_id IS NULL THEN







-- try to create the account id







INSERT INTO school_account_gl (account_title, account_balance)







VALUES (NEW.school_name || ' - School A/C', 0) returning id into created_account_id;







NEW.school_account_id = created_account_id;







RETURN NEW;







END IF;















RETURN NEW;







END;















$function$

CREATE OR REPLACE FUNCTION public.create_school_class(this_school_id numeric, this_class_code text, this_class_description text)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare

begin
		--Fetch the student account using payment code 
		IF NOT exists (select 1 from core_school where id = this_school_id) THEN
				RAISE EXCEPTION 'SCHOOL NOT FOUND WITH THE SPECIFIED SCHOOL ID';
		end if;


		-- Check if student has transacted atleast once ,if so then fail
		If exists (select 1 from core_school_class where class_code = this_class_code and institution=this_school_id) THEN
				RAISE NOTICE 'CLASS WITH THIS CODE IS ALREADY EXISTENT FOR THIS SCHOOL';
				RETURN false;
		end if;

		--CREATE THE CLASS
		INSERT INTO core_school_class (institution, class_code, class_description, date_created)
			VALUES (this_school_id, this_class_code, this_class_description, now());

		RAISE NOTICE 'CLASS % HAS BEEN CREATED', this_class_code;
		
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.create_student_account_on_student_creation_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$

declare 



created_account_id int = null;



BEGIN

-- If account id is not passed then add it 



IF NEW.student_account_id IS NULL THEN

-- try to create the account id

INSERT INTO student_account_gl (account_title, account_balance)

VALUES (NEW.first_name || ' ' || NEW.last_name || ' - Student A/C', 0) returning id into created_account_id;

NEW.student_account_id = created_account_id;

RETURN NEW;

END IF;



RETURN NEW;

END;



$function$

CREATE OR REPLACE FUNCTION public.datediff(units character varying, start_t timestamp without time zone, end_t timestamp without time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
   DECLARE
     diff_interval INTERVAL; 
     diff INT = 0;
     years_diff INT = 0;
   BEGIN
     IF units IN ('yy', 'yyyy', 'year', 'mm', 'm', 'month') THEN
       years_diff = DATE_PART('year', end_t) - DATE_PART('year', start_t);
 
       IF units IN ('yy', 'yyyy', 'year') THEN
         -- SQL Server does not count full years passed (only difference between year parts)
         RETURN years_diff;
       ELSE
         -- If end month is less than start month it will subtracted
         RETURN years_diff * 12 + (DATE_PART('month', end_t) - DATE_PART('month', start_t)); 
       END IF;
     END IF;
 
     -- Minus operator returns interval 'DDD days HH:MI:SS'  
     diff_interval = end_t - start_t;
 
     diff = diff + DATE_PART('day', diff_interval);
 
     IF units IN ('wk', 'ww', 'week') THEN
       diff = diff/7;
       RETURN diff;
     END IF;
 
     IF units IN ('dd', 'd', 'day') THEN
       RETURN diff;
     END IF;
 
     diff = diff * 24 + DATE_PART('hour', diff_interval); 
 
     IF units IN ('hh', 'hour') THEN
        RETURN diff;
     END IF;
 
     diff = diff * 60 + DATE_PART('minute', diff_interval);
 
     IF units IN ('mi', 'n', 'minute') THEN
        RETURN diff;
     END IF;
 
     diff = diff * 60 + DATE_PART('second', diff_interval);
 
     RETURN diff;
   END;
   $function$

CREATE OR REPLACE FUNCTION public.drop_school_details(this_school_code bigint)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
this_school_account_id int8;
this_school_id int8;
this_logo_id int8;

begin
		--Fetch the student account using payment code 
		IF NOT exists (select 1 from core_school where school_code = this_school_code) THEN
				RAISE EXCEPTION 'SCHOOL NOT FOUND WITH THE SPECIFIED CODE';
		end if;

		-- Select the student payment code
		SELECT id, school_account_id, school_logo into this_school_id, this_school_account_id, this_logo_id from core_school where school_code = this_school_code;
		
		-- Check if student has transacted atleast once ,if so then fail
		If exists (select 1 from school_account_transaction_history where account_id = this_school_account_id) THEN
				RAISE EXCEPTION 'SCHOOL CAN NOT BE DROPPED BECAUSE TRANSACTIONS EXIST FOR THEM';
		end if;

		If exists (select 1 from core_student where school_id = this_school_id) THEN
				RAISE EXCEPTION 'SCHOOL CAN NOT BE DROPPED BECAUSE IT HAS STUDENTS';
		end if;

		-- Delete CHANNEL ASSOCIATIONS
		DELETE FROM institution_payment_channel_association where institution = this_school_id;
		

		-- Delete classes
		DELETE FROM core_school_class where school_id = this_school_id;

		--Delete from accounts
		DELETE FROM core_bank_account_details WHERE school_id = this_school_id;
		
		-- Delete users
		DELETE FROM "user" WHERE school_id = this_school_id;

		-- Delete school details
		DELETE FROM core_school WHERE id = this_school_id;
		RAISE NOTICE 'SCHOOL WITH CODE % HAS BEEN DELETED', this_school_code;

		--Delete account
		DELETE FROM school_account_gl WHERE id = this_school_account_id;

		-- IF LOGO IS NOT NULL DELETE IT TOO
		IF this_logo_id IS NOT NULL THEN
		DELETE FROM image_bank WHERE id = this_logo_id;
		END IF;

		

		RAISE NOTICE 'SCHOOL ACCOUNT CODE % HAS BEEN DELETED', this_school_account_id;
		
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.drop_student_details(this_student_payment_code bigint)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_account_id int8;
    this_student_id int8;
    this_student_image_id int8;

begin



    --Fetch the student account using payment code
    -- Select the student payment code
    SELECT id, student_account_id, student_image into this_student_id, this_student_account_id, this_student_image_id from core_student where student_code = this_student_payment_code;

    IF this_student_id IS NULL THEN
        RAISE EXCEPTION 'STUDENT NOT FOUND WITH THE SPECIFIED PAYMENT CODE';
    end if;

    -- IF STUDENT OUTSTANDING BALANCE IS NOT ZERO, THEN FAIL
    IF exists (select 1 from student_account_gl where id=this_student_account_id AND outstanding_balance<>0)
    then
        RAISE EXCEPTION 'STUDENT HAS AN OUTSTANDING BALANCE, AND HENCE CANNOT BE DELETED';
    end if;

    -- Drop non payment transactions for this student e.g fees, adjustments etc
    DELETE FROM student_account_transaction_history WHERE account_id = this_student_account_id AND payment_id IS NULL;

    -- Drop from student fee association for this student
    DELETE FROM institution_fee_student_association WHERE student_id = this_student_id;


    -- Check if student has transacted atleast once ,if so then fail
    If exists (select 1 from student_account_transaction_history where account_id = this_student_account_id) THEN
        RAISE EXCEPTION 'STUDENT CAN NOT BE DROPPED BECAUSE TRANSACTIONS EXIST FOR THEM';
    end if;

    -- Delete photo i fits set
    IF this_student_image_id <> NULL THEN
        DELETE FROM image_bank WHERE id = this_student_image_id;
        RAISE NOTICE 'STUDENT IMAGE FOR % HAS BEEN DELETED', this_student_payment_code;
    END IF;

    -- Delete student details
    DELETE FROM core_student WHERE id = this_student_id;
    RAISE NOTICE 'STUDENT WITH PAYMENT CODE % HAS BEEN DELETED', this_student_payment_code;

    --Delete account
    DELETE FROM student_account_gl WHERE id = this_student_account_id;

    RAISE NOTICE 'STUDENT ACCOUNT CODE % HAS BEEN DELETED', this_student_account_id;

    RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public."getGrade"(this_student_id numeric, marks_obtained numeric)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
declare 

student_school_id int = null;
student_class_id int = null;
this_student_id int = NEW.id;
the_fee_id int;

BEGIN

-- FIND fees associated to the student's class and are valid
-- and apply the fee to this student

	
				select ifd.school_id as student_school_id from core_student ifd 
		
		where si.id = this_student_id ;
		
		
		


RETURN true;
END;

$function$

CREATE OR REPLACE FUNCTION public.get_record_by_int8(record_id bigint, tbl_name text, field text)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
DECLARE
the_row record;
BEGIN
EXECUTE format('SELECT * FROM %s WHERE %s = %s limit 1', tbl_name, field, record_id) INTO the_row;
-- raise notice '%', the_row.id;
return to_json(the_row);
-- RETURN  row_to_json(t) from select * from (EXECUTE format('SELECT * FROM %s WHERE id = %s', tbl_name, record_id)) t;

END
$function$

CREATE OR REPLACE FUNCTION public.get_student_by_payment_code_or_registration_number(this_payment_code text)
 RETURNS json
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare
number_of_students_for_regno int8;
student_record json;
begin

		IF this_payment_code ~ '^(\d{10})$' -- Payment code or not
		THEN
		student_record = row_to_json(t) from (select * from core_student where student_code=cast(this_payment_code as int8)) t;
		ELSE
		this_payment_code = upper(this_payment_code);
		-- How many students have this reg no?
		SELECT count(id) into number_of_students_for_regno from core_student where school_student_registration_number=this_payment_code;
			
		IF number_of_students_for_regno > 1
		THEN
		RETURN  row_to_json(data) FROM (VALUES (407, 'Student selection failed. Use the 10 digit payment code to select specific student')) data(returncode, returnmessage);
		END IF;
		student_record =   row_to_json(t) from (select * from core_student where school_student_registration_number=this_payment_code) t;
		END IF;
		-- Check if we have something atleast
		IF (student_record->>'id') IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (404, 'No student was found with the specified payment code or registration number')) data(returncode, returnmessage);
		END IF;
		RETURN  student_record;

end;
$function$

CREATE OR REPLACE FUNCTION public.process_student_transaction(in_payment_channel bigint, in_payment_json text)
 RETURNS json
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare
request_json json;
-- Parameters are down here
studentPaymentCode text;
schoolPayPaymentCode bigint;
in_amount numeric;
channelTransactionId text;
channelMemo text;
processDate TIMESTAMP;
paymentType text;
channelDepositorName text;
channelDepositorPhone text;
channelDepositorBranch text;
channelTealId text;


--Temp variables to hold fee associations
feeId int8;
tenderedAmount int8;
feeMemo text;
feeAssociations json; -- Ann array of fee associations in the form feeID:String, tenderedAmount:int8, feeMemo:String
feeAssocRecord json;
totalFeeAmount int8 = 0;

--JSONS
record_student json;
record_bank json;
record_school json;
record_school_gl json;
record_student_gl json;
record_student_class json;
record_payment_channel json;
record_payment_channel_secondary_account json;
record_payment_channel_gl json;
record_payment_channel_strategy json;
record_existing_payment json;

-- Secondary account ID
payment_channel_account_id int8;
payment_channel_secondary_account_id int8;
payment_channel_secondary_account_gl_id int8;


payment_channel_balance_before numeric;
payment_channel_balance_after numeric;

student_balance_before numeric;
student_balance_after numeric;
student_outstanding_balance_before numeric;
student_outstanding_balance_after numeric;
student_allow_part_payments boolean;

school_balance_before numeric;
school_balance_after numeric;
school_allow_part_payments boolean;

is_payment_part_payment boolean;
student_full_name text;


new_payment_id int8;
new_reciept_number int8;
associated_fee_due_id int8;


begin
		-- Parse INPUT
		request_json = in_payment_json::json;

		studentPaymentCode = request_json ->> 'studentPaymentCode';
		schoolPayPaymentCode = request_json ->> 'schoolPayPaymentCode';
		in_amount = cast(request_json ->> 'amount' as int8);
		processDate = to_timestamp(request_json ->> 'processDate', 'YYYY-MM-DD HH24:MI:SS');
		paymentType = request_json ->> 'paymentType';
		channelDepositorName = request_json ->> 'channelDepositorName';
		channelDepositorPhone = request_json ->> 'channelDepositorPhone';
		channelDepositorBranch = request_json ->> 'channelDepositorBranch';
		channelTealId = request_json ->> 'channelTealId';
		channelTransactionId = request_json ->> 'channelTransactionId';
		channelMemo = request_json ->> 'channelMemo';

		feeAssociations = (request_json ->> 'feeAssociations') :: json ;

-- RETURN  row_to_json(data) FROM (VALUES (in_payment_json::json, 'STUDENT PAYMENT CODE IS REQUIRED BUT
--  WAS NOT SUPPLIED')) data(returncode, returnmessage);
-- 
		IF studentPaymentCode IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (100, 'STUDENT PAYMENT CODE IS REQUIRED BUT WAS NOT SUPPLIED')) data(returncode, request_json);
		END IF;

		IF in_amount IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (101, 'AMOUNT IS REQUIRED BUT WAS NOT SUPPLIED')) data(returncode, returnmessage);
		END IF;

		IF in_amount <= 0
		THEN
			RETURN  row_to_json(data) FROM (VALUES (109, 'AMOUNT SHOULD BE GREATER THAN ZERO')) data(returncode, returnmessage);
		END IF;

		IF in_payment_channel IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (105, 'PAYMENT CHANNEL IS NOT SUPPLIED')) data(returncode, returnmessage);
		END IF;

		IF channelTransactionId IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (106, 'CHANNEL TRANS ID IS REQUIRED AND WAS NOT SPECIFIED')) data(returncode, returnmessage);
		END IF;

		IF channelMemo IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (107, 'CHANNEL MEMO IS REQUIRED AND WAS NOT SPECIFIED')) data(returncode, returnmessage);
		END IF;
		IF channelMemo IS NULL
		THEN
			RETURN  row_to_json(data) FROM (VALUES (108, 'PROCESS DATE IS REQUIRED AND WAS NOT SPECIFIED')) data(returncode, returnmessage);
		END IF;
	
		IF feeAssociations IS NOT NULL AND json_array_length(feeAssociations)>0
		THEN
			FOR feeAssocRecord IN SELECT * FROM json_array_elements(feeAssociations)
			LOOP
				feeId = cast(feeAssocRecord::json->>'feeId' as int8);
				tenderedAmount = cast(feeAssocRecord::json->>'tenderedAmount' as int8);
				feeMemo = feeAssocRecord::json->>'feeMemo';

					IF tenderedAmount IS NULL 
					THEN
						RETURN  row_to_json(data) FROM (VALUES (110, 'INVALID FEE BREAKDOWN AMOUNT WITH ID '|| feeId ||' SPECIFY A VALID FEE BREAKDOWN AMOUNT AND TRY AGAIN')) data(returncode, returnmessage);
					END IF;
				totalFeeAmount = totalFeeAmount + tenderedAmount;
			END LOOP;

			-- Check that fee associations sum is equal to payment amount
			IF in_amount <> totalFeeAmount 
			THEN
				RETURN  row_to_json(data) FROM (VALUES (143, 'FEES AMOUNT MISMATCH. TOTAL OF FEES BREAKDOWNS MUST TOTAL TO THE TOTAL AMOUNT TENDERED.')) data(returncode, returnmessage);
			END IF;
			
		END IF;

		-- Avoid blank payment types
		IF paymentType IS NULL
		THEN
		paymentType = 'CASH';
		END IF;
		-- Work on fee associations
		
		-- Get the PAYMENT CHANNEL
		select get_record_by_int8(in_payment_channel, 'payment_channels', 'id') into record_payment_channel;
		IF (record_payment_channel->>'id') IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (199, 'Payment channel was not found')) data(returncode, returnmessage);
		END IF;

		-- Get the student
		SELECT get_student_by_payment_code_or_registration_number(studentPaymentCode) into record_student;
		IF (record_student->>'returncode') IS NOT NULL
		THEN
		-- There was a problem getting the student
		return record_student;
		END IF;

		-- Get the student class record into record_student_class
		select get_record_by_int8(CAST(record_student->>'class_id' AS int8), 'core_school_class', 'id') into record_student_class;

		--Set the students full names
		student_full_name = record_student->>'first_name';
		IF (record_student->>'middle_name') IS NOT NULL
		THEN
		student_full_name = student_full_name || ' ' || (record_student->>'first_name');
		END IF;

		IF (record_student->>'last_name') IS NOT NULL
		THEN
		student_full_name = student_full_name || ' ' || (record_student->>'last_name');
		END IF;
		
		-- Get the schoolpay
		select get_record_by_int8(cast(record_student->>'school_id' as int8), 'core_school', 'id') into record_school;
		IF (record_school->>'id') IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, student does not belong to a valid school or institution')) data(returncode, returnmessage);
		END IF;


		select get_record_by_int8(cast(record_school->>'bank_name' as int8), 'core_nominated_bank', 'id') into record_bank;

		--
		-- RESOLVE payment channel account to use
		-- Get Strategy
		select get_record_by_int8(cast(record_payment_channel->>'payment_channel_processing_strategy' as int8), 'payment_channel_payment_processing_strategies', 'id') into record_payment_channel_strategy;
		IF (record_payment_channel_strategy->>'id') IS NULL
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (208, 'Payment channel processing strategy not set')) data(returncode, returnmessage);
		END IF; 

		-- IF STRATEFGY IS BANK DIFFERENTIAL THEN USE SECONDARY ACCOUNT FOR bank_account_details	
		-- ELSE USE MAIN ACCOUNT
		IF (record_payment_channel_strategy->>'strategy_code') = 'BANK_DIFFERENTIAL'
		THEN
		RAISE NOTICE 'PAYMENT CHANNEL WILL USE DIFFERENTIAL ACCOUNT';
		-- Get secondary account for the school's bank and use that
		-- If its not found please create IT select * from payment_channel_secondary_accounts record_payment_channel_secondary_account
		SELECT account_id into payment_channel_secondary_account_id FROM payment_channel_secondary_accounts WHERE payment_channel = in_payment_channel and bank_id = cast(record_school->>'bank_name' as int8);
			
			IF payment_channel_secondary_account_id IS NULL
			THEN
			-- Create IT
			RAISE NOTICE 'SECONDARY ACCOUNT NOT FOUND, LETS CREATE IT';
				-- Get the bank_account_details

				INSERT INTO payment_channel_account_gl(description, balance, account_limit, creation_date, last_transaction_date)
				VALUES ('Secondary A/C ' || (record_payment_channel->>'channel_name') || ' Bank ID ' ||  (record_bank->>'bank_name'), 
								0, 0, now(), null) RETURNING id INTO payment_channel_secondary_account_gl_id;
				-- Now create the secondary account record
				
				INSERT INTO payment_channel_secondary_accounts(
            payment_channel, date_created, bank_id, account_id)
						VALUES (in_payment_channel, NOW(), cast(record_bank->>'id' as int8), payment_channel_secondary_account_gl_id) RETURNING id INTO payment_channel_secondary_account_id;
					
				RAISE NOTICE 'SECONDARY ACCOUNT CREATED FOR PAYMENT CHANNEL';
				END IF; 
		
		payment_channel_account_id = payment_channel_secondary_account_id; -- The effective payment_channel_gl_id
		ELSE
		-- Load the main account
		RAISE NOTICE 'PAYMENT CHANNEL WILL USE MAIN ACCOUNT';
	RAISE notice 'value of a : %', record_payment_channel->>'payment_channel_account_id';
		payment_channel_account_id = cast(record_payment_channel->>'payment_channel_account_id' as int8); -- The effective payment_channel_gl_id
		END IF;

		-- Get payment channel record
		select get_record_by_int8(payment_channel_account_id, 'payment_channel_account_gl', 'id') into record_payment_channel_gl;

		-- Get balance for payment channel for UPDATE
		
		SELECT balance INTO payment_channel_balance_before FROM payment_channel_account_gl WHERE id = payment_channel_account_id FOR UPDATE;
		
		-- DOES THE PAYMENT CHANNEL HAVE ENOUGH BALANCE 
		payment_channel_balance_after = payment_channel_balance_before - in_amount;
		

		

		IF payment_channel_balance_after < cast(record_payment_channel_gl->>'account_limit' as numeric)
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (309, 'INSUFFICIENT BALANCE. PAYMENT CHANNEL DOESN''T HAVE SUFFICIENT BALANCE FOR THIS OPERATION')) data(returncode, returnmessage);
		END IF;
		
		-- CHECK IF STUDENT IS NOT ACTIVE
		IF cast(record_student->>'active' as BOOLEAN) = FALSE
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (502, 'INACTIVE: STUDENT IS NOT ACTIVE. ALL FUTURE REQUESTS FOR STUDENT WILL FAIL.')) data(returncode, returnmessage);
		END IF;

		
		
		

		-- Get balance for payment channel for UPDATE
		SELECT balance INTO payment_channel_balance_before FROM payment_channel_account_gl WHERE id = payment_channel_account_id FOR UPDATE;

		student_allow_part_payments = cast(record_student->>'allow_part_payments' as BOOLEAN);
		IF student_allow_part_payments IS NULL
		THEN
		student_allow_part_payments = TRUE; -- Should be true if not set
		END IF; 

		-- is school active
		IF cast(record_school->>'active' as BOOLEAN) = FALSE
		THEN
		-- There was a problem getting the school
		RETURN  row_to_json(data) FROM (VALUES (501, 'INACTIVE: THE ASSOCIATED SCHOOL / INSTITUTION IS NOT ACTIVE')) data(returncode, returnmessage);
		END IF;
		
		
		school_allow_part_payments = cast(record_school->>'default_part_payment_behaviour' as BOOLEAN);
		IF school_allow_part_payments IS NULL
		THEN
		school_allow_part_payments = TRUE; -- Should be true if not set
		END IF; 

		-- CHECK IF SCHOOL ALLOWS PAYMENTS FROM THIS CHANNEL 
	--	IF NOT EXISTS (SELECT 1 FROM institution_payment_channel_association WHERE institution = CAST(record_school->>'id' AS int8) AND payment_channel = in_payment_channel)
	--	THEN
	--	RETURN  row_to_json(data) FROM (VALUES (511, 'NOT ALLOWED. School ' || (record_school->>'school_name') || ' does not accept payments from ' || (record_payment_channel->>'channel_name') )) data(returncode, returnmessage);
		--END IF;

		-- Get school account gl record
		select get_record_by_int8(CAST(record_school->>'school_account_id' as int8), 'school_account_gl', 'id') into record_school_gl;
		IF (record_school_gl->>'id') IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, institution gl information is missing, contact support for assistance' )) data(returncode, returnmessage);
		END IF;
		SELECT account_balance INTO school_balance_before FROM school_account_gl WHERE id = CAST(record_school->>'school_account_id' as int8) FOR UPDATE;
		
		
		-- Get school account gl record
		select get_record_by_int8(CAST(record_student->>'student_account_id' as int8), 'student_account_gl', 'id') into record_student_gl;
		IF (record_student_gl->>'id') IS NULL
		THEN
		RETURN  row_to_json(data) FROM (VALUES (203, 'Sorry, student gl information is missing, contact support for assistance' )) data(returncode, returnmessage);
		END IF;
		SELECT account_balance, outstanding_balance INTO student_balance_before, student_outstanding_balance_before FROM student_account_gl WHERE id = CAST(record_student->>'student_account_id' as int8) FOR UPDATE;
				--RETURN  row_to_json(data) FROM (VALUES (501, 'OUTSANDING BAL SELECTED')) data(returncode, returnmessage);



		-- CHECK IF WE SHOULD ALLOW PART PAYMENTS select * from student_account_gl limit 10;
		IF 	school_allow_part_payments = FALSE
		THEN
			--	RETURN  row_to_json(data) FROM (VALUES (501, 'OUTSANDING BAL SELECTED')) data(returncode, returnmessage);

IF student_allow_part_payments=FALSE AND in_amount < ABS(cast(record_student_gl->>'outstanding_balance' as numeric))
			THEN
			--ITS A PART PAYMENT				

			
			RETURN  row_to_json(data) FROM (VALUES (204, 'Sorry, Student ' || student_full_name || ' is not allowed to make part payments. Minimum amount allowed is ' || (ABS(cast(record_student_gl->>'outstanding_balance' as numeric))) )) data(returncode, returnmessage);
			
							RETURN  row_to_json(data) FROM (VALUES (501, 'OTSTANDING BAL SELECTED2')) data(returncode, returnmessage);

			END IF;
		end if;


		-- Check if payment payment for trans id EXISTS	
		record_existing_payment = row_to_json(t) from (select * from payments_received where payment_channel = in_payment_channel and channel_trans_id=channelTransactionId LIMIT 1) t;
		IF (record_existing_payment->>'id') IS NOT NULL
			THEN
			--WE ALREADY HAVE A PAYMENT FOR THIS TRANSID AND FROM THIS CHANNEL
						IF record_student->>'id' = record_existing_payment->>'student_id' AND CAST(record_existing_payment->>'amount' as numeric) = in_amount
						THEN
						--IF AMOUNT IS SAME AND STUDENT IS THE SAME THEN ITS A REPOSTING, SO RETURN DETAILS WITH RETURN CODE 0
						
						RETURN  row_to_json(data) FROM (VALUES (0, 'PAYMENT WAS ALREADY PROCESSED ON ' || (record_existing_payment->>'date_created'), (record_existing_payment->>'reciept_number')  )) data(returncode, returnmessage, receiptnumber);
						END IF;

			-- An attempt to reuse a trans id should fail immediately
			RETURN  row_to_json(data) FROM (VALUES (456, 'DUPLICATE TRANS ID: EACH TRANS ID SHOULD BE UNIQUE FOR A PAYMENT CHANNEL. '|| channelTransactionId ||' WAS ALREADY RECEIVED FOR A COMPLETELY DIFFERENT PAYMENT.')) data(returncode, returnmessage);
		END IF;

		IF EXISTS (SELECT 1 from payments_received WHERE student_id = cast(record_student->>'id' as int8) AND payment_channel = in_payment_channel AND amount = in_amount and channel_teal_id = channelTealId AND reversed=false AND reversal=false AND  DateDiff('hour', date_created, now()::timestamp) <= 6 )
		THEN
						RETURN  row_to_json(data) FROM (VALUES (457, 'PROBABLE DUPLICATE POSTING. WE ALREADY RECEIVED A PAYMENT OF THE SAME AMOUNT, FOR THE SAME STUDENT AND FROM THE SAME POSTING USER AND CHANNEL. POSSIBLE DUPLICATE NOT ACCEPTED')) data(returncode, returnmessage);
		END IF;


		-- Check if fee associations are passed, then fetch them and try to validate it now
		IF feeAssociations IS NOT NULL AND json_array_length(feeAssociations)>0
		THEN
			FOR feeAssocRecord IN SELECT * FROM json_array_elements(feeAssociations)
			LOOP
				
				feeId = cast(feeAssocRecord::json->>'feeId' as int8);
				tenderedAmount = cast(feeAssocRecord::json->>'tenderedAmount' as numeric);
				feeMemo = feeAssocRecord::json->>'feeMemo';
					-- This feeId must exist and associated with student
					IF NOT EXISTS (SELECT 1 FROM institution_fee_student_association WHERE id = feeId AND student_id = cast(record_student->>'id' as int8))
					THEN
						RETURN  row_to_json(data) FROM (VALUES (458, format('FEE %s & STUDENT MISMATCH. SPECIFIED FEE IS NOT ASSOCIATED WITH THE STUDENT. OPERATION ABORTED.', feeId))) data(returncode, returnmessage);
					END IF;
			END LOOP;
		END IF;



		-- Now we are going to start updating
		

		-- Create the payment channel record first
				INSERT INTO payments_received (
					date_created,
					student_id,
					school_id,
					payment_channel,
					channel_trans_id,
					channel_memo,
					amount,
					channel_process_date,
					channel_payment_type,
					channel_depositor_name,
					channel_depositor_phone,
					channel_depositor_branch,
					channel_teal_id,
					reversed,
					date_reversed,
					reversal_payment_id,
					reversal,
					schpay_payment_code
				)
				VALUES
					(
						NOW(),
						cast(record_student->>'id' as int8),
						cast(record_school->>'id' as int8),
						in_payment_channel,
						channelTransactionId,
						channelMemo,
						in_amount,
						processDate,
						paymentType,
						channelDepositorName,
						channelDepositorPhone,
						channelDepositorBranch,
						channelTealId,
						false, --reversed
						null, --date reversed
						null, --reversal payment id
						false, --reversal
						schoolPayPaymentCode 
					) RETURNING id, reciept_number INTO new_payment_id, new_reciept_number;
		


		-- Create payment channel TH and update its balance
		UPDATE payment_channel_account_gl SET balance = payment_channel_balance_after, last_transaction_date = NOW() WHERE id = payment_channel_account_id;
			INSERT INTO payment_channel_account_transaction_history(
				date_created,
				amount,
				description,
				payment_id,
				trans_type,
				account_id,
				reversal_flag,
				reversed,
				date_reversed,
				balance_after,
				balance_before,
				channel_transaction_processed_date
			)
			VALUES
				(
					NOW(), 
					in_amount * -1, -- Amount is a debit
					format('PMNT_RECV-%s - %s - %s', record_school->>'school_name', record_student->>'payment_code', student_full_name), 
					new_payment_id, 
					'PAYMENT PROCESSED', -- Trans type 
					payment_channel_account_id, -- Account id
					false, -- Reversal flag
					false, -- Reversed
					null, -- Date Reversed
					payment_channel_balance_after, -- Balance after
					payment_channel_balance_before, -- Balance before
					processDate -- Channel process date
				) ;

		
		
		-- Work on student txns
		-- If with break down, each to be created separately
		IF feeAssociations IS NOT NULL AND json_array_length(feeAssociations)>0
		THEN
			
			FOR feeAssocRecord IN SELECT * FROM json_array_elements(feeAssociations)
			LOOP
				
				feeId = cast(feeAssocRecord::json->>'feeId' as int8);
				tenderedAmount = cast(feeAssocRecord::json->>'tenderedAmount' as numeric);
				feeMemo = feeAssocRecord::json->>'feeMemo';

				-- Get the fee due ID	 select * from institution_fee_student_association
				SELECT fee_id INTO associated_fee_due_id from institution_fee_student_association WHERE id = feeId;

				student_outstanding_balance_after = student_outstanding_balance_before + tenderedAmount;
				student_balance_after = student_balance_before + tenderedAmount;
				
				
				IF student_outstanding_balance_after > 0
				THEN
				-- Set to zero if its greater than 0
				student_outstanding_balance_after = 0;
				END IF;
				


				-- Update the GL and Create TH REcord, indicating fee -- select * from student_account_gl limit 10;
				UPDATE student_account_gl SET account_balance = student_balance_after, outstanding_balance = student_outstanding_balance_after,  last_transaction_date = NOW() WHERE id = CAST(record_student_gl->>'id' AS int8);
				-- Create TH Record For student
				-- Fee description
				if feeMemo IS NULL
				THEN
				feeMemo = format('PAY FEE - %s - %s', (record_payment_channel->>'channel_code'), channelTransactionId);
				ELSE
				feeMemo = format('PAY FEE - %s - %s', (record_payment_channel->>'channel_code'), feeMemo);
				END IF;
					INSERT INTO student_account_transaction_history (
						date_created,
						amount,
						description,
						fee_association_id,
						payment_id,
						trans_type,
						account_id,
						reversal_flag,
						reversed,
						date_reversed,
						balance_after,
						associated_fee_due,
						outstanding_balance_after
					)
					VALUES
						(
							NOW(),
							tenderedAmount,
							feeMemo,
							feeId,
							new_payment_id,
							'FEE PAYMENT RECEIVED',
							CAST(record_student_gl->>'id' AS int8),
							false, --Reversal
							false, --Reversed
							null, --Date Reversed
							student_balance_after, --Balance after
							associated_fee_due_id, --Associated fee due id
							student_outstanding_balance_after --Associated fee due id
						) ;
				student_outstanding_balance_before = student_outstanding_balance_after; -- Set the before for the next iteration
				student_balance_before = student_balance_after; -- Set the before for the next iteration
					
			END LOOP;

		ELSE
		-- Save only one
						 RAISE NOTICE 'BEFORE SUBTRACTION';

				student_outstanding_balance_after = student_outstanding_balance_before + in_amount;
				student_balance_after = student_balance_before + in_amount;
				
				 RAISE NOTICE 'AFTER SUBTRACTION';
				 
				IF student_outstanding_balance_after > 0
				THEN
				-- Set to zero if its greater than 0
				student_outstanding_balance_after = 0;
				END IF;
				
				-- Update the GL and Create TH REcord, indicating fee -- select * from student_account_gl limit 10;
				UPDATE student_account_gl SET account_balance = student_balance_after, outstanding_balance = student_outstanding_balance_after,  last_transaction_date = NOW() WHERE id = CAST(record_student_gl->>'id' AS int8);
				-- Create TH Record
				-- 'PAYMENT RECEIVED - " + paymentChannel.getChannelCode() + " - " + paymentRequestWrapper.getChannelTransactionId()
				INSERT INTO student_account_transaction_history (
						date_created,
						amount,
						description,
						fee_association_id,
						payment_id,
						trans_type,
						account_id,
						reversal_flag,
						reversed,
						date_reversed,
						balance_after,
						associated_fee_due,
						outstanding_balance_after
					)
					VALUES
						(
							NOW(),
							in_amount,
							format('PAYMENT RECEIVED %s - %s', (record_payment_channel->>'channel_code'), channelTransactionId),
							null, -- fee id
							new_payment_id,
							'PAYMENT RECEIVED',
							CAST(record_student_gl->>'id' AS int8),
							false, --Reversal
							false, --Reversed
							null, --Date Reversed
							student_balance_after, --Balance after
							null, --Associated fee due id
							student_outstanding_balance_after --Associated fee due id
						) ;
		
		END IF;


		-- School GL
		school_balance_after = school_balance_before + in_amount;
		UPDATE school_account_gl SET account_balance = school_balance_after, last_transaction_date = now() where id = cast(record_school_gl->>'id' as int8);
		--School TH
				INSERT INTO school_account_transaction_history (
					date_created,
					amount,
					posting_user,
					description,
					payment_id,
					trans_type,
					account_id,
					reversal_flag,
					reversed,
					date_reversed,
					balance_after
				)
				VALUES
					(
					NOW(),
					in_amount,
					null, -- posting user
					format('PMNT_RECV-%s - %s - %s', (record_payment_channel->>'channel_code'), (record_student->>'payment_code'), student_full_name), -- description "PMNT_RECV-" + paymentChannel.getChannelCode() + " - " + studentInformation.getPaymentCode() + " - " + studentInformation.getFirstName() + " " + studentInformation.getLastName()
					new_payment_id,
					'PAYMENT RECEIVED',
					CAST(record_school_gl->>'id' as int8),
					false, -- REversal
					false, -- reversed
					null, -- date reversed
					school_balance_after
					) ;


			-- If we should log for auto settlement select * from payment_channels
			IF CAST(record_payment_channel->>'log_for_auto_settlement' AS BOOLEAN) IS TRUE	
			THEN 
			-- Log for auto settlement
						INSERT INTO auto_settlement_requests (
							date_created,
							source_payment_channel,
							source_payment_channel_code,
							student_code,
							student_name,
							school_name,
							source_channel_trans_id,
							schoolpay_reciept_number,
							payment_id,
							amount,
							payer_phone,
							school_code,
							settled,
							settlement_bank_id
						)
						VALUES
							(
								NOW(),
								CAST(record_payment_channel->>'id' as int8),
								record_payment_channel->>'channel_code',
								record_student->>'student_code',
								student_full_name,
								record_school->>'school_name',
								channelTransactionId,
								new_reciept_number,
								new_payment_id,
								in_amount,
								channelDepositorPhone,
								record_school->>'school_code', 
								false, -- Settled 
								cast(record_school->>'bank_name'  as int8)
							) ;

			
			END IF;
		

		-- Return details as json
		-- Parameter validations
RETURN row_to_json (DATA)
FROM
	(

		VALUES
			(
				0,
				format('School fees payment has been processed succesfully. Reference:%s', new_reciept_number),
				record_school->>'school_code',
				record_school->>'school_name',
				record_student->>'first_name',
				record_student->>'middle_name',
				record_student->>'last_name',
				format('%s - %s', record_student_class->>'class_code', record_student_class->>'class_description'),
				new_reciept_number
			)
	) DATA (returncode, returnmessage, schoolcode, schoolname, firstname, middlename, lastname, studentclass, recieptnumber) ;

end;
$function$

CREATE OR REPLACE FUNCTION public.remove_fee_from_student(this_student_id bigint, this_fee_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying, adjust_student_balance boolean)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    this_student_school_id int8;
    this_fee_school_id int8;
    this_fee_approval_status boolean;
    this_fee_description varchar;
    this_fee_amount numeric;
    this_student_current_balance numeric;
    this_student_current_outstanding_balance numeric;
    this_student_end_balance numeric;
    this_student_account_id int8;
    this_fee_association_id int8;
    this_fee_applied_to_student boolean;
    new_outstanding_balance numeric;


begin


    IF NOT EXISTS (
            SELECT
                1
            FROM
                core_student
            WHERE
                    ID = this_student_id
        ) THEN
        RAISE NOTICE 'STUDENT WITH ID % NOT FOUND',
            this_student_id ; RETURN FALSE ;
    END
        IF ;
    -- Get the fee things
    SELECT
        school_id,
        due_amount,
        description,
        approval_status INTO this_fee_school_id,
        this_fee_amount,
        this_fee_description,
        this_fee_approval_status
    FROM
        institution_fees_due
    WHERE
            ID = this_fee_id ;


    if this_fee_school_id IS NULL THEN
        --Fee record does not exist
        RAISE EXCEPTION 'SPECIFIED FEE % WAS NOT FOUND', this_fee_id;
    end if;

    -- Fee should be approved
    IF this_fee_approval_status = false THEN
        RAISE EXCEPTION 'SPECIFIED FEE % IS NOT YET APPROVED', this_fee_id;
    end if;




    -- Select the student account id
    SELECT
        student_account_id,
        school_id INTO this_student_account_id,
        this_student_school_id
    FROM
        core_student
    WHERE
            ID = this_student_id ;


    -- School for fee and student should be the same

    IF this_fee_school_id <> this_student_school_id THEN
        RAISE EXCEPTION 'THE SCHOOL FOR THE FEE AND STUDENT MUST BE THE SAME';
    END
        IF ;


    -- Check if fee is not already applied to this student
    SELECT
        id, applied INTO this_fee_association_id, this_fee_applied_to_student
    FROM
        institution_fee_student_association
    WHERE
            fee_id = this_fee_id
      AND student_id = this_student_id ;
    IF this_fee_association_id IS NULL THEN
        RAISE NOTICE 'THIS FEE % IS NOT APPLIED TO THIS STUDENT %',
            this_fee_id,
            this_student_id ;
        RETURN FALSE ;
    END
        IF ;



    -- Get the current balance for UPDATE
    SELECT
        account_balance, outstanding_balance INTO this_student_current_balance, this_student_current_outstanding_balance
    FROM
        student_account_gl
    WHERE
            ID = this_student_account_id FOR UPDATE ;

    -- Adjust the new balance
    this_student_end_balance = this_student_current_balance + this_fee_amount;
    new_outstanding_balance = this_student_current_outstanding_balance + this_fee_amount;

    IF new_outstanding_balance > 0 THEN
        new_outstanding_balance = 0;
    END IF;

    --IF exists adjust the existing fee_association_id coz we are gonna delete it
    UPDATE student_account_transaction_history SET fee_association_id = NULL
    WHERE account_id = this_student_account_id
      AND fee_association_id = this_fee_association_id
      AND payment_id IS NULL;

    --Delete the association now
    DELETE FROM institution_fee_student_association WHERE fee_id = this_fee_id
                                                      AND student_id = this_student_id;

    -- Remove from fee student th
    DELETE FROM institution_fees_due_student_payment_transaction_history WHERE fee_student_association_id = this_fee_association_id;



    IF adjust_student_balance = TRUE and this_fee_applied_to_student THEN
        -- update balance
        UPDATE student_account_gl
        SET account_balance = this_student_end_balance,
            outstanding_balance = new_outstanding_balance
        WHERE
                ID = this_student_account_id ; -- Create th record
        INSERT INTO student_account_transaction_history (
            date_created,
            amount,
            posting_user,
            description,
            fee_association_id,
            payment_id,
            trans_type,
            account_id,
            reversal_flag,
            reversed,
            date_reversed,
            balance_after,
            associated_fee_due,
            outstanding_balance_after
        )
        VALUES
        (
            now(),
            this_fee_amount,
            web_user_name,
            'FEE REVERSAL - ' || this_fee_description,
            NULL,
            NULL,
            'FEE REVERSAL',
            this_student_account_id,
            TRUE,
            FALSE,
            NULL,
            this_student_end_balance,
            this_fee_id,
            new_outstanding_balance
        ) ;
    END
        IF ;


    -- Log now
    INSERT INTO web_console_log (
        ip_address,
        user_name,
        event_date,
        event_action,
        affected_student_id,
        errors,
        web_user_id
    )
    VALUES
    (
        ip_address,
        web_user_name,
        now(),
        'Reverse fee ' || this_fee_id,
        this_student_id,
        '',
        web_user_id
    ) ;
    RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.unarchive_student(this_student_id bigint, this_reason_for_unarchive character varying, web_user_id bigint, web_user_name character varying, ip_address character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
this_student_school_id int8;
this_student_class_before_archive_id int8;
this_student_class_before_archive_default_id int8;
this_student_archive_class_id int8;
this_student_current_class_id int8;
current_archive_status boolean;


begin

		if NOT exists (select 1 from core_student where id = this_student_id) THEN
			RAISE NOTICE 'STUDENT WITH ID % NOT FOUND', this_student_id;
				return false;
		end if;


		-- Select the student class ID
		SELECT class_id, school_id, archived,class_when_archived INTO this_student_current_class_id, this_student_school_id, current_archive_status, this_student_class_before_archive_id FROM core_student where id = this_student_id;

		IF current_archive_status = false THEN
			RAISE NOTICE 'STUDENT % IS NOT ARCHIVED' , this_student_id;
			RETURN false;
		END IF;


		--Check if class when archived actually exists
		IF NOT EXISTS (select 1 from core_school_class where id = this_student_class_before_archive_id AND school_id = this_student_school_id) 
		then
		
		--Check if default class exists
		SELECT id INTO this_student_class_before_archive_default_id FROM core_school_class where school_id = this_student_school_id and class_code = 'DEFAULT';
		
		IF this_student_class_before_archive_default_id is null
		then
			-- Create a default class where this student will go
			INSERT INTO core_school_class (school_id, class_code, class_description, date_created)
				VALUES (this_student_school_id, 'DEFAULT', 'DEFAULT CLASS', now()) returning id into this_student_class_before_archive_default_id;
			end if;

	this_student_class_before_archive_id = this_student_class_before_archive_default_id;
		end if;
		
		
		
		

		-- LEts update now
		update core_student set class_id = this_student_class_before_archive_id , active = true,
		archived = false, class_when_archived = null
		where id = this_student_id;
		

		-- Log now
		INSERT INTO web_console_log(
            ip_address, user_name, event_date, event_action, affected_student_id, 
            errors, web_user_id)
    VALUES (ip_address, web_user_name, now(), 
		format('Un-Archive student - %s', this_reason_for_unarchive), this_student_id, 
            '', web_user_id);
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.validate_class_school_on_student_update_trigger_function()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
declare 

student_school_id int = null;
class_school_id int = null;

BEGIN


SELECT school_id INTO class_school_id FROM core_school_class where id = NEW.class_id;
SELECT school_id INTO student_school_id FROM core_student where id = NEW.id;

IF student_school_id <> class_school_id THEN
raise EXCEPTION 'CLASS AND SCHOOL MISMATCH FOR THIS STUDENT. OPERATION ABORTED'; 
END IF;


RETURN NEW;
END;

$function$

CREATE OR REPLACE FUNCTION public.workflow_action_fee_edit(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
fee_record record;
user_record record;
penalty_record record;
request_params json;
penalty_params json;
update_query varchar;

insert_cols varchar = '';
insert_vals varchar = '';

_key varchar;
_value varchar;
_param_count int = 1;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from institution_fees_due ifd where id = workflow_item_record.record_id into fee_record;
		if fee_record.id is null
		then
				return_code = 404;
	    		return_message = 'Fee item not found';
	    		return;
		end if;
	
	
	
		-- For each request params key except *penalty*, update
		update_query = 'update institution_fees_due set ';
		
		_param_count = 1; --Set to 1 and start looping
		FOR _key, _value IN
	       SELECT * FROM json_each_text(request_params)
	    loop
	    	if _key = 'penalty'
	    	then 
	    		continue;
	    	end if;
	       -- do some math operation on its corresponding value
	       if _param_count = 1
	       then 
	        update_query = format('%s %s = %s ', update_query, _key, quote_nullable(_value));
	       else 
	        update_query = format('%s, %s = %s ', update_query, _key, quote_nullable(_value));
	       end if;
	      
	       _param_count = _param_count + 1;
	    END LOOP;
	
		update_query = format('%s where id = %s returning *', update_query, fee_record.id);
		
		execute update_query into fee_record;
	
		
	-- Work on penalty, if not exists, create it else update it
		if fee_record.has_penalty = true 
		then
			penalty_params = request_params->>'penalty';
			if fee_record.penalty_id is not null
			then
				--Just update the penalty fields
				update_query = 'update institution_fees_due_penalty set ';
		
			_param_count = 1; --Set to 1 and start looping
			FOR _key, _value IN
		       SELECT * FROM json_each_text(penalty_params)
			    loop
			       if _param_count = 1
			       then 
			        update_query = format('%s %s = %s ', update_query, _key, quote_nullable(_value));
			       else 
			        update_query = format('%s, %s = %s ', update_query, _key, quote_nullable(_value));
			       end if;
			      
			       _param_count = _param_count + 1;
			    END LOOP;
			
				update_query = format('%s where id = %s returning *', update_query, fee_record.penalty_id);
				
			
			else
				-- Insert new penalty and update it to the fee
					update_query = 'insert into institution_fees_due_penalty ';
					_param_count = 1; --Set to 1 and start looping
				FOR _key, _value IN
		       		SELECT * FROM json_each_text(penalty_params)
			    loop
			       if _param_count = 1
			       then 
			        insert_cols = format('%s %s', insert_cols, _key);
			        insert_vals = format('%s %s', insert_vals, quote_nullable(_value));
			       else 
			        insert_cols = format('%s, %s', insert_cols, _key);
			        insert_vals = format('%s, %s', insert_vals, quote_nullable(_value));
			       end if;
			      
			       _param_count = _param_count + 1;
			    END LOOP;
			   
			   update_query = format('%s (%s) values (%s) returning * ', update_query, insert_cols, insert_vals);
			end if;
			execute update_query into penalty_record;
			update institution_fees_due set has_penalty =true, penalty_id = penalty_record.id where id = fee_record.id;
		end if;
	
	   	return_code = 0;
	    return_message = 'Fee has been modified successfully';
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_assign_group_fee(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
student_record record;
user_record record;
fee_record record;
 the_fee varchar(50) := 'feed_id';

_the_student_id numeric;

_key varchar;
_value varchar;
_param_count int = 1;
request_params json;
member_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from student_group_information si where id = workflow_item_record.record_id into student_record;
		if student_record.id is null
		then
				return_code = 404;
	    		return_message = 'Student item not found';
	    		return;
		end if;
		
		
		select * from institution_fees_due ifd where id =  cast(request_params->>'fee_id' as numeric) into fee_record;
		if fee_record.id is null
		then
				return_code = 404;
	    		return_message = 'Fee item not found';
	    		return; 		
		end if;
	
		if fee_record.recurrent = true 
		then
			--Just update the penalty fields
			 perform apply_fee_to_group(
			 fee_record.id,
			 workflow_item_record.record_id, 
			user_record.username
			 );
			
					else
					FOR _the_student_id IN
      SELECT student_id from student_group_student where group_id = student_record.id
   LOOP
      --apply fee to each members
	
				perform apply_fee_to_student( 
						cast( _the_student_id as int),
							fee_record.id,
							user_id, 
							user_record.username, 
							'127.0.0.1');
   END LOOP;	
		end if;
	
	   	return_code = 0;
  return_message = 'Group Fee has been assigned successfully check';
	 -- return_message = _the_student_id;
--                
	    
					
	
		
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_remove_student_fee(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
student_record record;
user_record record;
fee_record record;
 the_fee varchar(50) := 'feed_id';

_key varchar;
_value varchar;

request_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from core_student si where id = workflow_item_record.record_id into student_record;
		if student_record.id is null
		then
				return_code = 404;
	    		return_message = 'Student item not found';
	    		return;
		end if;
		
		
		select * from institution_fees_due ifd where id =  cast(request_params->>'fee_id' as numeric) into fee_record;
		if fee_record.id is null
		then
				return_code = 404;
	    		return_message = 'Fee item not found';
	    		return; 		end if;
	  		
		perform remove_fee_from_student_with_exemption( 
		student_record.id,
		fee_record.id ,
		user_id, 
		user_record.username, 
		'127.0.0.1',
		cast(request_params->>'adjust_bal' as boolean),
		cast(request_params->>'adjust_bal' as boolean)
	);
	
	   	
	   -- END LOOP; 
			  		RAISE NOTICE 'SCHOOL WITH CODE % HAS BEEN DELETED', student_record.id;

return_code = 0;
	    --return_message = 'Fee has been modified successfully';
	    return_message = cast(request_params->>'adjust_bal' as VARCHAR);
		
	
		
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_delete_group(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
group_record record;
user_record record;
fee_record record;
 the_fee varchar(50) := 'feed_id';

_key varchar;
_value varchar;

request_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from student_group_information si where id = workflow_item_record.record_id into group_record;
		if group_record.id is null
		then
				return_code = 404;
	    		return_message = 'Group item not found';
	    		return;
		end if;
		
		
	delete from student_group_student where group_id = workflow_item_record.record_id;
	delete from institution_fee_group_association where group_id = workflow_item_record.record_id;
	delete from student_group_information where id = workflow_item_record.record_id;

	
	   	


return_code = 0;
	    --return_message = 'Fee has been modified successfully';
	    return_message = 'Successfully delete group';
		
	
		
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_remove_group_fee(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
student_record record;
user_record record;
fee_record record;
 the_fee varchar(50) := 'feed_id';

_the_student_id numeric;

_key varchar;
_value varchar;
_param_count int = 1;
request_params json;
member_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from student_group_information si where id = workflow_item_record.record_id into student_record;
		if student_record.id is null
		then
				return_code = 404;
	    		return_message = 'Student item not found';
	    		return;
		end if;
		
		
		select * from institution_fees_due ifd where id =  cast(request_params->>'fee_id' as numeric) into fee_record;
		if fee_record.id is null
		then
				return_code = 404;
	    		return_message = 'Fee item not found';
	    		return; 		
		end if;
	
		if fee_record.recurrent = true 
		then
			--Just update the penalty fields
			 perform remove_fee_from_group(
			 fee_record.id,
			 workflow_item_record.record_id
			 );
			
					else
					FOR _the_student_id IN
      SELECT student_id from student_group_student where group_id = student_record.id
   LOOP
      --remove fee to each members

				perform remove_fee_from_student_with_exemption( 
						cast( _the_student_id as int),
							fee_record.id,
							user_id, 
							user_record.username, 
							'127.0.0.1',
							cast(request_params->>'adjust_balance' as boolean),
							cast(request_params->>'exempt' as boolean)
							
							);
   END LOOP;	
		end if;
	
	   	return_code = 0;
  return_message = 'Group Fee has been removed successfully';
 		
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action(item_id bigint, user_id bigint, in_action character varying, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare



workflow_item_record record;
action_response_record record;
user_record record;

begin
	
		select * from "user" u  where id = user_id into user_record;
	
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
			
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		if workflow_item_record.approval_status <> 'PENDING'
		then
				return_code = 405;
	    		return_message = 'Workflow item is not pending approval';
	    		return;
		end if;
	
		if workflow_item_record.requested_by = user_id
		then
				return_code = 406;
	    		return_message = 'You cannot approve a work flow item you requested for';
	    		return;
		end if;
	
	
		-- If approval user has a school attached, then this school should be the same as the approval record school
		if user_record.school_id is not null and user_record.school_id <> workflow_item_record.school_id
		then 
				return_code = 419;
	    		return_message = 'Action can only be done by a user from the same school or an admin';
	    		return;
		end if;
	
		if in_action = 'REJECT' 
		then
			--Set as rejected
			update workflow_approval_requests set approval_status = 'REJECTED', rejection_reason = in_notes,
				date_approved = now(), approved_by = user_id where id = workflow_item_record.id;
				return_code = 909;
	    		return_message = 'Work flow item has been rejected';
	    		return;
		elsif in_action <> 'APPROVE' 
			then
				return_code = 409;
	    		return_message = 'The supplied action is not known';
	    		return;
		end if;
	
		--Now start working on the different scenarios
		if workflow_item_record.workflow_record_type = 'STUDENT_BALANCE_ADJUSTMENT'
		then 
			select * from workflow_action_student_balance_adjustment(item_id , user_id, in_notes) into action_response_record;
		
		elsif workflow_item_record.workflow_record_type = 'EDIT_FEE'
		then
			select * from workflow_action_fee_edit(item_id , user_id, in_notes) into action_response_record;
		
		----gogo assignment
		
		elsif workflow_item_record.workflow_record_type = 'STUDENT_FEE_ASSIGNMENT'
		then
			select * from workflow_action_assign_student_fee(item_id , user_id, in_notes) into action_response_record;
		
		
			elsif workflow_item_record.workflow_record_type = 'REMOVE_STUDENT_FEE'
		then
			select * from workflow_action_remove_student_fee(item_id , user_id, in_notes) into action_response_record;
		
		
		elsif workflow_item_record.workflow_record_type = 'GROUP_FEE_ASSIGNMENT'
		then
			select * from workflow_action_assign_group_fee(item_id , user_id, in_notes) into action_response_record;
		
		elsif workflow_item_record.workflow_record_type = 'DELETE_A_GROUP'
		then
			select * from workflow_action_delete_group(item_id , user_id, in_notes) into action_response_record;
			
		elsif workflow_item_record.workflow_record_type = 'REMOVE_GROUP_FEE'
		then
			select * from workflow_action_remove_group_fee(item_id , user_id, in_notes) into action_response_record;
		
			elsif workflow_item_record.workflow_record_type = 'GROUP_BALANCE_ADJUSTMENT'
		then
			select * from workflow_action_adjust_group_balance(item_id , user_id, in_notes) into action_response_record;
		
		
		---gogo end
		
		-- Add other endifs down here
		
		end if;
	
		return_code = action_response_record.return_code;
	    return_message = action_response_record.return_message;
		if action_response_record.return_code <> 0
		then
			--Dont proceed
			return;
		end if;
		
	   
		--Set as approved
			update workflow_approval_requests set approval_status = 'APPROVED', approval_notes = in_notes,
				date_approved = now(), approved_by = user_id where id = workflow_item_record.id;
				return_code = 0;
	    		return_message = 'Work flow item has been approved';
	    		return;
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_student_balance_adjustment(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
student_record record;
user_record record;
request_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from core_student  si where id = workflow_item_record.record_id into student_record;
		if student_record.id is null
		then
				return_code = 404;
	    		return_message = 'Student item not found';
	    		return;
		end if;
	
		perform adjust_student_balance(student_record.student_code, 
		
		cast(request_params->>'amount' as numeric), 
		user_id, 
		user_record.username, 
		workflow_item_record.request_notes, 
		'127.0.0.1', 
		request_params->>'mode');
	
	   	return_code = 0;
	    return_message = 'Balance adjustment has been done successfully';
	
end;
$function$

CREATE OR REPLACE FUNCTION public.adjust_student_balance(this_student_payment_code bigint, this_new_balance numeric, posting_user_id numeric, posting_user character varying, reason character varying, ip character varying, adjustment_mode character varying)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
 this_student_id integer;
this_student_account_id integer;
amount_to_post numeric;
new_outstanding_balance numeric;
student_th_record_id integer;
student_name varchar;
account_record_before record;
account_record record;
begin
		--Fetch the student account using payment code 
		if NOT exists (select 1 from core_student cs  where student_code = this_student_payment_code) THEN
				RAISE EXCEPTION 'STUDENT NOT FOUND WITH THE SPECIFIED PAYMENT CODE';
		end if;

		-- Select the student payment code
		SELECT id, student_account_id,
		format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) 
	into this_student_id, this_student_account_id, student_name from core_student cs  where student_code  = this_student_payment_code;
		
		--Select account for ID
		SELECT * from student_account_gl where id = this_student_account_id into account_record_before for update;
		--Determine what amount to POST
		amount_to_post = this_new_balance - account_record_before.account_balance;
		
		if adjustment_mode = 'BY'
		then 
		amount_to_post = this_new_balance;
		end if;
	
		-- if amount to post is zero, do NOTHING
		IF amount_to_post = 0 AND account_record_before.outstanding_balance = least(account_record_before.account_balance+amount_to_post, 0) THEN
			RAISE NOTICE 'NO AMOUNT TO PASS';
			RETURN false;
		END IF;
	
		update student_account_gl set account_balance = account_balance+amount_to_post, outstanding_balance=least(account_balance+amount_to_post, 0) where id = this_student_account_id returning * into account_record;

	
		
		--Now post by inserting into student th, and updating balance accordingly
		INSERT INTO student_account_transaction_history(
            date_created, amount, posting_user, description, 
            trans_type, account_id, reversal_flag, reversed, 
            balance_after, outstanding_balance_after, balance_before, outstanding_balance_before)
    VALUES (now(), amount_to_post, posting_user, reason,  
            'BAL-ADJUSTMENT', this_student_account_id, false, false,
            account_record.account_balance, account_record.outstanding_balance, account_record_before.account_balance, account_record_before.outstanding_balance) returning id into student_th_record_id;

		
		
		
		-- If amount is positive, apportion it to fees
		if amount_to_post > 0
		then
			perform apportion_payment_amount_to_fees(this_student_id, 0, amount_to_post);
		end if;
		
		
		-- Log to web console log
	INSERT INTO web_console_log ( "ip_address", "user_name", "event_date", "event_action", "affected_student_id", "web_user_id", "event_action_id", "student_th_id" )
	VALUES
		( ip, posting_user, now(), format('%s''s balance adjusted to %s', student_name, this_new_balance), this_student_id, posting_user_id, 'ADJUST_BALANCE', student_th_record_id );
		
		RETURN true;
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_adjust_group_balance(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
student_record record;
student_group_record record;
user_record record;
fee_record record;
 the_fee varchar(50) := 'feed_id';

_the_student_payment_code numeric;
_the_student_id numeric;

_key varchar;
_value varchar;
_param_count int = 1;
request_params json;
member_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from student_group_information si where id = workflow_item_record.record_id into student_group_record;
		if student_group_record.id is null
		then
				return_code = 404;
	    		return_message = 'Group item not found';
	    		return;
		end if;
		
	
					FOR _the_student_id IN
      SELECT student_id from student_group_student where group_id = student_group_record.id
   LOOP
      --apply fee to each members
select * from core_student si where id = _the_student_id into student_record;
		if student_record.id is null
		then
				return_code = 404;
	    		return_message = 'Group item not found';
	    		return;
		end if;
		
	
				perform adjust_student_balance( 
						student_record.student_code,
							cast(request_params->>'new_balance' as numeric),
							user_id, 
							user_record.username, 
							request_params->>'reason',
							'127.0.0.1',
							request_params->>'adjustment_mode');
   END LOOP;	
	
	   	return_code = 0;
  return_message = 'Group Fee has been adjusted successfully check';
	 -- return_message = _the_student_id;
--                
	    
					
	
		
	
end;
$function$

CREATE OR REPLACE FUNCTION public.workflow_action_assign_student_fee(item_id bigint, user_id bigint, in_notes character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare

workflow_item_record record;
student_record record;
user_record record;
fee_record record;
 the_fee varchar(50) := 'feed_id';

_key varchar;
_value varchar;

request_params json;
begin
	
		select * from workflow_approval_requests war where id = item_id into workflow_item_record;
	
		if workflow_item_record.id is null
		then
				return_code = 404;
	    		return_message = 'Workflow item not found';
	    		return;
		end if;
	
		request_params = workflow_item_record.request_params;
	
	
		select * from "user" u  where id = user_id into user_record;
		if user_record.id is null
		then
				return_code = 1404;
	    		return_message = 'Approval user not found';
	    		return;
		end if;
	
		select * from core_student si where id = workflow_item_record.record_id into student_record;
		if student_record.id is null
		then
				return_code = 404;
	    		return_message = 'Student item not found';
	    		return;
		end if;
		
		
		select * from institution_fees_due ifd where id =  cast(request_params->>'fee_id' as numeric) into fee_record;
		if fee_record.id is null
		then
				return_code = 404;
	    		return_message = 'Fee item not found';
	    		return; 		end if;
	  		
		perform apply_fee_to_student( 
		student_record.id,
		fee_record.id ,
		user_id, 
		user_record.username, 
		'127.0.0.1');
	
return_code = 0;
	    return_message = 'Fee has been assigned to student successfully';
		
	
		
	
end;
$function$

CREATE OR REPLACE FUNCTION public.remove_fee_from_student_with_exemption(this_student_id bigint, this_fee_id bigint, web_user_id bigint, web_user_name character varying, ip_address character varying, adjust_student_balance boolean, exempt boolean)
 RETURNS boolean
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$

declare
    success boolean;
    fee_record record;
    student_record record;
begin
    select remove_fee_from_student(this_student_id, this_fee_id, web_user_id, web_user_name, ip_address, adjust_student_balance) into success;

    if success = true
    then
        select * from institution_fees_due ifd where id = this_fee_id into fee_record;
        select * from core_student  si where id = this_student_id into student_record;

        if fee_record.child_of_recurrent and exempt = true
        then
            --exempt parent fee from this student
            perform exempt_student_from_fee(student_record.student_code, fee_record.parent_fee_id);
        end if;
    end if;
    RETURN success;
end;
$function$

CREATE OR REPLACE FUNCTION public.exempt_student_from_fee(in_payment_code bigint, in_fee_id bigint, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare
    student_record record;
    fee_record record;

begin
    select * from institution_fees_due ifd where id = in_fee_id into fee_record;
    if fee_record.id is null
    then
        return_code = 404;
        return_message = 'Fee not found';
        return;
    end if;
    if not exists (select 1 from fees_due_student_exemption fdse where fdse.fee_id = in_fee_id and student_number = in_payment_code)
    then
        select * from core_student si where student_code = in_payment_code into student_record;


        if student_record.id is not null and fee_record.id is not null and student_record.school_id = fee_record.school_id
        then
            insert into fees_due_student_exemption (fee_id, student_number) values(in_fee_id, in_payment_code);
        end if;
    end if;

    --if fee is child of recurrent, then exempt from parent too
    if fee_record.child_of_recurrent = true
    then
        --Recursive call to myself
        perform exempt_student_from_fee(in_payment_code, fee_record.parent_fee_id);
    end if;
    return_code = 0;
    return_message = 'Exempted';

end;
$function$

CREATE OR REPLACE FUNCTION public.apply_fee_to_group(in_fee_id bigint, in_group_id bigint, in_user_id character varying, OUT association_id bigint, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare



fetch_cursor refcursor;
fee_due_record record;
group_record record;
fee_association_record record;
active_child_fee_record record;

begin
	-- Get the fee
		OPEN fetch_cursor for 
			select * from institution_fees_due ifd 
			where ifd.id = in_fee_id limit 1;
			FETCH fetch_cursor INTO fee_due_record;
		close fetch_cursor;	
	
	    if fee_due_record.id is null
	    then
	    	return_code = 404;
	    	return_message = 'Fee not found';
		    return;
	    end if;
	   
	   if fee_due_record.end_date < current_date
	   then
	   		return_code = 504;
	    	return_message = 'Fee end date is in the past';
		    return;
	   end if;
	  
	  if fee_due_record.recurrent = false and fee_due_record.child_of_recurrent = false
	   then
	   		return_code = 505;
	    	return_message = 'Only recurrent fees or child of recurrent fees can be applied to groups';
		    return;
	   end if;
	
	  --get the group	
	   OPEN fetch_cursor for 
			select * from student_group_information sgi 
			where sgi.id = in_group_id limit 1;
			FETCH fetch_cursor INTO group_record;
		close fetch_cursor;	
	
		if group_record.id is null
	    then
	    	return_code = 404;
	    	return_message = 'Group not found';
		    return;
	    end if;
	   
	   if group_record.school_id <> fee_due_record.school_id
	   then
	   		return_code = 1404;
	    	return_message = 'Group and fees should belong to the same school';
		    return;
	   end if;
	
	  	--Check if already associated
	 	OPEN fetch_cursor for 
			select * from institution_fee_group_association assoc 
			where assoc.fee_id = fee_due_record.id and group_id = group_record.id limit 1;
			FETCH fetch_cursor INTO fee_association_record;
		close fetch_cursor;	
		
		if fee_association_record.id is not null
	    then
	    	return_code = 0;
	    	return_message = 'Already associated';
	    	association_id = fee_association_record.id;
		    return;
	    end if;
	   
	   
	   --Associate now
	   
	   insert
		into
		public.institution_fee_group_association (date_created,
		fee_id,
		group_id,
		applied,
		created_by)
		values(now(),
		fee_due_record.id,
		group_record.id,
		false,
		in_user_id);

	
	    -- if this fee has current child fee which is active, then associate it with the group too but with applied false
	    select * from institution_fees_due ifd where parent_fee_id = fee_due_record.id and ifd.effective_date<=current_date and ifd.end_date>=current_date limit 1 into active_child_fee_record;
	   	if active_child_fee_record.id is not null
	    	then
	    	insert
			into
			public.institution_fee_group_association (date_created,
			fee_id,
			group_id,
			applied,
			created_by)
			values(now(),
			active_child_fee_record.id,
			group_record.id,
			false,
			in_user_id);
		end if;
	    
	   
	   	return_code = 0;
	    return_message = 'Associated';
	    association_id = fee_association_record.id;
	
end;
$function$

CREATE OR REPLACE FUNCTION public.sp_resolve_student_school_details(in_student_id bigint, OUT school_code character varying, OUT school_name character varying, OUT bank_id bigint, OUT bank_code character varying, OUT return_code bigint, OUT return_message character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare



fetch_cursor refcursor;
student_record record;
section_record record;

begin
	-- Get the STUDENT
		OPEN fetch_cursor for 
			select sch.school_code, sch.school_name, nbd.id as bank_id, nbd.bank_code, 
			sch.enable_school_sections_logic, class_id, si.school_id from core_student si 
			inner join core_school sch on si.school_id = sch.id 
			inner join core_school_class isc on si.class_id = isc.id 
			inner join core_nominated_bank nbd on sch.bank_name = nbd.id 
			where si.id = in_student_id limit 1;
			FETCH fetch_cursor INTO student_record;
		close fetch_cursor;	
	
		bank_id = student_record.bank_id;
		bank_code = student_record.bank_code;
		school_code = student_record.school_code;
		school_name = student_record.school_name;
	
		if student_record.enable_school_sections_logic is true 
				then
					--Look for section for this student
					OPEN fetch_cursor for 
						select ssca.*,ss.section_primary_bank, ss.section_name, ss.section_code, nbd.bank_code 
					from school_section_class_association ssca inner join school_sections ss on ss.id = ssca.section_id 
					inner join core_nominated_bank nbd on ss.section_primary_bank = nbd.id
					where ssca.class_id = student_record.class_id and ss.school_id = student_record.school_id LIMIT 1;
						FETCH fetch_cursor INTO section_record;
					close fetch_cursor;			
				
					if section_record.id is not null
					then
						bank_id = section_record.section_primary_bank;
						bank_code = section_record.bank_code;
						school_code = section_record.section_code;
						school_name = format('%s - %s',student_record.school_name, section_record.section_name);
					end if;
					
				end if;
		
	
	return_code = 0;
    return_message = 'OK';
	
end;
$function$

CREATE OR REPLACE FUNCTION public.sp_resolve_payment_channel_account(in_payment_channel bigint, in_bank_id bigint, OUT account_id bigint, OUT account_balance numeric, OUT account_limit numeric, OUT return_code bigint, OUT return_message character varying, OUT channel_name character varying, OUT channel_code character varying, OUT channel_type character varying)
 RETURNS record
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare



fetch_cursor refcursor;
payment_channel_account_record record;
payment_channel_record record;
strategy_record record;
payment_channel_secondary_account_record record;

begin
	-- Get the PAYMENT CHANNEL
		OPEN fetch_cursor for 
		select * from payment_channels where id = in_payment_channel limit 1;
		FETCH fetch_cursor INTO payment_channel_record;
		close fetch_cursor;	
	
		if payment_channel_record.id is null
		then
		return_code = 404;
	    return_message = 'Payment channel not found';
		return;
		end if;
	
	   channel_code = payment_channel_record.channel_code;
	   channel_name = payment_channel_record.channel_name;
	   channel_type = payment_channel_record.channel_type;
	
	-- Get Strategy
		OPEN fetch_cursor for 
			select * from payment_channel_payment_processing_strategies pcpps where id = payment_channel_record.payment_channel_processing_strategy limit 1;
			FETCH fetch_cursor INTO strategy_record;
		close fetch_cursor;
		--
		-- RESOLVE payment channel account to use
	
		IF strategy_record.id IS NULL
		THEN
		-- There was a problem getting the school
		return_code = 208;
	    return_message = 'Payment channel processing strategy not set';
		return;
		END IF; 
	
	IF strategy_record.strategy_code = 'BANK_DIFFERENTIAL'
		THEN
		RAISE NOTICE 'PAYMENT CHANNEL WILL USE DIFFERENTIAL ACCOUNT';
		-- Get secondary account for the school's bank and use that
		-- If its not found please create IT payment_channel_secondary_account_record
			OPEN fetch_cursor for 
			select * from payment_channel_secondary_accounts pcsa where payment_channel = payment_channel_record.id and bank_id = in_bank_id limit 1;
			FETCH fetch_cursor INTO payment_channel_secondary_account_record;
			close fetch_cursor;
		
			IF payment_channel_secondary_account_record.id IS NULL
			THEN
			-- Create IT
			RAISE NOTICE 'SECONDARY ACCOUNT NOT FOUND, LETS CREATE IT';
				-- Get the bank_account_details

				INSERT INTO payment_channel_account_gl(description, balance, account_limit, creation_date, last_transaction_date)
				VALUES ('Secondary A/C ' || payment_channel_record.channel_name || ' Bank ID ' ||  in_bank_id, 
								0, 0, now(), null) RETURNING * INTO payment_channel_account_record;
				-- Now create the secondary account record
				
				INSERT INTO payment_channel_secondary_accounts(
            payment_channel, date_created, bank_id, account_id)
						VALUES (payment_channel_record.id, NOW(), in_bank_id, payment_channel_account_record.id) RETURNING * INTO payment_channel_secondary_account_record;
					
				RAISE NOTICE 'SECONDARY ACCOUNT CREATED FOR PAYMENT CHANNEL';
				else
				 -- load seconday account
				 OPEN fetch_cursor for 
					select * from payment_channel_account_gl pcag where id = payment_channel_secondary_account_record.account_id limit 1 for update;
					FETCH fetch_cursor INTO payment_channel_account_record;
					close fetch_cursor;		
				END IF; 
		
		ELSE
		-- Load the main account
		RAISE NOTICE 'PAYMENT CHANNEL WILL USE MAIN ACCOUNT';
			OPEN fetch_cursor for 
			select * from payment_channel_account_gl pcag where id = payment_channel_record.payment_channel_account_id limit 1 for update;
			FETCH fetch_cursor INTO payment_channel_account_record;
			close fetch_cursor;		
		END IF;
	
	return_code = 0;
    return_message = 'OK';
    account_id = payment_channel_account_record.id;
    account_balance = payment_channel_account_record.balance;
    account_limit = payment_channel_account_record.account_limit;
	
end;
$function$

CREATE OR REPLACE FUNCTION public.apportion_payment_amount_to_plans(in_student_id bigint, in_payment_amount bigint)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
declare


  record_student          json;
  cursor_schedule     cursor for
    select * from payment_plan_student_payment_schedule where student_id = in_student_id and schedule_balance>0 order by id;
  record_schedule record;

  --temps
  temp_amount              int8;
  temp_balance              int8;

begin
  -- Get student
  select get_record_by_int8(in_student_id, 'student_information', 'id')
  into record_student;
  IF (record_student ->> 'id') IS NULL
  THEN
    -- There was a problem getting the school
    RETURN row_to_json(data) FROM ( VALUES (199, 'Student was not found')) data (returncode, returnmessage);
  END IF;


  IF NOT EXISTS (select 1 from payment_plan_student_payment_schedule where student_id = in_student_id and schedule_balance>0)
    then
    -- No schedules updatable
    RETURN row_to_json(data) FROM ( VALUES (0, 'No outstanding updatable schedules')) data (returncode, returnmessage);
  end if;

  if in_payment_amount <= 0 then
    RETURN row_to_json(data) FROM ( VALUES (100, 'Payment amount can not be less than 0')) data (returncode, returnmessage);
  end if;


  -- Open row cursor
  temp_balance := in_payment_amount;
  open cursor_schedule;
  loop
    fetch cursor_schedule into record_schedule;
    exit when not found;
    exit when temp_balance <= 0;

    temp_amount := record_schedule.schedule_balance; --the amount to apportion

    if temp_balance < temp_amount then
      -- Amount remaining is less than schedule balance
      temp_amount = temp_balance;
    end if;

    --Adjust and set balance to balance minus adjustment amount
    UPDATE payment_plan_student_payment_schedule set
      schedule_balance = schedule_balance - temp_amount,
      schedule_paid_so_far = schedule_paid_so_far + temp_amount
      where id = record_schedule.id;

    temp_balance := temp_balance - temp_amount;

    raise notice '%', record_schedule.description;

  end loop;
  close cursor_schedule;



  RETURN row_to_json(data) FROM ( VALUES (0, 'Schedules updated')) data (returncode, returnmessage);
end;
$function$

CREATE OR REPLACE FUNCTION public.reverse_payment_by_receipt_number(in_payment_code bigint, in_receipt_number text, reason text)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
	declare
	--function params
	std_code int8=in_payment_code;
	receipt_no text =in_receipt_number;
	_in_reason text=reason;
	
	--user defined params
	pay_channel record;  -- record holding payment channel details
	chanel_type text;  --chaneel type
	channel_transaction_id text; --new channel transaction id
	original_channel_transaction_id text;  --original transaction id from channel
	proccess_date date;  --channell proccess date
	payment record; --original payment
	school record;  --school information
	curr_date date =now(); --current date
	count_transactions int8;  -- number of transactions identified by the original channel transaction id
	channelTealId int8;
	channelDepositorBranch text;
	reason_for_reversal text;  --reason for reversing the trnasaction
	receipt_number text;   --receipt number generated after new record inserted into payments_received
	originalPayment record;  --
	new_student_gl_balance int8;  -- new student account gl balance after reversal.

	student record; ---student_information record
	payment_channel_transaction_hist_th record;  -- record from payment_channel_account_transaction_history
	school_account_transaction_hist_th record;  -- record from schoolpay_loan_transaction_th
	student_account_transaction_hist record;  ---record from student_account_transaction_history

	paychannel_th_cursor refcursor;  -- list for all payment channel th results
	sch_trans_th_cursor refcursor;  -- list for all sch trans th results
	std_trans_th_cursor refcursor;   -- list for all student transactions th results
	has_fees_due int8;

	pchannel_th_count int8;  --number of records found in the payment_channel_account_transaction_history
	schaccount_th_count int8; ----number of records found in the schoolpay_loan_transaction_th
	stdccount_th_count int8;  --number oif records found in the student_account_transaction_history

	payment_account_id int8;  --new payment account id
	StudentAccountGl record;   --student gl record
  SchoolAccountGl  record;  --school aaccount gl record
  PaymentChannelGl record; -- paymnet channel gl record.

	newPaymentId int8;  --new poayment id
	transaction_date_reversed TIMESTAMP;  --date reversed the transaction
	initial_paymnet_account_balance int8;  --initial account balance in the dgl
	new_paymnet_account_balance int8; -- pay_channel_account gl balance
	description text;

	_date_created TIMESTAMP;

	student_id int8;
 new_school_gl_balance int8;
	

begin
		
			--get student information
		select si.id, si.student_account_id  from student_information si where si.payment_code=std_code into student  limit 1;
		if student is null
			then return row_to_json(data) FROM (VALUES (100, 'NO STUDENT FOUND MATCHING THE GIVEN PAYMENT CODE '||std_code)) data(returncode, returnmessage);
		end if;

			--check if oringal payment transaction exists.
			--check if oringal payment transaction exists.
		raise notice 'Rec is %, studentid is % ', receipt_no, student.id;

		select * from payments_received pr into payment where pr.student_id=student.id and pr.reciept_number=receipt_no limit 1;
		if payment is null
			THEN
					raise notice 'payment is null';

			 return row_to_json(data) FROM (VALUES (100, 'NO PROCESSED PAYMENT FOUND WITH THE SPECIFIED PAYMENT CODE '||std_code|| 'and RECEIPT NUMBER  '||receipt_no)) data(returncode, returnmessage);
		end if;

		--get channel type based on the channnel id
		select id, channel_code, channel_type,payment_channel_account_id,channel_name,payment_channel_processing_strategy from payment_channels into pay_channel where id = payment.payment_channel limit 1;
		
		--RETURN row_to_json(payment);-- from(values(0,receipt_no)) data(a, value);
		if pay_channel.channel_type !='Bank'
			THEN RETURN  row_to_json(data) FROM (VALUES (100, '1-> PAYMENT CHANNEL TYPE CAN NOT REVERSE PAYMENTS')) data(returncode, returnmessage);
		end if;

		--channel_type = request_json ->> 'channel_type';
		channel_transaction_id = 'REVERSAL'||receipt_no;--cast(request_json ->> 'channelTransactionId' as int8);
		original_channel_transaction_id = '';--request_json ->> 'originalChannelTransactionToReverseId';
		proccess_date = now()::TIMESTAMP;--cast(request_json->>'processDate' as date);
		channelTealId =0L;-- request_json->>'channelTealId';
		reason_for_reversal =_in_reason;-- request_json->>'channelReasonForReversal';
		channelDepositorBranch ='';-- request_json->>'channelDepositorBranch';
		
		--get count of transaction with transaction id
		--select count(*) from payments_received  into count_transactions where channel_trans_id=cast(channel_transaction_id as text) and payment_channel=channel_id limit 1;
		--return count_transactions;
		--if payment is not null
			--then return row_to_json(data) FROM (VALUES (100, 'SUPPLIED TRANS ID' || channel_transaction_id || ' WAS ALREADY USED FOR A PREVIOUSLY RECEIVED PAYMENT.')) data(returncode, returnmessage);
		--end if;
		
		-- check original chnanel transaction id
		--select count(*) from payments_received  into count_transactions where channel_trans_id=cast(original_channel_transaction_id as text) and payment_channel=pay_channel.id limit 1;
		

		
		--return row_to_json(payment);

		if payment.reversed = true
			then return row_to_json(data) FROM (VALUES (100, 'PAYMENT IS ALREADY REVERSED. REVERSAL CAN ONLY BE TRIGGERED ONCE.')) data(returncode, returnmessage);
		end if;

		--get school information
		select * from school_information into school where Id=payment.school_id  limit 1;

		
		
		--get payment account id from resolve payment
		select * from resolve_payment_channel(''||row_to_json(pay_channel)||'',pay_channel.id) into payment_account_id;

		--query the payment account GLOBAL
		select * from payment_channel_account_gl into PaymentChannelGl where id=payment_account_id;
		if PaymentChannelGl is NULL
				then  return row_to_json(data) FROM (VALUES (100, 'Sorry, payment channel details not retrievable')) data(returncode, returnmessage);
		end if;
		
		--count the trnasactions from the payment channel transaction history
		select count(*) from payment_channel_account_transaction_history into pchannel_th_count where payment_id=payment.id;
		if pchannel_th_count < 1
			then  return row_to_json(data) FROM (VALUES (100, 'Payment channel TH record not found while reversing '||payment.id)) data(returncode, returnmessage);
		end if;

		--count the trnasactions from  the school account transaction history
		select count(*) from school_account_transaction_history into schaccount_th_count where payment_id=payment.id;
		if schaccount_th_count < 1
			then return row_to_json(data) FROM (VALUES (100, 'School TH record not found while reversing')) data(returncode, returnmessage);
		end if;

		--count the trnasactions from the student account transaction history
		select count(*) from student_account_transaction_history into stdccount_th_count where payment_id=payment.id;
		if stdccount_th_count < 1
			then return row_to_json(data) FROM (VALUES (100, 'Student TH record not found while reversing')) data(returncode, returnmessage);
		end if;
		
		--query school account Gl 
		select * from school_account_gl into SchoolAccountGl where id=payment.school_id;
		if SchoolAccountGl is NULL
				then  return row_to_json(data) FROM (VALUES (100, 'Sorry, institution gl information is missing, contact support for assistance')) data(returncode, returnmessage);
		end if;

		--query student account Gl 
		select * from student_account_gl into StudentAccountGl where id=payment.student_id;
		if StudentAccountGl is NULL
				then return row_to_json(data) FROM (VALUES (100, 'Sorry, student gl information is missing, contact support for assistance')) data(returncode, returnmessage);
		end if;
		
		--create new duplicate record
		insert into payments_received(student_id,school_id,payment_channel,channel_trans_id,channel_memo,amount,channel_process_date,
																	channel_payment_type,channel_depositor_branch,channel_teal_id,reversed,reversal) 
													values(
																payment.student_id, payment.school_id,pay_channel.id, channel_transaction_id, reason_for_reversal ,payment.amount*-1 , 
																proccess_date::TIMESTAMP, 'REVERSAL', channelDepositorBranch,channelTealId, false,  true ) returning id, date_created, reciept_number into newPaymentId, transaction_date_reversed, receipt_number;

		-- update the original transaction
		update payments_received set reversed=true, reversal_payment_id=newPaymentId, date_reversed= transaction_date_reversed where id=payment.id;
		
		--initial payment_channel_account_gl balance before adjustments
		initial_paymnet_account_balance=PaymentChannelGl.balance;

		--create new duplicate transactions for the paymen5 channel transaction histroy
		open paychannel_th_cursor for select * from payment_channel_account_transaction_history  where payment_id=payment.id;
			loop
				--retrieve each independent transaction
				fetch paychannel_th_cursor into payment_channel_transaction_hist_th;
				exit when not found;
				
				--update the payment_channel_account_gl
				update payment_channel_account_gl set balance=balance+(payment_channel_transaction_hist_th.amount*-1) where id =payment_account_id 
				returning balance into new_paymnet_account_balance;
				description='REVERSAL OF TRANS ID '|| original_channel_transaction_id||' - ' ||reason_for_reversal;
				--create new entries into the payment_channel_account_transaction_history
				insert into payment_channel_account_transaction_history(amount,description,trans_type,account_id,reversal_flag,
										reversed,balance_after,balance_before,channel_transaction_processed_date, payment_id)
				values(
								payment_channel_transaction_hist_th.amount*-1 ,''||description||'',
								'REVERSAL',payment_channel_transaction_hist_th.account_id,true,false,
								new_paymnet_account_balance, initial_paymnet_account_balance, proccess_date, newPaymentId
				)returning date_created into _date_created;

			--update old transaction
			update payment_channel_account_transaction_history set reversed=true,date_reversed=_date_created where id=payment_channel_transaction_hist_th.id;

			end loop;
		close paychannel_th_cursor;

		--return row_to_json(data) FROM (VALUES (100, ' new payment id=>'||newPaymentId)) data(returncode, returnmessage);

		-- create new entries vinto school_account_transaction_history
		open sch_trans_th_cursor for select * from school_account_transaction_history  where payment_id=payment.id;
			loop
				--retrieve each independent transaction
				fetch sch_trans_th_cursor into school_account_transaction_hist_th;
				exit when not found;
				
				--update the payment_channel_account_gl
				update school_account_gl set account_balance=account_balance+(school_account_transaction_hist_th.amount*-1) where id =school.school_account_id
				returning account_balance into new_school_gl_balance;
				description='REVERSAL OF TRANS ID '|| original_channel_transaction_id||' - ' ||reason_for_reversal;
				--create new entries into the school_account_transaction_history
				insert into school_account_transaction_history(amount,description,payment_id,trans_type,account_id,reversal_flag,reversed,balance_after)
				values(
								school_account_transaction_hist_th.amount*-1 ,''||description||'' , newPaymentId,
								'REVERSAL',school_account_transaction_hist_th.account_id,true,false,new_school_gl_balance								
				) returning date_created into _date_created;

			--update old transaction
			update school_account_transaction_history set reversed=true,date_reversed=_date_created where id=school_account_transaction_hist_th.id;

			end loop;
		close sch_trans_th_cursor;


		--check if studet has outstanding balance and make adjustments.
		select count(*)  from institution_fees_due FD, institution_fee_student_association FSA 
		where FSA.student_id=payment.student_id and FSA.applied=true and FSA.fee_id=FD.id and FD.effective_date<now() 
		and FD.end_date>now() into has_fees_due;

		-- create new entries vinto school_account_transaction_history
		open std_trans_th_cursor for select * from student_account_transaction_history  where payment_id=payment.id;
			loop
				--retrieve each independent transaction
				fetch std_trans_th_cursor into student_account_transaction_hist;
				exit when not found;
				
				--update the student_account_gl
				update student_account_gl set account_balance=account_balance-student_account_transaction_hist.amount*-1 where id =student.student_account_id returning account_balance into new_student_gl_balance;
			
				--out standing balance cant be greater than 0
				if has_fees_due>0
					then update student_account_gl set outstanding_balance=0::BIGINT where id =student.student_account_id;
				--if student has outstanding fees
				ELSIF has_fees_due<0
					then update student_account_gl set outstanding_balance=outstanding_balance+school_account_transaction_hist_th.amount where id =student.student_account_id;
				end if;

				description='REVERSAL - '||pay_channel.channel_code||' '|| original_channel_transaction_id||' - ' ||reason_for_reversal;
				--create new entries into the school_account_transaction_history
				insert into student_account_transaction_history(amount,description,fee_association_id,payment_id,trans_type,account_id,reversal_flag,
										reversed,balance_after)
				values(
								student_account_transaction_hist.amount*-1 ,''||description||'',
								student_account_transaction_hist.fee_association_id ,newPaymentId,'REVERSAL', 
								student_account_transaction_hist.account_id,true,false, new_student_gl_balance
				) returning date_created into _date_created;

			--update old transaction
			update student_account_transaction_history set reversed=true,date_reversed=_date_created where id=student_account_transaction_hist.id;

			end loop;
		close std_trans_th_cursor;

	return row_to_json(data) FROM (VALUES (0, 'TRANSACTION HAS BEEN REVERSED SUCCESFULLY ', receipt_number)) data(returncode, returnmessage, receipt_number);

END;
$function$

CREATE OR REPLACE FUNCTION public.resolve_payment_channel(payment_chanel text, bankid numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
	declare
	request_json json;
	pay_channel json;
	pay_strategy text;
	secondary_accounts NUMERIC;
	channel_name text;
	nominated_bank_details record;

	channel_pay_strategy int;
	payment_channel_account_id numeric;
	payment_channel_id numeric;
	bank_name text;
	new_gl_id NUMERIC;
	
	main_account text ='MAIN_ACCOUNT';
	channel_dfrnt text = 'BANK_DIFFERENTIAL';
	

begin
		request_json = payment_chanel::json;
		channel_pay_strategy = request_json->>'payment_channel_processing_strategy';
		payment_channel_account_id =request_json->>'payment_channel_account_id';
		payment_channel_id=request_json->>'id';
		channel_name=request_json->>'channel_name';

		select strategy_code into pay_strategy from payment_channel_payment_processing_strategies where id =channel_pay_strategy;
		
		if pay_strategy = channel_dfrnt
				then 
						select count(*) from payment_channel_secondary_accounts into secondary_accounts where payment_channel =payment_channel_id and bank_id = bankid limit 1;
						
						--if secondary_account doesnt exist
						if secondary_accounts =0
								THEN
										select * from core_nominated_bank where id=bankid into nominated_bank_details;
										bank_name = 'Secondary A/C '|| channel_name|| ' ' || nominated_bank_details.bank_name;

										--insert into paymnet accounts gl
										insert into payment_channel_account_gl
											(description, balance, account_limit,creation_date)
											values(bank_name,0,0,now()::TIMESTAMP) RETURNING id into new_gl_id;

											--insert into secondary accounts
											insert into payment_channel_secondary_accounts
											(payment_channel, account_id, bank_id,date_created)
											values(payment_channel_id,new_gl_id,bankid,now()::TIMESTAMP) RETURNING account_id into new_gl_id;
											
											--account id to return
											payment_channel_account_id=new_gl_id;
									
						end if;

			return  payment_channel_account_id;
		end if;
		
		return  payment_channel_account_id;
		
END;
$function$

CREATE OR REPLACE FUNCTION public.reverse_payment(channel_id numeric, reverse_wrapper text)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
	declare
	request_json json;
	pay_channel record;  -- record holding payment channel details
	chanel_type text;  --chaneel type
	channel_transaction_id text; --new channel transaction id
	original_channel_transaction_id text;  --original transaction id from channel
	proccess_date date;  --channell proccess date
	payment record; --original payment
	school record;  --school information
	curr_date date =now(); --current date
	count_transactions numeric;  -- number of transactions identified by the original channel transaction id
	channelTealId NUMERIC;
	channelDepositorBranch text;
	reason_for_reversal text;  --reason for reversing the trnasaction
	receipt_number text;   --receipt number generated after new record inserted into payments_received
	originalPayment record;  --
	new_student_gl_balance numeric;  -- new student account gl balance after reversal.

	student record; ---core_student record
	payment_channel_transaction_hist_th record;  -- record from payment_channel_account_transaction_history
	school_account_transaction_hist_th record;  -- record from schoolpay_loan_transaction_th
	student_account_transaction_hist record;  ---record from student_account_transaction_history

	paychannel_th_cursor refcursor;  -- list for all payment channel th results
	sch_trans_th_cursor refcursor;  -- list for all sch trans th results
	std_trans_th_cursor refcursor;   -- list for all student transactions th results
	has_fees_due numeric;

	pchannel_th_count numeric;  --number of records found in the payment_channel_account_transaction_history
	schaccount_th_count numeric; ----number of records found in the schoolpay_loan_transaction_th
	stdccount_th_count numeric;  --number oif records found in the student_account_transaction_history

	payment_account_id numeric;  --new payment account id
	StudentAccountGl record;   --student gl record
  SchoolAccountGl  record;  --school aaccount gl record
  PaymentChannelGl record; -- paymnet channel gl record.

	newPaymentId numeric;  --new poayment id
	transaction_date_reversed TIMESTAMP;  --date reversed the transaction
	initial_paymnet_account_balance NUMERIC;  --initial account balance in the dgl
	new_paymnet_account_balance NUMERIC; -- pay_channel_account gl balance
	description text;

	_date_created TIMESTAMP;
	new_school_gl_balance NUMERIC;

begin
		request_json = reverse_wrapper::json;

		--get channel type based on the channnel id
		select id, channel_code, channel_type,payment_channel_account_id,channel_name,payment_channel_processing_strategy from payment_channels into pay_channel where id = channel_id limit 1;
		
			--RETURN row_to_json(pay_channel);
		if pay_channel.channel_type<>'Bank'
			THEN RETURN  row_to_json(data) FROM (VALUES (100, '1-> PAYMENT CHANNEL TYPE CAN NOT REVERSE PAYMENTS')) data(returncode, returnmessage);
		end if;

		--channel_type = request_json ->> 'channel_type';
		channel_transaction_id = cast(request_json ->> 'channelTransactionId' as numeric);
		original_channel_transaction_id = request_json ->> 'originalChannelTransactionToReverseId';
		proccess_date = cast(request_json->>'processDate' as date);
		channelTealId = request_json->>'channelTealId';
		reason_for_reversal = request_json->>'channelReasonForReversal';
		channelDepositorBranch = request_json->>'channelDepositorBranch';

		IF channel_id IS NULL
			THEN RETURN  row_to_json(data) FROM (VALUES (100, 'PAYMENT CHANNEL IS NOT SUPPLIED')) data(returncode, returnmessage);
		END IF;
		
		IF pay_channel.channel_type IS NULL
			THEN RETURN  row_to_json(data) FROM (VALUES (100, 'PAYMENT CHANNEL TYPE CAN NOT REVERSE PAYMENTS')) data(returncode, returnmessage);
		END IF;
		
		IF channel_transaction_id IS NULL
			THEN RETURN  row_to_json(data) FROM (VALUES (100, 'CHANNEL TRANS ID IS REQUIRED AND WAS NOT SPECIFIED')) data(returncode, returnmessage);
		END IF;

		IF original_channel_transaction_id IS NULL
			THEN RETURN  row_to_json(data) FROM (VALUES (100, 'ORIGINAL TRANS ID TO BE REVERSED IS REQUIRED')) data(returncode, returnmessage);
		END IF;

		IF proccess_date IS NULL
			THEN RETURN  row_to_json(data) FROM (VALUES (100, 'PROCESS DATE IS REQUIRED AND WAS NOT SPECIFIED')) data(returncode, returnmessage);
		END IF;
		
		-- get count of transaction with transaction id
		select count(*) from payments_received  into count_transactions where channel_trans_id=cast(channel_transaction_id as text) and payment_channel=channel_id limit 1;
		--return count_transactions;
		if count_transactions =1
			then return row_to_json(data) FROM (VALUES (100, 'SUPPLIED TRANS ID' || channel_transaction_id || ' WAS ALREADY USED FOR A PREVIOUSLY RECEIVED PAYMENT.')) data(returncode, returnmessage);
		end if;
		
		-- check original chnanel transaction id
		select count(*) from payments_received  into count_transactions where channel_trans_id=cast(original_channel_transaction_id as text) and payment_channel=pay_channel.id limit 1;
		if count_transactions =0
			then return row_to_json(data) FROM (VALUES (100, 'NO PROCESSED PAYMENT FOUND WITH THE SPECIFIED CHANNEL TRANS ID '||original_channel_transaction_id|| 'and payment channel '||pay_channel.id)) data(returncode, returnmessage);
		end if;

		--check if oringal payment transaction exists.
		select * from payments_received  into payment where channel_trans_id=original_channel_transaction_id and payment_channel=channel_id limit 1;
		--return row_to_json(payment);

		if payment.reversed = true
			then return row_to_json(data) FROM (VALUES (100, 'PAYMENT IS ALREADY REVERSED. REVERSAL CAN ONLY BE TRIGGERED ONCE.')) data(returncode, returnmessage);
		end if;

		--get school information
		select * from core_school into school where Id=payment.school_id  limit 1;

		--get student information
		select id, student_account_id  from core_student into student where id = payment.student_id limit 1;
		
		--get payment account id from resolve payment
		select * from resolve_payment_channel(''||row_to_json(pay_channel)||'',pay_channel.id) into payment_account_id;

		--query the payment account GLOBAL
		select * from payment_channel_account_gl into PaymentChannelGl where id=payment_account_id;
		if PaymentChannelGl is NULL
				then  return row_to_json(data) FROM (VALUES (100, 'Sorry, payment channel details not retrievable')) data(returncode, returnmessage);
		end if;
		
		--count the trnasactions from the payment channel transaction history
		select count(*) from payment_channel_account_transaction_history into pchannel_th_count where payment_id=payment.id;
		if pchannel_th_count < 1
			then  return row_to_json(data) FROM (VALUES (100, 'Payment channel TH record not found while reversing '||payment.id)) data(returncode, returnmessage);
		end if;

		--count the trnasactions from  the school account transaction history
		select count(*) from school_account_transaction_history into schaccount_th_count where payment_id=payment.id;
		if schaccount_th_count < 1
			then return row_to_json(data) FROM (VALUES (100, 'School TH record not found while reversing')) data(returncode, returnmessage);
		end if;

		--count the trnasactions from the student account transaction history
		select count(*) from student_account_transaction_history into stdccount_th_count where payment_id=payment.id;
		if stdccount_th_count < 1
			then return row_to_json(data) FROM (VALUES (100, 'Student TH record not found while reversing')) data(returncode, returnmessage);
		end if;
		
		--query school account Gl 
		select * from school_account_gl into SchoolAccountGl where id=payment.school_id;
		if SchoolAccountGl is NULL
				then  return row_to_json(data) FROM (VALUES (100, 'Sorry, institution gl information is missing, contact support for assistance')) data(returncode, returnmessage);
		end if;

		--query student account Gl 
		select * from student_account_gl into StudentAccountGl where id=payment.student_id;
		if StudentAccountGl is NULL
				then return row_to_json(data) FROM (VALUES (100, 'Sorry, student gl information is missing, contact support for assistance')) data(returncode, returnmessage);
		end if;
		
		--create new duplicate record
		insert into payments_received(student_id,school_id,payment_channel,channel_trans_id,channel_memo,amount,channel_process_date,
																	channel_payment_type,channel_depositor_branch,channel_teal_id,reversed,reversal) 
													values(
																payment.student_id, payment.school_id,pay_channel.id, channel_transaction_id, payment.channel_memo ,payment.amount*-1 , 
																proccess_date::TIMESTAMP, 'REVERSAL', channelDepositorBranch,channelTealId, false,  true ) returning id, date_created, reciept_number into newPaymentId, transaction_date_reversed, receipt_number;

		-- update the original transaction
		update payments_received set reversed=true, reversal_payment_id=newPaymentId, date_reversed= transaction_date_reversed where id=payment.id;
		
		--initial payment_channel_account_gl balance before adjustments
		initial_paymnet_account_balance=PaymentChannelGl.balance;

		--create new duplicate transactions for the paymen5 channel transaction histroy
		open paychannel_th_cursor for select * from payment_channel_account_transaction_history  where payment_id=payment.id;
			loop
				--retrieve each independent transaction
				fetch paychannel_th_cursor into payment_channel_transaction_hist_th;
				exit when not found;
				
				--update the payment_channel_account_gl
				update payment_channel_account_gl set balance=balance+(payment_channel_transaction_hist_th.amount*-1) where id =payment_account_id 
				returning balance into new_paymnet_account_balance;
				description='REVERSAL OF TRANS ID '|| original_channel_transaction_id||' - ' ||reason_for_reversal;
				--create new entries into the payment_channel_account_transaction_history
				insert into payment_channel_account_transaction_history(amount,description,trans_type,account_id,reversal_flag,
										reversed,balance_after,balance_before,channel_transaction_processed_date, payment_id)
				values(
								payment_channel_transaction_hist_th.amount*-1 ,''||description||'',
								'REVERSAL',payment_channel_transaction_hist_th.account_id,true,false,
								new_paymnet_account_balance, initial_paymnet_account_balance, proccess_date, newPaymentId
				)returning date_created into _date_created;

			--update old transaction
			update payment_channel_account_transaction_history set reversed=true,date_reversed=_date_created where id=payment_channel_transaction_hist_th.id;

			end loop;
		close paychannel_th_cursor;

		--return row_to_json(data) FROM (VALUES (100, ' new payment id=>'||newPaymentId)) data(returncode, returnmessage);

		-- create new entries vinto school_account_transaction_history
		open sch_trans_th_cursor for select * from school_account_transaction_history  where payment_id=payment.id;
			loop
				--retrieve each independent transaction
				fetch sch_trans_th_cursor into school_account_transaction_hist_th;
				exit when not found;
				
				--update the payment_channel_account_gl
				update school_account_gl set account_balance=account_balance+(school_account_transaction_hist_th.amount*-1) where id =school.school_account_id
				returning account_balance into new_school_gl_balance;

				description='REVERSAL OF TRANS ID '|| original_channel_transaction_id||' - ' ||reason_for_reversal;
				--create new entries into the school_account_transaction_history
				insert into school_account_transaction_history(amount,description,payment_id,trans_type,account_id,reversal_flag,reversed,balance_after)
				values(
								school_account_transaction_hist_th.amount*-1 ,''||description||'' , newPaymentId,
								'REVERSAL',school_account_transaction_hist_th.account_id,true,false,new_school_gl_balance
				) returning date_created into _date_created;

			--update old transaction
			update school_account_transaction_history set reversed=true,date_reversed=_date_created where id=school_account_transaction_hist_th.id;

			end loop;
		close sch_trans_th_cursor;


		--check if studet has outstanding balance and make adjustments.
		select count(*)  from institution_fees_due FD, institution_fee_student_association FSA 
		where FSA.student_id=payment.student_id and FSA.applied=true and FSA.fee_id=FD.id and FD.effective_date<now() 
		and FD.end_date>now() into has_fees_due;

		-- create new entries vinto school_account_transaction_history
		open std_trans_th_cursor for select * from student_account_transaction_history  where payment_id=payment.id;
			loop
				--retrieve each independent transaction
				fetch std_trans_th_cursor into student_account_transaction_hist;
				exit when not found;
				
				--update the student_account_gl
			--	update student_account_gl set account_balance=account_balance-student_account_transaction_hist.amount*-1 where id =student.student_account_id returning account_balance into new_student_gl_balance;
				update student_account_gl set account_balance=account_balance-student_account_transaction_hist.amount where id =student.student_account_id returning account_balance into new_student_gl_balance;

				--out standing balance cant be greater than 0
				if has_fees_due>0
					then update student_account_gl set outstanding_balance=0::BIGINT where id =student.student_account_id;
				--if student has outstanding fees
				ELSIF has_fees_due<0
					then update student_account_gl set outstanding_balance=outstanding_balance+school_account_transaction_hist_th.amount where id =student.student_account_id;
				end if;

				description='REVERSAL - '||pay_channel.channel_code||' '|| original_channel_transaction_id||' - ' ||reason_for_reversal;
				--create new entries into the school_account_transaction_history
				insert into student_account_transaction_history(amount,description,fee_association_id,payment_id,trans_type,account_id,reversal_flag,
										reversed,balance_after)
				values(
								student_account_transaction_hist.amount*-1 ,
								''||description||'',
								student_account_transaction_hist.fee_association_id ,newPaymentId,'REVERSAL', 
								student_account_transaction_hist.account_id,true,false,
								new_student_gl_balance
				) returning date_created into _date_created;

			--update old transaction
			update student_account_transaction_history set reversed=true,date_reversed=_date_created where id=student_account_transaction_hist.id;

			end loop;
		close std_trans_th_cursor;

	return row_to_json(data) FROM (VALUES (0, 'TRANSACTION HAS BEEN REVERSED SUCCESFULLY ', receipt_number)) data(returncode, returnmessage, receipt_number);

END;
$function$


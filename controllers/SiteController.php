<?php

namespace app\controllers;

use app\components\ToWords;
use app\models\ResendVerificationEmailForm;
use app\models\SignupForm;
use app\models\StudentPasswordResetRequestForm;
use app\models\StudentResetPasswordForm;
use app\models\User;
use app\models\UserSchoolAssociation;
use app\models\VerifyEmailForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\modules\banks\models\BankDetails;
use app\modules\e_learning\models\Teleconferencing;
use app\modules\logs\models\Logs;
use app\modules\messages\models\MessageOutbox;
use app\modules\reports\models\HomeStats;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use DateTime;
use Exception;
use Yii;
use yii\base\BaseObject;
use yii\base\DynamicModel;
use yii\base\InvalidArgumentException;

use yii\db\Expression;

use yii\data\Pagination;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii2fullcalendar\models\Event;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */

//    public function beforeAction($action) {
//
//
//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }
//
////something code right here if user valid
//
//        return true;
//
//
//
//    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index'],
                'except' => ['reset-all'],

                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'app\components\CustomCaptchaAction',
                'foreColor' => 0xF89033,
                'backColor' => 0x293894,


            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
//    public function actionIndex()
//    {
//        $model = new User();
//        if(!Yii::$app->user->identity){
//            return $this->render('login', ['model'=>$model]);
//        }
//        return $this->render('index');
//
//
//    }
    public function actionIndex()
    {
//        if (Yii::$app->user->isGuest)
//            return $this->redirect('site/login');

        if (ToWords::isSchoolUser() && Yii::$app->user->can('r_report') && isset(Yii::$app->user->identity->school_id)) {
            return $this->schAdmin();
        } else if (ToWords::isSchoolUser() && !Yii::$app->user->can('r_report') && isset(Yii::$app->user->identity->school_id)) {
            return Yii::$app->runAction('schoolcore/core-student/index');
        } else if (Yii::$app->user->can('schoolsuite_admin')) {
            //schoolpay_admin role is shared by all admin groups
            return $this->sysAdmin();
        } else if (Yii::$app->user->can('bank_user')) {
            return Yii::$app->runAction('/banks/reconciliation/chn-sch');
            // return $this->homePage('bank_index');
        } else if (!Yii::$app->user->can('non_student')) {
            return Yii::$app->runAction('/schoolcore/core-student/student-index');
        } else {
            return $this->homePage('other_index');
        }

    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionLogin()
    {
        $this->layout = 'loginlayout';
        if (!Yii::$app->user->isGuest) {
            if (ToWords::isSchoolUser()) {
                if (!Yii::$app->user->identity->school_id) {
                    $userSch = User::asscSchools(Yii::$app->user->identity->id);
                    if ($userSch) {
                        Logs::logEvent("Logged in", null);
                        return $this->redirect(['/site/sel-sch']);
                    }
                }
            }
            $this->redirect(['/site/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->login()) {
                if ($user->password_expiry_date <= Date('Y-m-d')) {
                    return $this->redirect(['/site/expiry-reset', 'id' => $user->id]);
                }
                if ($this->validatePasswordFreshness($user->id) <= 14) {
                    Yii::$app->session->setFlash('error', 'Dear user, your password is about to expire. You are encouraged to change it before it expires');

                }
//                if($user->hasAttribute('student_code')){
//                    Yii::trace('student');
//                    Yii::$app->student->login($user);
//                }else{
//                    Yii::trace('other');
//                    Yii::$app->user->login($user);
//                }
                Yii::$app->user->login($user);

                Logs::logEvent("Logged in", null);
                Yii::trace("koo");

                if (ToWords::isSchoolUser()) {
                    $userSch = User::asscSchools(Yii::$app->user->identity->id);
                    Yii::trace($userSch);
                    if ($userSch) {
                        if (count($userSch) == 1) {
                            $user->school_id = $userSch[0]['school_id'];
                            $user->save(false);
                            UserSchoolAssociation::delAssc($user->id);
                            return $this->redirect(['/site/index']);
                        }
                        return $this->redirect(['/site/sel-sch']);
                    } else {
                        return $this->redirect(['/site/index']);
                    }

                } else {
                    return $this->redirect(['/site/index']);
                }
            } else {
                $model->password = '';
                Logs::logFailedLogin($model->username, "LoginFailed", "Wrong Password or Username");
            }
        }
        return $this->render('login', ['model' => $model]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/site/login'])->send();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $this->layout = '@app/views/layouts/web_site';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                Yii::$app->mailer->compose()
                    ->setTo(Yii::$app->params['adminEmail'])
                    ->setFrom([$model->email => $model->name])
                    ->setSubject($model->subject)
                    ->setTextBody($model->body)
                    ->send();
                Logs::logEvent("Website email sent: <b>Subj</b>: " . $model->subject . "   <b>Email</b>: " . $model->email . " <b>Name</b>: " . $model->name, null, null);
                Yii::$app->session->setFlash('contactFormSubmitted');
                return $this->refresh();
            } catch (Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Client failed to contact us:<b>Name</b>: " . $model->name . ", <b>Email</b>: " . $model->email . ", <b>Message</b>: " . $model->body, $error, null);
                Yii::$app->session->setFlash('contactFormFailed');
            }

        }
        return $this->render('contact', [
            'model' => $model
        ]);

    }

    public function actionSelSch()
    {
        $this->layout = 'loginlayout';
        $userSch = User::asscSchools(Yii::$app->user->identity->id);
        return $this->render('selected_stu', ['model' => $userSch]);

    }

    private function homePage($homepage)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax($homepage) : $this->render($homepage);
    }

    private function schAdmin()
    {
        $request = Yii::$app->request;
        $searchModel = new DynamicModel(['start_date', 'end_date']);
        $searchModel->addRule(['start_date', 'end_date'], 'safe');
        $searchModel->load(Yii::$app->request->get());
        $res = (new HomeStats())->schAdmin($searchModel);
        $res['searchModel'] = $searchModel;
        return ($request->isAjax) ? $this->renderAjax('sch_index', $res) : $this->render('sch_index', $res);
    }

    private function sysAdmin()
    {

        if(Yii::$app->user->can('view_by_branch')) {
            if (empty(Yii::$app->user->identity->branch_id)) {
                return $this->redirect(['/user/update-branch', 'id' => Yii::$app->user->id]);

            }
        }
        if(Yii::$app->user->can('view_by_region')) {
            if (empty(Yii::$app->user->identity->region_id)) {
                return $this->redirect(['/user/update-region', 'id' => Yii::$app->user->id]);

            }
        }
        $request = Yii::$app->request;
        $res = (new HomeStats())->sysAdmin();
        return ($request->isAjax) ? $this->renderAjax('admin_index', $res) : $this->render('admin_index', $res);

    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $this->layout = '@app/views/layouts/web_site';
        return $this->render('about');
    }


    public function actionAssignSch($id)
    {
        User::setSchoolId($id);
        // return Yii::$app->runAction('/site/index');
        return $this->redirect(['/site/index']);
    }


    /**
     * Verify email address
     *
     * @param string $token
     * @return yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }



    public function actionExpiryReset($id)
    {
        $this->layout = 'loginlayout';
        $request = Yii::$app->request;
        //   if (Yii::$app->user->can('non_student')) {
        $model = User::find()->where(['id' => $id])->limit(1)->one();
        $model->scenario = 'change';

        try {
            if ($model) {

                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    Yii::trace("here");
                    $data = Yii::$app->request->post();
                    $passwd = $data['User']['new_pass'];
//                    $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
                    UserController::enforcePasswordPolicy($passwd);
                    $model->password_expiry_date = date('Y-m-d', strtotime('+1 months'));

                    $model->setPassword($passwd);
                    Yii::trace("saving " . $model->save(false));
                    if ($model->save(false)) {
                        Yii::trace("saved");
                        Yii::$app->user->login($model);
                        if (ToWords::isSchoolUser()) {
                            $userSch = User::asscSchools(Yii::$app->user->identity->id);
                            if ($userSch) {
                                if (count($userSch) == 1) {
                                    $model->school_id = $userSch[0]['school_id'];
                                    $model->save(false);
                                    Logs::logEvent("Logged in", null);
                                    return $this->redirect(['/site/index']);


                                }
                                return $this->redirect(['/site/sel-sch']);
                            } else {
                                Logs::logEvent("Logged in", null);
                                return $this->redirect(['/site/index']);
                            }
                        }
                        return $this->redirect(['site/index']);
                    }
                }

                return ($request->isAjax) ? $this->renderAjax('reset_password', ['model' => $model]) :
                    $this->render('reset_password', ['model' => $model]);
            }
        } catch (Exception $e) {
           // $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            $params = ['model' => $model, 'error' => $e->getMessage()];

            $model->addError('new_pass', $e->getMessage());
            return ($request->isAjax) ? $this->renderAjax('reset_password', $params) :
                $this->render('reset_password', $params);
        }


    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
//    public function actionRequestPasswordReset()
//    {
//        $this->layout = 'loginlayout';
//        $model = new PasswordResetRequestForm();
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            if ($model->sendEmail()) {
//                return $this->render('requestPasswordResetToken', [
//                    'model' => $model,
//                ]);
//
//            } else {
//                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
//
//            }
//
//        }
//
//        return $this->render('requestPasswordResetToken', [
//            'model' => $model,
//        ]);
//    }
    public function actionRequestPasswordReset()
    {
        $this->layout = 'loginlayout';
        $model = new PasswordResetRequestForm();

            if ($model->load(Yii::$app->request->post())) {
                try {
                    //$reset =null;
                    $reset = $model->sendEmail();
                    $userEmail = $model->getEmail();
                    if ($reset) {
                        Yii::$app->session->setFlash('password', "<i class='fa fa-check-circle-o'></i>&nbsp; We have sent an email to $userEmail. Please check your inbox for more information and next steps.");
                    } else {
                        Yii::$app->session->setFlash('error', "<i class='fa fa-check-circle-o'></i>&nbsp; Sorry, we are unable to reset password for the provided email address, please contact support on 0115-57-13-24");
                    }


                } catch (\Exception $e) {
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Logs::logEvent("Failed to reset  user password: ", $error, null);
                    return "<div class='alert alert-danger' style='    background-color: #dd4b39 !important;'>Password reset failed<br><br>
                    <p>Incorrect username or email, please try again </p> </div>";
                }
            }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);

    }



    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
//    public function actionResetPassword($token)
//    {
//        $this->layout = 'loginlayout';
//        try {
//            $model = new ResetPasswordForm($token);
//        } catch (InvalidArgumentException $e) {
//            throw new BadRequestHttpException($e->getMessage());
//        }
//
//        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
//
//            return $this->redirect(['/site/login'])->send() && Yii::$app->session->setFlash('success', 'New password saved.');
//        }
//
//        return $this->render('resetPassword', [
//            'model' => $model,
//        ]);
//    }

    public function actionResetPassword($token)
    {
        $this->layout = 'loginlayout';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            $model->password = '';
            Yii::$app->session->addFlash('passwdReset', "<div class='alert alert-success text-center'> You have successfully reset your password <br><p><b><a style='color:#3f31a3;' href='" . Url::to(['/site']) . "'>Return to login page</a></b></p> </div>");

        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

//Student password reset

    public function actionStudentRequestPasswordReset()
    {
        $this->layout = 'loginlayout';
        $model = new StudentPasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->addFlash('success', 'Check your email for further instructions.');


            } else {
                Yii::$app->session->addFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('studentRequestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionStudentResetPassword($token)
    {
        $this->layout = 'loginlayout';
        try {
            $model = new StudentResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->addFlash('passwdReset', "<div class='alert alert-success text-center'> You have successfully reset your password <br><p><b><a style='color:#3f31a3;' href='" . Url::to(['/site']) . "'>Return to login page</a></b></p> </div>");
        }

        return $this->render('studentResetPassword', [
            'model' => $model,
        ]);
    }


    /**
     * Checks when a user's password was last updated and returns
     * an error code.
     */


    private function validatePasswordFreshness($id)

    {
        $model = User::find()->where(['id' => $id])->limit(1)->one();

        $lastUpdateTime = strtotime($model->password_expiry_date);

        $now = time();


        $elapsedTime = ($lastUpdateTime - $now);


        $elapsedDays = round($elapsedTime / 86400);


        return $elapsedDays;


    }

    /**
     * [actionJsoncalendar description]
     * @param  [type] $start [description]
     * @param  [type] $end   [description]
     * @param  [type] $_     [description]
     * @return [type]        [description]
     */
    public function actionJsoncalendar($start = NULL, $end = NULL, $_ = NULL)
    {
        $events = array();

        //Demo
        $Event = new Event();
        $Event->id = 1;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\TH:m:s\Z');
        $events[] = $Event;

        $Event = new Event();
        $Event->id = 2;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\TH:m:s\Z', strtotime('tomorrow 8am'));
        $events[] = $Event;


        $event3 = new DateTime('+2days 10am');
        $Event = new Event();
        $Event->id = 2;
        $Event->title = 'Testing';
        $Event->start = $event3->format('Y-m-d\Th:m:s\Z');
        $Event->end = $event3->modify('+3 hours')->format('Y-m-d\TH:m:s\Z');
        $events[] = $Event;


        header('Content-type: application/json');
        echo Json::encode($events);

        Yii::$app->end();
    }

    public function actionR($i)
    {
        $id = base64_decode($i);
        $query = (new Query())->select(['th.date_created', 'si.first_name', 'si.last_name', 'th.trans_type', 'si.middle_name', 'sch.school_name', 'sch.school_logo', 'nb.bank_logo', 'si.student_code as payment_code', 'si.school_student_registration_number AS reg_no', 'pc.channel_name', 'pc.payment_channel_logo', 'pc.channel_type', 'th.amount', 'th.description', 'pr.channel_memo', 'pr.reciept_number', 'pr.channel_depositor_branch', 'pr.channel_depositor_name', 'im.image_base64', 'asr.settlement_bank_id'])
            ->from('student_account_transaction_history th')
            ->innerJoin('core_student si', 'si.student_account_id=th.account_id')
            ->innerJoin('core_school sch', 'sch.id=si.school_id')
            ->innerJoin('core_nominated_bank nb', 'nb.id=sch.bank_name')
            ->innerJoin('payments_received pr', 'pr.id=th.payment_id')
            ->leftJoin('auto_settlement_requests asr', 'pr.id=asr.payment_id')//We will use settlement bank logo as water mark
            ->leftJoin('payment_channels pc', 'pc.id=pr.payment_channel')
            ->leftJoin('image_bank im', 'im.id=sch.school_logo')
            ->where(['pr.id' => $id])
            ->limit(1)
            ->one(Yii::$app->db); //Use the backup db

        if ($query) {
            $html = $this->renderPartial('trans_desc_pdf', array(
                'trans' => $query,
            ));
            ob_clean();
            $logo = ($query['channel_type'] == 'Mobile') ? $query['bank_logo'] : $query['payment_channel_logo'];
            if ($query['settlement_bank_id']) {
                //Transaction has a settlement record
                //So logo should be logo of settlement bank
                $settlementBank = BankDetails::findOne(['id' => $query['settlement_bank_id']]);
                if ($settlementBank->bank_logo) $logo = $settlementBank->bank_logo;
            }
            $watermark = Url::to(['/import/import/image-link2', 'id' => $logo], true);
            Yii::trace($watermark);
            return Yii::$app->pdf->exportReciept(['title' => 'Trans Reciept',
                'filename' => 'trans_reciept', 'html' => $html, 'logo' => $watermark]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');

        }

    }


    public function actionSf($i)
    {
        $id = base64_decode($i);
        $query = (new Query())->select(['pr.date_created', 'si.first_name', 'si.last_name', 'si.middle_name', 'sch.school_name', 'sch.school_logo', 'nb.bank_logo', 'si.student_code as payment_code', 'si.school_student_registration_number AS reg_no', 'pc.channel_name', 'pc.payment_channel_logo', 'pc.channel_type', 'pr.amount', 'ifd.description', 'pr.channel_memo', 'pr.reciept_number', 'pr.channel_depositor_branch', 'pr.channel_depositor_name', 'im.image_base64', 'asr.settlement_bank_id'])
            ->from('supplementary_fee_payments_received pr')
            ->innerJoin('core_student si', 'si.id=pr.student_id')
            ->innerJoin('core_school sch', 'sch.id=si.school_id')
            ->innerJoin('core_nominated_bank nb', 'nb.id=sch.bank_name')
            ->innerJoin('institution_supplementary_fees ifd', 'ifd.id=pr.fee_id')
            ->leftJoin('auto_settlement_requests asr', 'pr.id=asr.payment_id')//We will use settlement bank logo as water mark
            ->leftJoin('payment_channels pc', 'pc.id=pr.payment_channel')
            ->leftJoin('image_bank im', 'im.id=sch.school_logo')
            ->where(['pr.id' => $id])
            ->limit(1)
            ->one(Yii::$app->db); //Use the backup db

        if ($query) {
            $html = $this->renderPartial('supplementary_desc_pdf', array(
                'trans' => $query,
            ));
            ob_clean();
            $logo = ($query['channel_type'] == 'Mobile') ? $query['bank_logo'] : $query['payment_channel_logo'];
            if ($query['settlement_bank_id']) {
                //Transaction has a settlement record
                //So logo should be logo of settlement bank
                $settlementBank = BankDetails::findOne(['id' => $query['settlement_bank_id']]);
                if ($settlementBank->bank_logo) $logo = $settlementBank->bank_logo;
            }
            $watermark = Url::to(['/import/import/image-link2', 'id' => $logo], true);
            Yii::trace($watermark);
            return Yii::$app->pdf->exportReciept(['title' => 'Trans Reciept',
                'filename' => 'trans_reciept', 'html' => $html, 'logo' => $watermark]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');

        }

    }

    public function actionM($i)
    {
        $id = base_convert($i, 36, 10);
        $query = (new Query())->select(['th.date_created', 'si.first_name', 'si.last_name', 'th.trans_type', 'si.middle_name', 'sch.school_name', 'sch.school_logo', 'nb.bank_logo', 'si.student_code as payment_code', 'si.school_student_registration_number AS reg_no', 'pc.channel_name', 'pc.payment_channel_logo', 'pc.channel_type', 'th.amount', 'th.description', 'pr.channel_memo', 'pr.reciept_number', 'pr.channel_depositor_branch', 'pr.channel_depositor_name', 'im.image_base64', 'asr.settlement_bank_id'])
            ->from('student_account_transaction_history th')
            ->innerJoin('core_student si', 'si.student_account_id=th.account_id')
            ->innerJoin('core_school sch', 'sch.id=si.school_id')
            ->innerJoin('core_nominated_bank nb', 'nb.id=sch.bank_name')
            ->innerJoin('payments_received pr', 'pr.id=th.payment_id')
            ->leftJoin('auto_settlement_requests asr', 'pr.id=asr.payment_id')//We will use settlement bank logo as water mark
            ->leftJoin('payment_channels pc', 'pc.id=pr.payment_channel')
            ->leftJoin('image_bank im', 'im.id=sch.school_logo')
            ->where(['pr.id' => $id])
            ->limit(1)
            ->one(Yii::$app->db); //Use the backup db

        if ($query) {
            $html = $this->renderPartial('trans_desc_pdf', array(
                'trans' => $query,
            ));
            ob_clean();
            $logo = ($query['channel_type'] == 'Mobile') ? $query['bank_logo'] : $query['payment_channel_logo'];
            if ($query['settlement_bank_id']) {
                //Transaction has a settlement record
                //So logo should be logo of settlement bank
                $settlementBank = BankDetails::findOne(['id' => $query['settlement_bank_id']]);
                if ($settlementBank->bank_logo) $logo = $settlementBank->bank_logo;
            }
            $watermark = Url::to(['/import/import/image-link2', 'id' => $logo], true);
            Yii::trace($watermark);
            return Yii::$app->pdf->exportReciept(['title' => 'Trans Reciept',
                'filename' => 'trans_reciept', 'html' => $html, 'logo' => $watermark]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');

        }
    }

    public function actionTest()
    {
        return $this->render('test');
    }

    public function actionHomePage()
    {
        $this->layout = 'siteLayout';
        return $this->render('home');
    }


    public function actionTeleLogout($id)
    {
        $meetingDetails = Teleconferencing::findOne(['meeting_id' => $id]);
        $meetingDetails->meeting_active = false;
        $meetingDetails->meeting_stop = new Expression('NOW()');
        $meetingDetails->save(false);

        // $this->layout = 'siteLayout';
        return $this->render('tele_logout');
    }


    public function actionStusearch()
    {
        $this->layout = '@app/views/layouts/web_site';
        $searchModel = new CoreStudentSearch();
        $request = '';
        $search_error = '';
        if (!$searchModel->load(Yii::$app->request->get())) {
            $search_error = 'Invalid search data';
            return $this->render('stu_sch_search', ['searchModel' => $searchModel, 'request' => $request, 'search_error' => $search_error]);
        }
        if (empty($searchModel->schsearch)) {
            $searchModel->addError('schsearch', 'Search and select your school');
            return $this->render('stu_sch_search', ['searchModel' => $searchModel, 'request' => $request, 'search_error' => $search_error]);
        }

        if (empty($searchModel->modelSearch) || strlen($searchModel->modelSearch) < 3) {
            $searchModel->addError('modelSearch', 'Student name is required & should be atleast 3 characters');
            return $this->render('stu_sch_search', ['searchModel' => $searchModel, 'request' => $request, 'search_error' => $search_error]);
        }


//        $request = $_GET['StudentSearch'];
        $request = Yii::$app->request->get('StudentSearch');
        if (!$searchModel->modelSearch || !$searchModel->schsearch) {
            $search_error = 'School and student names are required';
            return $this->render('stu_sch_search', ['searchModel' => $searchModel, 'request' => $request, 'search_error' => $search_error]);
        }

        $record = $searchModel->findStudentInfo();
        if ($record) {
            return (count($record) > 1) ? $this->render('_stu_found', ['record' => $record]) :
                $this->render('stu_results', ['record' => $record[0]]);
        } else {
            $search_error = 'Student not found, please refine your search and try again';
        }

        return $this->render('stu_sch_search', ['searchModel' => $searchModel, 'request' => $request, 'search_error' => $search_error]);
    }

    public function actionMyTrans()
    {
        $this->layout = '@app/views/layouts/web_site';
        $searchModel = new CoreStudentSearch();
        /*$captchaModel = new DynamicModel(['captcha']);
        $captchaModel->addRule(['captcha'], 'required');
        $captchaModel->addRule('captcha', 'captcha');*/
        $request = '';
        if (!$searchModel->load(Yii::$app->request->get()))
            return $this->render('student', ['searchModel' => $searchModel, 'request' => $request]);


        $request = $searchModel->pc_rgnumber;
        $record = $searchModel->findStudent();
        if ($record) {
            $res = isset($record[0]) ? $record[0] : $record;
            return $this->render('stu_results', ['record' => $res]);
        } else if (is_countable($record) && count(array($record)) > 1) {
            return $this->render('_stu_found', ['record' => $record]);
        }
        return $this->render('student', ['searchModel' => $searchModel, 'request' => $request]);
    }


    public function actionSchoolsAndChannels()
    {
        $this->layout = '@app/views/layouts/web_site';
        $model = new DynamicModel(['school']);
        $query = (new Query())->select(['school_name'])->from('core_school');
        $query2 = (new Query())->select(['channel_name', 'payment_instructions AS instructions', 'payment_channel_logo'])->from('payment_channels')->orderBy('channel_type DESC', 'channel_name');
        $pages = new Pagination(['totalCount' => $query->count()]);
        $query->offset($pages->offset)->orderBy('school_name')->limit($pages->limit);
        return $this->render('schools_and_channels', ['schools' => $query->all(), 'channels' => $query2->all(), 'pages' => $pages, 'model' => $model]);
    }

    public function actionSchool($id)
    {
        $query = (new Query())->select(['sch.id', 'sch.school_name', 'sch.village', 'img.image_base64'])
            ->from('core_school sch')
            ->leftJoin('image_bank img', 'img.id=sch.school_logo')
            ->where(['sch.id' => $id])->limit(1)->one();
        $img = $query['image_base64'] ? '<div class="col-xs-2 no-padding" style="margin:auto"><img class="img-thumbnail", src="data:image/jpeg;base64, ' . $query["image_base64"] . '" width="70" /></div>' : '';
        $html = $img ? $img . "<div class='col-xs-10 no-padding'>" : "<div class='col-xs-12'>";
        $html = $html . '<b>' . $query['school_name'] . '</b><br>' . $query['village'] . '<br> <a class="btn btn-sm btn-primary" href="' . Url::to(['/site/get-pc', 'sch' => $query['id']]) . '" >Request Payment Code</a></div>';
        return $html;
    }

    /*  public function actionGetPPP($sch = 0)
      {
          $this->layout = 'siteLayout';

          $model = new StudentPaymentCodeRequests();
          if ($sch > 0) {
              $model->school_id = $sch;
          }
          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
              $model->first_name = ucfirst($model->first_name);
              $model->last_name = ucfirst($model->last_name);
              $model->middle_name = ucfirst($model->middle_name);
              $model->save(false);
              try {
                  $adminEmail = Yii::$app->params['adminEmail'];
                  \Yii::$app->mailer->compose()
                      ->setTo($adminEmail)
                      ->setFrom([$adminEmail => $model->first_name . ' ' . $model->last_name])
                      ->setSubject('Payment code Request ' . $model->first_name . ' ' . $model->last_name)
                      ->setTextBody("Student Name: " . $model->first_name . " " . $model->middle_name . " " . $model->last_name . "\nReg No: " . $model->school_student_registration_number . "\nStudent Phone: " . $model->student_phone . "\nGuardian: " . $model->gurdian_name . "\nGuardian Phone: " . $model->gurdian_phone . "\nStudent Email: " . $model->student_email . "\n")
                      ->send();
              } catch (\Exception $e) {

              }

              return $this->redirect(['request-submitted', 'id' => $model->id]);
          }
          return $this->render('request_pc', ['model' => $model]);

      }*/

    public function actionStu($id)
    {
        $this->layout = '@app/views/layouts/web_site';
        $clearId = ToWords::decrypt($id);
        if (!$clearId)
            throw new ForbiddenHttpException('Invalid request');
        return $this->render('stu_results', [
            'record' => $this->findModel($clearId),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = CoreStudent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionFinanceMenu()
    {
//        return $this->render('finance_menu');
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('finance_menu') :
            $this->render('finance_menu');
    }

    public function actionLibraryMenu()
    {
      //  return $this->render('library_menu');
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('library_menu') :
            $this->render('library_menu');
    }



}

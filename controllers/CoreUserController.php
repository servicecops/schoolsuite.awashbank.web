<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoreUserController implements the CRUD actions for User model.
 * @author Ibrahim Kitagenda <ibrahim.kitagenda@gmail.com>
 */
class CoreUserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionView($id)
    {
        $model = User::find()->where(['id'=>$id])->limit(1)->one();
        return Yii::$app->runAction('/backofficeuser/view', ['id'=>$model->userTypeId]);
//        if($model->type =='bo'){
//            return Yii::$app->runAction('/backofficeuser/view', ['id'=>$model->userTypeId]);
//        } else if($model->type=='agent'){
//           return Yii::$app->runAction('/agent/view', ['id'=>$model->userTypeId]);
//        } else if($model->type=='bank_user'){
//            return Yii::$app->runAction('/bank-user/view', ['id'=>$model->userTypeId]);
//        }
    }

    /**
     * User changes his/her own password.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionChange()
    {
      
      if(!($_GET['id'] == Yii::$app->user->identity->id)){
          throw new ForbiddenHttpException('Insufficient privileges to access this area.');
      }
        $request = Yii::$app->request;
        $model=$this->findModel(Yii::$app->user->id);
        $model->scenario = 'change';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
            $model->setPassword($model->new_pass);
            if($model->save(false)){
              return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('change_pass', $res) : $this->render('change_pass', $res);

    }

    /**
     * Reset user password by sys_admin /super_admin
     * @param $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionReset($id)
    {

            if(!Helpers::can('w_bo')){
                throw new ForbiddenHttpException('Insufficient privileges to access this area.');
            }
            $request = Yii::$app->request;
            $model=$this->findModel($id);
            $model->scenario = 'reset';

          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//              $model->password_expiry_date = date('Y-m-d');
              $model->setPassword($model->new_pass);
              if($model->save(false)){
                return $this->redirect(['view', 'id' => $model->id]);
             }
          }
          $res = ['model' => $model];
          return ($request->isAjax) ? $this->renderAjax('reset', $res) : $this->render('reset', $res);


    }


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('del_user')){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
      } else {
        throw new ForbiddenHttpException('Insufficient privileges to access this area.');
      }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('User does not exist.');
        }
    }
    public function actionCreate()
    {
        $message = "wrong answer";
        echo "<script type='text/javascript'>alert('$message');</script>";

//        $request = Yii::$app->request;
//        $model = new User(['scenario' => 'create']);
//        $model->active = true;
//        echo "IN create";
//        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            $transaction = user::getDb()->beginTransaction();
//            try{
//                $model->saveUser();
//                $model->save(false);
//                $transaction->commit();
//                return $this->redirect(['view', 'id' => $model->id]);
//            } catch (\Exception $e){
//                $transaction->rollBack();
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                Yii::trace('Error: '.$error, 'CREATE USER ROLLBACK');
//            }
//        }
//        $res = ['model' => $model];
//        return  $request->isAjax ? $this->renderAjax('create', $res) : $this->render('create', $res);
    }

}

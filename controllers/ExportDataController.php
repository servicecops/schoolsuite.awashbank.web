<?php

namespace app\controllers;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreStudent;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

if (!Yii::$app->session->isActive){
    session_start();
}

class ExportDataController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    public function actionExportToPdf($model)
    {
        /*		$data = $model::getExportQuery();
                $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
                $query = $data['data'];
                $query = $query->limit(200)->all(Yii::$app->db);
                $type = 'Pdf';

                $html = $this->renderPartial($data['exportFile'],
                        ['query'=>$query,'type' => $type, 'subSessionIndex'=>$subSessionIndex
                    ]);
        //        try{
        //        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
        //        Logs::logEvent("Failed to create pdf", $error, null);
                    return Yii::$app->pdf->exportData($data['title'], $data['fileName'],$html);
        //        } catch(\Exception $e){
        //
        //            \Yii::$app->session->setFlash('actionFailed', $error);
        //            return \Yii::$app->runAction('/core-student/error');
        //        }*/

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial($data['exportFile'], ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => [$data['title']. '||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionExportExcel($model)
    {
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex'])  ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(20000)->all(Yii::$app->db);
        $type = 'Excel';

        $file = $this->renderPartial($data['exportFile'], [
            'query'=>$query, 'type'=>$type, 'subSessionIndex'=>$subSessionIndex
        ]);
        $fileName = $data['fileName'].'.xls';
        $options = ['mimeType'=>'application/vnd.ms-excel'];

        try{
            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        } catch(\Exception $e){
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to create Excel", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/core-student/error');
        }
    }

    public function actionExportToPdfOld($model)
    {
        $data = $model::getExportData();
        //print_r($data); exit;
        $type = 'Pdf';

        $html = $this->renderPartial($data['exportFile'],
            ['model'=>$data['data'],'type' => $type,
            ]);
        try{
            return Yii::$app->pdf->exportData($data['title'], $data['fileName'],$html);
        }catch(\Exception $e){
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to create Pdf", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/student/error');
        }
    }
    public function actionExportExcelOld($model)
    {
        $data = $model::getExportData();
        $type = 'Excel';

        $file = $this->renderPartial($data['exportFile'],
            ['model'=>$data['data'],
                'type'=>$type
            ]);
        $fileName = $data['fileName'].'.xls';
        $options = ['mimeType'=>'application/vnd.ms-excel'];

        try{
            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }catch(\Exception $e){
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to create Excel", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/student/error');
        }
    }
    public function actionExportPexcel($model)
    {
        $data = $model::getExportQuery();
        $objPHPExcel = new \PHPExcel();
        $sheet = 0;
        $styleArray = array(
            'font'  => array('bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'fill' => array(
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '3C389D'),

            ));
        $defaultStyle = array('font'=> array('name'=>'Arial', 'size'=>9),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT),
        );
        $objPHPExcel->getDefaultStyle()->applyFromArray($defaultStyle);

        // $type = 'Excel';
        ////////////////////////////////////////////////////////////////////////
        // Create Student Information worksheet
        $objPHPExcel->setActiveSheetIndex($sheet);
        $this->dataSheet($objPHPExcel, $styleArray, $data);
        $sheet++;

        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        $filename = $data['fileName'].'-'.date("d-m-Y-His").".xls";
        header('Content-Disposition: attachment;filename='.$filename .' ');
        header('Cache-Control: max-age=0');
        ob_end_clean();
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;

    }

    protected function dataSheet($objPHPExcel, $styleArray, $data)
    {
        $query = $data['data'];
        $labels = $data['labels'];
        $query = $query->limit(null)->all(Yii::$app->db);

        $HCol = 'A';
        $Hrow = 1;
        $Lcol ='';
        foreach($labels as $s){
            $s = ucfirst(str_replace('_', ' ', $s));
            $objPHPExcel->getActiveSheet()->setCellValue($HCol.''.$Hrow, $s);
            $Lcol = $HCol;
            $HCol++;
        }
        $row = 2;
        foreach($query as $t=>$sd)
        {
            $col='A';
            foreach($labels as $s){
                $objPHPExcel->getActiveSheet()->setCellValue($col.''.$row, $sd[$s]);
                $col++;
            }
            $row++;

        }
        $objPHPExcel->getActiveSheet()->getStyle('A1:'.$Lcol.'1')->applyFromArray($styleArray);
        for($cl = 'A'; $cl !== $HCol; $cl++)
            $objPHPExcel->getActiveSheet()->getColumnDimension($cl)->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->setTitle('Student Information');

    }

    public function actionExportPdf($model) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('pdf_test',['query'=>$query, 'subSessionIndex'=>$subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Privacy Policy - Krajee.com',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Krajee Privacy Policy||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetAuthor' => 'Kartik Visweswaran',
                'SetCreator' => 'Kartik Visweswaran',
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }
    public function actionExportPdfTrans($model) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportData();
        //print_r($data); exit;
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportTrasaction',['model'=>$data['data'],'type' => $type,
                ]
            ),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Privacy Policy - Krajee.com',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Krajee Privacy Policy||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetAuthor' => 'Kartik Visweswaran',
                'SetCreator' => 'Kartik Visweswaran',
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

}



?>

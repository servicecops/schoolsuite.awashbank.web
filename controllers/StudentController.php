<?php

namespace app\controllers;

use app\models\StudentLoginForm;
use app\modules\schoolcore\models\CoreStudent;
use Yii;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class StudentController extends Controller
{
    
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

            return $this->render('index');

    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'loginlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/student/index']);
        }

        $model = new StudentLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/student/index']);
        } else {
            $model->password = '';

            return $this->render('student_login', [
                'model' => $model,
            ]);
        }
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    protected function findModel($id)
    {
        if (($model = CoreStudent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['/student/login'])->send();
    }

    public function diffObjects($oldModel, $newModel){
        $newModel->date_of_birth = Date('Y-m-d', strtotime($oldModel->date_of_birth));
        $oldModel->allow_part_payments = (!$oldModel->allow_part_payments) ? 0 : 1;
        $oldModel->disability = (!$oldModel->disability) ? 0 : 1;
        $oldArray = ArrayHelper::toArray($oldModel, [], false);
        $newArray = ArrayHelper::toArray($newModel, [], false);
        $oldvalues = array_diff_assoc($oldArray, $newArray);
        $newvalues = array_diff_assoc($newArray, $oldArray);
        $edited = '';
        foreach($oldvalues as $ok=>$ov){
            $edited .=" |<b>".$ok."</b> from: ".$ov." to: ".$newvalues[$ok];
        }
        return $edited;
    }

}

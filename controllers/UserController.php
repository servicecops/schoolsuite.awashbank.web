<?php

namespace app\controllers;

use app\components\Helpers;
use app\components\ToWords;
use app\models\AuthItem;
use app\models\ImageBank;
use app\models\ResendVerificationEmailForm;
use app\models\SignupForm;
use app\models\User;
use app\models\UserSchoolAssociation;
use app\models\UserSearch;
use app\models\VerifyEmailForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\County;
use app\modules\workflow\controllers\WorkflowController;
use kartik\grid\EditableColumnAction;
use kartik\ipinfo\IpInfoAsset;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */

//    public function beforeAction($action) {
//
//
//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }
//
////something code right here if user valid
//
//        return true;
//
//
//
//    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','login'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        //Only schoolsuite admins and users with rw_user can access this area
        if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }

       $request = Yii::$app->request;
        $searchModel = new UserSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        // validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {
            if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin')) {
                throw new ForbiddenHttpException('Insufficient privileges to access this area.');
            }
            // instantiate your book model for saving
            $bookId = Yii::$app->request->post('editableKey');
            $model = User::findOne($bookId);

            // store a default json response as desired by editable
            $out = Json::encode(['output'=>'', 'message'=>'']);

            // fetch the first entry in posted data (there should only be one entry
            // anyway in this array for an editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            $posted = current($_POST['User']);
            $post = ['User' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one
                // EditableColumn in the grid view. We evaluate here a
                // check to see if buy_amount was posted for the Book model
//                if (isset($posted['email'])) {
//                    $output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
//                }

                // similarly you can check if the name attribute was posted as well
                if (isset($posted['email'])) {
                    $output = ''; // process as you need
                }
                $out = Json::encode(['output'=>$output, 'message'=>'']);
            }
            // return ajax json encoded response and exit
            echo $out;
            return;
        }
//        $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider];
//        return $request->isAjax ? $this->renderAjax('index', $res) : $this->render('index', $res);
        $dataProvider = $data['query'];

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
    }


    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionArchived()
    {
        //Only schoolsuite admins and users with rw_user can access this area
        if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }

        $request = Yii::$app->request;
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchArchived(Yii::$app->request->queryParams);


        // validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {
            if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin')) {
                throw new ForbiddenHttpException('Insufficient privileges to access this area.');
            }
            // instantiate your book model for saving
            $bookId = Yii::$app->request->post('editableKey');
            $model = User::findOne($bookId);

            // store a default json response as desired by editable
            $out = Json::encode(['output'=>'', 'message'=>'']);

            // fetch the first entry in posted data (there should only be one entry
            // anyway in this array for an editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            $posted = current($_POST['User']);
            $post = ['User' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one
                // EditableColumn in the grid view. We evaluate here a
                // check to see if buy_amount was posted for the Book model
//                if (isset($posted['email'])) {
//                    $output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
//                }

                // similarly you can check if the name attribute was posted as well
                if (isset($posted['email'])) {
                    $output = ''; // process as you need
                }
                $out = Json::encode(['output'=>$output, 'message'=>'']);
            }
            // return ajax json encoded response and exit
            echo $out;
            return;
        }
//        $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider];
//        return $request->isAjax ? $this->renderAjax('index', $res) : $this->render('index', $res);


        return ($request->isAjax) ? $this->renderAjax('index_archive', [  'dataProvider' => $dataProvider,
            'searchModel' => $searchModel]) :
            $this->render('index_archive', [  'dataProvider' => $dataProvider,
                'searchModel' => $searchModel]);
    }








//    public function actionIndex()
//    {
//        if(Yii::$app->user->can('rw_user')){
//            $request = Yii::$app->request;
//            $searchModel = new UserSearch();
//
//            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//            //Yii::trace($dataProvider);
//            $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider ];
//            return ($request->isAjax) ? $this->renderAjax('index', $res) : $this->render('index', $res);
//        } else {
//            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
//        }
//    }

    /**
     * Displays User model for logged in user
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionMyAccountProfile()
    {

        $selectedVal = [];
        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
            'model' => $this->findModel(Yii::$app->user->id)]) :
            $this->render('view', ['model' => $this->findModel(Yii::$app->user->id)]);
    }
    /**
     * Signs user up.
     *
     * @return mixed
     */


    public function actionCreateOriginal()
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        $request = Yii::$app->request;
        $model = new User(['scenario' => 'create']);
        $selectedVal = [];
        $schools = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $passwd = $data['User']['password'];

            if (Helpers::IsSchoolUser($model->user_level)) {
                $schs = $model->school_id;

                $ct = count($schs);
                if ($ct == 1) {
                    $model->school_id = $model->school_id[0];
                } else if ($ct> 1) {
                    $model->school_id = null;

                }
            } else {
                $model->school_id = null;
            }

            $model->username = strtolower($data['User']['username']);
            $model->setPassword($passwd);
            $model->password2 = $model->password;
            if(!$model->save(false)) {
                $res = ['model' => $model, 'selectedVal' => $selectedVal];
                Yii::$app->session->setFlash('error', 'Failed: Sorry, there was a problem creating user. Please try again or contact support for help.');

                return $request->isAjax ? $this->renderAjax('create', $res) : $this->render('create', $res);
            }
            $model->setUserSchools($schools, $model->id);
            $model->setAuthAssignment($model->user_level, strval($model->id));

            $transaction->commit();
            Logs::logEvent("New User Created(" . $model->id . "): " . $model->username, null, null);
            //Send user created email
            $fullname = $model->getFullname();
            $emailSubject = 'SchoolSuite Logins | ' . $fullname;

            $url = Url::to('/site/login', true);

            $emailText = "Hello $fullname\n
            
            Greetings from Schoolsuite!\n
            The following are your credentials for  Awash ESchool\n
            Username: $model->username \n
            Password: $passwd \n
            To login, go to $url\n\n
            Thank you for choosing  Awash ESchool.";
            ToWords::sendEmail($model->email,
                $emailSubject,
                $emailText);
            Yii::$app->session->setFlash('success', 'Successful, Credentials have been set to user\'s email address.');

            return $this->redirect(['/user/view', 'id' => $model->id]);

        }

        $model->school_id = !$model->school_id ? [] : $model->school_id;
        if (count($model->school_id) > 0) {
            foreach ($model->school_id as $v) {
                $selectedVal[$v] = CoreSchool::findOne($v)->school_name;
            }
        }
        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('create', $res) : $this->render('create', $res);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        //Only schoolpay admins and users with rw_user can access this area
        if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin') && !Yii::$app->user->can('sch_admin')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }
        $model = $this->findModel($id);
        Yii::trace($model);
        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
            'model' => $model]) :
            $this->render('view', ['model' => $model]);
    }
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        //Only schoolpay admins and users with rw_user can access this area
        $selectedVal = [];

        if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin') && !Yii::$app->user->can('sch_admin')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }
        $request = Yii::$app->request;

        //Only allow to update if the user can be updated by this user
        $usersICanAdminister = ArrayHelper::map(AuthItem::getUserLevelsICanAdminister(), 'name', 'description');
        $model = User::find()->where(['id' => $id])->andWhere(['in', 'user_level', array_keys($usersICanAdminister)])->one();
        if (!$model) {
            throw new NotFoundHttpException('Record not found');
        }
        if (is_numeric($model->school_id)) {
            $sch = $model->school_id;
            $model->school_id = [$sch];
            $selectedVal[$sch] = CoreSchool::findOne($sch)->school_name;
        }

        if (Helpers::IsSchoolUser($model->user_level)) {
            $val = [];
            $schs = new Query();
            $schs->select(['sa.school_id as id', 'sch.school_name as name'])
                ->from('user_school_association sa')
                ->innerJoin('core_school sch', 'sch.id=sa.school_id')
                ->andWhere(['sa.user_id' => $id]);
            if ($schs = $schs->all()) {
                foreach ($schs as $k => $v) {
                    Yii::trace($schs);
                    $val[] = $v['id'];
                    $selectedVal[$v['id']] = $v['name'];
                }
                $model->school_id = $val;
                Yii::trace($model->school_id);
            }
        }

        $model->scenario = 'update';
        $schools = null;
        $rep_schools = false;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            if (Helpers::IsSchoolUser($model->user_level)) {
                $schz = $model->school_id;

                $ct = count($schz);

                if ($ct == 1) {
                    $rep_schools = true;
                    $model->school_id = $model->school_id[0];
                } else if ($ct > 1) {
                    $schools = $model->school_id;
                    $model->school_id = $model->school_id[0];
                }
            } else {
                $model->school_id = null;
            }
            if (isset($data['User']['username'])) {
                $model->username = strtolower($data['User']['username']);
            }

            $model->updated_at = new Expression('NOW()');
            $model->save(false);
            if (isset($model->user_level)) {

                if ($schools) {
                    UserSchoolAssociation::delAssc($model->id);
                    UserSchoolAssociation::setUserSchools($schools, $model->id);
                } else if ($rep_schools) {
                    UserSchoolAssociation::delAssc($model->id);
                }

                $model->assign($model->user_level, strval($model->id));
            }

            Logs::logEvent("User Edited (" . $model->id . "): " . $model->username, null, null);
            return $this->redirect(['view', 'id' => $model->id]);
        }
        //setting initial values of school search
        $model->school_id = !$model->school_id ? [] : $model->school_id;
        $selectedVal = !$model->school_id ? [] : $selectedVal;
        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('update', $res) : $this->render('update', $res);

    }
    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }




    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLevelOptions(){
        $auth_items = AuthItem::getUserLevelsICanAdminister();
        if(!$auth_items){
            return "<option value='no_profile'>No User Roles</option>";
        }
        $options = "<option data-profile='no_profile' value=''>Type of User</option>";
        foreach ($auth_items as $k=>$v){
            $profile = 'admin';



            $query2 = (new Query())->select("child")
                ->from('auth_item_child')
                ->where(['parent'=>$v->name, 'child'=>'view_by_region'])
                ->limit(1);
            if($query2->one()) $profile = 'region_guy';
            $query = (new Query())->select("child")
                ->from('auth_item_child')
                ->where(['parent'=>$v->name, 'child'=>'own_sch'])
                ->limit(1);
            if($query->one()) $profile = 'school_guy';



            Yii::trace("is profile");
            Yii::trace($profile);

            $options .= "<option data-profile=".$profile." value=".$v->name.">".$v->description."</option>";
        }
        return $options;
    }

    public function actionChange()
    {

            $request = Yii::$app->request;
            $model = $this->findModel(Yii::$app->user->id);
            $model->scenario = 'change';

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $data = Yii::$app->request->post();
                $passwd = $data['User']['new_pass'];
             //   $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
                //For all users, apply password policy
                //Atleast 8 characters
                //Alpha numeric
                //Expires in 3 months

                    self::enforcePasswordPolicy($passwd);
                    $model->password_expiry_date = date('Y-m-d', strtotime('+3 months'));


                $model->updated_at = new Expression('NOW()');
                $model->setPassword($passwd);
                if ($model->save(false)) {
                    Logs::logEvent("User password reset(" . $model->id . "): " . $model->username, null, null);
                    Yii::$app->session->setFlash('success', 'Thank you for using schoolsuite. Your password has been successfully reset.');
                    return $this->redirect(['/site/index']);
                }
            } else {
                return ($request->isAjax) ? $this->renderAjax('change_pass', ['model' => $model]) :
                    $this->render('change_pass', ['model' => $model]);
            }


    }



    public function actionReset($id)
    {
        //Only schoolsuite admins and users with rw_user can access this area
        if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin') && !Yii::$app->user->can('sch_admin')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = 'reset';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $passwd = $data['User']['new_pass'];
            $model->locked = false;
            $model->password_expiry_date = date('Y-m-d');
            $model->setPassword($passwd);
            try {
                $model->save(false);
                Logs::logEvent("Passwd reset for (" . $model->id . ") By Admin: " . $model->username, null, null);
                //Send email to user on reset

                $url = Url::to('/site/login', true);
                //Send email to user on reset
                $fullname = $model->getFullname();
                $emailSubject = 'Awash ESchool password has been reset | ' . $fullname;
                $emailText = "Hello $fullname\n
            
            Greetings from Awash ESchool!\n
            Your Awash ESchool password for the username $model->username has been reset. \n
            New Password: $passwd \n
            To login, go to $url\n 
            If you did not request this reset, please contact Awash ESchool Support by email: contactcenter@awashbank.com
            Thank you for choosing Awash ESchool.";
                ToWords::sendEmail($model->email,
                    $emailSubject,
                    $emailText);
                Yii::$app->session->setFlash('success', 'Password has been reset successfully. New password sent to the user email!');

                return $this->redirect(['view', 'id' => $model->id]);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                \Yii::$app->session->setFlash('actionFailed', $error);
//                return \Yii::$app->runAction('/student/error');
            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('reset_pass', ['model' => $model]) :
                $this->render('reset_pass', ['model' => $model]);
        }

    }
    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }



    public function actionLists($id)
    {
        $posts = County::find()
            ->where(['district_id' => $id])
            ->all();

        if (!empty($posts)) {
            foreach($posts as $post) {
                echo "<option value='".$post->id."'>".$post->county_name."</option>";
            }
        } else {
            echo "<option>-</option>";
        }

    }
    public function actionUploadPicture($id)
    {


        $model = $this->findModel($id);

            $request = Yii::$app->request;

            $model->scenario = 'photo';
            $imageBank = null;
            $imageBank = $model->profile_pic ? ImageBank::find()->where(['id' => $model->profile_pic])->limit(1)->one() : new ImageBank();
            try {
                if ($imageBank->load(Yii::$app->request->post())) {
                    $data = $_POST['ImageBank']['image_base64'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $imageBank->image_base64 = $data;
                    $imageBank->description = 'User Photo Uploaded: ID - ' . $model->id;
                    if ($imageBank->save()) {
                        $model->profile_pic = $imageBank->id;
                        $model->save(false);
                        Logs::logEvent("Uploaded Student Photo: " . $model->username, null, $model->id);
                        $this->redirect(['/user/view', 'id' => $model->id]);
                    }
                }
                $res = ['imageBank' => $imageBank, 'model' => $model];
                return $this->render('signup', $res);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to upload user image: " . $model->fullname . "(" . $model->id . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/student/error');
            }
        }

    public function actionEmail()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())){
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('email')
                    ->from('user');
                $command = $query->createCommand();
                $emails= $command->queryAll(\PDO::FETCH_COLUMN);
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' .$file->baseName. '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }
                else{
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            }  catch (\Exception $e){
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: '.$error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to user: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('/user/error');
            }

        }
        return $this->render('contact',['model' => $model]);
    }

    public function actionSendEmail($id){
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())){
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('email')
                    ->from('user')
                    ->where(['id' => $id])->limit(1)->one();
                $command = $query->createCommand();
                $emails= $command->queryAll(\PDO::FETCH_COLUMN);
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' .$file->baseName. '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }
                else{
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            }  catch (\Exception $e){
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                $error = 'No email is attached to this user. Please add the email to the staff and try again';
                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: '.$error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to user: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('/user/error');
            }

        }
        return $this->render('contact',['model' => $model]);
    }


    public function actionArchiveUser($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_user', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $user_id = Yii::$app->user->identity->id;
            $user_name = Yii::$app->user->identity->username;
            $user_ip = Yii::$app->request->userIP;
            $model2 = new DynamicModel(['reason']);
            $model2->addRule(['reason'], 'required');
            if ($model2->load(Yii::$app->request->post())) {
                if (!$model->archived || $model2->validate()) {
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $connection->createCommand("select archive_user(
                         :userid, 
                         :reason, 
                         :webuser, 
                         :username, 
                         :userip)", [
                            ':userid' => $id,
                            ':reason' => $model2->reason,
                            ':webuser' => $user_id,
                            ':username' => $user_name,
                            ':userip' => $user_ip
                        ])->execute();
                        $transaction->commit();
//                        \Yii::$app->session->setFlash('Failed', "ghghsghsghs");
                        Logs::logEvent("Archived User : " . $model->fullname, null, $model->id);
                        \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $model->fullname . "  successfully archived");
                        return \Yii::$app->runAction('/user/view', ['id' => $id]);

                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                        Logs::logEvent("Error on Archive User: " . $model->fullname . "(" . $model->id . ")", $error, $model->id);
                        \Yii::$app->session->setFlash('actionFailed', $error);
                        return \Yii::$app->runAction('schoolcore/core-student/error');
                    }
                } else {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp Please provide reason for archiving OR user already archived");
                    return \Yii::$app->runAction('/user/view', ['id' => $id]);
                }
            }
            return ($request->isAjax) ? $this->renderAjax('archive_user', ['model2' => $model2, 'model' => $model]) : $this->render('archive_user', ['model2' => $model2, 'model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to change user');
        }
    }


    public function actionUnarchiveUser($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_user', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $user_id = Yii::$app->user->identity->id;
            $user_name = Yii::$app->user->identity->username;
            $user_ip = Yii::$app->request->userIP;
            $model2 = new DynamicModel(['reason']);
            $model2->addRule(['reason'], 'required');
            if ($model2->load(Yii::$app->request->post())) {
                if (!$model->archived || $model2->validate()) {
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $userDetails =  User::findOne($id);
                        $userDetails->archived = false;
                        $userDetails->locked = false;
                        $userDetails->unarchive_reason = $model2->reason;
                        $userDetails->save(false);

                        $transaction->commit();
//                        \Yii::$app->session->setFlash('Failed', "ghghsghsghs");
                        Logs::logEvent("UnArchived User : " . $model->fullname, null, $model->id);
                        \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $model->fullname . "  successfully archived");
                        return \Yii::$app->runAction('/user/view', ['id' => $id]);

                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                        Logs::logEvent("Error on UnArchive User: " . $model->fullname . "(" . $model->id . ")", $error, $model->id);
                        \Yii::$app->session->setFlash('actionFailed', $error);
                        return \Yii::$app->runAction('schoolcore/core-student/error');
                    }
                } else {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp Please provide reason for archiving OR user already archived");
                    return \Yii::$app->runAction('/user/view', ['id' => $id]);
                }
            }
            return ($request->isAjax) ? $this->renderAjax('unarchive_user', ['model2' => $model2, 'model' => $model]) : $this->render('unarchive_user', ['model2' => $model2, 'model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to change user');
        }
    }




    public function actionViewPrivacy() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('privacy'),
            'options' => [
                // any mpdf options you wish to set
            ],
            'cssFile' => \Yii::getAlias('@webroot/web') .'/css/privacy.css',
            'methods' => [
                'SetTitle' => 'Privacy Policy - Krajee.com',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Krajee Privacy Policy||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetAuthor' => 'Kartik Visweswaran',
                'SetCreator' => 'Kartik Visweswaran',
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
//         return $this->render('privacy');

    }

//create user with workflow
    public function actionAddAccount($id)
    {

        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        $request = Yii::$app->request;
        $model = new User(['scenario' => 'create']);
        $selectedVal = [];
        $schools = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            if(isset($data['User'])){
                $userData = $data;
            }
//
            $branch = null;
            if(Yii::$app->user->can('view_by_branch')){
                $branch = Yii::$app->user->identity->branch_id;
            }


            $workflowRequest = [
                'approval_required_permission' => json_encode(array('sch_admin','branch_chekers', 'super_admin', 'sys_admin')),
                'record_id' => $id,
                'request_params' => json_encode([

                    'userData' => $userData,

                ]),
                'request_notes' => 'Add/Update Bank details',
                'school_id' => $id,
                'workflow_record_type' => 'ADD_BANK_ACCOUNT',
                'branch_id'=>$branch
            ];


            $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
            Yii::$app->session->setFlash('workflowInfo', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
            return Yii::$app->runAction('/workflow/workflow/view', ['id' => $insert_id]);
////


        }

        $model->school_id = !$model->school_id ? [] : $model->school_id;
        if (count($model->school_id) > 0) {
            foreach ($model->school_id as $v) {
                $selectedVal[$v] = CoreSchool::findOne($v)->school_name;
            }
        }
        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }


//create user with workflow
    public function actionCreate()
    {

        $db = Yii::$app->db;
        $request = Yii::$app->request;
        $model = new User(['scenario' => 'create']);
        $selectedVal = [];
        $schools = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            if(isset($data['User'])){
                $userData = $data;
            }

            $branch = null;
            if(Yii::$app->user->can('view_by_branch')){

                Yii::trace( Yii::$app->user->identity->branch_id);
                $branch = Yii::$app->user->identity->branch_id;
            }

            $workflowRequest = [
                'approval_required_permission' => json_encode(array('branch_user','branch_chekers',  'super_admin', 'sys_admin')),
                'record_id' => 0,
                'request_params' => json_encode([
                    'userData' => $userData,
                ]),
                'request_notes' => 'Add User Details',
                'school_id' => null,
                'workflow_record_type' => 'ADD_USER',
                'branch_id'=> $branch
            ];



            $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
            Yii::$app->session->setFlash('workflowInfo', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
            return Yii::$app->runAction('/workflow/workflow/view', ['id' => $insert_id]);
////


        }

        $model->school_id = !$model->school_id ? [] : $model->school_id;
        if (count($model->school_id) > 0) {
            foreach ($model->school_id as $v) {
                $selectedVal[$v] = CoreSchool::findOne($v)->school_name;
            }
        }
        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }

    public static function enforcePasswordPolicy($passwd)
    {
        if (strlen($passwd) < 8)
            throw new Exception('Password should be at least 8 in length');

        if (!preg_match('/[A-Z]/', $passwd))
            throw new Exception('Password should have at least one upper case character');

        if (!preg_match('/[a-z]/', $passwd))
            throw new Exception('Password should have at least one lower case character');

        if (!preg_match('/[\d]/', $passwd))
            throw new Exception('Password should have at least one digit');
    }


    //allow user to add branch
    public function actionUpdateBranch($id)
    {

        $db = Yii::$app->db;
        $request = Yii::$app->request;
        $model = new User();
        $selectedVal = [];
        $schools = null;
        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            if(isset($data['User'])){
                $branchData = $data;
            }
            Yii::trace($branchData);
//            if(isset($model->school_id) && !empty($model->school_id)){
//                $schId = $model->school_id;
//            }else{
//                $schId = null;
//
//
            $userDetails = User::findOne([$id]);
            $userDetails->branch_id = $branchData['User']['branch_id'];
            $userDetails->save(false);


            return $this->redirect(['/user/view', 'id' => $id]);
        }


        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('update_branch', $res) : $this->render('update_branch', $res);

    }


    //allow user to update their region
    public function actionUpdateRegion($id)
    {

        $db = Yii::$app->db;
        $request = Yii::$app->request;
        $model = new User();
        $selectedVal = [];
        $schools = null;
        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            if(isset($data['User'])){
                $regData = $data;
            }
            Yii::trace($regData);

            $userDetails = User::findOne([$id]);
            $userDetails->region_id = $regData['User']['region_id'];
            $userDetails->save(false);


            return $this->redirect(['/user/view', 'id' => $id]);
        }


        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('update_region', $res) : $this->render('update_region', $res);

    }

}

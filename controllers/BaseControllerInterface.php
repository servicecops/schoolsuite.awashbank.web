<?php


namespace app\controllers;


interface BaseControllerInterface
{
    public function newModel();
    public function newSearchModel();
    public function createModelFormTitle();
}

<?php

namespace app\controllers;

use Yii;
use app\models\AuthItem;
use app\models\AuthItemChild;
use app\models\ItemSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use app\modules\logs\models\Logs;

/**
 * RolesController implements the CRUD actions for AuthItem model.
 */
class RolesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
//        r_role
        if (Yii::$app->user->can('r_role')) {
            $request = Yii::$app->request;
            $searchModel = new ItemSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return ($request->isAjax) ? $this->renderAjax('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider]);
        } else {
            throw new ForbiddenHttpException('No permissions to view this area.');
        }
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $req = Yii::$app->request;
        if (Yii::$app->user->can('super_admin')) {
            return ($req->isAjax) ? $this->renderAjax('view', ['model' => $this->findModel($id),
            ]) : $this->render('view', ['model' => $this->findModel($id)]);
        } else {
            throw new ForbiddenHttpException('No permissions to view this area.');
        }
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('super_admin')) {
            $request = Yii::$app->request;
            $model = new AuthItem();


            if ($model->load(Yii::$app->request->post())) {
                try {
                    $model->type = 1;
                    if (preg_match('/^\S.*\s.*\S$/', $model->name)) {
                        throw new Exception('Role name shouldnot contain spaces. Hint:you can seperate the name using _');
                    }
                    if ($model->save()) {
                        // return $this->redirect(['view', 'id' => $model->name]);
                        return Yii::$app->runAction('/roles/view', ['id' => $model->name]);
                    }
                } catch (\Exception $e) {
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                    \Yii::$app->session->setFlash('viewError', $error);
                    Logs::logEvent("Failed create new role : ", $error, null);
                }
            }

            return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) :
                $this->render('create', ['model' => $model]);
               

        } else {
            throw new ForbiddenHttpException('No permissions to update or create roles.');
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('super_admin')) {
            $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())) {
                $model->type = 1;
                if ($model->save()) {
                    // return $this->redirect(['view', 'id' => $model->name]);
                    return Yii::$app->runAction('/roles/view', ['id' => $model->name]);
                }
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new ForbiddenHttpException('No permissions to update or create roles.');
        }
    }

    public function actionPerms($name)
    {
//        rw_role
        if (Yii::$app->user->can('rw_role')) {
            $request = Yii::$app->request;
            $model = AuthItem::find()->where(['name' => $name])->limit(1)->one();


            $available = array();
            $assigned = array();
            $permissions = AuthItem::find()->where(['type' => 2])->all();
            //print_r( $permissions);
            foreach ($model->authItemChildren as $perms) {
                //print_r($perms); better to use Yii::trace($perms)

                $assigned[$perms->child] = $perms->child0->description;
            }

            foreach ($permissions as $perm) {
                if (!isset($assigned[$perm->name])) {
                    $available[$perm->name] = $perm->description;
                }
            }

            return ($request->isAjax) ? $this->renderAjax('perms', ['model' => $model,
                'available' => $available, 'assigned' => $assigned]) :
                $this->render('perms', ['model' => $model, 'available' => $available,
                    'assigned' => $assigned]);

        } else {
            throw new ForbiddenHttpException('No permissions to update or create roles.');
        }
    }

    public function actionAssign($name, $action)
    {
        $post = Yii::$app->request->post();
        $perms = $post['selected'];
        $error = [];
        if ($action == 'assign') {
            foreach ($perms as $k => $v) {
                try {
                    $item = new AuthItemChild();
                    $item->parent = $name;
                    $item->child = $v;
                    $item->save(false);
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }
            }
        } else {
            foreach ($perms as $k => $v) {
                try {
                    $item = AuthItemChild::find()->where(['parent' => $name, 'child' => $v])->one();
                    $item->delete();
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }

            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$this->actionPermSearch($name, 'available', ''),
            $this->actionPermSearch($name, 'assigned', ''),
            $error];
    }

    public function actionPermSearch($name, $target, $term = '')
    {
        $permissions = AuthItem::find()->where(['type' => 2])->all();
        $model = AuthItem::find()->where(['name' => $name])->limit(1)->one();
        $available = [];
        $assigned = [];

        foreach ($model->authItemChildren as $perms) {
            $assigned[$perms->child] = $perms->child0->description;
        }

        foreach ($permissions as $perm) {
            if (!isset($assigned[$perm->name])) {
                $available[$perm->name] = $perm->description;
            }
        }

        $result = [];
        $var = ${$target};
        if (!empty($term)) {
            foreach ($var as $i => $descr) {
                if (stripos($descr, $term) !== false) {
                    $result[$i] = $descr;
                }
            }
        } else {
            $result = $var;
        }

        return Html::renderSelectOptions('', $result);
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('del_role')) {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException('No permissions to delete roles.');
        }

    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

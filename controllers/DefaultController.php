<?php

namespace app\controllers;

use app\modules\logs\models\Logs;
use Yii;

use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransHistoryController implements the CRUD actions for SaccoMemberTransactionHistory model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    protected function classModels($id)
    {
        $classes = [
            'sacco-trans-history'=>[
                'search'=>'app\modules\sacco\models\SaccoMemberTransactionHistorySearch',
                'model'=>'app\modules\sacco\models\SaccoMemberTransactionHistory',
            ]
        ];

        return $classes[$id];
    }

    protected function generateSearchModel($id)
    {
        $model = $this->classModels($id)['search'];
        return new $model();
    }

    protected function generateClassModel($id)
    {
        $model = $this->classModels($id)['model'];
        return new $model();
    }

    /**
     * Lists all SaccoSchoolfeesLoan models.
     * @return mixed
     */
    public function actionIndex($mod)
    {
        $this->grantAccess($mod, 'index');
        $searchModel = $this->generateSearchModel($mod);
        $request = Yii::$app->request;
        $data = $searchModel->search(Yii::$app->request->queryParams);

        $template = "@app/views/common/the_index";
        $res = ['searchModel' => $searchModel,'data' => $data, 'mod'=>$mod];

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);



    }

    /**
     * Displays a single SaccoSchoolfeesLoan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($mod, $id)
    {
        $model = $this->findModel($mod, $id);
//        Yii::trace($model);
        $this->grantAccess($mod, 'view', $model);
        $request = Yii::$app->request;
        $template = $this->viewTemplate($mod);
        $res = ['model'=>$model, 'mod'=>$mod];

        return $request->isAjax ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }

    /**
     * Creates a new SaccoSchoolfeesLoan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate($mod)
    {
        try{
            $this->grantAccess($mod, 'create');
            $request = Yii::$app->request;
            $model = $this->generateClassModel($mod);
            if(method_exists($model,'setDefaultValues')) $model->setDefaultValues();
            $this->loadModel($mod, $model);
            $template = $this->formTemplate($mod, true);
            $res = ['model'=>$model, 'mod'=>$mod];
            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        } catch (\Exception $e) {
            Yii::trace($e);

            $model_error = $e->getMessage();
            Logs::logEvent("Failed : ", $model_error, null);
        }


    }

    /**
     * Updates an existing SaccoSchoolfeesLoan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($mod, $id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($mod, $id);
        $this->grantAccess($mod, 'update', $model);
        if(method_exists($model,'setDefaultValues')) $model->setDefaultValues();
        $this->loadModel($mod, $model);
        $template = $this->formTemplate($mod, false);;
        $res = ['model'=>$model, 'mod'=>$mod];
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    protected function loadModel($mod, $model)
    {
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if(method_exists($model, 'loadModel')
                && is_callable(array($model, 'loadModel'))){
                $model->loadModel();
            } else {
                $model->save(false);
                return $this->redirect(['view', 'mod'=>$mod,'id' => $model->id]);
            }
        }
    }

    protected function grantAccess($mod, $action,$model=null)
    {
        $mode = $this->classModels($mod);
        if(isset($mode['except'])){
            if(in_array($action, $mode['except'])){
                throw new NotFoundHttpException('Page Not Found');
            }
        }

        $right = (isset($mode['rights']) &&
            isset($mode['rights'][$action])) ? $mode['rights'][$action] : null;
        $perm = $right;
        $ruleParams = [];
        if(is_array($right)){
            $perm = isset($right[0]) ? $right[0] : null;
            $params = isset($right[1]) ? $right[1] : [];
            if(count($params) > 0){
                foreach ($params as $k=>$v){
                    $ruleParams[$k] = $model->{$v};
                }
            }
        }
        if($perm && !Yii::$app->user->can($perm, $ruleParams)) {
            throw new ForbiddenHttpException('No permissions to access this page. Contact admin');
        }
    }


    protected function viewTemplate($mod)
    {
        return "@app/views/common/view";
    }

    protected function formTemplate($mod, $isNewRecord)
    {
        return $isNewRecord ? "@app/views/common/create" : "@app/views/common/update";
    }

    /**
     * Deletes an existing SaccoSchoolfeesLoan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($mod, $id)
    {
        $model = $this->findModel($mod, $id);
        $this->grantAccess($mod, 'delete', $model);
        $model->delete();

        return $this->redirect(['index', 'mod'=>$mod]);
    }

    /**
     * Finds the SaccoSchoolfeesLoan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolfeesLoan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($mod, $id)
    {
        $class = $this->generateClassModel($mod);
        if (($model = $class::findOne($id)) !== null) {
//            Yii::trace($model);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\controllers;

use app\models\BaseModel;
use Yii;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

abstract class BaseController extends Controller implements BaseControllerInterface{

/*
* Finds the CoreStudent model based on its primary key value.
* If the model is not found, a 404 HTTP exception will be thrown.
* @param string $id
* @return CoreStudent the loaded model
* @throws NotFoundHttpException if the model cannot be found
*/

//    public function beforeAction($action) {
//
////
//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }
//
////something code right here if user valid
//
//        return true;
//
//
//
//    }
    protected function findModel($id)
    {
        $models = $this->newModel();

        if (($model = $models::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Displays a single CoreStudent model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
       // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $schoolData = $searchModel->viewModel($id);
        $modeler = $schoolData['models'];
        $attributes =$schoolData['attributes'];
//        return $this->render('@app/views/common/modal_view', [
//            'model' => $modeler,
//            'attributes'=>$attributes
//        ]);
        $res=['model' => $modeler,
            'attributes'=>$attributes];

        $template = "@app/views/common/modal_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }




    /**
     * Will render a generic form
     * @param BaseModel $formModel
     * @param array $formConfig
     * @return string
     */
    protected function modelForm($formModel, $formConfig = []) {
        //Load the form config from the model
        $request = Yii::$app->request;
        $formConfig['formConfig'] = $formModel->formAttributeConfig();
        $formConfig['formModel'] = $formModel;

        $template = "@app/views/common/generic_form";

        return ($request->isAjax) ? $this->renderAjax($template, $formConfig) : $this->render($template, $formConfig);

       // return $this->render('@app/views/common/generic_form', $formConfig);
    }

    /**
     * Will render a generic form
     * @param BaseModel $formModel
     * @param array $formConfig
     * @return string
     */
    protected function updateForm($formModel, $formConfig = []) {
        //Load the form config from the model
        $request =Yii::$app->request;
        $formConfig['formConfig'] = $formModel->updateAttributeConfig();
        $formConfig['formModel'] = $formModel;

       // return $this->render('@app/views/common/generic_form', $formConfig);
        $template = "@app/views/common/generic_form";

        return ($request->isAjax) ? $this->renderAjax($template, $formConfig) : $this->render($template, $formConfig);

    }

    protected function editForm($formModel, $formConfig = []) {
        //Load the form config from the model
        $request =Yii::$app->request;
        $formConfig['formConfig'] = $formModel->editAttributeConfig();
        $formConfig['formModel'] = $formModel;
        $template = "@app/views/common/generic_form";

        return ($request->isAjax) ? $this->renderAjax($template, $formConfig) : $this->render($template, $formConfig);

        //return $this->render('@app/views/common/generic_form', $formConfig);
    }
    protected function marksEditForm($formModel, $formConfig = []) {
        //Load the form config from the model
        $request = Yii::$app->request;
        $formConfig['formConfig'] = $formModel->editAttributeConfig();
        $formConfig['formModel'] = $formModel;
        $template = "@app/views/common/generic_form";

        return ($request->isAjax) ? $this->renderAjax($template, $formConfig) : $this->render($template, $formConfig);

       // return $this->renderAjax('@app/views/common/generic_form', $formConfig);
    }

    protected function searchForm($formModel, $formConfig = []) {
        //Load the form config from the model
        $request = Yii::$app->request;
        $formConfig['formConfig'] = $formModel->searchAttributeConfig();
        $formConfig['formModel'] = $formModel;
        $template = "@app/views/common/generic_form";

        return ($request->isAjax) ? $this->renderAjax($template, $formConfig) : $this->render($template, $formConfig);

       // return $this->render('@app/views/common/generic_form', $formConfig);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = $this->newModel();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $formConfig = [
            'model' => $model,
            'formTitle'=>$this->createModelFormTitle(),
        ];

        return  $this->modelForm($model, $formConfig );
    }

    /**
     * Creates a view.
          * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->searchIndex(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $title = $allData['title'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider,'title'=>$title, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
       // return $request->isAjax ? $this->renderAjax('card_summary', $res)


        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

       // return $this->render('@app/views/common/grid_view', $res);
    }

    /**
     * Updates an existing CoreGuardian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();

        $updateData = $searchModel->viewModel($id);
        $model = $updateData['models'];
        $attributes =$updateData['attributes'];

        if ($model->load(Yii::$app->request->post()) ) {
            $model->date_updated =date('Y-m-d H:i:s');
            $model->save();

            $template = "@app/views/common/modal_view";
            $res=['model' => $model,'searchModel'=>$searchModel,'attributes'=>$attributes,
                'formTitle'=>$this->createModelUpdateFormTitle()];
            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

//            return $this->render('@app/views/common/modal_view', [
//                'model' => $model,
//                'attributes' => $attributes
//            ]);
        }

        $formConfig = [
            'model' => $model,
            'formTitle'=>$this->createModelUpdateFormTitle(),
        ];

        return  $this->updateForm($model, $formConfig );

    }
    public function createModelUpdateFormTitle()
    {
        $str ="Update";
         $model = $this->newModel();
         return $str." ".$model->id;
    }
    public function createModelEditFormTitle()
    {
        $str ="Edit Student marks";
        $model = $this->newModel();
        return $str." ".$model->id;
    }


}

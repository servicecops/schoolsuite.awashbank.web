create or replace function svc_process_pending_penalties(in_batch_limit bigint) returns json
    security definer
    language plpgsql
as $$
declare

    /**
      This will apply pending penalties
      the in_batch_limit will determine how many records to process at a time
     */
    cursor_pending_penalties cursor (batch_size integer) for
        SELECT
            assoc.id AS associd
        FROM
            institution_fee_student_association assoc
                INNER JOIN core_student std ON
                    std.id = assoc.student_id
                INNER JOIN institution_fees_due fee ON
                    fee.id = assoc.fee_id
                INNER JOIN institution_fees_due_penalty penalty ON
                    penalty.id = assoc.penalty_id
        WHERE
                assoc.penalties_closed = FALSE
          AND assoc.next_penalty_apply_date <= current_date
          AND fee_outstanding_balance < 0
          AND assoc.has_penalty = TRUE
        LIMIT batch_size;

    penalty_record  record;


begin

    open cursor_pending_penalties(batch_size:=in_batch_limit);
    loop

        fetch cursor_pending_penalties into penalty_record;
        exit when not found;

        raise notice '% - Applying penalty %', now(), penalty_record.associd;
        perform apply_student_penalty(penalty_record.associd);
    end loop;
    close cursor_pending_penalties;

    -- Return details as json
    -- Parameter validations
    return jsonb_build_object('returnCode', 0, 'returnMessage', 'Penalties have been applied successfully');
end;
$$;

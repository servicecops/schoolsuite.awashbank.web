<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Maq
 * Date: 2/27/2018
 * Time: 12:21 PM
 */

namespace app\components;


use app\modules\synccore\models\AwashApiLoginControl;
use DateInterval;
use DateTime;
use Yii;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;

class AwashStatementHelper
{
    public static function getAccessToken() {
        $control = AwashApiLoginControl::findOne(['control_id'=>'AWASH']);

        if(!$control || !$control->token || !$control->expires) {
            if(!$control) {
                $control = new AwashApiLoginControl();

                $control->control_id = 'AWASH';
            }


            return self::statementLogin($control);
        }

        //Check if expired
        $now = new DateTime();
        if(new DateTime($control->expires) < $now) {
            return self::statementLogin($control);
        }

        return $control;
    }

    /**
     * @param AwashApiLoginControl $control
     * @return AwashApiLoginControl
     */
    public static function statementLogin($control)
    {
        try {
            $user = Yii::$app->params['awashStatementApiUser'];
            $password =  htmlspecialchars(Yii::$app->params['awashStatementApiPassword'], ENT_XML1); ;


            $payload = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"
        xmlns:sec=\"http://security.webservices.bankfusion.trapedza.com\">
           <soapenv:Header>
              <sec:ApplicationID>
                 <value>?</value>
              </sec:ApplicationID>
              <sec:OSUserID>
                 <value>?</value>
              </sec:OSUserID>
              <sec:passCode>
                 <value>?</value>
              </sec:passCode>
              <sec:userName>
                 <value>$user</value>
              </sec:userName>
              <sec:password>
                 <value>$password</value>
              </sec:password>
           </soapenv:Header>
           <soapenv:Body>
              <sec:request/>
           </soapenv:Body>
    </soapenv:Envelope>";

            $client = new Client([
                'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
            ]);
            $loginUrl = Yii::getAlias('@awashLoginUrl');

            Yii::trace('Sending ' . $payload);
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl($loginUrl)
                ->addHeaders(['content-type' => 'text/xml'])
                ->setContent($payload)
                ->setOptions(['sslVerifyPeer' => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false])
                ->send();

//        Yii::trace($response);
            $content = $response->getContent();

            if (!$response->isOk) {
                return ['returnCode' => 500, 'returnMessage' => 'Could not connect to Awash statements login service. Please try again later'];
            }


            $soapResponse = simplexml_load_string($content);

            $reponseBody = $soapResponse->children('http://schemas.xmlsoap.org/soap/envelope/')
                ->Body->children("http://security.webservices.bankfusion.trapedza.com")->response
                ->children()->return;

            $minutes_to_add = 5;
            $time = new DateTime();
            $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

            $stamp = $time->format('Y-m-d H:i:s');

            $control->date_updated = date('Y-m-d H:i:s');
            $control->token = $reponseBody;
            $control->expires = $stamp;
            $control->save(false);

            return $control;
        }
        catch (\Exception $e) {
            Yii::trace($e);
            throw new ForbiddenHttpException("CBS login failed while processing statement retrieval");
        }

    }

    public static function getStatement($account, $startDate, $endDate)
    {
        $token = self::getAccessToken();
        $from_date = date('Y-m-d', strtotime($startDate));
        $to_date = date('Y-m-d', strtotime($endDate));

        $statementUrl = Yii::getAlias('@awashStatementUrl');
        if (!$statementUrl) $statementUrl = 'http://10.10.102.7:9180/bfweb/retail/v1/accounts';

        $statementUrl = "$statementUrl/{$account}/transactions?startDate=$from_date&endDate=$to_date&limit=1000";

        Yii::trace($statementUrl);
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);

        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($statementUrl)
            ->addHeaders(['content-type' => 'application/json'])
            ->addHeaders(['Authorization' => $token->token])
            ->addHeaders(['X-Request-ID' => self::guidv4(null)])
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();


        $content = $response->getContent();
        Yii::trace($response->statusCode);
//        Yii::trace($content);
        if (!$response->isOk) {
            $error = 'Could not connect to Awash statements service. Please try again later';
            $contentObject = json_decode($content);
            if(isset($contentObject) && isset($contentObject->events) && count($contentObject->events)>0) {
                $error =  $contentObject->events[0]->message;
            } else if(isset($contentObject) && isset($contentObject->causes) && count($contentObject->causes)>0) {
                $error =  $contentObject->causes[0]->message;
            }
            return (object) ['returnCode' => 500, 'returnMessage' => $error];
        }


//        Yii::trace($content);
        return (object) ['returnCode' => 0, 'returnMessage' => 'Ok', 'returnObject'=>$content];
    }

    public static function guidv4($data = null) {
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}


}

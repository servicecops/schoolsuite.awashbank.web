<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\customGii\generators\model;
use yii\db\Schema;
use yii\helpers\Inflector;


/**
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ModelGenerator extends \yii\gii\generators\model\Generator
{
    public function generateModelFields()
    {
        $fields = $this->modelFields();
        return "'fields'=> [\n".$this->str(20).implode(",\n".$this->str(20), $fields).",\n".$this->str(16)."] \n" ;
    }
    private function str($n)
    {
        return str_repeat(' ', $n);
    }

    private function modelFields()
    {
        $columns = $this->getDbConnection()->getTableSchema($this->tableName)->columns;
        $fields = [];
        foreach ($columns as $column) {
            $label = Inflector::camel2words($column->name);
            switch ($column->type) {
                case Schema::TYPE_DATE:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $fields[] = "'{$column->name}' => ['type'=>'date', 'label'=>'$label']";
                    break;
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $fields[] = "'{$column->name}' => ['type'=>'number', 'label'=>'$label']";
                    break;
                default:
                    $fields[] = "'{$column->name}' => ['label'=>'$label']";
                    break;
            }
        }
        return $fields;
    }

}

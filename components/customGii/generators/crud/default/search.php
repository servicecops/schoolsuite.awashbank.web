<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\db\Schema;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();
$searchQuery = $generator->generateSearchQuery();
$cols = implode("', '",  $generator->getTableSchema()->columnNames);
$tbl_headers = $generator->generateTHeaders();


echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;
if (!Yii::$app->session->isActive) session_start();

/**
 * <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?>

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        <?= $searchQuery ?>
        <?= implode("\n        ", $searchConditions) ?>

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                '<?= $cols;?>'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('<?= yii\helpers\Inflector::camel2words($modelClass);?>', $query, $this->getVisibleCols(),  '<?= $generator->getTableSchema()->name; ?>');
        return ['query'=>$query->all(Yii::$app->db2), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'<?= yii\helpers\Inflector::camel2words($modelClass);?>', 'search_col_w'=>[2, 7, 3]];
    }


    public function getVisibleCols()
    {
    <?= $tbl_headers; ?>
    }

    public function getSearchCols()
    {
    <?= $generator->generateSearchCols() ?>
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components\customGii\generators\crud;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\Schema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Generates CRUD
 *
 * @property array $columnNames Model column names. This property is read-only.
 * @property string $controllerID The controller ID (without the module ID prefix). This property is
 * read-only.
 * @property array $searchAttributes Searchable attributes. This property is read-only.
 * @property boolean|\yii\db\TableSchema $tableSchema This property is read-only.
 * @property string $viewPath The controller view path. This property is read-only.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CrudGenerator extends \yii\gii\generators\crud\Generator
{
    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->modelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                default:
                    $likeConditions[] = "->andFilterWhere(['ilike', '{$column}', \$this->{$column}])";
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode("\n" . str_repeat(' ', 12), $hashConditions)
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode("\n" . str_repeat(' ', 12), $likeConditions) . ";\n";
        }

        return $conditions;
    }

    /**
     * Generates search Query
     * @return string
     */
    public function generateSearchQuery(){
        $tbl_name = $this->getTableSchema()->name;
        $tbl_cols = $this->getColumnNames();
        $cols = implode("', '",  $tbl_cols);
        $q = "\n".str_repeat(' ', 8)."\$query->select(['$cols'])\n";
        $q .=str_repeat(' ', 12)."->from('$tbl_name');\n";

        return $q;
    }

    public function generateTHeaders(){
        $headers =$this->getTableColumns();
        $hString = $this->str(12).implode(",\n".$this->str(12), $headers).",\n";
        $hString.= $this->str(12)."'actions' => [\n{$this->str(16)}'type'=>'action',\n";
        $hString.= $this->str(16)."'links' => [\n{$this->str(20)}['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],\n";
        $hString.= $this->str(20)."['url'=>'update', 'title'=>'Update', 'icon'=>'glyphicon glyphicon-edit']\n";
        $hString.= "{$this->str(16)}]\n{$this->str(12)}]\n";
        return $this->str(4)."return [\n".$hString.$this->str(8)."]; \n" ;
    }

    public function generateSearchCols()
    {
        $headers = $this->getTableColumns();
        return $this->str(4)."return [\n".$this->str(12).implode(",\n".$this->str(12), $headers).",\n".$this->str(8)."]; \n" ;
    }

    private function str($n)
    {
        return str_repeat(' ', $n);
    }

    private function getTableColumns()
    {
        $columns = $this->getTableSchema()->columns;
        $headers = [];
        foreach ($columns as $column) {
            $label = Inflector::camel2words($column->name);
            switch ($column->type) {
                case Schema::TYPE_DATE:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $headers[] = "'{$column->name}' => ['type'=>'date', 'label'=>'$label']";
                    break;
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $headers[] = "'{$column->name}' => ['type'=>'number', 'label'=>'$label']";
                    break;
                default:
                    $headers[] = "'{$column->name}' => ['label'=>'$label']";
                    break;
            }
        }
        return $headers;
    }

}

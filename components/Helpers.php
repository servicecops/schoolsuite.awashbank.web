<?php
namespace app\components;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class Helpers
{
    public static function ClassArray($id=null){
        $results = [];
        if($id){
            $classes = (new Query())->select(['id', 'class_description'])
                ->from('core_school_class')
                ->where(['school_id' => $id])
                ->andWhere(['<>', 'class_code', '__ARCHIVE__'])
                ->orderBy('class_code')
                ->all(Yii::$app->db);
            return ArrayHelper::map($classes, 'id', 'class_description');
//            $results = array_merge( array('ALL'=>'Send To All'), ArrayHelper::map($classes, 'id', 'class_description'));
        }
        return $results;
    }

    public static function GroupArray($id=null){
        $results = [];
        if($id){
            $groups = (new Query())->select(['id', 'group_name'])
                ->from('student_group_information')
                ->where(['school_id' => $id])
                ->orderBy('id DESC')
                ->all(Yii::$app->db);
//            $results = ArrayHelper::map($groups, 'id', 'group_name');
            $results = array_merge( array('ALL'=>'Send To All'), ArrayHelper::map($groups, 'id', 'group_name'));

        }
        return $results;
    }

    public static function IsSchoolUser($role){
//        $auth = Yii::$app->authManager;
//        return $auth->hasChild($role, 'own_sch');
        return (new Query)
            ->from('auth_item_child')
            ->where(['parent'=>$role, 'child'=>'own_sch'])
            ->one() !== false;
    }
    public static function IsRegionalUser($role){
//        $auth = Yii::$app->authManager;
//        return $auth->hasChild($role, 'own_sch');
        return (new Query)
            ->from('auth_item_child')
            ->where(['parent'=>$role, 'child'=>'view_by_region'])
            ->one() !== false;
    }
    public static function IsBranchUser($role){
//        $auth = Yii::$app->authManager;
//        return $auth->hasChild($role, 'own_sch');
        return (new Query)
            ->from('auth_item_child')
            ->where(['parent'=>$role, 'child'=>'view_by_branch'])
            ->one() !== false;
    }

    public static function ValidatePhone(){
        return '';
    }

    public static function is($role)
    {
        //    instead of returning 404 page, return false
        //    with false, the whole link will be hidden for the current user.
        //    Don't replace the return with the short-hand, false is surely required.
        if (\Yii::$app->user->isGuest) {
            return false;
        }

        return \Yii::$app->user->can($role) ? true : false;
    }
}

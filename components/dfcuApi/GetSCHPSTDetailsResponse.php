<?php

class GetSCHPSTDetailsResponse
{

    /**
     * @var Accountstatements[] $GetSCHPSTDetailsResult
     * @access public
     */
    public $GetSCHPSTDetailsResult = null;

    /**
     * @param Accountstatements[] $GetSCHPSTDetailsResult
     * @access public
     */
    public function __construct($GetSCHPSTDetailsResult)
    {
      $this->GetSCHPSTDetailsResult = $GetSCHPSTDetailsResult;
    }

}

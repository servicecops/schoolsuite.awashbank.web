<?php

class UmemeCustomerProfile
{

    /**
     * @var string $custRefNo
     * @access public
     */
    public $custRefNo = null;

    /**
     * @var string $custRefName
     * @access public
     */
    public $custRefName = null;

    /**
     * @var string $custType
     * @access public
     */
    public $custType = null;

    /**
     * @var int $outstandingBal
     * @access public
     */
    public $outstandingBal = null;

    /**
     * @var string $statuscode
     * @access public
     */
    public $statuscode = null;

    /**
     * @var string $statusdescription
     * @access public
     */
    public $statusdescription = null;

    /**
     * @var string $custErrorResult
     * @access public
     */
    public $custErrorResult = null;

    /**
     * @param string $custRefNo
     * @param string $custRefName
     * @param string $custType
     * @param int $outstandingBal
     * @param string $statuscode
     * @param string $statusdescription
     * @param string $custErrorResult
     * @access public
     */
    public function __construct($custRefNo, $custRefName, $custType, $outstandingBal, $statuscode, $statusdescription, $custErrorResult)
    {
      $this->custRefNo = $custRefNo;
      $this->custRefName = $custRefName;
      $this->custType = $custType;
      $this->outstandingBal = $outstandingBal;
      $this->statuscode = $statuscode;
      $this->statusdescription = $statusdescription;
      $this->custErrorResult = $custErrorResult;
    }

}

<?php

class TxnResultz
{

    /**
     * @var Requeststatus $requeststatus
     * @access public
     */
    public $requeststatus = null;

    /**
     * @var UmemePostTxnResult $umemeposttxnresult
     * @access public
     */
    public $umemeposttxnresult = null;

    /**
     * @var UmemePrepaidTxnResult $umemeprepaidtxnresult
     * @access public
     */
    public $umemeprepaidtxnresult = null;

    /**
     * @var UEDCLPrepaidTxnResult $uedclprepaidtxnresult
     * @access public
     */
    public $uedclprepaidtxnresult = null;

    /**
     * @var NWSCPostTxnResult $nwscposttxnresult
     * @access public
     */
    public $nwscposttxnresult = null;

    /**
     * @var URAPOSPostTxnResult $uraposposttxnresult
     * @access public
     */
    public $uraposposttxnresult = null;

    /**
     * @var SchoolpaystudentTranResponsedetails $schoolpaystudentranResponsedetails
     * @access public
     */
    public $schoolpaystudentranResponsedetails = null;

    /**
     * @param Requeststatus $requeststatus
     * @param UmemePostTxnResult $umemeposttxnresult
     * @param UmemePrepaidTxnResult $umemeprepaidtxnresult
     * @param UEDCLPrepaidTxnResult $uedclprepaidtxnresult
     * @param NWSCPostTxnResult $nwscposttxnresult
     * @param URAPOSPostTxnResult $uraposposttxnresult
     * @param SchoolpaystudentTranResponsedetails $schoolpaystudentranResponsedetails
     * @access public
     */
    public function __construct($requeststatus, $umemeposttxnresult, $umemeprepaidtxnresult, $uedclprepaidtxnresult, $nwscposttxnresult, $uraposposttxnresult, $schoolpaystudentranResponsedetails)
    {
      $this->requeststatus = $requeststatus;
      $this->umemeposttxnresult = $umemeposttxnresult;
      $this->umemeprepaidtxnresult = $umemeprepaidtxnresult;
      $this->uedclprepaidtxnresult = $uedclprepaidtxnresult;
      $this->nwscposttxnresult = $nwscposttxnresult;
      $this->uraposposttxnresult = $uraposposttxnresult;
      $this->schoolpaystudentranResponsedetails = $schoolpaystudentranResponsedetails;
    }

}

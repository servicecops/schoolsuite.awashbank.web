<?php

class AgencyBankDepositPostTxnResult
{

    /**
     * @var string $acct_id
     * @access public
     */
    public $acct_id = null;

    /**
     * @var string $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $StatusDesc
     * @access public
     */
    public $StatusDesc = null;

    /**
     * @var string $transactionid
     * @access public
     */
    public $transactionid = null;

    /**
     * @param string $acct_id
     * @param string $StatusCode
     * @param string $StatusDesc
     * @param string $transactionid
     * @access public
     */
    public function __construct($acct_id, $StatusCode, $StatusDesc, $transactionid)
    {
      $this->acct_id = $acct_id;
      $this->StatusCode = $StatusCode;
      $this->StatusDesc = $StatusDesc;
      $this->transactionid = $transactionid;
    }

}

<?php

class ValidateCustomerDetails
{

    /**
     * @var ValidcustReqz[] $initialdetails
     * @access public
     */
    public $initialdetails = null;

    /**
     * @param ValidcustReqz[] $initialdetails
     * @access public
     */
    public function __construct($initialdetails)
    {
      $this->initialdetails = $initialdetails;
    }

}

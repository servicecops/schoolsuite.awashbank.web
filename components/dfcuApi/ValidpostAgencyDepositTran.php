<?php

class ValidpostAgencyDepositTran
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $stracct_id
     * @access public
     */
    public $stracct_id = null;

    /**
     * @var string $strCustPhone
     * @access public
     */
    public $strCustPhone = null;

    /**
     * @var string $dtTxnDate
     * @access public
     */
    public $dtTxnDate = null;

    /**
     * @var string $dblAmount
     * @access public
     */
    public $dblAmount = null;

    /**
     * @var string $strnarration
     * @access public
     */
    public $strnarration = null;

    /**
     * @var string $strTxnRef
     * @access public
     */
    public $strTxnRef = null;

    /**
     * @var string $strAgentID
     * @access public
     */
    public $strAgentID = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $stracct_id
     * @param string $strCustPhone
     * @param string $dtTxnDate
     * @param string $dblAmount
     * @param string $strnarration
     * @param string $strTxnRef
     * @param string $strAgentID
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $stracct_id, $strCustPhone, $dtTxnDate, $dblAmount, $strnarration, $strTxnRef, $strAgentID, $strVendorCode, $strVendorPassword, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->stracct_id = $stracct_id;
      $this->strCustPhone = $strCustPhone;
      $this->dtTxnDate = $dtTxnDate;
      $this->dblAmount = $dblAmount;
      $this->strnarration = $strnarration;
      $this->strTxnRef = $strTxnRef;
      $this->strAgentID = $strAgentID;
      $this->strVendorCode = $strVendorCode;
      $this->strVendorPassword = $strVendorPassword;
      $this->strsignature = $strsignature;
    }

}

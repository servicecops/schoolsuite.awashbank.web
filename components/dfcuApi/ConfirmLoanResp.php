<?php

class ConfirmLoanResp
{

    /**
     * @var int $success
     * @access public
     */
    public $success = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var string $TransactionID
     * @access public
     */
    public $TransactionID = null;

    /**
     * @var string $Token
     * @access public
     */
    public $Token = null;

    /**
     * @var string $Units
     * @access public
     */
    public $Units = null;

    /**
     * @var string $PaymentReference
     * @access public
     */
    public $PaymentReference = null;

    /**
     * @param int $success
     * @param string $message
     * @param string $TransactionID
     * @param string $Token
     * @param string $Units
     * @param string $PaymentReference
     * @access public
     */
    public function __construct($success, $message, $TransactionID, $Token, $Units, $PaymentReference)
    {
      $this->success = $success;
      $this->message = $message;
      $this->TransactionID = $TransactionID;
      $this->Token = $Token;
      $this->Units = $Units;
      $this->PaymentReference = $PaymentReference;
    }

}

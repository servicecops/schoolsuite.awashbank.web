<?php

class PostAgencyCustTransactionResponse
{

    /**
     * @var TxnResultzAgencyBank[] $PostAgencyCustTransactionResult
     * @access public
     */
    public $PostAgencyCustTransactionResult = null;

    /**
     * @param TxnResultzAgencyBank[] $PostAgencyCustTransactionResult
     * @access public
     */
    public function __construct($PostAgencyCustTransactionResult)
    {
      $this->PostAgencyCustTransactionResult = $PostAgencyCustTransactionResult;
    }

}

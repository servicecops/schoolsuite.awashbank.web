<?php

class URAPRNDetail
{

    /**
     * @var string $Prn
     * @access public
     */
    public $Prn = null;

    /**
     * @var string $Tin
     * @access public
     */
    public $Tin = null;

    /**
     * @var string $TaxpayerName
     * @access public
     */
    public $TaxpayerName = null;

    /**
     * @var string $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var string $PaymentRegDt
     * @access public
     */
    public $PaymentRegDt = null;

    /**
     * @var string $ExpiryDt
     * @access public
     */
    public $ExpiryDt = null;

    /**
     * @var string $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $StatusDesc
     * @access public
     */
    public $StatusDesc = null;

    /**
     * @var string $errDesc
     * @access public
     */
    public $errDesc = null;

    /**
     * @param string $Prn
     * @param string $Tin
     * @param string $TaxpayerName
     * @param string $Amount
     * @param string $PaymentRegDt
     * @param string $ExpiryDt
     * @param string $StatusCode
     * @param string $StatusDesc
     * @param string $errDesc
     * @access public
     */
    public function __construct($Prn, $Tin, $TaxpayerName, $Amount, $PaymentRegDt, $ExpiryDt, $StatusCode, $StatusDesc, $errDesc)
    {
      $this->Prn = $Prn;
      $this->Tin = $Tin;
      $this->TaxpayerName = $TaxpayerName;
      $this->Amount = $Amount;
      $this->PaymentRegDt = $PaymentRegDt;
      $this->ExpiryDt = $ExpiryDt;
      $this->StatusCode = $StatusCode;
      $this->StatusDesc = $StatusDesc;
      $this->errDesc = $errDesc;
    }

}

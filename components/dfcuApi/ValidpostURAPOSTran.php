<?php

class ValidpostURAPOSTran
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strtransactionid
     * @access public
     */
    public $strtransactionid = null;

    /**
     * @var string $strtaxpayername
     * @access public
     */
    public $strtaxpayername = null;

    /**
     * @var string $strprn
     * @access public
     */
    public $strprn = null;

    /**
     * @var float $dblAmount
     * @access public
     */
    public $dblAmount = null;

    /**
     * @var string $strstatus
     * @access public
     */
    public $strstatus = null;

    /**
     * @var string $strCustPhone
     * @access public
     */
    public $strCustPhone = null;

    /**
     * @var string $strpaymentdate
     * @access public
     */
    public $strpaymentdate = null;

    /**
     * @var string $strtin
     * @access public
     */
    public $strtin = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strtransactionid
     * @param string $strtaxpayername
     * @param string $strprn
     * @param float $dblAmount
     * @param string $strstatus
     * @param string $strCustPhone
     * @param string $strpaymentdate
     * @param string $strtin
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strtransactionid, $strtaxpayername, $strprn, $dblAmount, $strstatus, $strCustPhone, $strpaymentdate, $strtin, $strVendorCode, $strVendorPassword, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strtransactionid = $strtransactionid;
      $this->strtaxpayername = $strtaxpayername;
      $this->strprn = $strprn;
      $this->dblAmount = $dblAmount;
      $this->strstatus = $strstatus;
      $this->strCustPhone = $strCustPhone;
      $this->strpaymentdate = $strpaymentdate;
      $this->strtin = $strtin;
      $this->strVendorCode = $strVendorCode;
      $this->strVendorPassword = $strVendorPassword;
      $this->strsignature = $strsignature;
    }

}

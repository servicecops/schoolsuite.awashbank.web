<?php

class RepayLoanResp
{

    /**
     * @var int $success
     * @access public
     */
    public $success = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var string $TransactionID
     * @access public
     */
    public $TransactionID = null;

    /**
     * @param int $success
     * @param string $message
     * @param string $TransactionID
     * @access public
     */
    public function __construct($success, $message, $TransactionID)
    {
      $this->success = $success;
      $this->message = $message;
      $this->TransactionID = $TransactionID;
    }

}

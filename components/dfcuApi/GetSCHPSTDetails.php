<?php

namespace app\components\dfcuApi;

class GetSCHPSTDetails
{

    /**
     * @var ValidSCHPST $posttrandetails
     * @access public
     */
    public $posttrandetails = null;

    /**
     * @param ValidSCHPST $posttrandetails
     * @access public
     */
    public function __construct($posttrandetails)
    {
      $this->posttrandetails = $posttrandetails;
    }

}

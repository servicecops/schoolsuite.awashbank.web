<?php

class ValidGenericPOSPRN
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strtranid
     * @access public
     */
    public $strtranid = null;

    /**
     * @var string $strpayername
     * @access public
     */
    public $strpayername = null;

    /**
     * @var string $strtinno
     * @access public
     */
    public $strtinno = null;

    /**
     * @var string $strpayamount
     * @access public
     */
    public $strpayamount = null;

    /**
     * @var string $strpaydate
     * @access public
     */
    public $strpaydate = null;

    /**
     * @var string $strtaxtype
     * @access public
     */
    public $strtaxtype = null;

    /**
     * @var string $strphone
     * @access public
     */
    public $strphone = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strtranid
     * @param string $strpayername
     * @param string $strtinno
     * @param string $strpayamount
     * @param string $strpaydate
     * @param string $strtaxtype
     * @param string $strphone
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strtranid, $strpayername, $strtinno, $strpayamount, $strpaydate, $strtaxtype, $strphone, $strVendorCode, $strVendorPassword, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strtranid = $strtranid;
      $this->strpayername = $strpayername;
      $this->strtinno = $strtinno;
      $this->strpayamount = $strpayamount;
      $this->strpaydate = $strpaydate;
      $this->strtaxtype = $strtaxtype;
      $this->strphone = $strphone;
      $this->strVendorCode = $strVendorCode;
      $this->strVendorPassword = $strVendorPassword;
      $this->strsignature = $strsignature;
    }

}

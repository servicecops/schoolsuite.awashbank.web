<?php

class UEDCLPrepaidTxnResult
{

    /**
     * @var string $ReceiptNumber
     * @access public
     */
    public $ReceiptNumber = null;

    /**
     * @var string $Token
     * @access public
     */
    public $Token = null;

    /**
     * @var string $TransactionId
     * @access public
     */
    public $TransactionId = null;

    /**
     * @var string $TenderAmount
     * @access public
     */
    public $TenderAmount = null;

    /**
     * @var string $CreditAmount
     * @access public
     */
    public $CreditAmount = null;

    /**
     * @var string $ServiceCharge
     * @access public
     */
    public $ServiceCharge = null;

    /**
     * @var string $Tax
     * @access public
     */
    public $Tax = null;

    /**
     * @var string $Units
     * @access public
     */
    public $Units = null;

    /**
     * @var string $AccountRef
     * @access public
     */
    public $AccountRef = null;

    /**
     * @var string $priceperunit
     * @access public
     */
    public $priceperunit = null;

    /**
     * @var string $meterno
     * @access public
     */
    public $meterno = null;

    /**
     * @var string $metertype
     * @access public
     */
    public $metertype = null;

    /**
     * @var string $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $statusDesc
     * @access public
     */
    public $statusDesc = null;

    /**
     * @var string $dfcutranref
     * @access public
     */
    public $dfcutranref = null;

    /**
     * @param string $ReceiptNumber
     * @param string $Token
     * @param string $TransactionId
     * @param string $TenderAmount
     * @param string $CreditAmount
     * @param string $ServiceCharge
     * @param string $Tax
     * @param string $Units
     * @param string $AccountRef
     * @param string $priceperunit
     * @param string $meterno
     * @param string $metertype
     * @param string $StatusCode
     * @param string $statusDesc
     * @param string $dfcutranref
     * @access public
     */
    public function __construct($ReceiptNumber, $Token, $TransactionId, $TenderAmount, $CreditAmount, $ServiceCharge, $Tax, $Units, $AccountRef, $priceperunit, $meterno, $metertype, $StatusCode, $statusDesc, $dfcutranref)
    {
      $this->ReceiptNumber = $ReceiptNumber;
      $this->Token = $Token;
      $this->TransactionId = $TransactionId;
      $this->TenderAmount = $TenderAmount;
      $this->CreditAmount = $CreditAmount;
      $this->ServiceCharge = $ServiceCharge;
      $this->Tax = $Tax;
      $this->Units = $Units;
      $this->AccountRef = $AccountRef;
      $this->priceperunit = $priceperunit;
      $this->meterno = $meterno;
      $this->metertype = $metertype;
      $this->StatusCode = $StatusCode;
      $this->statusDesc = $statusDesc;
      $this->dfcutranref = $dfcutranref;
    }

}

<?php

class CustRegRespx
{

    /**
     * @var int $success
     * @access public
     */
    public $success = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @param int $success
     * @param string $message
     * @access public
     */
    public function __construct($success, $message)
    {
      $this->success = $success;
      $this->message = $message;
    }

}

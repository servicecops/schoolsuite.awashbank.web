<?php

class PostCustomerTransactionResponse
{

    /**
     * @var TxnResultz[] $PostCustomerTransactionResult
     * @access public
     */
    public $PostCustomerTransactionResult = null;

    /**
     * @param TxnResultz[] $PostCustomerTransactionResult
     * @access public
     */
    public function __construct($PostCustomerTransactionResult)
    {
      $this->PostCustomerTransactionResult = $PostCustomerTransactionResult;
    }

}

<?php

class PostAgencyCustTransaction
{

    /**
     * @var ValidpostAgencyBanktranz[] $posttrandetails
     * @access public
     */
    public $posttrandetails = null;

    /**
     * @param ValidpostAgencyBanktranz[] $posttrandetails
     * @access public
     */
    public function __construct($posttrandetails)
    {
      $this->posttrandetails = $posttrandetails;
    }

}

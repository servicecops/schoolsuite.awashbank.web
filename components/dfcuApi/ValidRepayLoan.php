<?php

class ValidRepayLoan
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strmobileno
     * @access public
     */
    public $strmobileno = null;

    /**
     * @var int $stramount
     * @access public
     */
    public $stramount = null;

    /**
     * @var string $strtransactionref
     * @access public
     */
    public $strtransactionref = null;

    /**
     * @var string $strcustacc
     * @access public
     */
    public $strcustacc = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strusr
     * @param string $strpwd
     * @param string $strmobileno
     * @param int $stramount
     * @param string $strtransactionref
     * @param string $strcustacc
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strusr, $strpwd, $strmobileno, $stramount, $strtransactionref, $strcustacc, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strmobileno = $strmobileno;
      $this->stramount = $stramount;
      $this->strtransactionref = $strtransactionref;
      $this->strcustacc = $strcustacc;
      $this->strsignature = $strsignature;
    }

}

<?php

class URAPOSPostTxnResult
{

    /**
     * @var string $errorcode
     * @access public
     */
    public $errorcode = null;

    /**
     * @var string $errormessage
     * @access public
     */
    public $errormessage = null;

    /**
     * @param string $errorcode
     * @param string $errormessage
     * @access public
     */
    public function __construct($errorcode, $errormessage)
    {
      $this->errorcode = $errorcode;
      $this->errormessage = $errormessage;
    }

}

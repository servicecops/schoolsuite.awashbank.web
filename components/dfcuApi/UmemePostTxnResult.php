<?php

class UmemePostTxnResult
{

    /**
     * @var string $successfulField
     * @access public
     */
    public $successfulField = null;

    /**
     * @var string $postErrorField
     * @access public
     */
    public $postErrorField = null;

    /**
     * @var string $successfulReceiptNumber
     * @access public
     */
    public $successfulReceiptNumber = null;

    /**
     * @var string $dfcutranref
     * @access public
     */
    public $dfcutranref = null;

    /**
     * @param string $successfulField
     * @param string $postErrorField
     * @param string $successfulReceiptNumber
     * @param string $dfcutranref
     * @access public
     */
    public function __construct($successfulField, $postErrorField, $successfulReceiptNumber, $dfcutranref)
    {
      $this->successfulField = $successfulField;
      $this->postErrorField = $postErrorField;
      $this->successfulReceiptNumber = $successfulReceiptNumber;
      $this->dfcutranref = $dfcutranref;
    }

}

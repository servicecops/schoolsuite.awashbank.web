<?php

class Requeststatus
{

    /**
     * @var boolean $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @param boolean $status
     * @param string $description
     * @access public
     */
    public function __construct($status, $description)
    {
      $this->status = $status;
      $this->description = $description;
    }

}

<?php

class ValidSchoolpayStudentQuery
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strrequestid
     * @access public
     */
    public $strrequestid = null;

    /**
     * @var int $stramount
     * @access public
     */
    public $stramount = null;

    /**
     * @var string $strstudentpaymentcode
     * @access public
     */
    public $strstudentpaymentcode = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strusr
     * @param string $strpwd
     * @param string $strrequestid
     * @param int $stramount
     * @param string $strstudentpaymentcode
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strusr, $strpwd, $strrequestid, $stramount, $strstudentpaymentcode, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strrequestid = $strrequestid;
      $this->stramount = $stramount;
      $this->strstudentpaymentcode = $strstudentpaymentcode;
      $this->strsignature = $strsignature;
    }

}

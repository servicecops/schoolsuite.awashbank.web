<?php

class ValidcustNWSC
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strCustomerRef
     * @access public
     */
    public $strCustomerRef = null;

    /**
     * @var string $strCustArea
     * @access public
     */
    public $strCustArea = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strCustomerRef
     * @param string $strCustArea
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strCustomerRef, $strCustArea, $strVendorCode, $strVendorPassword, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strCustomerRef = $strCustomerRef;
      $this->strCustArea = $strCustArea;
      $this->strVendorCode = $strVendorCode;
      $this->strVendorPassword = $strVendorPassword;
      $this->strsignature = $strsignature;
    }

}

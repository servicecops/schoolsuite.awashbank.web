<?php

class UmemePrepaidTxnResult
{

    /**
     * @var string $lifeline
     * @access public
     */
    public $lifeline = null;

    /**
     * @var string $payaccount
     * @access public
     */
    public $payaccount = null;

    /**
     * @var string $remainingcredit
     * @access public
     */
    public $remainingcredit = null;

    /**
     * @var string $debtrecovery
     * @access public
     */
    public $debtrecovery = null;

    /**
     * @var string $receiptnumber
     * @access public
     */
    public $receiptnumber = null;

    /**
     * @var string $statusdescription
     * @access public
     */
    public $statusdescription = null;

    /**
     * @var string $statuscode
     * @access public
     */
    public $statuscode = null;

    /**
     * @var string $meternumber
     * @access public
     */
    public $meternumber = null;

    /**
     * @var string $units
     * @access public
     */
    public $units = null;

    /**
     * @var string $tokenvalue
     * @access public
     */
    public $tokenvalue = null;

    /**
     * @var string $inflation
     * @access public
     */
    public $inflation = null;

    /**
     * @var string $tax
     * @access public
     */
    public $tax = null;

    /**
     * @var string $fx
     * @access public
     */
    public $fx = null;

    /**
     * @var string $fuel
     * @access public
     */
    public $fuel = null;

    /**
     * @var string $totalamount
     * @access public
     */
    public $totalamount = null;

    /**
     * @var string $prepaidtoken
     * @access public
     */
    public $prepaidtoken = null;

    /**
     * @var string $dfcutranref
     * @access public
     */
    public $dfcutranref = null;

    /**
     * @param string $lifeline
     * @param string $payaccount
     * @param string $remainingcredit
     * @param string $debtrecovery
     * @param string $receiptnumber
     * @param string $statusdescription
     * @param string $statuscode
     * @param string $meternumber
     * @param string $units
     * @param string $tokenvalue
     * @param string $inflation
     * @param string $tax
     * @param string $fx
     * @param string $fuel
     * @param string $totalamount
     * @param string $prepaidtoken
     * @param string $dfcutranref
     * @access public
     */
    public function __construct($lifeline, $payaccount, $remainingcredit, $debtrecovery, $receiptnumber, $statusdescription, $statuscode, $meternumber, $units, $tokenvalue, $inflation, $tax, $fx, $fuel, $totalamount, $prepaidtoken, $dfcutranref)
    {
      $this->lifeline = $lifeline;
      $this->payaccount = $payaccount;
      $this->remainingcredit = $remainingcredit;
      $this->debtrecovery = $debtrecovery;
      $this->receiptnumber = $receiptnumber;
      $this->statusdescription = $statusdescription;
      $this->statuscode = $statuscode;
      $this->meternumber = $meternumber;
      $this->units = $units;
      $this->tokenvalue = $tokenvalue;
      $this->inflation = $inflation;
      $this->tax = $tax;
      $this->fx = $fx;
      $this->fuel = $fuel;
      $this->totalamount = $totalamount;
      $this->prepaidtoken = $prepaidtoken;
      $this->dfcutranref = $dfcutranref;
    }

}

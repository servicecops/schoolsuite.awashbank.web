<?php

class CustChkLimResp
{

    /**
     * @var int $success
     * @access public
     */
    public $success = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var int $customerlimit
     * @access public
     */
    public $customerlimit = null;

    /**
     * @param int $success
     * @param string $message
     * @param int $customerlimit
     * @access public
     */
    public function __construct($success, $message, $customerlimit)
    {
      $this->success = $success;
      $this->message = $message;
      $this->customerlimit = $customerlimit;
    }

}

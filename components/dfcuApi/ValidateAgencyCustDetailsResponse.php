<?php

class ValidateAgencyCustDetailsResponse
{

    /**
     * @var AgencyCustDetailz[] $ValidateAgencyCustDetailsResult
     * @access public
     */
    public $ValidateAgencyCustDetailsResult = null;

    /**
     * @param AgencyCustDetailz[] $ValidateAgencyCustDetailsResult
     * @access public
     */
    public function __construct($ValidateAgencyCustDetailsResult)
    {
      $this->ValidateAgencyCustDetailsResult = $ValidateAgencyCustDetailsResult;
    }

}

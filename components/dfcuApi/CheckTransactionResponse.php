<?php

class CheckTransactionResponse
{

    /**
     * @var TxnResultz[] $CheckTransactionResult
     * @access public
     */
    public $CheckTransactionResult = null;

    /**
     * @param TxnResultz[] $CheckTransactionResult
     * @access public
     */
    public function __construct($CheckTransactionResult)
    {
      $this->CheckTransactionResult = $CheckTransactionResult;
    }

}

<?php

class ValiddoFundsTransfer
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $stracc_id
     * @access public
     */
    public $stracc_id = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $stracc_id
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $stracc_id, $strVendorCode, $strVendorPassword, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->stracc_id = $stracc_id;
      $this->strVendorCode = $strVendorCode;
      $this->strVendorPassword = $strVendorPassword;
      $this->strsignature = $strsignature;
    }

}

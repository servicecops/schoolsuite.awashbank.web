<?php

class ValidRegCust
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strmobileno
     * @access public
     */
    public $strmobileno = null;

    /**
     * @var string $strCustomerRef
     * @access public
     */
    public $strCustomerRef = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strusr
     * @param string $strpwd
     * @param string $strmobileno
     * @param string $strCustomerRef
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strusr, $strpwd, $strmobileno, $strCustomerRef, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strmobileno = $strmobileno;
      $this->strCustomerRef = $strCustomerRef;
      $this->strsignature = $strsignature;
    }

}

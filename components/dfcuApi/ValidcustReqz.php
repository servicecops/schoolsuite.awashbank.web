<?php

class ValidcustReqz
{

    /**
     * @var ValidcustNWSC $validcustNWSC
     * @access public
     */
    public $validcustNWSC = null;

    /**
     * @var ValidcustUmemeUEDCL $validcustUmemeUEDCL
     * @access public
     */
    public $validcustUmemeUEDCL = null;

    /**
     * @var ValidPOSPRN $validposprn
     * @access public
     */
    public $validposprn = null;

    /**
     * @var ValidRegCust $validregcust
     * @access public
     */
    public $validregcust = null;

    /**
     * @var Validchkcustlim $validchkcustlim
     * @access public
     */
    public $validchkcustlim = null;

    /**
     * @var Validcustlalo $validcustlalo
     * @access public
     */
    public $validcustlalo = null;

    /**
     * @var ValidPstlo $validpstlo
     * @access public
     */
    public $validpstlo = null;

    /**
     * @var ValidKCCA $validkcca
     * @access public
     */
    public $validkcca = null;

    /**
     * @var Validconfirmloanreq $validconfirmloanreq
     * @access public
     */
    public $validconfirmloanreq = null;

    /**
     * @var ValidGenericPOSPRN $validgenericposprn
     * @access public
     */
    public $validgenericposprn = null;

    /**
     * @var ValidDeRegCust $validderegcust
     * @access public
     */
    public $validderegcust = null;

    /**
     * @var ValidRepayLoan $validrepayloan
     * @access public
     */
    public $validrepayloan = null;

    /**
     * @var ValidSchoolpayStudentQuery $validschoolpaystudentquery
     * @access public
     */
    public $validschoolpaystudentquery = null;

    /**
     * @var ValidcustDSTV $validcustdstv
     * @access public
     */
    public $validcustdstv = null;

    /**
     * @var ValidBALINQ $validbalinq
     * @access public
     */
    public $validbalinq = null;

    /**
     * @var ValiddoFundsTransfer $validdofundstransfer
     * @access public
     */
    public $validdofundstransfer = null;

    /**
     * @var ValidZeepayStudent $validzeepaystudent
     * @access public
     */
    public $validzeepaystudent = null;

    /**
     * @param ValidcustNWSC $validcustNWSC
     * @param ValidcustUmemeUEDCL $validcustUmemeUEDCL
     * @param ValidPOSPRN $validposprn
     * @param ValidRegCust $validregcust
     * @param Validchkcustlim $validchkcustlim
     * @param Validcustlalo $validcustlalo
     * @param ValidPstlo $validpstlo
     * @param ValidKCCA $validkcca
     * @param Validconfirmloanreq $validconfirmloanreq
     * @param ValidGenericPOSPRN $validgenericposprn
     * @param ValidDeRegCust $validderegcust
     * @param ValidRepayLoan $validrepayloan
     * @param ValidSchoolpayStudentQuery $validschoolpaystudentquery
     * @param ValidcustDSTV $validcustdstv
     * @param ValidBALINQ $validbalinq
     * @param ValiddoFundsTransfer $validdofundstransfer
     * @param ValidZeepayStudent $validzeepaystudent
     * @access public
     */
    public function __construct($validcustNWSC, $validcustUmemeUEDCL, $validposprn, $validregcust, $validchkcustlim, $validcustlalo, $validpstlo, $validkcca, $validconfirmloanreq, $validgenericposprn, $validderegcust, $validrepayloan, $validschoolpaystudentquery, $validcustdstv, $validbalinq, $validdofundstransfer, $validzeepaystudent)
    {
      $this->validcustNWSC = $validcustNWSC;
      $this->validcustUmemeUEDCL = $validcustUmemeUEDCL;
      $this->validposprn = $validposprn;
      $this->validregcust = $validregcust;
      $this->validchkcustlim = $validchkcustlim;
      $this->validcustlalo = $validcustlalo;
      $this->validpstlo = $validpstlo;
      $this->validkcca = $validkcca;
      $this->validconfirmloanreq = $validconfirmloanreq;
      $this->validgenericposprn = $validgenericposprn;
      $this->validderegcust = $validderegcust;
      $this->validrepayloan = $validrepayloan;
      $this->validschoolpaystudentquery = $validschoolpaystudentquery;
      $this->validcustdstv = $validcustdstv;
      $this->validbalinq = $validbalinq;
      $this->validdofundstransfer = $validdofundstransfer;
      $this->validzeepaystudent = $validzeepaystudent;
    }

}

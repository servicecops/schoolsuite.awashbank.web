<?php

class CustLastLoanResp
{

    /**
     * @var int $success
     * @access public
     */
    public $success = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var string $lastloan
     * @access public
     */
    public $lastloan = null;

    /**
     * @param int $success
     * @param string $message
     * @param string $lastloan
     * @access public
     */
    public function __construct($success, $message, $lastloan)
    {
      $this->success = $success;
      $this->message = $message;
      $this->lastloan = $lastloan;
    }

}

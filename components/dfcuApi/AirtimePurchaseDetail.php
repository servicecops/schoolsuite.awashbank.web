<?php

class AirtimePurchaseDetail
{

    /**
     * @var string $responseId
     * @access public
     */
    public $responseId = null;

    /**
     * @var string $code
     * @access public
     */
    public $code = null;

    /**
     * @var string $status
     * @access public
     */
    public $status = null;

    /**
     * @param string $responseId
     * @param string $code
     * @param string $status
     * @access public
     */
    public function __construct($responseId, $code, $status)
    {
      $this->responseId = $responseId;
      $this->code = $code;
      $this->status = $status;
    }

}

<?php

class Validposttranz
{

    /**
     * @var ValidpostNWSCTran $validpostNWSCtran
     * @access public
     */
    public $validpostNWSCtran = null;

    /**
     * @var ValidpostUmemeUEDCLTran $validpostUmemeUEDCLtran
     * @access public
     */
    public $validpostUmemeUEDCLtran = null;

    /**
     * @var ValidpostURAPOSTran $validposturapostran
     * @access public
     */
    public $validposturapostran = null;

    /**
     * @var ValidpostSchoolpayTran[] $validpostschoolpaytran
     * @access public
     */
    public $validpostschoolpaytran = null;

    /**
     * @param ValidpostNWSCTran $validpostNWSCtran
     * @param ValidpostUmemeUEDCLTran $validpostUmemeUEDCLtran
     * @param ValidpostURAPOSTran $validposturapostran
     * @param ValidpostSchoolpayTran[] $validpostschoolpaytran
     * @access public
     */
    public function __construct($validpostNWSCtran, $validpostUmemeUEDCLtran, $validposturapostran, $validpostschoolpaytran)
    {
      $this->validpostNWSCtran = $validpostNWSCtran;
      $this->validpostUmemeUEDCLtran = $validpostUmemeUEDCLtran;
      $this->validposturapostran = $validposturapostran;
      $this->validpostschoolpaytran = $validpostschoolpaytran;
    }

}

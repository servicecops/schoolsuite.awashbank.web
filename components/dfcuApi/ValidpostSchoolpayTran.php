<?php

class ValidpostSchoolpayTran
{

    /**
     * @var SchoolpayTrandetails $schoolpaytrandetails
     * @access public
     */
    public $schoolpaytrandetails = null;

    /**
     * @var BulkPaymentWrapper[] $feeassociations
     * @access public
     */
    public $feeassociations = null;

    /**
     * @param SchoolpayTrandetails $schoolpaytrandetails
     * @param BulkPaymentWrapper[] $feeassociations
     * @access public
     */
    public function __construct($schoolpaytrandetails, $feeassociations)
    {
      $this->schoolpaytrandetails = $schoolpaytrandetails;
      $this->feeassociations = $feeassociations;
    }

}

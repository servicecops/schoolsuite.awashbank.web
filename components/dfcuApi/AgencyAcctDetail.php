<?php

class AgencyAcctDetail
{

    /**
     * @var string $acct_id
     * @access public
     */
    public $acct_id = null;

    /**
     * @var string $acct_name
     * @access public
     */
    public $acct_name = null;

    /**
     * @var string $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $StatusDesc
     * @access public
     */
    public $StatusDesc = null;

    /**
     * @param string $acct_id
     * @param string $acct_name
     * @param string $StatusCode
     * @param string $StatusDesc
     * @access public
     */
    public function __construct($acct_id, $acct_name, $StatusCode, $StatusDesc)
    {
      $this->acct_id = $acct_id;
      $this->acct_name = $acct_name;
      $this->StatusCode = $StatusCode;
      $this->StatusDesc = $StatusDesc;
    }

}

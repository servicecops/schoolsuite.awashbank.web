<?php

class CustomerDetailz
{

    /**
     * @var Requeststatus $requeststatus
     * @access public
     */
    public $requeststatus = null;

    /**
     * @var UmemeCustomerProfile $umemecustomerprofile
     * @access public
     */
    public $umemecustomerprofile = null;

    /**
     * @var NWSC_Customer $nwsccustomerprofile
     * @access public
     */
    public $nwsccustomerprofile = null;

    /**
     * @var URAPRNDetail $uracustomerprofile
     * @access public
     */
    public $uracustomerprofile = null;

    /**
     * @var UEDCLCustomerProfile $uedclcustomerprofile
     * @access public
     */
    public $uedclcustomerprofile = null;

    /**
     * @var KCCAPRNDetail $kccaprndetails
     * @access public
     */
    public $kccaprndetails = null;

    /**
     * @var POSPaymentRegistrationDtlz $POSPaymentRegistrationDtlz
     * @access public
     */
    public $POSPaymentRegistrationDtlz = null;

    /**
     * @var CustRegRespx $custregrespx
     * @access public
     */
    public $custregrespx = null;

    /**
     * @var CustChkLimResp $custchklimresp
     * @access public
     */
    public $custchklimresp = null;

    /**
     * @var CustLastLoanResp $custlastloanresp
     * @access public
     */
    public $custlastloanresp = null;

    /**
     * @var CustPstLoanResp $custpstloanresp
     * @access public
     */
    public $custpstloanresp = null;

    /**
     * @var ConfirmLoanResp $confirmloanresp
     * @access public
     */
    public $confirmloanresp = null;

    /**
     * @var RegisteredPaymentTransDtlz $registeredpaymenttransdtlz
     * @access public
     */
    public $registeredpaymenttransdtlz = null;

    /**
     * @var RepayLoanResp $repayloanresp
     * @access public
     */
    public $repayloanresp = null;

    /**
     * @var SchoolpaystudentqueryResponse[] $schoolpaystudentqueryresponse
     * @access public
     */
    public $schoolpaystudentqueryresponse = null;

    /**
     * @var AirtimePurchaseDetail $airtimepurchasedetail
     * @access public
     */
    public $airtimepurchasedetail = null;

    /**
     * @var GetbundlesPeriodsDetail[] $getbundlesperiodsdetail
     * @access public
     */
    public $getbundlesperiodsdetail = null;

    /**
     * @param Requeststatus $requeststatus
     * @param UmemeCustomerProfile $umemecustomerprofile
     * @param NWSC_Customer $nwsccustomerprofile
     * @param URAPRNDetail $uracustomerprofile
     * @param UEDCLCustomerProfile $uedclcustomerprofile
     * @param KCCAPRNDetail $kccaprndetails
     * @param POSPaymentRegistrationDtlz $POSPaymentRegistrationDtlz
     * @param CustRegRespx $custregrespx
     * @param CustChkLimResp $custchklimresp
     * @param CustLastLoanResp $custlastloanresp
     * @param CustPstLoanResp $custpstloanresp
     * @param ConfirmLoanResp $confirmloanresp
     * @param RegisteredPaymentTransDtlz $registeredpaymenttransdtlz
     * @param RepayLoanResp $repayloanresp
     * @param SchoolpaystudentqueryResponse[] $schoolpaystudentqueryresponse
     * @param AirtimePurchaseDetail $airtimepurchasedetail
     * @param GetbundlesPeriodsDetail[] $getbundlesperiodsdetail
     * @access public
     */
    public function __construct($requeststatus, $umemecustomerprofile, $nwsccustomerprofile, $uracustomerprofile, $uedclcustomerprofile, $kccaprndetails, $POSPaymentRegistrationDtlz, $custregrespx, $custchklimresp, $custlastloanresp, $custpstloanresp, $confirmloanresp, $registeredpaymenttransdtlz, $repayloanresp, $schoolpaystudentqueryresponse, $airtimepurchasedetail, $getbundlesperiodsdetail)
    {
      $this->requeststatus = $requeststatus;
      $this->umemecustomerprofile = $umemecustomerprofile;
      $this->nwsccustomerprofile = $nwsccustomerprofile;
      $this->uracustomerprofile = $uracustomerprofile;
      $this->uedclcustomerprofile = $uedclcustomerprofile;
      $this->kccaprndetails = $kccaprndetails;
      $this->POSPaymentRegistrationDtlz = $POSPaymentRegistrationDtlz;
      $this->custregrespx = $custregrespx;
      $this->custchklimresp = $custchklimresp;
      $this->custlastloanresp = $custlastloanresp;
      $this->custpstloanresp = $custpstloanresp;
      $this->confirmloanresp = $confirmloanresp;
      $this->registeredpaymenttransdtlz = $registeredpaymenttransdtlz;
      $this->repayloanresp = $repayloanresp;
      $this->schoolpaystudentqueryresponse = $schoolpaystudentqueryresponse;
      $this->airtimepurchasedetail = $airtimepurchasedetail;
      $this->getbundlesperiodsdetail = $getbundlesperiodsdetail;
    }

}

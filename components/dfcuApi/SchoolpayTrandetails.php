<?php

class SchoolpayTrandetails
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strstudentpaymentcode
     * @access public
     */
    public $strstudentpaymentcode = null;

    /**
     * @var int $stramount
     * @access public
     */
    public $stramount = null;

    /**
     * @var string $strdepositor_phone
     * @access public
     */
    public $strdepositor_phone = null;

    /**
     * @var string $strdepositor_name
     * @access public
     */
    public $strdepositor_name = null;

    /**
     * @var string $strtran_id
     * @access public
     */
    public $strtran_id = null;

    /**
     * @var string $strmemo
     * @access public
     */
    public $strmemo = null;

    /**
     * @var string $strprocessDate
     * @access public
     */
    public $strprocessDate = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strstudentpaymentcode
     * @param int $stramount
     * @param string $strdepositor_phone
     * @param string $strdepositor_name
     * @param string $strtran_id
     * @param string $strmemo
     * @param string $strprocessDate
     * @param string $strusr
     * @param string $strpwd
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strstudentpaymentcode, $stramount, $strdepositor_phone, $strdepositor_name, $strtran_id, $strmemo, $strprocessDate, $strusr, $strpwd, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strstudentpaymentcode = $strstudentpaymentcode;
      $this->stramount = $stramount;
      $this->strdepositor_phone = $strdepositor_phone;
      $this->strdepositor_name = $strdepositor_name;
      $this->strtran_id = $strtran_id;
      $this->strmemo = $strmemo;
      $this->strprocessDate = $strprocessDate;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strsignature = $strsignature;
    }

}

<?php

class ValidateCustomerDetailsResponse
{

    /**
     * @var CustomerDetailz[] $ValidateCustomerDetailsResult
     * @access public
     */
    public $ValidateCustomerDetailsResult = null;

    /**
     * @param CustomerDetailz[] $ValidateCustomerDetailsResult
     * @access public
     */
    public function __construct($ValidateCustomerDetailsResult)
    {
      $this->ValidateCustomerDetailsResult = $ValidateCustomerDetailsResult;
    }

}

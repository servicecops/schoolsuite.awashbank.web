<?php

class ValidateAgencyCustDetails
{

    /**
     * @var ValidAgencyReqz[] $initialdetails
     * @access public
     */
    public $initialdetails = null;

    /**
     * @param ValidAgencyReqz[] $initialdetails
     * @access public
     */
    public function __construct($initialdetails)
    {
      $this->initialdetails = $initialdetails;
    }

}

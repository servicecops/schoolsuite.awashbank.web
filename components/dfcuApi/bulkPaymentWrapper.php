<?php

class bulkPaymentWrapper
{

    /**
     * @var string $feeId
     * @access public
     */
    public $feeId = null;

    /**
     * @var string $feeMemo
     * @access public
     */
    public $feeMemo = null;

    /**
     * @var string $tenderedAmount
     * @access public
     */
    public $tenderedAmount = null;

    /**
     * @param string $feeId
     * @param string $feeMemo
     * @param string $tenderedAmount
     * @access public
     */
    public function __construct($feeId, $feeMemo, $tenderedAmount)
    {
      $this->feeId = $feeId;
      $this->feeMemo = $feeMemo;
      $this->tenderedAmount = $tenderedAmount;
    }

}

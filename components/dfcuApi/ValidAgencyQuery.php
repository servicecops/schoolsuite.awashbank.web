<?php

class ValidAgencyQuery
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $stracctid
     * @access public
     */
    public $stracctid = null;

    /**
     * @var string $strAgentID
     * @access public
     */
    public $strAgentID = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $stracctid
     * @param string $strAgentID
     * @param string $strusr
     * @param string $strpwd
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $stracctid, $strAgentID, $strusr, $strpwd, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->stracctid = $stracctid;
      $this->strAgentID = $strAgentID;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strsignature = $strsignature;
    }

}

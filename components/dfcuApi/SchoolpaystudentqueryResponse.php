<?php

class SchoolpaystudentqueryResponse
{

    /**
     * @var SchoolpaystudentqueryResponsedetails $schoolpaystudentqueryresponsedetails
     * @access public
     */
    public $schoolpaystudentqueryresponsedetails = null;

    /**
     * @var AssociatedFees[] $associatedfees
     * @access public
     */
    public $associatedfees = null;

    /**
     * @param SchoolpaystudentqueryResponsedetails $schoolpaystudentqueryresponsedetails
     * @param AssociatedFees[] $associatedfees
     * @access public
     */
    public function __construct($schoolpaystudentqueryresponsedetails, $associatedfees)
    {
      $this->schoolpaystudentqueryresponsedetails = $schoolpaystudentqueryresponsedetails;
      $this->associatedfees = $associatedfees;
    }

}

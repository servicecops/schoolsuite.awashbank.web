<?php

class UEDCLCustomerProfile
{

    /**
     * @var string $MininumAmount
     * @access public
     */
    public $MininumAmount = null;

    /**
     * @var string $CustName
     * @access public
     */
    public $CustName = null;

    /**
     * @var string $Area
     * @access public
     */
    public $Area = null;

    /**
     * @var string $MeterNumber
     * @access public
     */
    public $MeterNumber = null;

    /**
     * @var string $AccountRef
     * @access public
     */
    public $AccountRef = null;

    /**
     * @var string $StatusDesc
     * @access public
     */
    public $StatusDesc = null;

    /**
     * @var string $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $custErrorResult
     * @access public
     */
    public $custErrorResult = null;

    /**
     * @param string $MininumAmount
     * @param string $CustName
     * @param string $Area
     * @param string $MeterNumber
     * @param string $AccountRef
     * @param string $StatusDesc
     * @param string $StatusCode
     * @param string $custErrorResult
     * @access public
     */
    public function __construct($MininumAmount, $CustName, $Area, $MeterNumber, $AccountRef, $StatusDesc, $StatusCode, $custErrorResult)
    {
      $this->MininumAmount = $MininumAmount;
      $this->CustName = $CustName;
      $this->Area = $Area;
      $this->MeterNumber = $MeterNumber;
      $this->AccountRef = $AccountRef;
      $this->StatusDesc = $StatusDesc;
      $this->StatusCode = $StatusCode;
      $this->custErrorResult = $custErrorResult;
    }

}

<?php

class TxnResultzAgencyBank
{

    /**
     * @var Requeststatus $requeststatus
     * @access public
     */
    public $requeststatus = null;

    /**
     * @var AgencyBankDepositPostTxnResult $agencybankdepositposttxnResult
     * @access public
     */
    public $agencybankdepositposttxnResult = null;

    /**
     * @param Requeststatus $requeststatus
     * @param AgencyBankDepositPostTxnResult $agencybankdepositposttxnResult
     * @access public
     */
    public function __construct($requeststatus, $agencybankdepositposttxnResult)
    {
      $this->requeststatus = $requeststatus;
      $this->agencybankdepositposttxnResult = $agencybankdepositposttxnResult;
    }

}

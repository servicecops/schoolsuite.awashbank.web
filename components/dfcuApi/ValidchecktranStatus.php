<?php

class ValidchecktranStatus
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strtran_refno
     * @access public
     */
    public $strtran_refno = null;

    /**
     * @var string $strtranDate
     * @access public
     */
    public $strtranDate = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strtran_refno
     * @param string $strtranDate
     * @param string $strusr
     * @param string $strpwd
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strtran_refno, $strtranDate, $strusr, $strpwd, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strtran_refno = $strtran_refno;
      $this->strtranDate = $strtranDate;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strsignature = $strsignature;
    }

}

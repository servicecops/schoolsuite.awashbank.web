<?php

class SchoolpaystudentqueryResponsedetails
{

    /**
     * @var string $dateOfBirth
     * @access public
     */
    public $dateOfBirth = null;

    /**
     * @var string $firstName
     * @access public
     */
    public $firstName = null;

    /**
     * @var string $lastName
     * @access public
     */
    public $lastName = null;

    /**
     * @var string $middleName
     * @access public
     */
    public $middleName = null;

    /**
     * @var string $outstandingAmount
     * @access public
     */
    public $outstandingAmount = null;

    /**
     * @var string $paymentCode
     * @access public
     */
    public $paymentCode = null;

    /**
     * @var string $registrationNumber
     * @access public
     */
    public $registrationNumber = null;

    /**
     * @var string $schoolCode
     * @access public
     */
    public $schoolCode = null;

    /**
     * @var string $schoolName
     * @access public
     */
    public $schoolName = null;

    /**
     * @var string $studentClass
     * @access public
     */
    public $studentClass = null;

    /**
     * @var int $returnCode
     * @access public
     */
    public $returnCode = null;

    /**
     * @var string $returnMessage
     * @access public
     */
    public $returnMessage = null;

    /**
     * @param string $dateOfBirth
     * @param string $firstName
     * @param string $lastName
     * @param string $middleName
     * @param string $outstandingAmount
     * @param string $paymentCode
     * @param string $registrationNumber
     * @param string $schoolCode
     * @param string $schoolName
     * @param string $studentClass
     * @param int $returnCode
     * @param string $returnMessage
     * @access public
     */
    public function __construct($dateOfBirth, $firstName, $lastName, $middleName, $outstandingAmount, $paymentCode, $registrationNumber, $schoolCode, $schoolName, $studentClass, $returnCode, $returnMessage)
    {
      $this->dateOfBirth = $dateOfBirth;
      $this->firstName = $firstName;
      $this->lastName = $lastName;
      $this->middleName = $middleName;
      $this->outstandingAmount = $outstandingAmount;
      $this->paymentCode = $paymentCode;
      $this->registrationNumber = $registrationNumber;
      $this->schoolCode = $schoolCode;
      $this->schoolName = $schoolName;
      $this->studentClass = $studentClass;
      $this->returnCode = $returnCode;
      $this->returnMessage = $returnMessage;
    }

}

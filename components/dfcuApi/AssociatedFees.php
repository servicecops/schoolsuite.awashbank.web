<?php

class AssociatedFees
{

    /**
     * @var string $feeAmount
     * @access public
     */
    public $feeAmount = null;

    /**
     * @var int $feeId
     * @access public
     */
    public $feeId = null;

    /**
     * @var string $feedescription
     * @access public
     */
    public $feedescription = null;

    /**
     * @var string $feeduefromtdate
     * @access public
     */
    public $feeduefromtdate = null;

    /**
     * @var string $feeduetodate
     * @access public
     */
    public $feeduetodate = null;

    /**
     * @var boolean $feeldspecified
     * @access public
     */
    public $feeldspecified = null;

    /**
     * @param string $feeAmount
     * @param int $feeId
     * @param string $feedescription
     * @param string $feeduefromtdate
     * @param string $feeduetodate
     * @param boolean $feeldspecified
     * @access public
     */
    public function __construct($feeAmount, $feeId, $feedescription, $feeduefromtdate, $feeduetodate, $feeldspecified)
    {
      $this->feeAmount = $feeAmount;
      $this->feeId = $feeId;
      $this->feedescription = $feedescription;
      $this->feeduefromtdate = $feeduefromtdate;
      $this->feeduetodate = $feeduetodate;
      $this->feeldspecified = $feeldspecified;
    }

}

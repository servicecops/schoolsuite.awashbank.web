<?php

class ValidAgencyReqz
{

    /**
     * @var ValidAgencyQuery $validagencyquery
     * @access public
     */
    public $validagencyquery = null;

    /**
     * @param ValidAgencyQuery $validagencyquery
     * @access public
     */
    public function __construct($validagencyquery)
    {
      $this->validagencyquery = $validagencyquery;
    }

}

<?php

class NWSCPostTxnResult
{

    /**
     * @var string $successful
     * @access public
     */
    public $successful = null;

    /**
     * @var string $postError
     * @access public
     */
    public $postError = null;

    /**
     * @var string $transactionid
     * @access public
     */
    public $transactionid = null;

    /**
     * @param string $successful
     * @param string $postError
     * @param string $transactionid
     * @access public
     */
    public function __construct($successful, $postError, $transactionid)
    {
      $this->successful = $successful;
      $this->postError = $postError;
      $this->transactionid = $transactionid;
    }

}

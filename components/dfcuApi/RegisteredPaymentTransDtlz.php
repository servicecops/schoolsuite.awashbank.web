<?php

class RegisteredPaymentTransDtlz
{

    /**
     * @var string $errorCode
     * @access public
     */
    public $errorCode = null;

    /**
     * @var string $errorMessage
     * @access public
     */
    public $errorMessage = null;

    /**
     * @var string $paymentRegNo
     * @access public
     */
    public $paymentRegNo = null;

    /**
     * @var string $searchCode
     * @access public
     */
    public $searchCode = null;

    /**
     * @param string $errorCode
     * @param string $errorMessage
     * @param string $paymentRegNo
     * @param string $searchCode
     * @access public
     */
    public function __construct($errorCode, $errorMessage, $paymentRegNo, $searchCode)
    {
      $this->errorCode = $errorCode;
      $this->errorMessage = $errorMessage;
      $this->paymentRegNo = $paymentRegNo;
      $this->searchCode = $searchCode;
    }

}

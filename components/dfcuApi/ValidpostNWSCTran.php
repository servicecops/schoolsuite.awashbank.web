<?php

class ValidpostNWSCTran
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strCustomerRef
     * @access public
     */
    public $strCustomerRef = null;

    /**
     * @var string $strCustArea
     * @access public
     */
    public $strCustArea = null;

    /**
     * @var string $strCustPhone
     * @access public
     */
    public $strCustPhone = null;

    /**
     * @var string $dtTxnDate
     * @access public
     */
    public $dtTxnDate = null;

    /**
     * @var float $dblAmount
     * @access public
     */
    public $dblAmount = null;

    /**
     * @var string $strTxnRef
     * @access public
     */
    public $strTxnRef = null;

    /**
     * @var string $strPayMethod
     * @access public
     */
    public $strPayMethod = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strCustomerRef
     * @param string $strCustArea
     * @param string $strCustPhone
     * @param string $dtTxnDate
     * @param float $dblAmount
     * @param string $strTxnRef
     * @param string $strPayMethod
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strCustomerRef, $strCustArea, $strCustPhone, $dtTxnDate, $dblAmount, $strTxnRef, $strPayMethod, $strVendorCode, $strVendorPassword, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strCustomerRef = $strCustomerRef;
      $this->strCustArea = $strCustArea;
      $this->strCustPhone = $strCustPhone;
      $this->dtTxnDate = $dtTxnDate;
      $this->dblAmount = $dblAmount;
      $this->strTxnRef = $strTxnRef;
      $this->strPayMethod = $strPayMethod;
      $this->strVendorCode = $strVendorCode;
      $this->strVendorPassword = $strVendorPassword;
      $this->strsignature = $strsignature;
    }

}

<?php

class ValidpostAgencyBanktranz
{

    /**
     * @var ValidpostAgencyDepositTran $validpostagencydepositTran
     * @access public
     */
    public $validpostagencydepositTran = null;

    /**
     * @param ValidpostAgencyDepositTran $validpostagencydepositTran
     * @access public
     */
    public function __construct($validpostagencydepositTran)
    {
      $this->validpostagencydepositTran = $validpostagencydepositTran;
    }

}

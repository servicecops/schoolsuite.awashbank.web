<?php

class RequeststatusSchpayst
{

    /**
     * @var string $Account_No
     * @access public
     */
    public $Account_No = null;

    /**
     * @var string $Branch_ID
     * @access public
     */
    public $Branch_ID = null;

    /**
     * @var string $Currency_Code
     * @access public
     */
    public $Currency_Code = null;

    /**
     * @var string $Date_From
     * @access public
     */
    public $Date_From = null;

    /**
     * @var string $Date_To
     * @access public
     */
    public $Date_To = null;

    /**
     * @var boolean $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @param string $Account_No
     * @param string $Branch_ID
     * @param string $Currency_Code
     * @param string $Date_From
     * @param string $Date_To
     * @param boolean $status
     * @param string $description
     * @access public
     */
    public function __construct($Account_No, $Branch_ID, $Currency_Code, $Date_From, $Date_To, $status, $description)
    {
      $this->Account_No = $Account_No;
      $this->Branch_ID = $Branch_ID;
      $this->Currency_Code = $Currency_Code;
      $this->Date_From = $Date_From;
      $this->Date_To = $Date_To;
      $this->status = $status;
      $this->description = $description;
    }

}

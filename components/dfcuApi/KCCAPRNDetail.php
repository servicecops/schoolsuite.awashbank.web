<?php

class KCCAPRNDetail
{

    /**
     * @var string $Prn
     * @access public
     */
    public $Prn = null;

    /**
     * @var string $Coin
     * @access public
     */
    public $Coin = null;

    /**
     * @var string $CityoperatorName
     * @access public
     */
    public $CityoperatorName = null;

    /**
     * @var float $Amount
     * @access public
     */
    public $Amount = null;

    /**
     * @var string $PrDate
     * @access public
     */
    public $PrDate = null;

    /**
     * @var string $ExpiryDate
     * @access public
     */
    public $ExpiryDate = null;

    /**
     * @var string $StatusCode
     * @access public
     */
    public $StatusCode = null;

    /**
     * @var string $StatusDesc
     * @access public
     */
    public $StatusDesc = null;

    /**
     * @var string $errDesc
     * @access public
     */
    public $errDesc = null;

    /**
     * @param string $Prn
     * @param string $Coin
     * @param string $CityoperatorName
     * @param float $Amount
     * @param string $PrDate
     * @param string $ExpiryDate
     * @param string $StatusCode
     * @param string $StatusDesc
     * @param string $errDesc
     * @access public
     */
    public function __construct($Prn, $Coin, $CityoperatorName, $Amount, $PrDate, $ExpiryDate, $StatusCode, $StatusDesc, $errDesc)
    {
      $this->Prn = $Prn;
      $this->Coin = $Coin;
      $this->CityoperatorName = $CityoperatorName;
      $this->Amount = $Amount;
      $this->PrDate = $PrDate;
      $this->ExpiryDate = $ExpiryDate;
      $this->StatusCode = $StatusCode;
      $this->StatusDesc = $StatusDesc;
      $this->errDesc = $errDesc;
    }

}

<?php

class AgencyCustDetailz
{

    /**
     * @var Requeststatus $requeststatus
     * @access public
     */
    public $requeststatus = null;

    /**
     * @var AgencyAcctDetail $agencyacctdetail
     * @access public
     */
    public $agencyacctdetail = null;

    /**
     * @param Requeststatus $requeststatus
     * @param AgencyAcctDetail $agencyacctdetail
     * @access public
     */
    public function __construct($requeststatus, $agencyacctdetail)
    {
      $this->requeststatus = $requeststatus;
      $this->agencyacctdetail = $agencyacctdetail;
    }

}

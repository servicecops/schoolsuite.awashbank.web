<?php
namespace app\components\dfcuApi;

include_once('CheckTransaction.php');
include_once('ValidchecktranStatus.php');
include_once('CheckTransactionResponse.php');
include_once('TxnResultz.php');
include_once('Requeststatus.php');
include_once('UmemePostTxnResult.php');
include_once('UmemePrepaidTxnResult.php');
include_once('UEDCLPrepaidTxnResult.php');
include_once('NWSCPostTxnResult.php');
include_once('URAPOSPostTxnResult.php');
include_once('SchoolpaystudentTranResponsedetails.php');
include_once('ValidateCustomerDetails.php');
include_once('ValidcustReqz.php');
include_once('ValidcustNWSC.php');
include_once('ValidcustUmemeUEDCL.php');
include_once('ValidPOSPRN.php');
include_once('ValidRegCust.php');
include_once('Validchkcustlim.php');
include_once('Validcustlalo.php');
include_once('ValidPstlo.php');
include_once('ValidKCCA.php');
include_once('Validconfirmloanreq.php');
include_once('ValidGenericPOSPRN.php');
include_once('ValidDeRegCust.php');
include_once('ValidRepayLoan.php');
include_once('ValidSchoolpayStudentQuery.php');
include_once('ValidcustDSTV.php');
include_once('ValidBALINQ.php');
include_once('ValiddoFundsTransfer.php');
include_once('ValidZeepayStudent.php');
include_once('ValidateCustomerDetailsResponse.php');
include_once('CustomerDetailz.php');
include_once('UmemeCustomerProfile.php');
include_once('NWSC_Customer.php');
include_once('URAPRNDetail.php');
include_once('UEDCLCustomerProfile.php');
include_once('KCCAPRNDetail.php');
include_once('POSPaymentRegistrationDtlz.php');
include_once('CustRegRespx.php');
include_once('CustChkLimResp.php');
include_once('CustLastLoanResp.php');
include_once('CustPstLoanResp.php');
include_once('ConfirmLoanResp.php');
include_once('RegisteredPaymentTransDtlz.php');
include_once('RepayLoanResp.php');
include_once('SchoolpaystudentqueryResponse.php');
include_once('SchoolpaystudentqueryResponsedetails.php');
include_once('AssociatedFees.php');
include_once('AirtimePurchaseDetail.php');
include_once('GetbundlesPeriodsDetail.php');
include_once('PostCustomerTransaction.php');
include_once('Validposttranz.php');
include_once('ValidpostNWSCTran.php');
include_once('ValidpostUmemeUEDCLTran.php');
include_once('ValidpostURAPOSTran.php');
include_once('ValidpostSchoolpayTran.php');
include_once('SchoolpayTrandetails.php');
include_once('PostCustomerTransactionResponse.php');
include_once('GetSCHPSTDetails.php');
include_once('ValidSCHPST.php');
include_once('GetSCHPSTDetailsResponse.php');
include_once('Accountstatements.php');
include_once('RequeststatusSchpayst.php');
include_once('AccountStatement.php');
include_once('ValidateAgencyCustDetails.php');
include_once('ValidAgencyReqz.php');
include_once('ValidAgencyQuery.php');
include_once('ValidateAgencyCustDetailsResponse.php');
include_once('AgencyCustDetailz.php');
include_once('AgencyAcctDetail.php');
include_once('PostAgencyCustTransaction.php');
include_once('ValidpostAgencyBanktranz.php');
include_once('ValidpostAgencyDepositTran.php');
include_once('PostAgencyCustTransactionResponse.php');
include_once('TxnResultzAgencyBank.php');
include_once('AgencyBankDepositPostTxnResult.php');
include_once('bulkPaymentWrapper.php');

class Webapi extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     * @access private
     */
    private static $classmap = array(
      'CheckTransaction' => '\CheckTransaction',
      'ValidchecktranStatus' => '\ValidchecktranStatus',
      'CheckTransactionResponse' => '\CheckTransactionResponse',
      'TxnResultz' => '\TxnResultz',
      'Requeststatus' => '\Requeststatus',
      'UmemePostTxnResult' => '\UmemePostTxnResult',
      'UmemePrepaidTxnResult' => '\UmemePrepaidTxnResult',
      'UEDCLPrepaidTxnResult' => '\UEDCLPrepaidTxnResult',
      'NWSCPostTxnResult' => '\NWSCPostTxnResult',
      'URAPOSPostTxnResult' => '\URAPOSPostTxnResult',
      'SchoolpaystudentTranResponsedetails' => '\SchoolpaystudentTranResponsedetails',
      'ValidateCustomerDetails' => '\ValidateCustomerDetails',
      'ValidcustReqz' => '\ValidcustReqz',
      'ValidcustNWSC' => '\ValidcustNWSC',
      'ValidcustUmemeUEDCL' => '\ValidcustUmemeUEDCL',
      'ValidPOSPRN' => '\ValidPOSPRN',
      'ValidRegCust' => '\ValidRegCust',
      'Validchkcustlim' => '\Validchkcustlim',
      'Validcustlalo' => '\Validcustlalo',
      'ValidPstlo' => '\ValidPstlo',
      'ValidKCCA' => '\ValidKCCA',
      'Validconfirmloanreq' => '\Validconfirmloanreq',
      'ValidGenericPOSPRN' => '\ValidGenericPOSPRN',
      'ValidDeRegCust' => '\ValidDeRegCust',
      'ValidRepayLoan' => '\ValidRepayLoan',
      'ValidSchoolpayStudentQuery' => '\ValidSchoolpayStudentQuery',
      'ValidcustDSTV' => '\ValidcustDSTV',
      'ValidBALINQ' => '\ValidBALINQ',
      'ValiddoFundsTransfer' => '\ValiddoFundsTransfer',
      'ValidZeepayStudent' => '\ValidZeepayStudent',
      'ValidateCustomerDetailsResponse' => '\ValidateCustomerDetailsResponse',
      'CustomerDetailz' => '\CustomerDetailz',
      'UmemeCustomerProfile' => '\UmemeCustomerProfile',
      'NWSC_Customer' => '\NWSC_Customer',
      'URAPRNDetail' => '\URAPRNDetail',
      'UEDCLCustomerProfile' => '\UEDCLCustomerProfile',
      'KCCAPRNDetail' => '\KCCAPRNDetail',
      'POSPaymentRegistrationDtlz' => '\POSPaymentRegistrationDtlz',
      'CustRegRespx' => '\CustRegRespx',
      'CustChkLimResp' => '\CustChkLimResp',
      'CustLastLoanResp' => '\CustLastLoanResp',
      'CustPstLoanResp' => '\CustPstLoanResp',
      'ConfirmLoanResp' => '\ConfirmLoanResp',
      'RegisteredPaymentTransDtlz' => '\RegisteredPaymentTransDtlz',
      'RepayLoanResp' => '\RepayLoanResp',
      'SchoolpaystudentqueryResponse' => '\SchoolpaystudentqueryResponse',
      'SchoolpaystudentqueryResponsedetails' => '\SchoolpaystudentqueryResponsedetails',
      'AssociatedFees' => '\AssociatedFees',
      'AirtimePurchaseDetail' => '\AirtimePurchaseDetail',
      'GetbundlesPeriodsDetail' => '\GetbundlesPeriodsDetail',
      'PostCustomerTransaction' => '\PostCustomerTransaction',
      'Validposttranz' => '\Validposttranz',
      'ValidpostNWSCTran' => '\ValidpostNWSCTran',
      'ValidpostUmemeUEDCLTran' => '\ValidpostUmemeUEDCLTran',
      'ValidpostURAPOSTran' => '\ValidpostURAPOSTran',
      'ValidpostSchoolpayTran' => '\ValidpostSchoolpayTran',
      'SchoolpayTrandetails' => '\SchoolpayTrandetails',
      'PostCustomerTransactionResponse' => '\PostCustomerTransactionResponse',
      'GetSCHPSTDetails' => '\GetSCHPSTDetails',
      'ValidSCHPST' => '\ValidSCHPST',
      'GetSCHPSTDetailsResponse' => '\GetSCHPSTDetailsResponse',
      'Accountstatements' => '\Accountstatements',
      'RequeststatusSchpayst' => '\RequeststatusSchpayst',
      'AccountStatement' => '\AccountStatement',
      'ValidateAgencyCustDetails' => '\ValidateAgencyCustDetails',
      'ValidAgencyReqz' => '\ValidAgencyReqz',
      'ValidAgencyQuery' => '\ValidAgencyQuery',
      'ValidateAgencyCustDetailsResponse' => '\ValidateAgencyCustDetailsResponse',
      'AgencyCustDetailz' => '\AgencyCustDetailz',
      'AgencyAcctDetail' => '\AgencyAcctDetail',
      'PostAgencyCustTransaction' => '\PostAgencyCustTransaction',
      'ValidpostAgencyBanktranz' => '\ValidpostAgencyBanktranz',
      'ValidpostAgencyDepositTran' => '\ValidpostAgencyDepositTran',
      'PostAgencyCustTransactionResponse' => '\PostAgencyCustTransactionResponse',
      'TxnResultzAgencyBank' => '\TxnResultzAgencyBank',
      'AgencyBankDepositPostTxnResult' => '\AgencyBankDepositPostTxnResult',
      'bulkPaymentWrapper' => '\bulkPaymentWrapper');

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     * @access public
     */
    public function __construct(array $options = array(), $wsdl)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }

      parent::__construct($wsdl, $options);
    }

    /**
     * @param CheckTransaction $parameters
     * @access public
     * @return CheckTransactionResponse
     */
    public function CheckTransaction(CheckTransaction $parameters)
    {
      return $this->__soapCall('CheckTransaction', array($parameters));
    }

    /**
     * @param ValidateCustomerDetails $parameters
     * @access public
     * @return ValidateCustomerDetailsResponse
     */
    public function ValidateCustomerDetails(ValidateCustomerDetails $parameters)
    {
      return $this->__soapCall('ValidateCustomerDetails', array($parameters));
    }

    /**
     * @param PostCustomerTransaction $parameters
     * @access public
     * @return PostCustomerTransactionResponse
     */
    public function PostCustomerTransaction(PostCustomerTransaction $parameters)
    {
      return $this->__soapCall('PostCustomerTransaction', array($parameters));
    }

    /**
     * @param GetSCHPSTDetails $parameters
     * @access public
     * @return GetSCHPSTDetailsResponse
     */
    public function GetSCHPSTDetails(GetSCHPSTDetails $parameters)
    {
      return $this->__soapCall('GetSCHPSTDetails', array($parameters));
    }

    /**
     * @param ValidateAgencyCustDetails $parameters
     * @access public
     * @return ValidateAgencyCustDetailsResponse
     */
    public function ValidateAgencyCustDetails(ValidateAgencyCustDetails $parameters)
    {
      return $this->__soapCall('ValidateAgencyCustDetails', array($parameters));
    }

    /**
     * @param PostAgencyCustTransaction $parameters
     * @access public
     * @return PostAgencyCustTransactionResponse
     */
    public function PostAgencyCustTransaction(PostAgencyCustTransaction $parameters)
    {
      return $this->__soapCall('PostAgencyCustTransaction', array($parameters));
    }

}

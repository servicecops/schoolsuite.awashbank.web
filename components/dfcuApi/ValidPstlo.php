<?php

class ValidPstlo
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strusr
     * @access public
     */
    public $strusr = null;

    /**
     * @var string $strpwd
     * @access public
     */
    public $strpwd = null;

    /**
     * @var string $strmobileno
     * @access public
     */
    public $strmobileno = null;

    /**
     * @var string $strCustomerRef
     * @access public
     */
    public $strCustomerRef = null;

    /**
     * @var int $stramount
     * @access public
     */
    public $stramount = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strusr
     * @param string $strpwd
     * @param string $strmobileno
     * @param string $strCustomerRef
     * @param int $stramount
     * @param string $strsignature
     * @access public
     */
    public function __construct($strUtilityCode, $strusr, $strpwd, $strmobileno, $strCustomerRef, $stramount, $strsignature)
    {
      $this->strUtilityCode = $strUtilityCode;
      $this->strusr = $strusr;
      $this->strpwd = $strpwd;
      $this->strmobileno = $strmobileno;
      $this->strCustomerRef = $strCustomerRef;
      $this->stramount = $stramount;
      $this->strsignature = $strsignature;
    }

}

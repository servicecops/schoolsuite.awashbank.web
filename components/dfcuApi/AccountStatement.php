<?php

class AccountStatement
{

    /**
     * @var dateTime $Trandate
     * @access public
     */
    public $Trandate = null;

    /**
     * @var string $TranId
     * @access public
     */
    public $TranId = null;

    /**
     * @var string $Instrmnt_Type
     * @access public
     */
    public $Instrmnt_Type = null;

    /**
     * @var string $Instrmnt_Num
     * @access public
     */
    public $Instrmnt_Num = null;

    /**
     * @var string $Particulars
     * @access public
     */
    public $Particulars = null;

    /**
     * @var string $DebitAmount
     * @access public
     */
    public $DebitAmount = null;

    /**
     * @var string $CreditAmount
     * @access public
     */
    public $CreditAmount = null;

    /**
     * @var string $Currency
     * @access public
     */
    public $Currency = null;

    /**
     * @var string $Balance
     * @access public
     */
    public $Balance = null;

    /**
     * @var string $Bal_Indicator
     * @access public
     */
    public $Bal_Indicator = null;

    /**
     * @var string $Remarks
     * @access public
     */
    public $Remarks = null;

    /**
     * @param dateTime $Trandate
     * @param string $TranId
     * @param string $Instrmnt_Type
     * @param string $Instrmnt_Num
     * @param string $Particulars
     * @param string $DebitAmount
     * @param string $CreditAmount
     * @param string $Currency
     * @param string $Balance
     * @param string $Bal_Indicator
     * @param string $Remarks
     * @access public
     */
    public function __construct($Trandate, $TranId, $Instrmnt_Type, $Instrmnt_Num, $Particulars, $DebitAmount, $CreditAmount, $Currency, $Balance, $Bal_Indicator, $Remarks)
    {
      $this->Trandate = $Trandate;
      $this->TranId = $TranId;
      $this->Instrmnt_Type = $Instrmnt_Type;
      $this->Instrmnt_Num = $Instrmnt_Num;
      $this->Particulars = $Particulars;
      $this->DebitAmount = $DebitAmount;
      $this->CreditAmount = $CreditAmount;
      $this->Currency = $Currency;
      $this->Balance = $Balance;
      $this->Bal_Indicator = $Bal_Indicator;
      $this->Remarks = $Remarks;
    }

}

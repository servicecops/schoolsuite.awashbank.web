<?php

class SchoolpaystudentTranResponsedetails
{

    /**
     * @var string $receiptNumber
     * @access public
     */
    public $receiptNumber = null;

    /**
     * @var string $firstName
     * @access public
     */
    public $firstName = null;

    /**
     * @var string $middleName
     * @access public
     */
    public $middleName = null;

    /**
     * @var string $lastName
     * @access public
     */
    public $lastName = null;

    /**
     * @var string $schoolCode
     * @access public
     */
    public $schoolCode = null;

    /**
     * @var string $schoolName
     * @access public
     */
    public $schoolName = null;

    /**
     * @var string $studentClass
     * @access public
     */
    public $studentClass = null;

    /**
     * @var int $returnCode
     * @access public
     */
    public $returnCode = null;

    /**
     * @var string $returnMessage
     * @access public
     */
    public $returnMessage = null;

    /**
     * @param string $receiptNumber
     * @param string $firstName
     * @param string $middleName
     * @param string $lastName
     * @param string $schoolCode
     * @param string $schoolName
     * @param string $studentClass
     * @param int $returnCode
     * @param string $returnMessage
     * @access public
     */
    public function __construct($receiptNumber, $firstName, $middleName, $lastName, $schoolCode, $schoolName, $studentClass, $returnCode, $returnMessage)
    {
      $this->receiptNumber = $receiptNumber;
      $this->firstName = $firstName;
      $this->middleName = $middleName;
      $this->lastName = $lastName;
      $this->schoolCode = $schoolCode;
      $this->schoolName = $schoolName;
      $this->studentClass = $studentClass;
      $this->returnCode = $returnCode;
      $this->returnMessage = $returnMessage;
    }

}

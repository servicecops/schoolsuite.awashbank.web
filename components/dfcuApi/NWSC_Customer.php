<?php

class NWSC_Customer
{

    /**
     * @var string $CustNo
     * @access public
     */
    public $CustNo = null;

    /**
     * @var string $CustName
     * @access public
     */
    public $CustName = null;

    /**
     * @var string $CustArea
     * @access public
     */
    public $CustArea = null;

    /**
     * @var string $CurrBalance
     * @access public
     */
    public $CurrBalance = null;

    /**
     * @var string $CustError
     * @access public
     */
    public $CustError = null;

    /**
     * @var string $CustPropertyRef
     * @access public
     */
    public $CustPropertyRef = null;

    /**
     * @param string $CustNo
     * @param string $CustName
     * @param string $CustArea
     * @param string $CurrBalance
     * @param string $CustError
     * @param string $CustPropertyRef
     * @access public
     */
    public function __construct($CustNo, $CustName, $CustArea, $CurrBalance, $CustError, $CustPropertyRef)
    {
      $this->CustNo = $CustNo;
      $this->CustName = $CustName;
      $this->CustArea = $CustArea;
      $this->CurrBalance = $CurrBalance;
      $this->CustError = $CustError;
      $this->CustPropertyRef = $CustPropertyRef;
    }

}

<?php

class GetbundlesPeriodsDetail
{

    /**
     * @var int $periodid
     * @access public
     */
    public $periodid = null;

    /**
     * @var string $description
     * @access public
     */
    public $description = null;

    /**
     * @param int $periodid
     * @param string $description
     * @access public
     */
    public function __construct($periodid, $description)
    {
      $this->periodid = $periodid;
      $this->description = $description;
    }

}

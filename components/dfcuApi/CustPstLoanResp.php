<?php

class CustPstLoanResp
{

    /**
     * @var int $success
     * @access public
     */
    public $success = null;

    /**
     * @var string $message
     * @access public
     */
    public $message = null;

    /**
     * @var int $LoanID
     * @access public
     */
    public $LoanID = null;

    /**
     * @var int $AmountRequested
     * @access public
     */
    public $AmountRequested = null;

    /**
     * @var int $Charge
     * @access public
     */
    public $Charge = null;

    /**
     * @var int $TotalAmount
     * @access public
     */
    public $TotalAmount = null;

    /**
     * @param int $success
     * @param string $message
     * @param int $LoanID
     * @param int $AmountRequested
     * @param int $Charge
     * @param int $TotalAmount
     * @access public
     */
    public function __construct($success, $message, $LoanID, $AmountRequested, $Charge, $TotalAmount)
    {
      $this->success = $success;
      $this->message = $message;
      $this->LoanID = $LoanID;
      $this->AmountRequested = $AmountRequested;
      $this->Charge = $Charge;
      $this->TotalAmount = $TotalAmount;
    }

}

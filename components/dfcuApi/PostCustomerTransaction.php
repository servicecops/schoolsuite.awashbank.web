<?php

class PostCustomerTransaction
{

    /**
     * @var Validposttranz[] $posttrandetails
     * @access public
     */
    public $posttrandetails = null;

    /**
     * @param Validposttranz[] $posttrandetails
     * @access public
     */
    public function __construct($posttrandetails)
    {
      $this->posttrandetails = $posttrandetails;
    }

}

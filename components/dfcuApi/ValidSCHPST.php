<?php
namespace app\components\dfcuApi;
class ValidSCHPST
{

    /**
     * @var string $strUtilityCode
     * @access public
     */
    public $strUtilityCode = null;

    /**
     * @var string $strSchoolcode
     * @access public
     */
    public $strSchoolcode = null;

    /**
     * @var string $strstartdate
     * @access public
     */
    public $strstartdate = null;

    /**
     * @var string $strenddate
     * @access public
     */
    public $strenddate = null;

    /**
     * @var string $strVendorCode
     * @access public
     */
    public $strVendorCode = null;

    /**
     * @var string $strVendorPassword
     * @access public
     */
    public $strVendorPassword = null;

    /**
     * @var string $strsignature
     * @access public
     */
    public $strsignature = null;

    /**
     * @param string $strUtilityCode
     * @param string $strSchoolcode
     * @param string $strstartdate
     * @param string $strenddate
     * @param string $strVendorCode
     * @param string $strVendorPassword
     * @param string $strsignature
     * @access public
     */
    public function __construct()
    {
    }

//    public function __construct($strUtilityCode, $strSchoolcode, $strstartdate, $strenddate, $strVendorCode, $strVendorPassword, $strsignature)
//    {
//      $this->strUtilityCode = $strUtilityCode;
//      $this->strSchoolcode = $strSchoolcode;
//      $this->strstartdate = $strstartdate;
//      $this->strenddate = $strenddate;
//      $this->strVendorCode = $strVendorCode;
//      $this->strVendorPassword = $strVendorPassword;
//      $this->strsignature = $strsignature;
//    }

}

<?php

class POSPaymentRegistrationDtlz
{

    /**
     * @var string $amountToBePaid
     * @access public
     */
    public $amountToBePaid = null;

    /**
     * @var string $errorCode
     * @access public
     */
    public $errorCode = null;

    /**
     * @var string $errorMessage
     * @access public
     */
    public $errorMessage = null;

    /**
     * @var string $expiryDate
     * @access public
     */
    public $expiryDate = null;

    /**
     * @var string $paymentRegDate
     * @access public
     */
    public $paymentRegDate = null;

    /**
     * @var string $paymentRegNo
     * @access public
     */
    public $paymentRegNo = null;

    /**
     * @var string $status
     * @access public
     */
    public $status = null;

    /**
     * @var string $taxPayerName
     * @access public
     */
    public $taxPayerName = null;

    /**
     * @var string $tin
     * @access public
     */
    public $tin = null;

    /**
     * @param string $amountToBePaid
     * @param string $errorCode
     * @param string $errorMessage
     * @param string $expiryDate
     * @param string $paymentRegDate
     * @param string $paymentRegNo
     * @param string $status
     * @param string $taxPayerName
     * @param string $tin
     * @access public
     */
    public function __construct($amountToBePaid, $errorCode, $errorMessage, $expiryDate, $paymentRegDate, $paymentRegNo, $status, $taxPayerName, $tin)
    {
      $this->amountToBePaid = $amountToBePaid;
      $this->errorCode = $errorCode;
      $this->errorMessage = $errorMessage;
      $this->expiryDate = $expiryDate;
      $this->paymentRegDate = $paymentRegDate;
      $this->paymentRegNo = $paymentRegNo;
      $this->status = $status;
      $this->taxPayerName = $taxPayerName;
      $this->tin = $tin;
    }

}

<?php

class Accountstatements
{

    /**
     * @var RequeststatusSchpayst $requeststatusschpayst
     * @access public
     */
    public $requeststatusschpayst = null;

    /**
     * @var AccountStatement[] $accountstatement
     * @access public
     */
    public $accountstatement = null;

    /**
     * @param RequeststatusSchpayst $requeststatusschpayst
     * @param AccountStatement[] $accountstatement
     * @access public
     */
    public function __construct($requeststatusschpayst, $accountstatement)
    {
      $this->requeststatusschpayst = $requeststatusschpayst;
      $this->accountstatement = $accountstatement;
    }

}

<?php

/**
 * Class used for export data in PDF Formate.
 *
 * @package EduSec.components
 */

namespace app\components;
use app\modules\studentreport\models\SchoolSocialMedia;
use Mpdf\Mpdf;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

class ExportToPdf extends Component
{
	public function exportData($title='School Pay',$filename='schoolpay Pdf',$html=NULL, $css='web/css/pdf.css')
	{
		$mpdf = new mPDF([], 'A4',0,'',15,15,25,16,4,9,'P');
		$mpdf->curlAllowUnsafeSslRequests = true;
		$org_name='<span style="text-align:center">'.$title.'</div>';

		$mpdf->SetHTMLHeader('<table style="border-bottom:1.6px solid #999998;border-top:hidden;border-left:hidden;border-right:hidden;width:100%;"><tr style="border:hidden"></tr></table>');
		$stylesheet = file_get_contents($css); // external css
		$mpdf->WriteHTML($stylesheet,0);
		$mpdf->showWatermarkImage = true;
		$mpdf->autoLangToFont = true;
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->allow_charset_conversion = false;
		$arr = [
		  'odd' => [
		    'L' => [
		      'content' => $title,
		      'font-size' => 10,
		      'font-style' => 'B',
		      'font-family' => 'serif',
		      'color'=>'#27292b'
		    ],
		    'C' => [
		      'content' => 'Page - {PAGENO}/{nbpg}',
		      'font-size' => 10,
		      'font-style' => 'B',
		      'font-family' => 'serif',
		      'color'=>'#27292b'
		    ],
		    'R' => [
		      'content' => 'Printed @ {DATE j-m-Y}',
		      'font-size' => 10,
		      'font-style' => 'B',
		      'font-family' => 'serif',
		      'color'=>'#27292b'
		    ],
		    'line' => 1,
		  ],
		  'even' => []
		];
		$mpdf->SetFooter($arr);
		$mpdf->WriteHTML('<sethtmlpageheader name="main" page="ALL" value="on" show-this-page="1">');
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename.'.pdf',"I");

	}

	public function exportReciept($options)
	{
		$title=$options['title'] ? $options['title'] : 'Transaction Receipt';
		$filename=$options['filename'] ? $options['filename'] : 'trans_reciept';
		$html=$options['html'] ? $options['html'] :NULL;
		$css='web/css/pdf.css';
		$config = [
			'mode' => 'utf-8',
			'format' => 'A5',
			'orientation' => 'L'
		];
		$mpdf = new mPDF($config);
		$mpdf->curlAllowUnsafeSslRequests = true;


		if (YII_ENV_DEV) {
            $mpdf->debug = true;
            $mpdf->showImageErrors = true;


		}

		$org_name='<span style="text-align:center">'.$title.'</div>';
		$watermark = $options['logo'];
		$mpdf->SetWatermarkImage($watermark, 0.20, array(86, 85), array(56, 50));
		$mpdf->showWatermarkImage = true;
		$mpdf->autoLangToFont = true;
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->allow_charset_conversion = false;
		$mpdf->WriteHTML($html);

		$mpdf->SetFooter("<div style='text-align:left;'>Contact us on email:eschoolsupport@awashbank.com</div>");
		$mpdf->Output($filename.'-'.date('Y-m-d_h:s-a').'.pdf',"I");
	}

	public function exportAgreement($options)
	{
		$title=$options['title'] ? $options['title'] : 'Payment Plan Agreement';
		$filename=$options['filename'] ? $options['filename'] : 'payment_plan_agreement';
		$html=$options['html'] ? $options['html'] :NULL;
		$css='web/css/pdf.css';
		$mpdf = new mPDF([], 'A4', 0, '', 24, 24, 10, 20, 8, 8);
		$mpdf->curlAllowUnsafeSslRequests = true;
		$org_name='<span style="text-align:center">'.$title.'</div>';
		$watermark = $options['logo'];
		$mpdf->SetWatermarkImage($watermark, 0.10, array(90, 93), array(58, 120));
		$mpdf->showWatermarkImage = true;
		$mpdf->autoLangToFont = true;
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->allow_charset_conversion = false;
		$mpdf->WriteHTML($html);
		$mpdf->SetFooter("<div style='text-align:left;'>Contact us on email:eschoolsupport@awashbank.com</div>");
		$mpdf->Output($filename.'-'.date('Y-m-d_h:s-a').'.pdf',"I");
	}

	public function exportCircular($options)
	{
		$title=$options['title'] ? $options['title'] : 'SCHOOL CIRCULAR';
		$filename=$options['filename'] ? $options['filename'] : 'school_circular';
		$html=$options['html'] ? $options['html'] :NULL;
		$css='web/css/pdf.css';
		$mpdf = new mPDF([], 'A4', 0, '', 24, 24, 10, 20, 8, 8);
		$mpdf->curlAllowUnsafeSslRequests = true;
		$org_name='<span style="text-align:center">'.$title.'</div>';
		$watermark = $options['logo'];
		$sm =SchoolSocialMedia::findOne(['school_id'=>Yii::$app->user->identity->school_id]);
		$facebook='';
		$linked='';
		$twitter='';
		if($sm){
			$facebook =$sm['facebook_link'];
			$twitter =$sm['twitter_link'];
			$linked =$sm['linkedin_link'];

		}
		$mpdf->SetWatermarkImage($watermark, 0.10, array(90, 93), array(58, 120));
		$mpdf->showWatermarkImage = true;
		$mpdf->autoLangToFont = true;
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->allow_charset_conversion = false;
		$mpdf->WriteHTML($html);
		$mpdf->SetFooter("<div style='text-align:left;'>Contact us on email:eschoolsupport@awashbank.com</div>
		<div style='text-align:left;'>
: $facebook <br> <i class='fa fa-twitter-square'></i>: $twitter <br/> <i class='fa fa-linkedin-square'></i>: $linked </div>");
		$mpdf->Output($filename.'-'.date('Y-m-d_h:s-a').'.pdf',"I");


	}


	public function exportStatementWithWaterMark($title='Awash eSchool',$filename='eSchool_Bank_Statement_Pdf',$html=NULL, $css='web/css/pdf.css', $watermark=null)
	{
		$config = [
			'mode' => 'utf-8',
			'format' => 'A4',
			'orientation' => 'P'
		];
		$mpdf = new mPDF($config);
		$mpdf->curlAllowUnsafeSslRequests = true;

		$org_image = '';
		$org_add = '';


		$org_name='<span style="text-align:center">'.$title.'</div>';

		$mpdf->SetHTMLHeader('<table style="border-bottom:1.6px solid #999998;border-top:hidden;border-left:hidden;border-right:hidden;width:100%;"><tr style="border:hidden"><td vertical-align="center" style="width:35px;border:hidden" align="left">'.$org_image.'</td><td style="border:hidden;text-align:left;color:#555555;"><b style="font-size:22px;">'.$org_name.'</b><br/><span style="font-size:10.2px">'.$org_add.'</td></tr></table>');
		$stylesheet = file_get_contents($css); // external css
		$mpdf->WriteHTML($stylesheet,0);
		$mpdf->autoLangToFont = true;
		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->allow_charset_conversion = false;
		$mpdf->showImageErrors = true;
		if($watermark) {
			Yii::trace($watermark);
			$mpdf->SetWatermarkImage($watermark, 0.20, array(86, 85), array(56, 50));
			$mpdf->showWatermarkImage = true;
		}

		$arr = [
			'odd' => [
				'L' => [
					'content' => $title,
					'font-size' => 10,
					'font-style' => 'B',
					'font-family' => 'serif',
					'color'=>'#27292b'
				],
				'C' => [
					'content' => 'Page - {PAGENO}/{nbpg}',
					'font-size' => 10,
					'font-style' => 'B',
					'font-family' => 'serif',
					'color'=>'#27292b'
				],
				'R' => [
					'content' => 'Printed @ {DATE j-m-Y}',
					'font-size' => 10,
					'font-style' => 'B',
					'font-family' => 'serif',
					'color'=>'#27292b'
				],
				'line' => 1,
			],
			'even' => []
		];
		$mpdf->SetFooter($arr);
//		$mpdf->WriteHTML('<sethtmlpageheader name="main" page="ALL" value="on" show-this-page="1">');
		$mpdf->WriteHTML($html);
		$mpdf->Output($filename.'.pdf',"I");

	}


}
?>

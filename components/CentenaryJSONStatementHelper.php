<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Maq
 * Date: 2/27/2018
 * Time: 12:21 PM
 */

namespace app\components;


use Yii;
use yii\httpclient\Client;

class CentenaryJSONStatementHelper
{

    public static function getStatement($account, $startDate, $endDate)
    {
        $request = ['account' => trim($account), 'startDate' => $startDate, 'endDate' => $endDate];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $statementUrl = Yii::getAlias('@centeJSONStatementUrl');
        if (!$statementUrl) $statementUrl = 'https://agency.centenarybank.co.ug:9004/billpaytest/rest/Incoming/AccountStatement';

        $req = json_encode($request);
        Yii::trace('Sending ' . $req);
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($statementUrl)
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($req)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        $content = $response->getContent();
//        Yii::trace('Cente json response is ' . $content);

        if (!$response->isOk) {
            return ['returnCode' => 500, 'returnMessage' => 'Could not connect to centenary statements service. Please try again later'];
        }

        return json_decode($content);
    }
}
<?php

namespace app\components;

use \SoapClient;

use Yii;

require_once(Yii::getAlias('@webroot/components/CenteSoapClient.php'));

class CenteStatements
{
    public function requestStatement($accNo, $fromDate, $toDate)
    {
        $options = array();
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        $options['stream_context'] = $context;
        $options['trace'] = 1;

        $client = new SoapClient(Yii::getAlias('@centeStatement'), $options);
        $settlement = new getStatement();
        $settlement->accountNo = $accNo;
        $settlement->FromDate = $fromDate;
        $settlement->ToDate = $toDate;
        $settlement->numberOfRecords = 500;
        $settlement->vCode = Yii::$app->params['vcode'];
        $settlement->vPwd = Yii::$app->params['vpwd'];
        $statement = $client->getStatement($settlement);
        Yii::error('Cente Soap request was ' . $client->__getLastRequest());
        Yii::error('Cente Soap response was ' . $client->__getLastResponse());
        return $statement;
    }
}

?>
<?php
namespace app\components;
use \SoapClient;

class getStatement{
var $FromDate;//dateTime
var $ToDate;//dateTime
var $accountNo;//string
var $numberOfRecords; 
var $vCode;//string
var $vPwd;//string
}
class getStatementResponse{
var $getStatementResult;//AccountStatement
}
class AccountStatement{
var $ReturnCode;//string
var $description;//string
var $acctStatementList;//ArrayOfAcctStatement
}
class ArrayOfAcctStatement{
var $AcctStatement;//AcctStatement
}
class AcctStatement{
var $EffectiveDt;//string
var $BankRefernce;//string
var $Credit;//string
var $Debit;//string
var $Closing;//string
var $Amt;//string
var $description;//string
}
class CenteFees 
 {
 var $soapClient;
 
private static $classmap = array('getStatement'=>'getStatement'
,'getStatementResponse'=>'getStatementResponse'
,'AccountStatement'=>'AccountStatement'
,'ArrayOfAcctStatement'=>'ArrayOfAcctStatement'
,'AcctStatement'=>'AcctStatement'

);

 function __construct($url='https://centefees.centenarybank.co.ug/schoolPayTestWS/eSchool.asmx?wsdl')
 {
  $this->soapClient = new SoapClient($url,array("classmap"=>self::$classmap,"trace" => true,"exceptions" => true));
 }
 
function getStatement($getStatement)
{

$getStatementResponse = $this->soapClient->getStatement($getStatement);
return $getStatementResponse;

}

}


?>
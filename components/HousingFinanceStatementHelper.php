<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Maq
 * Date: 2/27/2018
 * Time: 12:21 PM
 */

namespace app\components;


use Yii;
use yii\httpclient\Client;

class HousingFinanceStatementHelper
{

    public static function getStatement($account, $startDate, $endDate)
    {
        $from_date = date('d-m-Y', strtotime($startDate));
        $to_date = date('d-m-Y', strtotime($endDate));

        $dtd = @date('YmdHis');
        $user = Yii::$app->params['hfbStatementUser'];
        $payload = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
                    <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"
                    >
                        <soapenv:Body>
                            <invokeService xmlns=\"http://listeners.sel.panacea/\">
                                <arg0 xmlns=\"\"><![CDATA[
                                            <RequestMessage><Header><MTI>9200</MTI><PC>100000</PC>
                    <TDT>$dtd</TDT>
                    <SSIC>CBX</SSIC></Header>
                    <Payload>
                    <USERID>$user</USERID>
                    <StmntType>D</StmntType>
                    <Validation>600002</Validation>
                    <CurrencyCode>UGX</CurrencyCode>
                    <AccountNumber>$account</AccountNumber>
                    <FromDate>$from_date</FromDate>
                    <UptoDate>$to_date</UptoDate></Payload></RequestMessage>
                                            ]]>
                    
                                </arg0>
                            </invokeService>
                        </soapenv:Body>
                        </soapenv:Envelope>";

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $statementUrl = Yii::getAlias('@housingFinanceStatementUrl');

        Yii::trace('Sending ' . $payload);
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($statementUrl)
            ->addHeaders(['content-type' => 'text/xml'])
            ->setContent($payload)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

//        Yii::trace($response);

        $content = $response->getContent();

        if (!$response->isOk) {
            return ['returnCode' => 500, 'returnMessage' => 'Could not connect to HFB statements service. Please try again later'];
        }


        $soapResponse = simplexml_load_string($content);

        $reponseBody = $soapResponse->children('http://schemas.xmlsoap.org/soap/envelope/')
            ->Body->children('http://listeners.sel.panacea/')->invokeServiceResponse
            ->children()->return;

        $responseXml = simplexml_load_string($reponseBody);
        $payLoad = $responseXml->children()->Payload->children();
        $errorCode = $payLoad->ErrorCode;
        $errorMsg = $payLoad->ErrorMsg;

        Yii::trace("Error message " . $errorMsg);

        if ($errorCode != 0)
            return ['returnCode' => 500, 'returnMessage' => $errorMsg];

        $reportData = $payLoad->children();

        if (count($reportData) == 0)
            return ['returnCode' => 500, 'returnMessage' => 'No transactions found'];



        return ['returnObject'=>$reportData, 'returnCode'=>0, 'returnMessage'=>'ok'];
    }
}
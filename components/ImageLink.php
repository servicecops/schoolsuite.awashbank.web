<?php

/**
 * Class used for export data in PDF Formate.
 * 
 * @package EduSec.components 
 */

namespace app\components;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use mPDF;
 
class ImageLink  extends Component
{

	public function getImage($img_id){
		$data = 'iVBORw0KGgoAAAANSUhEUgAAABwAAAASCAMAAAB/2U7WAAAABl'
       . 'BMVEUAAAD///+l2Z/dAAAASUlEQVR4XqWQUQoAIAxC2/0vXZDr'
       . 'EX4IJTRkb7lobNUStXsB0jIXIAMSsQnWlsV+wULF4Avk9fLq2r'
       . '8a5HSE35Q3eO2XP1A1wQkZSgETvDtKdQAAAABJRU5ErkJggg==';
		$data = base64_decode($data);

		$im = imagecreatefromstring($data);
		if ($im !== false) {
		    header('Content-Type: image/png');
		    imagepng($im);
		    imagedestroy($im);
		}
		else {
		    echo 'An error occurred.';
		}

	}


	public function exportReciept($options)
	{
		$title=$options['title'] ? $options['title'] : 'Transaction Receipt';
		$filename=$options['filename'] ? $options['filename'] : 'trans_reciept';
		$html=$options['html'] ? $options['html'] :NULL;
		$css='web/css/pdf.css';
		$mpdf = new mPDF('', 'A5-L', 0, '', 9, 9, 4, 4, 8, 8);
		$org_name='<span style="text-align:center">'.$title.'</div>';
		$mpdf->SetWatermarkImage(Yii::getAlias('@web/uploads/icon_4.png'), 0.14, array(90, 90), array(56, 42));
		$mpdf->showWatermarkImage = true;
		$mpdf->WriteHTML($html);
		$mpdf->SetFooter("<div style='text-align:left;'>Contact Awash Eschool on Tel:0200.502.140, email:contactcenter@awashbank.com</div>");
		$mpdf->Output($filename.'-'.date('Y-m-d_h:s-a').'.pdf',"I");
	}


}
?>

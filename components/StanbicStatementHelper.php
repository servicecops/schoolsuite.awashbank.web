<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Maq
 * Date: 2/27/2018
 * Time: 12:21 PM
 */

namespace app\components;


use DateTime;
use Yii;
use yii\httpclient\Client;

class StanbicStatementHelper
{

    public static function getStatement($account, $startDate, $endDate)
    {
        //Reformat deta from yyyy-MM-dd to 19-Feb-18
        $startDate = DateTime::createFromFormat('Y-m-d', $startDate)->format('d-M-y');
        $endDate = DateTime::createFromFormat('Y-m-d', $endDate)->format('d-M-y');

        $request = ['merchant_code' => 'schpay', 'school_code' => trim($account),
            'start_date' => $startDate, 'end_date' => $endDate,
            'tran_reference' => '1'];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $statementUrl = Yii::getAlias('@stanbicStatementUrl');
        if (!$statementUrl) $statementUrl = 'https://196.8.208.145:9443/schoolpay/getStatement.php';

        $req = json_encode($request);
        Yii::trace('Sending ' . $req);
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($statementUrl)
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($req)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        $content = $response->getContent();
        if (!$response->isOk) {
            return ['returnCode' => 500, 'returnMessage' => 'Could not connect to stanbic statements service. Please try again later'];
        }

        return json_decode($content);
    }
}
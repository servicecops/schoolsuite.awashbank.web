<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Maq
 * Date: 2/27/2018
 * Time: 12:21 PM
 */

namespace app\components;


use app\modules\e_learning\models\Teleconferencing;
use BigBlueButton\BigBlueButton;
use Yii;
use yii\httpclient\Client;

class TeleconferingHelper
{

    public static function initialiseBBB()
    {
        $sharedSecret = Yii::$app->params['teleconferencingApiKey'];
        $bbbUrl = Yii::$app->params['teleconferencingAppUrl'];


        $bbb = new BigBlueButton($bbbUrl, $sharedSecret);

       return $bbb;
    }


    public static function isMeetingRunning($meetingID){
        $sharedSecret = Yii::$app->params['teleconferencingApiKey'];
        $bbbUrl = Yii::$app->params['teleconferencingAppUrl'] . '/api/isMeetingRunning/meetingID?=';
        $meetingDetails = Teleconferencing::findOne(['meeting_id'=>$meetingID]);


        $apiName = 'isMeetingRunning';
        $meetingName = 'name='.$meetingDetails->meeting_name;
        $meetingId= 'meetingID='.$meetingID;

        //$out =        createname=Test+Meeting&meetingID=abc123&attendeePW=111222&moderatorPW=333444

        $checksum = sha1($apiName.$meetingName.'&'.$meetingId.$sharedSecret);
        http://yourserver.com/bigbluebutton/api/isMeetingRunning?[parameters]&checksum=[checksum]
        $url=$bbbUrl.$meetingID;
            Yii::trace($url);
        $client = new Client();

        $response = $client->createRequest()
            ->setUrl($url)
            ->send();


    }


}

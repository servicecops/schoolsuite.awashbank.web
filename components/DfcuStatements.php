<?php
namespace app\components;
use app\components\dfcuApi\GetSCHPSTDetails;
use app\components\dfcuApi\Webapi;
use Yii;

class DfcuStatements {
    public function requestStatement($schCode, $fromDate, $toDate){
        $options = array();
        $context = stream_context_create([
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);
        $options['stream_context']= $context;
        $options['trace']= 1;

        $utilityCode = Yii::$app->params['dfcuUtilityCode'];
        $vCode=  Yii::$app->params['dfcuVCode'];
        $vPwd = Yii::$app->params['dfcuVPasswd'];

        $signature = $this->getSignature($utilityCode, $vCode, $vPwd, $schCode, $fromDate, $toDate);
        $settlement = new \app\components\dfcuApi\ValidSCHPST();
        $settlement->strUtilityCode = $utilityCode;
        $settlement->strSchoolcode = $schCode;
        $settlement->strstartdate = $fromDate;
        $settlement->strenddate = $toDate;
        $settlement->strVendorCode = $vCode;
        $settlement->strVendorPassword = $vPwd;
        $settlement->strsignature = $signature;
        $client = new Webapi($options, Yii::getAlias('@dfcuStatement'));
        $client->__setLocation(Yii::getAlias('@dfcuStatement'));

        $getSCHPSTDetailsResponse = $client->GetSCHPSTDetails(new GetSCHPSTDetails($settlement));
        Yii::error('Soap request was ' . $client->__getLastRequest());
        Yii::error('Soap response was ' . $client->__getLastResponse());
        return $getSCHPSTDetailsResponse;
    }

    public function getSignature($utilityCode, $vCode, $vPwd, $schCode, $fromDate, $toDate){
        $pkeyid = openssl_pkey_get_private(file_get_contents(Yii::getAlias('@app/components/dfcuApi/server.key')));

        // compute signature
        $data =  $utilityCode . $schCode . $fromDate . $toDate .$vCode . $vPwd;
        openssl_sign($data, $signature, $pkeyid);

        // free the key from memory
        openssl_free_key($pkeyid);
        $signature = base64_encode($signature);
        return $signature;
    }
}

?>
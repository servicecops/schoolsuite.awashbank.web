<?php

namespace app\components;

use DateTime;
use Yii;
use yii\helpers\Url;
use yii\validators\EmailValidator;

class ToWords
{

    public static function sendEmail($to, $subject, $message)
    {

        try {
            $validator = new EmailValidator();
            if (is_array($to)) {
                foreach ($to as $thisAddress) {
                    if (!$validator->validate($thisAddress, $error)) {
                        Yii::error('Email send error ' . $error);
                        return;
                    }
                }
            } else {
                if (!$validator->validate($to, $error)) {
                    Yii::error('Email send error ' . $error);
                    return;
                }
            }

            $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
            $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

            \Yii::$app->mailer->compose()
                ->setTo($to)
                ->setFrom([$senderAddress => $senderName])
                ->setSubject($subject)
                ->setTextBody($message)
                ->send();
        } catch (\Exception $e) {
            Yii::error('Email send error ' . $e->getTraceAsString());
        }
    }


    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public static function convertNumberToWord($num = false)
    {
        $num = str_replace(array(',', ' ','-'), '', trim($num));
        if (!$num) {
            return false;
        }
        $num = (int)$num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int)(($num_length + 2) / 3);
        $max_length = $levels * 3;
        Yii::trace($num);
        Yii::trace($max_length);
        $num = substr('00' . $num, -$max_length);

        $num_levels = str_split($num, 3);
            Yii::trace($num_levels);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int)($num_levels[$i] / 100);

            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
            $tens = (int)($num_levels[$i] % 100);
            $singles = '';
            if ($tens < 20) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
            } else {
                $tens = (int)($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int)($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . (($levels && ( int )($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }


        return implode(' ', $words);
    }

    public static function validatePhoneNumber($phone)
    {
        try {
            if (!$phone) return null;

            $phone = preg_match('/^0(9)\d{8}$/', $phone) ? "251" . substr($phone, 1) : $phone;
            $phone = preg_match('/^(9)\\d{8}$/', $phone) ? "251" . $phone : $phone;

            if (!preg_match('/^251(9)\\d{8}$/', $phone)) {
                return null;
            }
            return $phone;
        }catch (\Exception $e) {
            Yii::trace($e);
            return null;
        }

    }


    /**
     * Will check if we are on the test environment
     *
     */
    public static function isTestEnvironment() {
        if(!isset(Yii::$app->params['testEnvironment']))
           return false;
        return Yii::$app->params['testEnvironment'];
    }

    public static function createImageLink($imageId)
    {
        return Url::to(['/import/import/image-link2', 'id' => $imageId, 'h' => base64_encode('image' . $imageId)]);
    }

    public static function encrypt($message)
    {
        $secret_key = "schoolpaybank";
        $encrypt_method = "AES-256-CBC";
        $secret_iv = 'schoolpaybank'; // change this to one more secure
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        $output = openssl_encrypt($message, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;

    }

    /**
     * Decrypts (but does not verify) a message
     *
     * @param string $message - ciphertext message
     * @param string $key - encryption key (raw binary expected)
     * @param boolean $encoded - are we expecting an encoded string?
     * @return string
     */
    public static function decrypt($message)
    {
        $secret_key = "schoolpaybank";
        $encrypt_method = "AES-256-CBC";
        $secret_iv = 'schoolpaybank'; // change this to one more secure
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($message), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    public static function userHasSchoolId() {
        return isset(Yii::$app->user)
            && isset(Yii::$app->user->identity)
            && isset(Yii::$app->user->identity->school_id);
    }

    public static function isSchoolUser() {
        return Yii::$app->user->can('own_sch')
            && ToWords::userHasSchoolId();
    }

}


?>

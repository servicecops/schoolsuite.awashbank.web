<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Maq
 * Date: 2/27/2018
 * Time: 12:21 PM
 */

namespace app\components;


use Yii;
use yii\httpclient\Client;

class AwashCbsHelperHelper
{

    public static function validateAccount($account)
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $baseUrl = Yii::$app->params['BIBCbsBaseUrl'];
        $url = $baseUrl . '/queryAccountDetails/' . $account;
        Yii::trace('Connecting to  ' . $url);
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
//            ->setContent($req)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        $content = $response->getContent();
        Yii::trace($content);

        if (!$response->isOk) {
            return ['returnCode' => 500, 'returnMessage' => 'Could not connect to rubikon service. Please try again later'];
        }

        return json_decode($content);
    }

    public static function getStatement($account, $startDate, $endDate)
    {
//        "account_no":"1000010002149", "to_date":"11/20/2009", "from_date":"11/10/2009"
        Yii::trace("From $startDate");
        Yii::trace("To $startDate");
        $request = ['account_no' => trim($account), 'from_date' => $startDate, 'to_date' => $endDate];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);

        $baseUrl = Yii::$app->params['BIBCbsBaseUrl'];
        $url = $baseUrl . '/queryAccountStatement';
        $req = json_encode($request);
        Yii::trace('Connecting to  ' . $url . ' to send ' . $req);


        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($req)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        $content = $response->getContent();

        if (!$response->isOk) {
            return (object) ['responseCode' => 500, 'responseText' => 'Could not connect to BIB statements service. Please try again later. HTTP Code: '. $response->getStatusCode()];
        }


        $json_decode = json_decode($content);
       // Yii::trace($json_decode);
        return $json_decode;
    }
}

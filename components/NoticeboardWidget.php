<?php
namespace app\components;

use app\models\User;
use app\modules\schoolcore\models\Events;
use Yii;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class NoticeboardWidget
{
    public function events() {
        $user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $models = Events::find()->where(['school_id'=>$user->school_id]);

        return $models;
    }


}

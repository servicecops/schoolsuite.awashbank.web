<?php 
namespace app\components;

use yii\captcha\CaptchaAction;

class CustomCaptchaAction extends CaptchaAction{
	public $testLimit = 4;
    public $width = 200;
    public $height = 34;
    public $padding = 1;
	public $backColor = 0xffffff;
    public $foreColor = 0x9ba0a0;
    public $transparent = false;

    protected function generateVerifyCode()
    {
        if ($this->minLength > $this->maxLength) {
            $this->maxLength = $this->minLength;
        }
        if ($this->minLength < 3) {
            $this->minLength = 3;
        }
        if ($this->maxLength > 8) {
            $this->maxLength = 8;
        }
        $length = mt_rand($this->minLength, $this->maxLength);
        $digits = '0123456789';
        $code = '';
        for ($i = 0; $i < $length; ++$i) {
            $code .= $digits[mt_rand(0, 9)];
        }
        return $code;
    }

}
?>
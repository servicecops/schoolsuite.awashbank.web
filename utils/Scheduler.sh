#!/bin/bash

pid="/tmp/schoolsuitescheduler.pid"
#trap "rm -f $pid" SIGSEGV
#trap "rm -f $pid" SIGINT


RUNNING=0
if [ -f $pid ]
then
  PID=`cat $pid`
  ps -eo pid | grep -w $PID >/dev/null 2>&1
  if [ $? -eq 0 ]
  then
      RUNNING=1
      echo "Scheduler already running"
      exit 7;
  fi
fi
if [ $RUNNING -ne 1 ]
then
    PID=$$
    echo $PID > $pid
fi


if [ "$#" -ne 1 ]; then
    echo "Usage is $0 configfile"
    exit 4;
fi
source "$1"
SCRIPT_DIR=$(dirname "$0")
bash "$SCRIPT_DIR/SchedulerDbRoutines.sh" "$1"
# your normal workflow here...
#curl -k "$SCHEDULER_BASE_URL/process-pending-fee-class-associations" >> "$SCHEDULER_LOG_DIR/process-pending-fee-class-associations.log" 2>> "$SCHEDULER_LOG_DIR/process-pending-fee-class-associations.log"
#curl -k "$SCHEDULER_BASE_URL/process-penalties" >> "$SCHEDULER_LOG_DIR/process-penalties.log" 2>> "$SCHEDULER_LOG_DIR/process-penalties.log"
#curl -k "$SCHEDULER_BASE_URL/process-recurring-fees" >> "$SCHEDULER_LOG_DIR/process-recurring-fees.log" 2>> "$SCHEDULER_LOG_DIR/process-recurring-fees.log"
#curl -k "$SCHEDULER_BASE_URL/process-booked-student-fees" >> "$SCHEDULER_LOG_DIR/process-booked-student-fees.log" 2>> "$SCHEDULER_LOG_DIR/process-booked-student-fees.log"
#curl -k "$SCHEDULER_BASE_URL/process-sms-fee" >> "$SCHEDULER_LOG_DIR/process-sms-fee.log" 2>> "$SCHEDULER_LOG_DIR/process-sms-fee.log"
curl -k "$SCHEDULER_BASE_URL/dispatch-messages" >> "$SCHEDULER_LOG_DIR/dispatch-messages.log" 2>> "$SCHEDULER_LOG_DIR/dispatch-messages.log"
curl -k "$SCHEDULER_BASE_URL/dispatch-school-messages" >> "$SCHEDULER_LOG_DIR/dispatch-school-messages.log" 2>> "$SCHEDULER_LOG_DIR/dispatch-school-messages.log"
curl -k "$SCHEDULER_BASE_URL/process-suite-messages" >> "$SCHEDULER_LOG_DIR/process-suite-messages.log" 2>> "$SCHEDULER_LOG_DIR/process-suite-messages.log"
#curl -k "$SCHEDULER_BASE_URL/process-pending-fee-group-associations" >> "$SCHEDULER_LOG_DIR/process-pending-fee-group-associations.log" 2>> "$SCHEDULER_LOG_DIR/process-pending-fee-group-associations.log"

#rm -f $pid # remove pid file just before exiting
exit

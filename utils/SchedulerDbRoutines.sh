#!/bin/bash
#This will run scheduler db routines

if [ "$#" -ne 1 ]; then
    echo "Usage is $0 configfile"
    exit 4;
fi

source "$1"

export PGPASSWORD="$SCHEDULER_DB_PASSWORD"

#Run recurrent fees
if [[ -z "${CLASS_ASSOCIATION_BATCH_SIZE}" ]]; then
  LIMIT_BATCH="1000"
else
  LIMIT_BATCH="${CLASS_ASSOCIATION_BATCH_SIZE}"
fi
psql -h "$SCHEDULER_DB_HOST" -p "$SCHEDULER_DB_PORT" -U "$SCHEDULER_DB_USER" "$SCHEDULER_DB" -c "begin; select * from svc_process_pending_fee_class_associations($LIMIT_BATCH) ; commit; " >> "$SCHEDULER_LOG_DIR/process-pending-fee-class-associations.log" 2>> "$SCHEDULER_LOG_DIR/process-pending-fee-class-associations.log"


#Run penalties fees
if [[ -z "${PENALTY_BATCH_SIZE}" ]]; then
  LIMIT_BATCH="10000"
else
  LIMIT_BATCH="${PENALTY_BATCH_SIZE}"
fi
psql -h "$SCHEDULER_DB_HOST" -p "$SCHEDULER_DB_PORT" -U "$SCHEDULER_DB_USER" "$SCHEDULER_DB" -c "begin; select * from svc_process_pending_penalties($LIMIT_BATCH) ; commit; " >> "$SCHEDULER_LOG_DIR/process-penalties.log" 2>> "$SCHEDULER_LOG_DIR/process-penalties.log"


#Run recurrent fees
if [[ -z "${RECURRENT_FEE_BATCH_SIZE}" ]]; then
  LIMIT_BATCH="10000"
else
  LIMIT_BATCH="${RECURRENT_FEE_BATCH_SIZE}"
fi
psql -h "$SCHEDULER_DB_HOST" -p "$SCHEDULER_DB_PORT" -U "$SCHEDULER_DB_USER" "$SCHEDULER_DB" -c "begin; select * from svc_process_recurrent_fees($LIMIT_BATCH) ; commit; " >> "$SCHEDULER_LOG_DIR/process-recurring-fees.log" 2>> "$SCHEDULER_LOG_DIR/process-recurring-fees.log"



#Run BOOKED fees
if [[ -z "${BOOKED_FEE_BATCH_SIZE}" ]]; then
  LIMIT_BATCH="10000"
else
  LIMIT_BATCH="${BOOKED_FEE_BATCH_SIZE}"
fi
psql -h "$SCHEDULER_DB_HOST" -p "$SCHEDULER_DB_PORT" -U "$SCHEDULER_DB_USER" "$SCHEDULER_DB" -c "begin; select * from svc_process_booked_student_fees($LIMIT_BATCH) ; commit; " >> "$SCHEDULER_LOG_DIR/process-booked-student-fees.log" 2>> "$SCHEDULER_LOG_DIR/process-booked-student-fees.log"


psql -h "$SCHEDULER_DB_HOST" -p "$SCHEDULER_DB_PORT" -U "$SCHEDULER_DB_USER" "$SCHEDULER_DB" -c "begin; select * from process_fee_reminders() ; commit; " >> "$SCHEDULER_LOG_DIR/process-booked-student-fees.log" 2>> "$SCHEDULER_LOG_DIR/process-booked-student-fees.log"


#Run Group fees
if [[ -z "${GROUP_FEE_BATCH_SIZE}" ]]; then
  LIMIT_BATCH="10000"
else
  LIMIT_BATCH="${GROUP_FEE_BATCH_SIZE}"
fi
psql -h "$SCHEDULER_DB_HOST" -p "$SCHEDULER_DB_PORT" -U "$SCHEDULER_DB_USER" "$SCHEDULER_DB" -c "begin; select * from svc_process_pending_fee_group_associations($LIMIT_BATCH) ; commit; " >> "$SCHEDULER_LOG_DIR/process-pending-fee-group-associations.log" 2>> "$SCHEDULER_LOG_DIR/process-pending-fee-group-associations.log"

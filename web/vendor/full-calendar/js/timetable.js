$(document).ready(function($){ 

    var todayDate = new Date(),
         calendar,
         calendarEl = document.getElementById('calendar');

    var hostUrl = location.pathname;
        fetchUrl = hostUrl.replace(/view/gi, "load-time-table-schedules"),
        queryParams = new URLSearchParams(location.search);
        fetchUrl = fetchUrl + '?timetableId=' + queryParams.get('id'),
        deleteUrl = hostUrl.replace(/view/gi, "delete-schedule"),
        postUrl = hostUrl.replace(/view/gi, "create-schedule"),
        updateUrl = hostUrl.replace(/view/gi, "update-schedule"),
        assignColorUrl = hostUrl.replace(/view/gi, "adjust-timetable-colors"),
        newGuestEmailUrl =  hostUrl.replace(new RegExp("/time-table/view", "gi"), "/mailing-list/add-guest-email-addresses"),
        newMailingListUrl = hostUrl.replace(new RegExp("/time-table/view", "gi"), "/mailing-list/add-mailing-list"),
        fetchMailingListUrl = hostUrl.replace(new RegExp("/time-table/view", "gi"), "/mailing-list/email-addresses-for-selected-mailing-list"),
        modifyMailingListUrl = hostUrl.replace(new RegExp("/time-table/view", "gi"), "/mailing-list/modify-mailing-list"),
        createMeetingUrl =  hostUrl.replace(new RegExp("/time-table/view", "gi"), "/time-table-schedule-meeting/create"),
        viewMeetingUrl =   hostUrl.replace(new RegExp("/time-table/view", "gi"), "/time-table-schedule-meeting/view")
    
        var selectedMailingListId = '';


      function handleDatesRender(arg) {
        console.log('viewType:', arg.view.calendar.state.viewType);
      }

    $("#calendar").css("font-size", 14)
    //The popup shows up, but after it pops up the whole page becomes unusable.  
    //Fix for Bootstrap Modal Popup is Grayed Out bug
    $("#schedule-add").appendTo("body");
    $("#schedule-edit").appendTo("body");

      
    function loadEventCalendar(selectedClass = "all-classes"){

        var fetchEventsUrl = fetchUrl + '&&selectedClass=' + selectedClass;

        $.get(fetchEventsUrl, function(data,status){

           if (data.success === true) {
                var response = JSON.parse(data.schedules),
                    schedules = tranformCalendarEvents(response);
                    
                    calendar = new FullCalendar.Calendar(calendarEl, {
                                      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
                                      defaultView: 'dayGridMonth',
                                      handleWindowResize:true,
                                      defaultDate: todayDate,
                                      header: {
                                          left: 'prev,next today',
                                          center: 'title',
                                          right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
                                      },
                                      editable:true,
                                      firstDay: 1,
                                      eventLimit: true, // allow "more" link when too many events
                                      navLinks: true, // can click day/week names to navigate views
                                      selectable: true,
                                      select:function (info) {
                                          var event  = {
                                               id: calendar.getEvents().length + 1,
                                               daysOfWeek: [info.start.getDay()],
                                               startTime: moment(info.start).format('HH:mm'),
                                               endTime: moment(info.end).format('HH:mm')
                                          };
                                          
                                          
                                          $('#schedule-add').modal('show');
                                          var startDate = moment(new Date(info.start)).utcOffset('+03:00').format('YYYY-MM-DDTHH:mm');
                                          $('#start_date').val(startDate)
                                          $('#end_date').val(startDate)
                                         
                                      },
                                      eventClick:function(info){
                                                      
                                            let eventData = info.event;
                                            console.log(eventData)
                                            var title = eventData.title,
                                                start = moment(new Date(eventData.start)).utcOffset('+03:00').format('YYYY-MM-DDTHH:mm'),
                                                end = moment(new Date(eventData.end)).utcOffset('+03:00').format('YYYY-MM-DDTHH:mm'),
                                                id = eventData.id,
                                                description = eventData.extendedProps.description,
                                                selectedTeacher = eventData.extendedProps.selected_teacher,
                                                meetingLink = eventData.extendedProps.meetingLink,
                                                mailingListId = eventData.extendedProps.mailingListId,
                                                location = eventData.extendedProps.location
                                            
                                            //$("#schedule-edit").appendTo("body");
                                            $('#schedule-edit').modal('show')
                                            clearInputFields();
                                            //populate the input fields.
                                            $('#edit-event-title').val(title)
                                            $('#edit-event-id').val(id)
                                            $('#edit-event-description').val(description)
                                            $('#edit-start-date').val(start)
                                            $('#edit-end-date').val(end)
                                            $('#edit-select-teachers').val(selectedTeacher).trigger('change')
                                            $('#event-meeting-link').val(meetingLink)
                                            $('#selected-mailing-list').val(mailingListId).trigger('change')
                                            $('#edit-event-location').val(location)
                                            //craete a new meeting or view meeting link.
                                            //first clear the previous meeting link buttons.
                                            $('.determine-meeting-link').empty();
                                            
                                            if (meetingLink) {

                                                meetingLinkUrl = viewMeetingUrl + '?id=' + meetingLink;  //timetable_schedule_id
                                                var buttonLink = '<a href =' + meetingLinkUrl  + ' class = "btn btn-primary btn-sm">' +
                                                                 '<i class="fa fa-users">Meeting Link</i></a>';
                                                $(".determine-meeting-link").append(buttonLink)
                                                
                                            } else {
                                                meetingLinkUrl = createMeetingUrl + '?id=' + id ;  //timetable_schedule_id
                                                var buttonLink = '<a href =' + meetingLinkUrl  + ' class = "btn btn-primary btn-sm">' +
                                                                 '<i class="fa fa-users">New Meeting</i></a>';
                                                $(".determine-meeting-link").append(buttonLink)
                                            }

                                      },
                                      events: schedules,
                                      eventTextColor: '#FFFFFF',
                                      //eventColor: '#378006'
                                      eventTimeFormat: { // like '14:30:00'
                                        hour: '2-digit',
                                        minute: '2-digit',
                                        //second: '2-digit',
                                        meridiem: true
                                      }
                                  });
                                  //remove the loading screen.
                                 $('#loading-timetable').hide();
                                 calendar.render();
      
                      } else {
                        alert('Failed to load the timetable schedules')
                  } 
         })
    }
  

  var convertUTCDateToLocalDate = function (date) {
      return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(),  date.getHours(), date.getMinutes(), date.getSeconds()));
  }

  //Adding the timetable schedule.
  $("#add-schedule-event").submit(function (event) {
        event.preventDefault();

        var formData = {
            title: $("#event_title").val(),
            starts: $("#start_date").val(),
            ends: $("#end_date").val(),
            description: $("#event_description").val(),
            category: "time", // or allDay
            timetable_id: queryParams.get('id'),
            selected_class: $("#selected_class").val(),
            selected_teacher: $("#selected_teacher").val(),
            school_id: $("#event-school-id").val(),
            timetable_category: queryParams.get('category'),
            recurring_start_date: $("#recurring_start_date").val(),
            recurring_end_date: $("#recurring_end_date").val(),
            recurring_frequency: $("#recurring_frequency").val(),
            average_schedule_duration: $("#event-schedule-duration").val()
        };

        
        if (new Date(formData.recurring_start_date) > new Date(formData.recurring_end_date)) {
            alert("Recurring start date cannot be after the recurring end date")
            return;
        }

        $.post(postUrl, formData, function(response){
            if (response.success) {
               //calendar.fullCalendar('refetchEvents')
               var color = response.message.color,
                   eventId = response.message.id;

                if  (! isEmptyObject(response.message.recurring_event) ) {
                    let repeatEvent = response.message.recurring_event
                    calendar.addEvent({
                        id: eventId,
                        title: formData.title,
                        start: formData.starts,
                        end: formData.ends,
                        allDay: false,
                        backgroundColor: color? color: "",
                        //recurring details.
                        daysOfWeek: repeatEvent.recurring_day_of_week,
                        startTime: getTimeComponent(formData.starts),
                        endTime: getTimeComponent(formData.ends),
                        startRecur: new Date(repeatEvent.start_recurring),
                        endRecur: new Date(repeatEvent.end_recurring),
                        extendedProps:{
                            description:formData.description,
                            selected_teacher: formData.selected_teacher
                        } 
                     })

                } else {
                    
                    calendar.addEvent({
                        id: eventId,
                        title: formData.title,
                        start: formData.starts,
                        end: formData.ends,
                        allDay: false,
                        backgroundColor: color? color: "",
                        extendedProps:{
                          description:formData.description,
                          selected_teacher:formData.selected_teacher
                        }  
                    })
                }
                
                clearInputFields();
                $('#schedule-add').modal('hide');
                //Refetches events from all sources and rerenders them on the screen.
               //calendar.refetchEvents() 
            } else {
               alert(response.message)
            }
        })
  });

  //Deleting the timetable schedule
  $('#delete-schedule-event').click(function(e){
          e.preventDefault();

          let eventId = $("#edit-event-id").val();

          deleteUrl = deleteUrl + '?id='+ eventId;

          if(confirm("Are you sure you want to delete this event?")){
              $.ajax({
                url: deleteUrl,
                type: 'DELETE',
                success: function(response){
                    if (response.success === true) {
                        let eventObject = calendar.getEventById(eventId);
                        eventObject.remove();
                        $('#schedule-edit').modal('hide')
                    }else{
                        alert(response.message)
                    }
                }
            })
          }
      });

  //Edit the timetable schedule..
    $("#edit-schedule-event").submit(function (event) {
      event.preventDefault();

      let eventId = $("#edit-event-id").val();
    
      var formData = {
        schedule_id: eventId,
        title: $("#edit-event-title").val(),
        starts: $("#edit-start-date").val(),
        ends: $("#edit-end-date").val(),
        description: $("#edit-event-description").val(),
        category: "time", // or allDay
        selected_teacher: $("#edit-select-teachers").val(),
        mailing_list_id: $("#selected-mailing-list").val(),
        meeting_link: $("#event-meeting-link").val(),
        location: $("#edit-event-location").val()
      };

      $.ajax({
        url: updateUrl,
        type: 'PUT',
        data: formData,
        success: function(response){
          if (response.success === true) {
            var commentResponse = response.message;
            //update the event object on the calendar.
            let eventObject = calendar.getEventById(eventId);
                eventObject.setProp('title', formData.title) 
                eventObject.setExtendedProp('description', formData.description)
                eventObject.setDates(formData.starts, formData.ends);
            
            if (formData.selected_teacher !== "") {
               eventObject.setExtendedProp('selected_teacher', formData.selected_teacher)
            }

            if (formData.meeting_link !== "") {
                eventObject.setExtendedProp('meetingLink', formData.meeting_link)
            }
            if (formData.mailing_list_id !== "") {
                eventObject.setExtendedProp('mailingListId', formData.mailing_list_id)
            }

            if (formData.location !== "") {
                eventObject.setExtendedProp('location', formData.location)
            }


            $('#schedule-edit').modal('hide');

          }else{  
              alert(response.message)
          }
        }
      })
    });

    // show only the adjust colors container
    $('#show-color-picker').click(function(){
        $('#adjust-color-container').show();
        $('#show-color-picker').hide();
        $('#selected-class-form').hide();
        $('#create-email-list').hide();
        $('#new-email-list').hide();
        $('#hide-color-picker').show();
        $('#email-list-container').hide();
    })

    $('#hide-color-picker').click(function(){
        $('#adjust-color-container').hide();
        $('#hide-color-picker').hide();
        $('#show-color-picker').show();
        $('#selected-class-form').show();
        $('#create-email-list').show();
        $('#calendar-container').show();
    })

    $('#create-email-list').click(function(){
        $('#email-list-container').show();
        $('#selected-class-form').hide();
        $('#calendar-container').hide();
    });
    

    //Adding or updating  the color for selected class
    $("#assign-class-colors").submit(function (event) {
         event.preventDefault();

        var formData = {
           selected_class: $("#selected_class_item").val(),
           selected_color: $("#selected_class_color").val(),
           timetable_id: queryParams.get('id')
        };

        console.log('FormData', formData)

        $.post(assignColorUrl, formData, function(response){
            if (response.success) {

                alert('Assigned the color to class successful')  
            } else {
               alert(response.message)
            }
        })
    });


    //load timetable events  for  only selected class
    $("#selected-class-form").submit(function (event) {
        event.preventDefault();
        var selectedClass = $('#select-calendar-class').val()
        calendar.destroy();
        loadEventCalendar(selectedClass)
    });

    //event meeting attendees.
    $("#add-event-attendees").click(function(){
         $(".event-meeting").show();
         $("#add-event-attendees").hide();
    });

    $("#add-guest-email").click(function(){
       
        var emailListFormData = {
            emailList: $("#new-guest-email").val(),
            timetable_id: queryParams.get('id'),
            school_id: $("#event-school-id").val()
        }
        
        
        $.post(newGuestEmailUrl, emailListFormData, function(response){
            if (response.success) {
               let addedEmailList =  response.insertedEmailList
               $('.recent-added-emails').show();
               addedEmailList.forEach(item=>{
                  $('.recent-added-emails').append( ', ' + item)  
               })

               $('.recent-added-emails').css('color', 'green').fadeOut(5000);

               
            } else {
               //alert(response.message)
               var errorMessage = "Failed to add the guest email addresses"
               $('.recent-added-emails').text(errorMessage).css('color','red').fadeOut(5000);

            }
        })
        
    })


     //Modify the mailing list  (Add/remove guest email addresses)
     $("#event-attendee-list").submit(function (event) {
        event.preventDefault();

       var formData = {
          emailList: $("#selected-event-attendees").val(),
          title: $("#email-list-title").val(),
          timetable_id: queryParams.get('id'),
          school_id: $("#event-school-id").val(),
          mailingListId: selectedMailingListId
       };
       console.log('event-attendee-list', formData)

       if (formData.mailingListId !== "" && formData.emailList.length > 0) {
           
            $.post(modifyMailingListUrl, formData, function(response){
                if (response.success) {
 
                  alert('Modified the mailing list successful')  
                } else {
                  alert(response.message)
                }
            }) 
       }
   });

   // Add the mailing list
    $("#new-mailing-list").submit(function (event) {
        event.preventDefault();

        var formData = {
           id: generateRandomId(5), //virtual
           title: $("#new-mailing-list-title").val(),
           description: $("#new-mailing-list-description").val(),
           timetable_id: queryParams.get('id'),
           school_id: $("#event-school-id").val()
        };
        console.log('event-attendee-list', formData)

        $.post(newMailingListUrl, formData, function(response){
            if (response.success) {
                var mailingListId = response.message;
                console.log('hey mailing', mailingListId)
                var listItem = '<li class="list-group-item mailing-list">' +
                                   '<span>'  + formData.title  +  '</span>&nbsp;&nbsp;&nbsp;' +
                                   '<a href = "javascript:void(0);"><i class="fa fa-edit" data-mail-id="'  + mailingListId +
                                      '" data-mail-title="' + formData.title + 
                                   '">Edit</i></a>' + 
                                '</li>';

                $("ul.mailing-list-box").append(listItem);
            } else {
              alert(response.message)
           }
       })
    });

    //$('.fa-edit').click(function(e){
    $(document).on('click', '.fa-edit', function(e){
        e.preventDefault();
        var mailingListId = $(this).data('mail-id'),
            title = $(this).data('mail-title')

        $("#new-email-list").show();
        $("#create-email-list").hide();
        
        //clear the select box. for guest emails
        $('#selected-event-attendees').val([]).trigger('change');
        var loadUrl = fetchMailingListUrl + '?id=' + mailingListId;

        $.get(loadUrl, function(data,status){ 
            if (data.success === true) {
                var response = JSON.parse(data.message)

                $('.new-mailing-list').hide();
                $('.edit-mailing-list').show();
                $('#email-list-title').val(title)
                selectedMailingListId = mailingListId;

                if (response[0].email_list) {
                    var mailingListData = Array.from(response[0].email_list)
                    //check the selected values.
                    $('#selected-event-attendees').val(mailingListData).trigger('change');
                }
            } else {
                 alert(data.message)
            }
        });
    })

    $('#show-recurring-event').change(function(){
        if($(this).is(':checked')){
            $('.recurring-event-container').show();
        }else{

            $('.recurring-event-container').hide();
        } 
    });


    $("#new-email-list").click(function(){
        $("#new-email-list").hide();
        $("#create-email-list").show();
        //hide these container.
        $("#edit-mailing-list").hide();
        $(".new-mailing-list").show();
        $(".edit-mailing-list").hide();

    })
    

    function generateRandomId(length, an){
        //return Math.random().toString(36).substr(2, 10);
         an = an && an.toLowerCase();
         var str = "",
             i = 0,
           min = an == "a" ? 10 : 0,
           max = an == "n" ? 10 : 62;
           for (; i++ < length;) {
                var r = Math.random() * (max - min) + min << 0;
                str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
           }
           return str;
    }

    function clearInputFields(){
        //clear the add event fields.
        $("#event_title").val('')
        $("#start_date").val('')
        $("#end_date").val('')
        $("#event_description").val('')
        $("#selected_class").val('').trigger('change')
        $("#selected_teacher").val('').trigger('change')
        $("#recurring_start_date").val('')
        $("#recurring_end_date").val('')
        $("#recurring_frequency").val('').trigger('change')

        //clear the edit fields 
        $('#edit-event-title').val('')
        $('#edit-event-id').val('')
        $('#edit-event-description').val('')
        $('#edit-start-date').val('')
        $('#edit-end-date').val('')
        $('#edit-select-teachers').val('').trigger('change')
        $('#event-meeting-link').val('')
        $('#selected-mailing-list').val('').trigger('change')
        $('#edit-event-location').val('')
        //craete a new meeting or view meeting link.
        //first clear the previous meeting link buttons.
        $('.determine-meeting-link').empty();
    }


    function tranformCalendarEvents(response){
        var schedules = [];
        response.forEach(item=>{

            if (! isEmptyObject(item.recurring_event)) {
                  var repeatEvent = item.recurring_event;
                 //cater for recurring events
                 schedules.push({
                    id: item.id,
                    title: item.title,
                    start: item.start,
                    end: item.end,
                    extendedProps:{
                        description: item.description,
                        meetingLink: item.meeting_link,
                        mailingListId: item.mailing_list_id,
                        selected_teacher: item.selected_teacher,
                        location: item.location
                    },
                    backgroundColor: item.color,
                    //recurring details.
                    daysOfWeek: repeatEvent.recurring_day_of_week,
                    startTime: getTimeComponent(item.start),
                    endTime: getTimeComponent(item.end),
                    startRecur: new Date(repeatEvent.start_recurring),
                    endRecur: new Date(repeatEvent.end_recurring)
                })

            } else {
                //no recurring events..
                schedules.push({
                    id: item.id,
                    title: item.title,
                    start: item.start,
                    end: item.end,
                    //description: item.description,
                    extendedProps:{
                        description: item.description,
                        meetingLink: item.meeting_link,
                        mailingListId: item.mailing_list_id,
                        selected_teacher: item.selected_teacher,
                        location: item.location
                    },
                    backgroundColor: item.color
                })
                
            }
        })

        return schedules;
    }

    function isEmptyObject(obj){
        return Object.keys(obj).length == 0;
    }

    function getTimeComponent(dateObj){
        dateObj = new Date(dateObj)
        var hours = dateObj.getHours(),
            minutes = dateObj.getMinutes(),
            currentHours =  ("0" + hours).slice(-2);
            currentMinutes = ("0" + minutes).slice(-2);

        return currentHours + ':' + currentMinutes;
    }


   loadEventCalendar();

});
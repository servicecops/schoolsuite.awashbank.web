import Swiper from 'https://unpkg.com/swiper@7/swiper-bundle.esm.browser.min.js'
var swiper = new Swiper(".swiper-container", {
    pagination: ".swiper-pagination",
    paginationClickable: true,
    parallax: true,
    speed: 600,
    autoplay: 3500,
    loop: true,
    grabCursor: true
});

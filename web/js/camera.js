
Webcam.set({
		width: 320,
        height: 240,
        crop_width: 192,
        crop_height: 200,
        image_format: 'jpeg',
        jpeg_quality: 90,
		});
Webcam.on( 'error', function(err)
    {
        error = true;
        alert("We can't access webcam. Please allow Schoolpay.co.zm access to webcam");
    });
    Webcam.attach( '#my_camera' );

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
        	var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            document.getElementById('my_result').innerHTML = '<img  class="img-rounded" src="'+data_uri+'"/>';
            document.getElementById('imagebank-image_base64').value = raw_image_data;

        } );
    }


    function printPage(){
        printPage()
    }

<?php

/* @var $this yii\web\View */
/* @var $user app\modules\schoolcore\models\CoreStudent */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/student-reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?= $user->getFullname() ?>,

Follow the link below to reset your password:

<?= $resetLink ?>

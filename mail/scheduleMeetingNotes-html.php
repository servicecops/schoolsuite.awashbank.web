<?php
   use kartik\form\ActiveForm;
   use yii\helpers\Html;
   use yii\helpers\Url;
   use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
   use yii\helpers\HtmlPurifier;
   use kartik\select2\Select2;
   use app\models\User;
   use app\modules\planner\models\CoreStaff;
   
   /* @var $this yii\web\View */
   /* @var $model app\models\Classes */

?>

<div class = "letters">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b> Title </b></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? Html::encode($model->title) : "--" ?></div>
        </div>
    </div>
</div><br>

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b>Meeting Day and Time</b></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->datetime) ? Html::encode($model->datetime) : "--" ?></div>
        </div>
    </div>
</div><br>


<div class="row">
    <div class="col-md-6">
        <div class="row ">

           <?php 
               $staffInfo = empty($model->chairperson) ? '' : CoreStaff::findOne($model->chairperson);
               $selectedChairperson = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '--';

           ?>
           <div class="col-lg-6 col-xs-3 profile-label"><b>Chairperson </b> </div>
           <div class="col-lg-6 col-xs-9 profile-text"><?=  $selectedChairperson ?></div>
        </div>
    </div><br>

    <div class="col-md-6">
        <div class="row ">
           <?php 
               $staffInfo = empty($model->secretary) ? '' : CoreStaff::findOne($model->secretary);
               $selectedSecretary = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '--';

           ?>
           <div class="col-lg-6 col-xs-3 profile-label"> Secretary </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?=  $selectedSecretary ?></div>
        </div>
    </div>
</div> <br>

<div class="row">
    <div class="col-md-8">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b>Agenda</b> </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->agenda) ? HtmlPurifier::process($model->agenda) : "--"  ?></div>
        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-8">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b> Discussion Notes </b></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->discussion_notes) ? HtmlPurifier::process($model->discussion_notes) : "--"  ?></div>
        </div>
    </div>
</div>
<br>

<div class = "row">
        <div class = "col-md-8">
             <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-label"><b>Follow Up Actions</b></div>
                <div class="col-lg-6 col-xs-9 profile-text"> --- </div>
            </div>
        </div>
</div><br>


    <div class = "row">
        <div class = "col-md-8">
            <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-label"><b>Attended Meeting</b></div>
                <div class="col-lg-6 col-xs-9 profile-text">
                    <ul class="list-group">
                     <?php
                       if(!empty($attendeeList)){
                            foreach($attendeeList as $attendee){
                                echo  '<li class="list-group-item">' . $attendee . '</li>';
                            }
                        }
                     ?>
                    </ul>
                </div>
            </div>
         </div>
    </div> <br>
    
    <div class = "row">
        <div class = "col-md-8">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-label"><b>Absentee List</b></div>
                <div class="col-lg-6 col-xs-9 profile-text">
                    <ul class="list-group">
                        <?php
                            if(!empty($absenteeList)){
                              foreach($absenteeList as $absentee){
                                echo  '<li class="list-group-item">' . $absentee . '</li>';
                              }
                            }
                       ?>
                    </ul> 
                </div>
            </div> 
        </div>
    </div><br>
    <div class = "row">
        <div class="col-sm-6">
            <div class="row ">
              <div class="col-lg-6 col-xs-3 profile-label"><b>Next Meeting Date</b></div>
              <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->next_meeting_date) ? Html::encode($model->datetime) : "--" ?></div>
            </div>
        </div>
    </div>
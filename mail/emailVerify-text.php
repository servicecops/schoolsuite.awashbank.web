<?php

/* @var $this yii\web\View */
/* @var $user app\modules\schoolcore\models\CoreStudent */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello <?= $user->getFullname() ?>,

Follow the link below to verify your email:

<?= $verifyLink ?>

<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="bg-danger" style="background-color: #a71d2a">
Hello <?= $user->username ?>,

Follow the link below to reset your password:

<?= $resetLink ?>
</div>

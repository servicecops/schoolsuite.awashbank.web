<?php

use app\modules\schoolcore\models\CoreStudent;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\schoolcore\models\CoreStudent */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/student-reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hello <?= Html::encode($user->getFullname()) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>

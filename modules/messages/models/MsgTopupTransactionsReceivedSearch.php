<?php

namespace app\modules\messages\models;

use Yii;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

class MsgTopupTransactionsReceivedSearch extends MsgTopupTransactionsReceived
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_topup_transactions_received';
    }

    /**
     * @inheritdoc
     */
    public $date_from, $date_to;

    public function rules()
    {
        return [
            [['id', 'school_id', 'payment_channel',], 'integer'],
            [['date_from', 'date_to', 'payment_channel', 'channel_trans_id'], 'safe'],
        ];
    }

    public function search($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();
        $query->select(['msg.*', 'pc.channel_name', 'pc.payment_channel_logo', 'sch.school_name',])
            ->from('msg_topup_transactions_received msg')
            ->innerJoin('core_school sch', 'sch.id=msg.school_id')
            ->innerJoin('payment_channels pc', 'pc.id=msg.payment_channel');


        $query->andFilterWhere(['between', 'msg.date_created', $this->date_from, $date_to]);
        $query->andFilterWhere(['msg.channel_trans_id' => $this->channel_trans_id]);
        $query->andFilterWhere(['msg.payment_channel' => $this->payment_channel]);
        $query->andFilterWhere(['msg.school_id' => $this->school_id]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode

        $sort = new Sort([
            'attributes' => [
                'date_created', 'amount', 'channel_name', 'school_name', 'channel_trans_id', 'reciept_number', 'channel_code', 'channel_memo'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('msg.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page]; //Use slave db connection in reporting mode}
    }


    public function getVisibleCols()
    {
        return [
            'id' => ['type' => 'number', 'label' => 'Id'],
            'reciept_number' => ['label' => 'Reciept Number'],
            'date_created' => ['type' => 'date', 'label' => 'Date Created'],
            'student_id' => ['type' => 'number', 'label' => 'Student Id'],
            'institution_id' => ['type' => 'number', 'label' => 'Institution Id'],
            'payment_channel' => ['type' => 'number', 'label' => 'Payment Channel'],
            'channel_trans_id' => ['label' => 'Channel Trans Id'],
            'channel_memo' => ['label' => 'Channel Memo'],
            'amount' => ['type' => 'number', 'label' => 'Amount'],
            'channel_process_date' => ['type' => 'date', 'label' => 'Channel Process Date'],
            'channel_payment_type' => ['label' => 'Channel Payment Type'],
            'channel_depositor_name' => ['label' => 'Channel Depositor Name'],
            'channel_depositor_phone' => ['label' => 'Channel Depositor Phone'],
            'channel_depositor_branch' => ['label' => 'Channel Depositor Branch'],
            'channel_teal_id' => ['label' => 'Channel Teal Id'],
            'reversed' => ['type' => 'number', 'label' => 'Reversed'],
            'date_reversed' => ['type' => 'date', 'label' => 'Date Reversed'],
            'reversal_payment_id' => ['type' => 'number', 'label' => 'Reversal Payment Id'],
            'reason' => ['type' => 'number', 'label' => 'Reason'],
            'reversal' => ['type' => 'number', 'label' => 'Reversal'],
            'payment_reference' => ['label' => 'Payment Reference'],
            'student_name' => ['label' => 'Student Name'],
            'registration_number' => ['label' => 'Registration Number'],
            'actions' => [
                'type' => 'action',
                'links' => [
                    ['url' => 'view', 'title' => 'View', 'icon' => 'fa fa-search'],
                    ['url' => 'update', 'title' => 'Update', 'icon' => 'glyphicon glyphicon-edit']
                ]
            ]
        ];
    }

    public function getSearchCols()
    {
        return [
            'id' => ['type' => 'number', 'label' => 'Id'],
            'reciept_number' => ['label' => 'Reciept Number'],
            'date_created' => ['type' => 'date', 'label' => 'Date Created'],
            'student_id' => ['type' => 'number', 'label' => 'Student Id'],
            'institution_id' => ['type' => 'number', 'label' => 'Institution Id'],
            'payment_channel' => ['type' => 'number', 'label' => 'Payment Channel'],
            'channel_trans_id' => ['label' => 'Channel Trans Id'],
            'channel_memo' => ['label' => 'Channel Memo'],
            'amount' => ['type' => 'number', 'label' => 'Amount'],
            'channel_process_date' => ['type' => 'date', 'label' => 'Channel Process Date'],
            'channel_payment_type' => ['label' => 'Channel Payment Type'],
            'channel_depositor_name' => ['label' => 'Channel Depositor Name'],
            'channel_depositor_phone' => ['label' => 'Channel Depositor Phone'],
            'channel_depositor_branch' => ['label' => 'Channel Depositor Branch'],
            'channel_teal_id' => ['label' => 'Channel Teal Id'],
            'reversed' => ['type' => 'number', 'label' => 'Reversed'],
            'date_reversed' => ['type' => 'date', 'label' => 'Date Reversed'],
            'reversal_payment_id' => ['type' => 'number', 'label' => 'Reversal Payment Id'],
            'reason' => ['type' => 'number', 'label' => 'Reason'],
            'reversal' => ['type' => 'number', 'label' => 'Reversal'],
            'payment_reference' => ['label' => 'Payment Reference'],
            'student_name' => ['label' => 'Student Name'],
            'registration_number' => ['label' => 'Registration Number'],
        ];
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData'],
            'labels'=> ['date_created', 'school_name', 'payment_reference', 'channel_name', 'channel_trans_id', 'reciept_number', 'amount', 'channel_depositor_phone', 'topup_rate', 'number_of_messages'],
            'fileName'=>'transaction_histories',
            'title'=>'NCHE Transaction History',
            'exportFile'=>'/school-account-transaction-history/exportPdfExcel',
        ];

        return $data;
    }

}

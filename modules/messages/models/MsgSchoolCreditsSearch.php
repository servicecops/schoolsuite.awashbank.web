<?php

namespace app\modules\messages\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\messages\models\MsgSchoolCredits;
if (!Yii::$app->session->isActive) session_start();

/**
 * MsgSchoolCreditsSearch represents the model behind the search form about `app\modules\messages\models\MsgSchoolCredits`.
 */
class MsgSchoolCreditsSearch extends MsgSchoolCredits
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'credit_bal'], 'integer'],
            [['school_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        
        $query->select(['sc.id', 'sc.date_created', 'sc.description', 'sc.school_id','sch.school_name', 'sc.credit_bal'])
            ->from('msg_school_credits sc')
            ->innerJoin('core_school sch', 'sch.id=sc.school_id');
        $query->andFilterWhere([
            'school_id' => $this->school_id,
        ]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'description', 'school_name'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('School Credits', $query, $this->getVisibleCols(),  'msg_school_credits');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'School Message Accounts', 'search_col_w'=>[4, 5, 3]];
    }

    public function getVisibleCols()
    {
        return [
            'date_created' => ['type'=>'date', 'label'=>'Date'],
            'description' => ['label'=>'Description'],
            'school_name' => ['label'=>'School'],
            'credit_bal' => ['type'=>'number', 'label'=>'Credit Bal'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search', 'params'=>['id'=>'school_id', 'mod'=>'sch-credits']],
                ]
            ]
        ]; 
    }

    public function getSearchCols()
    {
        return [
            'school_id' => ['type'=>'search', 'label'=>'Search School', 'mode'=>'school', 'width'=>8]
        ]; 
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

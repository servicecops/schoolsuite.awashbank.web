<?php

namespace app\modules\messages\models;

use Yii;

/**
 * This is the model class for table "msg_school_credits_th".
 *
 * @property integer $id
 * @property integer $message_credits_id
 * @property integer $credits_before
 * @property integer $credits
 * @property integer $credits_after
 * @property string $date_created
 */
class MsgSchoolCreditsHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_school_credits_th';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_credits_id', 'credits_before', 'credits', 'credits_after'], 'integer'],
            [['date_created', 'description'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message_credits_id' => 'Message Credits ID',
            'credits_before' => 'Credits Before',
            'credits' => 'Credits',
            'credits_after' => 'Credits After',
            'date_created' => 'Date Created',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Msg School Credits History',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'message_credits_id' => ['type'=>'number', 'label'=>'Message Credits Id'],
                    'credits_before' => ['type'=>'number', 'label'=>'Credits Before'],
                    'credits' => ['type'=>'number', 'label'=>'Credits'],
                    'credits_after' => ['type'=>'number', 'label'=>'Credits After'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'message_credits_id'],
            ['attribute'=>'credits_before'],
            ['attribute'=>'credits'],
            ['attribute'=>'credits_after'],
            ['attribute'=>'date_created'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Msg School Credits History" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

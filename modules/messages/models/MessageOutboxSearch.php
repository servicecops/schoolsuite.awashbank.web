<?php

namespace app\modules\messages\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\messages\models\MessageOutbox;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

/**
 * MessageOutboxSearch represents the model behind the search form about `app\modules\messages\models\MessageOutbox`.
 */
class MessageOutboxSearch extends MessageOutbox
{

    public $date_from, $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['message_text', 'time_generated', 'time_sent', 'recipient_number', 'message_status', 'email_subject', 'email_attachment', 'date_from', 'date_to'], 'safe'],
            [['flash_message', 'email_message'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();
        $query->select(['message_text', 'recipient_number', 'time_generated', 'time_sent', 'message_status'])
            ->from('message_outbox')
            ->andFilterWhere(['message_status'=>$this->message_status]);
            if($this->date_from){
                $query->andWhere(['between', 'time_generated', $this->date_from, $date_to]);            }

            $pages = new Pagination(['totalCount' => $query->count()]);
            $sort = new Sort([
                'attributes' => [
                    'message_text', 'recipient_number', 'time_generated', 'time_sent'
                    ],
            ]);
            ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('time_generated DESC');
            $query->offset($pages->offset)->limit($pages->limit);
            return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];

    }


}

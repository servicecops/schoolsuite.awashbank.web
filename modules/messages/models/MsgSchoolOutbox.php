<?php

namespace app\modules\messages\models;

use Yii;

/**
 * This is the model class for table "msg_school_outbox".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $msg_id
 * @property string $recipient
 * @property string $msg_text
 * @property string $sender_id
 * @property boolean $sent
 * @property integer $school_id
 */
class MsgSchoolOutbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_school_outbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['msg_id', 'school_id'], 'integer'],
            [['sent'], 'boolean'],
            [['recipient', 'msg_text', 'sender_id'], 'string', 'max' => 2044]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'msg_id' => 'Msg ID',
            'recipient' => 'Recipient',
            'msg_text' => 'Msg Text',
            'sent' => 'Sent',
            'school_id' => 'School ID',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Msg School Outbox',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'msg_id' => ['type'=>'number', 'label'=>'Msg Id'],
                    'recipient' => ['label'=>'Recipient'],
                    'msg_text' => ['label'=>'Msg Text'],
                    'sent' => ['type'=>'number', 'label'=>'Sent'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'date_created'],
            ['attribute'=>'msg_id'],
            ['attribute'=>'recipient'],
            ['attribute'=>'msg_text'],
            ['attribute'=>'sent'],
            ['attribute'=>'school_id'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Msg School Outbox" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

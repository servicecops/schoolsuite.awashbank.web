<?php

namespace app\modules\messages\models;

use Yii;

/**
 * This is the model class for table "msg_topup_transactions_received".
 *
 * @property integer $id
 * @property string $reciept_number
 * @property string $date_created
 * @property integer $school_id
 * @property string $schoolpay_receipt_number
 * @property string $last_bank_settlement_message
 * @property integer $settlement_record_id
 * @property integer $payment_channel
 * @property string $channel_trans_id
 * @property string $channel_memo
 * @property integer $amount
 * @property string $channel_process_date
 * @property string $channel_payment_type
 * @property string $channel_depositor_name
 * @property string $channel_depositor_phone
 * @property string $channel_depositor_branch
 * @property string $channel_teal_id
 * @property boolean $reversed
 * @property string $date_reversed
 * @property integer $reversal_payment_id
 * @property boolean $reversal
 * @property string $payment_reference
 * @property boolean $posted_to_bank
 * @property string $bank_settlement_date
 * @property string $bank_settlement_reference
 * @property integer $topup_rate
 * @property integer $number_of_messages
 */
class MsgTopupTransactionsReceived extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_topup_transactions_received';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'channel_process_date', 'date_reversed', 'bank_settlement_date'], 'safe'],
            [['school_id', 'payment_channel', 'channel_trans_id', 'payment_reference', 'topup_rate', 'number_of_messages'], 'required'],
            [['school_id', 'payment_channel', 'amount', 'reversal_payment_id', 'topup_rate', 'number_of_messages'], 'integer'],
            [['schoolpay_receipt_number', 'channel_trans_id', 'channel_memo', 'channel_payment_type', 'channel_depositor_name', 'channel_depositor_phone', 'channel_depositor_branch', 'channel_teal_id', 'payment_reference'], 'string'],
            [['reversed', 'reversal', 'posted_to_bank'], 'boolean'],
            [['last_bank_settlement_message', 'bank_settlement_reference'], 'string', 'max' => 255],
            [['payment_channel', 'channel_trans_id'], 'unique', 'targetAttribute' => ['payment_channel', 'channel_trans_id'], 'message' => 'The combination of Payment Channel and Channel Trans ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reciept_number' => 'Reciept Number',
            'date_created' => 'Date Created',
            'school_id' => 'School ID',
            'schoolpay_receipt_number' => 'Schoolpay Receipt Number',
            'last_bank_settlement_message' => 'Last Bank Settlement Message',
            'settlement_record_id' => 'Settlement Record ID',
            'payment_channel' => 'Payment Channel',
            'channel_trans_id' => 'Channel Trans ID',
            'channel_memo' => 'Channel Memo',
            'amount' => 'Amount',
            'channel_process_date' => 'Channel Process Date',
            'channel_payment_type' => 'Channel Payment Type',
            'channel_depositor_name' => 'Channel Depositor Name',
            'channel_depositor_phone' => 'Channel Depositor Phone',
            'channel_depositor_branch' => 'Channel Depositor Branch',
            'channel_teal_id' => 'Channel Teal ID',
            'reversed' => 'Reversed',
            'date_reversed' => 'Date Reversed',
            'reversal_payment_id' => 'Reversal Payment ID',
            'reversal' => 'Reversal',
            'payment_reference' => 'Payment Reference',
            'posted_to_bank' => 'Posted To Bank',
            'bank_settlement_date' => 'Bank Settlement Date',
            'bank_settlement_reference' => 'Bank Settlement Reference',
            'topup_rate' => 'Topup Rate',
            'number_of_messages' => 'Number Of Messages',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Msg Topup Transactions Received',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'reciept_number' => ['label'=>'Reciept Number'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                    'schoolpay_receipt_number' => ['label'=>'Schoolpay Receipt Number'],
                    'last_bank_settlement_message' => ['label'=>'Last Bank Settlement Message'],
                    'settlement_record_id' => ['type'=>'number', 'label'=>'Settlement Record Id'],
                    'payment_channel' => ['type'=>'number', 'label'=>'Payment Channel'],
                    'channel_trans_id' => ['label'=>'Channel Trans Id'],
                    'channel_memo' => ['label'=>'Channel Memo'],
                    'amount' => ['type'=>'number', 'label'=>'Amount'],
                    'channel_process_date' => ['type'=>'date', 'label'=>'Channel Process Date'],
                    'channel_payment_type' => ['label'=>'Channel Payment Type'],
                    'channel_depositor_name' => ['label'=>'Channel Depositor Name'],
                    'channel_depositor_phone' => ['label'=>'Channel Depositor Phone'],
                    'channel_depositor_branch' => ['label'=>'Channel Depositor Branch'],
                    'channel_teal_id' => ['label'=>'Channel Teal Id'],
                    'reversed' => ['type'=>'number', 'label'=>'Reversed'],
                    'date_reversed' => ['type'=>'date', 'label'=>'Date Reversed'],
                    'reversal_payment_id' => ['type'=>'number', 'label'=>'Reversal Payment Id'],
                    'reversal' => ['type'=>'number', 'label'=>'Reversal'],
                    'payment_reference' => ['label'=>'Payment Reference'],
                    'posted_to_bank' => ['type'=>'number', 'label'=>'Posted To Bank'],
                    'bank_settlement_date' => ['type'=>'date', 'label'=>'Bank Settlement Date'],
                    'bank_settlement_reference' => ['label'=>'Bank Settlement Reference'],
                    'topup_rate' => ['type'=>'number', 'label'=>'Topup Rate'],
                    'number_of_messages' => ['type'=>'number', 'label'=>'Number Of Messages'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'reciept_number'],
            ['attribute'=>'date_created'],
            ['attribute'=>'school_id'],
            ['attribute'=>'schoolpay_receipt_number'],
            ['attribute'=>'last_bank_settlement_message'],
            ['attribute'=>'settlement_record_id'],
            ['attribute'=>'payment_channel'],
            ['attribute'=>'channel_trans_id'],
            ['attribute'=>'channel_memo'],
            ['attribute'=>'amount'],
            ['attribute'=>'channel_process_date'],
            ['attribute'=>'channel_payment_type'],
            ['attribute'=>'channel_depositor_name'],
            ['attribute'=>'channel_depositor_phone'],
            ['attribute'=>'channel_depositor_branch'],
            ['attribute'=>'channel_teal_id'],
            ['attribute'=>'reversed'],
            ['attribute'=>'date_reversed'],
            ['attribute'=>'reversal_payment_id'],
            ['attribute'=>'reversal'],
            ['attribute'=>'payment_reference'],
            ['attribute'=>'posted_to_bank'],
            ['attribute'=>'bank_settlement_date'],
            ['attribute'=>'bank_settlement_reference'],
            ['attribute'=>'topup_rate'],
            ['attribute'=>'number_of_messages'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Msg Topup Transactions Received" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

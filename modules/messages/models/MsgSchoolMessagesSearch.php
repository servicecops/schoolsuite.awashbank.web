<?php

namespace app\modules\messages\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\messages\models\MsgSchoolMessages;
if (!Yii::$app->session->isActive) session_start();

/**
 * MsgSchoolMessagesSearch represents the model behind the search form about `app\modules\messages\models\MsgSchoolMessages`.
 */
class MsgSchoolMessagesSearch extends MsgSchoolMessages
{
    public $date_from, $date_to, $is_error;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'recipient_group'], 'integer'],
            [['id', 'date_from', 'date_to', 'title', 'message_template', 'message_type', 'schedule_date', 'recipients', 'send_to', 'is_error'], 'safe'],
            [['active', 'processed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);

        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;

        $query = new Query();
        $query->select(['msg.id', 'msg.date_created', 'msg.title', 'message_type', 'msg.school_id', 'sch.school_name', 'msg.active', 'processed', 'schedule_date', 'send_to'])
            ->from('msg_school_messages msg')
            ->innerJoin('core_school sch', 'sch.id=msg.school_id')
            ->andFilterWhere(['processed' => $this->processed]);

        if(\app\components\ToWords::isSchoolUser()){
            $query->andWhere(['msg.school_id'=>Yii::$app->user->identity->school_id]);
        } else {
            $query->andFilterWhere(['msg.school_id' => $this->school_id,]);
        }

        $query->andFilterWhere(['between', 'date_created', $this->date_from, $date_to]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'date_created', 'title', 'message_template', 'message_type', 'school_id', 'active', 'processed', 'schedule_date', 'recipient_group', 'recipients', 'send_to'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Msg School Messages', $query, $this->getVisibleCols(),  'msg_school_messages');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'School Messages', 'search_col_w'=>[3, 6, 3]];
    }

    public function searchLogs($params)
    {
        $this->load($params);

        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;

        $query = new Query();
        $query->select(['event_date', 'event_description', 'message_id', 'is_error'])
            ->from('msg_processing_log')
            ->andFilterWhere(['message_id'=>$this->id, 'is_error' => $this->is_error])
            ->andFilterWhere(['>=', 'event_date', $this->date_from])
            ->andFilterWhere(['<=', 'event_date', $this->date_to]);

        $query->andFilterWhere(['between', 'date_created', $this->date_from, $date_to]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('event_date DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Message Logs', $query, $this->getLogCols(),  'msg_school_logs');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getLogCols(), 'searchCols'=>[], 'title'=>'School Messages Logs', 'search_col_w'=>[3, 6, 3]];
    }

    public function getLogCols()
    {
        return [
            'event_date' => ['type'=>'date', 'label'=>'Date'],
            'event_description' => ['label'=>'Log', 'has-error'=>'is_error'],
        ];
    }


    public function getVisibleCols()
    {
        $cols = [
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
            'title' => ['label'=>'Title'],
            'message_type' => ['label'=>'Message Type'],
            'school_name' => ['label'=>'School'],
            'schedule_date' => ['type'=>'date', 'label'=>'Schedule Date'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],
                    ['url'=>'update', 'title'=>'Update', 'icon'=>'glyphicon glyphicon-edit']
                ]
            ]
        ];
        if(Yii::$app->user->identity->school_id){
            unset($cols['school_name']);
        }
        return $cols;
    }

    public function getSearchCols()
    {
        return [
            'date_from' => ['type'=>'date', 'label'=>'From'],
            'date_to' => ['type'=>'date', 'label'=>'To', 'width'=>2],
            'school_id' => ['type'=>'search', 'label'=>'School', 'mode'=>'school'],
            'processed' => ['type'=>'dropdown', 'options'=>[''=>'Select Status', 1=>'Processed', 0=>'Not Processed'], 'width'=>2]
        ]; 
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

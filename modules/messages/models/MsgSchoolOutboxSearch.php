<?php

namespace app\modules\messages\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\messages\models\MsgSchoolOutbox;
if (!Yii::$app->session->isActive) session_start();

/**
 * MsgSchoolOutboxSearch represents the model behind the search form about `app\modules\messages\models\MsgSchoolOutbox`.
 */
class MsgSchoolOutboxSearch extends MsgSchoolOutbox
{
    public $date_from, $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'msg_id', 'school_id'], 'integer'],
            [['date_created', 'recipient', 'msg_text', 'date_from', 'date_to'], 'safe'],
            [['sent'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();
        $query->select(['otb.id', 'otb.date_created', 'sch.school_name', 'recipient', 'msg_text', 'sent', 'school_id'])
            ->from('msg_school_outbox otb')
            ->innerJoin('core_school sch', 'sch.id=otb.school_id')
            ->andFilterWhere(['sent' => $this->sent, 'school_id' => $this->school_id]);
        if(\app\components\ToWords::isSchoolUser()){
            $query->andWhere(['otb.school_id'=>Yii::$app->user->identity->school_id]);
        }

        $query->andFilterWhere(['between', 'otb.date_created', $this->date_from, $date_to]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'recipient', 'msg_text', 'school_name'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Msg School Outbox', $query, $this->getVisibleCols(),  'msg_school_outbox');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Msg School Outbox', 'search_col_w'=>[3, 6, 3]];
    }


    public function getVisibleCols()
    {
        return [
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
            'school_name' => ['label'=>'School'],
            'recipient' => ['label'=>'Recipient'],
            'msg_text' => ['label'=>'Msg Text'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],
                    ['url'=>'update', 'title'=>'Update', 'icon'=>'glyphicon glyphicon-edit']
                ]
            ]
        ];
    }

    public function getSearchCols()
    {
        return [
            'date_from' => ['type'=>'date', 'label'=>'From'],
            'date_to' => ['type'=>'date', 'label'=>'To'],
            'school_id' => ['type'=>'search', 'label'=>'Filter School', 'mode'=>'school'],
        ];
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

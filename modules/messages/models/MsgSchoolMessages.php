<?php

namespace app\modules\messages\models;

use app\components\Helpers;
use app\models\SchoolInformation;
use app\modules\schoolcore\models\CoreSchool;
use Yii;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "msg_school_messages".
 *
 * @property integer $id
 * @property string $date_created
 * @property string $title
 * @property string $message_template
 * @property string $message_type
 * @property integer $school_id
 * @property boolean $active
 * @property boolean $processed
 * @property string $schedule_date
 * @property integer $recipient_group
 * @property string $recipients
 * @property string $send_to
 */
class MsgSchoolMessages extends \yii\db\ActiveRecord
{
    public $send_immediately;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_school_messages';
    }

    public function setDefaultValues()
    {
        if(\app\components\ToWords::isSchoolUser()){
            $this->school_id = Yii::$app->user->identity->school_id;
        }
        if($this->isNewRecord)$this->active = true;
        else $this->recipient_group = explode(', ', $this->recipient_group);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'schedule_date', 'school_id', 'recipient_group'], 'safe'],
            [['title', 'message_template', 'send_immediately', 'message_type', 'school_id'], 'required'],
            [['active', 'processed'], 'boolean'],
            [['title', 'message_template', 'message_type', 'recipients', 'send_to'], 'string', 'max' => 2044],
            ['recipient_group', 'validateTarget', 'message'=>'', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'title' => 'Title',
            'message_template' => 'Message Template',
            'message_type' => 'Message Target',
            'school_id' => 'School',
            'active' => 'Active',
            'send_immediately' => 'When To Send ?',
            'processed' => 'Processed',
            'schedule_date' => 'Schedule Date / Time',
            'recipient_group' => 'Recipient Groups / Classes',
            'recipients' => 'Recipients',
            'send_to' => 'Send To',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->send_immediately=='IMMEDIATELY'){
                $this->schedule_date = date('Y-m-d H:i:s');
            }
            if(is_array($this->recipient_group)){
             $this->recipient_group = implode(', ', $this->recipient_group);
            }
            return true;
        } else {
            return false;
        }
    }


    public function formFields()
    {
        $msg_types = [
            ''=>'Select Target', 'STUDENT_GROUP'=>'Student Group',
            'BY_CLASSES'=>'Student Classes', 'STATIC_RECIPIENTS'=>'Static Recipients'];
        $fields =  [
            [
                'heading'=>'New School Message',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'school_id' => ['type'=>'search', 'label'=>'School', 'mode'=>'school'],
                    'title' => ['label'=>'Title'],
                    'message_type' => ['type'=>'dropdown', 'options'=>$msg_types],
                    'send_to' => ['type'=>'dropdown', 'options'=>[''=>'Select Group', 'GUARDIAN'=>'Parents', 'STUDENT'=>'Students']],
                    'recipient_group' => ['type'=>'select', 'label'=>'Recipient Groups / Classes', 'data'=>$this->selectData, 'multiple'=>true, 'same_div'=>'first'],
                    'recipients' => ['type'=>'textarea', 'label'=>'Enter Recipients Phone Numbers ( comma separated )', 'same_div'=>'last'],
                    'send_immediately' => ['type'=>'dropdown',  'options'=>[''=>'-- Select --', 'IMMEDIATELY'=>'Send Immediately', 'SCHEDULE'=>'Schedule date']],
                    'schedule_date' => ['type'=>'datetime', 'label'=>'Schedule Date'],
                ]
            ]
        ];

        if(\app\components\ToWords::isSchoolUser()) unset($fields[0]['fields']['school_id']);
        return $fields;

    }

    public function getSelectData()
    {
        $data = [];
        if($this->message_type == 'BY_CLASSES'){
            $data= Helpers::ClassArray($this->school_id);
        } else if($this->message_type =='STUDENT_GROUP'){
            $data = Helpers::GroupArray($this->school_id);
        }
        return $data;
    }

    public function getViewFields()
    {
        $fields =  [
            ['attribute'=>'title'],
            ['attribute'=>'message_template'],
            ['attribute'=>'message_type', 'value'=>$this->messageType],
            ['label'=>'School', 'value'=>$this->school->school_name],
            ['attribute'=>'processed', 'value'=> $this->processed ? 'YES' : 'NO'],
            ['attribute'=>'schedule_date'],
        ];

        if($this->message_type =='STATIC_RECIPIENTS') $fields[] =['attribute'=>'recipients'];
        else $fields[] =['attribute'=>'recipient_group', 'value'=>$this->recipientGroup];
        if($this->message_type !='STATIC_RECIPIENTS') $fields[] =['attribute'=>'send_to'];
        $fields[] =[
            'label'=>'Message Logs',
            'value'=>'View Message logs',
            'contentOptions'=>['class'=>'modal_link',],
            'options'=>[
                'class'=>'modal_link',
                'options'=>['class'=>'modal_link']
            ],
//            'value'=>"<a class='modal_link' href='javascript:;' data-href='".Url::to(['/messages/default/logs', 'id' => $this->id])."' data-title='Message Logs'>View message Logs</a>",
            'format'=>'html',
        ];
        return $fields;
    }

    public function getSchool()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getRecipientGroup()
    {
        $recipient = '';
        if($this->message_type== 'BY_CLASSES'){
            $classes = explode(', ', $this->recipient_group);
            $classes = array_filter($classes);
            if(count($classes) > 0){
                $q = (new Query())->select('class_code')->from('core_school_class')
                    ->where(['in', 'id', $classes])->all();
                $q = array_column($q, 'class_code');
                $recipient = implode(', ', $q);
            }
        } else if($this->message_type== 'STUDENT_GROUP'){
            $groups = explode(', ', $this->recipient_group);
            $groups = array_filter($groups);
            if(count($groups) > 0){
                $q = (new Query())->select('group_name')->from('student_group_information')
                    ->where(['in', 'id', $groups])->all();
                $q = array_column($q, 'group_name');
                $recipient = implode(', ', $q);
            }
        }
        return $recipient;
    }

    public function getMessageType()
    {
        if($this->message_type == 'BY_CLASSES') return "Student Class";
        else if($this->message_type == 'STUDENT_GROUP') return "Student Group";
        else return $this->message_type;
    }
    public  function getTitle()
    {
        return $this->isNewRecord ? "School Messages" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->title;
    }

    public function getPlaceholders()
    {
        $query = (new Query())->from('place_holders')->orderBy('type, name')->all();
        return $query;
    }

    public function validateTarget($attribute,$params)
    {

        $arr = ['BY_CLASSES', 'STUDENT_GROUP'];
        if(in_array($this->message_type, $arr) && !$this->recipient_group) {
            $this->addError($attribute, 'Select Recipients group/class');
        }
    }

}

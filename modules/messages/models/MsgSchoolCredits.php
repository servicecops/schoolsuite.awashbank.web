<?php

namespace app\modules\messages\models;

use app\models\SchoolInformation;
use app\modules\schoolcore\models\CoreSchool;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "msg_school_credits".
 *
 * @property integer $id
 * @property string $date_created
 * @property string $description
 * @property integer $school_id
 * @property integer $credit_bal
 */
class MsgSchoolCredits extends \yii\db\ActiveRecord
{
    public $top_up, $rate_per_message, $top_up_amount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg_school_credits';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'top_up', 'rate_per_message', 'top_up_amount'], 'safe'],
            [['rate_per_message', 'top_up_amount'], 'required', 'on'=>'top_up'],
            [['school_id'], 'unique'],
            [['school_id', 'credit_bal', 'top_up_amount', 'rate_per_message'], 'integer'],
            [['description'], 'string', 'max' => 2044]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['top-up'] = ['top_up', 'rate_per_message', 'credit_bal'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'description' => 'Description',
            'rate_per_message' => 'Rate Per Message (Birr)',
            'school_id' => 'School ID',
            'credit_bal' => 'Credit Bal',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Message School Credits',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'school_id' => ['type'=>'search', 'label'=>'School', 'mode'=>'school'],
                    'description' => ['label'=>'Description'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['label'=>'date_created', 'value'=>date('M d, Y', strtotime($this->date_created))],
            ['attribute'=>'description'],
            ['label'=>'School', 'value'=>$this->school->school_name],
            ['attribute'=>'credit_bal', 'value'=>"<code style='font-size:16px;'>".number_format($this->credit_bal)."</code>", 'format'=>'html'],
        ];
    }

    public function getViewActions()
    {
        $action = [
            ['link'=>'javascript:;', 'title'=>'<i class="fa fa-plus"></i>&nbsp;&nbsp;Credit Top-Up ',
                'htmlOptions'=>[
                    'class'=>'modal_link btn  btn-info btn-sm',
                    'data-href'=>Url::to(['top-up', 'mod' => 'sch-credits', 'id'=>$this->school_id]),
                    'data-title'=>$this->school->school_name." - Credit Top up",
                ]],
        ];

        return Yii::$app->user->can('w_msg_cr') ? $action : [];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "School Message A/C" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->school->school_name. " - Message A/C";
    }

    public function getCreditsHistory()
    {
        return (new MsgSchoolCreditsHistorySearch())->search(['MsgSchoolCreditsHistorySearch'=>['message_credits_id'=>$this->id]]);
    }

    public function getSchool()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

}

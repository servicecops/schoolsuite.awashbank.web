<?php

namespace app\modules\messages\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\messages\models\MsgSchoolCreditsHistory;
if (!Yii::$app->session->isActive) session_start();

/**
 * MsgSchoolCreditsHistorySearch represents the model behind the search form about `app\modules\messages\models\MsgSchoolCreditsHistory`.
 */
class MsgSchoolCreditsHistorySearch extends MsgSchoolCreditsHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'message_credits_id', 'credits_before', 'credits', 'credits_after'], 'integer'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        
        $query->select(['id', 'message_credits_id', 'description', 'credits_before', 'credits', 'credits_after', 'date_created'])
            ->from('msg_school_credits_th');
        $query->andFilterWhere([
            'message_credits_id' => $this->message_credits_id,
        ]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'message_credits_id', 'credits_before', 'credits', 'credits_after', 'date_created'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Msg School Credits History', $query, $this->getVisibleCols(),  'msg_school_credits_th');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Msg School Credits History', 'search_col_w'=>[2, 7, 3]];
    }


    public function getVisibleCols()
    {
        return [
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
            'description' => ['label'=>'Description'],
            'credits' => ['type'=>'number', 'label'=>'Credits'],
            'credits_before' => ['type'=>'number', 'label'=>'Credits Before'],
            'credits_after' => ['type'=>'number', 'label'=>'Credits After'],
        ]; 
    }

    public function getSearchCols()
    {
        return [
            'id' => ['type'=>'number', 'label'=>'Id'],
            'message_credits_id' => ['type'=>'number', 'label'=>'Message Credits Id'],
            'credits_before' => ['type'=>'number', 'label'=>'Credits Before'],
            'credits' => ['type'=>'number', 'label'=>'Credits'],
            'credits_after' => ['type'=>'number', 'label'=>'Credits After'],
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
        ]; 
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

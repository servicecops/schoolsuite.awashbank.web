<?php

namespace app\modules\messages;

class messages extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\messages\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

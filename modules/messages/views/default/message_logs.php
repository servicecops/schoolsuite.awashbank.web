<?php

use yii\helpers\Html;

$template = '@app/views/common/_table';
//$data['striped'] = false;
?>
    <div style="color:#F7F7F7">Message logs</div>

<div>

    <div class="row">
    <div class="col-xs-12 no-padding">
        <ul class='menu-list pull-right' style="padding-top:10px; padding-bottom:0px;">
            <li>
                <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; EXPORT PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-xs', 'target'=>'_blank']); ?>
            </li>
            <li>
                <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXPORT EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-xs', 'target'=>'_blank']); ?>
            </li>
        </ul>
    </div>
    </div>
    <?= $this->render($template, ['searchModel' => $searchModel,'data' => $data, 'mod'=>'messages']); ?>
</div>

<?php
$pdf = Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']);
$excel = Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']);
$script = <<< JS
$("document").ready(function(){
    var height = screen.height * 0.6;
    $("#modal_table").attr('style', 'height:'+height+"px;overflow:auto;");
    $("ul.pagination").attr('data-for', 'modal_table');
});
JS;
$this->registerJs($script);
?>

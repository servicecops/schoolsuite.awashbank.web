<?php

use app\models\NCHEInstitutions;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>
<div style="color:#F7F7F7">Student Information</div>

<div class="row hd-title" data-title="Message Topup Payments Processed">

    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('successAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('errorAlert')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='errorAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('errorAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="container bg-white">


            <?php $form = ActiveForm::begin([
                'action' => ['/messages/default/topup-payments'],
                'method' => 'get',
                'options' => ['class' => 'formprocess']
            ]); ?>
        <div class="row text-center" style="margin-top:20px">

            <div class="col-md-1 ">
                <?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></div>
            <div class="col-sm-2 "><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_to',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></div>

            <div class="col-md-3 ">
                <?= $this->render('@app/views/reusables/school_search',
                    ['form' => $form,
                        'model' => $searchModel,
                        'modelAttribute' => 'school_id',
                        'label' => false,
                        'size' => Select2::SMALL,
                        'inputId' => '_selected_school_id', //ID of component
                        'placeHolder' => 'Search School',
                        'showLogoAfterSelect' => false,
                    ]
                );
                ?>
            </div>

            <div class="col-md-2 ">
                <?= $this->render('@app/views/reusables/payment_channel_search',
                    ['form' => $form,
                        'model' => $searchModel,
                        'modelAttribute' => 'payment_channel',
                        'label' => false,
                        'size' => Select2::SMALL,
                        'placeHolder' => 'Payment channel',
                        'minimumInputLength' => 0,
                    ]
                ); ?>
            </div>


            <div class="col-md-2 "><?= $form->field($searchModel, 'channel_trans_id', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Trans ID'])->label(false) ?></div>
            <div class="col-md-2 "><?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-1 no-padding" style="padding-top:10px !important;">
        <div class="pull-right">
            <div class="menu-list no-padding">

               <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-pexcel', 'model' => get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target' => '_blank']) ?>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12">
    <div style="padding: 2px; font-size: 90%">
        <i class="fa fa-hourglass-half icon-pending-color"> Processing</i>&nbsp;
        <i class="fa fa-check-circle icon-success-color"> Completed</i>&nbsp;
        <!--            <span class="pull-right>">-->
        <!--                <i class="glyphicon glyphicon-print">Receipt</i>&nbsp;-->
        <!--                <i class="fa fa-arrow-circle-o-right"> Details</i>&nbsp;-->
        <!--            </span>-->
    </div>

    <div class="box-body table table-responsive no-padding">
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th class='clink'><?= $sort->link('date_created', ['label' => 'Date']) ?></th>
                <th class='clink'>Reference</th>
                <th class='clink'>School</th>
                <th class='clink'><?= $sort->link('channel_trans_id', ['label' => 'Ch Trans Id']) ?></th>
                <th class='clink'>Phone</th>
                <th class='clink'><?= $sort->link('reciept_number', ['label' => 'Receipt No.']) ?></th>
                <th class='clink'><?= $sort->link('channel_code', ['label' => 'Channel']) ?></th>
                <th class='clink'><?= $sort->link('channel_memo', ['label' => 'Ch. Info']) ?></th>
                <th class='clink'><?= $sort->link('amount') ?></th>
                <th class='clink'>Rate</th>
                <th class='clink'>Number of Messages</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">
                        <td><?= ($v['posted_to_bank']) ? '<i class="fa fa-check-circle icon-success-color"></i>' : '<i class="fa fa-hourglass-half icon-pending-color"></i>' ?></td>
                        <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['payment_reference']) ? $v['payment_reference'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_depositor_phone']) ? $v['channel_depositor_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['schoolpay_receipt_number']) ? $v['schoolpay_receipt_number'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_name']) ? '<img src="' . Url::to(['/import/import/image-link', 'id' => $v['payment_channel_logo']]) . '" width="30px" /> ' . $v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_memo']) ? $v['channel_memo'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['amount']) ? number_format($v['amount']) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['topup_rate']) ? number_format($v['topup_rate']) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['number_of_messages']) ? number_format($v['number_of_messages']) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['id']) ? Html::a('<span class="pull-right"><i class="glyphicon glyphicon-print"></i>&nbsp;&nbsp;&nbsp;</span>', Url::to(['/site/ms', 'i' => base64_encode($v['id'])]), ['target' => '_blank']) : ''; ?></td>

                    </tr>
                <?php endforeach;
            else : ?>
                <td colspan="14">No Record found</td>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?= LinkPager::widget([
        'pagination' => $pages['pages'],
    ]); ?>

</div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

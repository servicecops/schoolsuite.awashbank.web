<?php

use yii\helpers\Url;

?>
<div style="color:#F7F7F7">Student Information</div>

<div class="hd-title" data-title="<?= $model->title; ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="letter">
                <div class="row">
                    <div class="col-md-7"><h3><?= $model->title; ?></h3></div>
                    <?php if (\app\components\ToWords::isSchoolUser()): ?>
                        <div class="col-md-5" style="padding-top:27px;">
                    <span class="pull-right">
                        <a class="aclink btn  btn-info btn-sm"
                           href="<?= Url::to(['/messages/e/sch-messages/create']); ?>"><i class="fa fa-plus"></i>&nbsp;&nbsp; Add New</a>
                        <a class="aclink btn btn-primary btn-sm"
                           href="<?= Url::to(['/messages/e/sch-messages/update', 'id' => $model->id]); ?>"><i
                                    class="fa fa-edit"></i> Update</a>
                        <a class="btn btn-danger btn-sm"
                           href="<?= Url::to(['/messages/e/sch-messages/delete', 'id' => $model->id]); ?>"
                           data-confirm="Are you sure you want to delete this item?" data-method="post"><i
                                    class="fa fa-remove"></i> Delete</a>
                    </span>
                        </div>
                    <?php endif; ?>
                </div>
                <hr class="l_header">
                <table id="w0" class="table  detail-view">
                    <tbody>
                    <tr>
                        <th>Title</th>
                        <td><?= $model->title ?></td>
                    </tr>
                    <tr>
                        <th>Message Template</th>
                        <td> <?= $model->message_template ?> </td>
                    </tr>
                    <tr>
                        <th>Message Target</th>
                        <td><?= $model->messageType ?></td>
                    </tr>
                    <tr>
                        <th>School</th>
                        <td><?= $model->school->school_name ?></td>
                    </tr>
                    <tr>
                        <th>Processed</th>
                        <td><?= $model->processed ? 'YES' : 'NO' ?></td>
                    </tr>
                    <?php if ($model->processed) : ?>
                        <tr>
                            <th>No of Processed Messages</th>
                            <td><?= number_format($model->number_of_processed_messages) ?></td>
                        </tr>
                    <?php endif; ?>
                    <?php if ($model->message_type == 'STATIC_RECIPIENTS') : ?>
                        <tr>
                            <th>Recipients</th>
                            <td><?= $model->recipients ?></td>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <th>Send To</th>
                            <td><?= $model->send_to ?></td>
                        </tr>
                        <tr>
                            <th>Recipient Groups / Classes</th>
                            <td><?= $model->recipientGroup ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <th>Schedule Date / Time</th>
                        <td><?= date('Y-m-d h:i A', strtotime($model->schedule_date)) ?></td>
                    </tr>
                    <tr>
                        <th>Message Logs</th>
                        <td>
                            <a class='modal_link' href='javascript:;'
                               data-href="<?= Url::to(['/messages/default/logs', 'id' => $model->id]); ?>"
                               data-title='Message Logs'>View message Logs</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

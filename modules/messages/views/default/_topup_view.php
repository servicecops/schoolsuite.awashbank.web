<?php

use app\models\Schoodivnformation;
use kartik\select2\Select2;
use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


//Get the school
$mySchool = \app\modules\schoolcore\models\CoreSchool::findOne(['id' => Yii::$app->user->identity->school_id]);
if (!isset($paymentModel)) {
    $paymentModel = new DynamicModel(['payment_channel', 'phone_number', 'amount']);
    $paymentModel->addRule(['payment_channel', 'phone_number', 'amount'], 'required');
}




?>
<div style="color:#F7F7F7">Student Information</div>

<div class="alert">
    <strong><span style="color: #00a65a">NOTICE:</span></strong> You can topup your credits directly
    using Mobile Money<br>

    <div>
        <?php $form = ActiveForm::begin([
            'action' => ['/messages/default/request-topup-payments'],
            'method' => 'post',
        ]); ?>
        <div class="row">
            
                <div class="col-md-3 ">
                    <?= $this->render('@app/views/reusables/payment_channel_search',
                        ['form' => $form,
                            'model' => $paymentModel,
                            'modelAttribute' => 'payment_channel',
                            'label' => 'Select channel',
                            'size' => Select2::MEDIUM,
                            'placeHolder' => 'Payment channel',
                            'minimumInputLength' => 0,
                            'channelCodes' => 'MTN_UG,AIRTEL_MONEY_UG',
                            'showLogoAfterSelect' => true,
                        ]
                    ); ?>
                </div>

                <div class="col-md-3 ">
                    <?= $form->field($paymentModel, 'phone_number')->textInput(['placeHolder' => 'Phone Number'])->label('Phone Number') ?>
                </div>

                <div class="col-md-3 ">
                    <?= $form->field($paymentModel, 'amount')->textInput(['placeHolder' => 'Amount', 'type' => 'number'])->label('Amount') ?>
                </div>
                <div class="col-md-3 ">
                    <div class="container">
                        <br>
                        <?= Html::submitButton('PAY & TOPUP', ['class' => 'btn btn-info btn-md']) ?>
                    </div>
                </div>
         
            <?php if(isset($topupError)): ?>
                <div class="col-md-12 " style="color: #9e0505">
                    <?= $topupError ?>
                </div>
            <?php endif; ?>

        </div>

        <?php ActiveForm::end(); ?>
    </div>

    Or make a payment using schoolpay on MTN Mobile Money or Airtel Money using <strong><span
            style="color: #6b91fb">MESSAGES<?= $mySchool->school_code ?></span></strong> as the
    payment code.
</div>

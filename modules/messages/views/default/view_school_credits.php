<?php

$data = $model->creditsHistory;
$data['striped'] = false;
?>

<div style="color:#F7F7F7">Messages Information</div>

<?= $this->render('@app/views/common/view', ['model' => $model, 'mod' => 'sch-credits']); ?>
<div>
    <div class="row">
        <div class="col-md-12">
            <div class="letter">
                <h3>Credit History</h3>
                <hr class="l_header">


                <?= $this->render('@app/views/common/_table', ['data' => $data, 'mod' => 'loan-transaction-history', 'striped' => false]) ?>
            </div>
        </div>
    </div>

<?php

use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$template = '@app/views/common/_form';
$this->title = $model->title;
?>

<div class="row" data-title="<?= Html::encode($this->title) ?>">
    <div class="letter">
        <div class="row">
            <div class="sch-messages-form col">

                <?php $form = ActiveForm::begin(['id' => 'message_form']);?>
                <div class="col-md-12">
               <?php echo $this->render($template, ['model' => $model, 'form' => $form
                ]) ?>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div style="font-weight: bold;padding-bottom:5px;">Placeholders</div>
                        <div class="form-control" style="height:117px; padding:0px; overflow: auto">
                            <?php foreach ($model->placeholders as $k => $v) : ?>
                                <div class="message_placeholder">
                                    <a class="msg_placeholder" data-placeholder="<?= $v['key'] ?>"
                                       href="javascript:;"><?= $v['name'] ?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'message_template')->textarea(['rows' => 5]); ?>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-9">
                        Preview: <span id="MessagePreview"></span>
                    </div>
                </div>

                <div class="col-md-12 form-group">
                    <br>
                    <?= Html::submitButton($model->isNewRecord ? '<i class="fa fa-plus"></i> Create Message' : 'Edit Message', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

<?php
$classesUrl = Url::to(['/messages/e/sch-messages/classes']);
$groupsUrl = Url::to(['/messages/e/sch-messages/groups']);
$recipient_group = json_encode($model->recipient_group);
$school = $model->school_id;
$msg_type = $model->message_type;
$schoolModel = CoreSchool::findOne(['id' => $school]);
$schoolName = $schoolModel ? addslashes($schoolModel->school_name) : '';

$script = <<< JS
$("document").ready(function(){
    var selectedSch = '$school';
    var msg_type = '$msg_type'; // STUDENT_GROUP, BY_CLASSES, STATIC_RECIPIENTS
    var sel_data = '$recipient_group';
    
    var schoolIdComponent = $('#search_school_id');
    
    var messageRecipientsComponent = $('.form-group.field-msgschoolmessages-recipients');
    var messageRecipientsGroupComponent = $('.form-group.field-recipient_group_selector');
    var sendToComponent = $('.form-group.field-msgschoolmessages-send_to');
    var messageTypeComponent = $('select#msgschoolmessages-message_type');
    
    var sendImmediatelyComponent = $('#msgschoolmessages-send_immediately');
    var scheduleDateComponent = $('.form-group.field-msgschoolmessages-schedule_date');
    var scheduleDateInput = $('#msgschoolmessages-schedule_date');
    
    var recipientGroupSelector = $('#recipient_group_selector');
    
    var loadRecipientGroupList = function() {
        selectedSch = '$school';
        //evaluate the school
        if(schoolIdComponent && schoolIdComponent.val()) {
            selectedSch = schoolIdComponent.val();
        }
        
        //Fetch options
        if(msg_type === 'STUDENT_GROUP'){
           $.get('$groupsUrl', {id : selectedSch}, function(data) {
                appendOptions(data);
           });
        } else if(msg_type === 'BY_CLASSES'){
            $.get('$classesUrl', {id : selectedSch}, function(data) {
                appendOptions(data);
            });
        }
    }
    
    //This will be called the the recipient type is changed
    var recipientTypeChanged = function() {
        msg_type = messageTypeComponent.val() || msg_type;
        
        if(!selectedSch || !msg_type){
        messageRecipientsGroupComponent.hide();
        messageRecipientsComponent.hide();
        sendToComponent.hide();
    } else if(selectedSch && msg_type){
          loadRecipientGroupList();
        }
    }
    
    var refreshMessageRecipientComponents = function() {
        msg_type = messageTypeComponent.val() || '$msg_type';
        if(!msg_type){
             messageRecipientsGroupComponent.hide();
             messageRecipientsComponent.hide();
            sendToComponent.hide();
         }
        else if((msg_type  === 'STUDENT_GROUP') || (msg_type  === 'BY_CLASSES')){
        messageRecipientsComponent.hide();
        messageRecipientsGroupComponent.show();
        sendToComponent.show();
        }
        else if(msg_type === 'STATIC_RECIPIENTS'){
        messageRecipientsGroupComponent.hide();
        messageRecipientsComponent.show();
       sendToComponent.hide();
        }
    }
    
    var evaluateScheduleDateVisibility = function() {
        var sendImmediately = sendImmediatelyComponent.val();
        
        if(sendImmediately === 'SCHEDULE'){
          scheduleDateComponent.show();
      } else {
          scheduleDateInput.val('');
          scheduleDateComponent.hide();
      }
    }
    
     var appendOptions = function(p) {
        recipientGroupSelector.html('');
        for (var key in p) {
            if (p.hasOwnProperty(key)) {
                var option = $('#recipient_group_selector option[value='+key+']').val();
                if(!option) {
                    var selected = (sel_data.indexOf(key) === -1) ? '' : 'selected';
                    recipientGroupSelector.append('<option value='+key+' '+selected+'>'+p[key]+'</option>');
                }
            }
        }
        
        reInitRecipientSelect2(function(){
             $('#recipient_group_selector option').prop('selected', false);
         });
    }
    
    var changeOptions = function(p) {
        recipientGroupSelector.html('');
        for (var key in p) {
            if (p.hasOwnProperty(key)) {
                $('#recipient_group_selector').append('<option value='+key+'>'+p[key]+'</option>')
            }
        }
    }  
    
    //This function refreshes the destination select 2 component
    //It will take in an optional function of statements to run just before the final initialization
    var reInitRecipientSelect2 = function(beforeAppy) {
        var element = $('#recipient_group_selector');
         
         //reset select2 values if previously selected 
        element.val(null).trigger('change');

        //get plugin options
        var dataSelect = eval(element.data('krajee-select2'));

        //get kartik-select2 options
        var krajeeOptions = element.data('s2-options');
        
        if(beforeAppy) beforeAppy()

        //apply select2 options and load select2 again
        $.when(element.select2(dataSelect)).done(initS2Loading("recipient_group_selector", krajeeOptions));
    }
    
    
    evaluateScheduleDateVisibility();
    refreshMessageRecipientComponents();
    
    
    

    
    scheduleDateInput.on('change', function() {
      refreshMessageRecipientComponents();
    })
    
    
    
    
    
    
    schoolIdComponent.on('change', function(e) {
        loadRecipientGroupList();
    }); 
    
    sendImmediatelyComponent.on('change', function(e) {
        evaluateScheduleDateVisibility();
    });
    
    messageTypeComponent.on('change', function(e) {
        $('#recipient_group_selector').html('');
        loadRecipientGroupList();
        refreshMessageRecipientComponents();
    });
    
    messageTypeComponent.on('change', function(e) {
        $('#recipient_group_selector').html('');
        loadRecipientGroupList();
        refreshMessageRecipientComponents();
    });
   
    
   
    
    var templateTextArea = $('#msgschoolmessages-message_template');
    
    var ontemplateupdated = function(e) {
      var template = templateTextArea.val();
      var length = 0;
      var preview = $('#MessagePreview');
      if(template) {
          
          template = template.replace(/{GUARDIAN_NAME}/g, 'Annet Namazzi');
          template = template.replace(/{OUTSTANDING_BALANCE}/g, '100,000');
          template = template.replace(/{STUDENT_PAYMENT_CODE}/g, '1000231001');
          template = template.replace(/{SCHOOL_NAME}/g, '$schoolName');
          template = template.replace(/{STUDENT_CLASS}/g, 'P.5');
          template = template.replace(/{NEXT_PAYMENT_AMOUNT}/g, '150,100');
          template = template.replace(/{NEXT_PAYMENT_DATE}/g, '14/06/2020');
          template = template.replace(/{TOTAL_PLAN_OUSTANDING_BALANCE}/g, '400,000');
          template = template.replace(/{STUDENT_NAME}/g, 'Kalyesubula Ronald Marion');
          length = template.length;
      }
       preview.text(template + '('+length+' Approx Chars )');
      if(length > 160) {
          preview.addClass('alert-danger')
      }else {
          preview.removeClass('alert-danger')
      }
      
    }
    
    //Holder for updating message preview 
    // templateTextArea.on('change', function(e){
    //     ontemplateupdated(e);
    // });
    
    templateTextArea.on('keyup', function(e){
        ontemplateupdated(e);
    });

    
    
    $('.message_placeholder').on('click', function() {
        var template = $(this).find('a').attr('data-placeholder');
        var thisText = templateTextArea.val();
        templateTextArea.val(thisText+" "+template+" ")
        //Update template preview
        ontemplateupdated(undefined);
    })
    
    
    //Run the updater in case theres a message any way
    ontemplateupdated(undefined);
    
    
    //Create link to send to all and code it
    $('label[for=recipient_group_selector]').html('Recipient Groups / Classes <a id="send_to_all_link">Send to all</a>');
    
         $('#send_to_all_link').on('click', function(e) {
         $('#recipient_group_selector option').prop('selected', true);
         reInitRecipientSelect2(function(){
             $('#recipient_group_selector option').prop('selected', true);
         });
     });
    
    
});
JS;
$this->registerJs($script);
?>

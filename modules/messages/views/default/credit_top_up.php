<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div style="color:#F7F7F7">Credit Topup</div>

<?php $form = ActiveForm::begin([
    'action' => ['top-up', 'id' => $model->school_id, 'mod' => 'sch-credits'],
    'method' => 'post',
    'options' => ['class' => 'formprocess', 'id' => 'assign_fee_student_form_modal']
]); ?>

<div class="row">
    <div class="col-xs-12">
        <?= $form->field($model, 'top_up_amount')->textInput(['type' => 'number', 'id' => 'topup_amount'])->label('Topup Amount in Birr') ?>
    </div>
    <div class="col-xs-12">
        <?= $form->field($model, 'rate_per_message')->textInput(['type' => 'number', 'id' => 'topup_rate']) ?>
    </div>
</div>


<div class="col-xs-12" id="total_container">
    <code><h3>Number of Messages: <span id="credit_total_amount">0</span> Messages</h3></code>
</div>
<div class="form-group col-xs-12 no-padding" style="margin-top:20px;">
    <div class="col-xs-6">
        <?= Html::submitButton('Top Up <i class="fa fa-plus-square"></i>', ['disabled'=>'true', 'id'=>'topup_button', 'class' => 'btn btn-block btn-primary', 'data-confirm' => 'Are you sure you want Top up']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $("document").ready(function(){
         var amount_field = $('#topup_amount');
            var rate_field = $('#topup_rate');
            var message_count_container = $('#credit_total_amount')
            var total_container = $('#total_container')
            var topup_button = $('#topup_button')
            
        function recompute() {
           
            
            var amount = amount_field.val();
            var rate = rate_field.val();
            
            var message_count = 0;
            if(amount && rate) {
                message_count = (+amount)/(+rate);
            }else {
                topup_button.attr('disabled', true)
                return;
            }
            
            if(!Number.isInteger(message_count)) {
                total_container.addClass('alert alert-danger')
                topup_button.attr('disabled', true)
            }else {
               total_container.removeClass('alert alert-danger')
               topup_button.attr('disabled', false) 
            }
            
            message_count_container.html(numberWithCommas(message_count));
        }
        
                amount_field.on('keyup', function(e) {
            recompute()
        });
        
        rate_field.on('keyup', function(e) {
            recompute()
        });
        
        
        var numberWithCommas = function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
        }
    });
JS;
$this->registerJs($script);
?>

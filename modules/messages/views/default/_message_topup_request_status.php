<?php

use yii\helpers\Url; ?>
<div style="color:#F7F7F7">Student Information</div>

<div class="row hd-title" data-title="Processing payment">
    <div id="processing" class="col-xs-12">
        <img style="display: block; "
             src="<?= Yii::$app->request->baseUrl ?>/web/img/Blocks-1s-200px.gif">

        <?php if ($channel->channel_code == "MTN_UG"): ?>
            <div style="margin-top:-30px;">
                <h3>Please approve the payment on your phone</h3>
                Enter your Mobile Money PIN on your phone number <?= $phoneNumber ?> <br/>
                Or Dial *165#<br/>
                Select my account<br/>
                Select My Approvals
            </div>
        <?php endif; ?>


        <?php if ($channel->channel_code == "AIRTEL_MONEY_UG"): ?>
            <div style="margin-top:-30px;">
                <h3>Please approve the payment on your phone with your Airtel Money PIN</h3>
                We have sent a transaction request to your phone number <?= $phoneNumber ?><br/>
            </div>
        <?php endif; ?>

    </div>

    <div id="processing_complete" class="col-xs-12" style="display: none">
    </div>

    <div id="processing_successful" class="col-xs-12" style="display: none">
        <img style="display: block;"
             src="<?= Yii::$app->request->baseUrl ?>/web/img/success.gif"
             height="200" width="200">
        <div id="processing_successful_msg" class="col-xs-12 text-center">
        </div>
    </div>
</div>

<?php
$url = Url::to(['/payfees/default/confirm-payment']);
$checkUrl = Url::to(['/payfees/default/check-request']);
$receiptBaseUrl = Url::to(['/site/ms']);


$script = <<< JS
let mpn = '$mpn';
let chn = '$channel->channel_code';

$(document).ready(function(){
    
    var checkRequest = function(mpn, chn) {
      $.get('$checkUrl', {mpn : mpn, chn: chn}, function( data ) {
          if(data.code==404){
              $("#processing").hide();
              $("#processing_complete").html("<span class='col-xs-12 alert alert-danger'>"+data.message+"</span>");
              $("#processing_complete").show();
          } else if(data.code==100){
              $("#processing").hide();
              var rcpt_url = '$receiptBaseUrl'+'?i='+data.bs64Url;
              $("#processing_successful_msg").html("<span>Your transaction has been completed succesfully. <br>Transaction ID: "+data.transactionId+" <a href='"+rcpt_url+"' target='_blank'><br/><i class='fa fa-print'></i>&nbsp;&nbsp;Download receipt</a></span>");
              $("#processing_successful").show();
          }else {
              //Still waiting
              setTimeout(function(){
                  checkRequest(mpn, chn); 
              }, 5000);
          }
        });
    }
    
    
    setTimeout(function(){
                  checkRequest(mpn, chn); 
              }, 5000);
    
    
 });
JS;
$this->registerJs($script);
?>


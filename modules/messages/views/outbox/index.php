
<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Oubox Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outbox-index hd-title" data-title="Outbox Messages">
<div class="row">
    <div class="col-md-12">
    <div>
      <div class="col-md-3  no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-comments"></i> Outbox</span></div>

      <div class="col-md-9 no-padding" ><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>

    </div>
    </div>

</div>



    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr><th class='clink'><?= $sort->link('time_generated') ?></th><th class='clink'><?= $sort->link('message_text') ?></th><th class='clink'><?= $sort->link('recipient_number')?></th><th class='clink'><?= $sort->link('time_sent') ?></th></tr>

                    </thead>
                    <tbody>
                    <?php
                    if($dataProvider) :
                        foreach($dataProvider as $k=>$v) : ?>
                            <tr <?= ($v['message_status']=='SENT') ? " " :"style='background-color:#f9dbdb;'" ?> >
                                <td><?= ($v['time_generated']) ? date('Y-m-d h:i:s a', strtotime($v['time_generated'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['message_text']) ? $v['message_text'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['recipient_number']) ? $v['recipient_number'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['time_sent']) ? date('Y-m-d h:i:s a', strtotime($v['time_sent'])) : '<span class="not-set">(not set) </span>' ?></td>

                            </tr>
                        <?php endforeach;
                    else :?>
                        <td colspan="4">No messages found</td>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel'  => 'Last'
            ]); ?>

        </div>
    </div>



<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\messages\models\MessageOutbomdearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-outbox-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>

    <div class="col-md-3 no-padding"><?= DatePicker::widget([
        'model' => $model,
        'attribute' => 'date_from',
        'dateFormat' => 'yyyy-MM-dd',
        'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'From'],
        'clientOptions' =>[
              'class'=>'form-control',
              'changeMonth'=> true,
              'changeYear'=> true,
              'yearRange'=>'1900:'.(date('Y')+1),
              'autoSize'=>true,
              ],
      ]); ?></div>


    <div class="col-md-3 no-padding"><?= $form->field($model, 'message_status', ['inputOptions'=>[ 'class'=>'form-control input-sm']] )->dropDownList(['SENT'=>'SENT', 'PENDING'=>'PENDING'], ['prompt'=>'Message Status'])->label(false) ?></div>
    <div class="col-md-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-md']) ?></div>

    <?php ActiveForm::end(); ?>

</div>

<?php

namespace app\modules\messages\controllers;

use app\models\NchePaymentsReceivedSearch;

use app\modules\messages\models\MsgSchoolCredits;
use app\modules\messages\models\MsgSchoolCreditsHistory;
use app\modules\messages\models\MsgSchoolMessagesSearch;
use app\modules\messages\models\MsgTopupTransactionsReceivedSearch;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use Yii;
use yii\base\DynamicModel;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;

class DefaultController extends \app\controllers\DefaultController
{
    public function actionClasses($id)
    {
        $db = Yii::$app->db;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $classes = (new Query())->select(['id', 'class_description'])
            ->from('core_school_class')
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'class_code', '__ARCHIVE__'])
            ->orderBy('class_code')
            ->all($db);
        return ArrayHelper::map($classes, 'id', 'class_description');
    }

    public function actionTopUp($mod, $id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($mod, $id);
        $model->scenario = 'top_up';
        $model->rate_per_message = Yii::$app->params['ratePerMessage'];
        if ($model->load($request->post()) && $model->validate()) {
            $transaction = $model::getDb()->beginTransaction();
            try {
                //Compute topup
                $model->top_up = $model->top_up_amount / $model->rate_per_message;
                Yii::trace($model->top_up_amount);
                Yii::trace($model->top_up);
                Yii::trace($model->rate_per_message);

                $history = new MsgSchoolCreditsHistory();
                $history->message_credits_id = $model->id;
                $history->credits = intval($model->top_up);
                $history->credits_before = intval($model->credit_bal);
                $history->credits_after = intval($model->credit_bal) + intval($model->top_up);
                $amount = intval($model->rate_per_message) * intval($model->top_up);
                $history->description = "Top Up - " . number_format($model->top_up) . " @" . $model->rate_per_message . " - (" . number_format($amount) . " UGX)";
                $history->save(false);

                $model->credit_bal = $history->credits_after;
                $model->save(false);
                $transaction->commit();
                return $this->redirect(['view', 'mod' => $mod, 'id' => $model->school_id]);
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::trace($e->getMessage(), 'ERROR');
            }
        }
        $template = 'credit_top_up';
        $res = ['model' => $model, 'mod' => $mod];
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }

    public function actionGroups($id)
    {
        $db = Yii::$app->db;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $groups = (new Query())->select(['id', 'group_name'])
            ->from('student_group_information')
            ->where(['school_id' => $id])
            ->orderBy('id DESC')
            ->all($db);
        return ArrayHelper::map($groups, 'id', 'group_name');
    }

    public function actionLogs($id)
    {
        $searchModel = new MsgSchoolMessagesSearch();
        $request = Yii::$app->request;
        $params = Yii::$app->request->queryParams;
        $params['MsgSchoolMessagesSearch']['id'] = $id;
        $data = $searchModel->searchLogs($params);
        $template = "message_logs";
        $res = ['searchModel' => $searchModel, 'data' => $data, 'mod' => 'messages'];

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }

    protected function formTemplate($mod, $isNewRecord)
    {
        if ($mod == 'sch-messages') return 'create_message';
        else return parent::formTemplate($mod, $isNewRecord);
    }

    protected function viewTemplate($mod)
    {

        Yii::trace($mod);
        if ($mod == 'sch-credits') return 'view_school_credits';
        else if ($mod == 'sch-messages') return 'message_view';
        else return parent::viewTemplate($mod);
    }

    protected function findModel($mod, $id)
    {
        if ($mod == 'sch-credits') {
            return MsgSchoolCredits::find()->where(['school_id' => $id])->limit(1)->one();
        }
        return parent::findModel($mod, $id);

    }

    protected function classModels($id)
    {
        $classes = [
            'sch-credits' => [
                'search' => 'app\modules\messages\models\MsgSchoolCreditsSearch',
                'model' => 'app\modules\messages\models\MsgSchoolCredits',
                'rights' => [
                    'index' => 'r_msg_cr', 'view' => ['r_msg_cr', ['sch' => 'school_id']], 'create' => 'w_msg_cr',
                    'update' => ['w_msg_cr', ['sch' => 'school_id']], 'delete' => ['w_msg_cr', ['sch' => 'school_id']],
                ]
            ],
            'sch-messages' => [
                'search' => 'app\modules\messages\models\MsgSchoolMessagesSearch',
                'model' => 'app\modules\messages\models\MsgSchoolMessages',
                'rights' => [
                    'index' => 'r_msg', 'view' => ['r_msg', ['sch' => 'school_id']], 'create' => 'w_msg',
                    'update' => ['w_msg', ['sch' => 'school_id']], 'delete' => ['w_msg', ['sch' => 'school_id']],
                ]
            ],
            'sch-outbox' => [
                'search' => 'app\modules\messages\models\MsgSchoolOutboxSearch',
                'model' => 'app\modules\messages\models\MsgSchoolOutbox',
                'except' => ['create', 'update', 'delete'],
                'rights' => [
                    'index' => 'r_outbox', 'view' => ['r_outbox', ['sch' => 'school_id']],
                ]
            ],

        ];

        return $classes[$id];
    }

    public function actionTopupPayments()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('schoolpay_admin'))
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";

        $searchModel = new MsgTopupTransactionsReceivedSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        $params = [
            'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']];
        return ($request->isAjax) ? $this->renderAjax('msg_topup_payments', $params) :
            $this->render('msg_topup_payments', $params);

    }

    public function actionRequestTopupPayments()
    {
        $request = Yii::$app->request;
        if (!\app\components\ToWords::isSchoolUser())
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view this page";

        //Load
        $paymentModel = new DynamicModel(['payment_channel', 'phone_number', 'amount']);
        $paymentModel->addRule(['payment_channel', 'phone_number', 'amount'], 'required');

        try {

            $school_id = Yii::$app->user->identity->school_id;
            //Get the credits model
            $model = MsgSchoolCredits::findOne(['school_id' => $school_id]);
            $school = CoreSchool::findOne(['id'=>$school_id]);

            $defaultUrl = Url::to(['/messages/e/sch-credits/view', 'id' => $school_id]);
            if (!$request->post())
                return $this->redirect($defaultUrl);



            if (!$paymentModel->load($request->post())) {
                throw new \Exception('An error occurred processing your request');
            }

            if(!$paymentModel->payment_channel)
                throw new Exception('Payment Channel is required');


            $channel = PaymentChannels::findOne(['id'=>$paymentModel->payment_channel]);

            $client = new Client();
            $baseUrl = Yii::getAlias('@mtnPaymentApi');

            $param = rawurlencode('MESSAGES' . $school->school_code);
            $url = "{$baseUrl}/{$channel->channel_code}/{$paymentModel->phone_number}/{$param}/{$paymentModel->amount}";

            $response = $client->createRequest()
                ->setUrl($url)
                ->send();

            $content = json_decode($response->getContent());
            Yii::trace(print_r($content, true));
            if ($content->returnCode != 0) {
                $params = ['model' => $model, 'topupError' =>  $content->returnMessage];
                return ($request->isAjax) ? $this->renderAjax('view_school_credits', $params) :
                    $this->render('view_school_credits', $params);
            }
            $data = $content->returnObject;

            $processingNumber = ($channel->channel_code == 'AIRTEL_MONEY_UG') ?
                $data->airtelProcessingNumber : $data->mtnProcessingNumber;

            //Now load polling page
            $params = ['channel' => $channel, 'mpn' =>  $processingNumber, 'phoneNumber'=>$paymentModel->phone_number];
            return ($request->isAjax) ? $this->renderAjax('_message_topup_request_status', $params) :
                $this->render('_message_topup_request_status', $params);

        } catch (\Exception $e) {
            Yii::trace('Error is ' . $e->getMessage());
            $params = ['model' => $model, 'topupError' => $e->getMessage(), 'paymentModel'=>$paymentModel];
            return ($request->isAjax) ? $this->renderAjax('view_school_credits', $params) :
                $this->render('view_school_credits', $params);
        }



    }
}

<?php

namespace app\modules\messages\controllers;

use Yii;
use app\modules\messages\models\MessageOutbox;
use app\modules\messages\models\MessageOutboxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * OutboxController implements the CRUD actions for MessageOutbox model.
 */
class OutboxController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MessageOutbox models.
     * @return mixed
     */
   
    public function actionIndex()
    {
        if(Yii::$app->user->can('view_messages')){
            $request = Yii::$app->request;
            $searchModel = new MessageOutboxSearch();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages'=>$data['pages'], 'page'=>$data['cpage']];
            $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ];

            return ($request->isAjax) ? $this->renderAjax('index', $res) : $this->render('index', $res);
        } else {
        throw new ForbiddenHttpException('No permissions to view Outbox messages List.');
        }       
    }

    /**
     * Displays a single MessageOutbox model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   

    /**
     * Finds the MessageOutbox model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MessageOutbox the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MessageOutbox::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

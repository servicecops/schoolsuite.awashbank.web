<?php

namespace app\modules\web_site\controllers;


use app\models\User;
use app\modules\attendance\models\Rollcall;
use app\modules\attendance\models\RollcallSearch;
use app\modules\disciplinary\models\Disciplinary;
use app\modules\disciplinary\models\DisciplinarySearch;
use app\modules\feesdue\models\FeesDue;
use app\modules\feesdue\models\FeesDueStudentExemption;
use app\modules\feesdue\models\InstitutionFeesDuePenalty;
use app\modules\gradingcore\models\Grading;
use app\modules\gradingcore\models\GradingSearch;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class CaseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



    /**
     * Creates a new Disciplinary case model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_disp') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a disciplinary case.');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new Disciplinary();

        try {
            if ($model->load(Yii::$app->request->post())) {

                $incoming = Yii::$app->request->post();
                $post = $incoming['Disciplinary'];

                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }

                if ($post['is_student']) {

                    $std = CoreStudent::findOne(['id' => $post['student_code']]);
                    $model->class_id = $std->class_id;
                    $model->school_id =$std->school_id;
                    $model->student_code =$std->student_code;
                }



                if ($model->save(false)) {
                    Logs::logEvent("Created New Disciplinary Case: " . $model->id, null, null);

                }


                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id], 200);

                //   }
            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to new disciplinary case: ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

    public function actionStdList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, student_code AS text')
                ->from('core_student')
                ->where(['student_code'=> $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreStudent::find($id)->student_code];
        }
        return $out;
    }

    public function actionStaffList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, username AS text')
                ->from('user')
                ->where(['ilike', 'username', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->username];
        }
        return $out;
    }


    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_disp', ['sch' => $model->school_id])) {

            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [                'model' => $model  ]) :
                $this->render('view', ['model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to view this disciplinary.');
        }
    }

    /**
     * Finds the Disciplinary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Disciplinary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Disciplinary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a view.
     * @return mixed
     */
    public function actionIndex()
    {

            $request = Yii::$app->request;

            return ($request->isAjax) ? $this->renderAjax('index') :
                $this->render('index');


    }

}

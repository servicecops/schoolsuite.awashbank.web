<?php
namespace app\modules\web_site;

class SuiteSiteModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\web_site\controllers';
    public function init() {
        parent::init();
    }
}

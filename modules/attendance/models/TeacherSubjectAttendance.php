<?php

namespace app\modules\attendance\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $date_rollcalled
 * @property string $date_created
 * @property integer $student_id
 * @property integer $subject_id
 * @property integer $school_id
 * @property integer $present
 * @property boolean $ip_address
 * @property integer $teacher_id
 *

 */
class TeacherSubjectAttendance extends \yii\db\ActiveRecord
{
    public $school_student_registration_number,$school_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher_subject_attendance';
    }



    /**
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'teacher_id', 'subject_id','date_created',], 'required'],
            [['present'], 'boolean'],
            [['ip_address','method_used'], 'string'],
            [['class_id'], 'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Class ID',
            'clock_in' => 'Clock In',
            'clock_out' => 'Clock Out',
            'ip_address' => 'IP Address',
            'class_id' => 'Class',
            'present' => 'Present',
            'subject_id' => 'Subject Id',
        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }


    public function searchStudentClassGrades($class_id)
    {
        $query = (new Query())
            ->select(['cm.id','cm.min_mark', 'cm.max_mark','cm.grades'])
            ->from('core_grades cm')
            ->innerJoin('core_school sch','sch.id =cm.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cm.class_id' )
            ->where(['cm.class_id'=>$class_id]);

        $query->orderBy('cm.id desc');

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [

            'min_mark',
            'max_mark',
            'grades',

            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
}

<?php

namespace app\modules\attendance\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $date_rollcalled
 * @property string $date_created
 * @property string $clock_out
 * @property string $clock_in
 * @property integer $subject_id
 * @property integer $school_id
 * @property integer $present
 * @property string $is_clockin
 * @property integer $teacher_id
 *

 */
class StudentDailyAttendance extends \yii\db\ActiveRecord
{
    public $school_student_registration_number,$school_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_general_attendance';
    }



    /**
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'teacher_id','present','date_created','student_id'], 'required'],
            [['present'], 'boolean'],
            [['ip_address','method_used'], 'string'],
            [['class_id', 'student_id','school_id'], 'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher ID',
            'student_id' => 'Student ID',
            'method_used' => 'Method Used',
            'ip_address' => 'IP Address',
            'class_id' => 'Class',
            'present' => 'Present',
            'school_id' => 'School ID',
        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }


    public function searchStudentClassGrades($class_id)
    {
        $query = (new Query())
            ->select(['cm.id','cm.min_mark', 'cm.max_mark','cm.grades'])
            ->from('core_grades cm')
            ->innerJoin('core_school sch','sch.id =cm.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cm.class_id' )
            ->where(['cm.class_id'=>$class_id]);

        $query->orderBy('cm.id desc');

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [

            'min_mark',
            'max_mark',
            'grades',

            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
}

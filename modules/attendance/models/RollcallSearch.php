<?php

namespace app\modules\attendance\models;

use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class RollcallSearch extends Rollcall
{
    public $modelSearch, $schsearch, $pc_rgnumber, $school_student_registration_number;
    public $date_from, $date_to, $transaction_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['modelSearch','schsearch'], 'trim'],
            [['date_created', 'created_by','date_from', 'date_to','modelSearch','schsearch','school_student_registration_number'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['student_code', 'first_name', 'last_name', 'class_code', 'school_name', 'student_email', 'student_phone', 'rollcalled_by', 'student_attended'],
            'fileName' => 'Students Attendance Information',
            'title' => 'student Datasheet',
            'exportFile' => '/rollcall/exportPdfExcel',
            'exportGenFile' => '/rollcall/exportGenPdfExcel',
        ];

        return $data;
    }
    public function searchClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' );

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['/attendance/rollcall/display-subject', 'classId' => $model['id']]);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Choose class', ['/attendance/rollcall/display-subject', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }
    /*
       * Return particular fields for dataprovider
       */
    public function searchSubjects($params)
    {

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->Where(['cs.class_id' => $params]);
        $query->orderBy('cs.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['rollcall/students-list', 'subjectId' => $model['id']]);
                },
                'format'=>'html',
            ],

            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Choose subject', ['rollcall/students-list', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchStds($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
      //  $this->school_student_registration_number = trim($this->school_student_registration_number);

        $queryclass =CoreSubject::findOne([$params]);
        Yii::trace($queryclass);

        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            'si.school_student_registration_number'])
            ->from('core_student si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            //
            ->Where(['si.archived' => false])
            ->andWhere(['si.class_id'=>$queryclass->class_id]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);
                $query->orWhere(['schoolpay_paymentcode' => $this->modelSearch]);

            } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }


        //$pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
      /*  unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;*/
       // $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db),  'sort' => $sort, ];

    }



    public function searchAttendance($params)
    {
        $this->load($params);

        $today = date('Y-m-d', time()+86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if(Yii::$app->user->can('schoolpay_admin') && empty($params)){
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $this->school_student_registration_number = trim($this->school_student_registration_number);

        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name','at.student_attended','at.date_rollcalled','sb.subject_name','uz.username', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            'si.school_student_registration_number'])
            ->from('student_attendance at')
            ->innerJoin('core_student si', 'si.id= at.student_id')
            ->innerJoin('core_school_class cls', 'cls.id=at.class_id')
            ->innerJoin('core_school sch', 'sch.id=at.school_id')
            ->innerJoin('core_subject sb', 'sb.id=at.subject_id')
            ->innerJoin('user uz', 'uz.id=at.rollcalled_by')
            //
            ->Where(['si.archived' => false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        }

         elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);
                $query->orWhere(['schoolpay_paymentcode' => $this->modelSearch]);

            } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        if($this->date_from){
        $query->andWhere("at.date_rollcalled::date>='$this->date_from'");
        if($date_to) $query->andWhere("at.date_rollcalled::date<='$date_to'");
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name','subject_name', 'class_code','date_rollcalled', 'student_email', 'student_phone', 'school_student_registration_number','username','student_attended',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('at.date_rollcalled desc');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }




    public function searchGenAttendance($params)
    {
        $this->load($params);

        $today = date('Y-m-d', time()+86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if(Yii::$app->user->can('schoolpay_admin') && empty($params)){
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $this->school_student_registration_number = trim($this->school_student_registration_number);

        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name','at.present','at.date_created','uz.username', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            'si.school_student_registration_number'])
            ->from('student_general_attendance at')
            ->innerJoin('core_student si', 'si.id= at.student_id')
            ->innerJoin('core_school_class cls', 'cls.id=at.class_id')
            ->innerJoin('core_school sch', 'sch.id=at.school_id')
            ->innerJoin('user uz', 'uz.id=at.teacher_id')
            //
            ->Where(['si.archived' => false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);
                $query->orWhere(['schoolpay_paymentcode' => $this->modelSearch]);

            } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        if($this->date_from){
            $query->andWhere("at.date_created::date>='$this->date_from'");
            if($date_to) $query->andWhere("at.date_created::date<='$date_to'");
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name','subject_name', 'class_code','date_created', 'student_email', 'student_phone', 'school_student_registration_number','username',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('at.date_created desc');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }


    public function searchStdClass(array $queryParams)
{
    $query = (new Query())
        ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
        ->from('core_school_class csc')
        ->innerJoin('core_school sch','csc.school_id =sch.id' );

    $query->orderBy('csc.id desc');

    if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

        $trQuery = new Query();
        $trQuery->select(['tc.class_id'])
            ->from('teacher_class_subject_association tc')
            ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
            ->innerJoin('user uz', 'cs.id=uz.school_user_id')
            ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


        $commandt = $trQuery->createCommand();
        $data = $commandt->queryAll();


        $the_classId = [];
        Yii::trace($the_classId);

        foreach($data as $k =>$v){
            array_push($the_classId, $v['class_id']);
        }
        Yii::trace($the_classId);

        if ($this->class_id && $this->class_id != 0) {
            $query->andWhere(['csc.id' => intVal($this->class_id)]);
        } else {
            // $query->andWhere(['tca.class_id' => 18]);
            $query->andwhere(['in', 'csc.id', $the_classId]);
        }
    }
    elseif (\app\components\ToWords::isSchoolUser()) {
        if ($this->class_id && $this->class_id != 0) {
            $query->andWhere(['sch.id' => intVal($this->id)]);
        } else {
            $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
        }
    } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
        if (!empty($this->class_id)) {
            $query->andWhere(['sch.id' => intVal($this->id)]);
        } elseif ($this->schsearch) {
            $query->andWhere(['csc.id' => $this->schsearch]);
        }
    }

    //  ->where(['si.active'=>true, 'si.archived'=>false]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
    ]);

    $columns = [
        'class_code',
        [
            'label' => 'Class Name',
            'value'=>function($model){
                return Html::a($model['class_description'], ['/attendance/rollcall/class-students-list', 'classId' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'school_name',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('Choose class', ['/attendance/rollcall/class-students-list', 'classId' => $model['id']]);
            },
        ],
        ////
    ];
    $searchForm= 'subject_name';
    return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

}

public function ClassStds($params){
    $this->load($params);
    //Trim the model searches
    $this->modelSearch = trim($this->modelSearch);
    //  $this->school_student_registration_number = trim($this->school_student_registration_number);

    $query = new Query();
    $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
        "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
        'si.school_student_registration_number'])
        ->from('core_student si')
        ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
        ->innerJoin('core_school sch', 'sch.id=cls.school_id')
        //
        ->Where(['si.archived' => false])
        ->andWhere(['si.class_id'=>$params]);

    if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

        $trQuery = new Query();
        $trQuery->select(['tc.class_id'])
            ->from('teacher_class_subject_association tc')
            ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
            ->innerJoin('user uz', 'cs.id=uz.school_user_id')
            ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


        $commandt = $trQuery->createCommand();
        $data = $commandt->queryAll();


        $the_classId = [];
        Yii::trace($the_classId);

        foreach($data as $k =>$v){
            array_push($the_classId, $v['class_id']);
        }
        Yii::trace($the_classId);

        if ($this->class_id && $this->class_id != 0) {
            $query->andWhere(['cls.id' => intVal($this->class_id)]);
        } else {
            // $query->andWhere(['tca.class_id' => 18]);
            $query->andwhere(['in', 'cls.id', $the_classId]);
        }
    } elseif (\app\components\ToWords::isSchoolUser()) {
        if ($this->class_id && $this->class_id != 0) {
            $query->andWhere(['si.class_id' => intVal($this->class_id)]);
        } else {
            $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
        }
    } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
        if (!empty($this->class_id)) {
            $query->andWhere(['si.class_id' => intVal($this->class_id)]);
        } elseif ($this->schsearch) {
            $query->andWhere(['cls.school_id' => $this->schsearch]);
        }
    }


    if (!(empty($this->modelSearch))) {
        Yii::trace($this->modelSearch);

        if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
            $query->andWhere(['student_code' => $this->modelSearch]);
            $query->orWhere(['schoolpay_paymentcode' => $this->modelSearch]);

        } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
            //Registration numbers usually have a dash - or slass /
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
        } else {
            $query = $this->nameLogic($query, $this->modelSearch);
        }
    }

    if ($this->school_student_registration_number) {
        $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
    }


    //$pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
    $sort = new Sort([
        'attributes' => [
            'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
        ],
    ]);
    ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
    unset($_SESSION['findData']);
    $_SESSION['findData'] = $query;
    // $query->offset($pages->offset)->limit($pages->limit);
    return ['query' => $query->all(Yii::$app->db),  'sort' => $sort, ];

}



}

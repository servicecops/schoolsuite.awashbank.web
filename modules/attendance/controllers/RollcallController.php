<?php

namespace app\modules\attendance\controllers;


use app\models\User;
use app\modules\attendance\models\Rollcall;
use app\modules\attendance\models\RollcallSearch;
use app\modules\attendance\models\StudentDailyAttendance;
use app\modules\attendance\models\TeacherDailyAttendance;
use app\modules\attendance\models\TeacherSubjectAttendance;
use app\modules\feesdue\models\FeesDue;
use app\modules\feesdue\models\FeesDueStudentExemption;
use app\modules\feesdue\models\InstitutionFeesDuePenalty;
use app\modules\gradingcore\models\Grading;
use app\modules\gradingcore\models\GradingSearch;
use app\modules\logs\models\Logs;
use app\modules\paymentscore\models\ImageBank;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\studentreport\models\CoreSchoolCircular;
use app\modules\studentreport\models\CoreSchoolCircularSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\BaseObject;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class RollcallController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDisplayClass()
    {
        $request = Yii::$app->request;
        $searchModel = new RollcallSearch();
        $allData = $searchModel->searchClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title ='Select Class';
        $res = ['dataProvider' => $dataProvider,'requiresExport' => false,'title'=>$title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)


        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

       // return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionDisplaySubject($classId)
    {
        $request = Yii::$app->request;

        $searchModel = new RollcallSearch();
        $allData = $searchModel->searchSubjects($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title='Select Subject';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title'=>$title,'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

      //  return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionStudentsList($subjectId)
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $searchModel = new RollcallSearch();
            $data = $searchModel->searchStds($subjectId);
            $dataProvider = $data['query'];
            //$pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            $model = new Rollcall();
            return ($request->isAjax) ? $this->renderAjax('index', ['subjectId' => $subjectId, 'model' => $model,
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'sort' => $data['sort']]) :
                $this->render('index', ['searchModel' => $searchModel, 'subjectId' => $subjectId, 'model' => $model,
                    'dataProvider' => $dataProvider, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }
    }

    public function actionSubmitAttendance($subjectId)
    {

        if (Yii::$app->user->can('w_attendance')) {
            $request = Yii::$app->request;
            $model = new Rollcall();
            $transaction = Rollcall::getDb()->beginTransaction();

            try {

                if ($model->load(Yii::$app->request->post())) {



                    $data = Yii::$app->request->post();
                    $post = $data['Rollcall'];
                    $subjectDet = CoreSubject::findOne([$subjectId]);

                    foreach ($post as $K => $v) {
                        $model = new Rollcall();
                        $model->rollcalled_by = Yii::$app->user->identity->getId();

                        $model->class_id = $subjectDet->class_id;
                        $model->school_id = $subjectDet->school_id;
                        $model->subject_id = $subjectId;
                        if (\app\components\ToWords::isSchoolUser()) {
                            $model->school_id = Yii::$app->user->identity->school_id;
                        }

                        Yii::trace($v);
                        $model->student_id = $v['studentid'];
                        if (isset($v['present'])) {
                            $model->student_attended = true;
                        } else {
                            $model->student_attended = false;

                        }
                        $model->save(false);




                    }

                        $teacherSubAtt = new TeacherSubjectAttendance();
                        $teacherSubAtt->teacher_id = Yii::$app->user->identity->getId();
                        $teacherSubAtt->present = true;
                        $teacherSubAtt->subject_id = $subjectId;
                        $teacherSubAtt->ip_address = Yii::$app->request->userIP;
                        $teacherSubAtt->class_id = $subjectDet->class_id;
                        $teacherSubAtt->school_id = $subjectDet->school_id;
                        $teacherSubAtt->method_used = 'TICKING';
                        $teacherSubAtt->save(false);

                    $transaction->commit();
                    if ($model->save(false)) {
                        Logs::logEvent("Rollcalled Student for : " . $model->subject_id, null, $model->id);
                        return $this->redirect(['view-student-attendance']);
                    }
                }

                return ($request->isAjax) ? $this->renderAjax('index', ['model' => $model]) :
                    $this->render('index', ['model' => $model,'subjectId'=>$subjectId]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'ROLLCALL  STUDENT ROLLBACK');
            }


        } else {
            throw new ForbiddenHttpException('No permissions to rollcall students.');
        }
    }

    public function actionViewStudentAttendance(){

        if (Yii::$app->user->can('r_attendance')) {
            $request = Yii::$app->request;
            $searchModel = new RollcallSearch();
            $data = $searchModel->searchAttendance(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('student_attendance', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('student_attendance', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }
    }

    public function actionViewGenStudentAttendance(){

        if (Yii::$app->user->can('r_attendance')) {
            $request = Yii::$app->request;
            $searchModel = new RollcallSearch();
            $data = $searchModel->searchGenAttendance(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('student_gen_attendance', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('student_gen_attendance', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }
    }

    public  function actionTrDailyAttendanceSummary(){
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('Only School users are allowed to view attendance summary');
        }


        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $bopres = Yii::$app->db->createCommand('select sa.date_created, uz.firstname ,uz.lastname, sa.is_clockin, sa.clock_in,sa.clock_out,sa.ip_address, sa.longtitude, sa.latitude
from teacher_clockin_attendance sa 
inner join "user" uz on uz.id = sa.teacher_id
where sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('tr_summary', $res) : $this->render('tr_summary', $res);


    }

    public  function actionTrDailyAttendanceSummaryExport(){

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }

        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));


        $bopres = Yii::$app->db->createCommand('select sa.date_created, uz.firstname ,uz.lastname, sa.is_clockin, sa.clock_in,sa.clock_out,sa.ip_address
from teacher_clockin_attendance sa 
inner join "user" uz on uz.id = sa.teacher_id
where sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($bopres);

        $search_time = ['to'=>$to_date, 'from'=>$from_date];

        if(!empty($_REQUEST['trdailyexport']))
        {
//            $html = $this->renderPartial('channel_report_pdf',[
//                        'channel_data'=>$channel_data,'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
//                ]);

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('tr_daily_attendance_report_pdf',['tr_daily_data'=>$bopres,
                        'query'=>$search_time,
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print channel transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);

            ob_clean();
//            return Yii::$app->pdf->exportData('Channel Transactions summary','summery_report',$html);
            return $pdf->render();
        }
        elseif(!empty($_REQUEST['trdailyexcelexport']))
        {
            $file = $this->renderPartial('tr_daily_attendance_report_excel',[
                'tr_daily_data'=>$bopres, 'query'=>$search_time
            ]);
            $fileName = "tr_daily_attendance_report_excel".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }


    }

    public  function actionStdDailyAttendanceSummary(){
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('Only School users are allowed to view attendance summary');
        }


        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $bopres = Yii::$app->db->createCommand('select distinct(si.id),sa.date_created, sa.present, si.first_name, si.last_name, si.student_code, cla.class_code, uz.username from student_general_attendance sa inner join core_student si on si.id = sa.student_id
inner join core_school_class cla on cla.id = sa.class_id
inner join "user" uz on uz.id = sa.teacher_id
where sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('std_summary', $res) : $this->render('std_summary', $res);


    }


    public  function actionStdGenAttendanceSummary(){

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $bopres = Yii::$app->db->createCommand('select trans.school_name,trans.school_code, trans.presents, tyu.absents from(select cs.id ,cs.class_code, count(sa.id) as presents , sch.school_name,sch.school_code from student_general_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
inner join core_school sch on sch.id = sa.school_id
where  sa.date_created >= :from_date and sa.date_created < :end_date and sa.present=true
GROUP BY cs.id ,cs.class_code,  sch.school_name,sch.school_code)trans
inner join 
(select cs.id ,cs.class_code, count(sa.id) as absents , sch.school_name from student_general_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
inner join core_school sch on sch.id = sa.school_id
where  sa.date_created >= :from_date and sa.date_created < :end_date and sa.present=FALSE
GROUP BY cs.id ,cs.class_code,  sch.school_name)tyu on tyu.id= trans.id' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('std_general_summary', $res) : $this->render('std_general_summary', $res);


    }

    public  function actionTrGenAttendanceSummary(){

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $bopres = Yii::$app->db->createCommand('select count(teachers) as presents ,trans.id, trans.school_code, trans.school_name 
from (select distinct(tr.teacher_id) as teachers, sch.id, sch.school_code, sch.school_name from teacher_clockin_attendance tr inner join core_school sch on sch.id = tr.school_id
where  tr.date_created >= :from_date and tr.date_created < :end_date 

GROUP BY sch.id, sch.school_code, sch.school_name, tr.teacher_id)trans
GROUP BY trans.id, trans.school_code, trans.school_name ' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('tr_general_summary', $res) : $this->render('tr_general_summary', $res);


    }

    //teacher attendance report with percentages for non sch users
    public  function actionTrAttendanceReport(){

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];



        $bopres = Yii::$app->db->createCommand('

select pre.presents, pre.school_code , pre.school_name,trs.tttrs , trs.tttrs -pre.presents as absents  ,(pre.presents *100 /trs.tttrs) as percentage_present,((trs.tttrs -pre.presents) *100 /trs.tttrs) as percentage_absent  from	(select count(teachers) as presents ,trans.id, trans.school_code, trans.school_name 
from (select distinct(tr.teacher_id) as teachers, sch.id, sch.school_code, sch.school_name from teacher_clockin_attendance tr inner join core_school sch on sch.id = tr.school_id
where  tr.date_created >= :from_date and tr.date_created < :end_date 

GROUP BY sch.id, sch.school_code, sch.school_name, tr.teacher_id)trans
GROUP BY trans.id, trans.school_code, trans.school_name ) pre

 join 
 
 (select core_school.id as school_id ,
core_school.school_code,core_school.school_name,COUNT(core_staff.ID) as tttrs
FROM
"public".core_staff
inner JOIN "public".core_school ON "public".core_school."id" = "public".core_staff.school_id

WHERE core_staff.archived =false

GROUP BY core_school.school_code,core_school.school_name, core_school.id) trs

on pre.id = trs.school_id
  



' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('tr_general_attendance_report', $res) : $this->render('tr_general_attendance_report', $res);


    }


    //teacher attendance report with percentages for  sch users
    public  function actionSchTrAttendanceReport(){

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];

        $sch =Yii::$app->user->identity->school_id;
        Yii::trace($sch);

        $bopres = Yii::$app->db->createCommand('

select pre.presents, pre.school_code , pre.school_name,trs.tttrs , trs.tttrs -pre.presents as absents  ,(pre.presents *100 /trs.tttrs) as percentage_present,((trs.tttrs -pre.presents) *100 /trs.tttrs) as percentage_absent  from	(select count(teachers) as presents ,trans.id, trans.school_code, trans.school_name 
from (select distinct(tr.teacher_id) as teachers, sch.id, sch.school_code, sch.school_name from teacher_clockin_attendance tr inner join core_school sch on sch.id = tr.school_id 
    inner join "user" cu on cu.id = tr.teacher_id where  tr.date_created >= :from_date and tr.date_created < :end_date and tr.school_id =:schoolId and cu.user_level=:userlevel

GROUP BY sch.id, sch.school_code, sch.school_name, tr.teacher_id)trans
GROUP BY trans.id, trans.school_code, trans.school_name ) pre

 join 
 
 (select core_school.id as school_id ,
core_school.school_code,core_school.school_name,COUNT(core_staff.ID) as tttrs
FROM
"public".core_staff
inner JOIN "public".core_school ON "public".core_school."id" = "public".core_staff.school_id

WHERE core_staff.archived =false

GROUP BY core_school.school_code,core_school.school_name, core_school.id) trs

on pre.id = trs.school_id
  



' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':schoolId',$sch)
            ->bindValue(':userlevel','sch_teacher')
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('sch_tr_general_attendance_report', $res) : $this->render('sch_tr_general_attendance_report', $res);


    }


    //teacher attendance report with percentages for  admin users
    public  function actionAdminTrAttendanceReport(){

        $request = Yii::$app->request;
        $model = new Rollcall();
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $bopres=null;
        Yii::trace("posted");
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        Yii::trace(Yii::$app->request->post());
        if ($model->load(Yii::$app->request->post())) {
            Yii::trace("posted");

            $post = Yii::$app->request->post();
            $sch =$post['Rollcall']['school_id'];
            Yii::trace($sch);

            $bopres = Yii::$app->db->createCommand('

select pre.presents, pre.school_code , pre.school_name,trs.tttrs , trs.tttrs -pre.presents as absents  ,(pre.presents *100 /trs.tttrs) as percentage_present,((trs.tttrs -pre.presents) *100 /trs.tttrs) as percentage_absent  from	(select count(teachers) as presents ,trans.id, trans.school_code, trans.school_name 
from (select distinct(tr.teacher_id) as teachers, sch.id, sch.school_code, sch.school_name from teacher_clockin_attendance tr inner join core_school sch on sch.id = tr.school_id 
    inner join "user" cu on cu.id = tr.teacher_id where  tr.date_created >= :from_date and tr.date_created < :end_date and tr.school_id =:schoolId and cu.user_level=:userlevel

GROUP BY sch.id, sch.school_code, sch.school_name, tr.teacher_id)trans
GROUP BY trans.id, trans.school_code, trans.school_name ) pre

 join 
 
 (select core_school.id as school_id ,
core_school.school_code,core_school.school_name,COUNT(core_staff.ID) as tttrs
FROM
"public".core_staff
inner JOIN "public".core_school ON "public".core_school."id" = "public".core_staff.school_id

WHERE core_staff.archived =false

GROUP BY core_school.school_code,core_school.school_name, core_school.id) trs

on pre.id = trs.school_id
  



' )
                ->bindValue(':from_date', $from_date)
                ->bindValue(':end_date',$end_date)
                ->bindValue(':schoolId',$sch)
                ->bindValue(':userlevel','sch_teacher')
                ->queryAll();
            Yii::trace($bopres);
        }



        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date,
            'model'=>$model

        ];

        return $request->isAjax ? $this->renderAjax('admin_tr_general_attendance_report', $res) : $this->render('admin_tr_general_attendance_report', $res);


    }





    public  function actionStdGenAttendanceReport(){

        $request = Yii::$app->request;
        $model = new Rollcall();
        if (!Yii::$app->session->isActive){
            session_start();
        }
        if(\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('is_teacher')) {

            $schoolId =Yii::$app->user->identity->school_id;
            Yii::trace("svh");
            Yii::trace($schoolId);

        }
        else{
            Yii::trace("admin");
            $sch = $request->get('Rollcall') ;
            Yii::trace($sch['school_id']);
//            $sch= $_GET['Rollcall'];
          $schoolId= $sch['school_id'];
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $bopres = Yii::$app->db->createCommand(' select atten.school_name, atten.class_code ,  atten.presents, atten.absents, stds.ttstds, (atten.presents *100 /stds.ttstds) as percentage_present,(atten.absents *100 /stds.ttstds) as percentage_absent from

(select trans.school_name,trans.school_code,trans.class_code,trans.presents,trans.id, tyu.absents from(select distinct(cs.id) ,cs.class_code, count(sa.id) as presents , sch.school_name,sch.school_code
from student_general_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
inner join core_school sch on sch.id = sa.school_id
where  sa.date_created >= :from_date and sa.date_created < :end_date and sa.present=true
GROUP BY cs.id ,cs.class_code,  sch.school_name,sch.school_code)trans
inner join 
(select cs.id ,cs.class_code, count(sa.id) as absents , sch.school_name from student_general_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
inner join core_school sch on sch.id = sa.school_id
where  sa.date_created >= :from_date and sa.date_created < :end_date and sa.present=FALSE and sa.school_id =:school_id
GROUP BY cs.id ,cs.class_code,  sch.school_name)tyu on tyu.id= trans.id
)atten

join
(SELECT
core_school_class.id as class_id,COUNT(core_student.ID) as ttstds
FROM
"public".core_student
inner JOIN "public".core_school_class ON "public".core_school_class."id" = "public".core_student.class_id
WHERE core_student.archived =false 
GROUP BY core_school_class.id
HAVING COUNT(core_student.school_id) >5
ORDER BY COUNT(core_student.school_id) desc) stds

on stds.class_id  =atten.id
' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',$schoolId)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'model'=> $model,
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('std_general_attendance_report', $res) : $this->render('std_general_attendance_report', $res);


    }




    public function actionClasses()
    {
        $request = Yii::$app->request;
        $searchModel = new RollcallSearch();
        $allData = $searchModel->searchStdClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title='Select Class';
        $res = ['dataProvider' => $dataProvider,'title'=>$title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


       // return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionClassStudentsList($classId)
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $transaction = Rollcall::getDb()->beginTransaction();
            $searchModel = new RollcallSearch();

            //$pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            try{
                $data = $searchModel->ClassStds($classId);
                $dataProvider = $data['query'];
                $model = new Rollcall();

                if ($model->load(Yii::$app->request->post())) {



                    $dataz = Yii::$app->request->post();
                    $post = $dataz['Rollcall'];
                    $ClassDet = CoreSchoolClass::findOne([$classId]);

                    $attendance = Yii::$app->db->createCommand('select * from student_general_attendance where date_created >= now()::date + interval \'1h\' and class_id =:classId' )
                        ->bindValue(':classId', $classId)
                        ->queryAll();
                    Yii::trace($attendance);

                    if($attendance){
                        \Yii::$app->session->setFlash('appeal', "<div class='alert alert-danger'> General rollcall of Students in this class for the day is complete <br> </div>");

                        $transaction->rollBack();
                        return ($request->isAjax) ? $this->renderAjax('index_general_rollcall', ['classId' => $classId, 'model' => $model,
                            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'sort' => $data['sort']]) :
                            $this->render('index_general_rollcall', ['searchModel' => $searchModel, 'classId' => $classId, 'model' => $model,
                                'dataProvider' => $dataProvider, 'sort' => $data['sort']]);
                    }

                    foreach ($post as $K => $v) {
                        //check if student has already been rollcalled for the day



                        $model = new StudentDailyAttendance();
                        $model->teacher_id = Yii::$app->user->identity->getId();

                        $model->school_id = $ClassDet->school_id;
                        $model->class_id = $classId;
                        if (\app\components\ToWords::isSchoolUser()) {
                            $model->school_id = Yii::$app->user->identity->school_id;
                        }

                        Yii::trace($v);
                        $model->student_id = $v['studentid'];
                        if (isset($v['present'])) {
                            $model->present = true;
                        } else {
                            $model->present = false;

                        }
                        $model->save(false);




                    }


                    $transaction->commit();
                    if ($model->save(false)) {
                        Logs::logEvent("General Rollcalled Student for : " . $classId, null, $model->id);
                        \Yii::$app->session->setFlash('appeal', "<div class='alert alert-danger'>There was an issue sending your request try again later or call +256.414.597599<br> </div>");

                        return $this->redirect(['view-gen-student-attendance']);
                    }
                }




            return ($request->isAjax) ? $this->renderAjax('index_general_rollcall', ['classId' => $classId, 'model' => $model,
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'sort' => $data['sort']]) :
                $this->render('index_general_rollcall', ['searchModel' => $searchModel, 'classId' => $classId, 'model' => $model,
                    'dataProvider' => $dataProvider, 'sort' => $data['sort']]);


        } catch (\Exception $e) {
        $transaction->rollBack();
        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
        Yii::trace('Error: ' . $error, 'ROLLCALL  STUDENT ROLLBACK');
        Logs::logEvent("General Rollcalled Student for : " . $classId, $e->getMessage(), $model->id);

        \Yii::$app->session->setFlash('appeal', "<div class='alert alert-danger'>".$e->getMessage(). "<br> </div>");

    }


        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }
    }


    public function actionTrClockin()
    {
        $data=Yii::$app->request->queryParams;

        Yii::trace($data);
        Yii::trace($data['coords']['latitude']);




        try {
            $transaction = Rollcall::getDb()->beginTransaction();


            $lastClockinRecord = TeacherDailyAttendance::find()
                ->where(['teacher_id'=>Yii::$app->user->identity->id])
                ->orderBy(['date_created'=>SORT_DESC])
                ->limit(1)->one();

            Yii::trace($lastClockinRecord);

            $connection = Yii::$app->db;
            if(!$lastClockinRecord || !$lastClockinRecord->is_clockin){
                //   $transaction = $connection->beginTransaction();
                $sql2 = "INSERT INTO teacher_clockin_attendance (
                            rollcalled_by, 
                              clock_in, 
                               teacher_id,
                              is_clockin,
                              school_id,
                              ip_address,
                              longtitude,
                              latitude,
                              method_used          
                              )
                                VALUES (
                              :rollcalled_by, 
                              NOW() ,
                              :teacher_id,
                              true,
                              :school_id,
                              :ip_address,
                              :longtitude,
                               :latitude,
                               :method_used         
                                )";
                $fileQuery = $connection->createCommand($sql2);
                $fileQuery->bindValue(':rollcalled_by', Yii::$app->user->identity->id);
                $fileQuery->bindValue(':teacher_id', Yii::$app->user->identity->id);
                $fileQuery->bindValue(':school_id', Yii::$app->user->identity->school_id);
                $fileQuery->bindValue(':ip_address', Yii::$app->request->userIP);
                $fileQuery->bindValue(':longtitude', $data['coords']['longitude']);
                $fileQuery->bindValue(':latitude', $data['coords']['latitude']);
                $fileQuery->bindValue(':method_used', 'ticking');
                //Insert file
                $fileQuery->execute();

            }
            else {
                $recordId=$lastClockinRecord->id;

                Yii::trace($lastClockinRecord->id);

                $sql = "UPDATE teacher_clockin_attendance set is_clockin = false,clock_out= NOW() where id = :recordId " ;

                $numberOfStudentsMoved = $connection->createCommand($sql)
                    ->bindValue(':recordId', $recordId)
                    ->execute();
            }

            Logs::logEvent("Rollcalled teacher for : " .Yii::$app->user->identity->id, null, null);


            $transaction->commit();
            return $this->redirect(['/site/index']);
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting teacher rollcall details: ", $error, null);

            return json_encode([
                'returncode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }


    }

    public function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionGenExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportGenPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionExportExcel($model)
    {
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex'])  ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(20000)->all(Yii::$app->db);
        $type = 'Excel';

        $file = $this->renderPartial($data['exportFile'], [
            'query'=>$query, 'type'=>$type, 'subSessionIndex'=>$subSessionIndex
        ]);
        $fileName = $data['fileName'].'.xls';
        $options = ['mimeType'=>'application/vnd.ms-excel'];

        try{
            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        } catch(\Exception $e){
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to create Excel", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/core-student/error');
        }
    }

    public function actionGenExportExcel($model)
    {
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex'])  ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(20000)->all(Yii::$app->db);
        $type = 'Excel';

        $file = $this->renderPartial($data['exportGenFile'], [
            'query'=>$query, 'type'=>$type, 'subSessionIndex'=>$subSessionIndex
        ]);
        $fileName = $data['fileName'].'.xls';
        $options = ['mimeType'=>'application/vnd.ms-excel'];

        try{
            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        } catch(\Exception $e){
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to create Excel", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/core-student/error');
        }
    }

    public function actionSchView($id)
    {
        $request = Yii::$app->request;
        $viewModel = CoreSchoolCircular::findOne($id);
        return ($request->isAjax) ? $this->renderAjax('sch_view', [
            'model' => $viewModel,
        ]) :
            $this->render('sch_view', [
                'model' => $viewModel,
            ]);
    }
    public function actionSchz()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('No permissions to view school circulars.');
        }
        $searchModel = new CoreSchoolCircularSearch();
        $request = Yii::$app->request;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return ($request->isAjax) ? $this->renderAjax('schz_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) : $this->render('schz_index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * @param null $classes
     * @param null $student_codes
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\db\Exception
     */
    public function actionQrGenerator($classes = null, $student_codes = null)
    {
        $data = Yii::$app->request->post();
        Yii::trace($data);
        $model = new Rollcall();
        $request = Yii::$app->request;
        $studentIds = [];
        $studentClassArray = [];
        $school_id = null;

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            Yii::trace($data);
            try {
            if (isset($data['Rollcall']['classes'])) {
                Yii::trace("..... classses.........");
                Yii::trace($data['Rollcall']['classes']);
                $studentClassArray = $data['Rollcall']['classes'];
                //get student classes
                $query = (new Query())->select(['id'])->from('core_student')
                    ->andWhere(['in', 'class_id', array_values($data['Rollcall']['classes'])])
                    ->all();

                if (isset($_POST['exclude_student_codes'])) {
                    Yii::trace($_POST['exclude_student_codes']);
                    //get student codes
                    $codes = $_POST['exclude_student_codes'];
                    $p_codes = preg_split("/[\s,]+/", $codes);

                    $p_codes = array_unique(array_filter($p_codes));
                    Yii::trace($p_codes);
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->andWhere(['in', 'class_id', array_values($data['Rollcall']['classes'])])
                        ->andWhere(['not in', 'student_code', $p_codes])->all();

                }
//                $query = array_unique($query);
                foreach ($query as $v) {
                    array_push($studentIds, $v['id']);
                }

            } else if (isset($data['Rollcall']['student_class'])) {
                Yii::trace("..... codes.........");
                Yii::trace($_POST['student_codes']);
//                    get student codes
                $studentClass = $data['Rollcall']['student_class'];
                $codes = $_POST['student_codes'];
                $p_codes = preg_split("/[\s,]+/", $codes);
                $p_codes = array_unique(array_filter($p_codes));
                $query = (new Query())->select(['id','class_id'])
                    ->from('core_student')
                    ->where(['in', 'student_code', $p_codes])
                    ->all();
//                $query = array_unique($query);
                foreach ($query as $k => $v) {
                    array_push($studentIds, $v['id']);
                    array_push($studentClassArray, $v['class_id']);

                }
            }


            Yii::trace($studentIds);
            Yii::trace($studentClassArray);
            // get signatures attached
            /*$db = Yii::$app->db;
            $signature = $db->createCommand('select cs.* from core_signatures cs left join circular_signatures  css on (cs.id=css.signature_id) where css.circular_id=:circular_id',
                [':circular_id' => $circular_id])->queryOne();
            Yii::trace($signature);*/
            //now generator the PDF's
            $student_query = new Query();
            $student_query->select(['c_s.first_name', 'c_s.last_name','cs.school_logo','cl.class_description','cl.class_code', 'c_s.student_code', 'cs.school_name', 'cs.box_no', 'cs.contact_email_1', 'cs.contact_email_2', 'phone_contact_1', 'phone_contact_2', 'cs.vision', 'cs.school_reference_no'])
                ->from('core_student c_s')
                ->innerJoin('core_school cs', 'c_s.school_id=cs.id')
                ->innerJoin('core_school_class cl', 'cl.school_id=cs.id')
                ->where(['in', 'c_s.id', $studentIds])
                ->andWhere(['in', 'cl.id', $studentClassArray]);
            $command = $student_query->createCommand();
            $student_data = $command->queryAll();


            Yii::trace($student_data);
            $circular_data = [];

            foreach ($student_data as $v) {
            /*    ini_set('max_execution_time', '300');
                ini_set("pcre.backtrack_limit", "10000000");*/
                $circular_data[] = array(
                    'school_name' => $v['school_name'],
                    'box_no' => $v['box_no'],
                    'phone_contact_1' => $v['phone_contact_1'],
                    'phone_contact_2' => $v['phone_contact_2'],

                    'contact_email_1' => $v['contact_email_1'],

                    'first_name' => $v['first_name'],
                    'last_name' => $v['last_name'],
                    'student_code' => $v['student_code'],
                    'class_name' => $v['class_description'],
                    'class_code' => $v['class_code'],

                    'school_logo'=>$v['school_logo'],

                    'vision' => $v['vision']
                );
            }

            Yii::trace($circular_data);

                return $this->render('export_qr_pdf', ['circular_data' => $circular_data
                ]);
/*
            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('export_qr_pdf', ['circular_data' => $circular_data
                ]),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print student QR codes',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
//                    'SetHeader' => ['Reports||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => [],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            return $pdf->render();*/

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('export_qr_pdf', ['circular_data' => $circular_data
                ]),
//                'cssFile' => '@web/web/css/qr.css',
                'cssInline' => '.kv-heading-1{font-size:18px}',
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print students',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            return $pdf->render();
        } catch (\Exception $e) {
        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
        Yii::trace('Error: ' . $error, 'Generate circular callback');
    }
        }

        $res = ['student_codes' => $student_codes,  'classes' => $classes, 'model' => $model];
        return ($request->isAjax) ? $this->renderAjax('circular_selector', $res) : $this->render('circular_selector', $res);

    }
}

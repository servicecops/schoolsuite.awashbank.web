<?php
namespace app\modules\attendance;

class AttendanceModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\attendance\controllers';
    public function init() {
        parent::init();
    }
}

<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\jui\DatePicker;
use yii\web\JsExpression;
?>


<div class="container bg-white" style="width:100%;">
    <div >
        <?php $form = ActiveForm::begin([
            'action' => ['view-gen-student-attendance'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row mt-3" style="padding: 10px">

            <div class="row">
                <?php if (\app\components\ToWords::isSchoolUser()) : ?>

                    <div class="col-md-12 " >
                        <?php
                        $data =CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                        echo $form->field($searchModel, 'class_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map($data, 'id', 'class_code'),
                            'language' => 'en',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Find Class'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(false); ?>
                    </div>

                <?php endif; ?>
                <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>

                    <div class="col-sm-6 " >
                        <?php
                        $url = Url::to(['/schoolcore/core-school/active-schoollist']);
                        $selectedSchool = empty($searchModel->schsearch) ? '' : CoreSchool::findOne($searchModel->schsearch)->school_name;
                        echo $form->field($searchModel, 'schsearch')->widget(Select2::classname(), [
                            'initValueText' => $selectedSchool, // set the initial display text
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => 'Filter School',
                                'id' => 'school_search',
                                'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#rollcallsearch-class_id" ).html(data);
                        });'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],

                            ],
                        ])->label(false); ?>

                    </div>

                    <div class="col-sm-6" >
                        <?= $form->field($searchModel, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label(false) ?>
                    </div>

                <?php endif; ?>

            </div>

            <div class="col-sm-2" >
                <?= $form->field($searchModel, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control'],])->textInput(['title' => 'Enter student name or payment code',

                    'data-toggle' => 'tooltip',

                    'data-trigger' => 'hover',

                    'data-placement' => 'bottom'])->label(false) ?>

            </div>

            <div class="col-sm-4" >

                <ul class=" row menu-list no-padding" style="list-style-type:none">
                    <li class="col-md-6 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_from',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                            'clientOptions' => [
                                'class' => 'form-control',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                        ]); ?></li>
                    <li class="col-md-6 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'date_to',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                            'clientOptions' => [
                                'class' => 'form-control',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                        ]); ?></li>
                </ul>
            </div>
            <div><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php
$url = Url::to(['/schoolcore/core_school_class/lists']);
$cls = $searchModel->class_id;


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

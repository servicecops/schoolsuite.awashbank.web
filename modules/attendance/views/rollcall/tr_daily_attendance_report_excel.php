<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transaction History');
$this->params['breadcrumbs'][] = $this->title;

?>
<?php if(!empty($query))
    echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
?>

<div class="grid-view user-data">
    <div class="box-body table-responsive no-padding">
        <div class="row">
            <div class="col-md-12">
                <div class="box-body table table-responsive no-padding">
                    <table class="table table-striped">
                        <thead>
                        <?php
                        $x=1;
                        ?>
                        <tr>
                            <th></th>

                            <th class='clink'>Date</th>
                            <th class='clink'>First Name</th>
                            <th class='clink'>Last Name</th>
                            <th class='clink'>IP Address</th>
                            <th class='clink'>Clock In</th>
                            <th class='clink'>Clock Out</th>
                            <th class='clink'>Duration</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        if ($tr_daily_data) :

                            Yii::trace($tr_daily_data);
                            foreach ($tr_daily_data as $data) :


                                ?>

                                <tr data-key="0">
                                    <td><?php echo $x++?></td>

                                    <td>
                                        <?= ($data['date_created']) ? $data['date_created'] : '<span class="not-set"> -- </span>' ?>
                                    </td>
                                    <td>
                                        <?= ($data['firstname']) ? $data['firstname'] : '<span class="not-set"> -- </span>' ?>
                                    </td>
                                    <td>
                                        <?= ($data['lastname']) ? $data['lastname'] : '<span class="not-set"> --  </span>' ?>
                                    </td>
                                    <td>
                                        <?= ($data['ip_address']) ? $data['ip_address'] : '<span class="not-set"> --  </span>' ?>
                                    </td>

                                    <td >

                                        <?= ($data['clock_in']) ?  date('H:i:s',strtotime($data['clock_in']))
                                            : '<span class="not-set"> -- </span>' ?>
                                    </td>
                                    <td >

                                        <?= ($data['clock_in']) ?  date('H:i:s',strtotime($data['clock_out']))
                                            : '<span class="not-set"> -- </span>' ?>
                                    </td>
                                    <?php if(!$data['is_clockin']):?>
                                        <td >
                                            <?php

                                            Yii::trace($data['clock_out']);
                                            Yii::trace($data['clock_in']);
                                            $diff =strtotime($data['clock_out'])-strtotime($data['clock_in']);
                                            $hours= floor($diff/(60*60));
                                            $mins= floor(($diff-($hours*60*60))/60);

                                            $duration=$hours.':'.sprintf("%02d",$mins);

                                            echo  ($duration) ?  $duration: '<span class="not-set"> -- </span>'
                                            ?>
                                        </td>
                                    <?php endif;?>



                                </tr>
                            <?php endforeach;
                        else :?>
                            <tr>
                                <td colspan="8">No Teachers found</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>

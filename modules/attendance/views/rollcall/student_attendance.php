<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attendance\models\RollcallSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Attendance Information';
?>
<div class="letters">

<h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title) ?></h1>
<br>

<div class="row" style="background: white">
    <?php echo $this->render('_search', ['searchModel' => $searchModel]); ?>
</div>


<div class="row mt-3">
    <div class="pull-right">
        <?php
        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-pdf', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-danger',
            'target'=>'_blank',
            'data-toggle'=>'tooltip',
            'title'=>'Will open the generated PDF file in a new window'
        ]);
        echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-excel', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-success',
            'target'=>'_blank'
        ]);
        ?>
    </div>

</div>




<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <?php
                $x=1;
                ?>
                <tr>
                    <th></th>
                    <th class='clink'><?= $sort->link('date_rollcalled') ?></th>
                    <th class='clink'>Roll Called By</th>
                    <th class='clink'>Class</th>
                    <th class='clink'>Subject</th>
                    <th class='clink'><?= $sort->link('student_code') ?></th>
                    <th class='clink'><?= $sort->link('first_name', ['label' => 'Student Name']) ?></th>
                    <th class='clink'><?= $sort->link('school_student_registration_number', ['label' => 'Reg No.']) ?></th>
                    <th class='clink'>Attendance</th>


                </tr>
                </thead>
                <tbody>
                <?php

                if ($dataProvider) :
                    foreach ($dataProvider as $k => $v) : ?>
                        <tr data-key="0">
                            <td><?php echo $x++?></td>
                            <td>
                                <?= ($v['date_rollcalled']) ? $v['date_rollcalled'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td>
                                <?= ($v['username']) ? $v['username'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td>
                                <?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td>
                                <?= ($v['subject_name']) ? $v['subject_name'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td class="clink">

                                <?= ($v['student_code']) ? '<a href="' . Url::to(['/schoolcore/core-student/view', 'id' => $v['id']]) . '">' . $v['student_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>

                            <td>

                                <?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>

                            <td>

                                <?= ($v['school_student_registration_number']) ? $v['school_student_registration_number'] : '<span class="not-set">(not set) </span>' ?></td>

                            <td>
                                <?= ($v['student_attended']) ? "Present" : "Absent" ?>

                            </td>

                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No student found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>


    </div>
</div>

</div>

<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\studentreport\models\CoreSchoolCircularSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core School Circulars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">

    <?php

    $columns = [
        ['class' => 'yii\grid\SerialColumn'],

        'school_name',
        'description',
        'date_modified',

        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {

                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['sch-view', 'id' => $model['id']], ['class' => 'aclink']);

            },
        ]
    ];
    ?>
    <div class="row">
        <div class="dropdown">
            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                Generate QR codes
                <span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right">
                <li class="clink">
                    <?= Html::a('For Class', ['qr-generator', 'classes' => "true"]) ?>
                </li>
                <li class="clink"><a
                            href="<?= Url::to(['qr-generator', 'circular_id' => 1, 'student_codes' => "true"]) ?>">For
                        Student</a></li>
            </ul>
        </div>
    </div>

    <div class="mt-3">
        <div class="float-right">

        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
            'columns' => $columns,
            'resizableColumns' => true,
//    'floatHeader'=>true,
            'responsive' => true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>

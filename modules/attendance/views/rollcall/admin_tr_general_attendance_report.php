<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\date\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\web\Controller;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Teachers General Attendance(Clockin/ClockOut) Report';
?>
<i class="fa fa-info-circle"></i> <?= $this->title . " ( <span style='font-size:14px;'><b>" . date('M d, y', strtotime($from_date)) . " &nbsp;&nbsp;-&nbsp;&nbsp;  " . date('M d, y', strtotime($to_date)) . "</b></span>)" ?>
<br>

<div class="row card" data-title="Attendance Summary">
    <div class="card-body">
        <?php $form = ActiveForm::begin(['action' => ['admin-tr-attendance-report'], 'method' => 'post',
            'id' => 'trans-form',
            'options' => ['class' => 'form-horizontal formprocess']]); ?>


        <div class="container mb-3">
            <h4>Choose date range</h4>
            <div class="row" style="padding: 15px;">
                <div class="col-md-6" >

                    <?php
                    echo DatePicker::widget([
                        'name' => 'date_from',
                        'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : '',
                        'type' => DatePicker::TYPE_RANGE,
                        'name2' => 'date_to',
                        'value2' => (isset($_GET['date_to'])) ? $_GET['date_to'] : '',
                        'options' => ['placeholder' => 'Start date'],
                        'options2' => ['placeholder' => 'End date'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-6" >

                    <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>

                    <div class="col-sm-6 ">
                        <?php
                        $url = Url::to(['/schoolcore/core-school/active-schoollist']);
                        $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                        echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                            'initValueText' => $selectedSchool, // set the initial display text
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => 'Filter School',
                                'id' => 'school_search',
                                'class' => 'form-control',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                            ],
                        ])->label(false); ?>

                    </div>
                    <?php endif;?>
                </div>

            </div>


            <div clas="row" style="padding: 15px;">
                <div class="col">
                    <?= Html::submitButton('Find', ['class' => 'btn btn-success']) ?>

                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>



<div class="row mt-3">
    <div class="pull-right">
        <?php
        echo Html::a('<i class="fa far fa-envelope"></i> Send email', ['email'], [
            'class'=>'btn btn-sm btn-info',
            'data-toggle'=>'tooltip',
            'title'=>'Send an email to all students'
        ]);
        ?>
    </div>


    <div class=" float-right">
        <?php echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['transactions/trans'], ['class' => 'btn btn-back', 'style' => 'color:#fff']); ?>
        <?php echo Html::a('<i class="fa fa-file-excel"></i> Excel', ['transactions/channel-trans-list', 'channellistexcelexport' => 'channellistexcel'], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn-sm btn-success', 'style' => 'color:#fff')); ?>
        <?php echo Html::a('<i class="fa fa-file-pdf"></i> PDF', array('transactions/channel-trans-list', 'channellistexport' => 'channellistpdf'), array('title' => 'Export to PDF', 'target' => '_blank', 'class' => 'btn-sm btn-warning', 'style' => 'color:#fff')); ?>
    </div>


</div>




<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <?php
                $x=1;
                ?>
                <tr>
                    <th></th>


                    <th class='clink'>School Name</th>
                    <th class='clink'>No of Teachers</th>
                    <th class='clink'>Present</th>
                    <th class='clink'>Absent</th>
                    <th class='clink'>% Present</th>
                    <th class='clink'>% absent</th>


                </tr>
                </thead>
                <tbody>
                <?php

                if ($bopres) :

                    Yii::trace($bopres);
                    foreach ($bopres as $data) : ?>
                        <tr data-key="0">
                            <td><?php echo $x++?></td>

                            <td>
                                <?= ($data['school_name']) ? $data['school_name'] : '<span class="not-set"> --  </span>' ?>
                            </td>
                            <td>
                                <?= ($data['tttrs']) ? $data['tttrs'] : '<span class="not-set"> --  </span>' ?>
                            </td>
                            <td>
                                <?= ($data['presents']) ? $data['presents'] : '<span class="not-set"> --  </span>' ?>
                            </td>

                            <td>
                                <?= ($data['absents']) ? $data['absents'] : '<span class="not-set"> --  </span>' ?>
                            </td>
                            <td>
                                <?= ($data['percentage_present']) ? $data['percentage_present'] : '<span class="not-set"> --  </span>' ?>
                            </td>
                            <td>
                                <?= ($data['percentage_absent']) ? $data['percentage_absent'] : '<span class="not-set"> --  </span>' ?>
                            </td>




                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No results found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>


    </div>
</div>



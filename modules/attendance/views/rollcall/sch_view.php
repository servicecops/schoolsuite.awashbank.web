<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $model app\modules\studentreport\models\CoreSchoolCircular */

$this->title = $model->subject;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="classes-view hd-title"
     data-title="<?= $model->subject . ' - <b>' . 0 . '</b> Students' ?>">
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="pull-right">
            <div class="col-xs-12">
                <div class="pull-right">

                    <div class="dropdown">
                        <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                            Generate QR codes
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="clink">
                                <?= Html::a('For Class', ['qr-generator', 'circular_id' => $model->id, 'classes' => "true"]) ?>
                            </li>
                            <li class="clink"><a
                                        href="<?= Url::to(['qr-generator', 'circular_id' => $model->id, 'student_codes' => "true"]) ?>">For
                                    Student</a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <br><br>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('citation') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->citation ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('term') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->termName->term_name ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('subject') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->subject ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('citation') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->citation ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('C.C') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->ccc_description ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= "" ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= "" ?></div>
        </div>
    </div>
</div>






<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Add School Information</h3></div>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div style="padding: 10px;width:100%"></div>
    <div class="row">

        <div class="col-sm-6 ">
            <?php
            $url = Url::to(['core-school/active-schoollist']);
            $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size' => 'sm',
                'theme' => Select2::THEME_BOOTSTRAP,
                'class' => 'form-control  input-sm inputRequired',
                'options' => [
                    'placeholder' => 'Filter School',
                    'id' => 'school_search',
                    'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#grading-class_id" ).html(data);
                        });'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>

        </div>

    </div>
    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <br>
        <hr class="l_header" style="margin-top:15px;">

    </div>
    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <?= $this->render('_grades', ['model' => $model]); ?>

    </div>

    <div class="row">
        <div class="col-sm-4">
        </div>
        <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label('') ?>
    </div>


    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

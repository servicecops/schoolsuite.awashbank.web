<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Information';
?>
<div class="letters">
<h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title) ?></h1>
<br>



<div class="row mt-3">

    <div class="pull-right">
        <?php
        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['core-student/export-pdf', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-danger',
            'target'=>'_blank',
            'data-toggle'=>'tooltip',
            'title'=>'Will open the generated PDF file in a new window'
        ]);
        echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-success',
            'target'=>'_blank'
        ]);
        ?>
    </div>
</div>

<?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess'],
    'action' => ['submit-attendance', 'subjectId' => $subjectId]

]); ?>

<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <?php
                $x=1;
                ?>
                <tr>

                    <th></th>
                    <th class='clink'><?= $sort->link('student_code') ?></th>
                    <th class='clink'><?= $sort->link('first_name', ['label' => 'Student Name']) ?></th>
                    <th class='clink'><?= $sort->link('school_student_registration_number', ['label' => 'Reg No.']) ?></th>
                    <th class='clink'>Present</th>
                    <th style="padding:0 10px;"><span class="checkbox checkbox-info checkbox-circle" style="margin:6px;"><input type="checkbox" name="select-all" id="select-all" />
<label for="check_all_id1">Select All</label></span></th>


                </tr>
                </thead>
                <tbody>
                <?php

                if ($dataProvider) :
                    foreach ($dataProvider as $k => $v) : ?>
                        <tr data-key="0">

                            <td><?php echo $x++?></td>
                            <td class="clink">
                                <input type="hidden" name =" Rollcall[<?= $v['id'];?>][studentid] "  value ="<?= $v['id']?>"/>

                                <?= ($v['student_code']) ? '<a href="' . Url::to(['/schoolcore/core-student/view', 'id' => $v['id']]) . '">' . $v['student_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>

                            <td>

                                <?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>

                            <td>

                                <?= ($v['school_student_registration_number']) ? $v['school_student_registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                          <td>
                              <div class="checkbox checkbox-danger checkbox-circle" style="margin:6px;">
                                 <input
                                          type="checkbox" id="CoreSchool-default_part_payment_behaviour"
                                          name=" Rollcall[<?= $v['id'];?>][present]"
                                          value="1" <?= ($searchModel->student_attended) ? 'checked' : '' ?> >
                              </div>
                            </td>
                            <td></td>
                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No student found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>


    </div>
</div>

<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton( 'Submit' , ['class' => 'btn btn-block btn-primary']) ?>

        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>
</div>
<?php ActiveForm::end(); ?>
<?php
$script = <<< JS

$('#select-all').click(function(event) {   
    if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
            this.checked = true;                        
        });
    } else {
        $(':checkbox').each(function() {
            this.checked = false;                       
        });
    }
});




JS;
$this->registerJs($script);
?>

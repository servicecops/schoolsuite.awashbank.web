<?php

use app\models\ImageBank;
use Da\QrCode\QrCode;
use yii\bootstrap4\Button;
use yii\helpers\Html;

use app\assets\SiteAsset;
use yii2assets\printthis\PrintThis;

/* @var $this yii\web\View */

?>



<?php if ($circular_data): ?>
<div id="PrintPage">
    <p>
        <?php
        echo PrintThis::widget([
            'htmlOptions' => [
                'id' => 'PrintPage',
                'btnClass' => 'btn btn-info',
                'btnId' => 'PrintPage',
                'btnText' => 'Print QR codes',
                'btnIcon' => 'fa fa-print'
            ],
            'options' => [
                'debug' => false,
                'importCSS' => true,
                'importStyle' => false,
                'loadCSS' => "path/to/my.css",
                'pageTitle' => "",
                'removeInline' => false,
                'printDelay' => 333,
                'header' => null,
                'formValues' => true,
            ]
        ]);
        ?>
    </p>

    <div id="btnPrintThis">


<?php
/** @var TYPE_NAME $circular_data */
foreach ($circular_data as $data):
    ?>


    <div class="row qr-container mt-0">

        <div class="col">
            <section class="qr_section">
                <div id="back" class="qr_card">
                    <div class="top">
                    </div>
                    <div class="mid">
                        <div class="inner-div">



                            <div class="icons">
                                <div class="center-school-badge ml-5">
                                        <?= Html::img('@web/web/img/icon.png', ['alt' => 'logo','class'=>'ml-5']); ?>
                                </div>
                                <div class="icon"><i class="fa fa-envelope"></i><span class="text-dark"><?= $data['school_name']; ?></span></div>
                                <div class="icon"><i class="fa fa-home"></i><span><?= $data['first_name'].' '. $data['last_name']?></span></div>
                                <div class="icon"><i class="fa fa-globe"></i><span><?= $data['class_name']?></span></div>
                            </div>
                            <div class="qr">
                                <?php
                                $qrCode = (new QrCode($data['student_code']))
                                    ->setSize(100)
                                    ->setMargin(5);
                                echo '<img style="float: right;" src="' . $qrCode->writeDataUri() . '">';
                                ?>
                            </div>

                        </div>
                    </div>
                    <div class="bottom">
                        <div class="inner-div"></div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col">
            <section class="first qr_section">
                <div id="front" class="qr_card">
                    <div class="top"></div>
                    <div class="qr_logo">
                            <?= Html::img('@web/web/img/PNG1.png', ['alt' => 'logo', 'height'=>60]); ?>
                        <h1> <?= $data['school_name']; ?></h1>
                        <h2>If found, return it to schoolsuite, email us on contactcenter@awashbank.com</h2>
                        <h2>Awash Towers, Right Side <br></h2>
                        <h2>EMAIL: contactcenter@awashbank.com || PHONE: 0115-57-13-24</h2>
                    </div>
                    <div class="bottom">
                        <div class="inner-div"></div>
                    </div>
                </div>
            </section>
        </div>


    </div>
    <hr>

<?php endforeach; ?>
    </div>
</div>
<?php elseif (!$circular_data) : ?>

<div class="text-center">
    <?= Yii::$app->session->setFlash('danger', 'No records found');?>
</div>
<p><?= \yii\helpers\Html::a( 'Back to previous page', Yii::$app->request->referrer);?></p>
<?php endif; ?>


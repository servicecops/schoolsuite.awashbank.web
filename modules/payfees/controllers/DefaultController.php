<?php

namespace app\modules\payfees\controllers;

use app\models\AirtelPaymentRequestsSent;
use app\models\NchePaymentsReceived;

use app\modules\messages\models\MsgTopupTransactionsReceived;
use app\modules\payfees\models\MtnPaymentRequestsSent;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\BaseObject;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\httpclient\Client;
use yii\web\Response;

if (!Yii::$app->session->isActive) session_start();

/**
 * LoansController implements the CRUD actions for SaccoSchoolfeesLoan model.
 */
class DefaultController extends \app\controllers\DefaultController
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['select-channel', 'get-student', 'pay-fees', 'confirm-payment', 'check-request'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actionSelectChannel()
    {
        $this->layout = '@app/views/layouts/web_site';
        $channels = PaymentChannels::find()->where(['in', 'channel_code', ['AIRTEL_MONEY_UG', 'MTN_UG']])->all();
        return $this->render('select_channel', ['channels' => $channels]);
    }


/*    public function actionGetStudent($chn = null)
    {
        $this->layout = '@app/views/layouts/web_site';
        if ($chn) {
            $_SESSION['pay-fees']['channel'] = PaymentChannels::find()->where(['id' => $chn])->limit(1)->one();
        }

        $model = new DynamicModel(['pc_rgnumber']);
        $model->addRule(['pc_rgnumber'], 'required');
        $model->addRule(['pc_rgnumber'], 'safe');
        $error = "";
        $student = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try{
                $client = new Client();
                $baseUrl = Yii::getAlias('@getStudent');
                $channel = $_SESSION['pay-fees']['channel'];
                $param = rawurlencode($model->pc_rgnumber);
                $url = "{$baseUrl}/{$channel->channel_code}/{$param}";
                Yii::trace($url);

                $response = $client->createRequest()
                    ->setUrl($url)
                    ->send();
                $content = json_decode($response->getContent());


                Yii::trace($content);

                if($content && ($content->returnCode !=0)){
                    $error = $content->returnMessage;
                }

                if ($content && ($content->returnCode == 0)) {
                    $_SESSION['pay-fees']['student'] = $content;
                    return $this->redirect(['pay-fees']);
                }
            } catch (\Exception $e){
                Yii::trace($e);
                $error = "Service temporarily unavailable, please try again later";
            }

        }
        return $this->render('find_student', ['model' => $model, 'error' => $error]);
    }*/

    public function actionPayFees()
    {
        $this->layout = '@app/views/layouts/web_site';
        $model = new DynamicModel(['phone_number', 'amount']);
        $model->addRule(['phone_number', 'amount'], 'required');
        $model->addRule(['phone_number', 'amount'], 'safe');

        if (isset($_SESSION['pay-fees']['pay'])) {
            $model->amount = $_SESSION['pay-fees']['pay']['amount'];
            $model->phone_number = $_SESSION['pay-fees']['pay']['phone_number'];
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $_SESSION['pay-fees']['pay']['amount'] = $model->amount;
            $_SESSION['pay-fees']['pay']['phone_number'] = $model->phone_number;
            return $this->redirect(['confirm-payment']);
        }
        return $this->render('pay_fees', ['model' => $model]);
    }

    public function actionConfirmPayment($status = null)
    {
        if (!isset($_SESSION['pay-fees'])) {
            return $this->redirect('select-channel');
        }

        $this->layout = '@app/views/layouts/web_site';
        $response = Yii::$app->response;

        if ($status == 'REJECTED') {
            unset($_SESSION['pay-fees']);
            return $this->redirect('select-channel');
        }

        if ($status == 'PROCESSING') {
            $response->format = Response::FORMAT_JSON;
            try {
                $client = new Client();
                $baseUrl = Yii::getAlias('@mtnPaymentApi');
                $pay = $_SESSION['pay-fees']['pay'];
                $student = $_SESSION['pay-fees']['student'];
                $chn = $_SESSION['pay-fees']['channel'];
                $param = rawurlencode($student['payment_code']);
                $url = "{$baseUrl}";
                //$url = "{$baseUrl}/{$chn->channel_code}/{$pay['phone_number']}/{$param}/{$pay['amount']}";
                Yii::trace($url);
                $response = $client->createRequest()
                    ->setHeaders(['content-Type : application/x-www-form-urlencoded' ])
                    ->setUrl($url)
                    ->setMethod('POST')
                    ->setData(['channelCode' =>$chn->channel_code, 'phoneNumber' => $pay['phone_number'],'studentReference'=>$student['payment_code'],'amount'=>$pay['amount']])
                    ->send();
Yii::trace($response);
                $content = json_decode($response->getContent());
                Yii::trace(print_r($content, true));
                if ($content->returnCode != 0) {
                    return ['code' => 101, 'message' => $content->returnMessage];
                }
                $data = $content->returnObject;

                $processingNumber = ($chn->channel_code == 'AIRTEL_MONEY_UG') ?
                    $data->airtelProcessingNumber : $data->mtnProcessingNumber;

                unset($_SESSION['pay-fees']);
                return ['code' => 200, 'mpn' => $processingNumber, 'chn'=>$chn->channel_code];

            } catch (\Exception $e) {
                unset($_SESSION['pay-fees']);
                Yii::trace('Error is ' . $e->getMessage());
                return ['code' => 101, 'message' => $e->getMessage()];
            }
            return ['code' => 101, 'message' => "Could not connect to server. Please Try again"];
        }

        return $this->render('confirm_payment', ['status' => $status]);
    }

    public function actionCheckRequest($mpn, $chn)
    {
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        $record = $chn == "MTN_UG" ?
            MtnPaymentRequestsSent::find()->where(['mtn_processing_number' => $mpn])->limit(1)->one() :
            AirtelPaymentRequestsSent::find()->where(['airtel_processing_number' => $mpn])->limit(1)->one();


//        $record = MtnPaymentRequestsSent::find()->where(['mtn_processing_number' => $mpn])->limit(1)->one();

        if (!$record) {
            return ['code' => 404, 'message' => 'Payment request not yet saved'];
        }


        if (in_array($record->status, ['Requested', 'Pending'])) {
            //Still waiting for approval
            return ['code' => 200, 'message' => $record['payment_request_response']];
        }

        if ($record->status == 'Failed') {
            //Transaction has failed
            return ['code' => 404, 'message' => 'Transaction has failed: ' . $record['payment_request_response']];
        }

        if ($record->status == 'Approved') {
            //Transaction has Approved
            $normalPayment = PaymentsReceived::find()->where(['reciept_number' => strval($record->approval_payment_id)])->limit(1)->one();
            if(!$normalPayment) {
                //Try NCHE
                $normalPayment = NchePaymentsReceived::find()->where(['reciept_number' => strval($record->approval_payment_id)])->limit(1)->one();
            }
            if(!$normalPayment) {
                //Try MSgs
                $normalPayment = MsgTopupTransactionsReceived::find()->where(['schoolpay_receipt_number' => strval($record->approval_payment_id)])->limit(1)->one();
            }
            if(!$normalPayment) {
                //Try zeepay
                return ['code' => 100, 'bs64Url' => '', 'transactionId' => $record->approval_transaction_id];
            }
            $payment_id = $normalPayment->id;
            return ['code' => 100, 'bs64Url' => base64_encode($payment_id), 'transactionId' => $record->approval_transaction_id];
        }

    }

    protected function classModels($id)
    {
        $classes = [
            'incoming-transactions'=>[
                'search'=>'app\modules\payfees\models\MtnIncomingTransactionsSearch',
                'model'=>'app\modules\payfees\models\MtnIncomingTransactions',
                'except'=>['create', 'update', 'delete'],
                'rights'=>['index'=>'super_admin', 'view'=>'super_admin' ]
            ],
            'sent-payment-request'=>[
                'search'=>'app\modules\payfees\models\MtnPaymentRequestsSentSearch',
                'model'=>'app\modules\payfees\models\MtnPaymentRequestsSent',
                'except'=>['create', 'update', 'delete'],
                'rights'=>['index'=>'super_admin', 'view'=>'super_admin' ]
            ],
            'airtel-sent-payment-request'=>[
                'search'=>'app\models\AirtelPaymentRequestsSentSearch',
                'model'=>'app\models\AirtelPaymentRequestsSent',
                'except'=>['create', 'update', 'delete'],
                'rights'=>['index'=>'super_admin', 'view'=>'super_admin' ]
            ],
        ];

        return $classes[$id];
    }
    public function actionGetStudent($chn = null)
    {
        $this->layout = '@app/views/layouts/web_site';
        if ($chn) {
            $_SESSION['pay-fees']['channel'] = PaymentChannels::find()->where(['id' => $chn])->limit(1)->one();
        }

        $model = new DynamicModel(['pc_rgnumber']);
        $model->addRule(['pc_rgnumber'], 'required');
        $model->addRule(['pc_rgnumber'], 'safe');
        $error = "";
        $student = null;
        $content= null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {

                $data = Yii::$app->request->post();

                $stdTxn = Yii::$app->db->createCommand(' SELECT si.id, si.student_code as payment_code, si.first_name, si.last_name,gl.outstanding_balance, cls.class_code, sch.school_name, si.student_email, si.student_phone,  si.first_name, si.middle_name,  si.last_name, si.school_student_registration_number FROM core_student si INNER JOIN core_school_class cls ON cls.id=si.class_id INNER JOIN core_school sch ON sch.id=cls.school_id 
INNER JOIN student_account_gl gl on gl.id = si.student_account_id
                    WHERE ((si.archived=FALSE)  AND (student_code= :code)) 
                   OR (schoolpay_paymentcode=:code) 
                   ORDER BY si.student_code LIMIT 20')
                    ->bindValue(':code', $data['DynamicModel']['pc_rgnumber'])
                    ->queryAll();

                if ($stdTxn) {


                $content = $stdTxn[0];
                }

                Yii::trace($content);

                if(!$content){
                    $error = 'No student Found, please contact support@schoolsuite.co.ug for help';
                }

                if ($content) {
                    $_SESSION['pay-fees']['student'] = $content;
                    return $this->redirect(['pay-fees']);
                }
            } catch (\Exception $e){
                Yii::trace($e);
                $error = "Service temporarily unavailable, please try again later";
            }

        }
        return $this->render('find_student', ['model' => $model, 'error' => $error]);
    }
}

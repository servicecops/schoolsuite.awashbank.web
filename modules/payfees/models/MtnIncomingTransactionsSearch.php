<?php

namespace app\modules\payfees\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\payfees\models\MtnIncomingTransactions;
if (!Yii::$app->session->isActive) session_start();

/**
 * MtnIncomingTransactionsSearch represents the model behind the search form about `app\modules\payfees\models\MtnIncomingTransactions`.
 */
class MtnIncomingTransactionsSearch extends MtnIncomingTransactions
{
    public $date_from, $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['date_from', 'date_to', 'service_mtn_ova', 'service_mtn_spid', 'trace_unique_id', 'service_id', 'processing_number', 'acc_ref', 'request_amount', 'payment_ref', 'phone_number', 'status_code', 'date_status_checked', 'status_check_result'], 'safe'],
            [['status_checked'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();
        
        $query->select(['id', 'date_created', 'service', 'service_mtn_ova', 'service_mtn_spid', 'trace_unique_id', 'service_id', 'processing_number', 'acc_ref', 'request_amount', 'payment_ref', 'phone_number', 'status_code', 'status_checked', 'date_status_checked', 'status_check_result'])
            ->from('mtn_incoming_transactions');
        $query->andFilterWhere(['between', 'date_created', $this->date_from, $date_to]);

        $query->andFilterWhere(['ilike', 'phone_number', $this->phone_number]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'date_created', 'service', 'service_mtn_ova', 'service_mtn_spid', 'trace_unique_id', 'service_id', 'processing_number', 'acc_ref', 'request_amount', 'payment_ref', 'phone_number', 'status_code', 'status_checked', 'date_status_checked', 'status_check_result'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Mtn Incoming Transactions', $query, $this->getVisibleCols(),  'mtn_incoming_transactions');
        return ['query'=>$query->all(Yii::$app->db2), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Mtn Incoming Transactions', 'search_col_w'=>[2, 7, 3]];
    }


    public function getVisibleCols()
    {
        return [
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
            'payment_ref' => ['label'=>'Payment Ref'],
            'service_mtn_ova' => ['label'=>'Service Mtn Ova'],
            'service_mtn_spid' => ['label'=>'Service Mtn Spid'],
            'processing_number' => ['label'=>'Processing Number'],
            'acc_ref' => ['label'=>'Acc Ref'],
            'phone_number' => ['label'=>'Phone Number'],
            'status_checked' => ['type'=>'number', 'label'=>'Status Checked'],
            'request_amount' => ['type'=>'number', 'label'=>'Request Amount'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],
                ]
            ]
        ]; 
    }

    public function getSearchCols()
    {
        return [
            'date_from' => ['type'=>'date', 'label'=>'From'],
            'date_to' => ['type'=>'date', 'label'=>'To'],
            'phone_number' => ['label'=>'Phone Number'],
        ]; 
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

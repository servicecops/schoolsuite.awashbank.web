<?php

namespace app\modules\payfees\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\payfees\models\MtnPaymentRequestsSent;
if (!Yii::$app->session->isActive) session_start();

/**
 * MtnPaymentRequestsSentSearch represents the model behind the search form about `app\modules\payfees\models\MtnPaymentRequestsSent`.
 */
class MtnPaymentRequestsSentSearch extends MtnPaymentRequestsSent
{
    public $date_from, $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mtn_processing_number', 'approval_payment_id'], 'integer'],
            [['date_from', 'msisdn', 'date_to', 'date_payment_request_sent', 'reason', 'amount', 'account_reference', 'status', 'date_approved', 'approval_transaction_id', 'service_id'], 'safe'],
            [['approved'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        
        $query->select(['id', 'mtn_processing_number', 'date_created', 'msisdn', 'payment_request_response', 'date_payment_request_sent', 'reason', 'amount', 'account_reference', 'approved', 'status', 'date_approved', 'approval_transaction_id', 'approval_payment_id', 'service_id'])
            ->from('mtn_payment_requests_sent');
        $query->andFilterWhere([
            'between', 'date_created', $this->date_from, $date_to
        ]);

        $query->andFilterWhere(['ilike', 'msisdn', $this->msisdn])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'mtn_processing_number', 'date_created', 'msisdn', 'payment_request_response', 'date_payment_request_sent', 'reason', 'amount', 'account_reference', 'approved', 'status', 'date_approved', 'approval_transaction_id', 'approval_payment_id', 'service_id'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Mtn Payment Requests Sent', $query, $this->getVisibleCols(),  'mtn_payment_requests_sent');
        return ['query'=>$query->all(Yii::$app->db2), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Mtn Payment Requests Sent', 'search_col_w'=>[2, 7, 3]];
    }


    public function getVisibleCols()
    {
        return [
            'date_created' => ['type'=>'date', 'label'=>'Dated'],
            'reason' => ['label'=>'Reason'],
            'account_reference' => ['label'=>'Account Ref'],
            'msisdn' => ['label'=>'Msisdn'],
            'mtn_processing_number' => ['label'=>'Mtn Processing Number'],
            'status' => ['label'=>'Status'],
            'amount' => ['type'=>'number', 'label'=>'Amount'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],
                ]
            ]
        ]; 
    }

    public function getSearchCols()
    {
        return [
            'date_from' => ['type'=>'date', 'label'=>'From', 'width'=>2],
            'date_to' => ['type'=>'date', 'label'=>'To', 'width'=>2],
            'msisdn' => ['label'=>'Phone Number'],
            'status' => ['type'=>'dropdown', 'options'=>[''=>'Select Status', 'Pending'=>'Pending', 'Failed'=>'Failed', 'Approved'=>'Approved']],
        ];
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

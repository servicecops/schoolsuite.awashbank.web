<?php

namespace app\modules\payfees\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "mtn_incoming_transactions".
 *
 * @property integer $id
 * @property string $date_created
 * @property string $service
 * @property string $service_mtn_ova
 * @property string $service_mtn_spid
 * @property string $trace_unique_id
 * @property string $service_id
 * @property string $processing_number
 * @property string $acc_ref
 * @property string $request_amount
 * @property string $payment_ref
 * @property string $phone_number
 * @property string $status_code
 * @property boolean $status_checked
 * @property string $date_status_checked
 * @property string $status_check_result
 */
class MtnIncomingTransactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mtn_incoming_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_status_checked'], 'safe'],
            [['service_mtn_spid', 'trace_unique_id', 'service_id', 'processing_number', 'phone_number'], 'string'],
            [['status_checked'], 'boolean'],
            [['service', 'service_mtn_ova', 'acc_ref', 'request_amount', 'payment_ref', 'status_code', 'status_check_result'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'service' => 'Service',
            'service_mtn_ova' => 'Service Mtn Ova',
            'service_mtn_spid' => 'Service Mtn Spid',
            'trace_unique_id' => 'Trace Unique ID',
            'service_id' => 'Service ID',
            'processing_number' => 'Processing Number',
            'acc_ref' => 'Acc Ref',
            'request_amount' => 'Request Amount',
            'payment_ref' => 'Payment Ref',
            'phone_number' => 'Phone Number',
            'status_code' => 'Status Code',
            'status_checked' => 'Status Checked',
            'date_status_checked' => 'Date Status Checked',
            'status_check_result' => 'Status Check Result',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Mtn Incoming Transactions',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'service' => ['label'=>'Service'],
                    'service_mtn_ova' => ['label'=>'Service Mtn Ova'],
                    'service_mtn_spid' => ['label'=>'Service Mtn Spid'],
                    'trace_unique_id' => ['label'=>'Trace Unique Id'],
                    'service_id' => ['label'=>'Service Id'],
                    'processing_number' => ['label'=>'Processing Number'],
                    'acc_ref' => ['label'=>'Acc Ref'],
                    'request_amount' => ['label'=>'Request Amount'],
                    'payment_ref' => ['label'=>'Payment Ref'],
                    'phone_number' => ['label'=>'Phone Number'],
                    'status_code' => ['label'=>'Status Code'],
                    'status_checked' => ['type'=>'number', 'label'=>'Status Checked'],
                    'date_status_checked' => ['type'=>'date', 'label'=>'Date Status Checked'],
                    'status_check_result' => ['label'=>'Status Check Result'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'date_created'],
            ['attribute'=>'service'],
            ['attribute'=>'service_mtn_ova'],
            ['attribute'=>'service_mtn_spid'],
            ['attribute'=>'trace_unique_id'],
            ['attribute'=>'service_id'],
            ['attribute'=>'processing_number'],
            ['attribute'=>'acc_ref'],
            ['attribute'=>'request_amount'],
            ['attribute'=>'payment_ref'],
            ['attribute'=>'phone_number'],
            ['attribute'=>'status_code'],
            ['attribute'=>'status_checked'],
            ['attribute'=>'date_status_checked'],
            ['attribute'=>'status_check_result'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Mtn Incoming Transactions" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->phone_number;
    }

    public function getViewActions()
    {
        return [
            ['link'=>Url::to(['index', 'mod'=>'incoming-transactions']), 'title'=>'<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back',
                'htmlOptions'=>[
                    'class'=>'aclink  btn  btn-info btn-sm',
                ]
            ],
        ];
    }

}

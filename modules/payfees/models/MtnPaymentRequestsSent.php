<?php

namespace app\modules\payfees\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "mtn_payment_requests_sent".
 *
 * @property integer $id
 * @property integer $mtn_processing_number
 * @property string $date_created
 * @property string $msisdn
 * @property string $payment_request_response
 * @property string $date_payment_request_sent
 * @property string $reason
 * @property string $amount
 * @property string $account_reference
 * @property boolean $approved
 * @property string $status
 * @property string $date_approved
 * @property string $approval_transaction_id
 * @property integer $approval_payment_id
 * @property string $service_id
 */
class MtnPaymentRequestsSent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mtn_payment_requests_sent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_payment_request_sent', 'date_approved'], 'safe'],
            [['msisdn', 'reason', 'amount'], 'required'],
            [['approved'], 'boolean'],
            [['approval_transaction_id', 'service_id'], 'string'],
            [['approval_payment_id'], 'integer'],
            [['msisdn', 'payment_request_response', 'reason', 'amount', 'account_reference', 'status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mtn_processing_number' => 'Mtn Processing Number',
            'date_created' => 'Date Created',
            'msisdn' => 'Msisdn',
            'payment_request_response' => 'Payment Request Response',
            'date_payment_request_sent' => 'Date Payment Request Sent',
            'reason' => 'Reason',
            'amount' => 'Amount',
            'account_reference' => 'Account Reference',
            'approved' => 'Approved',
            'status' => 'Status',
            'date_approved' => 'Date Approved',
            'approval_transaction_id' => 'Approval Transaction ID',
            'approval_payment_id' => 'Approval Payment ID',
            'service_id' => 'Service ID',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Mtn Payment Requests Sent',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'mtn_processing_number' => ['type'=>'number', 'label'=>'Mtn Processing Number'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'msisdn' => ['label'=>'Msisdn'],
                    'payment_request_response' => ['label'=>'Payment Request Response'],
                    'date_payment_request_sent' => ['type'=>'date', 'label'=>'Date Payment Request Sent'],
                    'reason' => ['label'=>'Reason'],
                    'amount' => ['label'=>'Amount'],
                    'account_reference' => ['label'=>'Account Reference'],
                    'approved' => ['type'=>'number', 'label'=>'Approved'],
                    'status' => ['label'=>'Status'],
                    'date_approved' => ['type'=>'date', 'label'=>'Date Approved'],
                    'approval_transaction_id' => ['label'=>'Approval Transaction Id'],
                    'approval_payment_id' => ['type'=>'number', 'label'=>'Approval Payment Id'],
                    'service_id' => ['label'=>'Service Id'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'mtn_processing_number'],
            ['attribute'=>'date_created'],
            ['attribute'=>'msisdn'],
            ['attribute'=>'payment_request_response'],
            ['attribute'=>'date_payment_request_sent'],
            ['attribute'=>'reason'],
            ['attribute'=>'amount'],
            ['attribute'=>'account_reference'],
            ['attribute'=>'approved'],
            ['attribute'=>'status'],
            ['attribute'=>'date_approved'],
            ['attribute'=>'approval_transaction_id'],
            ['attribute'=>'approval_payment_id'],
            ['attribute'=>'service_id'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Mtn Payment Requests Sent" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->account_reference;
    }

    public function getViewActions()
    {
        return [
            ['link'=>Url::to(['index', 'mod'=>'sent-payment-request']), 'title'=>'<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back',
                'htmlOptions'=>[
                    'class'=>'aclink  btn  btn-info btn-sm',
                ]
            ],
        ];
    }

}

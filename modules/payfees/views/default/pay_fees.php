<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
$student = $channel = null;
if(Yii::$app->session->isActive && isset($_SESSION['pay-fees'])) {
    $student = $_SESSION['pay-fees']['student'];
    $channel = $_SESSION['pay-fees']['channel'];
}
?>


<!-- ======= Contact Section ======= -->
<section id="contact" class="contact" >
    <div class="container">
        <?php if($student && $channel) : ?>
        <?php $form = ActiveForm::begin(); ?>
        <div class="section-title" data-aos="fade-up">
            <h2 class="text-danger">Pay school fees</h2>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                <div class="contact-about">
                    <ul>
                        <li class="active">1. Select channel</li>
                        <li>2. Payment code</span></li>
                        <li>3. <span style="color: #4287f5">Amount and phone</li>
                        <li>4. Process</li>
                    </ul>
                </div>
            </div>


            <div class="col-lg-5 col-md-12" data-aos="fade-up" data-aos-delay="300">
                <form method="post" role="form" class="php-email-form">
                    <div class="form-group">
                        Student No : <?= $student['payment_code'] ?>
                    </div>
                    <div class="form-group">
                        School : <?= $student['school_name'] ?>
                    </div>
                    <div class="form-group">
                        Channel : <?= $channel->channel_name ?>
                    </div>
                    <div class="form-group">
                        Name : <?= $student['first_name'].' '. $student['middle_name'].' '.$student['last_name']  ?>
                    </div>
                    <div class="form-group">
                        <tt>Outstanding Balance :<b> <?= ($student['outstanding_balance'] < 0) ? '<span style="color:red">'. number_format(abs($student['outstanding_balance'])).'</span>' : number_format(abs($student['outstanding_balance']))  ?></b></tt>
                    </div>

                    <div class="form-group">
                        <?= $form->field($model, 'amount', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Amount'] ])->textInput(); ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'phone_number', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Phone Number'] ])->textInput(); ?>
                    </div>
                    <div class="text-center">
                        <?= Html::submitButton('Pay Fee', ['class' => 'btn btn-primary']) ?>
                    </div>
                </form>
            </div>

        </div>
            <?php ActiveForm::end(); ?>
        <?php else: ?>
            <div style="padding:40px 0">
                <h3><a href="<?= Url::to('select-channel')?>">Go Back</a></h3>
            </div>
        <?php endif; ?>
    </div>
</section><!-- End Contact Section -->

<?php
$script = <<< JS
$(document).ready(function(){
    $(".bg-danger").removeClass('bg-danger');
    $("#select-channel").addClass('bg-danger');
    $("#select-channel").addClass('text-white');
 
	
 });
JS;
$this->registerJs($script);
?>
<?php

use app\components\ToWords;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- ======= About Us Section ======= -->

<section id="features" class="features">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2 class="text-danger">Pay school fees</h2>
            <p>Select channel</p>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="300">
            <div class="col-lg-6" data-aos="fade-up" data-aos-delay="150">
                <ul>
                    <li class="active">1. <span style="color: #4287f5">Select channel</span></li>
                    <li>2. Payment code</li>
                    <li>3. Amount and phone</li>
                    <li>3. Process</li>
                </ul>
            </div>
            <div>
                <?php foreach($channels as $k=>$v) : ?>
                <?php if($v['payment_channel_logo']):
                if($v['channel_code'] == 'AIRTEL_MONEY_UG') $v['payment_instructions'] = 'To pay, Dial *185*6#, choose SchoolPay and follow instructions to complete your payment.';
                ?>

            <div class="col">


                <div class="icon-box">
<!--                    <i class="ri-store-line" style="color: #ffbb2c;"></i>-->
                    <?php if($v['channel_code']=='MTN_UG' || $v['channel_code']=='AIRTEL_MONEY_UG') : ?>
                        <a href="<?= Url::to(['get-student', 'chn'=>$v['id']]) ?>">
                            <img class="sch-icon" src="data:image/jpeg;base64,<?= \app\models\ImageBank::findOne(['id'=>$v['payment_channel_logo']])->image_base64 ?>"
                                 height="100" width="100"/>
                        </a>
                    <?php else : $val = Html::decode($v['payment_instructions']) ?>
                        <div data-html="true" data-toggle="popover" title="<b><span style='font-size:20px;align:center;margin-top:100px;'>Instructions</span></b>"  data-content="<?= $val ?>">
                            <img class="img-thumbnail" src="<?= ToWords::createImageLink($v['payment_channel_logo']) ?>" height="100" width="100" />
                        </div>
                    <?php endif; ?>
                    <h3><a href=""><?= "<h5>".$v['channel_name']."</h5>"; ?></a></h3>
                </div>
            </div>


            <?php endif; ?>
            <?php endforeach; ?>
            </div>
        </div>



    </div>
</section>


<?php
$script = <<< JS
$(document).ready(function(){
    $(".bg-danger").removeClass('bg-danger');
    $("#select-channel").addClass('bg-danger');
    $("#select-channel").addClass('text-white');
    $('[data-toggle="popover"]').popover(); 

    $('html').on('click', function(e) {
	  if (typeof $(e.target).data('original-title') == 'undefined' &&
	     !$(e.target).parents().is('[data-toggle="popover"]')) {
	    $('[data-original-title]').popover('hide');
	  }
	});
	
 });
JS;
$this->registerJs($script);
?>

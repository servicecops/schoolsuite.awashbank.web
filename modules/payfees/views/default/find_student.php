<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<!--<div class="" style="margin-bottom:0px;">
    <div class="container">
        <div class="student-search">

            <?php /*$form = ActiveForm::begin([
                'action' => ['get-students'],
            ]); */?>
            <section class="content-header">
                <div class="row col-md-offset-3">
                    <br>
                    <div class="col-xs-12">
                        <span class="pay-step">1.&nbsp;&nbsp;Select channel</span>
                        <span class="pay-step active">2.&nbsp;&nbsp;Payment code</span>
                        <span class="pay-step">3.&nbsp;&nbsp;Amount & Phone</span>
                        <span class="pay-step">4.&nbsp;&nbsp;Process</span>
                        <h1 style="font-size:21px;">Enter Payment Code/Reg No</h1>
                        <br/>
                    </div>
                    <div class="col-sm-7 col-xs-12">
                        <?/*= $form->field($model, 'pc_rgnumber', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Payment code /Registration Number'] ])->textInput()->label(false); */?>
                    </div>
                    <div class="col-sm-7 col-xs-12"><?/*= Html::submitButton('Search', ['class' => 'btn btn-block']) */?></div>
                    <?php /*if($error) : */?>
                        <div class="col-sm-7 col-xs-12">
                            <br>
                            <span style="color:red"><b><?/*= $error */?></b></span><br><br>
                        </div>
                    <?php /*endif; */?>
                </div>
            </section>

            <?php /*ActiveForm::end(); */?>
            <br/>
        </div>
    </div>
</div>-->

<section id="contact" class="contact">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2 class="text-danger">Payment code</h2>
            <p>search for student by entering the student payment code</p>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                <div class="contact-about">
                    <ul>
                        <li class="active">1. Select channel</li>
                        <li>2. <span style="color: #4287f5">Payment code</span></li>
                        <li>3. Amount and phone</li>
                        <li>3. Process</li>
                    </ul>
                </div>
            </div>



            <div class="col-lg-5 col-md-12" data-aos="fade-up" data-aos-delay="300">
                <?php $form = ActiveForm::begin([
                    'action' => ['get-student'],
                ]); ?>
                    <div class="form-group">
                        <?= $form->field($model, 'pc_rgnumber', ['inputOptions'=>[ 'class'=>'form-control','placeholder'=> 'Payment code /Registration Number'] ])->textInput()->label(false); ?>
                        <div class="validate"></div>

                        <?php if($error) : ?>
                            <div>

                                <div style="color: red"><?= $error ?></div>
                            </div>
                        <?php endif; ?>
                    </div>


                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?php ActiveForm::end(); ?>
            </div>

        </div>

    </div>
</section><!-- End Contact Section -->
<?php
$script = <<< JS
$(document).ready(function(){
    $(".bg-danger").removeClass('bg-danger');
    $("#select-channel").addClass('bg-danger');
    $("#select-channel").addClass('text-white');
 
	
 });
JS;
$this->registerJs($script);
?>
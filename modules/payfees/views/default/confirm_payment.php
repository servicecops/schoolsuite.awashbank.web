<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $channel app\models\PaymentChannels */
/* @var $form yii\widgets\ActiveForm */
$student = $channel = $pay = null;
if (Yii::$app->session->isActive && isset($_SESSION['pay-fees'])) {
    $student = $_SESSION['pay-fees']['student'];
    //$isNche = isset($student->nche_student) && $student->nche_student;
    $channel = $_SESSION['pay-fees']['channel'];
    $pay = $_SESSION['pay-fees']['pay'];
}
Yii::trace($student);
?>

<!--    <div class="" style="margin-bottom:0px;">
        <div class="container" style="background-color: #fffeff">
            <div class="student-search">

                <section class="content-header">
                    <div class="row col-md-offset-4">
                        <div class="col-sm-7 col-xs-12">
                            <br/>
                            <div id="confirm_payment">
                                <div class="col-xs-12">
                                    <h3>Confirm Payment</h3>
                                    You are about to make a payment to <?/*= $student ? $student['school_name'] : '--'; */?>
                                    of <code><b>Birr <?/*= $pay ? number_format($pay['amount']) : '--' */?></b></code>
                                    for <b><?/*= $student ? $student['first_name']. ' '.$student['middle_name'] .' '.$student['last_name']   : '--' */?></b>
                                    using <?/*= $pay ? $pay['phone_number'] : "--" */?>

                                    <br>You will need your phone in hand to complete this payment

                                </div>
                                <div class="col-xs-12 no-padding">
                                    <br/>
                                    <div class="col-xs-6">
                                        <a class="btn btn-block btn-default"
                                           href="<?/*= Url::to(['confirm-payment', 'status' => 'REJECTED']) */?>">Reject</a>
                                    </div>
                                    <div class="col-xs-6">
                                        <a class="btn btn-block btn-success confirm_payment"
                                           href="javascript:;">Confirm</a>
                                    </div>
                                </div>
                            </div>

                            <div id="waiting_to_process" class="col-xs-12 text-center"
                                 style="display: none; padding:40px 0">
                                <h1>Please Wait ...</h1>
                            </div>

                            <div id="processing" class="col-xs-12" style="display: none">
                                <img style="display: block; margin: auto"
                                     src="<?/*= Yii::$app->request->baseUrl */?>/web/img/Blocks-1s-200px.gif">

                                <?php /*if ($channel->channel_code == "MTN_UG"): */?>
                                    <div style="margin-top:-30px;">
                                        <h3>Please approve the payment on your phone</h3>
                                        Enter your Mobile Money PIN on your phone number <?/*= $pay['phone_number'] */?> <br/>
                                        Or Dial *165#<br/>
                                        Select my account<br/>
                                        Select My Approvals
                                    </div>
                                <?php /*endif; */?>


                                <?php /*if ($channel->channel_code == "AIRTEL_MONEY_UG"): */?>
                                    <div style="margin-top:-30px;">
                                        <h3>Please approve the payment on your phone with your Airtel Money PIN</h3>
                                        We have sent a transaction request to your phone number <?/*= $pay['phone_number'] */?><br/>
                                    </div>
                                <?php /*endif; */?>

                            </div>

                            <div id="processing_complete" class="col-xs-12" style="display: none">
                            </div>

                            <div id="processing_successful" class="col-xs-12" style="display: none">
                                <img style="display: block; margin: auto"
                                     src="<?/*= Yii::$app->request->baseUrl */?>/web/img/success.gif"
                                     height="200" width="200">
                                <div id="processing_successful_msg" class="col-xs-12 text-center">
                                </div>
                            </div>

                        </div>

                    </div>
                    <br/>
                </section>

            </div>
        </div>
    </div>-->


    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title" data-aos="fade-up">
                <h2 class="text-danger">Confirm payment</h2>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <div class="contact-about">

                        <ul>
                            <li class="active">1. Select channel</li>
                            <li>2. Payment code</span></li>
                            <li>3. Amount and phone</li>
                            <li>4. <span style="color: #4287f5">Process</li>
                        </ul>
                    </div>
                </div>


                <div class="col-lg-5 col-md-12" data-aos="fade-up" data-aos-delay="300">
                    You are about to make a payment to <?= $student ? $student['school_name'] : '--'; ?>
                    of <code><b>Birr <?= $pay ? number_format($pay['amount']) : '--' ?></b></code>
                    for <b><?= $student ? $student['first_name']. ' '.$student['middle_name'] .' '.$student['last_name']   : '--' ?></b>
                    using <?= $pay ? $pay['phone_number'] : "--" ?>

                    <br>You will need your phone in hand to complete this payment
                    <br>

                    <div class="row">
                        <div class="col">
                            <a class="btn btn-block btn-danger"
                                              href="<?= Url::to(['confirm-payment', 'status' => 'REJECTED']) ?>">Reject</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-block btn-success confirm_payment"
                                             href="javascript:;">Confirm</a>
                        </div>
                    </div>


                </div>




            </div>
            <div id="waiting_to_process" class="col-xs-12 text-center"
                 style="display: none; padding:40px 0">
                <h1>Please Wait ...</h1>
            </div>
            <div id="processing" class="col-xs-12" style="display: none">
                <img style="display: block; margin: auto"
                     src="<?= Yii::$app->request->baseUrl ?>/web/img/Blocks-1s-200px.gif">

                <?php if ($channel->channel_code == "MTN_UG"): ?>
                    <div style="margin-top:-30px;">
                        <h3>Please approve the payment on your phone</h3>
                        Enter your Mobile Money PIN on your phone number <?= $pay['phone_number'] ?> <br/>
                        Or Dial *165#<br/>
                        Select my account<br/>
                        Select My Approvals
                    </div>
                <?php endif; ?>


                <?php if ($channel->channel_code == "AIRTEL_MONEY_UG"): ?>
                    <div style="margin-top:-30px;">
                        <h3>Please approve the payment on your phone with your Airtel Money PIN</h3>
                        We have sent a transaction request to your phone number <?= $pay['phone_number'] ?><br/>
                    </div>
                <?php endif; ?>

            </div>

            <div id="processing_complete" class="col-xs-12" style="display: none">
            </div>

            <div id="processing_successful" class="col-xs-12" style="display: none">
                <img style="display: block; margin: auto"
                     src="<?= Yii::$app->request->baseUrl ?>/web/img/success.gif"
                     height="200" width="200">
                <div id="processing_successful_msg" class="col-xs-12 text-center">
                </div>
            </div>
        </div>
    </section><!-- End Contact Section -->

<?php
$url = Url::to(['/payfees/default/confirm-payment']);
$checkUrl = Url::to(['/payfees/default/check-request']);
$receiptBaseUrl = Url::to(['/site/r']);

//if nche, change receipt url accordingly
//if ($isNche) $receiptBaseUrl = Url::to(['/site/n']); //N is the action for nche receipts

$script = <<< JS
$(document).ready(function(){
     $(".bg-danger").removeClass('bg-danger');
    $("#select-channel").addClass('bg-danger');
    $("#select-channel").addClass('text-white');
    $("a.confirm_payment").on('click', function(e) {
        $("#confirm_payment").hide();
        $("#waiting_to_process").show();
        $.get('$url', {status : 'PROCESSING'}, function( data ) {
            $("#waiting_to_process").hide();
            if(data.code===101){
                $("#processing_complete").html("<span class='col-xs-12 alert alert-danger'>"+data.message+"</span>");
                $("#processing_complete").show();
            } else if(data.code===200) {
                $("#processing").show();
                checkRequest(data.mpn, data.chn);
            }
        });
    });
    
    var checkRequest = function(mpn, chn) {
      $.get('$checkUrl', {mpn : mpn, chn: chn}, function( data ) {
          if(data.code==404){
              $("#processing").hide();
              $("#processing_complete").html("<span class='col-xs-12 alert alert-danger'>"+data.message+"</span>");
              $("#processing_complete").show();
          } else if(data.code==100){
              $("#processing").hide();
              var rcpt_url = '$receiptBaseUrl'+'?i='+data.bs64Url;
              $("#processing_successful_msg").html("<span>Your transaction has been completed succesfully. <br>Transaction ID: "+data.transactionId+" <a href='"+rcpt_url+"' target='_blank'><br/><i class='fa fa-print'></i>&nbsp;&nbsp;Download receipt</a></span>");
              $("#processing_successful").show();
          }else {
              //Still waiting
              setTimeout(function(){
                  checkRequest(mpn, chn); 
              }, 5000);
          }
        });
    }
 });
JS;
$this->registerJs($script);
?>

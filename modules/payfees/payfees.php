<?php

namespace app\modules\payfees;

class Payfees extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\payfees\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
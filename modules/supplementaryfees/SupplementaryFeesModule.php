<?php
namespace app\modules\supplementaryfees;

class SupplementaryFeesModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\supplementaryfees\controllers';
    public function init() {
        parent::init();
    }
}

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeesDueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Supplementary Fees ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row" style="color:#F7F7F7">Fees</div>
<div class="letters">
    <div class="fees-due-index hd-title" data-title="Fees Dues">
              <div class="row">

            <div class="col-md-12">
                <div class="col-sm-3 col-md-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> &nbsp;Supplementary Fees</h3></div>
                <?php
                $col_w = 9;
                ?>
                <div class="col-sm-12 col-md-12 no-padding" style="padding-top: 20px !important;">
                    <?php echo $this->render('_search', ['searchModel' => $searchModel]); ?>
                </div>
                <div class="col-sm-12 col-md-12 no-padding" style="padding-top: 20px !important;">
                    <div class="pull-right">
                        <?php //if(\Yii::$app->user->can('rw_fd')) : ?>
                        <?= Html::a('<i class="fa fa-plus"></i>&nbsp;&nbsp; Add New Fee', ['create'], ['class' => 'aclink btn  btn-info btn-sm']) ?>
                        <?php// endif; ?>
                        <?php
                        echo \yii\bootstrap4\Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-pdf', 'model' => get_class($searchModel)], [
                            'class'=>'btn btn-sm btn-danger',
                            'target'=>'_blank',
                            'data-toggle'=>'tooltip',
                            'title'=>'Will open the generated PDF file in a new window'
                        ]);
                        echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                            'class'=>'btn btn-sm btn-success',
                            'target'=>'_blank'
                        ]);
                        ?>
                    </div>

                </div>

            </div>
            <div class="col-md-12">
                <div class="">
                    <table class="table table-striped">
                        <thead  class="bg-colorz table thead">
                        <tr ><?php echo "<th>".$sort->link('school_name')."</th>"?>
                            <th style="color:white"><?= $sort->link('description') ?></th>
                            <th><?= $sort->link('due_amount') ?></th>
                            <th>Category</th>
                            <th><?= $sort->link('start_date') ?></th>
                            <th><?= $sort->link('end_date') ?></th>

                            <th>&nbsp;Approval Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($dataProvider) :
                            foreach($dataProvider as $k=>$v) : ?>
                                <?php
                                Yii::trace($v);
                                ?>
                                <tr>
                                    <?php ?>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <?php// endif; ?>
                                    <td class="clink"><a href="<?= Url::to(['/supplementary/supplementary-fees/details', 'id'=>$v['id'] ]) ?>"><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></a></td>
                                    <td><?= ($v['due_amount']) ? number_format($v['due_amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['category_name']) ? $v['category_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['start_date']) ? $v['start_date'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['end_date']) ? $v['end_date'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td>
                                        <?php
                                        if( $v['ready_for_approval'] == true && $v['created_by'] != \Yii::$app->user->identity->id && $v['approval_status'] == false){
                                            echo '<a class="aclink" href="'. Url::to(['/supplementary/supplementary-fees//details', 'id'=>$v['id'] ]). '">Approve</a>';
                                        } else if($v['approval_status']==true) {
                                            echo "<i class='fa  fa-check'></i>";
                                        } else{
                                            echo "pending";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="javascript:;" onclick="clink('<?= Url::to(['/supplementary/supplementary-fees/details', 'id'=>$v['id']]) ?>')" title="View" ><span class="fa fa-search"></span></a>

                                    </td>
                                    <td></td>
                                </tr>
                            <?php endforeach;
                        else :
                            ?>
                            <tr><td colspan="7">No Fees set </td></tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>
                <?= LinkPager::widget([
                    'pagination' => $pages['pages'],
                ]);?>


            </div>
        </div>

    </div>
</div>

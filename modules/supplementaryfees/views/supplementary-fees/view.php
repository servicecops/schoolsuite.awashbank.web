<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */
/* @var $model app\models\FeesDue */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="letters">

    <div class="row">
        <div class="col-md-12 fees-due-view">
            <div class="col-md-12 " style="background: #efefef;">
                <div class="wizard">

                    <?php if ($model->created_by == Yii::$app->user->identity->id) : ?>
                        <a href="<?= Url::to(['update', 'id' => $model->id]) ?>" class="aclink col-md-3"><span
                                    class="badge">1</span> Edit Fee Information</a>
                    <?php else: ?>
                        <a class="col-md-3 gray"><span class="badge">1</span> Edit Fee Information</a>
                    <?php endif ?>
                    <a class="current col-md-3"><span class="badge">2</span> Assign Classes /Exceptions</a>
                    <a href="<?= Url::to(['details', 'id' => $model->id]) ?>" class="aclink col-md-3"><span
                                class="badge badge-inverse">3</span> Submit For Approval</a>
                    <a class="col-md-2 gray"><span class="badge">4</span> Approve Fee</a>

                </div>
            </div>


        </div>

    </div>
    <div class="row">

        <div class="col-md-8 no-padding">
            <h3><i class="fa fa-exchange"></i> &nbsp;<?= Html::encode($this->title) ?></h3>
        </div>

        <div class="col-md-4 no-padding">
            <div class="pull-right" style="padding-top:20px;">
                <?= Html::a('Back', ['index'], ['class' => 'btn btn-default btn-sm aclink']) ?>
                <?php if ($model->created_by == Yii::$app->user->identity->id) : ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <div class="row ">
        <div class="col-md-3 profile-text"
             style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('school_id') ?></div>
        <div class="col-md-9 profile-text"><?= ($model->school->school_name) ? Html::a($model->school->school_name, ['/schoolcore/core-school/view', 'id' => $model->school->id]) : "--" ?></div>
    </div>

    <div>
        <div style="width:50%;float:left">
            <div class="row ">
                <div class="col-md-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('due_amount') ?></div>
                <div class="col-md-6 profile-text"><?= $model->due_amount ?></div>
            </div>
        </div>
        <div style="width:50%;float:left">
            <div class="row ">
                <div class="col-sm-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('description') ?></div>
                <div class="col-sm-6 profile-text"><?= $model->description ?></div>
            </div>
        </div>
        <div style="width:50%;float:left">

            <div class=" row ">
                <div class=" col-sm-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('effective_date') ?></div>
                <div class="col-sm-6 profile-text"><?= $model->start_date ?></div>
            </div>
            <div class=" row ">
                <div class=" col-sm-6 profile-text "
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('end_date') ?></div>
                <div class=" col-sm-6 profile-text "><?= $model->end_date ?></div>
            </div>
        </div>
        <div style="width:50%;float:left">
            <div class="row ">
                <div class="col-lg-6 col-md-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('created_by') ?></div>
                <div class="col-lg-6 col-md-6 profile-text"><?= $model->createdBy->username ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('supplementary_fee_category_id') ?></div>
                <div class="col-lg-6 col-md-6 profile-text"><?= $model->supplementary_fee_category_id ?></div>
            </div>

        </div>

    </div>



    <?php $form = ActiveForm::begin([
        'action' => ['view', 'id' => $model->id],
        'method' => 'post',
        'options' => ['class' => 'formprocess', 'id' => 'assign_fee_student_form_modal'],
    ]); ?>


    <h3>Assign Classes</h3><br/>
    <div class="col-md-12">

        <div class="col-md-6 " id="class_options">
            <?= $form->field($model, 'attach_class')->dropDownList(['ALL' => 'Attach All Classes', 'ONEBYONE' => 'Select One By One'],

                ['id' => 'class_option', 'prompt' => 'Selection Type']
            )->label('Class Selection Type *') ?>
        </div>
        <div class="col-md-6" id="class_div">
            <?php
            $url = Url::to(['/feesdue/fees-due/classlist', 'schoolId' => $schoolId]);
            $selectedClass = empty($feeClass->class_id) ? '' : CoreSchoolClass::findOne($model->class_id)->class_description;
            echo $form->field($feeClass, 'class_id')->widget(Select2::classname(), [
                'initValueText' => $selectedClass, // set the initial display text
//                    'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Select Class',
                    'id' => 'feediv',
                    'multiple' => true,
                    'class' => 'form-control',
                    'style' => ['width' => '250px']
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ])->label('Choose Classes'); ?>
        </div>

    </div>


    <div class="form-group col-md-12 ">
        <div class="col-md-6 col-md-offset-3">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>


</div>
<?php
$url = Url::to(['/feesdue/fees-due/credithr-fee']);
$id = $model->id;
$script = <<< JS
    $(document).ready(function(){
         
        
            function evaluateSelectClassVisibility() {
        if($('#class_option').val() === 'ONEBYONE') {
           $('#class_div').show()
        }else {
           $('#class_div').hide() 
        }
}
          
     
          $('#class_div').hide();
            
             //On changing class options type, hide or show select class controls
   var _body = $('body');
          _body.on('change', '#class_options', function(){
           
       evaluateSelectClassVisibility();
   });   
    
             
             
       
   });
JS;
$this->registerJs($script);
?>

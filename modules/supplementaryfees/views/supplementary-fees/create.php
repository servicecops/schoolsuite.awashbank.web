<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\FeesDue */

$this->title = 'Add Supplementary Fee';
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="letter">


        <div class="col-md-12" style="background: #efefef">
            <div class="wizard">
                <a class="current col-md-6"><span class="badge">1</span> Add New Fee</a>
                <a class="col-md-6 gray"><span class="badge">2</span> Assign Classes </a>
            </div>
        </div>


        <div class="col-md-12">


            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
        <div style="clear: both"></div>
    </div>
</div>

<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\supplementaryfees\models\SupplementaryFeeCategories;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeesDue */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */

$disableControls = false;
if(!$model->isNewRecord ) {
    $disableControls = true;
}

?>
<?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>

<div class="row">


    <?php
    if ($this->context->action->id == 'update')
        $action = ['update', 'id' => $_REQUEST['id']];
    else
        $action = ['create'];
    ?>

    <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Please make sure you enter the
        correct START and END date. You will only be able to apply a fee in the period between it's start and
        end date.
    </div>
    <?php $form = ActiveForm::begin([
        'action' => $action,

    ]); ?>

    <div class="col-md-12 col-lg-12 no-padding">

        <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
            <div class="col-sm-6">
                <?php
                $url = Url::to(['../schoolcore/core-school/active-schoollist']);
                $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'class' => 'form-control',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'disabled' => !$model->isNewRecord,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'fee_selected_school_id',

                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label("School *"); ?>
            </div>
        <?php endif; ?>
        <div class="col-sm-6">

            <?php
            $items = ArrayHelper::map(SupplementaryFeeCategories::find()->all(), 'id', 'category_name');
            echo $form->field($model, 'supplementary_fee_category_id')
                ->dropDownList(
                    $items,           // Flat array ('id'=>'label')
                    ['prompt' => 'Select Fee Category',
                        ]    // options
                ); ?>

        </div>


    </div>

    <div class="col-md-12 col-lg-12 no-padding">

        <div class="col-sm-6">
            <?= $form->field($model, 'due_amount', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Fee Amount', 'step' => '0.1', ]])->textInput()->label('Amount *') ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Description']])->textInput()->label('Description *') ?>
        </div>

    </div>


    <div class="col-md-12 col-lg-12 no-padding">

        <div class="col-sm-6">
            <?= $form->field($model, 'start_date')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'emp_joining_date',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('start_date') ? ' is-invalid' : ''),
                        'prompt' => 'Effective Date',

                    ],
                    'dateFormat' => 'yyyy-MM-dd'
                ])->label('Start Date *') ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'emp_joining_date',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('end_date') ? ' is-invalid' : ''),
                        'prompt' => 'Effective Date',
                    ],
                    'dateFormat' => 'yyyy-MM-dd'
                ])->label('End Date *') ?>
        </div>

        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="SupplementaryFees[active]" value="0">
                <input type="checkbox" id="SupplementaryFees-active" name="SupplementaryFees[active]"
                       value="1" <?= ($model->active) ? 'checked' : '' ?> >
                <label for="SupplementaryFees-active">Active</label>
            </div>
        </div>


    </div>

    <div class="form-group col-md-12 col-sm-6 col-lg-4 no-padding">
        <div class="col-md-6">
            <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update',
                ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

$script = <<< JS



$("document").ready(function(){ 
    function evaluateRecurrentVisibility() {
        if($('#fee_type_select').val() === '1') {
           $('#recurrent-settings-container').show()
        }else {
           $('#recurrent-settings-container').hide() 
        }
}

    function evaluatePenaltyRecurrentVisibility() {
        if($('#penalty_frequency_select').val() === 'RECURRENT') {
           $('.penalty_recurrent_settings_container').show()
        }else {
           $('.penalty_recurrent_settings_container').hide() 
        }
}

function evaluatePenaltyAmountVisibility() {
        var _penaltyTypeSelect = $('#penalty_type_select');
        var _FixedAmountContainer = $('#_fixed_amount_container');
        var _VaryingAmountContainer = $('#_variable_amount_container');
        
        if(_penaltyTypeSelect.val() === 'FIXED' || _penaltyTypeSelect.val() === 'PERCENTAGE') {
           
           _FixedAmountContainer.show()
        }else {
          _FixedAmountContainer.hide() 
        }
        
        
        if(_penaltyTypeSelect.val() === 'VARYING_AMOUNT' || _penaltyTypeSelect.val() === 'VARYING_PERCENTAGE') {
           _VaryingAmountContainer.show()
        }else {
          _VaryingAmountContainer.hide() 
        }
}

function evaluatePenaltyControls() {
        if($('#has_penalty_select').is(":checked")) {
           $('#arrears-settings-container').show()
        }else {
           $('#arrears-settings-container').hide() 
        }
}

evaluateRecurrentVisibility();
evaluatePenaltyRecurrentVisibility();
evaluatePenaltyControls();
evaluatePenaltyAmountVisibility();

    //On changing fee type, hide or show recurrent controls
   var _body = $('body');
   
   _body.on('change', '#fee_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   _body.on('change', '#penalty_frequency_select', function(){
       evaluatePenaltyRecurrentVisibility();
   });   
   
   _body.on('change', '#has_penalty_select', function(){
       evaluatePenaltyControls();
   });
   
      _body.on('change', '#penalty_type_select', function(){
       evaluatePenaltyAmountVisibility();
   });
   
    });
   


JS;
$this->registerJs($script);
?>

<?php

use app\models\SchoolInformation;
use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeesDueSearch */
/* @var $form yii\widgets\ActiveForm */

$schoolUser = \app\components\ToWords::isSchoolUser();
?>

<div class="fees-due-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess']
    ]); ?>

    <div class="row">
        <?php if (!$schoolUser) : ?>
            <div class="col-md-4 no-padding">
                <?php
                $url = Url::to(['/schoolcore/core-school/schoollist']);
                $selected = empty($searchModel->school_id) ? '' :CoreSchool::findOne($searchModel->school_id)->school_name;
                echo $form->field($searchModel, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selected, // set the initial display text
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'size' => 'sm',
                    'options' => [
                        'placeholder' => 'Select School',
                        'id' => 'search_school_id',
                        'class' => 'form-control',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                    ],
                ])->label(false);
                ?>

            </div>
        <?php endif; ?>
        <div class="col-md-4 no-padding">
            <?= $form->field($searchModel, 'description', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Fees description'])->label(false) ?>
        </div>

        <div class="col-sm-4 no-padding">
            <?= Html::submitButton("<i class='fa fa-search'></i>", ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

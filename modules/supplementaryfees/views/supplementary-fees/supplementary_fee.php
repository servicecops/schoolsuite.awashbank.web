<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */
/* @var $model app\models\FeesDue */


$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$allowEdit = (($model->created_by == Yii::$app->user->identity->id)) || $admin;

$allowEdit = true; //Allow update by all, as requested by BiB


?>

<div class="row ">
    <div class="col-md-12">
            <div class="wizard">
                <?php if ($allowEdit): ?>
                    <a href="<?= Url::to(['update', 'id' => $model->id]) ?>" class="aclink col-md-3 gray"><span
                                class="badge">1</span> Edit Fee Information</a>
                <?php else: ?>
                    <a class="col-md-3 gray"><span class="badge">1</span> Edit Fee Information</a>
                <?php endif ?>
                <?php if ($allowEdit) : ?>
                    <a href="<?= Url::to(['view', 'id' => $model->id]) ?>" class="aclink col-md-3 gray"><span
                                class="badge">2</span> Assign Classes</a>
                <?php else : ?>
                    <a class="col-md-3 gray"><span class="badge">2</span> Assign Classes</a>
                <?php endif; ?>
                <a class="<?= ($model->ready_for_approval == true) ? 'gray ' : 'current ' ?> col-md-3"><span
                            class="badge badge-inverse">3</span> Submit For Approval</a>

                <a class="<?= ($model->ready_for_approval == true) ? 'current ' : 'gray ' ?> col-md-3"><span
                            class="badge">4</span> Approve Fee</a>

            </div>


    </div>

    <div class="col-md-12">
        <div class="col-md-8 no-padding">
            <h3><i class="fa fa-exchange"></i> &nbsp;<?= Html::encode($this->title) ?></h3>
        </div>


    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-label"><?= $model->getAttributeLabel('school_id') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->school->school_name) ? Html::a($model->school->school_name, ['/schoolcore/core-school/view', 'id' => $model->school->id]) : "--" ?></div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('due_amount') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->due_amount ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->description ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('start_date') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->start_date ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('end_date') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->end_date ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('created_by') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->createdBy) ? ucwords($model->createdBy->fullname) . ' (' . $model->createdBy->username . ')' : '--'; ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('supplementary_fee_category_id') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->supplementary_fee_category_id) ? $model->categoryNames->category_name : ' --'?></div>
        </div>
    </div>



</div>
<p><br></p>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12  no-padding">
            <div class="col-md-3 table-responsive no-padding">
                <?php

                echo "<table class ='table-bordered table'>";
                echo "<tr style='background-color:#F9F9F9'>";
                echo "<th class='text-center'> Selected Classes </th>";
                echo "<th class='text-center'> Remove </th>";
                echo "</tr>";
                if (!empty($class_fee)) {
                    foreach ($class_fee as $k => $v) {
                        $cid = $v['cid'];
                        $url = Html::a('<i class="fa fa-remove"></i>', ['remove-assigned-class', 'cid' => $cid, 'id' => $id]);
                        echo "<tr onclick=\"
                $.urlParam = function(name){
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    if (results==null){
                       return null;
                    }
                    else{
                       return results[1] || 0;
                    }
                }
                var urlval = $.urlParam('id');
                $('.tr_active').removeClass('tr_active');
                $(this).addClass('tr_active');
                $.get( '../fees-due/lists?clid=" . $v['cid'] . "&fid='+urlval, function( data ) {
                $( 'div#exempt' ).html(data);
                $(this).removeAttr('style');
            });\"
        >";
                        echo "<td class='text-center'>" . $v['cdes'] . "</td>";
                        echo "<td class='text-center'> " . $url . "</td>";

                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td class='text-center'>No Class Assigned this fee</td>";
                    echo "</tr>";

                }
                echo "</table>";
                ?>

                <div><?= LinkPager::widget(['pagination' => $cl_pages]); ?></div>
            </div>



        </div>
    </div>
    <div class='col-md-12'>
        <div class='pull-right'>

            <?php //if(!$admin) : ?>
            <?php if ($model->ready_for_approval == false && ($model->created_by == Yii::$app->user->identity->id || $admin)) : ?>
                <?= Html::a('Submit For Approval', ['ready-toapprove', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-block',
                    'data' => [
                        'confirm' => 'Are you sure you want to Submit  this fee for approval?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php elseif (($model->approval_status == false) && ($model->ready_for_approval == true) && ($model->created_by != \Yii::$app->user->identity->id || $admin) && !\Yii::$app->user->can('is_school_operator')) : ?>
                <?= Html::a('Approve', ['approve', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-block',
                    'data' => [
                        'confirm' => 'Are you sure you want to Approve  this fee ?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
            <?php //endif; ?>
        </div>
    </div>
       <div style="clear: both"></div>

<?php
$script = <<< JS
$("document").ready(function(){ 
   $.urlParam = function(name){
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    if (results==null){
                       return null;
                    }
                    else{
                       return results[1] || 0;
                    }
                }
                var urlval = $.urlParam('id');
    $('#approvalReady').click(function(){
      $.post( '../fees-due/ready-toapprove?id='+urlval, function() {
                  $( '#approvalReady' ).prop("disabled",true);
    });
  });
});
JS;
$this->registerJs($script);
?>

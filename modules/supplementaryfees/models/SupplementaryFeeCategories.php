<?php

namespace app\modules\supplementaryfees\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $category_name
 * @property int $student_id
 * @property string|null $category_description

 * @property int|null $created_by
 */
class SupplementaryFeeCategories extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'supplementary_fee_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'category_description'], 'required'],
            [['date_created'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_description' => 'Category Description',
            'category_name' => 'Category Name',

        ];
    }




    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('library_catalogue_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {

        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
                $upload = new CatalogueUploads();
                $fname = $file->baseName . '.' . $file->extension;

                $tmpfile_contents = file_get_contents($file->tempName);
                Yii::trace($tmpfile_contents);
                $upload->submission_id = $this->id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);

            }
        }
    }


    public function saveAndUpdateUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            //ofcourse first delete old uploads n then upload the new ones
            //simplest way to effect file uploads


            foreach ($this->uploads as $file) {
                $upload = new CatalogueUploads();
                $fname = $file->baseName . '.' . $file->extension;

                $tmpfile_contents = file_get_contents($file->tempName);
                Yii::trace($tmpfile_contents);
                $upload->submission_id = $this->id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);

            }
        }
    }

}

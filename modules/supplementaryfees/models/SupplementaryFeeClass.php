<?php

namespace app\modules\supplementaryfees\models;

use app\modules\schoolcore\models\CoreSchoolClass;
use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $fee_id
 * @property integer $class_id
 * @property boolean $applied
 * @property string $created_by
 *
 * @property SupplementaryFeeClass $fee
 * @property CoreSchoolClass $class
 * @property StudentAccountTransactionHistory[] $studentAccountTransactionHistories
 */
class SupplementaryFeeClass extends \yii\db\ActiveRecord
{
    public $concessions;
    public $selected_classes;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplementary_fee_class_association';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['fee_id', 'class_id'], 'integer'],

            [['created_by'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'fee_id' => 'Fee',
            'class_id' => 'Class',
            'applied' => 'Applied',
            'created_by' => 'Created By',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeDue()
    {
        return $this->hasOne(FeesDue::className(), ['id' => 'fee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeClass()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAccountTransactionHistories()
    {
        return $this->hasMany(StudentAccountTransactionHistory::className(), ['fee_association_id' => 'id']);
    }

}

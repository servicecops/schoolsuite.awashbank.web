<?php

namespace app\modules\supplementaryfees\models;

use app\components\ToWords;
use app\models\User;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreUser;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "institution_fees_due".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $school_id
 * @property string $due_amount
 * @property string $start_date
 * @property string $end_date
 * @property string $description
 * @property boolean $delete_flag
 * @property integer $priority
 * @property integer $created_by
 *
 * @property InstitutionFeeClassAssociation[] $institutionFeeClassAssociations
 * @property CoreSchool $school
 * * @property boolean $recurrent
 * @property boolean $child_of_recurrent
 * @property string $next_apply_date
 * @property string $last_apply_date
 * @property integer $apply_frequency
 * @property string $apply_frequency_unit
 * @property integer $parent_fee_id
 * @property boolean $has_penalty
 * @property integer $penalty_id
 * @property integer $arrears_after_count
 * @property string $arrears_after_units
 * @property string $arrears_start_date
 * @property string approval_status
 */
class SupplementaryFees extends \yii\db\ActiveRecord
{

    public $modelSearch;
    public $Concessions, $concessions_id,$recipient_group, $message_type;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_supplementary_fees';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'start_date', 'end_date',], 'safe'],
            [['due_amount', 'start_date', 'end_date', 'description','supplementary_fee_category_id'], 'required'],
            [['school_id',  'created_by', ], 'integer'],
            [['due_amount', ], 'number'],
            [['description','attach_class' ], 'string'],
            [['active', ], 'boolean'],
            [['school_id'], 'validateRequiredSchoolId', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['start_date', 'end_date'], 'feeDates', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'school_id' => 'School',
            'due_amount' => 'Due Amount',
            'start_date' => 'Effective Date',
            'end_date' => 'End Date',
            'description' => 'Description',
            'delete_flag' => 'Delete Flag',
            'priority' => 'Priority',
            'created_by' => 'Created By',
            'supplementary_fee_category_id' => 'Category',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        //If and recurrent inserting, next apply date should be the start date
//        if($insert && $this->recurrent) {
//            $this->next_apply_date = $this->start_date;
//        }
        if (parent::beforeSave($insert)) {
//            if(\app\components\ToWords::isSchoolUser()) {
//                $this->school_id = Yii::$app->user->identity->school_id;
//            }
            if($this->isNewRecord) {
                $this->created_by = Yii::$app->user->identity->id;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeClassAssociations()
    {
        return $this->hasMany(FeeClass::className(), ['fee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getSelectDesc(){
        return $this->description." - ".number_format($this->due_amount, 2);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getCategoryNames()
    {
        return $this->hasOne(SupplementaryFeeCategories::className(), ['id' => 'supplementary_fee_category_id']);
    }

    /**
     * Will validate the entered registration number against the school's reg no format
     * @param $attribute
     * @param $params
     */
    public function validateRecurrentFields($attribute, $params)
    {
        if (empty($this->recurrent)) {
            //Reg no not passed so just return;
            $this->recurrent = false;
            return;
        }

        //If recurrent if true, apply_frequency and apply_frequency_unit are required
        if($this->recurrent) {
            if(empty($this->apply_frequency)) {
                $this->addError('apply_frequency', 'Select how often this fee should recurrently be applied');
                return;
            }

            if(empty($this->apply_frequency_unit)) {
                $this->addError('apply_frequency_unit', 'Select how often this fee should recurrently be applied');
                return;
            }
        }
    }

    public function validateRequiredSchoolId($attribute, $params)
    {
        if(!\app\components\ToWords::isSchoolUser()
            && empty($this->school_id)) {
            $this->addError('school_id', 'The school is required');
            return;
        }
    }

    /**
     * Will validate penalty fields
     * @param $attribute
     * @param $params
     */
    public function validatePenaltyRequiredFields($attribute, $params)
    {
        if(!$this->has_penalty) {
            $this->arrears_after_count = 1000;
            $this->arrears_after_units = 'M';
            return;
        }

        //If has penalty, then the fields above should be set

        if (empty($this->arrears_after_count)) {
            //Reg no not passed so just return;
            $this->addError('arrears_after_count', 'Select how often the penalty will be applied for this fee');
            return;
        }

        if (empty($this->arrears_after_units)) {
            //Reg no not passed so just return;
            $this->addError('arrears_after_units', 'Select the term of how often the penalty will be applied for this fee');
            return;
        }

    }

    public function getCategoryName()
    {
        return $this->hasMany(SupplementaryFeeCategories::className(), ['id' => 'supplementary_fee_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(FeeClass::className(), ['child' => 'name']);
    }



    /**
     * Will validate the entered registration number against the school's reg no format
     * @param $attribute
     * @param $params
     */


    public function searchConcesions($params,$id)
    {
        $this->load($params);
        //Trim the model searches
        Yii::trace($params);

        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'cls.class_code','fc.id as concession',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            'si.school_student_registration_number'])
            ->from('core_student si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            ->innerJoin('student_fee_concessions sc', 'si.id=sc.student_id')
            ->innerJoin('fee_concessions fc', 'fc.id=sc.concessions_id');

        $query->andWhere(['fc.fee_id' => $id]);
        //

        if (!(empty($params['concessions_id']))) {

            $itemDetails =SchoolConcessions::findOne(['id'=>$params['concessions_id']]);

            Yii::trace($itemDetails);
            $query->andWhere(['sc.fee_id' => $itemDetails->fee_id]);

            //Registration numbers usually have a dash - or slass /
            $query->andWhere(['fc.id'=>$params['concessions_id']]);
            $query->andWhere(['sc.school_id'=>$itemDetails->school_id]);

        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function getPlaceholders()
    {
        $query = (new Query())->from('place_holders')->where(['type'=>'CORE'])->orderBy('type, name')->all();
        return $query;
    }

    public function getRecipientGroup()
    {
        $recipient = '';
        if($this->message_type== 'BY_CLASSES'){
            $classes = explode(', ', $this->recipient_group);
            $classes = array_filter($classes);
            if(count($classes) > 0){
                $q = (new Query())->select('class_code')->from('institution_student_classes')
                    ->where(['in', 'id', $classes])->all();
                $q = array_column($q, 'class_code');
                $recipient = implode(', ', $q);
            }
        } else if($this->message_type== 'STUDENT_GROUP'){
            $groups = explode(', ', $this->recipient_group);
            $groups = array_filter($groups);
            if(count($groups) > 0){
                $q = (new Query())->select('group_name')->from('student_group_information')
                    ->where(['in', 'id', $groups])->all();
                $q = array_column($q, 'group_name');
                $recipient = implode(', ', $q);
            }
        }
        return $recipient;
    }

    public function feeDates($attribute, $params)
    {
        if (!ToWords::validateDate($this->getAttribute($attribute), 'Y-m-d')) {
            $this->addError($attribute, 'Invalid date format. Enter a valid date in the format yyyy-mm-dd');
            return;
        }

        if($this->start_date && $this->end_date && $this->end_date<=$this->start_date) {
            $this->addError('start_date', 'Effective date cannot be after end date');
            $this->addError('end_date', 'End date cannot be before effective date');
            return;
        }
    }
}

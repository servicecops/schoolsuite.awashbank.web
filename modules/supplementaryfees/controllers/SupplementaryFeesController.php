<?php

namespace app\modules\supplementaryfees\controllers;


use app\modules\feesdue\models\FeesDueStudentExemption;
use app\modules\schoolcore\models\Book;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\supplementaryfees\models\SupplementaryFeeClass;
use app\modules\supplementaryfees\models\SupplementaryFees;
use app\modules\supplementaryfees\models\SupplementaryFeesSearch;
use Yii;
use app\controllers\BaseController;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\logs\models\Logs;

use app\modules\schoolcore\models\CoreStudent;
use yii\web\UploadedFile;




if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * CategoryController that handles the lesson plan/syllabus
 */
class SupplementaryFeesController extends BaseController
{


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }




    //you must override these methods to use BaseControler.
    /**
     * Creates a new Category model
     * @return SupplementaryFees
     */
    public function newModel()
    {
        $model = new SupplementaryFees();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return SupplementaryFeesSearch
     */
    public function newSearchModel()
    {
        $searchModel = new SupplementaryFeesSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create Publisher';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }


     /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('rw_fd')) {
            $request = Yii::$app->request;

            $model = new SupplementaryFees();

          //  $penaltyModel = $this->getFeesDuePenalty($model);

            if ($model->load(Yii::$app->request->post())) {
                try {

                    if (\app\components\ToWords::isSchoolUser()) {
                        $model->school_id = Yii::$app->user->identity->school_id;
                    }
                    $model->created_by = Yii::$app->user->identity->getId();

                    if ($model->save(true)) {


                        return $this->redirect(['view', 'id' => $model->id]);
                    }else {
                        \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . 'Error saving');
                    }
                } catch (\Exception $e) {
                    Yii::trace($e);
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    $res = ['model' => $model];
                    return ($request->isAjax) ? $this->renderAjax('create', $res) :
                        $this->render('create', $res);
                }

            }
            $res = ['model' => $model, ];
            return ($request->isAjax) ? $this->renderAjax('create', $res) :
                $this->render('create', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create a FEE.');
        }

    }




    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rw_fd'))
            throw new ForbiddenHttpException('No permissions to update a FEE.');


        $request = Yii::$app->request;
        $model = $this->findModel($id);

//        Yii::trace($penaltyModel);
        $res = ['model' => $model, ];

        if (!Yii::$app->request->post()) {
            return ($request->isAjax) ? $this->renderAjax('update', $res) :
                $this->render('update', $res);
        }
        //Load
        $model->load(Yii::$app->request->post());
        if (!$model->validate()) {
            return ($request->isAjax) ? $this->renderAjax('update', $res) :
                $this->render('update', $res);
        }



        //Save in transaction
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $model->save();
            //If with penalty, save with penalty too

            $transaction->commit();
            Logs::logEvent("updated Fees: " . $model->description, null, null);
            return $this->redirect(['details', 'id' => $model->id]);
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
            $transaction->rollBack();
            $res = ['model' => $model,  'error' => $e->getMessage()];
            return ($request->isAjax) ? $this->renderAjax('update', $res) :
                $this->render('update', $res);
        }
    }




    public function actionDo($action){
        //Yii::trace($action);

        $request = Yii::$app->request;
        $actionModel =new Category();

        $actionModel->load($request->post());
        Yii::trace($actionModel);
        $details = Category::findOne($actionModel['item']);

        if(!$details) throw new Exception('Category details not found, please try again or contact support');
        if($actionModel){
            $id =$actionModel['item'];
            $details->notes = $actionModel['notes'];
            if($action){
                $details->approved = true;
            }else{
                $details->approved = false;
            }
            $details->status ='MARKED';
            $details->approved_by = Yii::$app->user->identity->id;

            $details->save(false);
            $this->runAction(['view',id =>$id]);
        }
    }

    /**
     * Deletes an existing category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SupplementaryFees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SupplementaryFees::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionIndex()
    {
        if (Yii::$app->user->can('r_fd')) {
            $request = Yii::$app->request;
            $searchModel = new SupplementaryFeesSearch();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            $res = ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']];
            return ($request->isAjax) ? $this->renderAjax('index', $res) :
                $this->render('index', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to view FEES.');
        }


    }



    public function actionView($id)
    {
        $model = $this->findModel($id);
        $user_sch = $model->school_id;

        Yii::trace($user_sch);
        if (Yii::$app->user->can('rw_fd') && ($model->school_id == $user_sch || Yii::$app->user->can('schoolpay_admin'))) {
            $request = Yii::$app->request;
            $feeClass = new SupplementaryFeeClass();
            $feeModel = new SupplementaryFeeClass();
            $searchModel = new SupplementaryFees();

//            $exemptedModel = new FeesDueStudentExemption();
//            $exemptions = new FeesDueStudentExemption();
            $cls = (new Query())
                ->select(['class_id'])
                ->from('supplementary_fee_class_association')
                ->where(['fee_id' => $id]);


            Yii::trace("classes");
            $av_classes = CoreSchoolClass::find()->where(['school_id' => $user_sch])->all();

            if ($feeModel->load(Yii::$app->request->post())) {

                $classSelectionData = Yii::$app->request->post();
                if (isset($classSelectionData['SupplementaryFees']['attach_class']) ) {

                    $selectionType = $classSelectionData['SupplementaryFees']['attach_class'];

                    if($selectionType ==='ALL'){
                        foreach ($av_classes as $cls) {
                            Yii::trace($cls->id);
                            $f_class = new SupplementaryFeeClass();
                            $f_class->class_id = $cls->id;
                            $f_class->fee_id = $id;
                            $f_class->created_by = Yii::$app->user->identity->username;
                            $f_class->save(true);


                        }
                    }

                }
            }


            if ($feeClass->load(Yii::$app->request->post())) {

                Yii::trace($av_classes);
                // print_r($av_classes);
                $data = Yii::$app->request->post();
                Yii::trace($data);
                //Yii::trace($data['FeeClass']['class_id']);


                $data = Yii::$app->request->post();



                if (isset($data['SupplementaryFeeClass']['class_id'])) {
//                    throw new UserException("No classes submitted, Please click on skip page to continue");
//
//        if ($feeClass->load($data)) {
                    $feeClassModel = new SupplementaryFeeClass();
                    $feeClassModel->class_id = $data['SupplementaryFeeClass']['class_id'];


//            $feeClas =array();
                    $feeClas = $feeClassModel->class_id;
                    Yii::trace($feeClas);
//
//
                    if (!empty($feeClas)) {
                        //       $clses = Json::decode($data['FeeClass']['class_id']);
                        $clses = array_unique($feeClas);
                        if (!empty($clses)) {
                            $del_class = [];
                            if ($cls->count() > 0) {
                                $fm = $clses;
                                $sv_cl = array_column($cls->all(), 'class_id');
                                $clses = array_diff($clses, $sv_cl);
                                $del_class = array_diff($sv_cl, $fm);
                            }
                            if (!empty(array_filter($clses))) {
                                foreach ($clses as $cls) {
                                    $f_class = new SupplementaryFeeClass();
                                    $f_class->class_id = $cls;
                                    $f_class->fee_id = $id;
                                    $f_class->created_by = Yii::$app->user->identity->username;

                                    $f_class->save();
                                }
                            }
                            if (!empty(array_filter($del_class))) {
                                foreach ($del_class as $cls) {
                                    $fclass = SupplementaryFeeClass::find()->where(['class_id' => $cls])->limit(1)->one();
                                    $fclass->delete();
                                }
                            }
                            $res = ['id' => $id];
                            //   $feeClassModel->save(false);
                        }
                    }
                }


                // $transaction->commit();

                return $this->redirect(['details', 'id'=>$id]);
//
                //return Yii::$app->runAction('/fees-due/details', ['id' => $id]);
            }

            //Load penalty too
            $request = Yii::$app->request;
            $classes = Yii::$app->db->createCommand('SELECT * FROM core_school_class WHERE school_id =:id ')
                ->bindValue(':id', $user_sch)
                ->queryAll();

            $available = array();
            $assigned = array();
            //$classes = CoreSchoolClass::find()->where(['type'=>2])->all();


            foreach ($classes as $class) {
                if (!isset($assigned[$class['id']])) {
                    $available[$class['id']] = $class['class_description'];
                }
            }


            $res = ['model' => $model, 'feeModel' => $feeModel,
                 'feeClass' => $feeClass, 'schoolId' => $user_sch,'searchModel'=>$searchModel,
                'av_classes' => $av_classes, 'cls' => $cls->all(),  'available' => $available, 'assigned' => $assigned, 'classes' => $classes];
            return ($request->isAjax) ? $this->renderAjax('view', $res) :
                $this->render('view', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to view FEES.');
        }

    }

    public function actionDetails($id)
    {
        $model = $this->findModel($id);
        $user_sch = Yii::$app->user->identity->school_id;
        $read = $admin = false;
        {
            if (isset($user_sch) && $model->school_id == $user_sch) {
                $read = true;
            } else if (Yii::$app->user->can('schoolpay_admin')) {
                $read = true;
                $admin = true;
            }
        }
        if (Yii::$app->user->can('r_fd') && $read) {
            $request = Yii::$app->request;
            $fee_class = $this->findFeeClasses($id);

            $cl_pages = new Pagination(['totalCount' => $fee_class->count()]);
            $fee_class->offset($cl_pages->offset);
            $fee_class->limit($cl_pages->limit);
            $class_fee = $fee_class->all();

            Yii::trace($class_fee);

            $feeId = $id;
            $res = ['model' => $model, 'feeId' => $feeId,
                 'class_fee' => $class_fee, 'cl_pages' => $cl_pages,
                'admin' => $admin,  'id'=>$id];

            return ($request->isAjax) ? $this->renderAjax('supplementary_fee', $res) :
                $this->render('supplementary_fee', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to view FEES.');
        }

    }


    protected function findFeeClasses($id)
    {
        $clses = (new Query())
            ->select(['fc.class_id AS cid', 'cl.class_description AS cdes'])
            ->from('supplementary_fee_class_association  fc')
            ->join('join', 'core_school_class cl', 'cl.id = fc.class_id')
            ->where(['fee_id' => $id]);
        return $clses;

    }

    public function actionRemoveAssignedClass($cid,$id)
    {


                try{
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();

                    Yii::$app->db->createCommand("delete from supplementary_fee_class_association where fee_id = :id and class_id =:class_id")
                        ->bindValue(':id', $id)
                        ->bindValue(':class_id', $cid)
                        ->execute();
                    $transaction->commit();
                }catch (\Exception $e){
                    $transaction->commit();
                    Yii::trace("Class removal error: ".$e->getMessage());
                }


        // return $this->redirect(['index']);
        return $this->redirect(['details', 'id'=>$id]);
    }

    public function actionReadyToapprove($id)
    {
        $model = $this->findModel($id);
        $model->ready_for_approval = true;
        $model->save(false);
        Logs::logEvent("Ready to approve: " . $model->description, null, null);
        // return $this->redirect(['details', 'id'=>$id]);
        return $this->redirect(['details', 'id'=>$id]);

        //return Yii::$app->runAction('details', ['id' => $id]);
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $model->approval_status = true;
        $model->save(false);
        Logs::logEvent("Approved: " . $model->description, null, null);
        // return $this->redirect(['details', 'id'=>$id]);

//        $cls = (new Query())
//            ->select(['class_id'])
//            ->from('institution_fee_class_association')
//            ->where(['fee_id' => $id]);
//
//        if ($cls->count() > 0) {
//            foreach ($cls as $class) {
//                $stds = (new Query())
//                    ->select(['id'])
//                    ->from('core_student')
//                    ->where(['class_id' => $id]);
//                foreach ($stds as $std) {
//
//                }
//            }
//        }
        return $this->redirect(['details', 'id'=>$id]);

        // return Yii::$app->runAction('details', ['id' => $id]);
    }

    public function actionListToapprove()
    {
        $pending = FeesDue::find()->where(['ready_for_approval' => true, 'approval_status' => false])->all();

    }

}

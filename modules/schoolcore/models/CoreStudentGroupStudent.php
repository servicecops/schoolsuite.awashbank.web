<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "student_group_student".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $student_id
 * @property string $date_added
 */
class CoreStudentGroupStudent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_group_student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'student_id'], 'required'],
            [['group_id', 'student_id'], 'integer'],
            [['date_added'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'student_id' => 'Student ID',
            'date_added' => 'Date Added',
        ];
    }
}

<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "school_campuses".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $campus_name
 */
class SchoolCampuses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_campuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id','campus_name','physical_address'], 'required'],
            [['school_id'], 'integer'],
            [['campus_name','physical_address'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'campus_name' => 'Campus Name',
            'physical_address' => 'Physical Address',
        ];
    }

    public function formFields()
    {
        return [
            [
                'heading' => 'New School Campuses',
                'header_icon' => 'fa fa-plus',
                'col_n' => 2,
                'fields' => [
                    'id' => ['type' => 'number', 'label' => 'Id'],
                    'school_id' => ['type' => 'number', 'label' => 'School Id'],
                    'campus_name' => ['label' => 'Campus Name'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute' => 'id'],
            ['attribute' => 'school_id'],
            ['attribute' => 'campus_name'],
        ];
    }

    public function getTitle()
    {
        return $this->isNewRecord ? "School Campuses" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //Convert names to upper case
            $this->campus_name = ucwords(trim(strtolower($this->campus_name)));
            return true;
        } else {
            return false;
        }
    }

}

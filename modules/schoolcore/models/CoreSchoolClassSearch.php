<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;


/**
 * CoreSchoolClassSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreSchoolClass`.
 */
class CoreSchoolClassSearch extends CoreSchoolClass
{
    public $test,$answer,$schsearch, $modelSearch;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_id'], 'integer'],
            [['date_created', 'date_updated', 'class_code', 'class_description'], 'safe'],
            [['active'], 'boolean'],
            [['modelSearch'], 'trim'],


        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'Classes',
            'title' => 'Class info Datasheet',
            'exportFile' => '/core-school-class/exportPdfExcel',
        ];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $this->load($params);
        $this->modelSearch = trim($this->modelSearch);

//        ]);
        $query = new Query();
        $query->select(['cls.id', 'cls.class_code', 'cls.class_description','sch.school_name'])
            ->from('core_school_class cls')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            //
            ->Where(['cls.active' => true])
            ->andWhere(['!=', 'cls.class_code', '__ARCHIVE__']);

        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            $query->andWhere(['ilike','class_code', $this->modelSearch]);
            $query->orWhere(['ilike','class_description', $this->modelSearch]);


        }


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        }elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['cls.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'class_code', 'class_description', 'school_name',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('cls.class_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params)
    {
        $searchForm='class_description';

        $query = (new Query())
            ->select(['sc.id','sc.class_code','sc.class_description','csc.school_name','sc.date_created'])
            ->from('core_school_class sc')
            ->innerJoin('core_school csc', 'sc.school_id= csc.id');
        $query->orderBy('sc.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);



        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['distinct("tc"."class_id") '])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();

            Yii::trace($data);
            $the_classId = [];
            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'sc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }


        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);




        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['core-school-class/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fas fa-edit"></i>', ['core-school-class/update', 'id' => $model['id']]);
                    },
            ],
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {

                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-school-class/view', 'id' => $model['id']], ['class'=>'aclink']);

                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

//    public function classStudents($id, $params){
//        $this->load($params);
//        $query = new Query();
//        $query->select(['id', 'student_code', 'first_name', 'last_name', 'middle_name', 'student_email', 'student_phone'])
//            ->from('core_student')
//            ->where(['class_id'=>$id]);
//
//        if(!(empty($this->modelSearch))){
//            if (is_numeric($this->modelSearch)) {
//                $query->andWhere(['student_code' => $this->modelSearch]);
//            }
//            else
//            {
//                $search_words = explode(' ', $this->modelSearch);
//                $search_words = array_filter($search_words);
//                $words = array();
//                if(count($search_words) > 1) {
//                    foreach($search_words as $word) {
//                        $words[] = $word;
//                    }
//                    if(isset($words[0])){
//                        $name = str_replace(' ', '', $words[0]);
//                        $query->andFilterWhere(['or',
//                            ['ilike', 'first_name', $name],
//                            ['ilike', 'middle_name', $name],
//                            ['ilike', 'last_name', $name]
//                        ]);
//                    }
//                    if(isset($words[1])){
//                        $name = str_replace(' ', '', $words[1]);
//                        $query->andFilterWhere(['or',
//                            ['ilike', 'first_name', $name],
//                            ['ilike', 'middle_name', $name],
//                            ['ilike', 'last_name', $name]
//                        ]);
//                    }
//                    if(isset($words[2])){
//                        $name = str_replace(' ', '', $words[3]);
//                        $query->andFilterWhere(['or',
//                            ['ilike', 'first_name', $name],
//                            ['ilike', 'middle_name', $name],
//                            ['ilike', 'last_name', $name]
//                        ]);
//                    }
//
//                }
//                else{
//                    $name = str_replace(' ', '', $this->modelSearch);
//                    $query->andFilterWhere(['or',
//                        ['ilike', 'first_name', $name],
//                        ['ilike', 'middle_name', $name],
//                        ['ilike', 'last_name', $name],
//
//                    ]);
//                }
//            }
//        }
//
//        $pages = new Pagination(['totalCount' => $query->count()]);
//        $sort = new Sort([
//            'attributes' => [
//                'student_code', 'first_name', 'last_name', 'middle_name', 'student_email', 'student_phone'
//            ],
//        ]);
//        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('student_code');
//        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;
//        $query->offset($pages->offset)->limit($pages->limit);
//        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
//    }
    public function classStudents($id, $params){
        $this->load($params);
        $query = new Query();
        $query->select(['id', 'student_code', 'first_name', 'last_name', 'middle_name', 'student_email', 'student_phone'])
            ->from('core_student')
            ->where(['class_id'=>$id]);

        if(!(empty($this->modelSearch))){
            if (is_numeric($this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);
            }
            else
            {
                $search_words = explode(' ', $this->modelSearch);
                $search_words = array_filter($search_words);
                $words = array();
                if(count($search_words) > 1) {
                    foreach($search_words as $word) {
                        $words[] = $word;
                    }
                    if(isset($words[0])){
                        $name = str_replace(' ', '', $words[0]);
                        $query->andFilterWhere(['or',
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name]
                        ]);
                    }
                    if(isset($words[1])){
                        $name = str_replace(' ', '', $words[1]);
                        $query->andFilterWhere(['or',
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name]
                        ]);
                    }
                    if(isset($words[2])){
                        $name = str_replace(' ', '', $words[3]);
                        $query->andFilterWhere(['or',
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name]
                        ]);
                    }

                }
                else{
                    $name = str_replace(' ', '', $this->modelSearch);
                    $query->andFilterWhere(['or',
                        ['ilike', 'first_name', $name],
                        ['ilike', 'middle_name', $name],
                        ['ilike', 'last_name', $name],

                    ]);
                }
            }
        }

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'middle_name', 'student_email', 'student_phone'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }



}

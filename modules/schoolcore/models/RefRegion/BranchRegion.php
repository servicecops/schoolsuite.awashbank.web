<?php

namespace app\modules\schoolcore\models\RefRegion;

use Yii;

/**
 * This is the model class for table "ref_region".
 *
 * @property int $id
 * @property string $description
 * @property int|null $country_id
 * @property string|null $created_on
 */
class BranchRegion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'awash_bank_region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bank_region_name'], 'required'],
            [['bank_region_name'], 'string'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_region_name' => 'Branch Region Name',
            'created_on' => 'Created On',
        ];
    }


}

<?php

namespace app\modules\schoolcore\models\RefRegion;

use Yii;

/**
 * This is the model class for table "ref_region".
 *
 * @property int $id
 * @property string $description
 * @property int|null $country_id
 * @property string|null $created_on
 */
class RefRegion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ref_region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
            [['country_id'], 'default', 'value' => null],
            [['country_id'], 'integer'],
            [['created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'country_id' => 'Country ID',
            'created_on' => 'Created On',
        ];
    }
}

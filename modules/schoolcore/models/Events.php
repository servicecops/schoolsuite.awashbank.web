<?php

namespace app\modules\schoolcore\models;

use app\models\User;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string|null $event_date
* @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property User $createdBy
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['date_created','event_date','updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['created_by', 'updated_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['description'], 'string', 'max' => 1000],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'date_created' => 'Date Created',
            'event_date' => 'Event Date'
        ];
    }
public function getEncodedDescription (){
        return Html::encode($this->description);

}
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}

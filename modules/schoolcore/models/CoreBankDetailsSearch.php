<?php

namespace app\modules\schoolcore\models;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * BankDetailsSearch represents the model behind the search form about `app\modules\banks\models\BankDetails`.
 */
class CoreBankDetailsSearch extends CoreBankDetails
{
    /**
     * @inheritdoc
     */
    public $date_from, $channel, $date_to, $school, $settled;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['bank_name', 'bank_address', 'contact_email', 'contact_phone', 'date_created'], 'safe'],
        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'Banks',
            'title' => 'Bank details Datasheet',
            'exportFile' => '/core-bank-details/exportPdfExcel',
        ];

        return $data;
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params

     * @return array
     */
//    public function search($params)
//    {
//        $query = CoreBankDetails::find();
//        $searchForm= 'bank_name';
//        // add conditions that should always apply here
//        $columns = [
//            'bank_name',
//            'bank_code',
//            'bank_address',
//            'contact_phone',
//            'contact_email',
//            'date_created',
//            [
//
//                'label' => '',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    return Html::a('<i class="fas fa-eye"></i>', ['core-bank_details/view/', 'id' => $model['id']]);
//                },
//            ],
//            ////
//        ];
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
//        }
//
//        $query->andFilterWhere([
//            'id' => $this->id,
//        ]);
//
//        $query->andFilterWhere(['like', 'bank_name', $this->bank_name])
//            ->andFilterWhere(['like', 'bank_address', $this->bank_address])
//            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
//            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
//            ->andFilterWhere(['like', 'date_created', $this->date_created]);
//
//        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
//    }

    /*
       * Return particular fields for dataprovider
       */
//    public function searchIndex($params)
//    {
//        $searchForm ='bank_name';
//        $placeholder ='search bank ';
//        $query = (new Query())
//            ->select(['id','bank_name','bank_code','contact_phone', 'contact_email','date_created' ])
//            ->from('core_nominated_bank ');
//        $query->orderBy('id desc');
//        //  ->where(['si.active'=>true, 'si.archived'=>false]);
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $columns = [
//            'bank_name',
//            'bank_code',
//            'bank_address',
//            'contact_phone',
//            'contact_email',
//            'date_created',
//            [
//
//                'label' => '',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    return Html::a('<i class="fas fa-eye"></i>', ['core-bank-details/view', 'id' => $model['id']]);
//                },
//            ],
//            ////
//        ];
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm'=>$searchForm];
//    }

    public function search($params)
    {
        $query = CoreBankDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bank_code' => $this->bank_code,
        ]);

        $query->andFilterWhere(['ilike', 'bank_name', $this->bank_name])
            ->andFilterWhere(['ilike', 'contact_email', $this->contact_email]);

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        return $dataProvider;
    }
}

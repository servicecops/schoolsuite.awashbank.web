<?php

namespace app\modules\schoolcore\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreGuardian;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreGuardianSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreGuardian`.
 */
class CoreGuardianSearch extends CoreGuardian
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_id'], 'integer'],
            [['date_created', 'date_updated', 'name', 'phone_contact_1', 'phone_contact_2', 'address', 'email_address'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreGuardian::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'phone_contact_1', $this->phone_contact_1])
            ->andFilterWhere(['ilike', 'phone_contact_2', $this->phone_contact_2])
            ->andFilterWhere(['ilike', 'address', $this->address])
            ->andFilterWhere(['ilike', 'email_address', $this->email_address]);

        return $dataProvider;
    }

    /**
     * @param $id
     * @return array
     */


    /*
   * Return particular fields for dataprovider
   */
    public function searchIndex($params)
    {

        $query = (new Query())
            ->select(['cg.id','cg.name','cg.phone_contact_1','cg.phone_contact_2','cg.address','cg.email_address','csc.school_name','cg.date_created'])
            ->from('core_guardian cg')
            ->innerJoin('core_school csc', 'cg.school_id= csc.id');
        $query->orderBy('cg.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'name',
            'phone_contact_1',
            'school_name',

            'phone_contact_2',
            'address',
            'email_address',
            'date_created',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fas fa-eye"></i>', ['core-guardian/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider];
    }
}

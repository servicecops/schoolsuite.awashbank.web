<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "county".
 *
 * @property string|null $county_name
 * @property int $id
 * @property int|null $district_id
 * @property string|null $created_on
 * @property int|null $created_by
 * @property string|null $updated_on
 * @property int|null $updated_by
 */
class County extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'county';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'district_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['id', 'district_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['county_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'county_name' => 'County Name',
            'id' => 'ID',
            'district_id' => 'District ID',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getCounties(){
        return $this->hasOne(District::className(),['id'=>'dist_id']);
    }

     public static function getCountyList($dist_id){
         $out = [];
         $rows = (new \yii\db\Query())
             ->select(['id', 'county_name'])
             ->from('county')
             ->where(['district_id'=>$dist_id])
             ->all();

         return $rows;

     }

     public function getCounty(){

         return $this->hasOne(County::className(),['id'=>$this->id]);

     }
}

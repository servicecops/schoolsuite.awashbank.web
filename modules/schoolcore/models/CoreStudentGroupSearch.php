<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

/**
 * StudentGroupSearch represents the model behind the search form about `app\models\StudentGroupInformation`.
 */
class CoreStudentGroupSearch extends CoreStudentGroupInformation
{

    public $gender, $student_class, $searchTerm, $day_boarding, $campus_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id'], 'integer'],
            [['school_id', 'group_name', 'group_description', 'date_created', 'date_modified', 'gender','student_class', 'searchTerm', 'day_boarding', 'campus_id'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['gr.id', 'group_name', 'group_description', 'gr.date_created', 'gr.active', 'sch.school_name'])
              ->from('student_group_information gr')
              ->innerJoin('core_school sch', 'sch.id=gr.school_id')
              ->andFilterWhere(['gr.school_id'=>$this->school_id]);
        if(\app\components\ToWords::isSchoolUser()){
            $query->where(['gr.school_id'=>Yii::$app->user->identity->school_id]);
        }
        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'group_name', 'group_description', 'active', 'school_name' ]
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public function findStudents($grp, $params)
    {
        $query1 = CoreStudentGroupStudent::find()->select(['student_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'si.middle_name', 'si.gender', 'cls.class_code'])
            ->from('core_student si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andFilterWhere([
                'si.class_id'=>$params['student_class'],
                'si.gender'=>$params['gender'],
                'si.school_id'=>$grp['sch']])
            ->andWhere(['not in', 'si.id', $query1]);
        if(!empty($params['day_boarding'])){
            $query->andWhere(['si.day_boarding'=>$params['day_boarding']]);
        }
        if(!empty($params['campus_id'])){
            $query->andWhere(['si.campus_id'=>$params['campus_id']]);
        }

        if(!(empty($params['searchTerm']))){
            $searchTerm = $params['searchTerm'];
            if (is_numeric($searchTerm)) {
                $query->andWhere(['student_code' => $searchTerm]);
            }
            else
            {
                $search_words = explode(' ', $searchTerm);
                $search_words = array_filter($search_words);
                $words = array();
                  if(count($search_words) > 1) {
                    foreach($search_words as $word) {
                        $words[] = $word;
                    }
                        if(isset($words[0])){
                            $name = str_replace(' ', '', $words[0]);
                            $query->andFilterWhere(['or',
                                    ['ilike', 'first_name', $name],
                                    ['ilike', 'middle_name', $name],
                                    ['ilike', 'last_name', $name]
                                    ]);
                        }
                        if(isset($words[1])){
                            $name = str_replace(' ', '', $words[1]);
                            $query->andFilterWhere(['or',
                                    ['ilike', 'first_name', $name],
                                    ['ilike', 'middle_name', $name],
                                    ['ilike', 'last_name', $name]
                                    ]);
                        }
                        if(isset($words[2])){
                            $name = str_replace(' ', '', $words[3]);
                            $query->andFilterWhere(['or',
                                    ['ilike', 'first_name', $name],
                                    ['ilike', 'middle_name', $name],
                                    ['ilike', 'last_name', $name]
                                    ]);
                        }

                    }
                    else{
                        $name = str_replace(' ', '', $searchTerm);
                        $query->andFilterWhere(['or',
                                ['ilike', 'first_name', $name],
                                ['ilike', 'middle_name', $name],
                                ['ilike', 'last_name', $name],

                            ]);
                        }
                }
            }

            $query->orderBy('cls.class_code', 'si.last_name');
            $query->limit(500);
            return $query->all();

    }

    public static function findMembers($id){
        $query = (new Query())->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'si.middle_name', 'si.gender', 'cls.class_code'])
            ->from('student_group_student sgr')
            ->innerJoin('core_student si', 'si.id=sgr.student_id')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andWhere(['sgr.group_id'=>$id])
            ->orderBy('cls.class_code', 'si.last_name');
         return $query->all();
    }

    public static function getExportQuery()
    {
    $data = [
            'data'=>$_SESSION['findData'],
            'fileName'=>'StudentGroup',
            'title'=>'Grouped Student Datasheet',
            'exportFile'=>'/core-student-group/exportPdfExcel',
        ];

    return $data;
    }
}

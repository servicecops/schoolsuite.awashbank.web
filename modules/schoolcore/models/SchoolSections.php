<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "school_sections".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $date_created
 * @property string $section_code
 * @property string $section_name
 * @property boolean $active
 * @property integer $section_primary_bank
 * @property integer $section_account_number
 */
class SchoolSections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_sections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
//        return [
//            [['school_id', 'section_primary_bank'], 'integer'],
//            [['date_created'], 'safe'],
//            [['section_name', 'section_primary_bank'], 'required'],
//            [['active'], 'boolean'],
//            [['section_name'], 'string', 'max' => 255]
//        ];

        return [
            [['school_id'], 'integer'],
            [['date_created'], 'safe'],
            [['section_name', 'section_account_number', 'school_id'], 'required'],
            [['active'], 'boolean'],
            [['section_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'date_created' => 'Date Created',
            'section_code' => 'Section Code',
            'section_name' => 'Section Name',
            'section_primary_bank' => 'Section Pri. Bank',
            'active' => 'Active',
        ];
    }
}

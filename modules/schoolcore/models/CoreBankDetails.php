<?php

namespace app\modules\schoolcore\models;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "nominated_bank_details".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $bank_address
 * @property string $bank_code
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $date_created
 * @property integer $bank_logo
 */
class CoreBankDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_nominated_bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'bank_address', 'bank_code', 'contact_email', 'contact_phone', 'date_created'], 'string'],
            [['bank_name', 'bank_address', 'bank_code', 'contact_email', 'contact_phone', ], 'required'],
            [['bank_logo'], 'integer'],
            [['contact_email'], 'email'],
            [['contact_phone', ], 'validePhone', 'skipOnEmpty' => true, 'skipOnError' => false],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_code'=>'Bank Code *',
            'bank_name' => 'Bank Name *',
            'bank_logo'=>'Bank Logo',
            'bank_address' => 'Bank Address *',
            'contact_email' => 'Contact Email *',
            'contact_phone' => 'Contact Phone *',
            'date_created' => 'Date Created',
        ];
    }

    public function formAttributeConfig() {
        $config = [
            ['attributeName'=>'bank_name', 'controlType'=>'text'],
            ['attributeName'=>'bank_code', 'controlType'=>'text'],
            ['attributeName'=>'contact_phone', 'controlType'=>'text'],
           ['attributeName'=>'contact_email', 'controlType'=>'text'],
            ['attributeName'=>'bank_address', 'controlType'=>'text'],
        ];

        return $config;
    }

    public function updateAttributeConfig() {
        $config = [
            ['attributeName'=>'bank_name', 'controlType'=>'text'],
            ['attributeName'=>'bank_code', 'controlType'=>'text'],
            ['attributeName'=>'contact_phone', 'controlType'=>'text'],
            ['attributeName'=>'contact_email', 'controlType'=>'text'],
            ['attributeName'=>'bank_address', 'controlType'=>'text'],
        ];

        return $config;
    }
    public static function findAllNominatedBanks() {
        return self::find()->orderBy(['bank_name'=>SORT_ASC])->all();
    }

    public function viewModel($params)
    {
        $attributes = [


            'bank_name',
            'bank_code',
            'bank_address',
            'contact_phone',
            'contact_email',
            'date_created'
        ];

        if (($models = CoreBankDetails::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function validePhone($attribute, $params)
    {
        if (!preg_match('/^(0|251)(9|1)\d{8}$/i', $this->$attribute)) {
            $this->addError($attribute, 'Please Enter a valid Phone Number');
        }
    }
}

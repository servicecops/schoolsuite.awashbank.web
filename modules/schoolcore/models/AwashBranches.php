<?php

namespace app\modules\schoolcore\models;
use app\modules\schoolcore\models\RefRegion\BranchRegion;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "nominated_bank_details".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $bank_address
 * @property string $bank_code
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $date_created
 * @property integer $bank_logo
 */
class AwashBranches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'awash_branches';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['branch_code', 'branch_name',], 'string'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'branch_name'=>'Branch Name',
            'branch_code' => 'Branch Code',

        ];
    }




    public function viewModel($params)
    {

        $attributes = [
            'branch_code',
            'branch_name',
            [
                'label' => 'Branch Region',
                'value' => function ($model) {
                    if (isset($model->branch_region)) {
                        return $model->regionName->bank_region_name;
                    } else {
                        return "--";
                    }                },
            ],



        ];

        if (($models = AwashBranches::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models, ];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    public function getRegionName()
    {
        return $this->hasOne(BranchRegion::className(), ['id' => 'branch_region']);
    }

}

<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_user".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $first_name
 * @property string $last_name
 * @property string $other_names
 * @property string $school_id
 * @property string $email_address
 * @property string $locked
 * @property string $mobile_number
 * @property string $user_level
 * @property string $username
 * @property string $password
 * @property string $system_user_id
 * @property string $incorrect_access_count
 * @property string $accessToken
 * @property string $authKey
 */
class CoreUser extends BaseModel implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [[ 'username', 'password',], 'required', ],
            [['school_id', 'incorrect_access_count','system_user_id','user_level'], 'integer']

        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->authkey = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'User Name',
            'system_user_id' => 'System ID',
         //   'incorrect_access_count' => 'Incorrect Count',
         //   'password'=>'Password',
            'date_created'=>'Date Created',
            'locked'=>'Locked',
            'user_level'=>'User Level',
            'school_id'=>'School Id',
            'date_updated'=>'Date Updated',
            'created_by'=>'Created By',
            'authKey'=>'authKey',
            '$accessToken'=>'$accessToken',
        ];
    }

    public function formAttributeConfig() {
        $config = [
            ['attributeName'=>'username', 'controlType'=>'text'],
            ['attributeName'=>'password', 'controlType'=>'password'],
            ['attributeName'=>'user_level', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],

        ];

        return $config;
    }
    public function updateAttributeConfig() {
        $config = [
            ['attributeName'=>'username', 'controlType'=>'text'],
            ['attributeName'=>'user_level', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],

        ];

        return $config;
    }

    public function checkOldPassword($attribute,$params)
    {
        if(!$this->validatePassword($this->current_pass)) {
            $this->addError($attribute, 'Invalid or Wrong password');
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
//        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => strtolower($username)]);
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {

        echo "<script type='text/javascript'>alert('$password');</script>";
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'created_by']);
    }
    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'username',
            'user_level',
            'authKey',
            'accessToken',
            'date_created',
            'date_updated',

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->username;
                },
            ],


            [
                'label' => 'School Name',
                'value' => function ($model) {

                       if($model->school_id){
                           return $model->schoolName->school_name;
                       }else{return "--";}

                },
            ],
        ];

        if (($models = CoreUser::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

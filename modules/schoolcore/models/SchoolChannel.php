<?php

namespace app\modules\schoolcore\models;

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use Yii;

/**
 * This is the model class for table "institution_payment_channel_association".
 *
 * @property integer $id
 * @property integer $institution
 * @property integer $payment_channel
 * @property string $date_created
 * @property string $date_modified
 *
 * @property PaymentChannels $paymentChannel
 * @property CoreSchool $institution0
 */
class SchoolChannel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_payment_channel_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['institution', 'payment_channel'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['institution', 'payment_channel'], 'unique', 'targetAttribute' => ['institution', 'payment_channel'], 'message' => 'The combination of Institution and Payment Channel has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'institution' => 'Institution',
            'payment_channel' => 'Payment Channel',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentChannel()
    {
        return $this->hasOne(PaymentChannels::className(), ['id' => 'payment_channel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution0()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'institution']);
    }
}

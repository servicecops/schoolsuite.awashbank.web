<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "test_grouping".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 */
class CoreTestGrouping extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test_grouping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description',  ''], 'safe'],

            [['name','description'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Test Grouping',
            'description' => 'Descriptin',


        ];
    }

    public function formAttributeConfig() {
        $config = [

            ['attributeName'=>'name', 'controlType'=>'text'],
            ['attributeName'=>'description', 'controlType'=>'text'],

        ];

        return $config;
    }

}

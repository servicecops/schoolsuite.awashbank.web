<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "book_category".
 *
 * @property int $id
 * @property string|null $category_name
 * @property string|null $date_created
 * @property string|null $date_modified
 * @property int|null $school_id
 */
class BookCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_modified'], 'safe'],
            [['category_name'], 'required'],
            [['category_name'], 'string', 'max' => 255],
            [['school_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
}

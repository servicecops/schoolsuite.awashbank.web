<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

if (!Yii::$app->session->isActive){
          session_start();  
      }

/**
 * SchoolInformationSearch represents the model behind the search form about `app\models\SchoolInformation`.
 */
class SchoolCampusSearch extends SchoolCampuses
{
    public $modelSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_account_id'], 'integer'],
            [['campus_name', 'modelSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['c.id', 'c.campus_name', 'sch.school_name', 'sch.village','c.physical_address', 'sch.school_code','sch.external_school_code'])
            ->from('school_campuses c')
            ->innerJoin('core_school sch', 'c.school_id=sch.id')
            ->andFilterWhere(['school_id'=>$this->school_id])
            ->andFilterWhere(['ilike', 'c.campus_name', $this->campus_name])
            ->andFilterWhere(['ilike', 'sch.external_school_code', $this->modelSearch]);

            $pages = new Pagination(['totalCount' => $query->count()]);
            $sort = new Sort([
                'attributes' => ['school_code', 'school_name', 'campus_name', 'physical_address'],
            ]);
            ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('c.campus_name');
            unset($_SESSION['findData']);
            $_SESSION['findData'] = $query;
            $query->offset($pages->offset)->limit($pages->limit);
            return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public static function getExportQuery() 
    {
    $data = [
            'data'=>$_SESSION['findData'],
            'labels'=>['school_code', 'school_name', 'physical_address', 'school_type_name'],
            'fileName'=>'School', 
            'title'=>'Enrolled Schools',
            'exportFile'=>'/core-school/exportPdfExcel',
        ];

        return $data;
    }
}

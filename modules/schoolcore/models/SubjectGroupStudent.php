<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;

use Yii;

/**
 * This is the model class for table "student_group_student".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $student_id
 * @property string $date_added
 */
class SubjectGroupStudent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject_group_subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'subject_id'], 'required'],
            [['group_id', 'subject_id'], 'integer'],
            [['date_added'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'subject_id' => 'Student ID',
            'date_added' => 'Date Added',
        ];
    }
}

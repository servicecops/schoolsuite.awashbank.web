<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreSubject;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreStudentSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreStudent`.
 */
class CoreSubjectSearch extends CoreSubject
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;

    public static function findMembers($id)
    {
        $query = (new Query())->select(['sgr.id', 'sgr.subject_code', 'sgr.subject_name', 'cls.class_code'])
            ->from('core_subject sgr')
            ->innerJoin('core_school_class cls', 'cls.id=sgr.class_id')
            //->andWhere(['sgr.group_id'=>$id])
            ->orderBy('cls.class_code', 'sgr.subject_name');
        return $query->all();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'class_id', 'school_id',], 'integer'],
            [['date_created', 'date_updated', 'subject_code', 'subject_name',], 'safe'],
            [['active', 'archived'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /*
    * Return particular fields for dataprovider
    */

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreSubject::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'date_of_birth' => $this->date_of_birth,
            'class_id' => $this->class_id,
            'school_id' => $this->school_id,
            'active' => $this->active,
            'archived' => $this->archived,
            'subject_code' => $this->subject_code,
            'subject_name' => $this->subject_name,
        ]);

        $query->andFilterWhere(['ilike', 'subject_code', $this->subject_code])
            ->andFilterWhere(['ilike', 'subject_name', $this->subject_name]);
        return $dataProvider;
    }

    public function searchIndex($params)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id')
            ->Where(['cs.class_id' => $params]);
        $query->orderBy('cs.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);
//                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }
        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $sort = new Sort([
            'attributes' => [
                'student_name', 'subject_code', 'school_name'
            ],
        ]);

        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return Html::a($model['subject_name'], ['core-subject/view', 'id' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return Html::a($model['class_description'], ['core-school-class/view', 'id' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-subject/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm = 'subject_name';
        $title = 'List of School Subjects';
        return ['columns' => $columns, 'title' => $title, 'dataProvider' => $dataProvider, 'pages' => $pages, 'searchForm' => $searchForm, 'cpage' => $pages->page, 'sort' => $sort];
    }

    public function searchSubject($params)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id');


        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->innerJoin('teacher_class_subject_association tca', 'cs.id=tca.subject_id');

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach ($data as $k => $v) {
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['tca.class_id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andWhere(['csc.id' => $params]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
//                $query->andWhere(['csc.id' => $params]);
                $query->Where(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $params]);
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $searchForm = 'subject_name';
        return $dataProvider;
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function displayAllSubjects(array $queryParams)
    {
        $searchForm = 'description';
        $query = (new Query())
            ->select(['cs.id', 'cs.subject_name', 'cs.subject_code', 'csc.school_name', 'cl.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school csc', 'cs.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'cs.class_id= cl.id');
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return Html::a($model['subject_name'], ['core-subject/view', 'id' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            'subject_code',
            'school_name',
            'class_description',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fas fa-eye"></i>', ['core-term/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];


    }

    public function searchStudentSubject($params)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id');
//            ->innerJoin('core_student ct', 'ct.school_id =sch.id');

        $query->orderBy('cs.id desc')
            ->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);


        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return Html::a($model['subject_name'], ['core-subject/view', 'id' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Add Solution', ['elearning/student-submission', 'subjectId' => $model['id']]);
                },

            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];

    }

    public function viewStudentSubject($params)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id')
            ->where(['cs.id' => $params]);

        $query->orderBy('cs.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->innerJoin('teacher_class_subject_association tca', 'cs.id=tca.class_id');

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }

            // $query->andWhere(['tca.class_id' => 18]);
            $query->andwhere(['in', 'tca.subject_id', $the_subjectId]);

        }
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return Html::a($model['subject_name'], ['core-subject/view', 'id' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('View Solutions', ['elearning/student-index', 'subjectId' => $model['id']]);
                },
                'visible' => \Yii::$app->user->can('r_std_submission'),
            ],
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('View Solutions', ['elearning/single-student-index', 'subjectId' => $model['id']]);
                },
                'visible' => !\Yii::$app->user->can('non_student'),
            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function viewSingleStudentSubject($params)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id');

        $query->orderBy('cs.id desc')
            ->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);

//        if (\app\components\ToWords::isSchoolUser() ) {
//            if ($this->class_id && $this->class_id != 0) {
//                $query->andWhere(['cs.class_id' => intVal($this->class_id)]);
//            } else {
//                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
//            }
//        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
//            if (!empty($this->class_id)) {
//                $query->andWhere(['cs.class_id' => intVal($this->class_id)]);
//            } elseif ($this->schsearch) {
//                $query->andWhere(['csc.school_id' => $this->schsearch]);
//            }
//        }

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->innerJoin('teacher_class_subject_association tca', 'csc.id=tca.class_id');

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach ($data as $k => $v) {
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['tca.class_id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'tca.class_id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('View Solutions', ['elearning/single-student-index', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function searchClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id', 'csc.class_code', 'sch.school_name', 'csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch', 'csc.school_id =sch.id');

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return Html::a($model['class_description'], ['core-subject/display-subject', 'classId' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-subject/display-subject', 'classId' => $model['id']]);
                },
            ],
            ////
        ];

        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];

    }

    public function searchClassView(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id', 'csc.class_code', 'sch.school_name', 'csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch', 'csc.school_id =sch.id');

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->innerJoin('teacher_class_subject_association tca', 'csc.id=tca.class_id');


            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach ($data as $k => $v) {
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['tca.class_id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'tca.class_id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sc.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return Html::a($model['class_description'], ['core-school-class/view', 'id' => $model['id']], ['class' => 'aclink']);
                },
                'format' => 'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-test/the-index', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];

    }

    public function findSubjects($grp, $params)
    {
        Yii::trace($params);
        //$query1 = \app\models\StudentGroupStudent::find()->select(['student_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['si.id', 'si.subject_code', 'si.subject_name', 'cls.class_code'])
            ->from('core_subject si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andFilterWhere([
                'si.class_id' => $params['class_id']]);
        // 'si.school_id'=>$grp['sch']]);

        $query->orderBy('cls.class_code', 'si.subject_name');
        $query->limit(500);
        return $query->all();

    }

    public function studentSubject($class_id)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id')
            ->Where(['cs.class_id' => $class_id]);
        $query->orderBy('cs.id desc');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',
            'class_description',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-subject/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function studentSubj($class_id)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.subject_code', 'cs.subject_name', 'cs.date_created', 'sch.school_name', 'csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch', 'sch.id =cs.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cs.class_id')
            ->Where(['cs.class_id' => $class_id]);


        return $query;
    }
}

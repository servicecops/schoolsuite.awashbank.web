<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreTeacherSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreTeacher`.
 */
class CoreTermSearch extends CoreTerm
{
    public $test,$answer,$schsearch, $modelSearch;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_id',], 'integer'],
            [['date_created', 'date_modified', 'term_starts', 'term_ends', 'term_name', 'term_description','modelSearch'], 'safe'],
            [['modelSearch'], 'trim'],


        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'Terms',
            'title' => 'Terms Datasheet',
            'exportFile' => '/core-term/exportPdfExcel',
        ];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $query = CoreTerm::find();
        $searchForm = 'term_name';
        // add conditions that should always apply here
        $columns = [
            'term_name',
            'term_description',
            'term_starts',
            'term_ends',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            'date_created',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-term/view/', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['ilike', 'term_name', $this->modelSearch])
            ->andFilterWhere(['ilike', 'term_description', $this->modelSearch]);



        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params)
    {
        $searchForm = 'term_name';
        $placeholder = 'search term ';
        $query = (new Query())
            ->select(['ct.id', 'ct.term_name', 'ct.term_description', 'ct.term_ends', 'ct.term_starts', 'csc.school_name', 'ct.date_created', 'ct.date_modified',])
            ->from('core_term ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id');
        $query->orderBy('ct.id desc');

        if (\app\components\ToWords::isSchoolUser()) {
//            if ($this->test_id && $this->test_id != 0) {
//                $query->andWhere(['ct.id' => intVal($this->id)]);
          //  } else {
                $query->andWhere(['ct.school_id' => Yii::$app->user->identity->school_id]);
           // }
        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
//            if (!empty($this->test_id)) {
//                $query->andWhere(['ct.id' => intVal($this->id)]);
//            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
           // }
        }
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'term_name',

            [
                'label' => 'Term Name',
                'value'=>function($model){
                    return Html::a($model['term_description'], ['/schoolcore/core-term/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'term_starts',
            'term_ends',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-term/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

//    public function searchNewIndex($params)
//    {
//        $query = new Query();
//        $query->select(['ct.id', 'ct.term_name', 'ct.term_description', 'ct.term_ends', 'ct.term_starts', 'csc.school_name', 'ct.date_created', 'ct.date_modified',])
//        ->from('core_term ct')
//        ->innerJoin('core_school csc', 'ct.school_id= csc.id');
//        $query->orderBy('ct.id desc');
//
//
//        if (\app\components\ToWords::isSchoolUser()) {
////            if ($this->test_id && $this->test_id != 0) {
////                $query->andWhere(['ct.id' => intVal($this->id)]);
//            //  } else {
//            $query->andWhere(['ct.school_id' => Yii::$app->user->identity->school_id]);
//            // }
//        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
////            if (!empty($this->test_id)) {
////                $query->andWhere(['ct.id' => intVal($this->id)]);
////            } elseif ($this->schsearch) {
//            $query->andWhere(['csc.id' => $this->schsearch]);
//            // }
//        }
//
//        // add conditions that should always apply here
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'pagesize' => 20 // in case you want a default pagesize
//            ]
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'date_created' => $this->date_created,
//            'date_updated' => $this->date_updated,
//            'created_by' => $this->created_by,
//            'school_id' => $this->school_id,
//        ]);
//
//        $query->andFilterWhere(['ilike', 'term_code', $this->modelSearch])
//            ->andFilterWhere(['ilike', 'term_description', $this->modelSearch]);
//
//        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;
//        return $dataProvider;
//    }

    public function searchNewIndex($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $query = new Query();
        $query->select(['ct.id', 'ct.term_name', 'ct.term_description','ct.term_type','term_type_detail', 'ct.term_ends', 'ct.term_starts', 'csc.school_name', 'ct.date_created', 'ct.date_modified',])
            ->from('core_term ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id');
        $query->orderBy('ct.id desc');

        $query->andFilterWhere(['ilike', 'term_name', $this->modelSearch])
            ->orFilterWhere(['ilike', 'term_description', $this->modelSearch]);

        if (\app\components\ToWords::isSchoolUser()) {
//            if ($this->test_id && $this->test_id != 0) {
//                $query->andWhere(['ct.id' => intVal($this->id)]);
            //  } else {
            $query->andWhere(['ct.school_id' => Yii::$app->user->identity->school_id]);
            // }
        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
//            if (!empty($this->test_id)) {
//                $query->andWhere(['ct.id' => intVal($this->id)]);
//            } elseif ($this->schsearch) {
            $query->andWhere(['csc.id' => $this->schsearch]);
            // }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,
        ]);


        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        return $dataProvider;
    }

}

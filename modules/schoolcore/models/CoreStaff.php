<?php

namespace app\modules\schoolcore\models;

use app\components\ToWords;
use app\models\AuthItem;
use app\models\BaseModel;
use app\models\SubjectGroupStudent;
use app\models\User;
use app\modules\schoolcore\models\RefRegion\RefRegion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_teacher".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $first_name
 * @property string $last_name
 * @property string $other_names
 * @property string $date_of_birth
 * @property string $gender
 * @property string $class_id
 * @property string $school_id
 * @property string $phone_contact_1
 * @property string $phone_contact_2
 * @property string $email_address
 * @property string $student_code
 * @property bool $active
 * @property bool $archived
 * @property string $date_archived
 * @property string $passport_photo
 * @property string $account_id
 * @property string $user_level
 */
class CoreStaff extends BaseModel
{
    public $class_id, $device_id, $username;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'date_archived', 'date_of_birth','username'], 'safe'],
            [['created_by', 'school_id', 'passport_photo'], 'default', 'value' => null],
            [['created_by', 'school_id', 'passport_photo', 'staff_type_id', 'number_of_dependants'], 'integer'],
            [['school_id', 'first_name', 'last_name', 'gender', 'phone_contact_1', 'user_level', 'staff_type_id', 'email_address', 'district', 'region', 'date_of_birth'], 'required'],
            [['active', 'archived'], 'boolean'],
            [['first_name', 'last_name', 'other_names','other_non_teaching','non_teaching_category','teaching_category', 'phone_contact_1', 'phone_contact_2', 'nin_no', 'email_address', 'user_level', 'district', 'parish', 'educational_qualification', 'marital_status', 'village', 'sub_county', 'county', 'region',], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1],
            [['email_address',] ,'email'],
            [['date_of_birth'], 'validateDateOfBirthFormat', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'other_names' => 'Other Names',
            'gender' => 'Gender',
            'school_id' => 'School',
            'phone_contact_1' => 'Phone Contact 1',
            'phone_contact_2' => 'Phone Contact 2',
            'email_address' => 'Email Address',
            'active' => 'Active',
            'archived' => 'Archived',
            'date_archived' => 'Date Archived',
            'passport_photo' => 'Passport Photo',
            'user_level' => 'Staff Level',
            'staff_type_id' => 'Staff Type',
        ];
    }

    public function formAttributeConfig()
    {
        $config = [
            ['attributeName' => 'first_name', 'controlType' => 'text'],
            ['attributeName' => 'last_name', 'controlType' => 'text'],
            ['attributeName' => 'other_names', 'controlType' => 'text'],
            ['attributeName' => 'gender', 'controlType' => 'gender_picker'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_only', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],
            ['attributeName' => 'phone_contact_1', 'controlType' => 'text'],
            ['attributeName' => 'phone_contact_2', 'controlType' => 'text'],
            ['attributeName' => 'email_address', 'controlType' => 'text'],
            ['attributeName' => 'user_level', 'controlType' => 'staff_levels_id', 'placeholder' => 'Choose Staff Levels', 'prompt' => 'Choose Staff Levels'],
            ['attributeName' => 'staff_type_id', 'controlType' => 'staff_type_id', 'placeholder' => 'Choose Staff Type', 'prompt' => 'Choose Staff Type'],

        ];

        return $config;
    }

    public function updateAttributeConfig()
    {
        $config = [
            ['attributeName' => 'first_name', 'controlType' => 'text'],
            ['attributeName' => 'last_name', 'controlType' => 'text'],
            ['attributeName' => 'other_names', 'controlType' => 'text'],
            ['attributeName' => 'gender', 'controlType' => 'gender_picker'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_picker', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],
            ['attributeName' => 'phone_contact_1', 'controlType' => 'text'],
            ['attributeName' => 'phone_contact_2', 'controlType' => 'text'],
            ['attributeName' => 'email_address', 'controlType' => 'text'],
            ['attributeName' => 'user_level', 'controlType' => 'staff_levels_id', 'placeholder' => 'Choose Staff Levels', 'prompt' => 'Choose Staff Levels'],
            ['attributeName' => 'staff_type_id', 'controlType' => 'staff_type_id', 'placeholder' => 'Choose Staff Type', 'prompt' => 'Choose Staff Type'],

        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getDevice($id)
    {
        $deviceId = CoreStaffDeviceAssociation::findOne(['staff_id' => $id]);
        $staffDevice = null;
        if ($deviceId) {
            $staffDevice = $deviceId;
        }

    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $deviceId = CoreStaffDeviceAssociation::findOne(['staff_id' => $params]);
        $staffDevice = null;
        if ($deviceId) {
            $staffDevice = $deviceId['device_id'];
        }
        $attributes = [
            'first_name',
            'last_name',
            'gender',
            'phone_contact_1',
            'phone_contact_2',
            'email_address',
            'user_level',
            'marital_status',
            'educational_qualification',
            'number_of_dependants',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'District',
                'value' => function ($model) {
                    if (isset($model->district)) {
                        return $model->districtName->district_name;
                    } else {
                        return "--";
                    }
                },
            ],
            [
                'label' => 'Region',
                'value' => function ($model) {

                    if (isset($model->region)) {
                        return $model->regionName->description;
                    } else {
                        return "--";
                    }
                },
            ],

            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {


                    return $model->userName->firstname . ' ' . $model->userName->lastname;
                },
            ],

        ];

        if (($models = CoreStaff::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models, 'staffDevice' => $staffDevice];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getTeacherName()
    {
        return $this->first_name . '-' . $this->last_name;
    }

    public function findSubjects($grp, $params)
    {
        Yii::trace($params);
        //$query1 = \app\models\StudentGroupStudent::find()->select(['student_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['si.id', 'si.subject_code', 'si.subject_name', 'cls.class_code'])
            ->from('core_subject si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andFilterWhere([
                'si.class_id' => $params['class_id']]);
        // 'si.school_id'=>$grp['sch']]);
        if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['si.school_id' => Yii::$app->user->identity->school_id]);
        }
        $query->orderBy('cls.class_code', 'si.subject_name');
        $query->limit(500);
        return $query->all();

    }



    public static function findMembers($id)
    {
        $query = (new Query())->select(['si.id', 'si.subject_name', 'si.subject_code', 'cls.class_code'])
            ->from('subject_group_subject sgr')
            ->innerJoin('core_subject si', 'si.id=sgr.subject_id')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andWhere(['sgr.group_id' => $id])
            ->orderBy('cls.class_code', 'si.subject_name');
        return $query->all();
    }

    public function getTeachersFullname()
    {
        $fn = $this->first_name ? $this->first_name : '';
        $mn = $this->middle_name ? $this->middle_name : '';
        $ln = $this->last_name ? $this->last_name : '';
        return $fn . ' ' . $mn . ' ' . $ln;
    }

    public function getDistrictName()
    {
        return $this->hasOne(District::className(), ['id' => 'district']);
    }

    public function getRegionName()
    {
        return $this->hasOne(RefRegion::className(), ['id' => 'region']);
    }

    public function searchUser($params)
    {

        $this->load($params);

        $query = User::find()->where(['school_id' => Yii::$app->user->identity->school_id,'user_level' => 'sch_teacher']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        Yii::trace($this->username);
        $query->andFilterWhere(['ilike', 'username', $this->username]);
        $query->andFilterWhere(['archived' => false]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'invalid_login_count' => $this->invalid_login_count,
            'created_at' => $this->created_at,
            'locked' => $this->locked,
            'updated_at' => $this->updated_at,
        ]);
        //Search by name



            $query->andFilterWhere(['user_level' => 'sch_teacher']);


        if (\app\components\ToWords::isSchoolUser()) {

            $query->andWhere(['school_id' => Yii::$app->user->identity->school_id]);

        }


        return $dataProvider;
    }

    /**Validate date format for date of birth
     * @param $attribute
     * @param $paramsV
     */
    public function validateDateOfBirthFormat($attribute, $params)
    {
        if (!ToWords::validateDate($this->date_of_birth, 'Y-m-d')) {
            $this->addError($attribute, 'The date of birth is in an invalid format. Enter a valid date of birth in the format yyyy-mm-dd');
        }

        $now = date('Y-m-d');
        if ($now < $this->date_of_birth) {
            $this->addError($attribute, 'Date of birth cannot be in the future');
        }
    }
}

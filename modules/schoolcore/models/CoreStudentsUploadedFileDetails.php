<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "student_upload_file_details".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $hash
 * @property integer $uploaded_by
 * @property string $date_uploaded
 * @property string $description
 * @property integer $destination_class
 * @property boolean $imported
 * @property integer $destination_school
 */
class StudentUploadFileDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_upload_file_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'hash'], 'required'],
            [['file_name', 'hash', 'description'], 'string'],
            [['uploaded_by', 'destination_class', 'destination_school'], 'integer'],
            [['date_uploaded'], 'safe'],
            [['imported'], 'boolean'],
            [['hash'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'hash' => 'Hash',
            'uploaded_by' => 'Uploaded By',
            'date_uploaded' => 'Date Uploaded',
            'description' => 'Description',
            'destination_class' => 'Destination Class',
            'imported' => 'Imported',
            'destination_school' => 'Destination School',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Student Upload File Details',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'file_name' => ['label'=>'File Name'],
                    'hash' => ['label'=>'Hash'],
                    'uploaded_by' => ['type'=>'number', 'label'=>'Uploaded By'],
                    'date_uploaded' => ['type'=>'date', 'label'=>'Date Uploaded'],
                    'description' => ['label'=>'Description'],
                    'destination_class' => ['type'=>'number', 'label'=>'Destination Class'],
                    'imported' => ['type'=>'number', 'label'=>'Imported'],
                    'destination_school' => ['type'=>'number', 'label'=>'Destination School'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'file_name'],
            ['attribute'=>'hash'],
            ['attribute'=>'uploaded_by'],
            ['attribute'=>'date_uploaded'],
            ['attribute'=>'description'],
            ['attribute'=>'destination_class'],
            ['attribute'=>'imported'],
            ['attribute'=>'destination_school'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Student Upload File Details" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

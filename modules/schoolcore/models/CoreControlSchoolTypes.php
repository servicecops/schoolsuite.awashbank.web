<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "core_control_school_types".
 *
 * @property int $id
 * @property string $code
 * @property string $description
 */
class CoreControlSchoolTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_control_school_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'description'], 'required'],
            [['code', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'description' => 'Description',
        ];
    }
    public function viewModel($params)
    {
        $attributes = [
            'id',
            'code',
            'description',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreControlSchoolTypes::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

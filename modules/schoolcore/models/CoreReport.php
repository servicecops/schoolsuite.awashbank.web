<?php
/**
 * Created by IntelliJ IDEA.
 * User: GOGO
 * Date: 5/15/2020
 * Time: 2:35 PM
 */

namespace app\modules\schoolcore\models;


use app\models\BaseModel;
use app\models\User;
use yii\web\NotFoundHttpException;

class CoreReport extends BaseModel

{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', ], 'safe'],
            [['created_by', 'class_id', 'term_id', ], 'default', 'value' => null],
            [['created_by', 'class_id', 'term_id', ], 'integer'],
            [['description'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'class_id' => 'Date Updated',
            'term_id' => 'Created By',

        ];
    }

    public function formAttributeConfig() {
        $config = [

            ['attributeName'=>'subject_code', 'controlType'=>'text'],
            ['attributeName'=>'subject_name', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker_subject', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],

        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'subject_name',
            'subject_code',

            'date_created',

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSubject::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

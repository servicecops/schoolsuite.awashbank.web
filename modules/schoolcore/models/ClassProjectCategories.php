<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "class_project_categories".
 *
 * @property int $id
 * @property string|null $name
 * @property int $created_by
 * @property string|null $date_created
 * @property string|null $date_modified
 */
class ClassProjectCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'class_project_categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_by'], 'required'],
            [['created_by'], 'default', 'value' => null],
            [['created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
}

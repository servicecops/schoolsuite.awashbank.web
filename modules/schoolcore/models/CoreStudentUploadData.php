<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "student_upload_file_data".
 *
 * @property integer $id
 * @property string $first_name
 * @property integer $file_id
 * @property string $middle_name
 * @property string $last_name
 * @property string $school_student_registration_number
 * @property string $gender
 * @property string $date_of_birth
 * @property string $date_created
 * @property integer $student_class
 * @property string $student_email
 * @property string $student_phone
 * @property string $gurdian_name
 * @property string $guardian_relation
 * @property string $guardian_email
 * @property string $guardian_phone
 * @property boolean $disability
 * @property string $disability_nature
 * @property string $nationality
 * @property integer $school_id
 * @property string $day_boarding
 */
class StudentUploadFileData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_upload_file_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'gender', 'date_of_birth', 'student_class'], 'required'],
            [['first_name', 'middle_name', 'last_name', 'school_student_registration_number', 'student_email', 'student_phone', 'gurdian_name', 'guardian_relation', 'gurdian_email', 'guardian_phone', 'disability_nature', 'nationality'], 'string'],
            [['file_id', 'student_class', 'school_id'], 'integer'],
            [['date_of_birth', 'date_created'], 'safe'],
            [['disability'], 'boolean'],
            [['gender', 'day_boarding'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'file_id' => 'File ID',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'school_student_registration_number' => 'School Student Registration Number',
            'gender' => 'Gender',
            'date_of_birth' => 'Date Of Birth',
            'date_created' => 'Date Created',
            'student_class' => 'Student Class',
            'student_email' => 'Student Email',
            'student_phone' => 'Student Phone',
            'gurdian_name' => 'Guardian Name',
            'guardian_relation' => 'Guardian Relation',
            'guardian_email' => 'Guardian Email',
            'guardian_phone' => 'Guardian Phone',
            'disability' => 'Disability',
            'disability_nature' => 'Disability Nature',
            'nationality' => 'Nationality',
            'school_id' => 'School ID',
            'day_boarding' => 'Day Boarding',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Student Upload File Data',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'first_name' => ['label'=>'First Name'],
                    'file_id' => ['type'=>'number', 'label'=>'File Id'],
                    'middle_name' => ['label'=>'Middle Name'],
                    'last_name' => ['label'=>'Last Name'],
                    'school_student_registration_number' => ['label'=>'School Student Registration Number'],
                    'gender' => ['label'=>'Gender'],
                    'date_of_birth' => ['type'=>'date', 'label'=>'Date Of Birth'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'student_class' => ['type'=>'number', 'label'=>'Student Class'],
                    'student_email' => ['label'=>'Student Email'],
                    'student_phone' => ['label'=>'Student Phone'],
                    'gurdian_name' => ['label'=>'Guardian Name'],
                    'guardian_relation' => ['label'=>'Guardian Relation'],
                    'guardian_email' => ['label'=>'Guardian Email'],
                    'guardian_phone' => ['label'=>'Guardian Phone'],
                    'disability' => ['type'=>'number', 'label'=>'Disability'],
                    'disability_nature' => ['label'=>'Disability Nature'],
                    'nationality' => ['label'=>'Nationality'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                    'day_boarding' => ['label'=>'Day Boarding'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'first_name'],
            ['attribute'=>'file_id'],
            ['attribute'=>'middle_name'],
            ['attribute'=>'last_name'],
            ['attribute'=>'school_student_registration_number'],
            ['attribute'=>'gender'],
            ['attribute'=>'date_of_birth'],
            ['attribute'=>'date_created'],
            ['attribute'=>'student_class'],
            ['attribute'=>'student_email'],
            ['attribute'=>'student_phone'],
            ['attribute'=>'gurdian_name'],
            ['attribute'=>'guardian_relation'],
            ['attribute'=>'guardian_email'],
            ['attribute'=>'guardian_phone'],
            ['attribute'=>'disability'],
            ['attribute'=>'disability_nature'],
            ['attribute'=>'nationality'],
            ['attribute'=>'school_id'],
            ['attribute'=>'day_boarding'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Student Upload File Data" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

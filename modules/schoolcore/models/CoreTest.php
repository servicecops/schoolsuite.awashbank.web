<?php

namespace app\modules\schoolcore\models;

use app\components\ToWords;
use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *
 * @property string $description
 * @property string $subject_id
 * @property string $id
 * @property string $school_id
 * @property string $class_id
 * @property string $subject_teacher_id
 * @property string $date_modified
 * @property string $created_by
 * @property string $start_date
 * @property string $end_date
 * @property string $date_created
 * @property string $total_marks
 * @property string $term_id
 * @property string $test_description
 */
class CoreTest extends BaseModel
{
    public $test, $answer, $schsearch;
    /**
     * @var UploadedFile file attribute
     */
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_test';
    }

    /**
     * {@inheritdoc}
     */


    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'date_archived', 'start_date', 'end_date', 'answer'], 'safe'],
            [['created_by', 'class_id', 'school_id',], 'default', 'value' => null],
            [['created_by', 'class_id', 'school_id', 'subject_id', 'subject_teacher_id', 'total_marks', 'term_id', 'assessment_id'], 'integer'],
            [['class_id', 'school_id', 'subject_id', 'test_description', 'term_id','start_date','end_date','assessment_id'], 'required'],
            [['test_description', 'test'], 'string', 'max' => 255],
            [['file'], 'file'],
            [['start_date', 'end_date'], 'validEndDate', 'skipOnEmpty' => false, 'skipOnError' => false],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'test_description' => 'Description',
            'subject_teacher_id' => 'Subject Teacher',
            'class_id' => 'Class Name',
            'school_id' => 'School Name',
           'total_marks' => 'Total Marks',
            'start_date' => 'Test Start Date',
            'end_date' => 'Test End Date',
            'date_modified' => 'Date Modified',
            'subject_id' => 'Subject Name',
            'term_id' => 'Term Description',
            'assessment_id' => 'Assessment Type',

        ];
    }

    public function formAttributeConfig()
    {
        $config = [

            ['attributeName' => 'test_description', 'controlType' => 'text'],
            ['attributeName' => 'total_marks', 'controlType' => 'text'],
            ['attributeName' => 'start_date', 'controlType' => 'date_time_picker'],
            ['attributeName' => 'end_date', 'controlType' => 'date_time_picker'],
            ['attributeName' => 'term_id', 'controlType' => 'term_id_picker', 'placeholder' => 'select Term', 'prompt' => 'select term'],
            ['attributeName' => 'assessment_id', 'controlType' => 'assessment_type', 'placeholder' => 'Select Assessment', 'prompt' => 'Select Assessment Type'],
        //    ['attributeName' => 'test_grouping_id', 'controlType' => 'test_grouping', 'placeholder' => 'select Test Grouping', 'prompt' => 'select Test Type'],

        ];

        return $config;
    }

    public function updateAttributeConfig()
    {
        $config = [

            ['attributeName' => 'test_description', 'controlType' => 'text'],
            ['attributeName' => 'total_marks', 'controlType' => 'text'],

            ['attributeName' => 'start_date', 'controlType' => 'date'],
            ['attributeName' => 'end_date', 'controlType' => 'date'],
            ['attributeName' => 'assessment_id', 'controlType' => 'assessment_type', 'placeholder' => 'Select Assessment', 'prompt' => 'Select Assessment Type'],
           // ['attributeName' => 'test_grouping_id', 'controlType' => 'test_grouping', 'placeholder' => 'select Test Grouping', 'prompt' => 'select Test grouping'],

        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getTermName()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }

    public function getTeacherName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'subject_teacher_id']);
    }

    public function getAssessmentType()
    {
        return $this->hasOne(CoreAssessment::className(), ['id' => 'assessment_id']);
    }

    public function getTestGrouping()
    {
        return $this->hasOne(CoreTestGrouping::className(), ['id' => 'test_grouping_id']);
    }

    public function getFullname()
    {
        $modelUser = User::find()->where(['id' => $this->id]);
        Yii::trace($modelUser);
        return $modelUser->firstname . ' ' . $modelUser->lastname;
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $searchForm = 'test_description';
        $query = (new Query())
            ->select(['ct.id', 'sb.subject_name', 'ct.test_description', 'ct.start_date', 'ct.end_date', 'ctm.term_description', 'csc.school_name', 'cl.class_description', 'sb.subject_name', 'tc.first_name', 'ct.total_marks', 'ct.date_created',])
            ->from('core_test ct')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_term ctm', 'ct.term_id= ctm.id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $attributes = [

            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return $model->subjectName->subject_name;
                },
            ],
            'test_description',
            'total_marks',
            'start_date',
            'end_date',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname . ' ' . $model->userName->lastname;
                },
            ],
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname . ' ' . $model->userName->lastname;
                },
            ],
            [
                'label' => 'Assessment Type',
                'value' => function ($model) {
                    return $model->assessmentType->assessment_type;
                },
            ],
//            [
//                'label' => 'Test Grouping',
//                'value' => function ($model) {
//                    return $model->testGrouping->test_grouping;
//                },
//            ],
            'date_created',
            'date_modified',
        ];

        if (($models = CoreTest::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function getAllTests()
    {

        $query = new Query();
        $query->select(['ct.id', 'ct.test_description', 'sb.subject_code', 'sb.subject_name', 'cls.class_description'])
            ->from('core_test ct')
            ->innerJoin('core_subject sb', 'sb.id=ct.subject_id')
            ->innerJoin('core_school sch', 'sch.id=ct.school_id')
            ->innerJoin('core_school_class cls', 'cls.id=ct.class_id');
        //->andWhere(['si.archived' => false]);


        if (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ct.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['ct.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        } elseif (!Yii::$app->user->can('non_student')) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ct.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['cls.id' => Yii::$app->user->identity->class_id]);
            }
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        // ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function StudentTests($class_id)
    {
        $searchForm = 'test_description';
        $query = (new Query())
            ->select(['ct.id', 'sb.subject_name', 'ct.test_description', 'ct.start_date', 'ct.end_date', 'ctm.term_description', 'cl.class_description', 'sb.subject_name', 'ct.total_marks', 'ct.date_created',])
            ->from('core_test ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_term ctm', 'ct.term_id= ctm.id')
            ->where(['ct.class_id' => $class_id]);
        $query->orderBy('ct.id desc');

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_name',
            'test_description',
            'start_date',
            'end_date',
            'term_description',
            'class_description',
            'first_name',
            'total_marks',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-test/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function validEndDate($attribute, $params)
    {
        if (!ToWords::validateDate($this->getAttribute($attribute), 'Y-m-d H:i')) {
            $this->addError($attribute, 'Invalid date format. Enter a valid date in the format yyyy-mm-dd HH:MM');
            return;
        }

        if($this->start_date && $this->end_date && $this->end_date<=$this->start_date) {
            $this->addError('start_date', 'Start date and time cannot be after end date and time');
            $this->addError('end_date', 'End date and time term cannot be before start date and time');
            return;
        }
    }


}

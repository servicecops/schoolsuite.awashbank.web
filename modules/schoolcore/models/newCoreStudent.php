<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_school".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $school_name
 * @property int $school_type
 * @property string $location
 * @property string $phone_contact_1
 * @property string $phone_contact_2
 * @property string $contact_email_1
 * @property string $contact_email_2
 * @property string $school_code
 * @property string $account_number
 * @property string $contact_person
 * @property string $bank_name
 * @property string $enable_daily_stats
 * @property string $default_part_payment_behaviour
 * @property string $school_registration_number_format
 * @property string $sample_school_registration_number
 * @property string $account_type
 * @property string $account_title
 * @property string $region
 * @property string $district
 * @property string $county
 * @property string $sub_county
 * @property string $parish
 * @property string $village
 */
class newCoreStudent extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated'], 'safe'],
            [['created_by', 'school_type','school_code'], 'default', 'value' => null],
            [['created_by', 'school_type','bank_name'], 'integer'],
            [['active', 'archived'], 'boolean'],
            [['first_name', 'last_name', 'student_phone','student_email','school_student_registration_number','','guardian_email','guardian_relation', 'student_code','nin_no','parish','village','sub_county','county','region','district',], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1],
            [['date_of_birth', 'date_created', 'school', 'class_when_archived'], 'safe'],
            [['student_account_id', 'school_id'], 'integer'],
            [['active', 'allow_part_payments', 'archived'], 'boolean'],
            [['student_account_id'], 'unique'],
            [['disability_nature','middle_name','guardian_name','guardian_phone','allow_part_payments'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'school_name' => 'School Name',
            'school_type' => 'School Type',
            'location' => 'Location',
            'phone_contact_1' => 'Phone Contact 1',
            'phone_contact_2' => 'Phone Contact 2',
            'contact_email_1' => 'Contact Email 1',
            'contact_email_2' => 'Contact Email 2',
            'school_code' => 'School Code',
        ];
    }

    public function formAttributeConfig() {

        $config =[
            ['attributeName'=>'school_name', 'controlType'=>'text'],
            ['attributeName'=>'school_type', 'controlType'=>'school_type_picker', 'placeholder'=>'Choosinga a school', 'prompt'=>'Choosinga a school'],
            ['attributeName'=>'location', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_1', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_2', 'controlType'=>'text'],
            ['attributeName'=>'bank_name', 'controlType'=>'bank_name_search', 'placeholder'=>'Select a bank', 'prompt'=>'Select a bank'],
            ['attributeName'=>'account_number', 'controlType'=>'text'],
        ];

        return $config;
    }

    public function updateAttributeConfig() {

        $config =[
            ['attributeName'=>'school_name', 'controlType'=>'text'],
            ['attributeName'=>'school_type', 'controlType'=>'school_type_picker', 'placeholder'=>'Choosinga a school', 'prompt'=>'Choosinga a school'],
            ['attributeName'=>'location', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_1', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_2', 'controlType'=>'text'],
            ['attributeName'=>'account_number', 'controlType'=>'text'],
        ];

        return $config;
    }


    public function getSchoolType()
    {
        return $this->hasOne(CoreControlSchoolTypes::className(), ['id' => 'school_type']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'created_by']);
    }
    public function getBankName()
    {
        return $this->hasOne(CoreBankDetails::className(), ['id' => 'bank_name']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($id)
    {
        $attributes = [

            'school_name',
            [
                'label' => 'School Type',
                'value' => function ($model) {
                    return $model->schoolType->description;
                },
            ],
            'school_code',
            'phone_contact_1',
            'district',
            'school_registration_number_format',
            'sample_school_registration_number',
            'daily_stats_recipients',
            'account_type',
            'account_title',
            'contact_email_1',
            'region',
            'district',
            'county',
            'sub_county',
            'parish',
            'village',
            [
                'label' => 'Bank Name',
                'value' => function ($model) {
                    return $model->bankName->bank_name;
                },
            ],
            'account_number',
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSchool::findOne($id)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

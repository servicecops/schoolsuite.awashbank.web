<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_guardian".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $name
 * @property string $phone_contact_1
 * @property string $phone_contact_2
 * @property string $address
 * @property string $email_address
 * @property string $school_id
 */
class CoreGuardian extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_guardian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated'], 'safe'],
            [['name', 'phone_contact_1'], 'required'],
            [['created_by', 'school_id'], 'default', 'value' => null],
            [['created_by', 'school_id'], 'integer'],
            [['name', 'phone_contact_1', 'phone_contact_2', 'address', 'email_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'name' => 'Name',
            'phone_contact_1' => 'Phone Contact 1',
            'phone_contact_2' => 'Phone Contact 2',
            'address' => 'Address',
            'email_address' => 'Email Address',
            'school_id' => 'School ID',
        ];
    }

    public function formAttributeConfig() {
        $config = [
            ['attributeName'=>'name', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'email_address', 'controlType'=>'text'],
            ['attributeName'=>'address', 'controlType'=>'text'],
        ];

        return $config;
    }
    public function updateAttributeConfig() {
        $config = [
            ['attributeName'=>'name', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'email_address', 'controlType'=>'text'],
            ['attributeName'=>'address', 'controlType'=>'text'],
        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'created_by']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'name',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            'phone_contact_1',
            'phone_contact_2',
            'address',
            'email_address',
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreGuardian::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function updateModel($params)
    {
        $attributes = [
            'id',
            'name',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            'phone_contact_1',
            'phone_contact_2',
            'address',
            'email_address',
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreGuardian::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }



    }
}

<?php

namespace app\modules\schoolcore\models;

use app\modules\messages\models\MsgSchoolCredits;
use Yii;

/**
 * This is the model class for table "school_module_association".
 *
 * @property integer $id
 * @property integer $school_id
 * @property integer $module_id
 */
class SchoolModules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_module_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'module_id'], 'required'],
            [['school_id', 'module_id'], 'integer'],
            [['school_id', 'module_id'], 'unique', 'targetAttribute' => ['school_id', 'module_id'], 'message' => 'The combination of School ID and Module ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School',
            'module_id' => 'Module',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->module_id ==3){
                $msgCredit = MsgSchoolCredits::find()->where(['school_id'=>$this->school_id])->limit(1)->one();
                $credit =$msgCredit ? $msgCredit : new MsgSchoolCredits();
                $credit->school_id = $this->school_id;
                $credit->credit_bal = 0;
                $credit->description = $credit->school->school_name." -  Message A/C";
                $credit->save(false);
            }
            return true;
        } else {
            return false;
        }

    }
}

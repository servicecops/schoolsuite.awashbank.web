<?php

namespace app\modules\schoolcore\models;

use app\models\User;
use app\modules\feesdue\models\FeeClass;
use app\modules\gradingcore\models\Grading;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_school_class".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property bool $active
 * @property string $class_code
 * @property string $class_description
 * @property string $school_id
 *  @property CoreSchool $institution0

 */
class CoreSchoolClass extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $uploads, $grading , $class_id;
    public static function tableName()
    {
        return 'core_school_class';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated','class_id','grading'], 'safe'],
            [['created_by', 'school_id'], 'default', 'value' => null],
            [['created_by', 'school_id','class_teacher'], 'integer'],
            [['active'], 'boolean'],
            [['school_id','class_code','class_description'], 'required'],
            [['class_code', 'class_description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'active' => 'Active',
            'class_code' => 'Class Code',
            'class_description' => 'Class Description',
            'school_id' => 'School Name',
            'class_teacher' => 'Class Teacher',
        ];
    }
    public function formAttributeConfig() {

        $config =[
            ['attributeName'=>'class_code', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_only', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'class_description', 'controlType'=>'text'],
            ['attributeName'=>'class_teacher', 'controlType'=>'class_teacher_only', 'placeholder'=>'Choose class', 'prompt'=>'Choose class']];

        return $config;
    }

    public function updateAttributeConfig() {

        $config =[
            ['attributeName'=>'class_code', 'controlType'=>'text'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_only', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'class_description', 'controlType'=>'text'],
            ['attributeName'=>'active', 'controlType'=>'boolean']];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getClassTeacher()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'class_teacher']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'class_code',
            'class_description',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSchoolClass::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeClassAssociation()
    {
        return $this->hasMany(FeeClass::className(), ['class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitution0()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getCUnebActive()
    {
        $unebVal = $this->institution0->unebActive;
        if($unebVal != 0){
            $unebAssc = (new \yii\db\Query())->select(['cc.class_name AS cert'])->from('uneb_class_association ca')
                ->join('join', 'uneb_class_classification cc', 'cc.id=ca.uneb_classification')->where(['class_id'=>$this->id])->limit(1);
            if($unebAssc->exists()){
                $unebAssc = $unebAssc->one();
                return $unebAssc['cert'];
            }
        }else
            return 0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentInformations()
    {
        return $this->hasMany(CoreStudent::className(), ['student_class' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //Convert names to upper case
            $this->class_code = strtoupper(trim($this->class_code));
            $this->class_description = strtoupper(trim($this->class_description));
            return true;
        } else {
            return false;
        }
    }
    public function saveGrades()
    {
        Yii::trace($this->grading);

        if(is_array($this->grading) && $this->grading){

            foreach ($this->grading as $k=>$v) {

                $contact = new Grading();
                $contact->created_by = Yii::$app->user->identity->getId();
                if(\app\components\ToWords::isSchoolUser()){
                    $contact->school_id = Yii::$app->user->identity->school_id;
                }else{
                    $contact->school_id = $this->school_id;
                }

                $contact->min_mark = $v['min'];
                $contact->max_mark = $v['max'];
                $contact->grades = $v['grades'];
                $contact->class_id = $this->class_id;
                $contact->save(false);
                Yii::trace('Saved grade ' . $contact->grades);
            }
        }
    }

    public function updateGrades()
    {
        Yii::trace("thererer");
        $grades = $this->getRetrieveGrades();
        Yii::trace($grades);
        $db_grades = array_column($grades, 'id');
        Yii::trace($db_grades);

        $post_grades = array_column($this->grading, 'id');
        Yii::trace($post_grades);
        $diff_del = array_diff($db_grades, $post_grades);

        Yii::trace($diff_del);
        if (!empty($diff_del)) {
            foreach ($diff_del as $v) {
                $contact = Grading::find()->where(['id' => $v])->limit(1)->one();
                $contact->delete();
            }
        }

        Yii::trace($this->grading);
        Yii::trace($this->id);
        foreach ($this->grading as $k => $v) {
            $contact = isset($v['id']) ? Grading::find()->where(['id' => $v['id']])->limit(1)->one() : new Grading();
            if(\app\components\ToWords::isSchoolUser()){
                $contact->school_id = Yii::$app->user->identity->school_id;
            }else{
                $contact->school_id = $this->school_id;
            }

            $contact->min_mark = $v['min_mark'];
            $contact->max_mark = $v['max_mark'];
            $contact->grades = $v['grades'];
            $contact->comment = $v['comment'];
            $contact->class_id = $this->id;
            $contact->save(false);
        }
    }

    public function saveClasses()
    {


        if(is_array($this->grading) && $this->grading){

            foreach ($this->grading as $k=>$v) {
                $contact = new CoreSchoolClass();

                $contact->school_id = $this->school_id;
                $contact->class_code = $v['min'];
                $contact->class_description = $v['grades'];
                $contact->save(false);
                Yii::trace('Saved grade ' . $contact->grades);
            }
        }
    }
    public function getRetrieveGrades(){
        $grades = (new Query())->select(['id', 'min_mark','class_id', 'max_mark', 'grades','comment'])
            ->from('core_grades')
            ->where(['class_id'=>$this->id]);
        return $grades->all();
    }

    /**
     * Given a class, code will attempt to retrieve the class and if it doesnt exist will create it
     * @param $class_code
     * @return CoreSchoolClass
     */
    public static function getOrAutoCreateClassByCode($school_id, $class_code)
    {
        $class_code = str_replace(' ', '', strtoupper(trim($class_code)));
        $theClass = self::findOne(['school_id'=>$school_id, 'class_code'=>$class_code]);

        if(!$theClass) {
            $theClass = new CoreSchoolClass();
            $theClass->school_id = $school_id;
            $theClass->class_code = $class_code;
            $theClass->class_description = $class_code;
            $theClass->save();
        }

        return $theClass;
    }


}

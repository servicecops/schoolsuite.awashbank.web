<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\BookCategory;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * BookCategorySearch represents the model behind the search form of `app\modules\schoolcore\models\BookCategory`.
 */
class AwashBranchesSearch extends AwashBranches
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['branch_name', 'branch_code','date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function search($params)
    {


        $this->load($params);
        //Trim the model searches
        $this->branch_name = trim($this->branch_name);
        $this->branch_code = trim($this->branch_code);

        $query = new Query();
        $query ->select(['ab.id', 'ab.branch_code', 'ab.branch_name','abr.bank_region_name'])
            ->from('awash_branches ab')
            ->innerJoin('awash_bank_region abr', 'abr.id =ab.branch_region');



        if (!(empty($this->branch_code))) {
            Yii::trace($this->branch_code);

             //Registration numbers usually have a dash - or slass /
                $query->andWhere(['ilike','branch_code' ,trim(strtoupper($this->branch_code))]);

        }
        if (!(empty($this->branch_name))) {
            Yii::trace($this->branch_name);

             //Registration numbers usually have a dash - or slass /
                $query->andWhere(['ilike','branch_name' , trim(strtoupper($this->branch_name))]);

        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'branch_code', 'branch_name', 'bank_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ab.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

}

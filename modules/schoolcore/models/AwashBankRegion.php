<?php

namespace app\modules\schoolcore\models;
use app\modules\schoolcore\models\RefRegion\BranchRegion;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "nominated_bank_details".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $bank_address
 * @property string $bank_code
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $date_created
 * @property integer $bank_logo
 */
class AwashBankRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'awash_bank_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_region_name', ], 'string'],


        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_region_name'=>'Bank Region Name',
            'branch_code' => 'Branch Code',

        ];
    }




    public function viewModel($params)
    {

        $attributes = [
            'bank_region_name',



        ];

        if (($models = AwashBankRegion::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models, ];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    public function getRegionName()
    {
        return $this->hasOne(BranchRegion::className(), ['id' => 'branch_region']);
    }

}

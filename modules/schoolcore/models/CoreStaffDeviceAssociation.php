<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_teacher".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $device_id
 * @property string $last_name
 * @property string $staff_web_ser_id
 * @property string $staff_id
 * @property string $school_id
 */
class CoreStaffDeviceAssociation extends BaseModel
{
    public $class_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staff_device_assciation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['staff_id',  'school_id','staff_web_ser_id'], 'integer'],
            [['device_id'], 'string']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'staff_web_ser_id' => 'Date Updated',
            'device_id' => 'Device Id',
            'school_id' => 'School Id',
            'staff_id' => 'Staff Id',
        ];
    }

    public function formAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
            ['attributeName'=>'other_names', 'controlType'=>'text'],
            ['attributeName'=>'gender', 'controlType'=>'gender_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_only', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'email_address', 'controlType'=>'text'],
            ['attributeName'=>'user_level', 'controlType'=>'staff_levels_id', 'placeholder'=>'Choose Staff Levels', 'prompt'=>'Choose Staff Levels'],
            ['attributeName'=>'staff_type_id', 'controlType'=>'staff_type_id', 'placeholder'=>'Choose Staff Type', 'prompt'=>'Choose Staff Type'],

        ];

        return $config;
    }

    public function updateAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
            ['attributeName'=>'other_names', 'controlType'=>'text'],
            ['attributeName'=>'gender', 'controlType'=>'gender_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'email_address', 'controlType'=>'text'],
            ['attributeName'=>'user_level', 'controlType'=>'staff_levels_id', 'placeholder'=>'Choose Staff Levels', 'prompt'=>'Choose Staff Levels'],
            ['attributeName'=>'staff_type_id', 'controlType'=>'staff_type_id', 'placeholder'=>'Choose Staff Type', 'prompt'=>'Choose Staff Type'],

        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'first_name',
            'last_name',
            'gender',
            'phone_contact_1',
            'phone_contact_2',
            'email_address',
            'user_level',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],

            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreStaff::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password,$this->password);
    }

    public function getTeacherName(){
        return $this->first_name .'-'.$this->last_name;
    }
    public function findSubjects($grp, $params)
    {
        Yii::trace($params);
        //$query1 = \app\models\StudentGroupStudent::find()->select(['student_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['si.id', 'si.subject_code', 'si.subject_name', 'cls.class_code'])
            ->from('core_subject si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andFilterWhere([
                'si.class_id'=>$params['class_id']]);
        // 'si.school_id'=>$grp['sch']]);

        $query->orderBy('cls.class_code', 'si.subject_name');
        $query->limit(500);
        return $query->all();

    }

    public static function findMembers($id){
        $query = (new Query())->select(['si.id', 'si.subject_name', 'si.subject_code', 'cls.class_code'])
            ->from('subject_group_subject sgr')
            ->innerJoin('core_subject si', 'si.id=sgr.subject_id')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andWhere(['sgr.group_id'=>$id])
            ->orderBy('cls.class_code', 'si.subject_name');
        return $query->all();
    }
    public function getTeachersFullname()
    {
        $fn = $this->first_name ? $this->first_name : '';
        $mn = $this->middle_name ? $this->middle_name : '';
        $ln = $this->last_name ? $this->last_name : '';
        return $fn . ' ' . $mn . ' ' . $ln;
    }

}

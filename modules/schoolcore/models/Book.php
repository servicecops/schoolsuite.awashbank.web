<?php

namespace app\modules\schoolcore\models;

use app\models\ImageBank;
use Yii;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string|null $date_created
 * @property string|null $date_updated
 * @property int|null $created_by
 * @property string|null $book_title
 * @property int $category
 * @property string|null $ISBN_number
 * @property string|null $author
 * @property string|null $publisher
 * @property int $shelf_number
 * @property string $book_cover
 * @property int|null $school_id
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated'], 'safe'],
            [['created_by', 'category', 'shelf_number'], 'default', 'value' => null],
            [['created_by', 'category', 'shelf_number', 'school_id'], 'integer'],
            [['category', 'shelf_number'], 'required'],
            [['book_title', 'ISBN_number', 'author', 'publisher'], 'string', 'max' => 255],
        ];
    }

//    public function scenarios()
//    {
//        $scenarios['photo'] = ['book_cover'];
//        return $scenarios;
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'book_title' => 'Book Title',
            'category' => 'Category',
            'ISBN_number' => 'Isbn Number',
            'author' => 'Author',
            'publisher' => 'Publisher',
            'shelf_number' => 'Shelf Number',
        ];
    }

    public function getCategory(){
        return BookCategory::find()->where(['id' => $this->id]);
    }



    public function getAvailable()
    {
        return BookCopy::find()->where(['book_id' => $this->id, 'available'=>true])->count();
    }

    public function getTotalBooks()
    {
        return BookCopy::find()->where(['book_id' => $this->id])->count();
    }

    public function getBookCover()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'book_cover']);
    }
}

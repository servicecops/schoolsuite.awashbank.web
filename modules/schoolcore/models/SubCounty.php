<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "sub_county".
 *
 * @property int $id
 * @property string|null $sub_county_name
 * @property int|null $county_id
 * @property string|null $created_on
 * @property int|null $created_by
 * @property string|null $updated_on
 * @property int|null $updated_by
 */
class SubCounty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_county';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'county_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['id', 'county_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['sub_county_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sub_county_name' => 'Sub County Name',
            'county_id' => 'County ID',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public static function getProdList($dist_id, $cnty_id){
        $subcounties=self::find()
            ->select(['id', 'sub_county_name'])
            ->where(['district_id'=>$dist_id])
            ->andWhere(['county_id'=>$cnty_id])
            ->asArray()
            ->all();
        return $subcounties;
    }
}

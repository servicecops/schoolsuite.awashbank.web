<?php

namespace app\modules\schoolcore\models;

use app\components\ToWords;
use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *

 * @property string $term_name
 * @property string $term_starts
 * @property string $id
 * @property string $school_id
 * @property string $term_ends
 * @property string $date_modified
 * @property string $created_by
 * @property string $date_created
 * @property string $term_description
 */
class CoreTerm extends BaseModel
{
    public $quarter_detail, $semester_type_detail;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_term';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_modified','date_updated', 'term_starts','term_ends', ], 'safe'],
            [['created_by',  'school_id', ], 'default', 'value' => null],
            [['created_by',  'school_id', ], 'integer'],
            [[ 'school_id', 'term_starts','term_ends', 'term_name','term_description','term_type',], 'required'],
            [['term_name','term_description','quarter_detail','term_type_detail','semester_type_detail'], 'string', 'max' => 255],
            [['term_starts', 'term_ends'], 'validEndOfTermDate', 'skipOnEmpty' => false, 'skipOnError' => false],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'term_name' => 'Term Name',
            'term_starts' => 'Beginning of Term',
            'term_ends'=>'End of Term',
            'school_id'=>'School Name',
            'created_by'=>'Created By',
            'date_updated'=>'Date Modified',
            'term_description'=>'Term Description',

        ];
    }

    public function formAttributeConfig() {
        $config = [

            ['attributeName'=>'term_name', 'controlType'=>'text'],
            ['attributeName'=>'term_description', 'controlType'=>'text'],
            ['attributeName'=>'term_starts', 'controlType'=>'date_picker'],
            ['attributeName'=>'term_ends', 'controlType'=>'date_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_only', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],

        ];

        return $config;
    }
    public function updateAttributeConfig() {
        $config = [

            ['attributeName'=>'term_name', 'controlType'=>'text'],
            ['attributeName'=>'term_description', 'controlType'=>'text'],
            ['attributeName'=>'term_starts', 'controlType'=>'date_picker'],
            ['attributeName'=>'term_ends', 'controlType'=>'date_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_only', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],

        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }
    public function getTeacherName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'subject_teacher_id']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [


            'term_name',
            'term_description',
            'term_starts',
            'term_ends',
            'term_type',
            'term_type_detail',

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_updated',
        ];

        if (($models = CoreTerm::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    public function findSchoolTeachers(){

        $query = (new Query())
            ->select(['id','term_name','term_description','term_ends', 'term_starts',  'date_created', 'date_modified' ])
            ->from('core_term ');
           // ->innerJoin('core_school csc', 'ct.school_id= csc.id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function validEndOfTermDate($attribute, $params)
    {
       if (!ToWords::validateDate($this->getAttribute($attribute), 'Y-m-d')) {
            $this->addError($attribute, 'Invalid date format. Enter a valid date in the format yyyy-mm-dd');
            return;
        }

        if($this->term_starts && $this->term_ends && $this->term_ends<=$this->term_starts) {
            $this->addError('term_starts', 'Start of term cannot be after end of term');
            $this->addError('term_ends', 'End of term cannot be before start of term');
            return;
        }
    }

}

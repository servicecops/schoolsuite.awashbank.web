<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "core_student_guardian_association".
 *
 * @property string $id
 * @property string $school_id
 * @property string $student_id
 * @property string $guardian_id
 * @property int $relationship_id
 */
class CoreStudentGuardianAssociation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_student_guardian_association';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id', 'student_id', 'guardian_id', 'relationship_id'], 'required'],
            [['school_id', 'student_id', 'guardian_id', 'relationship_id'], 'default', 'value' => null],
            [['school_id', 'student_id', 'guardian_id', 'relationship_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'student_id' => 'Student ID',
            'guardian_id' => 'Guardian ID',
            'relationship_id' => 'Relationship ID',
        ];
    }
}

<?php

namespace app\modules\schoolcore\models;

use app\modules\schoolcore\models\BookCopy;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * BookCopySearch represents the model behind the search form of `app\modules\schoolcore\models\BookCopy`.
 */
class BookCopySearch extends BookCopy
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'book_id'], 'integer'],
            [['date_created', 'date_updated', 'ISBN_number'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BookCopy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'book_id' => $this->book_id,
        ]);

        $query->andFilterWhere(['ilike', 'ISBN_number', $this->ISBN_number]);

        return $dataProvider;
    }

    public function searchCopy($params, $id)
    {
        $query = BookCopy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'book_id' => $this->book_id,
        ]);
        $query->andWhere(['book_id' =>$id]);

        $query->andFilterWhere(['ilike', 'ISBN_number', $this->ISBN_number]);

        return $dataProvider;
    }

    public function searchIssued($params)
    {
        $query = BookCopy::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'book_id' => $this->book_id,
        ]);
        $query->andWhere(['available' =>false]);

        $query->andFilterWhere(['ilike', 'ISBN_number', $this->ISBN_number]);

        return $dataProvider;
    }
}

<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * CoreStudentSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreStudent`.
 */
class CoreStudentSearch extends CoreStudent
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['student_code', 'first_name','last_name', 'middle_name', 'class_code', 'school_name', 'student_email', 'student_phone','campus_id'],
            'fileName' => 'Students',
            'title' => 'student Datasheet',
            'exportFile' => '@app/modules/schoolcore/views/core-student/exportPdfExcel',
        ];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_code'], 'integer'],
            [['modelSearch'], 'trim'],
            [['active', 'archived'], 'boolean'],
            [['student_code','guardian_phone_2', 'class_id', 'class_when_archived','campus_id', 'school_id', 'schsearch', 'modelSearch', 'school_student_registration_number', 'date_of_birth', 'last_name', 'pc_rgnumber', 'phoneSearch'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $this->school_student_registration_number = trim($this->school_student_registration_number);

        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.middle_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name", 'si.web_user_id',
            'si.school_student_registration_number'])
            ->from('core_student si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            //
            ->Where(['si.archived' => false]);

        if($this->campus_id == 0) $this->campus_id = null;
        $query->andFilterWhere(['campus_id'=>$this->campus_id]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
               // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);
            } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    private function nameLogic($query, $seachName)
    {
        $search_words = preg_split("/[\s,]+/", $seachName);
        $search_words = array_filter($search_words);
        $words = array();
        if (count($search_words) > 1) {
            foreach ($search_words as $word) {
                $words[] = $word;
            }
            if (isset($words[0])) {
                $name = str_replace(' ', '', $words[0]);
                $query->andFilterWhere(['or',
                    ['school_student_registration_number' => trim(strtoupper($this->modelSearch))],
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }
            if (isset($words[1])) {
                $name = str_replace(' ', '', $words[1]);
                $query->andFilterWhere(['or',
                    ['school_student_registration_number' => trim(strtoupper($this->modelSearch))],
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }
            if (isset($words[2])) {
                $name = str_replace(' ', '', $words[2]);
                $query->andFilterWhere(['or',
                    ['school_student_registration_number' => trim(strtoupper($this->modelSearch))],
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }

        } else {
            $name = str_replace(' ', '', $seachName);
            $query->andFilterWhere(['or',
                ['school_student_registration_number' => trim(strtoupper($this->modelSearch))],
                ['ilike', 'first_name', $name],
                ['ilike', 'middle_name', $name],
                ['ilike', 'last_name', $name],

            ]);
        }
        return $query;

    }


    public function findStudent()
    {
        if (!empty($this->pc_rgnumber)) {
            if (preg_match("#^[0-9]{10}$#", $this->pc_rgnumber)) {
                $query = CoreStudent::find()->where(['student_code' => $this->pc_rgnumber, 'active' => true, 'archived' => false])->limit(1)->one(Yii::$app->db);
            } else {
                $query = CoreStudent::find()->where(['school_student_registration_number' => trim(strtoupper($this->pc_rgnumber))])->andWhere(['active' => true, 'archived' => false])->all(Yii::$app->db);
            }
            return $query ? $query : null;

        }
    }


    public function searchArchived($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone'])
            ->from('core_student si')
            ->innerJoin('core_school sch', 'sch.id=si.school_id')
            ->leftJoin('core_school_class cls', 'cls.id=si.class_when_archived')
            ->andWhere(['si.archived' => true]);

        if (\app\components\ToWords::isSchoolUser()) {
            if (!empty($this->class_when_archived)) {
                $query->andWhere(['si.class_when_archived' => intVal($this->class_when_archived)]);
            } else {
                $query->andWhere(['si.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_when_archived' => intVal($this->class_when_archived)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['si.school_id' => $this->schsearch]);
            }
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }

        if (!(empty($this->modelSearch))) {
            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);

            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }
/*    public function findStudentInfo()
    {
        $query = CoreStudent::find()
            ->filterWhere(['school_id' => $this->schsearch, 'class_id' => $this->class_id, 'active' => true, 'archived' => false]);
        $query = $this->nameLogic($query, $this->modelSearch);
//            ->andFilterWhere(['or',
//                ['ilike', 'first_name', $this->modelSearch],
//                ['ilike', 'last_name', $this->modelSearch],
//            ]);

        //If phone is a valid phone, search by phone too
        $query->andFilterWhere([
            'or',
            ['guardian_phone'=>$this->phoneSearch],
            ['student_phone'=>$this->phoneSearch],
        ]);
        return $query->all(Yii::$app->db);
    }*/

    public function findStudentInfo()
    {
        $query = CoreStudent::find()
            ->filterWhere(['school_id' => $this->schsearch, 'class_id' => $this->class_id, 'active' => true, 'archived' => false]);

        $query = $this->nameLogic($query, $this->modelSearch);
//            ->andFilterWhere(['or',
//                ['ilike', 'first_name', $this->modelSearch],
//                ['ilike', 'last_name', $this->modelSearch],
//            ]);

        return $query->limit(10)->all(Yii::$app->db);
    }


    public function searchStudentsWtPswd($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $this->school_student_registration_number = trim($this->school_student_registration_number);

        $query = new Query();
        $query->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'si.school_id','cls.class_code', 'sch.school_name', 'si.student_email',
            'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            'si.school_student_registration_number'])
            ->from('core_student si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            //
            ->Where(['si.archived' => false])
            ->andWhere(['si.is_password_created'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['student_code' => $this->modelSearch]);
                $query->orWhere(['schoolpay_paymentcode' => $this->modelSearch]);

            } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function searchgeneratePswd($params)
    {
        $this->load($params);
        //Trim the model searches

    }

}

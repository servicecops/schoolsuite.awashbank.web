<?php

namespace app\modules\schoolcore\models;

use app\components\ToWords;
use app\models\BaseModel;

use app\models\ImageBank;
use app\models\SchoolTypes;
use app\models\User;
use app\modules\banks\models\BankDetails;
use app\modules\feesdue\models\InstitutionFeesDue;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\SchoolAccountGl;
use app\modules\paymentscore\models\SchoolAccountTransactionHistory;
use app\modules\schoolcore\models\RefRegion\BranchRegion;
use app\modules\schoolcore\models\RefRegion\RefRegion;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_school".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $school_name
 * @property int $school_type
 * @property string $location
 * @property string $phone_contact_1
 * @property string $school_logo
 * @property string $phone_contact_2
 * @property string $contact_email_1
 * @property string $contact_email_2
 * @property string $bank_account_number
 * @property string $school_code
 * @property string $account_number
 * @property string $contact_person
 * @property string $bank_name
 * @property string $enable_daily_stats
 * @property string $default_part_payment_behaviour
 * @property string $school_registration_number_format
 * @property string $sample_school_registration_number
 * @property string $account_type
 * @property string $account_title
 * @property string $region
 * @property integer $district
 * @property integer $schoolpay_schoolcode
 * @property string $county
 * @property string $sub_county
 * @property string $parish
 * @property string $village
 * @property CoreControlSchoolTypes $school
 * @property SchoolAccountGl $schoolAccount
 * @property PaymentsReceived[] $paymentsReceiveds
 * @property string $box_no
 * @property string $vision
 * @property string $mission
 * @property string $school_reference_no;
 * @property string $sms_sender_id;
 */
class CoreSchool extends BaseModel
{
    public $sp_modules, $section_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_school';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'school_logo'], 'safe'],
            [['created_by', 'school_type', ], 'default', 'value' => null],
            [['created_by', 'school_type', 'schoolpay_schoolcode',  'bank_name', 'district','branch_id','branch_region'], 'integer'],
            [['school_type', 'school_name', 'bank_name', 'contact_person', 'phone_contact_1', 'contact_email_1','external_school_code','district','region','branch_id','branch_region'], 'required'],
            [['school_name', 'account_number', 'parish', 'bank_account_number', 'village', 'sub_county','transaction_api_password','tertiary','non_tertiary',
                'other_tertiary_level','school_level', 'county', 'region', 'account_type', 'account_title', 'school_registration_number_format',
                'license_number','source_of_funding','sex_composition','other_source_of_funding','day_boarding_structure','distance_to_deo','distance_to_nearest_health_facility','distance_to_nearest_sch_of_same_level',
                'sample_school_registration_number', 'location', 'phone_contact_1', 'phone_contact_2', 'contact_person', 'contact_email_1', 'contact_email_2','registration_status','founding_body','legal_ownership','year_founded','other_legal_ownership',
                 'box_no', 'vision', 'mission','school_reference_no', 'sms_sender_id'], 'string', 'max' => 255],

            [['active', 'default_part_payment_behaviour', 'enable_daily_stats', 'enable_school_sections_logic', 'enable_transaction_api',
                'enable_bank_statement', 'is_sync','allow_payments_on_zero_balance','allow_payment_reversal'], 'boolean'],
            [['phone_contact_1', 'phone_contact_2'], 'validePhone', 'skipOnEmpty' => true, 'skipOnError' => false],
            [['contact_email_1','contact_email_2'] ,'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'school_name' => 'School Name',
            'school_type' => 'School Type',
            'location' => 'Location',
            'school_level' => 'School Level',
            'phone_contact_1' => 'Phone Contact 1',
            'phone_contact_2' => 'Phone Contact 2',
            'contact_email_1' => 'Contact Email 1',
            'contact_email_2' => 'Contact Email 2',
            'bank_account_number' => 'Bank Ac No',
            'school_code' => 'School Code',
            'district_id' => 'District ',
            'village' => 'Physical Address',
            'bank_name' => 'Primary Bank',
            'default_part_payment_behaviour' => 'Allow part payments',
            'school_registration_number_format' => 'Reg No format',
            'sample_school_registration_number' => 'Sample Reg Number',
            'vision' => 'School Vision',
            'mission' => 'School Mission',
            'box_no' => 'P.O.BOX',
            'school_reference_no'=>'School Reference No',
            'external_school_code'=>'School Code',
            'sms_sender_id'=>'SMS Sender ID'
        ];
    }

    public function formAttributeConfig()
    {

        $config = [
            ['attributeName' => 'school_name', 'controlType' => 'text'],
            ['attributeName' => 'school_type', 'controlType' => 'school_type_picker', 'placeholder' => 'Choosinga a school', 'prompt' => 'Choosinga a school'],
            ['attributeName' => 'location', 'controlType' => 'text'],
            ['attributeName' => 'phone_contact_1', 'controlType' => 'text'],
            ['attributeName' => 'phone_contact_2', 'controlType' => 'text'],
            ['attributeName' => 'contact_email_1', 'controlType' => 'text'],
            ['attributeName' => 'contact_email_2', 'controlType' => 'text'],
            ['attributeName' => 'bank_name', 'controlType' => 'bank_name_search', 'placeholder' => 'Select a bank', 'prompt' => 'Select a bank'],
            ['attributeName' => 'account_number', 'controlType' => 'text'],
            ['attributeName' => 'sms_sender_id', 'controlType' => 'text'],
        ];

        return $config;
    }

    public function updateAttributeConfig()
    {

        $config = [
            ['attributeName' => 'school_name', 'controlType' => 'text'],
            ['attributeName' => 'school_type', 'controlType' => 'school_type_picker', 'placeholder' => 'Choosinga a school', 'prompt' => 'Choosinga a school'],
            ['attributeName' => 'location', 'controlType' => 'text'],
            ['attributeName' => 'phone_contact_1', 'controlType' => 'text'],
            ['attributeName' => 'phone_contact_2', 'controlType' => 'text'],
            ['attributeName' => 'contact_email_1', 'controlType' => 'text'],
            ['attributeName' => 'contact_email_2', 'controlType' => 'text'],
            ['attributeName' => 'account_number', 'controlType' => 'text'],
            ['attributeName' => 'sms_sender_id', 'controlType' => 'text'],
        ];

        return $config;
    }


    public function getSchoolType()
    {
        return $this->hasOne(CoreControlSchoolTypes::className(), ['id' => 'school_type']);
    }
    public function getBranches()
    {
        return $this->hasOne(AwashBranches::className(), ['id' => 'branch_id']);
    }
 public function getRegions()
    {
        return $this->hasOne(BranchRegion::className(), ['id' => 'branch_region']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'created_by']);
    }

    public function getBankName()
    {
        return $this->hasOne(CoreBankDetails::className(), ['id' => 'bank_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses()
    {
        return CoreSchoolClass::find()->where(['school_id' => $this->id])
            ->andWhere(['<>', 'class_code', '__ARCHIVE__'])->orderBy('class_code')->all();
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($id)
    {
        $attributes = [

            'school_name',
            [
                'label' => 'School Type',
                'value' => function ($model) {
                    return $model->schoolType->description;
                },
            ],
            'school_code',
            'phone_contact_1',
            'district',
            'school_registration_number_format',
            'sample_school_registration_number',
            'daily_stats_recipients',
            'account_type',
            'account_title',
            'contact_email_1',
            'region',
            'district',
            'county',
            'sub_county',
            'parish',
            'village',

            [
                'label' => 'District',
                'value' => function ($model) {
                    return $model->districtName->district_name;
                }
            ],
            [
                'label' => 'Bank Name',
                'value' => function ($model) {
                    return $model->bankName->bank_name;
                },
            ],
            'account_number',
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname . ' ' . $model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSchool::findOne($id)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function getDistricts()
    {
        return $this->hasOne(District::className(), ['id' => 'dist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionFeesDues()
    {
        return $this->hasMany(InstitutionFeesDue::className(), ['school_id' => 'id']);
    }

    public function enableStats($attribute, $params)
    {
        if (($this->enable_daily_stats == 1) && empty($this->daily_stats_recipients)) {
            $this->addError($attribute, 'Enter recipient for Daily Stats');

        }
    }

    public function getDistrictName()
    {
        return $this->hasOne(District::className(), ['id' => 'district']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAccount()
    {
        return $this->hasOne(SchoolAccountGl::className(), ['id' => 'school_account_id']);
    }

    public function getBank()
    {
        return $this->hasOne(BankDetails::className(), ['id' => 'bank_name']);
    }

    public function getAccountHistories()
    {
        $query = SchoolAccountTransactionHistory::find()->where(['account_id' => $this->school_account_id])->orderBy('date_created DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;

    }

    public function getSum()
    {
        return SchoolAccountTransactionHistory::find()->where(['account_id' => $this->school_account_id])->sum('amount');

    }

    public function getTransCount()
    {
        return SchoolAccountTransactionHistory::find()->where(['account_id' => $this->school_account_id])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsReceiveds()
    {
        // return $this->hasMany(PaymentsReceived::className(), ['school_id' => 'id']);
        $query = PaymentsReceived::find()->where(['school_id' => $this->id]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count('*', Yii::$app->db)]);
        $query->offset($pages->offset)->limit($pages->limit)->orderBy('date_created DESC');
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages];
    }

    public function getLogo()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'school_logo']);
    }

    public function getSchChannels()
    {
        return $this->hasMany(SchoolChannel::className(), ['institution' => 'id']);
    }

    public function getRegionName()
    {
        return $this->hasOne(RefRegion::className(), ['id' => 'region']);
    }


    public function hasModule($code)
    {
        if (in_array($code, $this->moduleCodes)) return true;
        else return false;
    }

    /**
     * Evolution don't judge
     */
    public function getModuleCodes()
    {
        $modules = (new \yii\db\Query())->select('mod.module_code')
            ->from('school_module_association assc')
            ->innerJoin('suite_modules mod', 'mod.id=assc.module_id')
            ->where(['school_id' => $this->id])->all();
        return array_column($modules, 'module_code');
    }

    public function getModules()
    {
        $modules = (new \yii\db\Query())->select('module_id')
            ->from('school_module_association')
            ->where(['school_id' => $this->id])->all();
        return array_column($modules, 'module_id');
    }

    public function getYearsList() {
        $currentYear = date('Y');
        $yearFrom = 1900;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }

    /**
     * Will validate a  sample registration number against the school's reg no format
     * @param $attribute
     * @param $params
     */
    public function validateRegNoAgainstSchoolFormat($registrationNumber)
    {
        //If empty then nothing to do
        if (empty($this->school_registration_number_format)) {
            //For schools without reg format, ban reserved formats
            $bankInterChannelCodes = Yii::$app->params['reservedRegistrationNumberFormats']; //An array of reserved regno formats
            foreach ($bankInterChannelCodes as $format) {
                $patternToUse = "/^" . $format . "$/";
                if (preg_match($patternToUse, $registrationNumber)) {
                    throw new Exception("The entered registration number $registrationNumber is in a format reserved for another school and is not acceptable");
                }
            }

            //No format enforced so return
            return;
        }
        if (empty($registrationNumber)) {
            //No format specified
            return;
        }
        $patternToUse = "/^" . $this->school_registration_number_format . "$/";

        if (!preg_match($patternToUse, $registrationNumber)) {
            throw new Exception("The registration number $registrationNumber does not match the school format " . $this->school_registration_number_format);
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->external_school_code = strtoupper(trim(str_replace(' ', '', $this->external_school_code)));
            $this->school_name = strtoupper(trim($this->school_name));

            //Correct phone numbers
            $validatedSchoolPhone1 = ToWords::validatePhoneNumber($this->phone_contact_1);
            if($validatedSchoolPhone1) {
                $this->phone_contact_1 = $validatedSchoolPhone1;
            }

            $validatedSchoolPhone2 = ToWords::validatePhoneNumber($this->phone_contact_2);
            if($validatedSchoolPhone2) {
                $this->phone_contact_2 = $validatedSchoolPhone2;
            }

            $this->sms_sender_id = strtoupper(trim(str_replace(' ', '', $this->sms_sender_id)));

            return true;
        } else {
            return false;
        }

    }

    public function validePhone($attribute, $params)
    {
        if (!preg_match('/^(0|251)(9|1)\d{8}$/i', $this->$attribute)) {
            $this->addError($attribute, 'Please Enter a valid Phone Number');
        }
    }
}

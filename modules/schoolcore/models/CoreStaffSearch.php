<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreStaff;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreTeacherSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreTeacher`.
 */
class CoreStaffSearch extends CoreStaff
{
    public $test,$answer,$schsearch;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_id', 'passport_photo', ], 'integer'],
            [['date_created', 'date_updated','schsearch' ,'first_name', 'last_name', 'other_names', 'gender', 'phone_contact_1', 'phone_contact_2', 'email_address', 'student_code', 'date_archived'], 'safe'],
            [['active', 'archived'], 'boolean'],
        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'School Staff',
            'title' => 'School Staff info Datasheet',
            'exportFile' => '@app/modules/schoolcore/views/core-staff/exportPdfExcel',
        ];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreStaff::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'school_id' => $this->school_id,
            'active' => $this->active,
            'archived' => $this->archived,
            'date_archived' => $this->date_archived,
            'passport_photo' => $this->passport_photo,
        ]);

        $query->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'other_names', $this->other_names])
            ->andFilterWhere(['ilike', 'gender', $this->gender])
            ->andFilterWhere(['ilike', 'phone_contact_1', $this->phone_contact_1])
            ->andFilterWhere(['ilike', 'phone_contact_2', $this->phone_contact_2])
            ->andFilterWhere(['ilike', 'email_address', $this->email_address]);

        return $dataProvider;
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params)
    {
        $this->load($params);


        //Trim the model searches
        $this->last_name = trim($this->last_name);
        $this->first_name = trim($this->first_name);

        $query = (new Query())
            ->select(['ct.id','ct.first_name', 'ct.last_name', 'csc.school_name', 'ct.gender','ct.date_created', 'ct.phone_contact_1', 'ct.email_address','lvl.description'])
            ->from('core_staff ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            //->innerJoin('user uz', 'ct.web_user_id= uz.id')
            ->innerJoin('auth_item lvl', 'ct.user_level= lvl.name');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if ($this->last_name) {
            $query->andWhere(['ct.last_name' => trim($this->last_name)]);
        }

        if ($this->first_name) {
            $query->andWhere(['ct.first_name' => trim($this->first_name)]);
        }


        if (\app\components\ToWords::isSchoolUser() ) {
            if ($this->school_id && $this->school_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['ct.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if ($this->schsearch) {
                $query->andWhere(['ct.school_id' => $this->schsearch]);
            }
        }
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            ['class' => 'kartik\grid\SerialColumn',],
            [

                'label' => 'First Name',
                'value'=>function($model){
                    return Html::a($model['first_name'], ['core-staff/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],

            [
                'label' => 'Last Name',
                'value'=>function($model){
                    return Html::a($model['last_name'], ['core-staff/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            'gender',
            'description',
            'phone_contact_1',
            'email_address',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa fa-eye"></i>', ['core-staff/view', 'id' => $model['id']]);
                },
            ] ,
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa fa-pencil"></i>', ['core-staff/update', 'id' => $model['id']]);
                },
            ]
            ,[

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa fa-tag"></i>', ['core-staff/attach-device', 'id' => $model['id']]);
                },
            ]
            ////
        ];
        $title ='List of Staff Members';
        $searchForm= 'first_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm,'title'=>$title];
    }


}

<?php

namespace app\modules\schoolcore\models;

use app\components\ToWords;
use app\models\BaseModel;

use app\models\ImageBank;
use app\models\User;
use app\modules\payment_plan\models\PaymentPlanSearch;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\StudentAccountGl;
use app\modules\paymentscore\models\StudentAccountHistory;
use Yii;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table "core_student".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $first_name
 * @property string $last_name
 * @property string $archive_reason
 * @property string $school_student_registration_number

 * @property string $other_names
 * @property string $date_of_birth
 * @property string $gender
 * @property string $class_id
 * @property string $school_id
 * @property string $phone_contact_1
 * @property string $phone_contact_2
 * @property string $email_address
 * @property integer $student_code
 * @property bool $active
 * @property bool $allow_part_payments
 * @property bool $archived
 * @property string $date_archived
 * @property string $passport_photo
 * @property string $account_id
 * @property string $student_phone
 * @property string $day_boarding
 * @property string $middle_name
 * @property string $nationality
 * @property string $student_email
 * @property string $guardian_name
 * @property string $guardian_relation
 * @property string $guardian_email
 * @property string $guardian_phone
 * @property integer $student_account_id
 * @property string $nin_no
 * @property string $region
 * @property string $district
 * @property string $county
 * @property string $sub_county
 * @property string $parish
 * @property string $disability_nature
 * @property string $village
 * @property integer $invalid_login_count
 * @property integer $campus_id
 * @property integer $schoolpay_paymentcode
 * @property integer $class_when_archived
 * @property boolean $locked
 * @property boolean $disability
 * @property boolean $is_password_created
 * @property string $password_reset_token
 * @property PaymentsReceived[] $paymentsReceiveds
 * @property CoreSchoolClass $studentClass
 * @property StudentAccountGl $studentAccount
 */
class CoreStudent extends User
{
    public $password2, $new_pass, $current_pass, $credit_hours;
    public $school;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_student';
    }

    public function __construct()
    {
        $this->campus_id = 0;
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'date_of_birth', 'date_archived', 'year_of_joining', 'default_part_payment_behaviour', 'locked'.'credit_hours'], 'safe'],
            [['created_by', 'class_id', 'school_id', 'passport_photo'], 'default', 'value' => null],
            [['created_by', 'class_id', 'school_id', 'passport_photo', 'account_id','student_code','schoolpay_paymentcode','class_when_archived','web_user_id'], 'integer'],
            [['class_id', 'school_id', 'first_name', 'last_name', 'gender','nationality','school_student_registration_number', 'date_of_birth'], 'required'],
            [['active', 'archived'], 'boolean'],
            [['school_student_registration_number'], 'uniqueRegNo', 'skipOnEmpty' => true, 'skipOnError' => false],
            [['first_name', 'archive_reason','last_name','orphan_type', 'student_phone','day_boarding','student_email','school_student_registration_number','guardian_email','guardian_relation','nin_no','guardian2_phone','disability_nature','parish','village','sub_county','county','region','district',], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1],
            [['date_of_birth', 'date_created', 'school', 'class_when_archived'], 'safe'],
            [['date_of_birth'], 'validateDateOfBirthFormat', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['student_account_id', 'school_id','campus_id'], 'integer'],
            [['active', 'allow_part_payments', 'archived','disability','is_password_created'], 'boolean'],
            [['student_account_id'], 'unique'],
            [['student_email'], 'unique', 'message'=>'This email has already been taken'],
            [['student_email','guardian_email'], 'email'],
            [['password_reset_token', 'auth_key'], 'string', 'max' => 2044],
            ['password', 'string', 'min' => 6],
            ['password2', 'compare', 'compareAttribute' => 'new_pass', 'on'=>['change', 'reset']],
            [['guardian_phone', 'student_phone', 'guardian2_phone'], 'validePhone', 'skipOnEmpty' => true, 'skipOnError' => false],
          //  [['first_name', 'last_name'], 'validatenames', 'skipOnEmpty' => true, 'skipOnError' => false],
            ['current_pass','checkOldPassword','on'=>'change','message'=>'', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['new_pass','diffPassword','on'=>'change','message'=>'', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['disability_nature','middle_name','guardian_name','guardian_phone','allow_part_payments', 'guardian2_name', 'guardian2_relation', 'guardian2_email', 'guardian2_phone'], 'safe']];

    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['photo'] = ['passport_photo'];
        $scenarios['change'] = ['current_pass', 'new_pass', 'password', 'password2'];
        $scenarios['reset'] = ['new_pass', 'password', 'password2'];
        return $scenarios;
    }

    public function getFullname()
    {
        $fn = $this->first_name ? $this->first_name : '';
        $mn = $this->middle_name ? $this->middle_name : '';
        $ln = $this->last_name ? $this->last_name : '';
        return $fn . ' ' . $mn . ' ' . $ln;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Names',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'class_id' => 'Class',
            'school_id' => 'School',
            'nationality' => 'Nationality',
            'disability' => 'Disability',
            'disability_nature' => 'Nature of Disability',
            'student_code' => 'Student Code',
            'active' => 'Active',
            'archived' => 'Archived',
            'date_archived' => 'Date Archived',
            'day_boarding' => 'Passport Photo',
            'account_id' => 'Account ID',
            'student_email' => 'Student Email',
            'student_phone' => 'Student Phone',
            'guardian_name' => 'Guardian Name',
            'guardian_relation' => 'Guardian Relation',
            'guardian_email' => 'Gurdian Email',
            'guardian2_name' => 'Guardian Name',
            'guardian2_relation' => 'Guardian Relation',
            'guardian2_email' => 'Guardian Email',
            'guardian2_phone' => 'Guardian Phone',
            'guardian_phone' => 'Guardian Phone',
            'allow_part_payments' => 'Allow Part Payments',
        ];
    }

    public function formAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
            ['attributeName'=>'middle_name', 'controlType'=>'text'],
          ['attributeName'=>'gender', 'controlType'=>'gender_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'student_phone', 'controlType'=>'text'],
            ['attributeName'=>'student_email', 'controlType'=>'text'],
           ['attributeName'=>'date_of_birth', 'controlType'=>'date_picker'],
           ['attributeName'=>'guardian_name', 'controlType'=>'text'],
            ['attributeName'=>'guardian_phone', 'controlType'=>'text'],
            ['attributeName'=>'guardian_email', 'controlType'=>'text'],
            ['attributeName'=>'guardian_relation', 'controlType'=>'text'],
            ['attributeName'=>'day_boarding', 'controlType'=>'text'],
            ['attributeName'=>'disability', 'controlType'=>'text'],
            ['attributeName'=>'disability_nature', 'controlType'=>'text'],
            ['attributeName'=>'nationality', 'controlType'=>'text'],
            ['attributeName'=>'nin_no', 'controlType'=>'text'],
            ['attributeName'=>'allow_part_payments', 'controlType'=>'text'],

        ];

        return $config;
    }

    public function updateAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
//            ['attributeName'=>'other_names', 'controlType'=>'text'],
            ['attributeName'=>'gender', 'controlType'=>'gender_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'class_id', 'controlType'=>'class_id_picker', 'placeholder'=>'Choose class', 'prompt'=>'Choose class'],
            ['attributeName'=>'nationality', 'controlType'=>'text'],
            ['attributeName'=>'district', 'controlType'=>'text'],
            ['attributeName'=>'disability', 'controlType'=>'disability_picker'],
//            ['attributeName'=>'country_of_residence', 'controlType'=>'text'],
            ['attributeName'=>'student_phone', 'controlType'=>'text'],
            ['attributeName'=>'student_email', 'controlType'=>'text'],
            ['attributeName'=>'date_of_birth', 'controlType'=>'text'],
            ['attributeName'=>'guardian_name', 'controlType'=>'text'],
            ['attributeName'=>'student_phone', 'controlType'=>'text'],
            ['attributeName'=>'guardian_name', 'controlType'=>'text'],
            ['attributeName'=>'guardian_email', 'controlType'=>'text'],
            ['attributeName'=>'guardian_relation', 'controlType'=>'text'],
            ['attributeName'=>'day_boarding', 'controlType'=>'text'],
            ['attributeName'=>'disability', 'controlType'=>'text'],
            ['attributeName'=>'disability_nature', 'controlType'=>'text'],
            ['attributeName'=>'nationality', 'controlType'=>'text'],
            ['attributeName'=>'nin_no', 'controlType'=>'text'],
            ['attributeName'=>'allow_part_payments', 'controlType'=>'text'],
        ];

        return $config;
    }
    public function searchAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
            ['attributeName'=>'other_names', 'controlType'=>'text'],
        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'first_name',
            'last_name',
            'gender',
            'student_phone',
            'guardian_phone',
            'date_created',
            'nin_no',
            'parish','village','sub_county','county','region','district',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreStudent::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');


    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //Correct phone numbers
            $validatedGuardianPhone = ToWords::validatePhoneNumber($this->guardian_phone);
            if($validatedGuardianPhone) {
                $this->guardian_phone = $validatedGuardianPhone;
            }

            $validatedStudentPhone = ToWords::validatePhoneNumber($this->student_phone);
            if($validatedStudentPhone) {
                $this->student_phone = $validatedStudentPhone;
            }

            $validatedGuardianPhone2 = ToWords::validatePhoneNumber($this->guardian2_phone);
            if($validatedGuardianPhone2) {
                $this->guardian2_phone = $validatedGuardianPhone2;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsReceiveds()
    {
        return $this->hasMany(PaymentsReceived::className(), ['student_id' => 'id']);
    }

    public function whichDisability($attribute, $params)
    {
        if (($this->disability == 1) && empty($this->disability_nature)) {
            $this->addError($attribute, 'Please provide nature of disability');

        }
    }

    public function uniqueRegNo($attribute, $params)
    {
        $query = (new Query())->select(['school_student_registration_number', 'student_code'])->from('core_student')->where(['school_id' => $this->school_id, 'school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))])->limit(1)->one();
        $invalid = false;
        if(!$query) {
            $invalid = false;
        }
        else if ($this->isNewRecord) {
            $invalid = $query['school_student_registration_number'] ? true : false;
        } else {
            if ($query['school_student_registration_number'] && ($query['student_code'] != $this->student_code)) {
                $invalid = true;
            } else {
                $invalid = false;
            }
        }

        if ($invalid) {
            $this->addError($attribute, 'Student code belongs to another student');

        }
    }

    /**Validate date format for date of birth
     * @param $attribute
     * @param $paramsV
     */
    public function validateDateOfBirthFormat($attribute, $params)
    {
        if (!ToWords::validateDate($this->date_of_birth, 'Y-m-d')) {
            $this->addError($attribute, 'The date of birth is in an invalid format. Enter a valid date of birth in the format yyyy-mm-dd');
        }

        $now = date('Y-m-d');
        if ($now < $this->date_of_birth) {
            $this->addError($attribute, 'Date of birth cannot be in the future');
        }
    }

    /**
     * Will validate the entered registration number against the school's reg no format
     * @param $attribute
     * @param $params
     */
    public function validateRegNoAgainstSchoolFormat($attribute, $params)
    {
//        Yii::trace('Enter student regno validator');
        if (empty($this->school_student_registration_number)) {
            //Reg no not passed so just return;
            return;
        }

        $school_information = (new Query())->select(['school_registration_number_format', 'sample_school_registration_number'])
            ->from('school_information')->where(['id' => $this->school_id])->limit(1)->one();

        if (empty($school_information['school_registration_number_format'])) {
            //For schools without reg format, ban reserved formats
            $bankInterChannelCodes = Yii::$app->params['reservedRegistrationNumberFormats']; //An array of reserved regno formats
            foreach ($bankInterChannelCodes as $format) {
                $patternToUse = "/^" . $format . "$/";
                if (preg_match($patternToUse, $this->$attribute)) {
                    $this->addError($attribute, 'The entered registration number format is reserved for another school and is not acceptable');
                    return;
                }
            }

            //No format enforced so return
            return;
        }

        //First trim it
        $format = trim($school_information['school_registration_number_format']);
        $this->school_student_registration_number = trim(strtoupper($this->school_student_registration_number));

        $patternToUse = "/^" . $format . "$/";
        Yii::trace('Pattern to use to validate student regno is ' . $patternToUse);
        if (!preg_match($patternToUse, $this->$attribute)) {
            $this->addError($attribute, 'The entered registration number does not match the school format ' . $format . '. Enter format such as ' . $school_information['sample_school_registration_number']);
        }
    }

    public function validateGurdian($attribute, $params)
    {
        if ($this->guardian_phone && !$this->gurdian_name) {
            $this->addError($attribute, 'Provide Guardian Name for ' . $this->guardian_phone);
        }
    }

    public function validateGurdian2($attribute, $params)
    {
        if ($this->guardian2_phone && !$this->guardian2_name) {
            $this->addError($attribute, 'Provide Guardian Name for ' . $this->guardian2_phone);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentClass()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getLastArchivedClass()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_when_archived']);
    }

    public function getSchoolId()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }


    public function getSchool()

    {
        return $this->studentClass->institution0->id;
    }

    public function getImage()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'passport_photo']);
    }

    public function getChannelImages($id)
    {
        return $this->hasOne(ImageBank::className(), ['id' => $id]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAccount()
    {
        return $this->hasOne(StudentAccountGl::className(), ['id' => 'student_account_id']);
    }

    public function getAccountHistories()
    {
        $query = StudentAccountHistory::find()->where(['account_id' => $this->student_account_id, 'reversal_flag' => false, 'reversed' => false])->andWhere('payment_id is not null')->orderBy('date_created DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }


    public function getGrades($average, $subjectId)
    {
        $classId = Yii::$app->db->createCommand('SELECT class_id FROM core_subject WHERE id=:subjectId')
            ->bindValue(':subjectId', $subjectId)
            ->queryOne();
        Yii::trace($classId);

        $grade = Yii::$app->db->createCommand('SELECT grades FROM core_grades WHERE (min_mark <= :average and max_mark >=:average )and class_id =:class_id')
            ->bindValue(':average', $average)
            ->bindValue(':class_id', $classId['class_id'])
            ->queryOne();
        Yii::trace($grade);
        $grade =$grade['grades'];
        return $grade;
    }

    public function getSchLogo()
    {
        return $this->studentClass->institution0->school_logo;
    }

    public function getPayable()
    {

        //For BiB, payable should include future fees
//        $payable = (new \yii\db\Query())
//            ->select(['fd.id', 'fd.effective_date', 'fd.description AS desc', 'fd.due_amount AS fee', 'fd.effective_date AS start_date',
//                'fd.end_date AS end_date', 'fs.fee_outstanding_balance', 'fs.penalties_outstanding_balance', 'fs.applied'])
//            ->from('institution_fee_student_association fs')
//            ->join('join', 'institution_fees_due fd', 'fd.id=fs.fee_id')
//            ->where('fs.student_id=' . $this->id . ' AND fd.end_date >= NOW() and fd.recurrent=false')
//            ->orderBy('fd.priority')
//            ->all();
//        return $payable;

        $payable = (new \yii\db\Query())
            ->select(['fd.id', 'fd.effective_date', 'fd.description AS desc', 'fd.due_amount AS fee', 'fd.effective_date AS start_date',
                'fd.end_date AS end_date', 'fs.fee_outstanding_balance', 'fs.penalties_outstanding_balance', 'fs.applied', 'fd.recurrent', 'fd.child_of_recurrent'])
            ->from('institution_fee_student_association fs')
            ->join('join', 'institution_fees_due fd', 'fd.id=fs.fee_id')
            ->where('fs.student_id=' . $this->id . ' AND fd.end_date >= NOW() and fd.recurrent=false')
            ->orderBy('fd.priority')
            ->all();
        return $payable;
    }

    public function getExempts()
    {
        $exempts = (new \yii\db\Query())
            ->select(['fd.description AS desc', 'fd.due_amount AS fee'])
            ->from('fees_due_student_exemption exmp')
            ->join('join', 'institution_fees_due fd', 'fd.id=exmp.fee_id')
            ->where('exmp.student_number=' . $this->student_code)
            ->all();
        return $exempts;
    }

    public function getPayableSum()
    {
        $sumPayable = (new \yii\db\Query())
            ->from('institution_fee_student_association fs')
            ->join('join', 'institution_fees_due fd', 'fd.id=fs.fee_id')
            ->where('fs.student_id=' . $this->id . 'AND fs.applied =true AND fd.end_date >= NOW() AND fd.effective_date <= NOW()')
            ->sum('fd.due_amount');
        return $sumPayable;
    }

    public function getBal()
    {
        $bal = (new \yii\db\Query())
            ->select(['account_balance AS bal'])
            ->from('student_account_gl')
            ->where(['id' => $this->student_account_id])
            ->limit(1)->one();
        return $bal['bal'];
    }

    public function getLogo()
    {
        $logo = ImageBank::find()->where(['id' => $this->schLogo])->limit(1)->one();
        return $logo->image_base64;
    }

    public function getCancelledPaymentSchedule()
    {
        return (new PaymentPlanSearch())->searchCancelledPlanSchedule(['PaymentPlanSearch'=>['student_id'=>$this->id]]);
    }
    public function getPaymentSchedule()
    {
        return (new PaymentPlanSearch())->searchPlanSchedule(['PaymentPlanSearch'=>['student_id'=>$this->id]]);
    }
    public function getStudentAgreements()
    {
        return (new PaymentPlanSearch())->searchStudentPlans(['PaymentPlanSearch'=>['student_id'=>$this->id]]);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validateStdPassword($password)
    {
        return Yii::$app->security->validatePassword($password,$this->password);
    }

    public static function findByUsername($username)
    {
       return static::find()->where(['student_code' => $username,'archived' => false, 'locked'=>false])->one();
   }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'archived' => false, 'locked'=>false]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'locked' => false,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public static function getSubCatList($cat_id){
        $subCategories= County::find()
            ->select(['id','county_name'])
            ->where(['district_id'=>$cat_id])
            ->asArray()
            ->all();
        return $subCategories;
    }

    public static function getChannelLogos($id){
        $logo = ImageBank::find()->where(['id' => $id])->limit(1)->one();
        return $logo->image_base64;
    }

    public function getComments($average, $subjectId)
    {
        $classId = Yii::$app->db->createCommand('SELECT class_id FROM core_subject WHERE id=:subjectId')
            ->bindValue(':subjectId', $subjectId)
            ->queryOne();
        Yii::trace($classId);

        $comments = Yii::$app->db->createCommand('SELECT comment FROM core_grades WHERE (min_mark <= :average and max_mark >=:average )and class_id =:class_id')
            ->bindValue(':average', $average)
            ->bindValue(':class_id', $classId['class_id'])
            ->queryOne();
        Yii::trace($comments);
        return $comments['comment'];
    }

    public function getYearsList() {
        $currentYear = date('Y');
        $yearFrom = 1990;
        $yearsRange = range($yearFrom, $currentYear);
        return array_combine($yearsRange, $yearsRange);
    }


    public function onePhoneNumber($attribute, $params)
    {
        if (!$this->guardian_phone && !$this->student_phone) {
            $this->addError($attribute, 'Enter atleast one Phone Number (Guardian or Student)');
        }
    }

    public function validePhone($attribute, $params)
    {
        if (!preg_match('/^(0|251)\d{9}$/i', $this->$attribute)) {
            $this->addError($attribute, 'Please Enter a valid Phone Number');
        }
    }

    public function validatenames($attribute, $params)
    {
        if (!preg_match('/^[\u1200-\u137F]+ [a-zA-Z]+[a-zA-Z]$/i', $this->$attribute)) {
            $this->addError($attribute, 'Should not contain spaces, numbers or special characters');
        }
    }

    public function nameWithSpaces($attribute, $params)
    {
        if (!preg_match('/^[\p{L} ]+$/i', $this->$attribute)) {
            $this->addError($attribute, 'Should not Contain numbers or special characters');
        }
    }
}


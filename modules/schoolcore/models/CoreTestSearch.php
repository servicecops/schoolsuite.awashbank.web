<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * CoreTeacherSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreTeacher`.
 */
class CoreTestSearch extends CoreTest
{
    public $test, $answer, $schsearch;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_id', 'subject_id', 'class_id', 'term_id'], 'integer'],
            [['date_created', 'date_modified', 'test_description', 'total_marks', 'start_date','ens_date', 'subject_teacher_id',], 'safe'],

        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'Test',
            'title' => 'Test info Datasheet',
            'exportFile' => '@app/modules/schoolcore/views/core-test/exportPdfExcel',

        ];

        return $data;
    }
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreTest::find();
        $searchForm = 'test_description';
        // add conditions that should always apply here
        $columns = [

            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return $model->subjectName->subject_name;
                },
            ],
            'test_description',
            'total_marks',
            'start_date',
            'end_date',
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Term Desc',
                'value' => function ($model) {
                    return $model->termName->term_description;
                },
            ],

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Subject Teacher',
                'value' => function ($model) {
                    return $model->teacherName->first_name;
                },
            ],

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->username;
                },
            ],
            'date_created',
            'date_modified',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-eye"></i>', ['core-test/view', 'id' => $model['id']]);
                },
            ],
        ];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,
            'class_id' => $this->class_id,
            'total_marks' => $this->total_marks,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'subject_id' => $this->subject_id,
            'subject_teacher_id' => $this->subject_teacher_id,
        ]);
        $query->andFilterWhere(['ilike', 'test_description', $this->test_description]);

        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params)
    {
        $searchForm = 'test_description';
        $query = (new Query())
            ->select(['ct.id', 'sb.subject_name', 'ct.test_description', 'ct.start_date','ct.end_date', 'ctm.term_description', 'csc.school_name', 'cl.class_description', 'sb.subject_name', 'ct.total_marks', 'ct.date_created',])
            ->from('core_test ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_term ctm', 'ct.term_id= ctm.id')
            ->where(['ct.class_id'=>$params]);
        $query->orderBy('ct.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['distinct("tc"."class_id")'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_class_id = [];
            foreach ($data as $k => $v) {
                array_push($the_class_id, $v['class_id']);
            }
            Yii::trace($the_class_id);

            $query->andwhere(['in', 'cl.id', $the_class_id]);
        }



        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_name',

            [
                'label' => 'Test Name',
                'value'=>function($model){
                    return Html::a($model['test_description'], ['core-test/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'start_date',
            'end_date',
            'term_description',
            'school_name',
            'class_description',
            'first_name',
            'total_marks',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-eye"></i>', ['core-test/view', 'id' => $model['id']]);
                },
            ],


            ////
        ];
        $title = 'All Tests';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm,'title'=>$title];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchView($params)
    {
        $searchForm = 'test_description';
        $query = (new Query())
            ->select(['ct.id', 'sb.subject_name', 'ct.test_description', 'ct.start_date','ct.end_date', 'ctm.term_description', 'csc.school_name', 'cl.class_description', 'sb.subject_name', 'tc.first_name', 'ct.total_marks', 'ct.date_created',])
            ->from('core_test ct')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_term ctm', 'ct.term_id= ctm.id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $attributes = [
            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return $model->subjectName->subject_name;
                },
            ],
            [
                'label' => 'Test Name',
                'value'=>function($model){
                    return Html::a($model['test_description'], ['core-test/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'total_marks',
            'start_date',
            'end_date',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            'date_created',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            ////
        ];
        if (($models = CoreTest::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function searchSubject($params)
    {

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);

        $query->orderBy('cs.id desc');

  if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

        $trQuery = new Query();
        $trQuery->select(['tc.subject_id'])
            ->from('teacher_class_subject_association tc')
            ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
            ->innerJoin('user uz', 'cs.id=uz.school_user_id')
            ->Where(['uz.id' => Yii::$app->user->identity->getId()])
            ->andWhere(['tc.class_id' => $params]);


        $commandt = $trQuery->createCommand();
        $data = $commandt->queryAll();


        $the_subjectId = [];
        foreach ($data as $k => $v) {
            array_push($the_subjectId, $v['subject_id']);
        }
        Yii::trace($the_subjectId);

        $query->andwhere(['in', 'cs.id', $the_subjectId]);
    }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',
           'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Create Test', ['core-test/create-test', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];


    }

    public function searchSubjectView($params)
    {

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);

        $query->orderBy('cs.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('View Test', ['core-test/the-index', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];


    }


    public function generateStudentList($params)
    {

        $query = (new Query())
            ->select(['st.student_code', 'st.id', 'st.first_name', 'st.last_name'])
            ->from('core_student st')
            ->innerJoin('core_school_class csc','csc.id =st.class_id' )
            ->innerJoin('core_school  sc','sc.id =st.school_id' )
            ->where(['st.class_id'=> $params]);


        $query->orderBy('st.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sc.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sc.id' => $this->schsearch]);
            }
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
//        if (\app\components\ToWords::isSchoolUser() ) {
//            if ($this->class_id && $this->class_id != 0) {
//                $query->andWhere(['st.class_id' => intVal($this->class_id)]);
//            } else {
//                $query->andWhere(['csc.id' => Yii::$app->user->identity->class_id]);
//            }
//        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
//            if (!empty($this->class_id)) {
//                $query->andWhere(['st.class_id' => intVal($this->class_id)]);
//            } elseif ($this->schsearch) {
//                $query->andWhere(['csc.school_id' => $this->schsearch]);
//            }
//        }
        $columns = [
            'student_code',
            'id',
            'first_name',
            'last_name',
            'marks_obtained'
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' );

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['core-test/display-subject', 'classId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-test/display-subject', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }

    public function searchClassView(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' );

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['core-test/the-index', 'classId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-test/the-index', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }
}

<?php

namespace app\modules\schoolcore\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreUser;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreStudentSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreStudent`.
 */
class CoreUserSearch extends CoreUser
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'incorrect_access_count'], 'integer'],
            [['date_created',  ], 'safe'],
            [['locked', ], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'date_created' => $this->date_created,
//            'school_id' => $this->school_id,
//          //  'locked' => $this->locked,
//            'system_user_id' => $this->system_user_id,
//            'user_level' => $this->user_level,
//           // 'incorrect_access_count' => $this->incorrect_access_count,
//            'username' => $this->username,
//          //  'password' => $this->password,
//            'date_updated' => $this->date_updated,
//        ]);

//        $query->andFilterWhere(['ilike', 'school_id', $this->school_id])
//            ->andFilterWhere(['ilike', 'locked', $this->locked])
//            ->andFilterWhere(['ilike', 'system_user_id', $this->system_user_id])
//            ->andFilterWhere(['ilike', 'username', $this->username]);
        return $dataProvider;
    }
    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params)
    {

        $query = (new Query())
            ->select(['cs.id','cs.username', 'csc.school_name','cs.user_level'])
            ->from('core_user cs')
            ->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'username',
            'school_name',
            'user_level',
            [

                'label' => 'Name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fas fa-eye"></i>', ['core-user/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'username';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
}

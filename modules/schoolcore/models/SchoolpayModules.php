<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "schoolpay_modules".
 *
 * @property integer $id
 * @property string $module_code
 * @property string $module_name
 */
class SchoolpayModules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suite_modules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module_code', 'module_name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_code' => 'Module Code',
            'module_name' => 'Module Name',
        ];
    }
}

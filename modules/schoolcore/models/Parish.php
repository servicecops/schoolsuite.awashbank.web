<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "parish".
 *
 * @property int $id
 * @property string|null $parish_name
 * @property int|null $sub_county_id
 * @property string|null $created_on
 * @property int|null $created_by
 * @property string|null $updated_on
 * @property int|null $updated_by
 */
class Parish extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parish';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'sub_county_id', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['id', 'sub_county_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['parish_name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parish_name' => 'Parish Name',
            'sub_county_id' => 'Sub County ID',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
}

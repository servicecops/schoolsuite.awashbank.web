<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "school_section_class_association".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $school_id
 * @property integer $class_id
 * @property integer $section_id
 */
class SchoolSectionClassAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_section_class_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['school_id', 'class_id', 'section_id'], 'integer'],
            [['class_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'school_id' => 'School ID',
            'class_id' => 'Class ID',
            'section_id' => 'Section ID',
        ];
    }
}

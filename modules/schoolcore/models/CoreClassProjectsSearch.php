<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreClassProjects;
use yii\db\Query;

/**
 * CoreClassProjectsSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreClassProjects`.
 */
class CoreClassProjectsSearch extends CoreClassProjects
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'subject_id', 'term_id', 'class_id', 'school_id', 'created_by'], 'integer'],
            [['project_name', 'description', 'project_category', 'date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = new Query();
        $query->select(['csp.*', 'ct.term_name', 'cs.school_name', 'cbs.subject_name', 'csc.class_description'])
            ->from('core_class_projects csp')
            ->innerJoin('core_term ct', 'csp.term_id= ct.id')
            ->innerJoin('core_subject cbs', 'csp.subject_id= cbs.id')
            ->innerJoin('core_school_class csc', 'csp.class_id= csc.id')
            ->innerJoin('core_school cs', 'csp.school_id= cs.id');
        $query->orderBy('csp.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->Where(['csp.school_id' => Yii::$app->user->identity->school_id]);
        } else if (\app\components\ToWords::isSchoolUser()) {
            $query->Where(['csp.school_id' => Yii::$app->user->identity->school_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'term_id' => $this->term_id,
            'class_id' => $this->class_id,
            'school_id' => $this->school_id,
            'created_by' => $this->created_by,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
        ]);

        $query->andFilterWhere(['ilike', 'project_name', $this->project_name])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'project_category', $this->project_category]);

        return $dataProvider;
    }
}

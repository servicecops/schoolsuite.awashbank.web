<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\Book;

/**
 * BookSearch represents the model behind the search form of `app\modules\schoolcore\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'category', 'shelf_number'], 'integer'],
            [['date_created', 'date_updated', 'book_title', 'ISBN_number', 'author', 'publisher'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'category' => $this->category,
            'shelf_number' => $this->shelf_number,
        ]);

        $query->andFilterWhere(['ilike', 'book_title', $this->book_title])
            ->andFilterWhere(['ilike', 'ISBN_number', $this->ISBN_number])
            ->andFilterWhere(['ilike', 'author', $this->author])
            ->andFilterWhere(['ilike', 'publisher', $this->publisher]);

        $query->andWhere(['school_id' =>Yii::$app->user->identity->school_id]);


        return $dataProvider;
    }
}

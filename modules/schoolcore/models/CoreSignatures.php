<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "core_signatures".
 *
 * @property string $name
 * @property string $position
 * @property string $file_attachment
 * @property string $contact
 * @property string $email
 * @property string|null $date_created
 * @property int|null $school_id
 * @property int $id
 * @property string|null $date_updated
 * @property int|null $created_by
 */
class CoreSignatures extends \yii\db\ActiveRecord
{
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_signatures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'contact', 'email'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['image'], 'file', 'extensions' => 'jpg, jpeg, png'],
            [['image'], 'file', 'maxSize' => '100000'],
            [['school_id', 'created_by'], 'default', 'value' => null],
            [['school_id', 'created_by'], 'integer'],
            [['name', 'position', 'contact', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'position' => 'Position',
            'contact' => 'Contact',
            'email' => 'Email',
            'date_created' => 'Date Created',
            'school_id' => 'School ID',
            'id' => 'ID',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'file_attachment' => 'Attach Signature Image',
            'image' => 'Attach scanned Signature/ Signature Image'
        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
}

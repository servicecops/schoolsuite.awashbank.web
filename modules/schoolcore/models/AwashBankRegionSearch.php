<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\BookCategory;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * BookCategorySearch represents the model behind the search form of `app\modules\schoolcore\models\BookCategory`.
 */
class AwashBankRegionSearch extends AwashBankRegion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['bank_region_name','date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function search($params)
    {


        $this->load($params);
        //Trim the model searches
        $this->bank_region_name = trim($this->bank_region_name);

        $query = new Query();
        $query ->select(['ab.id', 'ab.bank_region_name'])
            ->from('awash_bank_region ab');



        if (!(empty($this->bank_region_name))) {
            Yii::trace($this->bank_region_name);

             //Registration numbers usually have a dash - or slass /
                $query->andWhere(['ilike','bank_region_name', trim(strtoupper($this->bank_region_name))]);


        }




        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'bank_region_name',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ab.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

}

<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "core_control_student_guardian_relationships".
 *
 * @property int $id
 * @property string $code
 * @property string $description
 */
class CoreControlStudentGuardianRelationships extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_control_student_guardian_relationships';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'description'], 'required'],
            [['code', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'description' => 'Description',
        ];
    }
}

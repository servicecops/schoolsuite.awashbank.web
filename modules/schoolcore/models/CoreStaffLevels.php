<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *

 * @property string $level_description
 * @property string $code

 */
class CoreStaffLevels extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_staff_levels';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code','level_description',], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'level_description' => 'Description',

        ];
    }

    public function formAttributeConfig() {
        $config = [

            'id' => 'ID',
            'code' => 'Code',
            'level_description' => 'Description',
        ];

        return $config;
    }
    public function updateAttributeConfig() {
        $config = [

            ['attributeName'=>'code', 'controlType'=>'text'],
            ['attributeName'=>'level_description', 'controlType'=>'text'],

        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }
    public function getTeacherName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'subject_teacher_id']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [


            'term_name',
            'term_description',
            'term_starts',
            'term_ends',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = CoreTerm::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    public function findSchoolTeachers(){

        $query = (new Query())
            ->select(['id','term_name','term_description','term_ends', 'term_starts',  'date_created', 'date_modified' ])
            ->from('core_term ');
           // ->innerJoin('core_school csc', 'ct.school_id= csc.id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}

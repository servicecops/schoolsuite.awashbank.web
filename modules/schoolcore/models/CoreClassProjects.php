<?php

namespace app\modules\schoolcore\models;

use app\models\User;
use Yii;

/**
 * This is the model class for table "core_class_projects".
 *
 * @property int $id
 * @property int|null $subject_id
 * @property int|null $term_id
 * @property int|null $class_id
 * @property int|null $school_id
 * @property string|null $project_name
 * @property string|null $description
 * @property int|null $project_category
 * @property int $created_by
 * @property string|null $date_created
 * @property string|null $date_modified
 */
class CoreClassProjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_class_projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_id', 'term_id', 'class_id', 'school_id', 'project_category', 'created_by'], 'default', 'value' => null],
            [['subject_id', 'term_id', 'class_id', 'school_id', 'project_category', 'created_by'], 'integer'],
            [['description'], 'string'],
            [['project_name', 'description', 'subject_id', 'term_id', 'class_id', 'school_id', 'project_category', 'created_by'], 'required'],
            [['date_created', 'date_modified'], 'safe'],
            [['project_name',], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Subject ID',
            'term_id' => 'Term ID',
            'class_id' => 'Class Id',
            'school_id' => 'School ID',
            'project_name' => 'Project Name',
            'description' => 'project Description',
            'project_category' => 'Project Assessment Category',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }

    public function getProjectCategory()
    {
        return $this->hasOne(ClassProjectCategories::className(), ['id' => 'project_category']);
    }


    public function getTermName()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }

}

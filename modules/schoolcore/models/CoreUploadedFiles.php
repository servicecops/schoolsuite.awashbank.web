<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use Yii;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table "submission_uploads".
 *
 * @property string $id
 * @property string $date_created
 * @property string $test_id
 * @property string $file_name
 * @property string $file_data
 */
class CoreUploadedFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uploaded_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['test_id', 'file_name', 'file_data'], 'required'],
            [['test_id','student_class','school_id'], 'integer'],
            [['file_data','description'], 'string'],
            [['file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'test_id' => 'Test Id',
            'file_name' => 'File Name',
            'file_data' => 'File Data',
        ];
    }
}

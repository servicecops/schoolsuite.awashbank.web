<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "book_copy".
 *
 * @property int $id
 * @property string|null $date_created
 * @property string|null $date_updated
 * @property int|null $created_by
 * @property string|null $ISBN_number
 * * @property string|null $ASN_No
 * @property int|null $book_id
 * @property boolean $available
 * @property int|null $student_id
 * @property string|null $issue_date
 * @property string|null $return_date
 */
class BookCopy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book_copy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated','student_id'], 'safe'],
            [['issue_date', 'return_date','student_id'], 'required', 'on'=>'issue_book'],
            [['date_returned'], 'required', 'on'=>'return_book'],
            [['created_by', 'book_id'], 'default', 'value' => null],
            [['created_by', 'book_id','student_id'], 'integer'],
            [['available'], 'boolean'],
            [['ISBN_number', 'ASN_No'], 'string', 'max' => 255],
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['issue_book'] = ['issue_date', 'return_date','student_id'];
        $scenarios['return_book'] = ['issue_date', 'return_date','student_id','date_returned'];

        return $scenarios;
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'ISBN_number' => 'Isbn Number',
            'ASN_No' => 'ASN No',
            'book_id' => 'Book ID',
            'student_id' => 'Student Name',
        ];
    }

    public function getStudent()
    {
        return CoreStudent::find()->where(['id' => $this->student_id]);
    }
    public function getBookName()
    {
        return Book::find()->where(['id' => $this->book_id]);
    }
}

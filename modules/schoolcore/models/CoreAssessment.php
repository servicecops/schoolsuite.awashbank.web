<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *
 * @property string $id
 * @property string $description
 * @property string $assessment_type
 * @property string $created_by
 * @property string $first_name
 * @property string $last_name
 * @property string $other_names
 * @property string $date_of_birth
 * @property string $gender
 * @property string $class_id
 * @property string $school_id
 * @property string $phone_contact_1
 * @property string $phone_contact_2
 * @property string $email_address
 * @property string $student_code
 * @property bool $active
 * @property bool $archived
 * @property string $date_archived
 * @property string $passport_photo
 * @property string $account_id
 * @property string $subject_code
 * @property string $subject_name
 */
class CoreAssessment extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_assessment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assessment_type', 'description',  ''], 'safe'],

            [['assessment_type','description'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'assessment_type' => 'Assessment Type',


        ];
    }

    public function formAttributeConfig() {
        $config = [

            ['attributeName'=>'description', 'controlType'=>'text'],
            ['attributeName'=>'assessment_type', 'controlType'=>'text'],

        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'subject_name',
            'subject_code',

            'date_created',

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSubject::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreSignatures;
use yii\db\Query;

/**
 * CoreSignaturesSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreSignatures`.
 */
class CoreSignaturesSearch extends CoreSignatures
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'file_attachment', 'contact', 'email', 'date_created', 'date_updated'], 'safe'],
            [['school_id', 'id', 'created_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {


        $query = new Query();
        $query->select(['cs.*', 'csc.school_name'])
            ->from('core_signatures cs')
            ->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cs.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->Where(['cs.school_id' => Yii::$app->user->identity->school_id]);
        } else if (\app\components\ToWords::isSchoolUser()) {
            $query->Where(['cs.school_id' => Yii::$app->user->identity->school_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'school_id' => $this->school_id,
            'id' => $this->id,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'position', $this->position])
            ->andFilterWhere(['ilike', 'file_attachment', $this->file_attachment])
            ->andFilterWhere(['ilike', 'contact', $this->contact])
            ->andFilterWhere(['ilike', 'email', $this->email]);

        return $dataProvider;
    }
}

<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *

 * @property string $term_name
 * @property string $term_starts
 * @property string $id
 * @property string $school_id
 * @property string $term_ends
 * @property string $marks_obtained
 * @property string $created_by
 * @property string $date_created
 * @property string $student_answer
 * @property string $student_id
 * @property string $test_id
 * @property string $class_id
 * @property string $question_id
 * @property string $answers
 * @property string $is_correct_answer
 * @property string $correct_answers
 * @property string $is_marked
 */
class OnlineStudentAnswers extends BaseModel
{
    public $test, $testing;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online_test_answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_modified','is_correct_answer'], 'safe'],
            [[ 'test_id', 'question_id'], 'integer'],
            [[ 'test_id', 'question_id', 'student_id','student_answer'], 'required'],
            [['student_answer' ], 'string', 'max' => 255],
            [['is_correct_answer','is_marked' ], 'boolean'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'question' => 'Question',
            'question_type' => 'Question TYpe',
            'test_id' => 'Test',
            'school_id' => 'School Name',
            'created_by' => 'Created By',
            'date_modified' => 'Date Modified',
        ];
    }

    public function formAttributeConfig()
    {
        $config = [

            ['attributeName' => 'term_name', 'controlType' => 'text'],
            ['attributeName' => 'term_description', 'controlType' => 'text'],
            ['attributeName' => 'term_starts', 'controlType' => 'date_picker'],
            ['attributeName' => 'term_ends', 'controlType' => 'date_picker'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_only', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],

        ];

        return $config;
    }

    public function updateAttributeConfig()
    {
        $config = [

            ['attributeName' => 'term_name', 'controlType' => 'text'],
            ['attributeName' => 'term_description', 'controlType' => 'text'],
            ['attributeName' => 'term_starts', 'controlType' => 'date_picker'],
            ['attributeName' => 'term_ends', 'controlType' => 'date_picker'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_picker', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],

        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }

    public function getTeacherName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'subject_teacher_id']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [


            'term_name',
            'term_description',
            'term_starts',
            'term_ends',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = CoreTerm::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    //view online test

    public function viewOnlineTest($id)
    {

        $searchForm = 'test_description';
        $query = (new Query())
            ->select(['ct.test_description', 'csc.school_name', 'cs.subject_name','ot.created_by','ot.date_created', 'cc.class_description', 'ot.answer_options', 'ot.question', 'ot.question_type', 'ot.correct_answer'])
            ->from('online_test ot')
            ->innerJoin('core_subject cs', 'ot.subject_id= cs.id')
            ->innerJoin('core_school csc', 'ot.school_id= csc.id')
            ->innerJoin('core_school_class cc', 'ot.class_id= cc.id')
            ->innerJoin('core_test ct', 'ot.test_id= ct.id');
        $query->orderBy('ct.id desc')
         ->where(['ot.test_id'=>$id]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $columns = [


            'test_id',
            'answer_options',
            'question',
            'question_type',
            'subject_name',
            'class_description',
            'school_name',
            'date_created',
            'created_by'

        ];

//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);

        return ['data' => $data, 'columns' => $columns];

    }


}

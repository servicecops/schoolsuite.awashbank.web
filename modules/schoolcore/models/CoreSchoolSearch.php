<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreSchoolSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreSchool`.
 */
class CoreSchoolSearch extends CoreSchool
{
    /**
     * {@inheritdoc}
     */
    public $date_to,$date_from,$schoolpay_schoolcode;
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_type'], 'integer'],
            [['date_created', 'date_updated', 'school_name', 'location', 'account_number', 'schoolpay_schoolcode','phone_contact_1','bank_name', 'phone_contact_2','date_from','date_to', 'contact_email_1', 'contact_email_2', 'school_code'], 'safe'],
        ];
    }


    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['school_code', 'school name', 'District', 'Contact Phone', 'school type'],
            'fileName' => 'School',
            'title' => 'School info Datasheet',
            'exportFile' => '/core-school/exportPdfExcel',
        ];

        return $data;
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
//    public function search($params)
//    {
////    {
//        Yii::trace($params);
//        $searchForm = 'school_name';
//        $searchModel = new CoreSchoolSearch();
////        // add conditions that should always apply here
//        $columns = [
//            'school_code',
//            'school_name',
//            'description',
//            'location',
//
//
//            [
//                'label' => '',
//                'value'=>function($model){
//                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-school/view', 'id' => $model['id']], ['class'=>'aclink']);
//                },
//                'format'=>'html',
//            ],
//            [
//
//                'label' => '',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    return Html::a('<i class="fas fa-edit"></i>', ['core-school/update', 'id' => $model['id']]);
//                },
//            ],
////            ['class' => 'kartik\grid\ActionColumn',]
//            ////
//        ];
//
//        $this->load($params);
//        $query = new Query();
//        $query->select(['sch.id', 'sch.school_code', 'sch.school_name', 'sch.district', 'typ.description'])
//            ->from('core_school sch')
//            ->innerJoin('core_control_school_types typ', 'typ.id=sch.school_type')
//            ->andFilterWhere(['school_code' => $this->school_code])
//            ->andFilterWhere(['bank_name' => $this->bank_name])
//            ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
//
//        $pages = new Pagination(['totalCount' => $query->count()]);
//        $sort = new Sort([
//            'attributes' => ['school_code', 'school_name', 'physical_address', 'school_type_name'],
//        ]);
//        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('sch.school_code');
//        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;
//        $query->offset($pages->offset)->limit($pages->limit);
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//
//        //return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
//
//    }
//    public function search($params)
//    {
//        $query = CoreSchool::find();
//
//        // add conditions that should always apply here
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        // grid filtering conditions
//        $query->andFilterWhere([
//            'location' => $this->location,
//        ]);
//
//        $query->andFilterWhere(['ilike', 'school_code', $this->school_code])
//            ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
//
//        return $dataProvider;
//    }

    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['sch.id', 'sch.school_code','sch.external_school_code', 'sch.school_name', 'dt.district_name','sch.phone_contact_1','typ.description as school_type'])
            ->from('core_school sch')
            ->innerJoin('core_control_school_types typ', 'typ.id=sch.school_type')
            ->innerJoin('district dt', 'dt.id=sch.district')
            ->andFilterWhere(['ilike', 'external_school_code',$this->school_code])
            ->andFilterWhere(['ilike', 'school_name', $this->school_name])
            ->andFilterWhere(['bank_name'=>$this->bank_name]);


//        if(Yii::$app->user->can('view_by_region')){
//            //If school user, limit and find by id and school id
//            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);
//
//        }

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function searchSync($params)
    {
        $this->load($params);

        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();
        $query->select(['sch.id', 'sch.school_code','sch.external_school_code', 'sch.school_name','sch.date_created','sch.schoolpay_schoolcode', 'dt.district_name','sch.phone_contact_1','typ.description as school_type'])
            ->from('core_school sch')
            ->innerJoin('core_control_school_types typ', 'typ.id=sch.school_type')
            ->innerJoin('district dt', 'dt.id=sch.district')
            ->where(['sch.is_sync'=>true])
            ->andFilterWhere(['external_school_code'=>$this->school_code])
            ->andFilterWhere(['schoolpay_schoolcode'=>$this->schoolpay_schoolcode])
            ->andFilterWhere(['ilike', 'school_name', $this->school_name])
            ->andFilterWhere(['bank_name'=>$this->bank_name]);

            if($this->date_from){
                $query  ->andFilterWhere(['between', 'sch.date_created', $this->date_from, $date_to]);


            }
        ;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }


    public function searchReport($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['br.branch_name','rg.description', 'count(sch.id) as schools'])
            ->from('core_school sch')
            ->innerJoin('awash_branches br', 'br.id=sch.branch_id')
            ->innerJoin('ref_region rg', 'rg.id=sch.region')
        ;

        $query->groupBy(['br.branch_name','rg.description'])
            ->orderBy('br.branch_name');

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }


}

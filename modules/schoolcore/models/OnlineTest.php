<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *

 * @property string $term_name
 * @property string $term_starts
 * @property string $id
 * @property string $school_id
 * @property string $term_ends
 * @property string $date_modified
 * @property string $created_by
 * @property string $date_created
 * @property string $term_description
 * @property string $question_type
 * @property string $question
 * @property string $auto_assessment
 * @property string $answer_options
 * @property string $correct_answer
 * @property string $is_marked
 */
class OnlineTest extends BaseModel
{
    public $test, $testing, $schsearch;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online_test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_modified','auto_assessment'], 'safe'],
            [['created_by', 'school_id',], 'default', 'value' => null],
            [['created_by', 'test_id', 'question_type'], 'integer'],

            [['school_id', 'test_id', 'class_id', 'subject_id'], 'required'],
            [['question', 'answer_options', 'correct_answer', 'question_type', 'testing'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'question' => 'Question',
            'question_type' => 'Question TYpe',
            'test_id' => 'Test',
            'school_id' => 'School Name',
            'created_by' => 'Created By',
            'date_modified' => 'Date Modified',
        ];
    }

    public function formAttributeConfig()
    {
        $config = [

            ['attributeName' => 'question', 'controlType' => 'text'],
            ['attributeName' => 'question_type', 'controlType' => 'text'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_only', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],

        ];

        return $config;
    }

    public function updateAttributeConfig()
    {
        $config = [

            ['attributeName' => 'question', 'controlType' => 'text'],
            ['attributeName' => 'question_type', 'controlType' => 'text'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_only', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],

        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }

    public function getTeacherName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'subject_teacher_id']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [


            'term_name',
            'term_description',
            'term_starts',
            'term_ends',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = CoreTerm::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    //view online test

    public function viewOnlineTest($id)
    {

        $searchForm = 'test_description';
        $query = (new Query())
            ->select(['ct.test_description', 'csc.school_name', 'cs.subject_name','ot.created_by','ot.date_created', 'cc.class_description', 'ot.answer_options', 'ot.question', 'ot.question_type', 'ot.correct_answer', 'ot.auto_assessment','ot.id'])
            ->from('online_test ot')
            ->innerJoin('core_subject cs', 'ot.subject_id= cs.id')
            ->innerJoin('core_school csc', 'ot.school_id= csc.id')
            ->innerJoin('core_school_class cc', 'ot.class_id= cc.id')
            ->innerJoin('core_test ct', 'ot.test_id= ct.id');
        $query->orderBy('ct.id desc')
         ->where(['ot.test_id'=>$id]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $columns = [
            'id',
            'test_id',
            'answer_options',
            'question',
            'question_type',
            'subject_name',
            'class_description',
            'school_name',
            'date_created',
            'created_by',
            'auto_assessment'

        ];

//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);

        return ['data' => $data, 'columns' => $columns];

    }

    public function studentSubmissions($testId)
    {
        $query = (new Query());
        $query->select('cs.student_code, ct.test_description, ct.id, ca.student_id')
            ->from('core_test ct ')
            ->innerJoin('online_test_answers ca','ca.test_id =ct.id' )
            ->innerJoin('core_student cs','cs.id = ca.student_id' )
            ->innerJoin('online_test ot','ot.id =ca.question_id' )

            ->where(['ct.id'=>$testId])
            ->andWhere(['ot.auto_assessment'=>false])
            ->andWhere(['ca.is_marked'=>false]);
        $query->orderBy('ct.id asc');
        $query->groupBy('ct.id,cs.student_code, ct.test_description, ct.id, ca.student_id');


        if (\app\components\ToWords::isSchoolUser() ) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ct.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['ct.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['ct.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        $command = $query->createCommand();
        $data = $command->queryAll();


        $searchModel = new OnlineTestSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];

    }
    public function getQtnAnswerDetails($testId, $student_id)
    {
        $query = (new Query());
        $query->select('ot.question, oa.student_answer,oa.id,oa.question_id')
            ->from('online_test ot ')
            ->innerJoin('online_test_answers oa','oa.question_id =ot.id' )


            ->where(['ot.test_id'=>$testId])
            ->andWhere(['ot.auto_assessment'=>false])
            ->andWhere(['oa.student_id'=>$student_id])
            ->andWhere(['oa.is_marked'=>false]);
        $query->orderBy('ot.id desc');

        $command = $query->createCommand();
        $data = $command->queryAll();


        $searchModel = new OnlineTestSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];

    }

}

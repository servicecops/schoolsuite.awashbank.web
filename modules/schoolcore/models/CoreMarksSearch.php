<?php

namespace app\modules\schoolcore\models;

use kartik\editable\Editable;
use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreMarks;
use yii\db\Query;
use yii\helpers\Html;
use yii\bootstrap\Modal;
/**
 * CoreTeacherSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreTeacher`.
 */
class CoreMarksSearch extends CoreMarks
{
    public $studentName,$test,$answer,$schsearch,$class_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'marks_obtained', 'test_id','student_id' ], 'integer'],
            [[ 'test_id', 'student_id', 'marks_obtained', 'studentName' ], 'safe'],

        ];

    }

    public static function getExportQuery()
    {
        $file = '@app/modules/schoolcore/views/core-marks/exportPdfExcel';



        if($_SESSION['SCORE_MODE']=='marks') {
            $file = '@app/modules/schoolcore/views/core-marks/exportMarksPdfExcel';

        }
        if(isset($_SESSION['SCORE_MODE']) && $_SESSION['SCORE_MODE']=='scores') {
            $file = '@app/modules/schoolcore/views/core-marks/exportScoresPdfExcel';

        }

        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'Marks',
            'title' => 'Marks info Datasheet',
            'exportFile' => $file,
        ];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreMarks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'student_id' => $this->student_id,
            'test_id' => $this->test_id,
            'marks_obtained'=>$this->marks_obtained
        ]);


        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array ActiveDataProvider
     */
    public function searchStudentInfo($params)
    {
        $query = CoreStudent::find();

        $columns = [
            'first_name',
            'last_name',
            'class_description',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    // return Html::a("...", ['core-subject/view', 'id' => $model['id']]);
                    return Html::textInput('marks_obtained', '', ['class' => 'form-control']);
                },
            ],

        ];
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return ['columns' => $columns, 'dataProvider' => $dataProvider,];
        }

        // grid filtering conditions
        $query->andFilterWhere(['ilike', 'first_name', $this->first_name])
            ->andFilterWhere(['ilike', 'last_name', $this->last_name])
            ->andFilterWhere(['ilike', 'other_names', $this->other_names]);


        return ['columns' => $columns, 'dataProvider' => $dataProvider,];
    }


    /*
    * Return particular fields for dataprovider
    */
    public function searchStudent($params)
    {

        $query = (new Query())
            ->select(['cs.id','cs.first_name', 'cs.last_name','ct.class_description' ])
            ->from('core_student cs')
            ->innerJoin('core_school_class ct', 'cs.class_id= ct.id');
        //->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cs.id desc')
            ->where(['cs.class_id'=>$params]);
        $query->andFilterWhere([
        'id' => $this->id,
        'created_by' => $this->created_by,
        'student_id' => $this->student_id,
        'test_id' => $this->test_id,
        'marks_obtained'=>$this->marks_obtained
    ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'first_name',
            'last_name',
            'class_description',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    // return Html::a("...", ['core-subject/view', 'id' => $model['id']]);
                    return Html::textInput('marks_obtained', '', ['class' => 'form-control']);
                },
            ],

        ];

        return ['columns' => $columns, 'dataProvider' => $dataProvider];

    }


    /*
  * Return particular fields for dataprovider
  */
    public function searchIndex($params)
    {

        $searchForm= 'description';
        $query = (new Query())
            ->select(['ct.id', 'tc.first_name','ct.description', 'cm.marks_obtained',])
            ->from('core_marks cm')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_student cs', 'ct.subject_teacher_id= tc.id')
            ->innerJoin('core_test ct', 'ct.id= cm.test_id');
        $query->orderBy('ct.id desc');

//        if (\app\components\ToWords::isSchoolUser() ) {
//            if ($this->class_id && $this->class_id != 0) {
//                $query->andWhere(['cm.class_id' => intVal($this->class_id)]);
//            } else {
//                $query->andWhere(['cm.school_id' => Yii::$app->user->identity->school_id]);
//            }
//        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {
//            if (!empty($this->class_id)) {
//                $query->andWhere(['cm.class_id' => intVal($this->class_id)]);
//            } elseif ($this->schsearch) {
//                $query->andWhere(['csc.id' => $this->schsearch]);
//            }
//        }
        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->innerJoin('teacher_class_subject_association tca', 'cl.id=tca.class_id');

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['tca.class_id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'tca.class_id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_name',
            [
                'label' => 'Test Description',
                'value'=>function($model){
                    return Html::a($model['description'], ['core-marks/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'start_date',
            'end_date',
            'school_name',
            'class_description',
            'subject_name',
            'first_name',
            'total_marks',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fas fa-eye"></i>', ['core-marks/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }

    /*
* Return all student marks per test id
*/
    public function studentTestMarksObtained($id)
    {
        $searchForm= 'marks_obtained';
        $query = (new Query())
            ->select(['cm.id','cs.student_code','cs.first_name', 'cs.last_name','cs.middle_name','cc.class_description','cm.marks_obtained', ])
            ->from('core_marks cm')
            ->innerJoin('core_student cs', 'cm.student_id =cs.id')
            ->innerJoin('core_school_class cc', 'cc.id= cs.class_id')
            ->innerJoin('core_test ct', 'ct.id = cm.test_id')
            ->where(['cm.test_id'=>$id]);

//        if($studentSearch) {
//            $query->andFilterWhere(['ilike', 'cs.first_name', $studentSearch->studentName]);
//        }
        //->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cm.id desc');

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //$model= new CoreMarks();
        $columns = [

            'student_code',
            'first_name',
            'last_name',
            'other_name',
            'class_description',
            'marks_obtained',


            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fas fa-pen"></i>', ['core-marks/edit', 'id' => $model['id']]);
                },
            ],


        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }

    public function searchMarksIndex(){
        $searchForm= 'description';
        $query = (new Query())
            ->select(['ct.id','ct.description', 'tc.first_name','ct.test_description', 'csc.school_name','cl.class_description'])
            ->from('core_marks cm')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_teacher tc', 'ct.subject_teacher_id= tc.id')
            ->innerJoin('core_test ct', 'ct.id= cm.test_id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'first_name',
            [
                'label' => 'Test Description',
                'value'=>function($model){
                    return Html::a($model['description'], ['core-marks/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            'class_description',
            'subject_name',
            'description',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-marks/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }
    public function displayAllTest(){
        $searchForm= 'description';
        $query = (new Query())
            ->select(['ct.id','sb.subject_name','ct.test_description', 'ct.start_date','ct.end_date','ctm.term_description', 'csc.school_name','cl.class_description' ,'sb.subject_name', 'ct.total_marks','ct.date_created',])
            ->from('core_test ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_term ctm', 'ct.term_id= ctm.id')
            ->innerJoin('core_marks mks', 'ct.id= mks.test_id');

        $query->orderBy('ct.id desc');
        //$query->having('count(mks.test_id)>0');
        $query->groupBy(['ct.id','sb.subject_name','ct.test_description', 'ct.start_date','end_date','ctm.term_description', 'csc.school_name','cl.class_description' ,'sb.subject_name', 'ct.total_marks','ct.date_created']);
        //  ->where(['si.active'=>true, 'si.archived'=>false]);


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cl.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cl.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['csc.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }



        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_name',
            [
                'label' => 'Test Description',
                'value'=>function($model){
                    return Html::a($model['test_description'], ['core-marks/score', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'start_date',
            'end_date',
            'term_description',
            'school_name',
            'class_description',
            'total_marks',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-marks/score', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
}

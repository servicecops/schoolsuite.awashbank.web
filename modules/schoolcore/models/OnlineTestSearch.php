<?php

namespace app\modules\schoolcore\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\schoolcore\models\CoreTest;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * CoreTeacherSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreTeacher`.
 */
class OnlineTestSearch extends OnlineTest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_id', 'subject_id','class_id' ,'term_id'], 'integer'],
            [['date_created', 'date_modified', 'test_description', 'total_marks', 'start_date','end_date', 'question',], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoreTest::find();
        $searchForm= 'test_description';
        // add conditions that should always apply here
        $columns = [

            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return $model->subjectName->subject_name;
                },
            ],
            'test_description',
            'total_marks',
            'start_date',
            'end_date',
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            [
                'label' => 'Term Desc',
                'value' => function ($model) {
                    return $model->termName->term_description;
                },
            ],

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Subject Teacher',
                'value' => function ($model) {
                    return $model->teacherName->first_name;
                },
            ],

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-test/view', 'id' => $model['id']]);
                },
            ],
        ];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,
            'class_id' => $this->class_id,
            'total_marks' => $this->total_marks,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'subject_id' => $this->subject_id,
            'subject_teacher_id' => $this->subject_teacher_id,
        ]);
        $query->andFilterWhere(['ilike', 'test_description', $this->test_description]);

        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params)
    {
        $searchForm= 'test_description';
        $query = (new Query())
            ->select(['ct.id','sb.subject_name','ct.test_description', 'ct.start_date','end_date','ctm.term_description', 'csc.school_name','cl.class_description' ,'sb.subject_name', 'ct.total_marks','ct.date_created',])
            ->from('core_test ct')
            ->innerJoin('core_school csc', 'ct.school_id= csc.id')
            ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
            ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
            ->innerJoin('core_term ctm', 'ct.term_id= ctm.id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_name',
            'test_description',
            'start_date',
            'end_date',
            'term_description',
            'school_name',
            'class_description',
            'first_name',
            'total_marks',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-test/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchView($params)
    {
        $searchForm= 'test_description';
        $query = (new Query())
        ->select(['ct.id','sb.subject_name','ct.test_description', 'ct.start_date','end_date','ctm.term_description', 'csc.school_name','cl.class_description' ,'sb.subject_name','tc.first_name', 'ct.total_marks','ct.date_created',])
        ->from('core_test ct')

        ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
        ->innerJoin('core_school csc', 'ct.school_id= csc.id')
        ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
        ->innerJoin('core_term ctm', 'ct.term_id= ctm.id');
        $query->orderBy('ct.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $attributes = [
            [
                'label' => 'Subject Name',
                'value' => function ($model) {
                    return $model->subjectName->subject_name;
                },
            ],
            'test_description',
            'total_marks',
            'start_date',
            'end_date',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            'date_created',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            ////
        ];
        if (($models = CoreTest::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewOnlineTestModel($params)
    {
        $attributes = [

            'question',

            'class_d',
            'school_id',

        ];

        if (($models = OnlineTest::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function getStudentResult($studentId, $testId)


    {
        $query = (new Query())
            ->select(['ot.answer_options', 'ot.question', 'ot.correct_answer','oa.student_answer','oa.is_correct_answer'])
            ->from('online_test ot')
            ->innerJoin('online_test_answers oa', 'oa.question_id= ot.id');

        $query->orderBy('ot.id desc')
            ->where(['ot.test_id'=>$testId])
            ->andWhere(['oa.student_id'=>$studentId]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        $columns = [

            'answer_options',
            'question',
            'correct_answer',
            'student_answer',
            'is_correct',

        ];

        return ['data' => $data, 'columns' => $columns];
    }
}

<?php

namespace app\modules\schoolcore\models;

use app\models\BaseModel;
use app\models\User;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $subject_outline
 * @property string $outline_attachment;
 * @property string $subject_description
 * @property string $class_id
 * @property string $school_id
 * @property string $teacher_id
 * @property string $subject_code
 * @property string $subject_name
 * @property string $subject_compulsory
 * @property string $uneb_consideration
 * @property string $is_co_curricular_activity
 *
 */
class CoreSubject extends BaseModel
{
    public $attachment;

    /**.
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_subject';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'date_archived', 'attachment'], 'safe'],
            [['created_by', 'class_id', 'school_id',], 'default', 'value' => null],
            [['created_by', 'class_id', 'school_id',], 'integer'],
            [['subject_code', 'subject_name','class_id'], 'required'],
            [['subject_compulsory', 'uneb_consideration', 'is_co_curricular_activity'], 'boolean'],
            [['subject_code', 'subject_name', 'subject_outline', 'outline_attachment', 'subject_description'], 'string', 'max' => 255],
           // [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx, xls, xlsx, mp3,mp4', 'maxFiles' => 10],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'subject_code' => 'Subject Code',
            'subject_name' => 'Subject Name',
            'school_id' => 'School Name',
            'subject_compulsory' => 'Is subject compulsory',
            'uneb_consideration' => 'Is Subject Considered for new UNEB Grading',
            'is_co_curricular_activity' => 'Consider as a co-curricular activity ',
            'attachment' => 'Attach subject outline(optional)'

        ];
    }

    public function formAttributeConfig()
    {
        $config = [

            ['attributeName' => 'subject_code', 'controlType' => 'text'],
            ['attributeName' => 'subject_name', 'controlType' => 'text'],
            ['attributeName' => 'school_id', 'controlType' => 'school_id_picker_subject', 'placeholder' => 'Choose school name', 'prompt' => 'Choose school name'],

        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getTeachersName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'teacher_id']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'subject_name',
            'subject_code',

            'date_created',

            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname . ' ' . $model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSubject::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

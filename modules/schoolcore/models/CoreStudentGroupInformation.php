<?php

namespace app\modules\schoolcore\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "student_group_information".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $group_name
 * @property string $group_description
 * @property string $date_created
 * @property string $date_modified
 * @property boolean $active
 */
class CoreStudentGroupInformation extends \yii\db\ActiveRecord
{
    public $classes;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_group_information';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'group_name'], 'required'],
            [['school_id'], 'integer'],
            [['date_created', 'date_modified', 'classes'], 'safe'],
            [['active'], 'boolean'],
            [['group_name', 'group_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School',
            'group_name' => 'Group Name',
            'group_description' => 'Group Description',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
            'active' => 'Active',
        ];
    }

    public function getSchool(){
        return CoreSchool::find()->where(['id'=>$this->school_id])->one();
    }

}

<?php

namespace app\modules\schoolcore\models;

use app\modules\schoolcore\models\CoreUploadedFiles;
use app\models\BaseModel;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *
 * @property string $description
 * @property string $subject_id
 * @property string $id
 * @property string $school_id
 * @property string $class_id
 * @property string $student_id
 * @property string $test_id
 * @property string $created_by
 * @property string $marks_obtained
 * @property string $date_created
 * @property string $total_marks
 * @property string $project_id
 */
class CoreMarks extends BaseModel
{

    /**
     * @var UploadedFile file attribute
     */
    public $uploads;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_marks';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['test_id', 'project_id', 'student_id', 'marks_obtained'], 'integer'],
            [['student_id', 'marks_obtained'], 'required'],
            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx, xls, xlsx', 'maxFiles' => 1],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'marks_obtained' => 'Marks Obtained',
            'test_id' => 'Exam name',
            'created_by' => 'Created By',
            'student_id' => 'Student Name',


        ];
    }

    public function formAttributeConfig()
    {
        $config = [

            ['attributeName' => 'marks_obtained', 'controlType' => 'text'],
            ['attributeName' => 'student_id', 'controlType' => 'text'],

        ];

        return $config;
    }

    public function editAttributeConfig()
    {
        $config = [


            ['attributeName' => 'marks_obtained', 'controlType' => 'text'],

        ];

        return $config;
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'created_by']);
    }

    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }

    public function getTeacherName()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'subject_teacher_id']);
    }

    public function getStudentName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [

            [
                'label' => 'Student Name',
                'value' => function ($model) {
                    return $model->studentName->first_name . ' ' . $model->last_name;
                },
            ],
            'marks_obtained',

        ];

        if (($models = CoreMarks::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    /*
     * edit student marks
     */

    /**
     * @param $params
     * @return array
     * Return attributes and model for editting
     */

    public function editStudentMarks($params)
    {
        $attributes = [
            [
                'label' => 'Student Name',
                'value' => function ($model) {
                    return $model->studentName->first_name . ' ' . $model->last_name;
                },
            ],
            [
                'label' => 'Marks Obtained',
                'value' => function ($model) {
                    return $model->marks_obtained;
                },
            ],


        ];

        if (($models = CoreMarks::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }


    /*
     * Add student marks per subject
     */
    public function addMarks($id, $studentSearch = null)
    {

        $query = (new Query())
            ->select(['cs.id', 'cs.student_code', 'cs.first_name', 'cs.last_name', 'cs.middle_name', 'ct.class_description'])
            ->from('core_student cs')
            ->innerJoin('core_school_class ct', 'cs.class_id= ct.id')
            ->innerJoin('core_test te', 'te.class_id= ct.id')
            ->where(['te.id' => $id]);

        if ($studentSearch) {
            $query->andFilterWhere(['ilike', 'cs.first_name', $studentSearch->studentName]);
        }
        //->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cs.id desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $columns = [
            'student_code',
            'first_name',
            'last_name',
            'other_names',
            'class_description',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    // return Html::a("...", ['core-subject/view', 'id' => $model['id']]);
                    $rowId = $model['id'];
                    return Html::textInput("marks_obtained[$rowId]", '', ['class' => 'formprocess']);
                    //$form->field($model, 'cards_taken_by', ['inputOptions'=>[ 'class'=>'form-control'] ])->textInput();
                },
            ],

        ];

        return ['columns' => $columns, 'dataProvider' => $dataProvider];

    }

    public function saveUploads($id)
    {
        if (is_array($this->uploads) && $this->uploads) {
            foreach ($this->uploads as $file) {
                $fname = $file->baseName . '.' . $file->extension;
                $tmpfile_contents = file_get_contents($file->tempName);
                $upload = new CoreUploadedFiles();
                $upload->test_id = $id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);
            }
        }
    }

    public function getStoredFiles()
    {
        $files = (new Query())->select(['id', 'file_name'])->from('uploaded_files');
        //->where(['test_id'=>$id]);
        return $files->all();
    }

    public function StudentMarks($id)
    {

        $searchForm = 'marks_obtained';
        $query = (new Query())
            ->select(['cm.id', 'cs.student_code', 'ct.test_description', 'cc.class_description', 'cm.marks_obtained'])
            ->from('core_marks cm')
            ->innerJoin('core_student cs', 'cm.student_id =cs.id')
            ->innerJoin('core_school_class cc', 'cc.id= cs.class_id')
            ->innerJoin('core_test ct', 'ct.id = cm.test_id')
            ->where(['cs.id' => $id]);

//        if($studentSearch) {
//            $query->andFilterWhere(['ilike', 'cs.first_name', $studentSearch->studentName]);
//        }
        //->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cm.id desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //$model= new CoreMarks();
        $columns = [

            'student_code',
            'test_description',
            'class_description',
            'marks_obtained'


        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }


    public function searchStudentsAssessments($id)
    {

        $query = (new Query())
            ->select(['cpa.marks_obtained', 'cs.id', 'cs.student_code', 'cs.first_name', 'cs.last_name', 'cs.middle_name'])
            ->from('core_marks cpa')
            ->innerJoin('core_student cs', 'cpa.student_id= cs.id')
            ->where(['cpa.project_id' => $id]);

//        if ($studentSearch) {
//            $query->andFilterWhere(['ilike', 'cs.first_name', $studentSearch->studentName]);
//        }
        //->innerJoin('core_school csc', 'cs.school_id= csc.id');
        $query->orderBy('cs.id desc');

        /** @var TYPE_NAME $dataProvider */
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;

    }

    public function searchProjectMarks($id, $params)
    {
        $query = (new Query())
            ->select(['cpa.marks_obtained', 'cpa.id', 'cs.student_code', 'cs.first_name', 'cs.last_name', 'cs.middle_name'])
            ->from('core_marks cpa')
            ->innerJoin('core_class_projects ccp', 'cpa.project_id= ccp.id')
            ->innerJoin('core_student cs', 'cpa.student_id= cs.id')
            ->where(['cpa.project_id' => $id]);
        $query->orderBy('cpa.id desc');




        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->Where(['ccp.school_id' => Yii::$app->user->identity->school_id]);
        } else if (\app\components\ToWords::isSchoolUser()) {
            $query->Where(['ccp.school_id' => Yii::$app->user->identity->school_id]);
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        Yii::trace($dataProvider);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student_id' => $this->student_id,
            'project_id' => $this->project_id,
            'marks_obtained' => $this->marks_obtained,
            'created_by' => $this->created_by,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
        ]);

        return $dataProvider;
    }

    public function getProjectName()
    {
        return $this->hasOne(CoreMarks::className(), ['id' => 'project_id']);
    }


}

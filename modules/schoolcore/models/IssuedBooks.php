<?php

namespace app\modules\schoolcore\models;

use Yii;

/**
 * This is the model class for table "issued_books".
 *
 * @property int $id
 * @property string|null $date_issued
 * @property string|null $date_updated
 * @property string|null $due_date
 * @property int|null $issued_by
 * @property string|null $book_title
 * @property int $category
 * @property string|null $ISBN_number
 * @property string|null $author
 * @property string|null $publisher
 * @property int $shelf_number
 */
class IssuedBooks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'issued_books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_issued', 'date_updated', 'due_date'], 'safe'],
            [['issued_by', 'category', 'shelf_number'], 'default', 'value' => null],
            [['issued_by', 'category', 'shelf_number'], 'integer'],
            [['category', 'shelf_number'], 'required'],
            [['book_title', 'ISBN_number', 'author', 'publisher'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_issued' => 'Date Issued',
            'date_updated' => 'Date Updated',
            'due_date' => 'Due Date',
            'issued_by' => 'Issued By',
            'book_title' => 'Book Title',
            'category' => 'Category',
            'ISBN_number' => 'Isbn Number',
            'author' => 'Author',
            'publisher' => 'Publisher',
            'shelf_number' => 'Shelf Number',
        ];
    }
}

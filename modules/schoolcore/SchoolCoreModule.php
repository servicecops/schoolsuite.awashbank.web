<?php

namespace app\modules\schoolcore;

/**
 * schoolcore module definition class
 */
class SchoolCoreModule extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\schoolcore\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

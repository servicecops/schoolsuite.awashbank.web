<?php
use kartik\form\ActiveForm;
use yii\helpers\Html;

?>

<div class="container">
    <?php

    $form = ActiveForm::begin(['method'=>'Post','action' => ['create-online-test'], 'options' => ['class' => 'formprocess', 'style' => 'width:100%']]); ?>
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Select Report Criteria</h3></div>
    </div>

    <div>
        <p>Please grade type</p>
        <input type="radio" name="[CoreTest][grade_type]" value="cumulative">
        <label for="cumulative">Cumulative</label><br>
        <input type="radio"  name="[CoreTest][grade_type]" value="average">
        <label for="average">Average</label><br>
    </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Select</th>
                <th>Subject Code</th>
                <th>Subject Name</th>
                <th>Test Description</th>
                <th>Class</th>
                <th>% Contribution</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach($dataProvider as $k =>$v){
            ?>
            <tr>
                <td><input type="checkbox" name="[CoreTest][selected_test][<?= $v['id']; ?>]" value =<?= $v['id']?> /></td>
                <td><?php  echo $v['subject_code']  ?></td>
                <td><?php  echo $v['subject_name']  ?></td>
                <td><?php  echo $v['test_description']  ?></td>
                <td><?php  echo $v['class_description']  ?></td>
                <td><input name="[CoreTest][contribution]"  type="text"/></td>
            </tr>
                <?php
            }

            ?>
            </tbody>

        </table>

    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">

                <?= Html::submitButton( 'Save' , ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>
</div>


<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\BankDetails */

$this->title = $model->bank_name;
?>

<div class="container" style="background-color: #ffffff">
<div class="row">
<div class="col-md-12">
    <div class="letter no-padding">
      <div class="row" style="padding-top:100px;">
                <?php if($model->bank_logo): ?>
               <img class="sch-icon" src="<?= Url::to(['image-link', 'id'=>$model->bank_logo]) ?>" height="100" width="100" />
                <?php endif; ?><br>
         <h2>  <span class="col"> <?= Html::encode($this->title) ?></span></h2>

           <div class="col">
            <div class="top-buttons float-right">
                <?= Html::a('<i class="fa fa-edit"></i> Edit Logo', ['logo', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-primary']) ?>
                <?= Html::a('<i class="fa fa-edit"></i> Edit Details', ['update', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-primary']) ?>
            </div>
            </div>
    </div>
    <div class="letter">
    <hr class="l_header">
    
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
            //'id',
            'bank_name',
            'bank_address',
            'contact_email:email',
            'contact_phone',
            'date_created',
        ],
    ]) ?>
    </div>
    </div>
</div>
</div>
</div>



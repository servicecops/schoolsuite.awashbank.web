<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreBankDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col">

    <?php $form = ActiveForm::begin(['options'=>['class'=>'formprocess']]); ?>

    <?= $form->field($model, 'bank_name',['labelOptions'=>['style'=>'color:#000']])->textInput()->label('Bank Name *') ?>
    <?= $form->field($model, 'bank_code',['labelOptions'=>['style'=>'color:#000'], 'inputOptions' => ['class' => 'form-control star','disabled'=>!$model->isNewRecord]])->textInput()->label('Bank Code *')  ?>

    <?= $form->field($model, 'bank_address',['labelOptions'=>['style'=>'color:#000']])->textInput() ?>

    <?= $form->field($model, 'contact_email',['labelOptions'=>['style'=>'color:#000']])->textInput()->label('Bank Name *')  ?>

    <?= $form->field($model, 'contact_phone',['labelOptions'=>['style'=>'color:#000']])->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add Bank' : 'Edit Bank', ['class' => $model->isNewRecord ? 'btn bg-gradient-success' : 'btn btn-lg btn-block btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

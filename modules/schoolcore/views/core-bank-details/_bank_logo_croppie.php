<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\bootstrap4\ActiveForm */
\app\assets\CroppieAsset::register($this);
?>
<div style="color:#F7F7F7">Update Logo</div>
<div class="letter">
    <?php $form = ActiveForm::begin(['id'=>'bank_logo', 'options'=>['enctype'=>'multipart/form-data']]); ?>

    <div class="row">
        <div class="col-md-6 no-padding">
            <div id="upload-demo" ></div>
            <input type="hidden" id="imagebase64" name="ImageBank[image_base64]">
        </div>
        <div class="col-md-6 text-center" style="padding-top:10px;">
            <div>
                <h3 style="padding:0px;line-height:30px; color: #000"><?= $model->bank_name; ?><h3>
                        <hr class="style14">
                        <div id="crop_result" >
                            <?php if($model->bank_logo) : ?>
                                <img class="img-thumbnail" src="<?= Url::to(['image-link', 'id'=>$model->bank_logo]) ?>" height="250" width="230" />'

                            <?php endif; ?>
                        </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="btn btn-primary btn-file">
                Upload<input type="file" id="upload" style="display:none;" value="Choose a file"/>
            </label>
            <span class="btn btn-info upload-result">Crop</span>
            <?= Html::submitButton('Save Logo', ['class' => 'btn btn-info save_cropped_photo', 'style'=>'display:none;']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>




<script type="text/javascript">
    $( document ).ready(function() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    });
                    $('.upload-demo').addClass('ready');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 200,
                height: 220,
            },
            boundary: {
                width: 400,
                height: 400
            }
        });
        $('.cr-viewport').html("<div class='text-center' style='color:gray;padding:75px 10px;font-size:22px;'>Click Upload Button Below</div>");

        $('#upload').on('change', function () {  $('.cr-viewport').html(''); readFile(this); });
        $('.upload-result').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport',
                format:'jpeg'
            }).then(function (resp) {
                $('#imagebase64').val(resp);
                $('#crop_result').html('<img class="img-thumbnail" src="'+resp+'" height="210" width="200" />');
                $('.save_cropped_photo').css('display', 'inline');
                return false;
            });
        });

    });
</script>

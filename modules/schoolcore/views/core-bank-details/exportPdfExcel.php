<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3>  Bank Info Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Code</th><th>Name</th><th>Address</th><th>Email</th><th>Phone number</th></tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['bank_code']) ? $sinfo['bank_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['bank_name']) ? $sinfo['bank_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['bank_address']) ? $sinfo['bank_address'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['contact_phone']) ? $sinfo['contact_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['contact_email']) ? $sinfo['contact_email'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

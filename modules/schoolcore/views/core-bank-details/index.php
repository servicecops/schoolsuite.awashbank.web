<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\banks\models\BankDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banks';
?>




<!--new-->
<div class="core-school-index">

    <h5><?= Html::encode($this->title) ?></h5>


    <?php


    $columns = [
        ['class' => 'kartik\grid\SerialColumn'],
        'bank_code',
        'bank_name',
        'bank_address',
        'contact_email:email',
        'contact_phone',

        ['class' => 'kartik\grid\ActionColumn'],
    ];
    ?>
<div class="row">

        <?php if (Yii::$app->user->can('rw_bank')) : ?>
            <?= Html::a('Add bank', ['create'], ['class' => 'btn bg-gradient-success text-white float-right']) ?>
        <?php endif; ?>

</div>
    <div class="float-right">
        <?php
        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['core-school/export-to-pdf', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-danger',
            'target'=>'_blank',
            'data-toggle'=>'tooltip',
            'title'=>'Will open the generated PDF file in a new window'
        ]);
        echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-success',
            'target'=>'_blank'
        ]);
        ?>
    </div>


    <?= GridView::widget([
        'dataProvider'=> $dataProvider,
        //    'filterModel' => $searchModel,
        'columns' => $columns,
        'resizableColumns'=>true,
        //    'floatHeader'=>true,
        'responsive'=>true,
        'responsiveWrap' => false,
        'bordered' => false,
        'striped' => true,
    ]); ?>


</div>

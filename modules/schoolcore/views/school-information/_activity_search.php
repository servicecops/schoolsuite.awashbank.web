<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\SchoolInformation;
use kartik\select2\Select2;
use app\modules\banks\models\BankAccountDetails;
use yii\jui\DatePicker;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsReceivedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/schoolcore/school-information/schoolactivity'],
        'method' => 'post',
        'options'=>['id'=>'bank_statement_form'],
    ]); ?>



    <div class="col-md-2 no-padding">
        <?= $form->field($model, 'from_date')->widget(DatePicker::className(),
            [
                'model' => $model,
                'attribute' => 'from_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'From Date'],
                'clientOptions' =>[
                    'class'=>'form-control',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=>'1900:'.(date('Y')+1),
                    'autoSize'=>true,
                ],
            ])->label(false) ?>
    </div>

    <div class="col-md-2 no-padding">
        <?= $form->field($model, 'to_date')->widget(DatePicker::className(),
            [
                'model' => $model,
                'attribute' => 'to_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'To Date'],
                'clientOptions' =>[
                    'class'=>'form-control',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=>'1900:'.(date('Y')+1),
                    'autoSize'=>true,
                ],
            ])->label(false) ?>
    </div>
    <div class="col-sm-3">

        <?php
        $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
        echo $form->field($model, 'branch_region')
            ->dropDownList(
                $items,           // Flat array ('id'=>'label')
                ['prompt' => 'Select Branch Region',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/branches']) . '?id="+$(this).val(), function( data ) {
                            $( "select#branches" ).html(data);
                        });']    // options
            )->label(false); ?>

    </div>
    <div class="col-sm-3">

        <?php
        $items = ArrayHelper::map(\app\modules\schoolcore\models\AwashBranches::find()->where(['branch_region'=>$model->branch_region])->all(), 'id', 'branch_name');
        echo $form->field($model, 'branch_id')->dropDownList($items
            , ['id' => 'branches',
                'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])->label(false)
        ?>

    </div>


    <div class="col-md-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
    <div class="col-md-12 " id="has-error-date_overflow"></div>
    <?php ActiveForm::end(); ?>

</div>

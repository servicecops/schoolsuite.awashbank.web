<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = 'Add School';
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-information-create hd-title" data-title="Add School">
  <div class="letter">
  <div class=" col-xs-12 no-padding">
    <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
  </div>
    <?= $this->render('_form', [
        'model' => $model, 'sp_modules'=>$sp_modules, 'sch_modules'=>[], 'banks'=>$banks, 'bank_info_error'=>$bank_info_error
    ]) ?>

</div>
</div>

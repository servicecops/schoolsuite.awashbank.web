<?php


use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Sections';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index hd-title" data-title="School Sections">
<div class="row">
    <div class="col-xs-12">
      <div class="col-sm-8 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
      <div class="col-sm-4 col-xs-12" >
          <?php echo $this->render('_section_search', ['model' => $searchModel]); ?>
      </div>
    </div>

    <div class="col-xs-12">
       <div class="box-body table table-responsive no-padding">
        <table class="table table-striped">
        <thead>
        <tr>
            <th class='clink'><?= $sort->link('section_code') ?></th>
            <th class='clink'><?= $sort->link('section_name') ?></th>
            <th class='clink'><?= $sort->link('school_name') ?></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            if($dataProvider) :
            foreach($dataProvider as $k=>$v) : ?>
                <tr data-key="0">
                    <td class="clink"><?= ($v['section_code']) ? '<a href="'.Url::to(['/school-information/vwsection', 'id'=>$v['id']]).'">'.$v['section_code'].'</a>' : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['section_name']) ? $v['section_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
            <?php endforeach; 
            else :?>
            <tr><td colspan="8">No sections found</td></tr>
        <?php endif; ?>
        </tbody>
        </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]);?>

    </div>
</div>
<?php

use kartik\helpers\Html;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

?>

<div class="row student-index hd-title" data-title="Bank Statement">


    <div class="col-md-12">
        <div class="col-md-3 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> Reports</span>
        </div>



        <div class="col-md-12 col-xs-12 no-padding">
            <ul class="menu-list pull-right">
                <?php
                echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-sm btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-sm btn-success',
                    'target' => '_blank'
                ]);
                ?>

        </div>
        <?php $form = ActiveForm::begin([
            'action' => ['/schoolcore/school-information/student-users'],
            'method' => 'get',
            'options' => ['class' => 'formprocess'],
        ]); ?>
        <ul style="list-style-type:none" class="row">
            <li class="col-md-2 col-sm-1 col-xs-1"><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]) ?></li>
            <li class="col-md-2 col-sm-1 col-xs-1"><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_to',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></li>
            <div class="col-sm-3">

                <?php
                $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
                echo $form->field($searchModel, 'branch_region')
                    ->dropDownList(
                        $items,           // Flat array ('id'=>'label')
                        ['prompt' => 'Select Branch Region',
                            'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/branches']) . '?id="+$(this).val(), function( data ) {
                            $( "select#branches" ).html(data);
                        });']    // options
                    )->label(false); ?>

            </div>
            <div class="col-sm-3">

                <?= $form->field($searchModel, 'branch_id')->dropDownList([],

                    ['id' => 'branches',
                    ])->label(false)
                ?>

            </div>

            <li class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></li>
        </ul>
        <?php ActiveForm::end(); ?>
    </div>



<div class="box-body table table-responsive no-padding">
    <h3>Student Users</h3>
    <table class="table table-striped">
        <thead>
        <?php
        $x=1;
        ?>
        <tr>
            <th></th>

            <th class='clink'>Bank Region Name</th>
            <th class='clink'>Branch Name</th>
            <th class='clink'>Country Region</th>
            <th class='clink'>School Name</th>
            <th class='clink'>Total Student Users</th>
            <th class='clink'>Active Student Users</th>
            <th class='clink'>Inactive Student Users</th>
            <th class='clink'>Number of Student</th>
        </tr>
        </thead>
        <tbody>
        <?php

        if ($dataProvider) :

            foreach ($dataProvider as $data) : ?>
                <tr data-key="0">
                    <td><?php echo $x++?></td>


                    <td>
                        <?= ($data['bank_region_name']) ? $data['bank_region_name'] : '<span class="not-set"> 0 </span>' ?>
                    </td>
                    <td>
                        <?= ($data['branch_name']) ? $data['branch_name'] : '<span class="not-set"> 0 </span>' ?>
                    </td>
                    <td>
                        <?= ($data['description']) ? $data['description'] : '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['school_name']) ? $data['school_name'] : '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['ttusers']) ? $data['ttusers'] : '<span class="not-set"> 0  </span>' ?>
                    </td>

                    <td>
                        <?= ($data['actuser']) ? $data['actuser'] : '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['inactive']) ? $data['inactive']: '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['stds']) ? $data['stds'] : '<span class="not-set"> 0  </span>' ?>
                    </td>



                </tr>
            <?php endforeach;
        else :?>
            <tr>
                <td colspan="12">No data found </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
    <?= LinkPager::widget([
        'pagination' => $pages['pages'],
    ]); ?>
</div>
</div>

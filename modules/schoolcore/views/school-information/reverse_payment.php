<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/schoolcore/school-information/reverse-payment', 'id'=>$model->id],
    'method' => 'post',
    'options'=>['class'=>'formprocess', 'id'=>'reverse_school_payment']
]); ?>

    <div class="col-md-12">
        <?= $form->field($model2, 'reason', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Give Reason'] ])->textInput()->label('') ?>
    </div>
    <div class="form-group col-md-12 no-padding" style="margin-top:20px;">
        <div class="col-md-6">
            <?= Html::submitButton('Reverse', ['class' => 'btn btn-block btn-info', 'data-confirm'=>'Are you sure you want reverse this transaction']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
        $('form#reverse_school_payment').yiiActiveForm('validate');
          $("form#reverse_school_payment").on("afterValidate", function (event, messages) {
              if($(this).find('.has-error').length) {
                        return false;
                } else {
                    $('.modal').modal('hide');
                }
            });
   });
JS;
$this->registerJs($script);
?>
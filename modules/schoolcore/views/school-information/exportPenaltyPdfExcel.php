<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="student-information">
    <?php  
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Penalty Report Information</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
        <th>Date</th>
        <th>Details</th>
        <th>Code</th>
        <th>Reg No.</th>
        <th>Student</th>
        <th>Class</th>
        <th>Penalty amount</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($query as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['payment_code']) ? $v['payment_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['registration_number']) ? $v['registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['penalty_amount']) ? number_format($v['penalty_amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>
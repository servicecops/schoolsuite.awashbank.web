<div class="row">
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Date</div>
        <div class="col-xs-7 profile-text"><?= date('Y-m-d H:i a', strtotime($data['date_created'])) ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Receipt Number</div>
        <div class="col-xs-7 profile-text"><?= $data['reciept_number'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Description</div>
        <div class="col-xs-7 profile-text"><?= $data['description'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Source Channel</div>
        <div class="col-xs-7 profile-text"><?= $data['channel_name'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Source Channel Memo</div>
        <div class="col-xs-7 profile-text"><?= $data['channel_memo'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Channel Depositor Branch</div>
        <div class="col-xs-7 profile-text"><?= $data['channel_depositor_branch'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Channel Depositor Name</div>
        <div class="col-xs-7 profile-text"><?= $data['channel_depositor_name'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Channel Depositor Phone</div>
        <div class="col-xs-7 profile-text"><?= $data['channel_depositor_phone'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Transaction Type</div>
        <div class="col-xs-7 profile-text"><?= $data['trans_type'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Destination Account Title</div>
        <div class="col-xs-7 profile-text"><?= $data['account_title'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label"><b>Amount</b></div>
        <div class="col-xs-7 profile-text"><b><?= number_format($data['amount']) ?></b></div>
      </div>
</div>
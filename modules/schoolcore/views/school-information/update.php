<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = 'Update : ' . ' ' . $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="school-information-create hd-title" data-title="Add School">
<div class="letter">
  <div class="col-xs-12 no-padding">
    <h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3>
  </div>

    <?= $this->render('_form', [
        'model' => $model, 'sp_modules'=>$sp_modules, 'sch_modules'=>$sch_modules, 'banks'=>$banks, 'accounts'=>$accounts, 'bank_info_error'=>$bank_info_error, 'sections'=>$sections,
        'section_info_error'=>$section_info_error, 'school_banks'=>$school_banks
    ]) ?>

</div>
</div>

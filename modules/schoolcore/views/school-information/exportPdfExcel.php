<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="student-information">
    <?php  
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> School Information</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr><th>School Code') </th><th>School Name</th><th>Physical Address</th><th>School Type</th>
        </thead>
        <tbody>
            <?php 
            foreach($query as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['school_code']) ? $v['school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['physical_address']) ? $v['physical_address'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_type_name']) ? $v['school_type_name'] : '<span class="not-set">(not set) </span>'?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>
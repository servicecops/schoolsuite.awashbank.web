<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use bupy7\cropbox\Cropbox;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-bank-form">
    <div class="row">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    <?= $form->errorSummary($model); ?>
   <div class="col-xs-12 col-lg-12 no-padding">
        <div class="col-sm-6" style="overflow:auto!important">
    <?= $form->field($imageBank, 'image_base64')->widget(Cropbox::className(), [
        'attributeCropInfo' => 'crop_info', 
        'options'=>['accept'=>'image/*'],  
         'previewImagesUrl'=>[(!$imageBank->isNewRecord) ? "data:image/jpeg;base64,".$imageBank->image_base64 : '' ],
	]); ?>
  <?php if($err){
      echo "<div style='color:red' >".$err."</div><br>";
      } ?>
      </div>
   </div>

    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
        <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
     </div>

    <?php ActiveForm::end(); ?>
</div>

</div>

<?php 
$script = <<< JS
$("document").ready(function(){ 
    if( $('#cropped_image').is(':empty') ) {
        $('input[type="submit"], input[type="button"], button').disable(true);
        }
  });
JS;
$this->registerJs($script);
?>
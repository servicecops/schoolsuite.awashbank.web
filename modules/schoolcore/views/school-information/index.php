<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SchoolInformationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Informations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id='title' style='display:none'><?= $this->title; ?></div>
<div class="row">
    <div class="col-xs-12">

      <div class="col-sm-4 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
      <div class="col-sm-5 col-xs-12 no-padding"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
      <div class="col-sm-3 col-xs-12 no-padding">
        <ul class="menu-list pull-right">
            <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']) ?>
            </li>
            <li>
            <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
            </li>
        </ul>
    </div>
    </div>

    <div class="col-xs-12">
        <div class="box-body table table-responsive no-padding">
        <table class="table table-striped table-responsive">
        <thead>
        <tr><th><?= $sort->link('school_code') ?></th><th><?= $sort->link('school_name') ?></th><th><?= $sort->link('physical_address') ?></th> <th>Assign Channel</th><th><?= $sort->link('school_type_name') ?></th><th>&nbsp;</th></tr>
        </thead>
        <tbody>
            <?php 
            foreach($dataProvider as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['school_code']) ? $v['school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td class="clink"><?= ($v['school_name']) ? '<a href="'.Url::to(['/school-information/view', 'id'=>$v['id']]).'">'.$v['school_name'].'</a>' : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['physical_address']) ? $v['physical_address'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td class="clink"><a href="<?= Url::to(['/school-information/channels', 'id'=>$v['id']]) ?>" ><i class="fa fa-exchange"></i></a></td>
                    <td><?= ($v['school_type_name']) ? $v['school_type_name'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><a class="aclink" href="<?= Url::to(['/school-information/view', 'id'=>$v['id']]) ?>" title="View" data-pjax="0"><span class="fa fa-search"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php if (\Yii::$app->user->can('rw_sch')) : ?><a class="aclink" href="<?= Url::to(['/school-information/update', 'id'=>$v['id']]) ?>" title="Update" data-pjax="0"><span class="glyphicon glyphicon-edit"></span></a><?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]);?>

    </div>
</div>

<?php

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\SchoolCampuses;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\LinkPager;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>

<div style="color:#F7F7F7">Account Histories</div>

<div class="letter">
    <div class="row hd-title" data-title="School Account History">

        <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('successAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (\Yii::$app->session->hasFlash('errorAlert')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('errorAlert'); ?>
                </div>
            </div>
        <?php endif; ?>


    </div>
    <div class="row ">
        <div class="col-md-2">
            <span style="font-size:20px">&nbsp;<i class="fa fa-th-list"></i> Penalty Report</span>
        </div>
        <div class="col-md-7">
            <?php $form = ActiveForm::begin([
                'action' => ['/schoolcore/school-information/penalty-report'],
                'method' => 'get',
                'options' => ['class' => 'formprocess'],
            ]); ?>
            <ul style="list-style-type:none" class="col-md-12">
                <li class="col-md-2 "><?= DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_from',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                        'clientOptions' => [
                            'class' => 'form-control',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]); ?></li>
                <li class="col-md-2 "><?= DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_to',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                        'clientOptions' => [
                            'class' => 'form-control',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]); ?></li>
                <?php if (Yii::$app->user->can('rw_sch')) {

                    $url = Url::to(['/schoolcore/core-school/schoollist']);
                    $selectedSchool = empty($searchModel->account_id) ? '' : CoreSchool::findOne(['school_account_id' => $searchModel->account_id])->school_name;
                    echo '<li class="col-md-3">';
                    echo $form->field($searchModel, 'account_id')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
//                                'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => 'Filter School',
                            'id' => 'student_school_search',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],

                        ],
                    ])->label(false);
                    echo '</li>';
                    echo '<li class="col-md-3">';

                    echo $form->field($searchModel, 'student_campus')->dropDownList(['prompt' => 'Select Campus'], ['id' => 'student_campus_list'])->label(false);
                    echo '</li>';

                }


                ?>
                <?php if (\app\components\ToWords::isSchoolUser()) : ?>
                    <?php if (Yii::$app->user->identity->school->hasModule('SCHOOL_CAMPUSES')): ?>
                        <li class="col-md-2 "><?= $form->field($searchModel, 'student_campus')->dropDownList(
                                ArrayHelper::map(SchoolCampuses::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'campus_name'), ['prompt' => 'Filter Campus', 'class' => 'form-control input-sm']
                            )->label(false) ?></li>
                    <?php endif; ?>
                <?php endif; ?>

                <li class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></li>

            </ul>
            <?php ActiveForm::end(); ?>
        </div>


        <div class="col-md-3">

            <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                <?php
                echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-md btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-md btn-success',
                    'target' => '_blank'
                ]);
                ?>
            </ul>

        </div>
    </div>

    <div class="row">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class='clink'>Details</th>
                    <th class='clink'>Code</th>
                    <th class='clink'>Reg No.</th>
                    <th class='clink'>Student</th>
                    <th class='clink'>Class</th>
                    <th class='clink'><?= $sort->link('penalty_amount') ?></th>

                </tr>
                </thead>
                <tbody>
                <?php
                if ($dataProvider) :
                    foreach ($dataProvider as $k => $v) : ?>
                        <tr data-key="0">
                            <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['payment_code']) ? $v['payment_code'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['registration_number']) ? $v['registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['penalty_amount']) ? number_format($v['penalty_amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>

                        </tr>
                    <?php endforeach;
                else : ?>
                    <td colspan="14">No Record found</td>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]); ?>

    </div>
</div>

<?php

$feesUrl = Url::to(['/schoolcore/core-school/feeslists']);

$campusurl = Url::to(['/schoolcore/core-school/campuslists']);
$campus = $searchModel->student_campus;


$script = <<< JS


    
    
var schoolChanged = function() {
        var sch = $("#student_school_search").val();
        if(sch){
          
        
        $.get('$campusurl', {id : sch}, function( data ) {
                    $('#student_campus_list').html(data);
                    $('#student_campus_list').val('$campus');
                });
;
        }
        
    }
    



$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#balance_adjustment_report").addClass('active');
    
    $('body').on('change', '#student_school_search', function(){
         schoolChanged();
    });
    
    //By default trigger change in case school is already set
    schoolChanged();
    
  });
JS;
$this->registerJs($script);

?>

<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\SchoolTypes;
use app\modules\banks\models\BankDetails;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */
/* @var $form yii\widgets\ActiveForm */
$accounts = (!$model->isNewRecord) ? $accounts : '';
?>

    <?php $form = ActiveForm::begin(['options'=>['class'=>'formprocess']]); ?>
    <div class="row">
    <div class="col-xs-12 col-lg-12 no-padding">
        <div class="col-sm-4">
        <?= $form->field($model, 'school_name', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'School Name'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-4">
        <?= $form->field($model, 'physical_address', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Physical Address'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-4">
        <?= $form->field($model, 'contact_person', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Contact Person'] ])->textInput()->label('') ?>
        </div>
   </div>

   <div class="col-xs-12 col-lg-12 no-padding">
        <div class="col-sm-4">
        <?= $form->field($model, 'contact_phone1', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Contact Phone'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-4">
        <?= $form->field($model, 'contact_phone2', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Another Contact Phone'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-4">
        <?= $form->field($model, 'contact_email', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Contact Email'] ])->textInput()->label('') ?>
        </div>
   </div>

    <div class="col-xs-12 col-lg-12 no-padding">
        <div class="col-sm-4">
            <?= $form->field($model, 'school_type')->dropDownList(
                ArrayHelper::map(SchoolTypes::find()->orderBy('school_type_name')->all(), 'id', 'school_type_name'), ['prompt'=>'Select Type of School ..']
            )->label('') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'school_registration_number_format', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Reg No. Format'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'sample_school_registration_number', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Sample Reg No.'] ])->textInput()->label('') ?>
        </div>
    </div>

   <div class="col-xs-12 col-lg-12 no-padding">
       <div class="col-sm-4">
           <?= $form->field($model, 'transaction_api_password', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Transaction Api Password'] ])->textInput()->label('') ?>
       </div>
        <div class="col-sm-4">
            <?= $this->render('@app/views/reusables/bank_search',
                ['form' => $form,
                    'model' => $model,
                    'modelAttribute' => 'bank_name',
                    'label' => false,
                    'size' => Select2::MEDIUM,
                    'inputId' => 'selected_bank_id', //ID of component
                    'placeHolder' => 'Select Primary Bank',
                    'showLogoAfterSelect'=>false,
                    'label'=>''
                ]
            );
            ?>
        </div>

        <div class="col-sm-4" id="daily_stats_recipients_div">
        <?= $form->field($model, 'daily_stats_recipients', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Daily stats Recipients'] ])->textInput()->label('') ?>
        </div>
   </div>
   
    <div class="col-xs-12">
          <br>
          <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[active]" value="0">
            <input type="checkbox" id="schoolinformation-active" name="SchoolInformation[active]" value="1" <?= ($model->active) ? 'checked' : '' ?> > 
            <label for="schoolinformation-active">Active</label>
          </div>

          <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[default_part_payment_behaviour]" value="0"><input type="checkbox" id="schoolinformation-default_part_payment_behaviour" name="SchoolInformation[default_part_payment_behaviour]" value="1" <?= ($model->default_part_payment_behaviour) ? 'checked' : '' ?> >
            <label for="schoolinformation-default_part_payment_behaviour">Allow Part Payments</label>
          </div>

          <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[enable_daily_stats]" value="0">
            <input type="checkbox" id="schoolinformation-enable_daily_stats" name="SchoolInformation[enable_daily_stats]" value="1" data-checked="negative" <?= ($model->enable_daily_stats) ? 'checked' : '' ?>> 
            <label for="schoolinformation-enable_daily_stats">Enable Daily Stats</label>
          </div>

        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[enable_school_sections_logic]" value="0">
            <input type="checkbox" id="schoolinformation-enable_school_sections_logic" name="SchoolInformation[enable_school_sections_logic]" value="1" data-checked="negative" <?= ($model->enable_school_sections_logic) ? 'checked' : '' ?>>
            <label for="schoolinformation-enable_school_sections_logic">Enable Sections Logic</label>
        </div>

        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[enable_transaction_api]" value="0">
            <input type="checkbox" id="schoolinformation-enable_transaction_api" name="SchoolInformation[enable_transaction_api]" value="1" data-checked="negative" <?= ($model->enable_transaction_api) ? 'checked' : '' ?>>
            <label for="schoolinformation-enable_transaction_api">Enable Transaction Api</label>
        </div>

        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[enable_bank_statement]" value="0">
            <input type="checkbox" id="schoolinformation-enable_bank_statement" name="SchoolInformation[enable_bank_statement]" value="1" data-checked="negative" <?= ($model->enable_bank_statement) ? 'checked' : '' ?>>
            <label for="schoolinformation-enable_bank_statement">Enable Bank Statement</label>
        </div>

        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[enable_registration_numbers]" value="0">
            <input type="checkbox" id="schoolinformation-enable_registration_numbers" name="SchoolInformation[enable_registration_numbers]" value="1" data-checked="negative" <?= ($model->enable_registration_numbers) ? 'checked' : '' ?>>
            <label for="schoolinformation-enable_registration_numbers">Enable Registration Numbers</label>
        </div>

        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[registration_numbers_required]" value="0">
            <input type="checkbox" id="schoolinformation-registration_numbers_required" name="SchoolInformation[registration_numbers_required]" value="1" data-checked="negative" <?= ($model->registration_numbers_required) ? 'checked' : '' ?>>
            <label for="schoolinformation-registration_numbers_required">Registration Numbers Required</label>
        </div>

   </div>



   <div class="col-xs-12  no-padding">
    <div class="col-sm-12"> <h3><i class="fa fa-check-square-o"></i>&nbsp;Provide Bank Information</h3> </div>
   </div>
   <div class="col-xs-12 no-padding">
     <?= $this->render('_bank_info', ['form'=>$form, 'model'=>$model, 'banks'=>$banks, 'accounts'=>$accounts, 'bank_info_error'=>$bank_info_error]); ?>
   </div>

   <div class="col-xs-12 col-lg-12 no-padding">
    <div class="col-sm-12"> <h3><i class="fa fa-check-square-o"></i>&nbsp;Select the modules this school will use</h3> </div>
   </div>
<?php if($sp_modules) : ?>
    
      <div class="col-xs-12 no-padding">
      <div class="checkbox">
       <?php
       foreach ($sp_modules as $k => $v) :
           $checked = in_array($v['id'], $sch_modules) || ($v['module_code']=='SCHOOLPAY_CODE') ? true : false;
           ?>
              <div class = "checkbox checkbox-inline checkbox-info">
              <input type="checkbox" class="checkbox" name="sp_modules[]" value="<?= $v['id'] ?>" id="<?= $v['module_code'] ?>" <?= $checked ? 'checked' : ''; ?> >
              <label for="<?= $v['module_code'] ?>">&nbsp;<?= $v['module_name'] ?></label>
              </div>
        <?php endforeach; ?>
      </div>
      </div>
    
    <?php endif; ?>


        <?php if($model->enable_school_sections_logic) : ?>
            <div class="col-xs-12  no-padding">
                <div class="col-sm-12"> <h3><i class="fa fa-check-square-o"></i>&nbsp;School Sections</h3> </div>
            </div>
            <div class="col-xs-12 no-padding">
                <?= $this->render('_school_sections', ['form'=>$form, 'model'=>$model,
                    'sections'=>$sections, 'section_info_error'=>$section_info_error, 'banks'=>$banks, 'accounts'=>$accounts,
                    'school_banks'=>$school_banks]); ?>
            </div>
        <?php endif; ?>



   <div class="form-group col-xs-12 col-sm-12 col-lg-12">
   <br>
       <hr class="l_header" style="margin-top:15px;">
   
   </div>
   <div class="form-group col-xs-6 no-padding">
    <div class="col-xs-6">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
    </div>
    <div class="col-xs-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
   </div>

</div>
    <?php ActiveForm::end(); ?>
<script type="text/javascript">
$('document').ready(function(){
    if(!$("input#schoolinformation-enable_daily_stats").attr('checked')){
      $('input#schoolinformation-enable_daily_stats').attr('data-checked', 'negative');
      $('div#daily_stats_recipients_div').css('display', 'none');
    } else {
      $('input#schoolinformation-enable_daily_stats').attr('data-checked', 'positive');
      $('div#daily_stats_recipients_div').css('display', 'block');
    }

    $('body').on('change', 'input#schoolinformation-enable_daily_stats', function(){
        if($(this).attr('data-checked')=='negative'){
          $('div#daily_stats_recipients_div').css('display', 'block');
          $(this).attr('data-checked', 'postive');
        } else {
          $('div#daily_stats_recipients_div').css('display', 'none');
          $(this).attr('data-checked', 'negative');
        }
    });
}) 
</script>
<?php

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\LinkPager;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>


<div class="row hd-title" data-title="School Account History">

    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('successAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('errorAlert')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('errorAlert'); ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="row ">
        <div class="col-md-12 ">

            <?php $form = ActiveForm::begin([
                'action' => ['/schoolcore/school-information/accounthistory'],
                'method' => 'get',
                'options' => ['class' => 'formprocess'],
            ]); ?>
            <ul style="list-style-type:none" class="row">
                <li class="col-md-2 col-sm-1 col-xs-1"><?= DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_from',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                        'clientOptions' => [
                            'class' => 'form-control',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]); ?></li>
                <li class="col-md-2 col-sm-1 col-xs-1"><?= DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_to',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                        'clientOptions' => [
                            'class' => 'form-control',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]); ?></li>
                <?php if (Yii::$app->user->can('rw_sch')) {
                    $url = Url::to(['core-school/schools']);
                    $selectedSchool = empty($searchModel->account_id) ? '' : CoreSchool::findOne(['school_account_id' => $searchModel->account_id])->school_name;
                    echo '<li class="col-md-2">';
                    echo $form->field($searchModel, 'account_id')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
                        'size' => 'sm',
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => 'Search School',
                            'id' => 'student_selected_school_id',
                            'class' => '',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],

                        ],
                    ])->label(false);
                    echo '</li>';

                } ?>

                <li class="col-md-2 col-sm-4 col-xs-4"><?= $form->field($searchModel, 'payment_id')->dropDownList(ArrayHelper::map(PaymentChannels::find()->all(), 'id', 'channel_name'), ['prompt' => 'Filter channel', 'class' => 'form-control input-sm'])->label(false) ?></li>
                <li class="col-md-2 col-sm-2 col-xs-2"><?= $form->field($searchModel, 'transaction_id', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Trans ID'])->label(false) ?></li>
                <li class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></li>
            </ul>
            <?php ActiveForm::end(); ?>
        </div>


    <div class="col-md-12">
        <div class="col-md-6">

            <i class="fa fa-hourglass-half icon-pending-color"> Processing</i>&nbsp;
            <i class="fa fa-check-circle icon-success-color"> Completed</i>&nbsp;

        </div>

        <div class="col-md-6">

            <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                <?php
                echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-sm btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-sm btn-success',
                    'target' => '_blank'
                ]);
                ?>
            </ul>

        </div>
    </div>
</div>

<div class="row">
    <div class="box-body table table-responsive no-padding">
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th class='clink'><?= $sort->link('date_created', ['label' => 'Date']) ?></th>
                <th class='clink'>Details</th>
                <th class='clink'>Code</th>
                <th class='clink'>Reg No.</th>
                <th class='clink'>Student</th>
                <th class='clink'>Class</th>
                <th class='clink'><?= $sort->link('channel_trans_id', ['label' => 'Ch Trans Id']) ?></th>
                <th class='clink'><?= $sort->link('reciept_number', ['label' => 'Receipt No.']) ?></th>
                <th class='clink'><?= $sort->link('trans_type', ['label' => 'Type']) ?></th>
                <th class='clink'><?= $sort->link('channel_code', ['label' => 'Channel']) ?></th>
                <th class='clink'><?= $sort->link('channel_memo', ['label' => 'Ch. Info']) ?></th>
                <th class='clink'><?= $sort->link('amount') ?></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">
                        <td><?= ($v['processed']) ? '<i class="fa fa-check-circle icon-success-color"></i>' : '<i class="fa fa-hourglass-half icon-pending-color"></i>' ?></td>
                        <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['payment_code']) ? $v['payment_code'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['registration_number']) ? $v['registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['reciept_number']) ? $v['reciept_number'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['trans_type']) ? $v['trans_type'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_code']) ? '<img src="' . Url::to(['/import/import/image-link2', 'id' => $v['payment_channel_logo']]) . '" width="30px" /> ' . $v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_memo']) ? $v['channel_memo'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['payment_id']) ? Html::a('<span class="pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;&nbsp;</span>', Url::to(['/site/r', 'i' => base64_encode($v['payment_id'])]), ['target' => '_blank']) : ''; ?></td>

                        <td data-name="<?= $v['trans_type'] . ' (' . number_format($v['amount']) . ')' ?>"
                            data-toggle=popover
                            data-href="<?= Url::to(['/schoolcore/school-information/history', 'id' => $v['id']]) ?>">
                            <i class="fa fa-arrow-alt-circle-right"></i>&nbsp;&nbsp;
                        </td>
                        <td>

                        </td>
                    </tr>
                <?php endforeach;
            else : ?>
                <td colspan="16">No Record found</td>
            <?php endif; ?>

            </tbody>
        </table>
    </div>
    <?= LinkPager::widget([
        'pagination' => $pages['pages'],
    ]); ?>

</div>


<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

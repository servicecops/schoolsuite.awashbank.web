<?php $i=1; ?>
<div id="sch_sections_info">
<?php if($section_info_error) :?>
			<div class='col-xs-12'><span style="color:red;">Section information error</span></div>
		<?php endif; ?>
		<div class='col-xs-12'>
    <span id="add_section_button" class="btn btn-info">Add Section</span></div>
	<?php if(!$model->isNewRecord) :
		foreach($sections as $key=>$value)  : ?>
	<div id="<?= 'add_section_row_'.$i ?>" class="col-xs-12 no-padding">
		<br>

		<input type="hidden" name="section_name[<?= $i ?>][id]" value="<?= $value['id'] ?>">
		<div class="col-xs-3 no-padding-right">
			<input  class="form-control" type="text" name="section_name[<?= $i;?>][section_code]" placeholder="Section Code" value="<?= $value['section_code']?>" readonly disabled>
		</div>
		<div class="col-xs-3 no-padding-right">
			<input  class="form-control" type="text" name="section_name[<?= $i;?>][section_name]" placeholder="Section Name" value="<?= $value['section_name']?>">
		</div>


        <div class="col-xs-2 no-padding-right">
            <select id="sel_section_bank_id" class="form-control" name="section_name[<?= $i ?>][section_primary_bank]">
                <option value=""> Section Pri. Bank</option>
                <?php foreach($school_banks as $k=>$v) : ?>
                    <option value="<?= $v['id']?>" <?= $v['bank_id']==$value['section_primary_bank'] ? 'selected' : ''; ?> > <?= $v['bank_name'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>


		<div class="col-xs-1 no-padding-left">
            <span id="remove_section_button" class="btn btn-danger" data-for="<?= 'add_section_row_'.$i ?>">Remove</span>
		</div>
	</div>
	<?php 
		$i++;
		endforeach;
		endif;
		?>



</div>

<?php
$newSectionsBankDropDownData = '';
 foreach($school_banks as $k=>$v) {
     $newSectionsBankDropDownData .= '<option value="' . $v['id'] . '" > '.$v['bank_name'].'</option>';
}

$script = <<< JS
$("document").ready(function(){
		var row = $i;
		$('body').on('click', 'span#add_section_button', function(e){
			row++;
			var new_row_html = row_html(row);
			$('div#sch_sections_info').append(new_row_html);
		});
		$('body').on('click', 'span#remove_section_button', function(e){
			$('div#'+$(this).attr('data-for')).remove();
		});

		var row_html = function(i){
			var this_html = '<div id="add_section_row_'+i+'" class="col-xs-12 no-padding">' +
			 '<br><div class="col-xs-3 no-padding-right"><input  class="form-control" type="text" name="section_name['+i+'][section_code]" placeholder="Section Code" readonly disabled></div>' +
			  '' +
			   '<div class="col-xs-3"><input  class="form-control" type="text" name="section_name['+i+'][section_name]" placeholder="Section Name"></div>' +
			    '' +
			     '<div class="col-xs-2 no-padding-right"> '+
            '<select id="sel_section_bank_id" class="form-control" name="section_name['+i+'][section_primary_bank]">'+
                '<option value=""> Section Pri. Bank</option>$newSectionsBankDropDownData</select>' +
        '</div>' +
			      '' +
			     '<div class="col-xs-1 no-padding-left"><span id="remove_section_button" class="btn btn-danger" data-for="add_section_row_'+i+'">Remove</span></div></div>';
			return this_html;
		}
	});
JS;
$this->registerJs($script);
?>


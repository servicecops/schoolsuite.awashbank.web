<?php
use yii\helpers\Html;

?>

<div class='row'>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('school_code') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->school_code) ? $model->school_code : "--" ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('physical_address') ?></b>
            </div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->physical_address) ? $model->physical_address : "--" ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('school_type') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->school_type) ? $model->schoolType->school_type_name : "-- " ?></div>
        </div>

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('contact_email') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->contact_email) ? $model->contact_email : "--" ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('contact_person') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->contact_person) ? $model->contact_person : " --" ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('contact_phone1') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->contact_phone1) ? $model->contact_phone1 : " --" ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('bank_name') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->bank_name) ? $model->bank->bank_name : " --" ?></div>
        </div>

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('bank_account_number') ?></b>
            </div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->bank_account_number) ? $model->bank_account_number : " --" ?></div>
        </div>

    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('contact_phone2') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->contact_phone2) ? $model->contact_phone2 : " --" ?></div>
        </div>

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('date_created') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->date_created ?></div>
        </div>
    </div>


    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text">
                <b><?= $model->getAttributeLabel('school_registration_number_format') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->school_registration_number_format) ? $model->school_registration_number_format : " --" ?></div>
        </div>

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text">
                <b><?= $model->getAttributeLabel('sample_school_registration_number') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->sample_school_registration_number) ? $model->sample_school_registration_number : " --" ?></div>
        </div>

    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('active') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->active) ? "Yes" : "No" ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text">
                <b><?= $model->getAttributeLabel('default_part_payment_behaviour') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->default_part_payment_behaviour) ? "Yes" : "No" ?></div>
        </div>

    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('daily_stats_recipients') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->daily_stats_recipients) ? $model->daily_stats_recipients : "--"  ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text">
                <b>Enable Sections Logic</b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->enable_school_sections_logic) ? "Yes" : "No" ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b><?= $model->getAttributeLabel('transaction_api_password') ?></b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->transaction_api_password) ? $model->transaction_api_password : "--"  ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text">
                <b>Enable Transaction Api</b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->enable_transaction_api) ? "Yes" : "No" ?></div>
        </div>
    </div>

    <div class="col-xs-12 ">
        <div class=" col-xs-3 profile-text">
            <b>Enable Bank Statement</b></div>
        <div class="col-xs-9 profile-text"><?= ($model->enable_bank_statement) ? "Yes" : "No" ?></div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-text"><b>Enable Registration Numbers?</b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->enable_registration_numbers) ? "Yes" : "No"  ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-text">
                <b>Registration Numbers Required</b></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->registration_numbers_required) ? "Yes" : "No" ?></div>
        </div>
    </div>

    <div class="col-xs-12">
        <table class="table" style="">
            <thead>
            <tr>
                <th colspan="4" style="text-align:center"><h3>Bank Information</h3></th>
            </tr>
            <tr>
                <th> Bank</th>
                <th> Account Title</th>
                <th> Account Type</th>
                <th> Account Number</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($accounts) :
                foreach ($accounts as $k => $v): ?>
                    <tr>
                        <td><?= $v['bank_name'] ?></td>
                        <td><?= $v['account_title'] ?></td>
                        <td><?= $v['account_type'] ?></td>
                        <td><?= $v['account_number'] ?></td>
                    </tr>
                <?php endforeach;
            else :
                ?>
                <tr>
                    <td colspan="4">No account provided for this school</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <?php if ($model->enable_school_sections_logic) : ?>
        <div class="col-xs-12">
            <table class="table" style="">
                <thead>
                <tr>
                    <th colspan="4" style="text-align:center"><h3>School Sections</h3></th>
                </tr>
                <tr>
                    <th> Section Code</th>
                    <th> Section Name</th>
                    <th> Section Pri. Bank</th>

                </tr>
                </thead>
                <tbody>
                <?php
                if ($sections) :
                    foreach ($sections as $k => $v): ?>
                        <tr>
                            <td><?= $v['section_code'] ?></td>
                            <td><?= $v['section_name'] ?></td>
                            <td><?= $v['bank_name'] ?></td>
                        </tr>
                    <?php endforeach;
                else :
                    ?>
                    <tr>
                        <td colspan="2">No sections available for this school</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    <?php endif; ?>
</div>
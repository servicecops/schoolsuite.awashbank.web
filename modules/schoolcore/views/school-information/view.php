<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="school-information-view hd-title" data-title="<?= $model->school_name ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="letter">
                    <div class="row" style="padding:0 18px;">
                        <div class="l-address">
                            Address: <?= $model->physical_address ?><br>
                            Email: <?= $model->contact_email ?><br>
                            Tel: <?= $model->contact_phone1 ?><br>
                            Mobile:<?= $model->contact_phone2 ?>

                        </div>
                        <?php if ($model->school_logo): ?>
                            <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->logo->image_base64 ?>"
                                 height="100" width="100"/>
                        <?php else : ?>
                            <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>

                        <?php endif; ?><br>
                        <span class="col-md-12 col-sm-12 col-xs-12 sch-title"> <?= Html::encode($this->title) ?></span>

                        <div class="top-buttons pull-right">
                            <?php if (Yii::$app->user->can('edit_school_logo')) : ?>
                                <?= Html::a('<i class="fa fa-edit"></i> Edit Logo', ['logo', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary aclink']) ?>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can('schoolpay_admin') && Yii::$app->user->can('rw_sch')) : ?>
                                <?= Html::a('<i class="fa fa-exclamation"></i> Suite Me Up!', ['enable-suite', 'id' => $model->school_code], ['class' => 'btn btn-sm btn-success aclink', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to enable the school suite model for this school"]) ?>
                                <?= Html::a('<i class="fa fa-edit"></i> Edit Details', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary aclink']) ?>
                            <?php endif; ?>

                            <?php if (Yii::$app->user->can('schoolpay_admin') && Yii::$app->user->can('del_sch')) : ?>
                                <?= Html::a('<i class="fa fa-remove"></i> Delete School', ['delete', 'id' => $model->school_code], ['class' => 'btn btn-sm btn-danger', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to delete this school"]) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <hr class="l_header">


                    <div class="profile-data">

                        <ul class="nav nav-tabs " id="profileTab">
                            <li class="active" id="school-tab"><a href="#schoolInfo" data-toggle="tab"><i
                                            class="fa fa-street-view"></i> School Info</a></li>
                            <li id="classes-tab"><a href="#schClasses" data-toggle="tab"><i class="fa fa-user"></i>
                                    Classes</a></li>
                            <?php if (Yii::$app->user->can('payments_received_list')) : ?>
                                <li id="trasactions-tab"><a href="#transHistory" data-toggle="tab"><i
                                                class="fa fa-inr"></i> Transaction History</a></li>
                                <li id="payments-tab"><a href="#paymentsReceived" data-toggle="tab"><i
                                                class="fa fa-inr"></i> Payments Received</a></li>
                            <?php endif; ?>
                        </ul>
                        <br>
                        <div id='content' class="tab-content responsive">

                            <div class="tab-pane active" id="schoolInfo">
                                <?= $this->render('_tab_school_info', ['model' => $model, 'accounts' => $accounts, 'sections'=>$sections]) ?>
                            </div>

                            <div class="tab-pane" id="schClasses">
                                <?= $this->render('_tab_school_classes', ['model' => $model]) ?>
                            </div>
                            <?php if (Yii::$app->user->can('payments_received_list')) : ?>
                                <div class="tab-pane" id="transHistory">
                                    <?= $this->render('_tab_trans_history', ['model' => $model]) ?>
                                </div>

                                <div class="tab-pane" id="paymentsReceived">
                                    <?= $this->render('_tab_payments_received', ['model' => $model]) ?>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#schoolInfo").addClass('active');
    $("#schools").addClass('active');
  });
JS;
$this->registerJs($script);
?>

<?php $this->registerJs("(function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);", yii\web\View::POS_END, 'responsive-tab'); ?>

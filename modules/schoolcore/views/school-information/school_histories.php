<?php

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\LinkPager;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>


<div class="row hd-title" data-title="School Account History">

    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('successAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('errorAlert')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('errorAlert'); ?>
            </div>
        </div>
    <?php endif; ?>


        <div class="col-md-12 ">

            <?php $form = ActiveForm::begin([
                'action' => ['/schoolcore/school-information/branch-performance'],
                'method' => 'get',
                'options' => ['class' => 'formprocess'],
            ]); ?>
            <ul style="list-style-type:none" class="row">
                <li class="col-md-2 col-sm-1 col-xs-1"><?= DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_from',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                        'clientOptions' => [
                            'class' => 'form-control',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]) ?></li>
                <li class="col-md-2 col-sm-1 col-xs-1"><?= DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_to',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                        'clientOptions' => [
                            'class' => 'form-control',
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]); ?></li>
                <div class="col-sm-3">

                    <?php
                    $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
                    echo $form->field($searchModel, 'branch_region')
                        ->dropDownList(
                            $items,           // Flat array ('id'=>'label')
                            ['prompt' => 'Select Branch Region',
                                'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/branches']) . '?id="+$(this).val(), function( data ) {
                            $( "select#branches" ).html(data);
                        });']    // options
                        )->label(false); ?>

                </div>
                <div class="col-sm-3">


                    <?= $form->field($searchModel, 'branch_id')->dropDownList([],

                        ['id' => 'branches',
                        ])->label(false)
                    ?>

                </div>

                <li class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></li>
            </ul>
            <?php ActiveForm::end(); ?>
        </div>


    <div class="col-md-12">
        <div class="col-md-6">


        </div>

        <div class="col-md-6">

            <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                <?php
                echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-sm btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class' => 'btn btn-sm btn-success',
                    'target' => '_blank'
                ]);
                ?>
            </ul>

        </div>
    </div>

    <div class="col-md-12 box-body table table-responsive no-padding">
        <table class="table table-striped">
            <thead>
            <tr>

                <th class='clink'>Bank Region</th>
                <th class='clink'>Bank Branch</th>
                <th class='clink'>Country Region</th>
                <th class='clink'>No of Schools</th>

            </tr>
            </thead>
            <tbody>
            <?php

            if ($data) :
                foreach ($data as $k => $v) :
                    ?>

                    <tr data-key="0">
                        <td><?= ($v['bank_region_name']) ? $v['bank_region_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['branch_name']) ? $v['branch_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['schs']) ? $v['schs'] : '<span class="not-set">(not set) </span>' ?></td>
                    </tr>
                <?php endforeach;
            else : ?>
                <td colspan="16">No Record found</td>
            <?php endif; ?>

            </tbody>
        </table>
    </div>
    <?= LinkPager::widget([
        'pagination' => $pages['pages'],
    ]); ?>

</div>


<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

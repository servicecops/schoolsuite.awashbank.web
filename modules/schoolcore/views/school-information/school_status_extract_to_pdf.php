
<?php


?>
<div class="col-md-12">
        <table class="table">
        <?php if($type=='excel') : ?>
            <tr><td colspan="7">SCHOOL STATUS REPORT </td><tr>
        <?php endif; ?>
        <thead>
        <tr>
        	<th></th>

        </tr>
        </thead>
        <tbody>

        </tbody>
        </table>
    <table class="table table-striped">
        <thead>
        <?php
        $x=1;
        ?>
        <h3>Active Schools Users by Branch and Region</h3>

        <tr>

            <th>Bank Region</th>
            <th>Country Region</th>
            <th>Branch</th>
            <th>School ID</th>
            <th>School Name</th>
            <th>Number of Students</th>
            <th>Last Transaction Date</th>
            <th>status</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($dataProvider) :
            foreach($dataProvider as $k=>$v) : ?>
                <?php
                Yii::trace($v);
                ?>
                <tr>
                    <?php ?>
                    <td><?= ($v['bank_region_name']) ? $v['bank_region_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['branch_name']) ? $v['branch_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['external_school_code']) ? $v['external_school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['stds']) ? $v['stds'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['last_transaction_date']) ? date('d/m/y H:i', strtotime($v['last_transaction_date'])) : '<span class="not-set">(not set) </span>' ?></td>

                    <td><?= ($v['status']) ? $v['status'] : '<span class="not-set">(not set) </span>' ?></td>

                    <td></td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr><td colspan="7">No School Lists found </td></tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

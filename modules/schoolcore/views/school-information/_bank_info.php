<?php $i=1; ?>
<div id="sch_bank_info">
<?php if($bank_info_error) :?>
			<div class='col-xs-12'><span style="color:red;">Invalid Bank information (Rows with blank fields where detected. Please recreate)</span></div>
		<?php endif; ?>
	<?php if(!$model->isNewRecord && $accounts) : 
		foreach($accounts as $key=>$value)  : ?>
	<div id="<?= 'add_bank_row_'.$i ?>" class="col-xs-12 no-padding">
		<br>

		<input type="hidden" name="bank_name[<?= $i ?>][id]" value="<?= $value['bid'] ?>">
		<div class="col-xs-2 no-padding-right">
			<select id="sel_bank_id" class="form-control" name="bank_name[<?= $i ?>][bank_id]">
				<option value=""> Select Bank</option>
				<?php foreach($banks as $k=>$v) : ?>
					<option value="<?= $v['id']?>" <?= $v['id']==$value['bank_id'] ? 'selected' : ''; ?> > <?= $v['bank_name'] ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="col-xs-3 no-padding-right">
			<input  class="form-control" type="text" name="bank_name[<?= $i;?>][account_type]" placeholder="Account Type" value="<?= $value['account_type']?>">
		</div>
		<div class="col-xs-3 no-padding-right">
			<input  class="form-control" type="text" name="bank_name[<?= $i;?>][account_title]" placeholder="Account Title" value="<?= $value['account_title']?>">
		</div>
		<div class="col-xs-3">
			<input  class="form-control" type="text" name="bank_name[<?= $i;?>][account_number]" placeholder="Account Number" value="<?= $value['account_number']?>">
		</div>

		<div class="col-xs-1 no-padding-left">
			<?php if($i ==1)  : ?>
				<span id="add_bank_button" class="btn btn-info">Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<?php else : ?>
				<span id="remove_bank_button" class="btn btn-danger" data-for="<?= 'add_bank_row_'.$i ?>">Remove</span>
			<?php endif; ?>
		</div>
	</div>
	<?php 
		$i++;
		endforeach;
		else :
	?>
	
	<div id="<?= 'add_bank_row_'.$i ?>" class="col-xs-12 no-padding">
		<br>
		<div class="col-xs-2 no-padding-right">
			<select id="sel_bank_id" class="form-control" name="bank_name[<?= $i ?>][bank_id]">
				<option value=""> Select Bank</option>
				<?php foreach($banks as $k=>$v) : ?>
					<option value="<?= $v['id']?>" > <?= $v['bank_name'] ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="col-xs-3 no-padding-right">
			<input  class="form-control" type="text" name="bank_name[<?= $i;?>][account_type]" placeholder="Account Type" >
		</div>
		<div class="col-xs-3 no-padding-right">
			<input  class="form-control" type="text" name="bank_name[<?= $i;?>][account_title]" placeholder="Account Title" >
		</div>
		<div class="col-xs-3">
			<input  class="form-control" type="text" name="bank_name[<?= $i;?>][account_number]" placeholder="Account Number" >
		</div>

		<div class="col-xs-1 no-padding-left">
			<?php if($i ==1)  : ?>
				<span id="add_bank_button" class="btn btn-info">Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<?php else : ?>
				<span id="remove_bank_button" class="btn btn-danger" data-for="<?= 'add_bank_row_'.$i ?>">Remove</span>
			<?php endif; ?>
		</div>
	</div>
	<?php endif; ?>



</div>

<?php
$script = <<< JS
$("document").ready(function(){
		var row = $i;
		$('span#add_bank_button').on('click', function(e){
		    e.preventDefault();
			row++;
			var new_row_html = row_html(row);
			$('div#sch_bank_info').append(new_row_html);
		});
		
		$('body').on('click', 'span#remove_bank_button', function(e){
			$('div#'+$(this).attr('data-for')).remove();
		});

		var row_html = function(i){
			var bank_options = $('select#sel_bank_id').html();
			var this_html = '<div id="add_bank_row_'+i+'" class="col-xs-12 no-padding"><br><div class="col-xs-2 no-padding-right"><select class="form-control" name="bank_name['+i+'][bank_id]">'+bank_options+'</select></div><div class="col-xs-3 no-padding-right"><input  class="form-control" type="text" name="bank_name['+i+'][account_type]" placeholder="Account Type"></div><div class="col-xs-3 no-padding-right"><input  class="form-control" type="text" name="bank_name['+i+'][account_title]" placeholder="Account Title"></div><div class="col-xs-3"><input  class="form-control" type="text" name="bank_name['+i+'][account_number]" placeholder="Account Number"></div><div class="col-xs-1 no-padding-left"><span id="remove_bank_button" class="btn btn-danger" data-for="add_bank_row_'+i+'">Remove</span></div></div>';

			return this_html;
		}
	});
JS;
$this->registerJs($script);
?>



<?php


?>
<div class="col-md-12">
        <table class="table">
        <?php if($type=='excel') : ?>
            <tr><td colspan="7">USERS REPORT </td><tr>
        <?php endif; ?>
        <thead>
        <tr>
        	<th></th>

        </tr>
        </thead>
        <tbody>

        </tbody>
        </table>
    <table class="table table-striped">
        <thead>
        <?php
        $x=1;
        ?>
        <h3>Active Schools Users by Branch and Region</h3>

        <tr>
            <th></th>

            <th class='clink'>Bank Region Name</th>
            <th class='clink'>Branch Name</th>
            <th class='clink'>Country Region</th>
            <th class='clink'>School Name</th>
            <th class='clink'>Total Schools Users</th>
            <th class='clink'>Active School Users</th>
            <th class='clink'>Inactive School Users</th>
        </tr>
        </thead>
        <tbody>
        <?php

        if ($dataProvider) :

            foreach ($dataProvider as $data) : ?>
                <tr data-key="0">
                    <td><?php echo $x++?></td>


                    <td>
                        <?= ($data['bank_region_name']) ? $data['bank_region_name'] : '<span class="not-set"> 0 </span>' ?>
                    </td>
                    <td>
                        <?= ($data['branch_name']) ? $data['branch_name'] : '<span class="not-set"> 0 </span>' ?>
                    </td>
                    <td>
                        <?= ($data['description']) ? $data['description'] : '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['school_name']) ? $data['school_name'] : '<span class="not-set"> 0  </span>' ?>
                    </td>

                    <td>
                        <?= ($data['ttusers']) ? $data['ttusers'] : '<span class="not-set"> 0  </span>' ?>
                    </td>

                    <td>
                        <?= ($data['actuser']) ? $data['actuser'] : '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['inactive']) ? $data['inactive'] : '<span class="not-set"> 0  </span>' ?>
                    </td>



                </tr>
            <?php endforeach;
        else :?>
            <tr>
                <td colspan="8">No data found </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

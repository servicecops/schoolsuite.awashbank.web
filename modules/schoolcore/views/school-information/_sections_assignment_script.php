<?php
use yii\helpers\Url;
?>
<script type="text/javascript">
<?php $this->beginBlock('JS_END') ?>
    yii.process = (function ($) {
        var _onSearch = false;
        var pub = {
            PermSearch: function () {
                if (!_onSearch) {
                    _onSearch = true;
                    var $th = $(this);
                    setTimeout(function () {
                        _onSearch = false;
                        var data = {
                            class:<?= json_encode($model->section_name) ?>,
                            section:<?= json_encode($model->id ) ?> ,
                            target:$th.data('target'),
                            term: $th.val(),
                        };
                        var target = '#' + $th.data('target');
                        $.get('<?= Url::toRoute(['perm-search']) ?>', data,
                            function (html) {
                                $(target).html(html);
                            });
                    }, 500);
                }
            },
            action: function () {
                var action = $(this).data('action');
                var params = $((action == 'assign' ? '#available' : '#assigned') + ', .perm-search').serialize();
                var urlAssign = '<?= Url::toRoute(['setclasssection', 'section' => $model->id,'action'=>'assign', 'school_id'=> $model->school_id]) ?>';
                var urlDelete = '<?= Url::toRoute(['setclasssection', 'section' =>  $model->id,'action'=>'delete', 'school_id'=> $model->school_id]) ?>';
                $.post(action=='assign' ? urlAssign : urlDelete,
                    params, function (r) {
                        $('#available').html(r[0]);
                        $('#assigned').html(r[1]);
                    });
                return false;
            }
        }

        return pub;
    })(window.jQuery);
<?php $this->endBlock(); ?>

<?php $this->beginBlock('JS_READY') ?>
    $('.perm-search').keydown(yii.process.PermSearch);
    $('a[data-action]').click(yii.process.action);
<?php $this->endBlock(); ?>
</script>
<?php
yii\web\YiiAsset::register($this);
$this->registerJs($this->blocks['JS_END'], yii\web\View::POS_END);
$this->registerJs($this->blocks['JS_READY'], yii\web\View::POS_READY);
?>

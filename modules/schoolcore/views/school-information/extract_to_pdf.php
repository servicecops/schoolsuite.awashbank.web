
<?php


?>
<div class="col-md-12">
        <table class="table">
        <?php if($type=='excel') : ?>
            <tr><td colspan="7">BRANCH REPORT (<?= $model->from_date?> - <?= $model->to_date ?>)</td><tr>
        <?php endif; ?>
        <thead>
        <tr>
        	<th></th>

        </tr>
        </thead>
        <tbody>

        </tbody>
        </table>
    <table class="table table-striped">
        <thead>
        <?php
        $x=1;
        ?>
        <h3>Active Schools by Branch and Region</h3>

        <tr>
            <th></th>

            <th class='clink'>Branch Name</th>
            <th class='clink'>Region</th>
            <th class='clink'>Inactive Schools</th>
            <th class='clink'>Active Schools</th>
            <th class='clink'>Total Schools</th>

        </tr>
        </thead>
        <tbody>
        <?php

        if ($dataProvider) :
            Yii::trace($dataProvider);

            foreach ($dataProvider as $data) : ?>
                <tr data-key="0">
                    <td><?php echo $x++?></td>


                    <td>
                        <?= ($data['branch_name']) ? $data['branch_name'] : '<span class="not-set"> 0 </span>' ?>
                    </td>
                    <td>
                        <?= ($data['description']) ? $data['description'] : '<span class="not-set"> 0  </span>' ?>
                    </td>

                    <td>
                        <?= ($data['inactive_schools']) ? $data['inactive_schools'] : '<span class="not-set"> 0  </span>' ?>
                    </td>

                    <td>
                        <?= ($data['active_schools']) ? $data['active_schools'] : '<span class="not-set"> 0  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['total_schools']) ? $data['total_schools'] : '<span class="not-set"> 0  </span>' ?>
                    </td>



                </tr>
            <?php endforeach;
        else :?>
            <tr>
                <td colspan="8">No data found </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

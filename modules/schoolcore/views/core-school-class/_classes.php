<div class="container">
    <?php $i=1; ?>
    <div id="submission_Contacts">
        <div style="padding:20px 0 10px 0;" class="row">
            <div class="col no-padding-left"><span class="text-uppercase">Class Code</span></div>
            <div class="col no-padding-left"><span class="text-uppercase">Class Description</span></div>
            <div class="col-auto no-padding-left"><span style="padding-left: 24px;">&nbsp;</span></div>
        </div>
        <?php if(is_array($model->grading) && $model->grading) :
            foreach( $model->grading as $key=>$value)  : ?>
                <div style="padding-bottom:20px;" id="<?= 'add_contact_row_'.$i ?>" class="row">
                    <div class="col no-padding-left">
                        <?php if(!$model->isNewRecord) : ?>
                            <input type="hidden" name="CoreSchoolClass[grading][<?= $i ?>][id]" value="<?= $value['id'] ?>">
                        <?php endif; ?>
                        <input  class="form-control" type="text" required="required"  name="CoreSchoolClass[grading][<?= $i;?>][min]" value="<?= $value['min']?>">
                    </div>

                    <div class="col no-padding-left">
                        <input  class="form-control" type="text"   name="CoreSchoolClass[grading][<?= $i;?>][grades]" value="<?= $value['grades']?>">
                        <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label('Class *') ?>

                    </div>

                    <div class="col-auto no-padding-left">
                        <?php if($i ==1)  : ?>
                            <span class="add_contact_button" style="font-size: 27px;"><i class="fa-plus-circle text-primary"></i></span>
                        <?php else : ?>
                            <span class="remove_contact_button" type="button" style="font-size: 27px;" data-for="<?= 'add_contact_row_'.$i ?>"><i class="fas fa-minus-circle text-danger"></i></span>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
                $i++;
            endforeach;
        else :
            ?>
            <div style="padding-bottom:20px;" id="<?= 'add_contact_row_'.$i ?>" class="row">
                <div class="col no-padding-left">
                    <input  class="form-control" type="text" required="required" name="CoreSchoolClass[grading][<?= $i;?>][min]"  aria-required="true">
                </div>
                <div class="col no-padding-left">
                    <input  class="form-control" type="text" name="CoreSchoolClass[grading][<?= $i;?>][grades]" >
                </div>


                <div class="col-auto no-padding-left">
                    <?php if($i ==1)  : ?>
                        <span class="add_contact_button" style="font-size: 27px;"><i class="fas fa-plus-circle text-primary"></i></span>
                    <?php else : ?>
                        <span class="remove_contact_button" style="font-size:27px;" data-for="<?= 'add_contact_row_'.$i ?>"><i class="fas fa-minus-circle text-danger"></i></span>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php
$script = <<< JS
$("document").ready(function(){
		var row = $i;
		$('.add_contact_button').on('click', function(e){
		    e.preventDefault();
			row++;
			var new_row_html = row_html(row);
			$('div#submission_Contacts').append(new_row_html);
			return false;
			
		});
		
		$('body').on('click', '.remove_contact_button', function(e){
		    e.preventDefault();
			$('div#'+$(this).attr('data-for')).remove();
		});

		var row_html = function(rw){
            var this_html = '<div style="padding-bottom:20px;" id="add_contact_row_'+rw+'" class="row"><div class="col no-padding-left">' +
             '<input  class="form-control" type="text"  name="CoreSchoolClass[grading]['+rw+'][min]" >' +
              '</div><div class="col no-padding-left">' +
              
              '<input  class="form-control" type="text"  name="CoreSchoolClass[grading]['+rw+'][grades]">' +
              '</div><div class="col-auto no-padding-left">' +
              '<span class="remove_contact_button" style="font-size:27px;" data-for="add_contact_row_'+rw+'"><i class="fas fa-minus-circle text-danger"></i></span>' +
              '</div></div>';

			return this_html;
		}
	});
JS;
$this->registerJs($script);
?>


<?php

use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $processSummary array */
/* @var $fromClass \app\models\InstitutionStudentClasses */
/* @var $toClass \app\models\InstitutionStudentClasses */

$this->title = 'Promotion Studio';
$this->params['breadcrumbs'][] = $this->title;

$isSchoolUser = Yii::$app->user->can('schoolpay_admin');
$isSchoolpayAdmin = Yii::$app->user->can('schoolpay_admin');
?>
<div class="letter" data-title='Promotion Studio'>
    <div class="row">

            <h4>Promotion results for school <?= $school->school_name ?></h4>
            <?php if(isset($processError)) : ?>
            <div class="alert alert-danger">
                <?= $processError ?> <i class="fa fa-exclamation-circle"></i>
            </div>
            <?php else: ?>
                <div class="alert alert-success">
                    Class promotion has been completed successfully, check the results below <i class="fa fa-check-square"></i>
                </div>
            <?php endif; ?>

            <div id="w1" class="grid-view">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th>From Class</th>
                        <th>To Class</th>
                        <th>Details</th>
                    </tr>
                    </thead>
                    <tbody id="promotion_content_body">
                    <?php foreach ($processSummary as $summary) : ?>
                    <?php
                        $fromClass = $summary['fromClass'];
                        $toClass = $summary['toClass'];
                        $toArchive = isset($summary['toArchive']) ? $summary['toArchive'] : false;
                        $summaryText = $toArchive ?  $summary['numberOfStudents'] . ' students archived' : $summary['numberOfStudents'] . ' students promoted / moved';
                        ?>
                        <tr>
                            <td><?= $fromClass->class_code . ' - ' .$fromClass->class_description ?></td>
                            <td><?= $toArchive ? ' - ' : $toClass->class_code . ' - ' .$toClass->class_description ?></td>
                            <td style="color: #00a65a"><?= $summaryText ?> <i class="fa fa-check-circle-o"></i></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>

</div>

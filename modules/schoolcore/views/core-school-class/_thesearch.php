<?php

use app\models\SchoolInformation;
use app\modules\sacco\models\LoanProviders;
use app\modules\sacco\models\Member;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\sacco\models\LoanRequestsSearch */
/* @var $form yii\widgets\ActiveForm */
$request = Yii::$app->request;
$params = $request->queryParams;
if(isset($mod) && $mod) $params['mod'] = $mod;
$action = array_merge(['/'.$request->pathInfo], $params);
?>

<div class="sacco-loan-requests-search">

    <?php $form = ActiveForm::begin([
        'action' => $action,
        'method' => 'get',
        'options' => ['class'=>'formprocess']
    ]); ?>

    <div class="col-sm-12 no-padding">
            <?php
            $initOptions = ['type'=>'string', 'label'=>" "];
                foreach($cols as $k=>$v){
                    $v = array_merge($initOptions, $v);
                    $w = isset($v['width']) ? $v['width'] : 3;
                    echo "<div class='col-sm-$w no-padding'>";
                    switch ($v['type']) {
                        case 'number':
                            echo $form->field($model, $k)->textInput([ 'class'=>'form-control input-sm', 'type'=>'number', 'placeholder'=> $v['label']])->label(false);
                            break;
                        case 'date':
                            echo $form->field($model, $k)->widget(DatePicker::className(),
                                [
                                    'clientOptions' =>[
                                        'changeMonth'=> true,
                                        'changeYear'=> true,
                                        'yearRange'=>'1900:'.(date('Y')+1),
                                        'autoSize'=>true,
                                    ],
                                    'options'=>[
                                        'class'=>'form-control input-sm',
                                        'placeholder'=>$v['label']
                                    ],
                                ])->label(false);
                            break;
                        case 'dropdown':
                            $select_values = isset($v['options']) ? $v['options'] : [''=>'Nothing to select'];
                            $input_options = isset($v['inputOptions']) ? $v['inputOptions'] : [];
                            echo $form->field($model, $k, ['inputOptions'=>['class'=>'form-control input-sm']])->dropDownList($select_values, $input_options)->label(false);
                            break;
                        case 'search':
                            $url = '';
                            $selected = '';
                            $mode = isset($v['mode']) ? $v['mode'] : '';
                            if($mode=='sacco_member'){
                                $url = Url::to(['/sacco/default/find-members']);
                                $selected = empty($model->{$k}) ? '' : Member::findOne($model->{$k})->name;
                            } else if($mode=='school'){
                                $url = Url::to(['/schoolcore/core-school/schoollist']);
                                $selected = empty($model->{$k}) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->{$k})->school_name;
                            } else if($mode=='provider'){
                                $url = Url::to(['/sacco/default/find-providers']);
                                $selected = empty($model->{$k}) ? '' : LoanProviders::findOne($model->{$k})->name;
                            } else{
                                continue;
                            }

                            echo $form->field($model, $k)->widget(Select2::classname(), [
                                'initValueText' => $selected, // set the initial display text
                                'theme'=>Select2::THEME_BOOTSTRAP,
                                'size'=>'sm',
                                'options' => [
                                    'placeholder' => $v['label'],
                                    'id'=>'search_'.$k,
                                    'class'=>'form-control',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                ],
                            ])->label(false);
                            break;
                        default:
                            echo $form->field($model, $k)->textInput([ 'class'=>'form-control input-sm', 'placeholder'=> $v['label']])->label(false);
                        break;
                }
                echo "</div>";
            } ?>

        <div class="col-xs-1 no-padding">
            <?= Html::submitButton("<i class='fa fa-search'></i>", ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

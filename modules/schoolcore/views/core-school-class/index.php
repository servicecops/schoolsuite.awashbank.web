<?php

use kartik\export\ExportMenu;
use yii\bootstrap4\LinkPager;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Class List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">


    <?php


    $columns = [



        'class_code',
        [
            'label' => 'Class Name',
            'value' => function ($model) {
                return Html::a($model['class_description'], ['core-school-class/view', 'id' => $model['id']], ['class' => 'aclink']);
            },
            'format' => 'html',
        ],
        [
            'label' => 'School Name',
            'value' => function ($model) {
                return $model->schoolName->school_name;
            },
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa fa-edit"></i>', ['core-school-class/update', 'id' => $model['id']]);
            },
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {

                return Html::a('<i class="fa  fa-eye"></i>', ['core-school-class/view', 'id' => $model['id']], ['class' => 'aclink']);

            },
        ]


    ];
    ?>

</div>


<div class="letters">
    <div class="row" >

        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-md-5 "><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
        <div class="col-md-4 ">
            <div class="pull-right">
            <?php if (Yii::$app->user->can('rw_sch')) : ?>
                <?= Html::a('Add Class <i class="fa far fa-plus"></i>', ['create'], ['class' => 'btn btn-md btn-primary  float-right']) ?>
            <?php endif; ?>

            <?php
            echo Html::a('<i class="fa far fa-file-pdf-o"></i> Pdf', ['core-school-class/export-to-pdf', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-md btn-danger',
                'target' => '_blank',
                'data-toggle' => 'tooltip',
                'title' => 'Will open the generated PDF file in a new window'
            ]);
            echo Html::a('<i class="fa far fa-file-excel-o"></i> Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-md btn-success',
                'target' => '_blank'
            ]);
            ?>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class='clink'><?= $sort->link('class_code') ?></th>
                        <th class='clink'><?= $sort->link('class_description') ?></th>
                        <th class='clink'><?= $sort->link('school_name') ?></th>


                        <th class='clink'></th>
                        <th class='clink'>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td class="clink"><?= ($v['class_code']) ? '<a href="' . Url::to(['core-school-class/view', 'id' => $v['id']]) . '">' . $v['class_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['class_description']) ? $v['class_description'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>

                                <td colspan="2">
                                    <?= Html::a('<i class="fa fa-edit"></i>', ['core-school-class/update', 'id' => $v['id']]); ?>
                                    <?= Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-school-class/view', 'id' => $v['id']], ['class' => 'aclink']) ?>

                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No Classes found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ]); ?>

        </div>
    </div>

</div>

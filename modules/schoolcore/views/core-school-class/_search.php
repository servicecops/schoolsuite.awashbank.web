<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
?>



        <div class="row text-center">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'options'=>['class'=>'formprocess'],
            ]); ?>

            <div class="row mt-12">


                <div class="col">
                    <?= $form->field($model, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control'],])->textInput(['title' => 'Enter Class name or Class code',

                        'data-toggle' => 'tooltip',

                        'data-trigger' => 'hover',

                        'data-placement' => 'bottom'])->label(false) ?>

                </div>
                <div>

                    <?= Html::submitButton('Search <i class=" fa fa-search"></i>', ['class' => 'btn btn-info btn-md']) ?></div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>

<?php
$url = Url::to(['core_school_class/lists']);
$cls = $model->class_id;


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

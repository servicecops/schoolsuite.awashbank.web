<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Class';
$this->params['breadcrumbs'][] = ['label' => 'Core School Class', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
<div class="col-md-12">
    <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
</div>
<div class="col-md-12">
    <?= $this->render('_create_class_form', [
        'model' => $model,
    ]) ?>
</div>
</div>

<?php

use yii\base\DynamicModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */

$this->title = 'Promotion Studio';
$this->params['breadcrumbs'][] = $this->title;

$isSchoolUser = \app\components\ToWords::isSchoolUser();
$isSchoolpayAdmin = Yii::$app->user->can('schoolpay_admin');
?>
<div style="color:#F7F7F7">Student Promotion</div>

<div class="letters" data-title='Promotion Studio'>
    <div class="row">


        <div class="col-md-12">
            <div class="alert alert-info">
                <i class="fa fa-exclamation-triangle"></i>Pay close attention while using this utility <br>
                <i class="fa fa-arrow-circle-down"></i> It is advisable to change classes starting from the higher to
                lower classes, in descending order. <br>
                <i class="fa fa-exclamation-circle"></i> Wrong use of this utility will lead to serious data mix-up. If
                you are unsure of how to use this utility, please contact support for assistance
            </div>


            <div class="form-group col-md-12 col-sm-6 col-lg-12 no-padding">
                <?php if ($isSchoolpayAdmin) : ?>
                    <div class="col-md-4 col-sm-12">
                        <?php
                        $model = new DynamicModel(['schsearch']);
                        $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                            'options' => ['class' => 'formprocess'],
                        ]);
                        $model = new DynamicModel(['schsearch']);
                        ?>
                        <?= $this->render('/core-school-class/school_search',
                            ['form' => $form,
                                'model' => $model,
                                'modelAttribute' => 'schsearch',
                                'label' => false,
                                'size' => Select2::SMALL,
                                'inputId' => 'promotion_selected_school_id', //ID of component
                                'placeHolder' => 'Filter School',
                                'label' => 'Select School'
                            ]
                        ); ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php endif; ?>

                <div class="col-md-6 col-sm-12">
                    <br><input type="checkbox" value="on" id="prevent_collision" checked> Smart Mode - <i
                            class="fa fa-lightbulb-o"></i> Select this for smart promotion assistance
                </div>

            </div>

            <?php
            $form = ActiveForm::begin([
                'action' => ['promotion-studio'],
                'method' => 'post',
                'options' => ['class' => 'formprocess', 'id' => 'promotion_form'],
            ]);

            ?>
            <div id="w1" class="grid-view">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th>From Class</th>
                        <th></th>
                        <th>To Class</th>
                    </tr>
                    </thead>
                    <tbody id="promotion_content_body">
                    </tbody>
                </table>

                <div class="form-group col-md-12 col-sm-6 col-lg-12 no-padding">
                    <div class="col-xs-6">
                        <?= Html::button('Execute Now', ['class' => 'btn btn-block btn-success', 'id' => 'promote-button']) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::resetButton('Reset & Start Over', ['class' => 'btn btn-sm btn-danger ']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>

<?php
$url = Url::to(['core-school-class/json-list']);
$formActionBase = Url::to('promotion-studio');

$school = $isSchoolUser ? Yii::$app->user->identity->school_id : null; //TODO if school user, infer from user
//$school = 15841;
$script = <<< JS

$("document").ready(function(){
    var classList = null; //Holder for class list
    var schoolSelector = $("#promotion_selected_school_id");
    var recordBody = $("#promotion_content_body");

    var numberOfClasses = 0;
    let body = $('body');
    

    let buildSelectCombo = function(parsedClassList) {
        var html = "";
        for(var i=0; i<parsedClassList.length; i++) {
            let zClass = parsedClassList[i];
            html += "<option value='" + zClass.id + "'>" +zClass.class_code + " - " + zClass.description + "</option>"
        }
        return html;
    }
    //This will build the stage and populate the records


    let buildStage = function() {
        if(!classList) return;

        var innerHtml = '';
        //Parse json
        var parsedClassList = JSON.parse(classList) || [];

        numberOfClasses = parsedClassList.length; //Update the number of classes

        let selectCombo = buildSelectCombo(parsedClassList);

        for(var i=0; i<numberOfClasses; i++) {
           innerHtml += "<tr>" +
            "<td>" +
             "<select class='form-control source_classes' name='SourceClass["+i+"]' rowId='"+i+"'>" +
              "<option value=''>--Select Source Class --</option>" +
             selectCombo +
              "</select>" +
             "</td>" +

            "<td><i class='fa fa-arrow-right'><i/></td>" + //Middle column

            "<td>" +
             "<select class='form-control destination_classes' name='DestinationClass["+i+"]' rowId='"+i+"'>" +
              "<option value=''>--Select Destination Class --</option>" +
              "<option value='___ARCHIVE___'>!!!Archive Students!!!</option>" +
             selectCombo +
              "</select>" +
             "</td>"
        }
        recordBody.html(innerHtml);

    }

    let fetchClassList = function(schoolId) {
        recordBody.html('<tr><td colspan=3>Loading...please wait</td></tr>'); //Temporary loading indicator
        $.get('$url', {id : schoolId}, function( data ) {
            classList = data;
            buildStage();
            //Adjust the form action
            $('#promotion_form').attr('action', '$formActionBase?schoolId=' + schoolId)
        });
    }

    let thisSourceSelected = function(selector) {
        //If not value, do nothing
        let rowId = selector.attr('rowId');
        let selectedClassId = selector.val();

        //In smart mode, the source selected automatically becomes the destination on the next row
        let smartMode = $('#prevent_collision').is(':checked');
        if(smartMode) {
            //This destination selected option should be autoselected on next row
            let nextRowId = new Number(rowId) + 1;
             let nextDestinationSelector = $('.destination_classes').filter('[rowId="'+nextRowId+'"]');
             if(nextDestinationSelector) {
                 nextDestinationSelector.val(selectedClassId).change()
             }
        }

        //Now update other source selectors
        //This will remove this select items in the subsequent selects
        if(selectedClassId) updateOtherSourceSelectorOptions(rowId, selectedClassId);

    }

    //Given a selected row id and a selected class id
    //Will update other selectors options, to exclude this row id
    let updateOtherSourceSelectorOptions = function(selectedRowId, selectedClassId) {
        $('.source_classes')
        .each(function(index) {
            let selectedSelectElement = $(this);
            let thisRowId = selectedSelectElement.attr('rowId');
            //We will work on all except the selected one
            if(thisRowId === selectedRowId)
                return;

            let optionToRemove = selectedSelectElement.find('[value=\"'+selectedClassId+'\"]')
            if(optionToRemove) optionToRemove.remove();
        })
    }
    
    
    
    let thisDestinationSelected = function(selector) {
        //If not value, do nothing
        let rowId = selector.attr('rowId');
        let selectedClassId = selector.val();
        
        let destinationClasses = $('.destination_classes');
        //Clear errors first
        destinationClasses.removeClass('has-errors');
        
         //If nothing selected, do nothing, just return
        if(!selectedClassId) return;

        
        //In smart mode, highlight duplicate destinations
        let smartMode = $('#prevent_collision').is(':checked');
        if(smartMode) {
            var dropDownsWithThisValue = [];
        
        //Iterate all, if value is selectedClass, add to array
        destinationClasses.each(function(index) {
            let _thisDropDown = $(this);
            if(_thisDropDown.val() === selectedClassId)
                dropDownsWithThisValue.push(_thisDropDown);
        })
        
        //Now if the compiled list has more than one item, highlight them
        if(dropDownsWithThisValue.length > 1) {
            for(var i=0; i<dropDownsWithThisValue.length; i++) {
                dropDownsWithThisValue[i].addClass('has-errors');
            }
        }
        }
    }

    //Hook up change event
    body.on('change', '#promotion_selected_school_id', function(){
        fetchClassList($(this).val());
    });

    //Change event for source selectors
   body.on('change', '.source_classes', function(){
        thisSourceSelected($(this));
    });
    
    //Change event for source selectors
    body.on('change', '.destination_classes', function(){
        thisDestinationSelected($(this));
    });


    body.on('click', '.reset_button', function(){
        buildStage();
    });

   $('#promote-button').on('click', function (e) {
    	var conf = confirm("Are you sure you want to submit the promotion request. Make sure you have confirmed all records before continuing.");
    	if(conf) {
    	    $('#promotion_form').submit();
    	    return true;
    	}
        return false;
    });




    //By default fetch classes and set stage
    var sch = '$school' ? '$school' : schoolSelector.val();
    if(sch){
        fetchClassList(sch);
    }

  });
JS;
$this->registerJs($script);
?>

<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\bootstrap4\ActiveForm */


$disableControls = false;
if(!$model->isNewRecord ) {
    $disableControls = true;
}
?>


<div class="formz">


    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div class="row">

        <?php if (\app\components\ToWords::isSchoolUser()) : ?>
        <?php if($model->isNewRecord) : ?>
            <div class="col-sm-6">
                <?php
                $url = Url::to(['core-school-class/classlist','id' => Yii::$app->user->identity->school_id]);
                $selectedClass = empty($model->class_id) ? '' : CoreSchoolClass::findOne($model->class_id)->class_description;
                echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedClass, // set the initial display text
                    'theme'=>Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Select Class',
                        'id'=>'school_id',
                        'multiple'=>true,
                        'class'=>'form-control',

                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                    ],
                ])->label('Class *'); ?>
            </div>


        <?php endif; ?>
        <?php endif; ?>

        <?php if(!$model->isNewRecord) :
            ?>
                <div class="col-sm-6">

                    <h2>Editing Grades for <?= ($model->class_code) ? $model->class_code : "--" ?></h2>
                </div>

        <?php endif; ?>



    </div>
    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <br>
        <h4>Class Grades <span style="font-weight: 300; font-size:18px;"> Final grades for your class  ( <span style="font-size: 14px;">click on the + button to add more grades</span> )</span>
        </h4>
        <hr class="l_header">

    </div>
    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <?= $this->render('_grades', ['model' => $model]); ?>

    </div>



    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

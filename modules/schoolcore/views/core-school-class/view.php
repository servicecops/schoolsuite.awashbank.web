<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->class_description;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$pg = $pages['pages'];
?>
<p style="color:#F7F7F7">Student Class</p>
<div style="margin-top:20px"></div>
<div class="letter">


    <div class="row" style="margin-top:20px">

        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i>Class Details for <?php echo $this->title ?></span>
        </div>
    </div>
<br/>

    <div class="row">

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('school_id') ?></div>
                <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>
                    <div class="col-md-6 profile-text"><?= ($model->school_id) ? Html::a($model->institution0->school_name, ['core-school/view', 'id' => $model->institution0->id]) : "--" ?></div>
                <?php else : ?>
                    <div class="col-md-6 profile-text"><?= ($model->school_id) ? $model->institution0->school_name : "--" ?></div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('class_description') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->class_description) ? $model->class_description : "--" ?></div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('class_code') ?></div>
                <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->school_id) ? $model->class_code : "--" ?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;"><b>Total Students</b></div>
                <div class="col-lg-6 col-xs-9 profile-text"><b><?= $pg->totalCount; ?></b></div>
            </div>
        </div>
    </div>

    <br>
    <br/>
    <?php if (\Yii::$app->user->can('rw_class'))  : ?>

    <div class="row">

        <div class="col-sm-3 padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i>&nbsp;&nbsp;List of Class Students </span>
        </div>
        <div class="col-sm-5 padding">
            <?php $form = ActiveForm::begin([
                'action' => ['/schoolcore/core-school-class/view', 'id' => $model->id],
                'method' => 'get',
                'options' => ['class' => 'formprocess']
            ]); ?>
            <div class="col-md-6 padding">
                <?= $form->field($searchModel, 'modelSearch', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Find Student (Payment Code / Student Name)']])->textInput()->label(false) ?>
            </div>

            <div class="col-md-6 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm']) ?></div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4 no-padding">
            <div class="pull-right">
                <?= Html::a('<i class="fa fa-plus"></i>&nbsp;&nbsp; Add New Class', ['/schoolcore/core-school-class/create'], ['class' => 'btn  btn-info btn-sm']) ?>
                <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp; Edit Class', ['update', 'id' => $model->id], ['class' => 'btn  btn-info btn-sm']) ?>

                <?php if ($pg->totalCount == 0) : ?>
                    <?= Html::a('<i class="fa fa-remove"></i> Delete Class', ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-danger', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to delete this class"]) ?>
                <?php endif; ?>
            </div>
        </div>


    </div>

<br/>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><?= $sort->link('student_code') ?></th>
                        <th><?= $sort->link('first_name') ?></th>
                        <th><?= $sort->link('last_name') ?></th>
                        <th><?= $sort->link('middle_name') ?></th>
                        <th><?= $sort->link('student_email') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td onclick="clink('<?= Url::to(['/schoolcore/core-student/view', 'id' => $v['id']]) ?>')"><?= ($v['student_code']) ? '<a href="javascript:;">' . $v['student_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set">- </span>' ?></td>
                                <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set">- </span>' ?></td>
                                <td><?= ($v['middle_name']) ? $v['middle_name'] : '<span class="not-set">- </span>' ?></td>
                                <td><?= ($v['student_email']) ? $v['student_email'] : '<span class="not-set">- </span>' ?></td>
                                <td><a href="javascript:"
                                       onclick="clink('<?= Url::to(['/schoolcore/core-student/view', 'id' => $v['id']]) ?>')"
                                       title="View" data-pjax="0"><span class="fa fa-search"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="javascript:"
                                       onclick="clink('<?= Url::to(['/schoolcore/core-student/update', 'id' => $v['id']]) ?>')"
                                       title="Update" data-pjax="0"><span class="glyphicon glyphicon-edit"></span></a>
                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No student found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last'
            ]); ?>

        </div>
    </div>
    <?php endif;?>
</div>

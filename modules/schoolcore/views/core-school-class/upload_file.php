<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SchoolInformation;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Classes;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>
<p style="color:#F7F7F7">Import Class</p>

<div class="row hd-title" data-title="Upload Excel">
    <div class="col-xs-12 col-lg-12 no-padding">
        <table class="table" style="background-color:#fff; margin-bottom:3px;">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <tr>

                <th>            <?= $form->field($model, 'importFile', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'File']])->fileInput()->label('Upload File *') ?>
                </th>
                <th>            <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'Description']])->textInput()->label('Description *') ?>
                </th>
                <th>

                    <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>
                        <?php
                        $url = Url::to(['core-school/active-schoollist']);
                        $selectedSchool = empty($model->school) ? '' : CoreSchool::findOne($model->school)->school_name;
                        echo $form->field($model, 'school')->widget(Select2::classname(), [
                            'initValueText' => $selectedSchool, // set the initial display text
                            'size' => 'sm',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => 'Filter School',
                                'id' => 'school_search',
                                'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                        $( "select#dynamicmodel-student_class" ).html(data);
                    });'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],

                            ],
                        ])->label('School Name *'); ?>

                    <?php endif; ?>
                </th>

                <th>
                    <button class="btn btn-block btn-primary btn-sm">Upload</button>
                </th>
            </tr>
            <?php ActiveForm::end(); ?>
        </table>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">
            <div class="col-md-12 no-padding">
                <p style="font-size:15px;color:grey;"><b>Note:</b> Only excel data sheets that follow the <b>template
                        format provided by schoolERP</b> will be allowed.
                    <b>**And follow column Order !important**</b> <a
                            href="<?= \Yii::getAlias('@web/uploads/schsuite_class_data_import_template.xlsx'); ?>"
                            target="_blank">download template</a></p>
                <table class="table table-striped" style="font-size:12px;">
                    <thead>
                    <tr>
                        <th>Class Description</th>
                        <th>Class Code</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Primary One South</td>
                        <th>P1S
                        </td></tr>
                    <tr>
                        <td>Required</td>
                        <td>Required</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

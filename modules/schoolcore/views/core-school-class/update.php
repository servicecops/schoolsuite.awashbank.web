<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchoolClass */

$this->title = 'Update Class: ' . $model->class_description;
$this->params['breadcrumbs'][] = ['label' => 'Core School Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row card border-0">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create_class_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStaff;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression; ?>

<div class="formz">
    <p>All fields marked with * are required</p>

    <div class="row">

    <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>
    <?php if (!\app\components\ToWords::isSchoolUser()): ?>


        <div class="col-md-12">
            <?php
            $url = Url::to(['core-school/active-schoollist']);
            $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id' => 'school_search',
                    'class' => 'form-control',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ])->label('School Name') ->label('School Name *') ; ?>

        </div>

        <?php endif; ?>


        <div class="col-md-6">
            <?= $form->field($model, 'class_code', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Class Code']])->textInput()->label('')->label('Class Code *')   ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'class_description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Class Description']])->textInput()->label('')->label('Class Description *')  ?>
        </div>

        <div class="col-md-6">

            <?php if (\app\components\ToWords::isSchoolUser()) : ?>
                <?php
                $data = CoreStaff::find()->where(['school_id' => Yii::$app->user->identity->school_id, 'user_level' => 'sch_teacher'])->all();

                echo $form->field($model, 'class_teacher')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'first_name'),
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['placeholder' => 'Find Teacher'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Class Teacher'); ?>
            <?php endif; ?>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xs-6">
            <?php if ($model->isNewRecord): ?>
            <?= Html::resetButton('Reset <i class="fa fa-save"></i>', ['class' => 'btn btn-default btn-block']) ?>
            <?php endif;?>
        </div>
    </div>

<?php ActiveForm::end(); ?>
</div>



<?php
$script = <<< JS
    $(".active").removeClass('active');
    $("#classes").addClass('active');
    $("#classes_create").addClass('active');
JS;
$this->registerJs($script);
?>



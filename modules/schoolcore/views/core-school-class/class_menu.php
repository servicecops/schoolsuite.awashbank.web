<?php

use app\components\Helpers;
use yii\helpers\Url; ?>

<div class="cardz my-1 bg-light">
    <h3 class="card-header ">School classes center</h3>
    <div class="row card-body justify-content-center">
        <a class="cardz col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/schoolcore/core-school-class/index']); ?>">
            <div class="display-4  text-center"> <span>View classes</span>  <i class="fa fa-eye" aria-hidden="true"></i></div>
        </a>

        <?php if (Helpers::is('rw_class')) : ?>
            <a class="cardz col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/schoolcore/core-school-class/create']); ?>">
                <div class="display-4 d text-center"> <span>Add class</span>  <i class="fa fa-plus-circle" aria-hidden="true"></i> </div>
            </a>

            <a class="cardz col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/schoolcore/core-school-class/upload-excel']); ?>">
                <div class="display-4  text-center"> <span>Import classes</span>  <i class="fa fa-cloud-download" aria-hidden="true"></i></div>
            </a>
        <?php endif; ?>
    </div>
</div>

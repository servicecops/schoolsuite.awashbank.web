<?php 
use yii\helpers\Url;

        if($status['state']){
            echo $status['message'];
        }
        ?>
        
        <div class="row">
        <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">

        <table class="table" style="background-color:#fff; margin-bottom:3px;" >

        </table>
            <table class="table" style="background-color:#fff; margin-bottom:3px;" >
                <tr>
                    <th class="pull-right no-padding"><a class='aclink' href="<?= Url::to(['/schoolcore/core-school-class/file-details', 'id'=>$file]) ?>" data-confirm="Please confirm to save class" data-method="post" data-params='{"action":"save"}'><span class="btn btn-primary btn-sm"><b>Click to Submit class </b>&nbsp; &nbsp;</span></a></th>

                </tr>

            </table>
        <div class="box-body table table-responsive no-padding">
        <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>Sr.No</th>
            <th>Class Name</th>
            <th>Class Code</th>
            <th>School Name </th>

            <th>&nbsp;&nbsp;&nbsp;</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $srNo = 1;
            foreach($data as $k=>$v) : ?>
                <tr><td><?= $srNo; ?></td>
                    <td><?= ($v['class_name']) ? $v['class_name'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set"> -- </span>' ?></td>

                </tr>
            <?php 
            $srNo++;
            endforeach; ?>
        </tbody>
        </table>
        </div>
    </div>
    </div>
    </div>

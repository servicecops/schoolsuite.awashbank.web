<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm*/

$this->title = 'Edit Bank BRegion: ' . $model->bank_region_name;
$this->params['breadcrumbs'][] = ['label' => 'Awash Branch', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-school-class-update">

<?= $this->render('_form', [
    'model' => $model
]) ?>

</div>

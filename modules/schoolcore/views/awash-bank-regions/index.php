<?php

use yii\bootstrap4\LinkPager;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\BookCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Awash Branches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <h1><?= Html::encode($this->title) ?></h1>


    <div class="col-md-12">
         <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="col-md-12">
        <div class="pull-right">

            <?= Html::a('Add Bank Region', ['create'], ['class' => 'btn btn-success']) ?>

        </div>

        <table class="table table-striped">
            <thead>
            <?php
            $x=1;
            ?>
            <tr>


                <th class='clink'></th>
                <th class='clink'>Region Name</th>

                <th class='clink'>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">
                    <td><?php echo $x++?></td>&nbsp

                        <td><?= ($v['bank_region_name']) ? $v['bank_region_name'] : '<span class="not-set">(not set) </span>' ?></td>


                        <td>

                            <?= Html::a('<i class="fa fa-edit"></i>', ['awash-bank-regions/update', 'id' => $v['id']]); ?>
                            <?= Html::a('<i class="fa  fa-eye"></i>', ['awash-bank-regions/view', 'id' => $v['id']], ['class' => 'aclink']) ?>
                        </td>
                    </tr>
                <?php endforeach;
            else :?>
                <tr>
                    <td colspan="8">No Bank Regions found</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

        <?= LinkPager::widget([
            'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ]); ?>


    </div>

</div>

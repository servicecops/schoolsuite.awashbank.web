<?php

use app\modules\feesdue\models\FeesDue;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\payment_plan\models\PaymentPlan;
use yii\widgets\ActiveForm;

?>
<?php if ($state) {
    echo $state['message'];
} ?>

<div class="row" style="background: #01b5d2">





    <div class="col-md-12 no-padding" style="background: #efefef">
        <div class="wizard">
            <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">1</span> Edit Group Information</a>
            <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">2</span> Edit Group Members</a>

           <a  class="col-md-4 current"><span class="badge">2</span> Assign Payment Plan</a>
        </div>
    </div>
</div>

<div class="row" style="background: white;padding:10px; margin-bottom: 5px;">
    <span style="font-size: 16px; color:#3c8dbc;">Note: A fee will only be available if it's <b>APPROVED</b> and  between  it's <b>effective & end </b>date</span>
    <br><br>
    <span style="font-size:20px;"><b><?= "Group Members - " . ucwords($model->group_name) . "</b> (" . $model->school->school_name . ")" ?></span>

</div>


<?php if ($members) :

$form = ActiveForm::begin([
    'action' => ['cancel-plan', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess'],
]);
?>
<div class="row" style="background: white;padding:10px; margin-bottom: 5px;">

    <div class="col-sm-3 no-padding">
        <?= $form->field($model2, 'fee_id')->dropDownList(
            ArrayHelper::map(FeesDue::find()->where(['school_id' => $model->school_id, 'approval_status' => true])->andWhere("effective_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "'")->orderBy('date_created')->all(), 'id', 'selectDesc'), ['prompt' => 'Select Fee', 'class' => 'form-control input-sm']
        )->label(false) ?>
    </div>
    <div class="col-sm-3 no-padding">
        <?= $form->field($model2, 'payment_plan')->dropDownList(
            ArrayHelper::map(PaymentPlan::find()->where(['school_id' => $model->school_id])->all(), 'id', 'description'), ['prompt' => 'Select Plan', 'class' => 'form-control input-sm']
        )->label(false) ?>
    </div>
    <div class="col-sm-3 no-padding">
        <?= $form->field($model2, 'cancellation_reason', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Reason']])->textInput() ->label(false)?>

    </div>
    <div class="col-sm-3 no-padding">
    <?= Html::submitButton('<b>Cancel Payment Plan</b>', ['class' => 'btn btn-primary btn-sm', 'data-confirm' => 'Please confirm to cancel this fee']) ?>
    <?php ActiveForm::end(); ?>
    <?php endif; ?>
    </div>
    <div class="pull-right">
        <a class="aclink" href="<?= Url::to(['/student-group/index']) ?>">
            <button class="btn btn-default btn-sm">Back to Groups</button>
        </a>
    </div>
</div>


<div class="box-body table table-responsive no-padding">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Sr.No</th>
            <th>Payment code</th>
            <th>Student Name</th>
            <th>Class</th>
            <th>Gender</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($members) :
            $srNo = 1;
            foreach ($members as $k => $v) : ?>
                <tr>
                    <td><?= $srNo; ?></td>
                    <td class="clink"><a
                                href="<?= Url::to(['/schoolcore/core-student/view', 'id' => $v['id']]) ?>"><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set"> -- </span>' ?></a>
                    </td>
                    <td><?= $v['last_name'] . " " . $v['middle_name'] . " " . $v['first_name'] ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                </tr>
                <?php
                $srNo++;
            endforeach;
        else :
            ?>
            <td colspan="8">No Records found for the search Criteria</td>
        <?php endif; ?>
        </tbody>
    </table>
</div>

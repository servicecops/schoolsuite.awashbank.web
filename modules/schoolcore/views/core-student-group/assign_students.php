<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
// By Ibrahim Kitagednda

$this->title = 'Assign Members';
$this->params['breadcrumbs'][] = $this->title;
?>

<p style="color:#F7F7F7">Student Information</p>

<div class="row">
    <div class="row" data-title="<?= $this->title; ?>">
        <div class="col-md-12 no-padding" style="background: #efefef">
            <div class="wizard">
                <a href="<?= Url::to(['core-student-group/update', 'id' => $model->id]) ?>"
                   class="aclink col-md-4"><span class="badge">1</span> Edit Group Information</a>
                <a class="col-md-4 current"><span class="badge">2</span> Assign Students</a>
                <a class="col-md-3 gray"><span class="badge badge-inverse">3</span> Take Action</a>
            </div>
        </div>

        <div class="row" style="background: white;padding:10px">
            <span style="font-size:20px;color:#3c8dbc"><b><?= ucwords($model->group_name) . "  -  " ?> </b></span>
            <span style="font-size:18px;color:#3c8dbc"><b><?= ucwords($model->school->school_name) ?> </b></span>
            <hr class="l_header">
            <div class="col-md-12 no-padding">
                <div class="col-sm-2 col-md-12 no-padding"><h3 class="box-title" style="font-size:19px;">&nbsp;<i
                                class="fa fa-th-list"></i>&nbsp;&nbsp;Get Members</h3></div>
                <div class="col-sm-7 col-md-12 no-padding"
                     style="padding-top: 20px !important;"><?php echo $this->render('_search', ['searchModel' => $searchModel, 'model' => $model]); ?></div>
                <div class="col-sm-3 col-md-12 no-padding" style="padding-top: 20px !important;">
                    <span style="font-size:18px;border-bottom:1px dashed #000"><b>OR <a href="javascript:"
                                                                                        class="code_link">Paste Student Codes</a></b></span>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <span style="font-size:15px; ">Search and assign members then click on <b>Take Action</b> button to use the group.</span>
                <span style="font-size:15px;color:#613030;"><b> Note:</b> The search result will contain only <b>500 records</b> that are not already assigned to this group.</span>
                <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>">Back to Groups</a>
            </div>
        </div>
    </div>
    <div class="row">

            <div class="col-md-6">
                <div class="select_tbl">
                    <?php echo $this->render('select_students', ['data' => $data, 'model' => $model]); ?>
                </div>
                <!-- Payment codes -->
                <div class="codes_input" style="display:none;">
                    <span style="font-size:20px;font-weight:500px;">&nbsp;Paste student codes (Separate with spaces or commas)</span>
                    <a href="javascript:" class="back_link">Back</a>
                    <?php $form = ActiveForm::begin([
                        'action' => ['assign', 'id' => $model->id],
                        'method' => 'post',
                        'options' => ['class' => 'formprocess'],
                    ]); ?>
                    <textarea class="form-control" id="payment_codes" name="payment_codes" rows="6"
                              placeholder="Paste payment codes"></textarea>

                    <?= Html::submitButton('<b>Assign Members</b>', ['class' => 'btn btn-primary btn-sm']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
                <!--  -->
            </div>
            <!-- Group Members Table -->
            <div class="col-md-6" >
                <?php echo $this->render('group_members', ['data' => $members, 'model' => $model]); ?>
            </div>
        </div>
    </div>
</div>
<?php
$script = <<< JS

$(function () {
    
$(".code_link").click(function(){
      $(".select_tbl").css('display', 'none');
      $(".codes_input").css('display', 'block');
    });
    $(".back_link").click(function(){
      $(".select_tbl").css('display', 'block');
      $(".codes_input").css('display', 'none');
    });
    
    $("#empty-group-button").bind('click', function() {
       $("#empty-group").val('empty-group') 
    })
});





JS;
$this->registerJs($script);
?>

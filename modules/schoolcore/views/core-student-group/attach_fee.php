<?php

use app\modules\feesdue\models\FeesDue;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>
<?php

//$fees = FeesDue::find()->where(['school_id' => $model->school_id, 'approval_status' => true, 'recurrent' => false])
//    ->andWhere("end_date >= '" . date("Y-m-d") . "'")
//    ->andWhere("recurrent = false")
//    ->orderBy('date_created')
//    ->all();

if ($state) {
    echo $state['message'];
} ?>

<div class="col-md-12 no-padding" style="background: #01b5d2">
    <div class="wizard">
        <a href="<?= Url::to(['core-student-group/update', 'id' => $model->id]) ?>" class="aclink col-md-4"><span
                    class="badge">1</span> Edit Group Information</a>
        <a href="<?= Url::to(['core-student-group/assign', 'id' => $model->id]) ?>" class="aclink col-md-4"><span
                    class="badge">2</span> Edit Group Members</a>
        <a class="col-md-3 current"><span class="badge badge-inverse">3</span> Assign Fee</a>
    </div>
</div>

<div class="col-md-12" style="background: white;padding:10px; margin-bottom: 5px;">
    <div class="row">
        <div class="col-md-12">
            <span style="font-size: 16px; color:#3c8dbc;">Note: A fee will only be available if it's <b>APPROVED</b> and  between  it's <b>effective & end </b>date</span>
            <br><br>
        </div>
        <div class="col-md-12 no-pading">
            <span style="font-size:20px;"><b><?= "Group Members - " . ucwords($model->group_name) . "</b> (" . $model->school->school_name . ")" ?></span>
            <?php if ($members) :

            $form = ActiveForm::begin([
                'action' => ['assign-fee', 'id' => $model->id],
                'method' => 'post',
                'options' => ['class' => 'formprocess', 'id' => 'assign_fee_student_form_modal'],
                'fieldConfig' => [
                    'template' => "{label}{input}{error}",
                ],
            ]);
            ?>
            <div class="col-md-12">

                <div class="col-md-3 no-padding">
                    <?= $form->field($model2, 'fee_id')->dropDownList(
                        ArrayHelper::map($fees, 'id', 'selectDesc'), ['prompt' => 'Select Fee', 'class' => 'form-control input-sm',
                            'id' => 'feediv'
                        ]
                    )->label(false) ?>
                </div>
                <div class="col-md-3 no-padding" id='credithrsdiv'>
                    <?= $form->field($model2, 'credit_hours', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Credit Hours', 'id' => 'credithrs']])->textInput()->label(false) ?>

                </div>


                <?= Html::submitButton('<b>Assign A Fee</b>', ['class' => 'btn btn-primary ', 'data-confirm' => 'Please confirm to apply this fee']) ?>
                <?php ActiveForm::end(); ?>
                <?php endif; ?>
                <div class="pull-right">
                    <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>">
                        <button class="btn btn-default btn-sm">Back to Groups</button>
                    </a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6  alert alert-danger invalid-accounts-error" style="display: none">
                    Please enter credit hours to continue
                </div>
            </div>
        </div>

    </div>

</div>

<?= $this->render('_action_group_members',
    [
        'members' => $members,
    ]);
?>

<?php
$url = Url::to(['/feesdue/fees-due/credithr-fee']);

$script = <<< JS
    $(document).ready(function(){
    //    alert("tyeus");
       var isCrdit=false;
       
          $('#credithrsdiv').hide();
          
          
        $('form#assign_fee_student_form_modal').yiiActiveForm('validate');
 
             $('#feediv').change(function(){
                 var feeId =$(this).val();
              
             $.get('$url?id='+feeId, function(data ) {
                 
                // 
                 console.log(JSON.stringify(data))
    
                if(data === '1' || data==='true'|| data === true){
                    isCrdit = true;
                      $('#credithrsdiv').show();
                } else{
                      $('#credithrsdiv').hide();
                }
                
                
                console.log("the fee id" +  feeId)
                });
        });
//             
             
        let schoolForm = $('#assign_fee_student_form_modal');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.form-control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val() && isCrdit === true ) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });     
   
   });
JS;
$this->registerJs($script);
?>

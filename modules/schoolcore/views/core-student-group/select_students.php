<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<br/>
<?php
$form = ActiveForm::begin([
    'action' => ['assign', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess'],
]); ?>
    <div class="row">
        <div class="col-md-12 no-pading">
            <span style="font-size:20px;font-weight:500px;">&nbsp;Select New Members</span>
            <?php if ($data) : ?>
                <div class="pull-right">
                    <?= Html::submitButton('<b>Assign Members</b>', ['class' => 'btn btn-primary btn-sm generate-btn', 'data-confirm' => 'Confirm to add students']) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <fieldset>
        <div class="box-body table table-responsive ">
            <table class="table">
                <thead>
                <tr style="background: white;padding:10px">
                    <th style="padding:0 15px;"><span class="checkbox checkbox-info checkbox-circle"
                                                      style="margin:6px;"><input type="checkbox" class="checkall"
                                                                                 onclick="checkall()"
                                                                                 id="check_all_id1"><label
                                    for="check_all_id1">Select All</label></span></th>
                    <th>Payment code</th>
                    <th>Student Name</th>
                    <th>Class</th>
                    <th>Gender</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($data) :
                    $srNo = 1;
                    foreach ($data as $k => $v) : ?>
                        <?php
                        $studentName = $v['first_name'] ? $v['first_name'] : "";
                        $studentName .= $v['middle_name'] ? " ".$v['middle_name'] : "";
                        $studentName .= $v['last_name'] ? " ".$v['last_name'] : "";
                        ?>
                        <tr>
                            <td style="padding:0 15px;">
                                <span class="checkbox checkbox-info checkbox-circle"
                                      style="margin:6px;">
                                    <input type="checkbox"
                                           class="checkbox"
                                           name="selected_stu[]"
                                           id="<?= $v['id']; ?>"
                                           value="<?= $v['id'] ?>">
                                    <label class='col-md-12 no-padding' for="<?= $v['id']; ?>"></label>
                                </span>
                            </td>
                            <td><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= $studentName ?></td>
                            <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                        </tr>
                        <?php
                        $srNo++;
                    endforeach;
                else :
                    ?>
                    <td colspan="8">No Records found for the search Criteria</td>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </fieldset>
<?php ActiveForm::end(); ?>

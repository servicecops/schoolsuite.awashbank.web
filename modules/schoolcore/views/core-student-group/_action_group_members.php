<?php

?>
<div class="box-body table table-responsive no-padding">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Sr.No</th>
            <th>Payment code</th>
            <th>Student Name</th>
            <th>Class</th>
            <th>Gender</th>
        </tr>
        </thead>
        <tbody>
        <?php

        use yii\helpers\Url;

        if ($members) :
            $srNo = 1;
            foreach ($members as $k => $v) : ?>
                <?php
                $studentName = $v['first_name'] ? $v['first_name'] : "";
                $studentName .= $v['middle_name'] ? " ".$v['middle_name'] : "";
                $studentName .= $v['last_name'] ? " ".$v['last_name'] : "";
                ?>
                <tr>
                    <td><?= $srNo; ?></td>
                    <td class="clink"><a
                            href="<?= Url::to(['core-student/view', 'id' => $v['id']]) ?>"><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set"> -- </span>' ?></a>
                    </td>
                    <td><?= $studentName ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                </tr>
                <?php
                $srNo++;
            endforeach;
        else :
            ?>
            <td colspan="8">No Records found for the search Criteria</td>
        <?php endif; ?>
        </tbody>
    </table>
</div>

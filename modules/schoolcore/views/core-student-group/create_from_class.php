<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = 'Add Class Group';
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p>GROUPING</p>
<div class="letter">


    <div class="col-md-12 no-padding" style="background: #efefef">
        <div class="wizard">
            <a class="current col-md-4"><span class="badge">1</span> Create Class Group</a>
            <a class="col-md-4 gray"><span class="badge">2</span> Assign Students</a>
            <a class="col-md-3 gray"><span class="badge badge-inverse">3</span> Take Action</a>
        </div>
    </div>

    <div class='col-md-12'>

        <div class="col-md-12"><h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3>
        </div>
        <p>&nbsp;</p>
        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'options' => ['class' => 'formprocess'],
        ]); ?>


        <?php if (Yii::$app->user->can('schoolpay_admin')): ?>
            <div class="col-md-12 ">
                <?php
                $url = Url::to(['core-school/active-schoollist']);
                $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'size' => 'sm',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Assign School',
                        'id' => 'group_school_search',
                        'onchange' => '$.post( "' . Url::to(['core-school-class/lists/']) . '?id="+$(this).val(), function( data ) {
                                            $( "select#group_class_select" ).html(data);
                                        });',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Select School *'); ?>
            </div>

            <div class="col-md-12 ">
                <?= $form->field($model, 'classes')->widget(Select2::classname(), [
                    'data' => [],
                    'initValueText' => $selectedVal,
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'size' => 'sm',
                    'options' => [
                        'id' => 'group_class_select',
                        'multiple' => true,
                        'placeholder' => 'Find Class'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label("Choose Classes *"); ?>
            </div>
        <?php endif; ?>

        <?php if (\app\components\ToWords::isSchoolUser()) : ?>
            <div class="col-md-6 col-sm-4 no-padding-right">
                <?php
                $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();


                echo $form->field($model, 'classes')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'class_code'),
                    'initValueText' => $selectedVal,
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Find Class',
                        'id' => 'group_class_select',
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Choose Classes *'); ?>
            </div>
        <?php endif; ?>


        <div class="col-md-12 no-padding">
            <div class="col-md-6 col-sm-4 no-padding-right">
                <?= $form->field($model, 'group_name', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Enter Group Name']])->textInput(['maxlength' => true])->label('Group Name *') ?>
            </div>


            <div class="col-md-6 col-sm-4 no-padding-right">
                <?= $form->field($model, 'group_description', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Description']])->textInput(['maxlength' => true])->label('Group Description *') ?>
            </div>
        </div>
        <div class="col-md-12 col-sm-2">
            <div class="checkbox checkbox-info">
                <input type="checkbox" id="studentgroupinformation-active" name="StudentGroupInformation[active]"
                       value="1" checked>
                <label for="studentgroupinformation-active">Active</label>
            </div>
        </div>
        <div class="col-md-12 no-padding">
            <div class="col-md-4 no-padding-right">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Add Class Group' : 'Edit Group', ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-info btn-sm']) ?>
                </div>
            </div>
        </div>


        <?php ActiveForm::end(); ?>


    </div>
    <div style="clear: both"></div>
</div>

<?php
$url = Url::to(['/core-school-class/lists']);
$classes = $model->classes ? $model->classes : [];
$script = <<< JS
    $("document").ready(function(){ 
       var sch = $("#group_school_search").val();
        if(sch){
            $.post( '$url/'+sch, function( data ) {
                $( "select#group_class_select" ).html(data);
            })
        }
      
    });
JS;
$this->registerJs($script);
?>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<?php if($state){
    echo $state['message'];
}?>

<div class="col-md-12 " style="background: #01b5d2;margin-bottom: 20px">
    <div class="wizard">
        <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">1</span> Edit Group Information</a>
        <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">2</span> Edit Group Members</a>
        <a  class="col-md-3 current"><span class="badge badge-inverse">3</span> Delete Students</a>
    </div>
</div>
<div style="margin:20px"></div>
<div class="row" style="background: white;padding:10px; margin-bottom: 5px;">

<!--        <div class="col-md-12">-->
<!--            <span style="font-size: 16px; color:#3c8dbc;">Note: A fee will only be available if it's <b>APPROVED</b> and  between  it's <b>effective & end </b>date</span>-->
<!--            <br><br>-->
<!--        </div>-->
        <div class="col-md-12">
            <span style="font-size:20px;"><b><?= "Group Members - ".ucwords($model->group_name)."</b> (".$model->school->school_name.")" ?></span>
            <?php if($members) :

            $form = ActiveForm::begin([
                'action' => ['remove-students', 'id'=>$model->id],
                'method' => 'post',
                'options'=>['class'=>'formprocess'],
            ]);
            ?>
            <div>
                <div class="col-md-3 ">
                    <?= $form->field($model2, 'reason', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Enter Reason to delete'] ])->textInput()->label(false) ?>
                </div>

                <?= Html::submitButton('<b>Delete Students</b>', ['class' => 'btn btn-primary ', 'data-confirm'=>'Please confirm to delete students']) ?>
                <?php ActiveForm::end(); ?>
                <?php endif;?>
                <div class="pull-right">
                    <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>"><button class="btn btn-default btn-sm">Back to Groups</button></a>
                </div>
            </div>
        </div>



</div>

<?= $this->render('_action_group_members',
    [
        'members'=>$members,
    ]);
?>

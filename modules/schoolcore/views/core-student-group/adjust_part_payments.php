<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\feesdue\models\FeesDue;
use yii\widgets\ActiveForm;
?>
<?php if($state){
  echo $state['message'];
  }?>

<div class="col-md-12 no-padding" style="background: #01b5d2">
<div class="wizard">
    <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">1</span> Edit Group Information</a>
    <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">2</span> Edit Group Members</a>
    <a  class="col-md-3 current"><span class="badge badge-inverse">3</span> Allow/Disallow Part Payments</a>
</div>
</div>

<div class="col-md-12" style="background: white;padding:10px; margin-bottom: 5px;">
<div class="row">
<div class="col-md-12 no-pading">
    <span style="font-size:20px;"><b><?= "Group Members - ".ucwords($model->group_name)."</b> (".$model->school->school_name.")" ?></span>
    <?php if($members) : 

    $form = ActiveForm::begin([
            'action' => ['part-payments', 'id'=>$model->id],
            'method' => 'post',
            'options'=>['class'=>'formprocess'],
        ]); 
    ?>
    <div>
    <div class="col-md-3 no-padding" >
    <?= $form->field($model2, 'allow_part_payments')->dropDownList(
        ['' => 'Choose Behaviour', '1' => 'Allow Part Payments', '0' => 'Don\'t Allow Part Payments'])->label(false) ?>
    </div>

      <?= Html::submitButton('<b>Update</b>', ['class' => 'btn btn-primary ', 'data-confirm'=>'Please confirm to change the part payments requirement for these students' ]) ?>
      <?php ActiveForm::end(); ?>
  <?php endif;?>
  <div class="pull-right">
      <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>"><button class="btn btn-default ">Back to Groups</button></a>
  </div>
  </div>
  </div>

</div>   
    
</div>

<?= $this->render('_action_group_members',
    [
        'members'=>$members,
    ]);
?>

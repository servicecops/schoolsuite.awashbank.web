<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

    <?php
    $form = ActiveForm::begin([
        'action' => ['assign', 'id' => $model->id],
        'method' => 'post',
        'id' => 'assign-form',
        'options' => ['class' => 'formprocess'],
    ]);

    ?>

<div class="col-md-12">
    <br/>
    <input id="empty-group" name="empty-group" type="hidden" value="">


    <div class="col-md-12 no-pading">
        <span style="font-size:20px;font-weight:500;">&nbsp;Group Members</span>
        <div class="col-md-12 no-pading">
            <?php if ($data) : ?>
                <div class="pull-right">
                    <?= Html::submitButton('<b>Empty Group <i class="fa fa-trash-o"></i></b>', ['name' => 'empty-group-button', 'id' => 'empty-group-button', 'class' => 'btn btn-danger btn-sm', 'data-confirm' => 'Are you sure you want to remove all students from this group']) ?>
                </div>
                <div class="pull-right">
                    <?= Html::submitButton('<b>Remove Selected Members <i class="fa fa-trash"></i></b>', ['class' => 'btn btn-danger btn-sm generate-btn', 'data-confirm' => 'Are you sure you want to remove the selected students from this group']) ?>
                </div>
                <div class="dropdown pull-right">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Take
                        Action
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="clink"><a href="<?= Url::to(['core-student-group/promote', 'id' => $model->id]) ?>">Promote
                                Class</a></li>
                        <li class="clink"><a
                                    href="<?= Url::to(['core-student-group/assign-fee', 'id' => $model->id]) ?>">Assign
                                Fee</a></li>

                        <?php if (Yii::$app->user->can('schoolpay_admin') ||
                            Yii::$app->user->identity->school->hasModule('SCHOOLPAY_PAYMENT_PLANS')) : ?>
                            <li class="clink"><a
                                        href="<?= Url::to(['core-student-group/assign-plan', 'id' => $model->id]) ?>">Assign
                                    Payment Plan</a></li>
                            <li class="clink"><a
                                        href="<?= Url::to(['core-student-group/cancel-plan', 'id' => $model->id]) ?>">Cancel
                                    Payment Plan</a></li>

                        <?php endif; ?>
                        <li class="clink"><a
                                    href="<?= Url::to(['core-student-group/remove-fee', 'id' => $model->id]) ?>">Remove
                                Fee</a></li>
                        <?php if (Yii::$app->user->can('adjust_student_bal')) : ?>
                            <li class="clink"><a
                                        href="<?= Url::to(['core-student-group/adjust-bal', 'id' => $model->id]) ?>">Adjust
                                    Balance</a></li>
                        <?php endif; ?>
                        <li class="clink"><a
                                    href="<?= Url::to(['core-student-group/part-payments', 'id' => $model->id]); ?>">Part
                                Payments</a></li>
                        <li class="clink"><a href="<?= Url::to(['core-student-group/archive', 'id' => $model->id]) ?>">Archive</a>
                        </li>
                        <li class="clink"><a
                                    href="<?= Url::to(['core-student-group/un-archive', 'id' => $model->id]) ?>">Un-archive</a>
                        </li>
                        <li class="clink"><a
                                    href="<?= Url::to(['core-student-group/remove-students', 'id' => $model->id]) ?>">Delete
                                Students</a></li>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class=" col-md-12 ">
    <table class="table table-striped">
        <thead style="background: white;padding:10px">
        <tr>
            <th style="padding:0 15px;"><span class="checkbox checkbox-danger checkbox-circle"
                                              style="margin:6px;"><input type="checkbox" class="checkall"
                                                                         onclick="checkall()"
                                                                         id="check_all_id2"><label
                            for="check_all_id2">Select All</label></span></th>
            <th>Sr.No</th>
            <th>Payment code</th>
            <th>Student Name</th>
            <th>Class</th>
            <th>Gender</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($data) :
            $srNo = 1;
            foreach ($data as $k => $v) : ?>
                <?php
                $studentName = $v['first_name'] ? $v['first_name'] : "";
                $studentName .= $v['middle_name'] ? " " . $v['middle_name'] : "";
                $studentName .= $v['last_name'] ? " " . $v['last_name'] : "";
                ?>
                <tr>
                    <td style="padding:0 15px;"><span class="checkbox checkbox-danger checkbox-circle"
                                                      style="margin:6px;"><input type="checkbox" class="checkbox"
                                                                                 name="remove_stu[]"
                                                                                 id="<?= $v['id']; ?>"
                                                                                 value="<?= $v['id'] ?>"><label
                                    class='col-md-12 no-padding' for="<?= $v['id']; ?>"></label></span></td>
                    <td><?= $srNo; ?></td>
                    <td><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= $studentName ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                </tr>
                <?php
                $srNo++;
            endforeach;
        else :
            ?>
            <td colspan="8">No Records found for the search Criteria</td>
        <?php endif; ?>
        </tbody>
    </table>

</div>
<?php ActiveForm::end(); ?>


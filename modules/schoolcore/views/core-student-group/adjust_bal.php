<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<?php if($state){
  echo $state['message'];
  }?>
  <div class="col-md-12 no-padding" style="background: #01b5d2">
<div class="wizard">
    <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">1</span> Edit Group Information</a>
    <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">2</span> Edit Group Members</a>
    <a  class="col-md-3 current"><span class="badge badge-inverse">3</span> Adjust Fee</a>
</div>
</div>

<div class="col-md-12" style="background: white;padding:10px; margin-bottom: 5px;">
<div class="row">
<div class="col-md-12 no-pading">
    <span style="font-size:20px;"><b><?= "Group Members - ".ucwords($model->group_name)."</b> (".$model->school->school_name.")" ?></span>
    <?php if($members) : 

    $form = ActiveForm::begin([
            'action' => ['adjust-bal', 'id'=>$model->id],
            'method' => 'post',
            'options'=>['class'=>'formprocess'],
        ]); 
    ?>
    <div>
    <div class="col-md-3 no-padding"> 
    <?= $form->field($model2, 'new_balance', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Enter New Balance'] ])->textInput()
        ->label(false) ?>
    </div>

        <div class="col-md-3 no-padding">
            <?= $form->field($model2, 'adjustment_mode', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'Adjustment Mode']])->dropDownList(
                [
                    'TO' => 'Adjust To - (Balances become new student balances)',
                    'BY' => 'Adjust By - (Amounts will be added to student balances)',
                ], ['prompt' => 'Choose Adjustment Mode']
            )->label(false) ?>
    </div>

        <div class="col-md-3 no-padding">
    <?= $form->field($model2, 'reason', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Give Reason'] ])->textInput()->label(false) ?>
    </div>

      <?= Html::submitButton('<b>Adjust Balance</b>', ['class' => 'btn btn-primary btn-sm', 'data-confirm'=>'Please confirm to change balance for these students']) ?>
      <?php ActiveForm::end(); ?>
  <?php endif;?>
  <div class="pull-right">
      <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>"><button class="btn btn-default ">Back to Groups</button></a>
  </div>
  </div>
  </div>
</div>  
    
</div>


<?= $this->render('_action_group_members',
    [
        'members'=>$members,
    ]);
?>

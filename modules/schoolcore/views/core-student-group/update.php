<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = 'Edit Group Information: ' . ' ' . $model->group_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-group-information-update row hd-title" data-title="Edit Group Information">

<div class="row" style="background: #efefef">
<div class="wizard">
    <a  class="current col-md-4"><span class="badge">1</span> Edit Group Information</a>
    <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-md-4"><span class="badge">2</span> Assign Students</a>
    <a  class="col-md-3 gray"><span class="badge badge-inverse">3</span> Take Action</a>
</div>
</div>
</div>
<div class='row'></div>
<div class="letter">
  <div class="row">
	<div class="col-md-12"> <h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>


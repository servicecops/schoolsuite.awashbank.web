<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<?php if ($state) {
    echo $state['message'];
} ?>

<div class="col-md-12 no-padding" style="background: #01b5d2">
    <div class="wizard">
        <a href="<?= Url::to(['core-student-group/update', 'id' => $model->id]) ?>" class="col-md-4"><span
                    class="badge">1</span> Edit Group Information</a>
        <a href="<?= Url::to(['core-student-group/assign', 'id' => $model->id]) ?>" class="aclink col-md-4"><span
                    class="badge">2</span> Edit Group Members</a>
        <a class="col-md-3 current"><span class="badge badge-inverse">3</span> Change Class</a>
    </div>
</div>

<div class="col-md-12" style="background: white;padding:10px; margin-bottom: 5px;">
    <div class="row">
        <div class="col-md-12 no-pading">
            <span style="font-size:20px;"><b><?= "Group Members - " . ucwords($model->group_name) . "</b> (" . $model->school->school_name . ")" ?></span>
            <?php if ($members) :

            $form = ActiveForm::begin([
                'action' => ['promote', 'id' => $model->id],
                'method' => 'post',
                'options' => ['class' => 'formprocess'],
            ]);
            ?>
            <div>
                <div class="row">
                <div class="col-md-3 padding">
                    <?= $form->field($model2, 'class_to')->dropDownList(
                        ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $model->school_id])->orderBy('class_code')->all(), 'id', 'class_description'), ['prompt' => 'Select class', 'class' => 'form-control input-sm']
                    )->label(false) ?>
                </div>
                </div>

                <div class="row padding">
                    <div class="col-md-4">
                        <?= $form->field($model2, 'emptyafter')->checkbox(['value' => true, 'label' => 'Automatically empty group after'])->label(false) ?>
                    </div>

                    <?= Html::submitButton('<b>Change Class</b>', ['class' => 'btn btn-primary btn-sm', 'data-confirm' => 'Please confirm to change students class']) ?>
                </div>


                <?php ActiveForm::end(); ?>
                <?php endif; ?>
                <div class="pull-right">
                    <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>">
                        <button class="btn btn-default btn-sm">Back to Groups</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>

<?= $this->render('_action_group_members',
        [
                'members'=>$members,
        ]);
?>

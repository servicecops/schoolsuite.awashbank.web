<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> School staff Info Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Frst name</th><th>Last name</th><th>School Name</th><th>Gender</th><th>Phone contact</th><th>Email</th><th>Staff level</th></tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['first_name']) ? $sinfo['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['last_name']) ? $sinfo['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['school_name']) ? $sinfo['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['gender']) ? $sinfo['gender'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['phone_contact_1']) ? $sinfo['phone_contact_1'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['email_address']) ? $sinfo['email_address'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['description']) ? $sinfo['description'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

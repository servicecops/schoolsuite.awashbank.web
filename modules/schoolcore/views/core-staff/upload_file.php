<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStaffType;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\SchoolInformation;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Classes;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="color:#F7F7F7">Import Staff</div>





<div class="row hd-title" data-title="Upload Excel">
    <div class="col-md-12 col-lg-12 no-padding">
        <p>All fields are marked * are required</p>
        <?php $form = ActiveForm::begin(['id' => 'UploadExcel',
            'options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="col-sm-3 col-md-2">
            <?= $form->field($model, 'importFile', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'File']])->fileInput()->label('Choose file *') ?>
        </div>
        <div class="col-sm-3 col-md-2">
            <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'Description']])->textInput()->label('Description *') ?>
        </div>
        <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
            <div class="col-md-3 col-sm-2 col-md-2">
                <?php
                $url = Url::to(['/schoolcore/core-school/active-schoollist']);
                $selectedSchool = empty($model->school) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->school)->school_name;
                echo $form->field($model, 'school')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'size' => 'sm',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'school_search',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('School *'); ?>
            </div>
        <?php endif; ?>

        <div class="col-md-3 col-sm-2 col-md-2 class_behaviour_container">
            <?php
            $thetypes = CoreStaffType::find()->orderBy(['id' => 'ASC'])->all();

            echo $form
                ->field($model, 'staff_type_id')
                ->dropDownList(ArrayHelper::map($thetypes, 'id', 'staff_description'), [
                    'placeholder' => 'Select Type',
                    'prompt' => 'Select Staff Type',
                    'id' => 'staff_type',
                ])
                ->label('Staff Type *'); ?>
        </div>



        <div class="col-sm-2" style="margin-top: 7px;">
            <br>
            <?= Html::submitButton("Upload File <i class='fa fa-upload'></i>", ['class' => "btn btn-primary "]) ?>
        </div>


        <?php ActiveForm::end(); ?>


    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">
            <div class="col-md-12 no-padding">
                <p style="font-size:15px;color:grey;"><b>Note:</b> Only excel data sheets that follow the <b>template format provided by schoolPay</b> will be allowed.
                    <b>**And follow column Order !important**</b> <a href="<?= \Yii::getAlias('@web/uploads/schsuite_teacher_data_import_template.xlsx'); ?>" target="_blank">download template</a></p>
                <table class="table table-striped" style="font-size:12px;">
                    <thead>
                    <tr><th>Last Name</th><th>First Name</th><th>Middle Name</th><th>DoB</th><th>Gender</th>
                        <th>Teacher Email</th><th>Teacher Phone</th><th>Nin No</th><th>Other Contact</th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr><td>Lukwago</td><td>Ashiraf</td><th>--</td><th>1990-09-23</td><td>M</td>
                        <td>teacher1@gmail.com</td><td>0753777052</td><td>--</td>
                        <td>--</td>
                    </tr>
                    <tr><td>Required</td><td>Required</td><td>Optional</td><th>Required</td><td>Required</td>
                       <td>Required</td><td>Required</td>
                      <td>Optional</td>
                        <td>Optional</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

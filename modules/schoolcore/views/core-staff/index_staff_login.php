<?php

use app\models\UserSearch;
use app\models\User;
use kartik\editable\Editable;
use kartik\export\ExportMenu;
use kartik\ipinfo\IpInfo;
use kartik\popover\PopoverX;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use yii\widgets\Pjax;

use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $model app\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Teacher Logins';

?>



<!--new-->
    <div class="core-school-index">

        <h5><?= Html::encode($this->title) ?></h5>


        <?php


        $columns = [
            ['class' => 'kartik\grid\SerialColumn',],
            'username',
         'email',
            'user_level',
            [
                'attribute'=>'school_id',
                'value'=>'school.school_name',
            ],




            [
                'attribute'=>'Reset',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-unlock"></i>', ['/user/reset', 'id'=>$model->id], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'View',
                'value'=>function($model){
                    return Html::a('<i class="fa  fa-eye"></i>', ['/user/view', 'id'=>$model->id]);
                },
                'format'=>'html',
            ],



        ];
        ?>



        <div class="row" style="padding: 20px">
        <?php echo $this->render('_search_staff_login', ['model' => $searchModel]); ?>
        </div>
        <div class="row">



        </div>

        <div class="table-responsive mt-3">
            <div class="float-right">


            </div>
            <?= GridView::widget([
                'dataProvider'=> $dataProvider,
                //    'filterModel' => $searchModel,
                'columns' => $columns,
                'resizableColumns'=>true,
                'responsiveWrap' => false,
                //    'floatHeader'=>true,

                'responsive'=>false,
                'bordered' => false,
                'striped' => true,
            ]); ?>

        </div>


    </div>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model app\models\ContactForm */

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Email';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letter">
    <div class="site-contact container">
        <h4><?= Html::encode($this->title) ?></h4>


        <?php $form = ActiveForm::begin(['id' => 'contact-form', 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="row">

            <div class="col">
                <?= $form->field($model, 'subject')->label('Subject *') ?>
                <?= $form->field($model, 'cc')->label('CC *') ?>
                <?= $form->field($model, 'bcc')->label('BCC *') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Body *') ?>
            </div>
            <div class="col">
                <?= $form->field($model, 'attachment[]')->widget(FileInput::classname(), [
                    'options' => ['multiple' => true],
                ]) ?>    </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>

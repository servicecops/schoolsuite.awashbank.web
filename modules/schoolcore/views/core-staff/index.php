<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Students';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="row core-student-index">


    <div class="col-md-12">
        <div class="float-right">
            <!--        data download or export widget-->
            <!--    --><?php
            /*    echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'dropdownOptions' => [
                        'label' => 'Export',
                    ]

                ])
                */ ?>

            <?php
            echo Html::a('<i class="fa far fa-padlock"></i> Teacher Logins', ['core-staff/staff-logins', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-sm btn-info',
                'target' => '_blank',
                'data-toggle' => 'tooltip',
                'title' => 'Will open the generated PDF file in a new window'
            ]);
            echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-sm btn-danger',
                'target' => '_blank',
                'data-toggle' => 'tooltip',
                'title' => 'Will open the generated PDF file in a new window'
            ]);
            echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-sm btn-success',
                'target' => '_blank'
            ]);
            ?>
        </div>
        <div class="float-left">
            <div class="col-md-12 "><span style="font-size:20px;color:#2c3844">&nbsp;<i
                            class="fa fa-th-list"></i> <?php echo $title ?> </span></div>
            <br/>
            <br/>
        </div>
        <div class="float-left">
            <div class="col-md-12 "><span style="font-size:20px;color:#2c3844">&nbsp; </span></div>
            <br/>
            <br/>

            <div class="col-md-12"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
        </div>
    </div>
    <div class="col-md-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
            'columns' => $columns,
            'resizableColumns' => true,
//    'floatHeader'=>true,
            'options' => [
                'class' => 'bg-colorz',
            ],
            'responsive' => true,
            'responsiveWrap' => false,
            'hover' => true,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>


    <div style="clear:both"></div>
</div>

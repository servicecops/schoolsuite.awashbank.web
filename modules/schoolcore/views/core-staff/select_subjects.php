<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action' => ['assign', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess'],
]); ?>
    <div class="row">
        <div class="col-xs-12 no-pading">
            <span style="font-size:20px;font-weight:500px;">&nbsp;Select New Subjects</span>
            <?php if ($data) : ?>
                <div class="pull-right">
                    <?= Html::submitButton('<b>Assign Subjects</b>', ['class' => 'btn btn-primary btn-sm generate-btn', 'data-confirm' => 'Confirm to add subjects']) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <fieldset>
        <div class="box-body table table-responsive no-padding">
            <table class="table">
                <thead>
                <tr>
                    <th style="padding:0 10px;"><span class="checkbox checkbox-info checkbox-circle"
                                                      style="margin:6px;"><input type="checkbox" class="checkall"
                                                                                 onclick="checkall()"
                                                                                 id="check_all_id1"><label
                                    for="check_all_id1">Select All</label></span></th>
                    <th>Subject code</th>
                    <th>Class</th>
                    <th>Subject Name</th>

                </tr>
                </thead>
                <tbody>
                <?php
                if ($data) :
                    $srNo = 1;
                    foreach ($data as $k => $v) : ?>
                        <?php

                        ?>
                        <tr>
                            <td style="padding:0 10px;">
                                <span class="checkbox checkbox-info checkbox-circle"
                                      style="margin:6px;">
                                    <input type="checkbox"
                                           class="checkbox"
                                           name="selected_stu[]"
                                           id="<?= $v['id']; ?>"
                                           value="<?= $v['id'] ?>">
                                    <label class='col-xs-12 no-padding' for="<?= $v['id']; ?>"></label>
                                </span>
                            </td>
                            <td><?= ($v['subject_code']) ? $v['subject_code'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['subject_name']) ? $v['subject_name'] : '<span class="not-set"> -- </span>' ?></td>
                        </tr>
                        <?php
                        $srNo++;
                    endforeach;
                else :
                    ?>
                    <td colspan="8">No Records found for the search Criteria</td>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </fieldset>
<?php ActiveForm::end(); ?>

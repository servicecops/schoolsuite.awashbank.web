<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;


?>

<div style="color:#F7F7F7">Staff Information</div>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>


<div class="row">


    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
        <div class="col-md-3">

            <?php
            $url = Url::to(['core-school/active-schoollist']);
            $selectedSchool = empty($model->schsearch) ? '' : CoreSchool::findOne($model->schsearch)->school_name;
            echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id' => 'school_search',
                    'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#corestaffsearch-class_id" ).html(data);
                        });'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>


        </div>
    <?php endif; ?>


    <div class="col-md-3">
        <?= $form->field($model, 'first_name', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'First Name'])->label(false) ?>

    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control'],])->textInput(['title' => 'Enter Last Name',

            'data-toggle' => 'tooltip',

            'data-trigger' => 'hover',

            'placeHolder' => 'Last Name',
            'data-placement' => 'bottom'])->label(false) ?>

    </div>
    <div class="col-md-2">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>



<?php
$url = Url::to(['core_school_class/lists']);
$cls = $model->class_id;


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

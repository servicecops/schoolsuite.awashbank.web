<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStaff */

$this->title = $model->first_name . " " . $model->last_name;
$username = null;
$user = User::findOne(['school_user_id' => $id]);
if ($user) {
    $username = $user->username;
}
?>
<div class=" row">

    <div class="letter">
        <h1><?= Html::encode($this->title) ?></h1>


        <div class="col-md-12" style="float:right">
            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

            <?= Html::a('Classes', ['core-staff/view-classes', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-envelope">Send email</i>', ['send-email', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>


            <?= Html::a('Edit signature', ['staff-signature', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>

        </div>


        <div class="col-md-12 " style="padding:15px">
            <div class="col-md-4">
                <label style="color: black;font-size: 15px;"><p style="font-weight: bold">Username</p></label>
            </div>
            <div class="col-md-5">
                <b>  <p style="font-weight: bold"><?php echo $username ?></p></b>

            </div>

        </div>

        <div class="col-md-12 " style="padding:15px">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => $attributes
            ]) ?>
        </div>
    </div>


</div>

<?php

use app\models\AuthItem;
use app\models\UserSearch;
use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>


<div class="container">
    <div class="formz">
        <div></div>

        <div class="fees-due-form">
            <?php $form = ActiveForm::begin([
                'action' => ['staff-logins'],
                'method' => 'get',
            ]); ?>

            <div class="row ">
                <div class="col-md-2 ">

                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'username', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Username'])->label(false) ?>
                </div>



                <div class="col-md-2">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-sm btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

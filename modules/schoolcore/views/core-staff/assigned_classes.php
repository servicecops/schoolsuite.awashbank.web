<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>



    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class='clink'>Class Description</th>
                        <th class='clink'>Subject Code</th>
                        <th class='clink'>Subject Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($data) :
                        foreach ($data as $k => $v) : ?>
                            <tr data-key="0">
                                <td><?= ($v['class_description']) ? $v['class_description'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['subject_code']) ? $v['subject_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['subject_name']) ? $v['subject_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td class="clink"><a href="<?= Url::to(['core-staff/remove-subject', 'id' => $v['assId']]) ?>" ,
                                                     data-confirm="Are you sure you want to remove this subject from this teacher"
                                                     data-method="post" ,
                                                    '><i
                                                class="fa  fa-remove"></i>&nbsp;&nbsp;&nbsp;</a>
                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No classes found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>


        </div>
    </div>

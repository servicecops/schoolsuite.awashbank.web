<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel  app\modules\schoolcore\models\CoreStaffSearch*/
/* @var $dataProvider yii\data\ActiveDataProvider */
// By John Mark

$this->title = 'Assign Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="color:#F7F7F7">Assign Classes</p>
<div class="letter" data-title="<?= $this->title; ?>">



    <div class="row" >
        <span style="font-size:18px;color:#3c8dbc"><b><?= ucwords($model->schoolName->school_name) ?> - Assign Subjects to Teacher </b></span>

        <hr class="l_header">
        <div class="col-md-12 ">
            <div class="col-md-2 col-xs-12 no-padding"><h3 class="box-title" style="font-size:19px;">&nbsp;<i class="fa fa-th-list"></i>&nbsp;&nbsp;Get Subjects</h3></div>
            <div class="col-md-7 col-xs-12 no-padding" style="padding-top: 20px !important;"><?php echo $this->render('_searchClass', ['model'=>$model]); ?></div>

        </div>
        <div class="col-md-12">
            <span style="font-size:15px; ">Search and assign subjects then click on <b>Take Action</b> button to use the group.</span>
        </div>
    </div>

    <div class="row">
        <!-- Title and filters -->

        <!-- Note -->
        <!-- Selection Table -->

        <div class="col-md-5 " style="padding-top: 10px;">
            <div class="select_tbl">
                <?php echo $this->render('select_subjects', ['data' => $data, 'model'=>$model]); ?>
            </div>
            <!-- Payment codes -->

            <!--  -->
        </div>
        <!-- Group Members Table -->
        <div class="col-md-7 " style="padding-top: 10px;">
            <?php echo $this->render('group_members', ['data' => $members , 'model'=>$model]); ?>
        </div>
    </div>
</div>

<?php
$script = <<< JS

$(function () {
    
$(".code_link").click(function(){
      $(".select_tbl").css('display', 'none');
      $(".codes_input").css('display', 'block');
    });
    $(".back_link").click(function(){
      $(".select_tbl").css('display', 'block');
      $(".codes_input").css('display', 'none');
    });
    
    $("#empty-group-button").bind('click', function() {
       $("#empty-group").val('empty-group') 
    })
});





JS;
$this->registerJs($script);
?>

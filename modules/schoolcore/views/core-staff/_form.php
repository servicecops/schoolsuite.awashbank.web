<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStaffType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeesDue */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */


//if(!isset($model->recurrent)) $model->recurrent = true;
//$disableControls = false;
//if (!$model->isNewRecord && $model->approval_status) {
//    $disableControls = true;
//}
//Yii::trace($model->approval_status);
//Yii::trace($disableControls);
?>
<div class="formz">
    <div></div>

    <div class="fees-due-form">

        <?php
        $thetypes = CoreStaffType::find()->orderBy(['id' => 'ASC'])->all();
        Yii::trace($thetypes);

        if ($this->context->action->id == 'update')
            $action = ['update', 'id' => $_REQUEST['id']];
        else
            $action = ['create'];
        ?>
        <?php $form = ActiveForm::begin([
            'action' => $action,

        ]); ?>

        <p>All fields marked with * are required</p>

        <div class="row">
            <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
                <div class="col-md-6">
                    <?php

                    $url = Url::to(['../schoolcore/core-school/active-schoollist']);
                    $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                    Yii::trace("School id" . $selectedSchool);
                    echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
                        'class' => 'form-control',
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'disabled' => !$model->isNewRecord,
                        'options' => [
                            'placeholder' => 'Filter School',
                            'id' => 'fee_selected_school_id',

                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],

                        ],
                    ])->label('School Name *'); ?>
                </div>
            <?php endif; ?>


        </div>
        <div class="row">

            <div class="col-sm-4">
                <?= $form->field($model, 'first_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'First Name']])->textInput() ->label('First Name')?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Last Name']])->textInput()->label('Last Name *') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'date_of_birth')->widget(DatePicker::className(),
                    [
                        'model' => $model,
                        'attribute' => 'date_of_birth',
                        'dateFormat' => 'yyyy-MM-dd',
                        'clientOptions' => [
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1980:' . (date('Y')),
                            'autoSize' => true,
                            'dateFormat' => 'yyyy-MM-dd',
                        ],
                        'options' => [
                            'class' => 'form-control' . ($model->hasErrors('date_of_birth') ? ' is-invalid' : ''),
                            'placeholder' => 'Choose Date'
                        ],]) ->label('Date of Birth*'); ?>
            </div>
            <div class="col-md-4 ">
                <?= $form->field($model, 'gender')->dropDownList(['M' => 'Male', 'F' => 'Female'], ['prompt' => 'Gender']) ->label('Gender *') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'phone_contact_1', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Phone Number']])->textInput()->label('Phone Contact *') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'phone_contact_2', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Other Phone Number']])->textInput()->label('Another Phone Contact') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'email_address', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput()->label('Email *') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'nin_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'National ID NIN']])->textInput() ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'educational_qualification', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Educational Qualification']])->textInput() ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'number_of_dependants', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Number of Dependants']])->textInput() ?>
            </div>
            <div class="col-sm-4">

                <?= $form->field($model, 'marital_status', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                    ['Single' => 'Single',
                        'Married' => 'Married',
                        'Separated' => 'Separated',
                        'Divorced' => 'Divorced'],
                    ['id' => 'user_level_id1', 'prompt' => 'Marital Status']
                )->label('Marital Status *') ?>


            </div>


            <div class="col-sm-4">
                <?= $form->field($model, 'user_level', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                    ['sch_teacher' => 'School Teacher'],
                    ['id' => 'user_level_id1', 'prompt' => 'Type of User']
                )->label('Type Of User *') ?>
            </div>
            <div class="col-sm-4">
                <?= $form
                    ->field($model, 'staff_type_id')
                    ->dropDownList(ArrayHelper::map($thetypes, 'id', 'staff_description'), [
                        'placeholder' => 'Select Type',
                        'prompt' => 'Select Type',
                        'id' => 'staff_type',
                    ])
                    ->label('Select Category *'); ?>
            </div>
            <div class="col-sm-4" id ="div_teaching_category">
                <?= $form->field($model, 'teaching_category')->dropDownList([
                    'Trained' => 'Trained',
                    'Qualified' => 'Qualified',


                ],
                    ['id'=>'teaching_category', 'prompt' => 'Teaching Category']) ?>
            </div>

            <div class="col-sm-4" id="non_teaching">
                <?= $form->field($model, 'non_teaching_category')->dropDownList([
                    'Nurses' => 'Nurses',
                    'Bursars' => 'Bursars',
                    'Sign language interpreters' => 'Sign language interpreters',
                    'Cooks' => 'Cooks',
                    'Cleaners' => 'Cleaners',
                    'Security/Gateman' => 'Security/Gateman',
                    'Librarian' => 'Librarian',
                    'Lab technicians' => 'Lab technicians',
                    'Administrative staff with no instructional duties' => 'Administrative staff with no instructional duties',
                    'Other' => 'Other',


                ],
                    ['id'=>'non_teaching_category', 'prompt' => 'Non Teaching Category']) ?>
            </div>
            <div class="col-md-4 " id="other_non_teaching">
                <?= $form->field($model, 'other_non_teaching', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Others Specify','id'=>'other'] ])->textInput()->label('Other Non Teaching Category Specify') ?>

            </div>
        </div>





        <hr class="l_header"/>
        <div class="row">
            <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Address Information</h3></div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php
                $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\RefRegion::find()->all(), 'id', 'description');
                echo $form->field($model, 'region')
                    ->dropDownList(
                        $items,           // Flat array ('id'=>'label')
                        ['prompt' => 'Select region',
                            'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });']    // options
                    )->label('Region *'); ?>
            </div>
            <div class="col-sm-4">
                <?php
                $items = ArrayHelper::map(\app\modules\schoolcore\models\District::find()->where(['region_id'=>$model->region])->all(), 'id', 'district_name');
                echo $form->field($model, 'district')->dropDownList($items
                    , ['id' => 'district',
                        'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])->label('District *')
                ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Village']])->textInput()->label('Physical Address *') ?>
            </div>

        </div>

        <hr>

        <div class="row">
            <div class="col-xs-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xs-6">
                <?= Html::resetButton('Reset <i class="fa fa-save"></i>', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?php

$url = Url::to(['/user/level-options']);
$user_level = $model->user_level;

$script = <<< JS



$("document").ready(function(){ 
    
    
 
      $('#other_non_teaching').hide();
      $('#div_teaching_category').hide();
      $('#non_teaching').hide();
    
    
    function evaluateRecurrentVisibility() {
        if($('#fee_type_select').val() === '1') {
           $('#recurrent-settings-container').show()
        }else {
           $('#recurrent-settings-container').hide() 
        }
}

    function evaluatePenaltyRecurrentVisibility() {
        if($('#penalty_frequency_select').val() === 'RECURRENT') {
           $('.penalty_recurrent_settings_container').show()
        }else {
           $('.penalty_recurrent_settings_container').hide() 
        }
}

function evaluatePenaltyControls() {
       // console.log($('#user_level_id').val());
       //alert("user_level");
        if($('#user_level_id') === 'sch_teacher') {
           $('#arrears-settings-container').show()
        }else {
           $('#arrears-settings-container').hide() 
        }
        
         $(".active").removeClass('active');
    $("#user").addClass('active');
    $("#user_create").addClass('active');
    $('#school_div').hide();
    $('#bank_div').hide();
    $.get('$url', function( data ) {
        $('select#user_level_id').html(data);
        $('select#user_level_id').val('$user_level');
        
        if($('#user_level_id').val()){
            var profile =$('select#user_level_id option[value="' + $('#user_level_id').val() + '"]').attr("data-profile");
            if(profile == 'school_guy'){
                $('#school_div').show();
            }
            $('#school_div').show();
        }
        if($('#user_level_id').val() == 'bank_user'){
            $('#bank_div').show();
        }
    });
    
    $('#user_level_id').change(function(){
        var profile =$('select#user_level_id option[value="' + $(this).val() + '"]').attr("data-profile");
        if(profile == 'school_guy'){
            $('#bank_div').hide();
            $('#school_div').show();
        }
        else if($(this).val() == 'bank_user'){
             $('#school_div').hide();
            $('#bank_div').show();
        }
        else {
            $('#school_id').prop('selectedIndex', 0);
            $('#school_div').hide();
        }
    });
}

evaluateRecurrentVisibility();
evaluatePenaltyRecurrentVisibility();
evaluatePenaltyControls();

    //On changing fee type, hide or show recurrent controls
   $('body').on('change', '#fee_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   $('body').on('change', '#penalty_frequency_select', function(){
       evaluatePenaltyRecurrentVisibility();
   });
      
      
    
        let thisTeachingType = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);
   
        if (selector.val() === '1') {

             $('#div_teaching_category').show();
             $('#non_teaching').hide();
        } else {
            $('#div_teaching_category').hide();
            $('#non_teaching').show();

        }
     

    }
       let thisNonTeachingCategory = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

   
        if (selector.val() === 'Other') {

             $('#other_non_teaching').show();
        } else {
            $('#other_non_teaching').hide();
        }
     
        
    }
    
    
     $("#non_teaching_category").on("change", function () {
       // alert('inside');
        thisNonTeachingCategory($(this));
        let selector = $(this);
     
    });
        
        $("#staff_type").on("change", function () {
        thisTeachingType($(this));
        let selector = $(this);
     
    }); 
    
});
   


JS;
$this->registerJs($script);
?>

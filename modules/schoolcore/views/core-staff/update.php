<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Update Core User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Core User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-user-update">

    <div class="row">
        <div class="row">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="row">

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

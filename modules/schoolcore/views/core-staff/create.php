<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Add Staff';
$this->params['breadcrumbs'][] = ['label' => 'Core Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="row">
        <div class="row">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="row">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>


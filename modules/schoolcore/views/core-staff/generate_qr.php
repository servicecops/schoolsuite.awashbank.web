<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\widgets\ActiveForm */
?>

<div">


    <?php $form = ActiveForm::begin(['id'=>'student_image', 'options'=>['enctype'=>'multipart/form-data']]); ?>

        <header id="header" class="qr-header"><h1><?= $staffModel->firstname. '  ' .$staffModel->lastname?></h1></header>

        <div id="content" class="qr-content">



            <div class="row no-gutters">

                <div class="col-6 col-md-4">

                    <form id="qrForm" onsubmit="return false;">

                        <div class="qr-inputContainer">

                            <input type="hidden" onchange="updateQrData();" name="dataInput" value="<?= $staffModel->id?>" id="dataInput" required />

                        </div>



                        <div class="qr-inputContainer">

                            <label for="colorInput">Color</label>

                            <input type="color" onchange="updateQrColor();" name="colorInput" id="colorInput" />

                        </div>

                        <div class="qr-inputContainer">

                            <label for="typeInput">Type</label>

                            <select name="typeInput" id="typeInput" onchange="updateQrType();">

                                <option value="square">Square</option>

                                <option value="rounded">Rounded</option>

                                <option value="dots">Dots</option>

                            </select>

                        </div>

                    </form>

                    <button id="formButton" onclick="download();" class="qr-formButton">Download</button>

                </div>
                <div class="col-12 col-sm-6 col-md-8">
                    <div id="canvas"></div>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>



<?php
$this->registerJsFile(Url::to('@web/web/js/qr.js', \yii\web\View::POS_END));
$this->registerJsFile(Url::to('https://unpkg.com/qr-code-styling/lib/qr-code-styling.js', \yii\web\View::POS_END));
?>


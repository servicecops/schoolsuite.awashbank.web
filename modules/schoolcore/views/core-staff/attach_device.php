<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStaffType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeesDue */
/* @var $form yii\widgets\ActiveForm */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */


//if(!isset($model->recurrent)) $model->recurrent = true;
//$disableControls = false;
//if (!$model->isNewRecord && $model->approval_status) {
//    $disableControls = true;
//}
//Yii::trace($model->approval_status);
//Yii::trace($disableControls);
?>
<div class="formz">



        <?php $form = ActiveForm::begin([
            'action' => ['attach-device', 'id' => $id]

        ]); ?>





        <table style="font-family: arial, sans-serif;  border-collapse: collapse;  width: 100%;">
            <tr >
                <th style="width:50%;float:left">Username</th>
                <th style="width:50%;float:left">Device Id</th>
            </tr>
            <tr>
            <td style="width:50%;float:left"><?php print_r($username)?></td>
            <td style="width:50%;float:left">    <?= $form->field($model, 'device_id')->textInput() ?></td>

            </tr>
        </table>




        <div class="row" style="padding:2em">
            <div class="col-md-6">
                <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update',
                    ['class' => 'btn btn-block btn-primary', $model->id]) ?>
            </div>
            <div class="col-md-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

</div>

<?php

$url = Url::to(['/user/level-options']);
$user_level = $model->user_level;

$script = <<< JS



$("document").ready(function(){ 
    
    
 
    
    
    
    function evaluateRecurrentVisibility() {
        if($('#fee_type_select').val() === '1') {
           $('#recurrent-settings-container').show()
        }else {
           $('#recurrent-settings-container').hide() 
        }
}

    function evaluatePenaltyRecurrentVisibility() {
        if($('#penalty_frequency_select').val() === 'RECURRENT') {
           $('.penalty_recurrent_settings_container').show()
        }else {
           $('.penalty_recurrent_settings_container').hide() 
        }
}

function evaluatePenaltyControls() {
       // console.log($('#user_level_id').val());
       //alert("user_level");
        if($('#user_level_id') === 'sch_teacher') {
           $('#arrears-settings-container').show()
        }else {
           $('#arrears-settings-container').hide() 
        }
        
         $(".active").removeClass('active');
    $("#user").addClass('active');
    $("#user_create").addClass('active');
    $('#school_div').hide();
    $('#bank_div').hide();
    $.get('$url', function( data ) {
        $('select#user_level_id').html(data);
        $('select#user_level_id').val('$user_level');
        
        if($('#user_level_id').val()){
            var profile =$('select#user_level_id option[value="' + $('#user_level_id').val() + '"]').attr("data-profile");
            if(profile == 'school_guy'){
                $('#school_div').show();
            }
            $('#school_div').show();
        }
        if($('#user_level_id').val() == 'bank_user'){
            $('#bank_div').show();
        }
    });
    
    $('#user_level_id').change(function(){
        var profile =$('select#user_level_id option[value="' + $(this).val() + '"]').attr("data-profile");
        if(profile == 'school_guy'){
            $('#bank_div').hide();
            $('#school_div').show();
        }
        else if($(this).val() == 'bank_user'){
             $('#school_div').hide();
            $('#bank_div').show();
        }
        else {
            $('#school_id').prop('selectedIndex', 0);
            $('#school_div').hide();
        }
    });
}

evaluateRecurrentVisibility();
evaluatePenaltyRecurrentVisibility();
evaluatePenaltyControls();

    //On changing fee type, hide or show recurrent controls
   $('body').on('change', '#fee_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   $('body').on('change', '#penalty_frequency_select', function(){
       evaluatePenaltyRecurrentVisibility();
   });
      
      
    
    
});
   


JS;
$this->registerJs($script);
?>

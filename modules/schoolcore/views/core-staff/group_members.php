<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
        'action' => ['assign', 'id'=>$model->id],
        'method' => 'post',
        'id'=>'assign-form',
        'options'=>['class'=>'formprocess'],
    ]);

?>
    <input id="empty-group" name="empty-group" type="hidden" value="">

<div class="row">
<div class="col-xs-12 no-pading">
    <span style="font-size:20px;font-weight:500;">&nbsp;Selected Subjects</span>
    <?php if($data) : ?>
    <div class="pull-right">
        <?= Html::a('<i class="fa fa-eye" >Back to Staff View</i>', ['view', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>

    </div>
        <div class="pull-right">
      <?= Html::submitButton('<b>Empty Group <i class="fa fa-trash-o"></i></b>', ['name'=>'empty-group-button','id'=>'empty-group-button', 'class' => 'btn btn-danger btn-sm', 'data-confirm'=>'Are you sure you want to remove all subjects from this teacher' ]) ?>

    </div>
        <div class="pull-right">
      <?= Html::submitButton('<b>Remove Selected Members <i class="fa fa-trash"></i></b>', ['class' => 'btn btn-danger btn-sm generate-btn', 'data-confirm'=>'Are you sure you want to remove the selected subjects from this teacher']) ?>

    </div>
    <div class="dropdown pull-right">
      <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Take Action
      <span class="caret"></span></button>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="clink"><a href="<?= Url::to(['assign-subjects', 'id'=>$model->id]) ?>">Assign Subjects</a></li>


     </ul>
    </div>
  <?php endif;?>
  </div>
</div>
<fieldset> 
<div class="box-body table table-responsive no-padding">     
<table class="table table-striped">
<thead>
<tr><th style="padding:0 10px;"><span class="checkbox checkbox-danger checkbox-circle" style="margin:6px;"><input type="checkbox" class="checkall" onclick="checkall()" id="check_all_id2"><label for="check_all_id2">Select All</label></span></th>
<th>Sr.No</th><th>Subject code</th><th>Class</th><th>Subject Name</th>
</tr>
</thead>        
<tbody>
    <?php 
    if($data) :
    $srNo = 1;
    foreach($data as $k=>$v) : ?>
        <?php

        ?>
        <tr>
            <td style=" git statuspadding:0 10px;"><span class="checkbox checkbox-danger checkbox-circle" style="margin:6px;"><input type="checkbox" class="checkbox" name="remove_stu[]" id="<?= $v['id']; ?>" value="<?=$v['id'] ?>" ><label class='col-xs-12 no-padding' for="<?= $v['id']; ?>"></label></span></td>
            <td><?= $srNo; ?></td>
            <td><?= ($v['subject_code']) ? $v['subject_code'] : '<span class="not-set"> -- </span>' ?></td>

            <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
            <td><?= ($v['subject_name']) ? $v['subject_name'] : '<span class="not-set"> -- </span>' ?></td>
        </tr>
    <?php 
    $srNo++;
    endforeach ;
    else :
     ?>
     <td colspan="8">No Records found for the search Criteria</td>
   <?php endif; ?>
</tbody>
</table>
</div>
</fieldset>
<?php ActiveForm::end(); ?>

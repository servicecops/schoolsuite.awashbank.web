<?php


use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model  app\modules\schoolcore\models\CoreStaff */
/* @var $form yii\widgets\ActiveForm */
/* @var $school  app\modules\schoolcore\models\CoreSchool;*/
?>

<div class="row">
    <?php $form = ActiveForm::begin([
        'action' => ['assign', 'id' => $model->id],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>
<?php
$schoolId = Yii::$app->user->identity->school_id;
?>

    <div class="col-md-8 no-padding-right">
        <?= $form->field($model, 'class_id')->dropDownList(
            ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $schoolId])->andWhere(['<>', 'class_code', '__ARCHIVE__'])->orderBy('class_code')->all(), 'id', 'class_description'), ['prompt' => 'Filter class', 'class' => 'form-control input-sm']
        )->label(false) ?>
    </div>

    <div class="col-md-4 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>

    <?php ActiveForm::end(); ?>

</div>

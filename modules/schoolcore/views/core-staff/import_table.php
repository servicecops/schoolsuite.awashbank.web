<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>


<!--        -->
<?php
if ($status['state']) {
//    echo $status['message'];
}
?>

<div class="row">
    <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">
            <div id="flash_message">
                <?php  if (\Yii::$app->session->hasFlash('uploadFailed')) : ?>
                    <?= \Yii::$app->session->getFlash('uploadFailed'); ?>
                <?php endif; ?>
            </div>
            <table class="table" style="background-color:#fff; margin-bottom:3px;">
                <tr>
                    <th>
                        <span style="font-size:17px;"><strong>&nbsp;<?= $imported['description'] . " (" . $imported['school_name'] . ")"; ?></strong></span>
                    </th>

                    <th class="pull-right no-padding">
                        <button class="btn btn-danger btn-sm"
                                onclick="clinkConf('<?= Url::to(['core-student/delete-file', 'id' => $file]) ?>', 'Are you sure you want to delete this file')">
                            <b>Remove file</b>&nbsp; &nbsp;
                        </button>
                    </th>
                    <?php if (!$imported['imported'] && Yii::$app->user->can('approve_students')): ?>
                        <th class="pull-right no-padding"><a class='aclink'
                                                             href="<?= Url::to(['core-staff/file-details', 'id' => $file]) ?>"
                                                             data-confirm="Please confirm to save staff"
                                                             data-method="post" data-params='{"action":"save"}'><span
                                        class="btn btn-primary btn-sm disabled"><b>Click to Submit staff </b>&nbsp; &nbsp;</span></a>
                        </th>

                    <?php endif; ?>



                </tr>
            </table>
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>DoB</th>
                        <th>Gender</th>
                        <th>Teacher's Email</th>
                        <th>Teacher's Phone</th>
                        <th>Other Phone</th>
                        <th>Nin No</th>
                        <th>&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $srNo = 1;
                    foreach ($data as $k => $v) : ?>
                        <tr>
                            <td><?= $srNo; ?></td>
                            <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['middle_name']) ? $v['middle_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['dob']) ? $v['dob'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['teacher_email']) ? $v['teacher_email'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['teachers_phone']) ? $v['teachers_phone'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['other_phone']) ? $v['other_phone'] : '<span class="not-set"> -- </span>' ?></td>

                            <td><?= ($v['teachers_phone']) ? $v['teachers_phone'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['nin_no']) ? $v['nin_no'] : '<span class="not-set"> -- </span>' ?></td>
                            <td class="clink"><a href="<?= Url::to(['/classes/file-details', 'id' => $file]) ?>" ,
                                                 data-confirm="Are you sure you want to delete this staff"
                                                 data-method="post" ,
                                                 data-params='{"action":"del_student", "stu":"<?= $v['id'] ?>"}'><i
                                            class="fa  fa-remove"></i>&nbsp;&nbsp;&nbsp;</a>
                            </td>
                        </tr>
                        <?php
                        $srNo++;
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Uploaded Staff files';
?>
<div class="row">
    <div class="col-md-12">
        <?php if (\Yii::$app->session->hasFlash('fileListAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:" class='close-notify' data-flash='fileListAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('fileListAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>


    <div class="col-md-12">

        <div class="col-md-4"><span style="font-size:22px;">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>

        <div class="col-md-8"><?php echo $this->render('_uploaded_files_search', ['searchModel' => $searchModel]); ?></div>

    </div>

    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped ">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?= $sort->link('file_name') ?></th>
                    <th><?= $sort->link('description') ?></th>
                    <th><?= $sort->link('date_uploaded') ?></th>
                    <th><?= $sort->link('username') ?></th>
                    <th><?= $sort->link('school_name') ?></th>
                    <th><?= $sort->link('imported') ?></th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($model) :
                    foreach ($model as $k => $v) : ?>

                        <tr>
                            <td><?= $k + 1 ?></td>
                            <td class="clink"><a
                                        href="<?= Url::to(['core-staff/file-details', 'id' => $v['id']]) ?>"><?= ($v['file_name']) ? $v['file_name'] : '<span class="not-set">(not set) </span>' ?></a>
                            </td>
                            <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['date_uploaded']) ? date('M d, Y - g:i:s A', strtotime($v['date_uploaded'])) : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['username']) ? $v['username'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['imported']) ? "<i class='fa  fa-check'></i>" : '<span class="not-set">pending</span>' ?></td>
                            <td>&nbsp;&nbsp;&nbsp;
                                <a class="aclink confirm" style="font-size:22px; padding-left: 10px;"
                                   href="<?= Url::to(['core-student/delete-file', 'id' => $v['id']]) ?>"
                                   data-method="post"
                                   data-confirm="Confirm to delete file"><i class="fas fa-window-close text-danger"></i></a>

                            </td>
                        </tr>
                    <?php endforeach;
                else : ?>
                    <tr>
                        <td colspan="8">You have no uploaded files.</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div style="clear: both"></div>
    </div>
</div>
<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Url;

$this->title = 'Uploaded student files';
?>
    <div class="letters">
        <div class="col-md-12">
            <?php if (Yii::$app->session->hasFlash('fileListAlert')) : ?>
                <div class="notify notify-success">
                    <a href="javascript:" class='close-notify' data-flash='fileListAlert'>&times;</a>
                    <div class="notify-content">
                        <?= Yii::$app->session->getFlash('fileListAlert'); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>


        <div class="col-md-12">
            <div>
                <div class="col-md-3"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>

                <div class="col-md-8 "><?php echo $this->render('_uploaded_files_search', ['model' => $searchModel]); ?></div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="">

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th><?= $sort->link('file_name') ?></th>
                        <th><?= $sort->link('description') ?></th>
                        <th><?= $sort->link('date_uploaded') ?></th>
                        <th><?= $sort->link('username') ?></th>
                        <th><?= $sort->link('school_name') ?></th>
                        <th><?= $sort->link('class_code') ?></th>
                        <th><?= $sort->link('imported') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($model) :
                        foreach ($model as $k => $v) : ?>
                            <?php
                            $classCode = '-';
                            $classCode = $v['class_code'] ? $v['class_code'] : $classCode;
                            if ($v['destination_class'] == '0') $classCode = '**Class is defined per record**';
                            ?>
                            <tr>
                                <td><?= $k + 1 ?></td>
                                <td class="clink"><a
                                            href="<?= Url::to(['/schoolcore/student-upload/file-details', 'id' => $v['id']]) ?>"><?= ($v['file_name']) ? $v['file_name'] : '<span class="not-set">(not set) </span>' ?></a>
                                </td>
                                <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['date_uploaded']) ? date('M d, Y - g:i:s A', strtotime($v['date_uploaded'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['username']) ? $v['username'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($classCode) ? $classCode : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['imported']) ? "<i class='fa  fa-check'></i>" : '<span class="not-set">pending</span>' ?></td>
                                <td><a href="javascript:"
                                       onclick="clinkConf('<?= Url::to(['/schoolcore/student-upload/delete-file', 'id' => $v['id']]) ?>', 'Are you sure you want to delete this file')"
                                       title="Delete"><span class="fa  fa-remove"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        <?php endforeach;
                    else : ?>
                        <tr>
                            <td colspan="8">You have no uploaded files.</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>
<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>
<?php

use app\models\SchoolInformation;
use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClassesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class=" letters">

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/schoolcore/student-upload/uploaded-files']),
        'method' => 'post',
    ]); ?>

    <div class="col-md-9">
        <?php
        $url = Url::to(['/schoolcore/core-school/active-schoollist']);
        $selectedSchool = empty($model->schsearch) ? '' : CoreSchool::findOne($model->schsearch)->school_name;
        echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
            'initValueText' => $selectedSchool, // set the initial display text
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => 'Filter School',
                'id' => 'school_search',
                'class' => 'form-control',
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
            ],
        ])->label(false); ?>
    </div>

    <div class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-primary']) ?></div>

    <?php ActiveForm::end(); ?>

</div>

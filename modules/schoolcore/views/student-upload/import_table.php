<?php

use yii\helpers\Url;

?>

<div class="row">
    <div class="col-md-12">
        <?php if (!$imported['imported'] && $imported['uploaded_by'] == Yii::$app->user->identity->id) : ?>
            <p style="font-size:15px;color:grey;"><b>Note:</b> This file will be approved and imported in the student's
                database by <b>another admin</b>. For more information contact support</p>
        <?php elseif (!$imported['imported'] && (Yii::$app->user->can('approve_students')) && ($imported['uploaded_by'] != Yii::$app->user->identity->id)) : ?>
            <p style="font-size:15px;color:grey;"><b>Upload Notice:</b> Hello Admin, Please Confirm data before import
                for any irregularities.</p>
        <?php elseif ($imported['imported']) : ?>
            <p style="font-size:15px;color:grey;"><b>Notice:</b> This data is already imported. You are free to remove
                it from our temporary database</p>
        <?php elseif (!$imported['imported'] && $imported['uploaded_by'] != Yii::$app->user->identity->id) : ?>
            <p style="font-size:15px;color:grey;"><b>Note:</b> This file is not yet approved. <b>An admin</b> will
                approve and import this data into the student's database. For more information contact support</p>
        <?php endif; ?>
    </div>
</div>

<?php
if ($status['state']) {
    echo $status['message'];
}
?>

<div class="row">
    <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">

            <table class="table" style="background-color:#fff; margin-bottom:3px;">
                <tr>
                    <th>
                        <span style="font-size:17px;"><strong>&nbsp;<?= $imported['description'] . " (" . $imported['school_name'] . ")"; ?></strong></span>
                    </th>

                    <th class="pull-right no-padding">
                        <button class="btn btn-danger"
                                onclick="clinkConf('<?= Url::to(['/schoolcore/student-upload/delete-file', 'id' => $file]) ?>', 'Are you sure you want to delete this file')">
                            <b>Remove file <i class="fa fa-trash-o"></i></b>&nbsp; &nbsp;
                        </button>
                    </th>
                    <?php if (!$imported['imported'] && Yii::$app->user->can('approve_students') && $imported['uploaded_by'] != Yii::$app->user->identity->id): ?>
                        <th class="pull-right no-padding"><a class='aclink'
                                                             href="<?= Url::to(['/schoolcore/student-upload/file-details', 'id' => $file]) ?>"
                                                             data-confirm="Please confirm to save students"
                                                             data-method="post" data-params='{"action":"save"}'><span
                                        class="btn btn-primary "><b>Click to Submit Students <i class="fa fa-save"></i> </b>&nbsp; &nbsp;</span></a>
                        </th>
                    <?php endif; ?>
                    <th class="clink pull-right no-padding"><a
                                href="<?= Url::to(['/schoolcore/student-upload/uploaded-files']) ?>">
                            <button class="btn btn-info btn-sm"><b><i class="fa fa-arrow-circle-left"></i> Back</b>&nbsp; &nbsp;</button>
                        </a></th>
                </tr>
            </table>
            <div class="">
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>DoB</th>
                        <th>Reg No</th>
                        <th>Gender</th>
                        <th>Student Email</th>
                        <th>Student Phone</th>
                        <th>Nationality</th>
                        <th>Disability</th>
                        <th>Disability Nature</th>
                        <th>Guardian Name</th>
                        <th>Guardian Relation</th>
                        <th>Guardian Email</th>
                        <th>Guardian Phone</th>
                        <th>D/B</th>
                        <th>Outstanding Balance</th>
                        <th>Class Code</th>
                        <th>Campus</th>
                        <th>&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $srNo = 1;
                    foreach ($data as $k => $v) : ?>
                        <?php
                        $classCode = $v['file_class_code'] ? $v['file_class_code'] : '-';
                        $classCode = $v['file_destination_class'] == '0' && $v['import_class_code'] ? $v['import_class_code'] : $classCode;
                        ?>
                        <tr>
                            <td><?= $srNo; ?></td>
                            <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['middle_name']) ? $v['middle_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['dob']) ? $v['dob'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['reg_no']) ? $v['reg_no'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['student_email']) ? $v['student_email'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['nationality']) ? $v['nationality'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['disability']) ? 'Yes' : 'No' ?></td>
                            <td><?= ($v['disability_nature']) ? $v['disability_nature'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gurdian_name']) ? $v['gurdian_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['guardian_relation']) ? $v['guardian_relation'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gurdian_email']) ? $v['gurdian_email'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gurdian_phone']) ? $v['gurdian_phone'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['day_boarding']) ? $v['day_boarding'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['outstanding_balance']) ? $v['outstanding_balance'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($classCode) ? $classCode : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['campus_name']) ? $v['campus_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td class="clink"><a
                                        href="<?= Url::to(['/schoolcore/student-upload/file-details', 'id' => $file]) ?>"
                                        , data-confirm="Are you sure you want to delete this student" data-method="post"
                                        , data-params='{"action":"del_student", "stu":"<?= $v['id'] ?>"}'><i
                                            class="fa  fa-remove"></i>&nbsp;&nbsp;&nbsp;</a>
                            </td>
                        </tr>
                        <?php
                        $srNo++;
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

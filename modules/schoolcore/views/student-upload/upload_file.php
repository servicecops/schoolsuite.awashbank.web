<?php

use app\modules\schoolcore\models\SchoolCampuses;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="row hd-title" data-title="Upload Excel">
        <div class="col-md-12 col-lg-12 no-padding">
            <p>All fields are marked * are required</p>
            <?php $form = ActiveForm::begin(['id' => 'UploadExcel',
                'options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="col-sm-2 col-md-2">
                <?= $form->field($model, 'importFile', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'File']])->fileInput()->label('Choose file *') ?>
            </div>
            <div class="col-sm-2 col-md-2">
                <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'Description']])->textInput()->label('Description *') ?>
            </div>
            <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
                <div class="col-md-2 col-sm-2 col-md-2">
                    <?php
                    $url = Url::to(['/schoolcore/core-school/active-schoollist']);
                    $selectedSchool = empty($model->school) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->school)->school_name;
                    echo $form->field($model, 'school')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
                        'size' => 'sm',
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => 'Filter School',
                            'id' => 'school_search',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],

                        ],
                    ])->label('School *'); ?>
                </div>
            <?php endif; ?>

            <div class="col-md-2 col-sm-2 col-md-2 class_behaviour_container"><?= $form->field($model, 'class_import_behaviour', ['inputOptions' => ['class' => 'form-control input-sm inputRequired class_import_behaviour', 'placeholder' => 'Choose Class Source']])->dropDownList(
                    [
                        'FROM_EXCEL' => 'Import To Respective Classes In Excel',
                        'SINGLE_EXISTING' => 'Import To Existing Class',
                    ], ['prompt' => 'Choose Class Import Behavior']
                )->label('Import to class behaviour *') ?></div>

            <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
                <div class="col-md-2 col-sm-2 col-md-2 destination_class_container"><?= $form->field($model, 'student_class', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Students Class', 'id' => 'upload_to_class']])->dropDownList(
                        ['' => 'Filter class'])->label('Student Class') ?></div>
            <?php elseif (\app\components\ToWords::isSchoolUser()) : ?>
                <div class="col-md-2 col-sm-2 col-md-2 destination_class_container"><?= $form->field($model, 'student_class', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Students Class']])->dropDownList(
                        ArrayHelper::map(\app\modules\schoolcore\models\CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->orderBy('class_code')->all(), 'id', 'class_description'), ['prompt' => 'Filter class']
                    )->label('Student Class') ?></div>
            <?php endif; ?>


            <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
                <div class="col-md-2 col-sm-2 col-md-2 destination_campus_container"><?= $form->field($model, 'campus_id', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Campus', 'id' => 'upload_to_campus']])->dropDownList(
                        ['' => 'Filter Campus'])->label('Campus') ?></div>
            <?php elseif (\app\components\ToWords::isSchoolUser()) : ?>
                <div class="col-md-2 col-sm-2 col-md-2 destination_campus_container"><?= $form->field($model, 'campus_id', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Campus']])->dropDownList(
                        ArrayHelper::map(SchoolCampuses::find()->where(['school_id' => Yii::$app->user->identity->school_id])->orderBy('campus_name')->all(), 'id', 'campus_name'), ['prompt' => 'Filter Campus']
                    )->label('Campus') ?></div>
            <?php endif; ?>


            <div class="col-sm-2" style="margin-top: 7px;">
                <br>
                <?= Html::submitButton("Upload File <i class='fa fa-upload'></i>", ['class' => "btn btn-primary "]) ?>
            </div>


            <?php ActiveForm::end(); ?>


        </div>
    </div>

<?php if (isset($error)) : ?>
    <div class="row">
        <div class='alert alert-danger'><?= $error ?></div>
    </div>
<?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div id="loading"></div>
            <div id="errorMessage"></div>
            <div id="response">
                <div class="col-md-12 no-padding">
                    <p style="font-size:15px;color:grey;"><b>Note:</b> Only excel data sheets that follow the <b>template
                            format provided by schoolPay</b> will be allowed.
                        <b>**And follow column Order !important**</b> <a
                                href="<?= \Yii::getAlias('@web/uploads/schPay_data_import_template.xlsx'); ?>"
                                target="_blank">download template</a></p>
                    <table class="table table-striped" style="font-size:12px;">
                        <thead>
                        <tr>

                            <th>First Name</th>
                            <th>Middle Name</th>
                            <th>Last Name</th>
                            <th>DoB</th>
                            <th>Reg No</th>
                            <th>Gender</th>
                            <th>Student Email</th>
                            <th>Student Phone</th>
                            <th>Nationality</th>
                            <th>Disability</th>
                            <th>Disability Nature</th>
                            <th>Guardian Name</th>
                            <th>Guardian Relation</th>
                            <th>Guardian Email</th>
                            <th>Guardian Phone</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                            <td>John</td>
                            <td>--</td>
                            <td>Musoke</td>
                            <td>1990-09-23
                            </td>
                            <td>--</td>
                            <td>M</td>
                            <td>--</td>
                            <td>--</td>
                            <td>Ethipia</td>
                            <td>FALSE</td>
                            <td>--</td>
                            <td>James Musoke</td>
                            <td>Father</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        <tr>
                            <td>Required</td>
                            <td>Optional</td>
                            <td>Required</td>

                            <td>Required</td>
                            <td>Optional</td>
                            <td>Required</td>
                            <td>Optional</td>
                            <td>Optional</td>
                            <td>Required</td>
                            <td>Required</td>
                            <td>dep-prev</td>
                            <td>Optional</td>
                            <td>dep-prev</td>
                            <td>Optional</td>
                            <td>Optional</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

<?PHP

$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->student_class;

$campusurl = Url::to(['/schoolcore/core-school/campuslists']);
$campus = $model->campus_id;

$script = <<< JS


$("document").ready(function(){
function evaluateClassesVisibility() {
if($('.class_import_behaviour').val() === 'SINGLE_EXISTING') {
$('.destination_class_container').show()
}else {
$('.destination_class_container').hide()
}
}

function populateClassesAndCampuses() {
 var school = $('#school_search').val();   
 if(!school) return;
$.get('$url', {id : school}, function( data ) {
            $('#upload_to_class').html(data);
        });
        
        $.get('$campusurl', {id : school}, function( data ) {
            $('#upload_to_campus').html(data);
        });
}


evaluateClassesVisibility();
populateClassesAndCampuses();

//On changing fee type, hide or show recurrent controls
$('body').on('change', '.class_import_behaviour', function(){
evaluateClassesVisibility();
});


    $('body').on('change', '#school_search', function(){
        populateClassesAndCampuses();
        
    });


});



JS;
$this->registerJs($script);
?>

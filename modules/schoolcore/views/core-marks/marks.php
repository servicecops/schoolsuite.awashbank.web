<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreMarks */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
//$studentId = $model['id'];
?>

<div class="letter">
    <div class="core-school-view">
        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-plus"></i> Add Student Marks</span></div>
        <div class="col-md-8 ">
        <?= $this->render('search', ['model' => $searchModel, 'id'=>$id]); ?>
        </div>
        <?php $form = ActiveForm::begin([
            'action' => ['marks', 'id'=>$id],
            'method' => 'post',
            'options' => ['class'=>'formprocess']
        ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => $columns,
                'floatHeaderOptions'=>['top'=>'50'],
                'responsive'=>true,
                'hover'=>true,
                'bordered' => false,
                'striped' => true,
                'options' => [
                    'class' => 'bg-colorz',
                ],
            ]); ?>

        <div class="row">
            <?= Html::submitButton('<i class="fa fa-save" style="color:#fff;"> Submit Marks</i>', ['class' => 'btn btn-primary btn-sm']) ?>
        </div>

    </div>
    <?php ActiveForm::end(); ?>


</div>



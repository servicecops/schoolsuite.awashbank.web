<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options'=>['class'=>'formprocess modal_form', 'id'=>'adjust_bal_form_modal']
]); ?>
    <div>check</div>

<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
    $('form#adjust_bal_form_modal').yiiActiveForm('validate');
      $("form#adjust_bal_form_modal").on("afterValidate", function (event, messages) {
          if($(this).find('.has-error').length) {
                        return false;
                } else {
                    $('.modal').modal('hide');
                }
        });
   });
JS;
$this->registerJs($script);
?>

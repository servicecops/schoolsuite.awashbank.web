<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */

/* @var $form yii\widgets\ActiveForm */
?>
<div class="letter">

    <div id="row">
        <div><h3>Upload Student Marks</h3></div>
        <table class="table" style="background-color:#fff; margin-bottom:3px;">
            <?php $form = ActiveForm::begin([
                'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]); ?>
            <tr>

                <th>            <?= $form->field($model, 'importFile', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'File']])->fileInput()->label(false) ?>
                </th>
                <th>            <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'Description']])->textInput()->label(false) ?>
                </th>

                <th>
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                    <?php if (\app\components\ToWords::isSchoolUser()) : ?>

                        <div class="col-md-12">
                            <?php
                            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                            echo $form->field($model, 'student_class')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                                'language' => 'en',
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Find Class'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false); ?>
                        </div>

                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>

                    <div class="row">
                        <div class="col-sm-6 ">
                            <?php
                            $url = Url::to(['core-school/active-schoollist']);
                            $selectedSchool = empty($model->school) ? '' : CoreSchool::findOne($model->school)->school_name;
                            echo $form->field($model, 'school')->widget(Select2::classname(), [
                                'initValueText' => $selectedSchool, // set the initial display text
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'placeholder' => 'Filter School',
                                    'id' => 'school_search',
                                    'class' => 'form-control',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                ],
                            ])->label(false); ?>

                        </div>

                        <div class="col-sm-6">
                            <?= $form->field($model, 'student_class', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
                                ['prompt' => 'Filter class'])->label(false) ?>            </div>


                        <?php endif; ?>
                    </div>

                </th>
                <th>
                    <button class="btn btn-block btn-primary btn-sm">Upload</button>
                </th>
            </tr>
            <?php ActiveForm::end(); ?>
        </table>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div id="loading"></div>
            <div id="errorMessage"></div>
            <div id="response">
                <div class="col-md-12 no-padding">
                    <p style="font-size:15px;color:grey;"><b>Note:</b> Only excel data sheets that follow the <b>template
                            format provided by schoolPay ERP</b> will be allowed.
                        <b>**And follow column Order !important**</p>
                    <table class="table table-striped" style="font-size:12px;">
                        <thead>
                        <tr>
                            <th>Student Code</th>
                            <th>Student Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Marks Obtained</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>9900000087</td>
                            <td>1</td>
                            <td>Ayesiga</td>
                            <td>Gorret</td>
                            <td>90</td>
                        </tr>
                        <tr>
                            <td>Required</td>
                            <th>Required
                            </td>
                            <td>Optional</td>
                            <td>Required</td>
                            <th>Required
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>

<?php
$url = Url::to(['core-school-class/lists']);
$cls = $model->student_class;

$campusurl = Url::to(['/school-information/campuslists']);

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
    
    }
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>




<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreMarksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['marks','id'=>$id],
    'method' => 'get',
    'options' => ['class'=>'formprocess']
]); ?>
<div class="row">
        <div class="col-8">
            <?= $form->field($model, 'studentName')->textInput(['class'=>'form-control form-control-sm', 'placeholder'=>'Search First Name'])->label(false) ?>
        </div>
    <div class="col-4">
        <?= Html::submitButton('<i class="fa fa-search" style="color:#fff;"> Search</i>', ['class' => 'btn btn-primary btn-sm'])  ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

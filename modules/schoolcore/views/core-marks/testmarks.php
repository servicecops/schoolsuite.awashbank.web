<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreMarks */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
//$studentId = $model['id'];
?>


<div class="core-school-view">
    <h2>Add Student Marks</h2>
    <?= $this->render('search', ['model' => $searchModel, 'id'=>$id]); ?>

    <div class="row">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns
        ]); ?>
    </div>


</div>
<?php ActiveForm::end(); ?>



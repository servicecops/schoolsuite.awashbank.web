<?php

use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])?>

<?= FileInput::widget([
    'model' => $model,
    'attribute' => 'uploads[]',
    'options' => ['multiple' => true],
    'pluginOptions' => [
        'previewFileType' => 'any',
        'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'docx', 'doc', 'xls', 'xlsx', 'pdf'],
        'showCancel' => false,
        'showUpload' => false,
        'maxFileCount' => 5,
        'fileActionSettings' => [
            'showZoom' => false,
        ]
    ]
]); ?>
    <button>Submit</button>

<?php ActiveForm::end() ?>
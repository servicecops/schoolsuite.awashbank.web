<?PHP
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<br>
<div class="row">
    <table class="table" >
        <thead>
        <tr><th colspan="4"><span class="lead">Attached Files</span></th></tr>
        <tr><th> File name</th><th><div class="float-right">
                    <?php if(Helpers::can('manage_files')) : ?>View / Remove<?php endif; ?></div></th></tr>
        </thead>
        <tbody>
        <?php
        if($uploads) :
            foreach($uploads as $k=>$v):
                $name = explode( '.', $v['file_name'] );
                $ext = $name[count($name)-1];
                $target = in_array($ext, ['png', 'jpg', 'jpeg', 'gif']) ? '_blank' : '_self';
                ?>
                <tr>
                    <td><?= $v['file_name'] ?></td>

                </tr>
            <?php endforeach;
        else :
            ?>
            <tr><td colspan="4">No school contacts provided</td></tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>


<?php
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/*
 * @var $dataProvider yii\data\ActiveDataProvider ;
 */

$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";
?>

<?= GridView::widget([
    'layout' => $layout,
    'dataProvider' => $dataProvider,
    //  'filterModel' => $searchModel,
    //  'columns' => 3,
    'columns' => $columns,
    'tableOptions' => [
        'class' => $tableOptions,
    ],
    'pager' => [

    ],

]);



?>


<?php

use yii\helpers\Url;

?>

<p style="color:#F7F7F7">Upload Test Results</p>
<div class="letter">


    <?php
    if ($status['state']) {
        echo $status['message'];
    }
    ?>

    <div class="row">
        <div class="col-md-12">
            <div id="loading"></div>
            <div id="errorMessage"></div>
            <div id="response">

                <table class="table" style="background-color:#fff; margin-bottom:3px;">
                    <tr>
                        <th class="pull-right no-padding"><a class='aclink'
                                                             href="<?= Url::to(['/schoolcore/core-marks/file-details', 'id' => $file]) ?>"
                                                             data-confirm="Please confirm to save students"
                                                             data-method="post" data-params='{"action":"save"}'><span
                                        class="btn btn-primary btn-sm"><b>Click to Submit Students </b>&nbsp; &nbsp;</span></a>
                        </th>

                    </tr>

                </table>
                <div class="box-body table table-responsive no-padding">
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Student ID</th>
                            <th>Marks Obtained</th>
                            <th>Test ID</th>
                            <th>&nbsp;&nbsp;&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $srNo = 1;
                        foreach ($data as $k => $v) : ?>
                            <tr>
                                <td><?= $srNo; ?></td>
                                <td><?= ($v['student_id']) ? $v['student_id'] : '<span class="not-set"> -- </span>' ?></td>
                                <td><?= ($v['marks_obtained']) ? $v['marks_obtained'] : '<span class="not-set"> -- </span>' ?></td>
                                <td><?= ($v['test_id']) ? $v['test_id'] : '<span class="not-set"> -- </span>' ?></td>
                                <td></td>
                            </tr>
                            <?php
                            $srNo++;
                        endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
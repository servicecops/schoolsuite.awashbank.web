<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Tests Info Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Student Code</th><th>First Name</th><th>Last Name</th><th>Middle Name</th><th>Class Description</th><th>Marks Obtained</th></tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['student_code']) ? $sinfo['student_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['first_name']) ? $sinfo['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['last_name']) ? $sinfo['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['middle_name']) ? $sinfo['middle_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['class_description']) ? $sinfo['class_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['marks_obtained']) ? $sinfo['marks_obtained'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

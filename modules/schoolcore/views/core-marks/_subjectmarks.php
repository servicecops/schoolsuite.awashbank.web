<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


/*
 * @var $dataProvider yii\data\ActiveDataProvider ;
 */

$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";
?>

<div class="float-right">

  <!--  --><?php

    /*
       echo ExportMenu::widget([
           'dataProvider' => $dataProvider,
           'columns' => $columns,
           'exportConfig' => [
               ExportMenu::FORMAT_TEXT => false,
               ExportMenu::FORMAT_HTML => false,
               ExportMenu::FORMAT_EXCEL => false,
               ExportMenu::FORMAT_PDF => [
                   'pdfConfig' => [
                       'methods' => [
                           'SetTitle' => 'Grid Export - Krajee.com',
                           'SetSubject' => 'Generating PDF files via yii2-export extension has never been easy',
                           'SetHeader' => ['Krajee Library Export||Generated On: ' . date("r")],
                           'SetFooter' => ['|Page {PAGENO}|'],
                           'SetAuthor' => 'Kartik Visweswaran',
                           'SetCreator' => 'Kartik Visweswaran',
                           'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, GridView, Grid, yii2-grid, yii2-mpdf, yii2-export',
                       ]
                   ]
               ],
           ],
           'dropdownOptions' => [
               'label' => 'Export',
               'class' => 'btn btn-outline-secondary'
           ]
       ])
           */
    $title ='Student Marks';
    ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php


    echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
        'class'=>'btn btn-sm btn-danger',
        'target'=>'_blank',
        'data-toggle'=>'tooltip',
        'title'=>'Will open the generated PDF file in a new window'
    ]);
    echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
        'class'=>'btn btn-sm btn-success',
        'target'=>'_blank'
    ]);

    ?>

</div>



<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
    'floatHeaderOptions'=>['top'=>'50'],
    'responsive'=>true,
    'hover'=>true,
    'bordered' => false,
    'striped' => true,
    'options' => [
        'class' => 'bg-colorz',
    ],

]); ?>


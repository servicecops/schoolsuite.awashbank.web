<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-school-form form-horizontal">
    <div class="row card">
        <div class="card-header">
            <strong><?= $this->title ?></strong>
        </div>
        <div class="card-body">
            <?php $form = ActiveForm::begin([
                'fieldConfig' => function ($model, $attribute) {
                    if (true) {
                        return [
//                            'template' => "{label}{input}\n{hint}\n{error}",
                            'options' => ['class' => 'form-group'],
                            'labelOptions' => ['class' => ''],
                            'inputOptions' => ['class' => 'form-control']
                        ];
                    }

                    return ['options' => ['class' => 'form-group']];
                },
            ]);
            ?>

            <?= $form->field($model, 'date_created')->textInput() ?>
            <?= $form->field($model, 'created_by')->textInput() ?>
            <?= $form->field($model, 'username')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

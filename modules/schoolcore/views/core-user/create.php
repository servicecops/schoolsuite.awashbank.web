<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreUser */

$this->title = 'Create Core School';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

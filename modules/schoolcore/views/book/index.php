<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\schoolcore\models\Book */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            'date_created',
//            'date_updated',
//            'created_by',

            [
                'label' => 'Book Title',
                'value'=>function($model){
                    return Html::a($model['book_title'], ['book/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
//            [
//                'attribute'=>'Category',
//                'value'=>function($model){
//                    return $model->category->category_name;
//
//                },
//                'format'=>'html',
//
//            ],

            'author',
            'publisher',
            'shelf_number',
            [
                'attribute'=>'Total',
                'value'=>function($model){
                    return $model->totalBooks;

                },
                'format'=>'html',

            ],
            i

            ['class' => 'kartik\grid\ActionColumn'],
        ],
        'resizableColumns'=>true,
        'responsiveWrap' => false,
        //    'floatHeader'=>true,

        'responsive'=>false,
        'bordered' => false,
        'striped' => true,
    ]); ?>


</div>

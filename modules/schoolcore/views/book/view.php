<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\Book */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<p class="float-right">
    <?= Html::a('change cover', ['book-cover', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to    </div>
s item?',
            'method' => 'post',
        ],
    ]) ?>
</p>
<div class="component">
    <ul class="align">

        <!-- Book 3 -->
        <li>
            <figure class='book'>
                <!-- Front -->
                <ul class='hardcover_front'>
                    <li>
<!--                        <img src="https://s.cdpn.io/13060/book3.jpg" alt="" width="100%" height="100%">-->

                        <?php if ($model->book_cover): ?>
                            <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->bookCover->image_base64 ?>"
                                 height="100%" width="100%"/>
                        <?php else : ?>
                            <?= Html::img('@web/web/img/dummy_book_cover.png', ['alt' => 'cover','width' => '100%','height' => '100%']) ?>
                        <?php endif; ?>
                    </li>
                    <li></li>
                </ul>
                <!-- Pages -->
                <ul class='page'>
                    <li></li>
                    <li>
                    </li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                <!-- Back -->
                <ul class='hardcover_back'>
                    <li></li>
                    <li></li>
                </ul>
                <ul class='book_spine'>
                    <li></li>
                    <li></li>
                </ul>
                <figcaption>
                    <h4><?=$model->book_title;?></h4>
                    <span>By <?=$model->author;?></span>
                    <p>Publisher: <?=$model->publisher;?></p>
                    <p>Category: <?=$model->category;?></p>
                    <p>Shelf Number: <?=$model->shelf_number;?></p>
                </figcaption>
            </figure>
        </li>
    </ul>
</div>

<div class="book-view">


<div class="row">
    <div class="card w-100">
        <h3 class="ml-4 mt-2">Copies</h3>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'ASN_No',

                    [
                        'attribute'=>'ISBN_number',
                        'value'=>function($model){
                            return Html::img('@web/web/img/barcode.png', ['alt' =>'barcode','height'=>20])
                                .'<br>'.$model->ISBN_number;
                        },
                        'format'=>'html',

                    ],

                    [
                        'attribute'=>'Status',
                        'value'=>function($model){
                            if ($model->available == true){
                                return '<span
                        class="badge bg-primary font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">Available</span>';
                            }   else{
                                return '<span
                        class="badge bg-danger font-12 text-white font-weight-medium badge-pill ml-2 d-md-none d-lg-block">Issued</span>';
                            }

                        },
                        'format'=>'html',

                    ],

                    [
                        'attribute'=>'Issue/Return',
                        'value'=>function($model){
                            if ($model->available==true){
                                return Html::a('<i class="fa  fa-arrow-alt-circle-right"></i>Issue', ['/schoolcore/book-copy/issue-book', 'id'=>$model->id]);

                            } else {
                                return Html::a('<i class="fa  fa-arrow-alt-circle-left text-dark"></i><span class="text-dark">Return</span>', ['/schoolcore/book-copy/return-book', 'id'=>$model->id]);

                            }
                        },
                        'format'=>'html',
                    ],
//            [
//                'attribute'=>'Issued To',
//                'value'=>function($model){
//                    return $model->totalBooks;
//
//                },
//                'format'=>'html',
//
//            ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'color:#337ab7'],
                        'template' => '{view}{update}{delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<i class="fa fa-eye"></i>',['book-copy/view','id' => $model->id], [
                                    'title' => Yii::t('app', 'view'),
                                ]);
                            },

                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-edit"></i>', ['book-copy/update','id' => $model->id], [
                                    'title' => Yii::t('app', 'update'),
                                ]);
                            },
                            'delete' => function ($url, $model) {
                                return Html::a('<span class="fa fa-trash"></span>', ['book-copy/delete','id' => $model->id], [
                                    'title' => 'delete',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                            }

                        ],

                    ],
                ],
                'resizableColumns'=>true,
                'responsiveWrap' => false,
                //    'floatHeader'=>true,

                'responsive'=>false,
                'bordered' => false,
                'striped' => true,
            ]); ?>
        </div>
    </div>

</div>
</div>

<?php

use  wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>
<div class=" card bg-white">
<div class=" card-body book-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'book_title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'category')->dropDownList(
                ArrayHelper::map(\app\modules\schoolcore\models\BookCategory::find()->orderBy('category_name')->all(), 'id', 'category_name'), ['prompt' => 'Select book category ..']
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'publisher')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col">
            <?php if($model->isNewRecord): ?>
            <?= $form->field($model, 'school_id')->hiddenInput(['value'=> Yii::$app->user->identity->school_id])->label(false) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'shelf_number')->textInput() ?>
        </div>
        <div class="col">

        </div>
    </div>



    <div class="row">
        <div class="row card" style="width: 100%">
            <div class="card-header" style="background-color: black !important;"><h4><i class="fa fa-file"></i> Add book copies</h4></div>
            <div class="card-body">
                <?php DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 4, // the maximum times, an element can be cloned (default 999)
                    'min' => 1, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelBookCopies[0],
                    'formId' => 'dynamic-form',
                    'formFields' => [
                        'ISBN_number',
                        'ASN_No'
                    ],
                ]); ?>

                <div class="container-items"><!-- widgetContainer -->
                    <?php foreach ($modelBookCopies as $i => $modelBookCopies): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <div class="pull-right">
                                    <button type="button" class="add-item btn btn-success btn-xs"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                // necessary for update action.
                                if (! $modelBookCopies->isNewRecord) {
                                    echo Html::activeHiddenInput($modelBookCopies, "[{$i}]id");
                                }
                                ?>

                                <div class="row">
                                    <div class="col">
                                        <?= $form->field($modelBookCopies, "[{$i}]ISBN_number")->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col">
                                        <?= $form->field($modelBookCopies, "[{$i}]ASN_No")->textInput(['maxlength' => true]) ?>
                                    </div>
                                </div>
                                <!-- .row -->
                            <!-- .row -->
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php DynamicFormWidget::end(); ?>
            </div>
    </div>



    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

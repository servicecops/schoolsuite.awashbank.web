<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Term Info Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Term Name</th><th>Term description</th><th>Beginning of term Date</th><th>End of term Date</th><th>School Name</th></tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['term_name']) ? $sinfo['term_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['term_description']) ? $sinfo['term_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['term_starts']) ? $sinfo['term_starts'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['term_ends']) ? $sinfo['term_ends'] : '<span class="not-set">(not set) </span>' ?></td>
                   <!-- <?php /*if(Yii::$app->user->can('rw_sch')) : */?>
                        <td><?/*= ($sinfo['school_name']) ? $sinfo['school_name'] : '<span class="not-set">(not set) </span>' */?></td>
                    --><?php /*endif; */?>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

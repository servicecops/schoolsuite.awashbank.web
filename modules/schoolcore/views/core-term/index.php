<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Term List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letters">



    <?php

    $columns = [
        [
            'label' => 'Term Name',
            'value'=>function($model){
                return Html::a($model['term_name'], ['core-term/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'term_type',
        'term_type_detail',
        'term_starts',
        'term_ends',
        [
            'label' => 'School Name',
            'value' => function ($model) {
                return $model['school_name'];
            },
        ],
        'date_created',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-term/view/', 'id' => $model['id']]);
            },
        ],
        ////
    ];
    ?>


        <div class="row" style="margin-top:20px">

            <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            <div class="col-md-5 ">


                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
            <div class="col-md-4">


                <div class=" row float-right">
                    <?php if (Yii::$app->user->can('rw_term')) : ?>
                        <?= Html::a('Add new Term <i class="fa far fa-plus"></i>', ['create'], ['class' => 'btn btn-sm btn-primary  float-right']) ?>
                    <?php endif; ?>
                    <?php
                    echo Html::a('<i class="fa far fa-file-pdf-o"></i> Pdf', ['core-term/export-to-pdf', 'model' => get_class($searchModel)], [
                        'class'=>'btn btn-md btn-danger',
                        'target'=>'_blank',
                        'data-toggle'=>'tooltip',
                        'title'=>'Will open the generated PDF file in a new window'
                    ]);
                    echo Html::a('<i class="fa far fa-file-excel-o"></i> Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                        'class'=>'btn btn-md btn-success',
                        'target'=>'_blank'
                    ]);
                    ?>


                </div>
                <div class="row">

                </div>

            </div>
        </div>









    <div class="mt-3">
        <div class="float-right">

        </div>

        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
//    'filterModel' => $searchModel,
            'columns' => $columns,
            'resizableColumns'=>true,
//    'floatHeader'=>true,
            'responsive'=>true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>


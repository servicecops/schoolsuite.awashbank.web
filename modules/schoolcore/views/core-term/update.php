<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchoolClass */

$this->title = 'Update Term: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Core Term', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-school-class-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_term', [
        'model' => $model,
    ]) ?>

</div>

<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = 'Details for';
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div style="margin-top:20px"></div>
<div class="letter">

    <div class="col" style="z-index: 1; padding-bottom: 100px; margin-top:-10px;">
        <div class="float-right">

                    <?= Html::a('<i class="fa fa-edit" ></i> <span >Edit Details</span>', ['update', 'id' => $model->id], ['class' => 'btn btn-md btn-primary ']) ?>

        </div>
    </div>
    <div class="col-12 justify-content-center sch-title">

        <span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title.' '.$model->term_name  ?></span>

    </div>
    <hr class="l_header">



<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text"><?= $model->getAttributeLabel('term_name') ?></div>
            <div class="col-6 profile-text"><?= ($model->term_name) ? $model->term_name : "--" ?></div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text"><?= $model->getAttributeLabel('term_description') ?></div>
            <div class="col-6 profile-text"><?= ($model->term_description) ? $model->term_description : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text">Term Starts</div>
            <div class="col-6 profile-text"><?= ($model->term_starts) ? $model->term_starts : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text">Term Ends</div>
            <div class="col-6 profile-text"><?= ($model->term_ends) ? $model->term_ends : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text"><?= $model->getAttributeLabel('date_created') ?></div>
            <div class="col-6 profile-text"><?= ($model->date_created) ? $model->date_created : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text"><?= $model->getAttributeLabel('term_ends') ?></div>
            <div class="col-6 profile-text"><?= ($model->term_ends) ? $model->term_ends : "--" ?></div>
        </div>
    </div>
</div>
    <div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text">Term Type</div>
            <div class="col-6 profile-text"><?= ($model->term_type) ? $model->term_type : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-6 profile-text">Term Type Detail</div>
            <div class="col-6 profile-text"><?= ($model->term_type_detail) ? $model->term_type_detail : "--" ?></div>
        </div>
    </div>
</div>


</div>
<br>

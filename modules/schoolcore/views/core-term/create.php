<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchoolClass */

$this->title = 'Create Term ';
$this->params['breadcrumbs'][] = ['label' => 'Core Term', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="core-school-class-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_term', [
        'model' => $model,
    ]) ?>

</div>

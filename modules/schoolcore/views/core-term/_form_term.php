<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">


    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <p>All fields marked with * are required</p>


    <div class="row">

        <div class="col-sm-6 ">
            <?= $form->field($model, 'term_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Term Name']])->textInput()->label('Term Name *') ?>


        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'term_description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Term Description ']])->textInput()->label('Term Description *') ?>

        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'term_starts')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'term_starts',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1980:' . (date('Y')),
                        'autoSize' => true,
                        'dateFormat' => 'yyyy-MM-dd',
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('term_starts') ? ' is-invalid' : ''),
                        'placeholder' => 'Beginning of Term'
                    ],])->label('Beginning of Term *') ?>    </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'term_ends')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'term_ends',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1980:' . (date('Y')),
                        'autoSize' => true,
                        'dateFormat' => 'yyyy-MM-dd',
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('term_ends') ? ' is-invalid' : ''),
                        'placeholder' => 'End of Term'
                    ],])->label('End of Term *') ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'term_type', ['inputOptions' => ['class' => 'form-control',
                'placeholder' => 'Term Type',
            ]])->dropDownList(
                ['SEMESTER' => 'Semester', 'QUARTER' => 'Quarter'],
                ['id' => 'term_type_select', 'prompt' => 'Term Type']
            )->label('Term Type') ?>
        </div>

        <div class="col-sm-3" id="semester-settings-container">
            <?= $form->field($model, 'semester_type_detail', ['inputOptions' => ['class' => 'form-control',
                'placeholder' => 'Term Type',
            ]])->dropDownList(
                ['SEMESTER_ONE' => 'Semester One', 'SEMESTER_TWO' => 'Semester Two'],
                ['id' => 'semester_detail_select', ]
            )->label('Semester Detail') ?>
        </div>

           <div class="col-sm-3" id="quarter-settings-container">
            <?= $form->field($model, 'quarter_detail', ['inputOptions' => ['class' => 'form-control',
                'placeholder' => 'Term Type',
            ]])->dropDownList(
                ['QUARTER_ONE' => 'Quarter One', 'QUARTER_TWO' => 'Quarter Two','QUARTER_THREE' => 'Quarter Three', 'QUARTER_FOUR' => 'Quarter Four'],
                ['id' => 'quarter_detail_select', ]
            )->label('Quarter Detail') ?>
        </div>



    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-xs-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update <i class="fa fa-save"></i>', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xs-6">
                <?php if ($model->isNewRecord): ?>
                <?= Html::resetButton('Reset <i class="fa fa-undo"></i>', ['class' => 'btn btn-default btn-block']) ?>
                <?php endif;?>
            </div>


        </div>


    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS



$("document").ready(function(){
function evaluateVisibility() {
if($('#term_type_select').val() === 'SEMESTER') {
$('#semester-settings-container').show()
$('#quarter-settings-container').hide()
}else if($('#term_type_select').val() === 'QUARTER') {
$('#semester-settings-container').hide()
$('#quarter-settings-container').show()
}else{
  $('#semester-settings-container').hide()
$('#quarter-settings-container').hide()  
}


}



evaluateVisibility();


//On changing fee type, hide or show recurrent controls
var _body = $('body');

_body.on('change', '#term_type_select', function(){
evaluateVisibility();
});




});



JS;
$this->registerJs($script);
?>

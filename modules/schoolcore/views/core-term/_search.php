<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;

?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>

<div class="row">


    <div class="col-md-8">
        <?= $form->field($model, 'modelSearch', ['inputOptions' => ['class' => 'form-control'],])->textInput(['title' => 'Enter Term Name or Description ',

            'data-toggle' => 'tooltip',

            'data-trigger' => 'hover',
            'placeholder' => 'Enter Term Name or Description ',

            'data-placement' => 'bottom'])->label(false) ?>

    </div>
    <div class="col-md-4">

        <?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-md']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>


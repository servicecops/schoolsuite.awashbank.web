<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">


    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div class="row">

        <div class="col-sm-6 ">
            <?php
            $url = Url::to(['/schoolcore/core-school/sch-list']);
            $cityDesc = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
            $selectedSchool = $cityDesc;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'options' => ['multiple'=>true, 'placeholder' => 'Search school ...'],
                'initValueText' => $selectedSchool, // set the initial display text
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label('school'); ?>

        </div>

        <div class="col-sm-6">
        </div>


    </div>
    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <br>
        <h4>Class Grades <span style="font-weight: 300; font-size:18px;"> Final grades for your class  ( <span style="font-size: 14px;">click on the + button to add more grades</span> )</span>
        </h4>
        <hr class="l_header">

    </div>
    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <?= $this->render('_classes', ['model' => $model]); ?>

    </div>



    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update <i class="fa fa-save"></i>', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?php if ($model->isNewRecord): ?>
                <?= Html::resetButton('Reset <i class="fa fa-undo"></i>', ['class' => 'btn btn-default btn-block']) ?>
                <?php endif;?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

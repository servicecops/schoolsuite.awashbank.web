<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\IssuedBooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Issued Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="issued-books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Issued Books', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_issued',
            'date_updated',
            'due_date',
            'issued_by',
            //'book_title',
            //'category',
            //'ISBN_number',
            //'author',
            //'publisher',
            //'shelf_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

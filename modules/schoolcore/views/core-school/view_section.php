<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = "Section Assignments : " . $model->section_code . ": " .$model->section_name;
$this->params['breadcrumbs'][] = ['label' => 'Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perms-view hd-title" data-title="Sections Assignment">

 <section class="content-header">
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">    
        <i class="fa fa-institution"></i> <?= Html::encode($this->title) ?>

    </h2>
  </div><!-- /.col -->
</div>
</section>

<div class="class-associate row">
        <div class="col-xs-12 col-lg-12">
            <div class="col-lg-5 col-sm-5 col-xs-12">
                <b>Classes Available</b>:
                <div class="form-group has-feedback">
                    <input name="search_av" type="text" class="form-control perm-search" placeholder="Search..." data-target="available"/>
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
                <?php
                echo Html::listBox('selected', '', $available, [
                    'id' => 'available',
                    'multiple' => true,
                    'size' => 20,
                    'class'=>'form-control',
                    'style' => 'width:100%;height:350px;padding:5px']);
                ?>
            </div>

             <div class="col-lg-2 col-sm-2 col-xs-12 text-center" style="padding-top:100px;">
                <br><br>
                <?php
                echo Html::a('>>', '#', ['class' => 'btn btn-block btn-primary', 'title' => 'Assign', 'data-action' => 'assign']) . '<br><br>';
                echo Html::a('<<', '#', ['class' => 'btn btn-block btn-danger', 'title' => 'Delete', 'data-action' => 'delete']) . '<br>';
                ?>
            <br><br>
            </div>

            <div class="col-lg-5 col-sm-5 col-xs-12">
                <b>Classes Assigned</b>:
                <div class="form-group has-feedback">
                    <input name="search_asgn" type="text" class="form-control perm-search" placeholder="Search..." data-target="assigned"/>
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
                <?php
                echo Html::listBox('selected', '', $assigned, [
                    'id' => 'assigned',
                    'multiple' => true,
                    'size' => 20,
                    'class'=>'form-control',
                    'style' => 'width:100%;height:350px;padding:5px']);
                ?>
            </div>

        </div>
        </div>
        <?php $this->render('_sections_assignment_script',['model'=>$model]); ?>
</div>

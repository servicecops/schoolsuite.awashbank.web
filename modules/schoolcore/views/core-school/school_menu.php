<?php

use app\components\Helpers;
use Faker\Extension\Helper;
use yii\helpers\Url;
$this->title = "School center menu";

function is($role){
//    instead of returning 404 page, return false
//    with false, the whole link will be hidden for the current user.
//    Don't replace the return with the short-hand, false is surely required.
    if (\Yii::$app->user->isGuest){
        return false;
    }

    return \Yii::$app->user->can($role) ? true :false;
}
?>

<style>
    /* RESET STYLES & HELPER CLASSES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    :root {
        --level-1: #4a96d3;
        --level-2: #2984cd;
        --level-3: #29389517;
        --level-4: #4152b01f;
        --level-5: #4152b0;
        --black: ##f1f4f68a;
    }


    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        /*font-family: 'Esphimere', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;*/
    }

    ol {
        list-style: none;
    }

    .rectangle {
        position: relative;
        padding: 10px;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.15);
    }


    /* LEVEL-1 STYLES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .level-1 {
        width: 100%;
        margin: 0 auto 15px;
        background: var(--level-1);
        font-family: "Esphimere", sans-serif;
        background-image: linear-gradient(180deg,#4a96d3 10%,#4a96d3 100%) !important;

    }

    .level-1::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        transform: translateX(-50%);
        width: 2px;
        height: 20px;
        background: var(--black);
        font-family: "Esphimere", sans-serif;
    }


    /* LEVEL-2 STYLES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .level-2-wrapper {
        position: relative;
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        font-family: "Esphimere", sans-serif;
    }

    .level-2-wrapper::before {
        content: "";
        position: absolute;
        top: -20px;
        left: 25%;
        width: 50%;
        height: 2px;
        background: var(--black);
    }

    .level-2-wrapper::after {
        display: none;
        content: "";
        position: absolute;
        left: -20px;
        bottom: -20px;
        width: calc(100% + 20px);
        height: 2px;
        background: var(--black);
    }

    .level-2-wrapper li {
        position: relative;
    }

    .level-2-wrapper > li::before {
        content: "";
        position: absolute;
        bottom: 100%;
        left: 50%;
        transform: translateX(-50%);
        width: 2px;
        height: 20px;
        background: var(--black);
    }

    .level-2 {
        width: 70%;
        margin: 0 auto 15px;
        background: var(--level-2);
        background-image: linear-gradient(180deg,#2C94DC 10%,#287EC8 100%) !important;
    }

    .level-2::before {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        transform: translateX(-50%);
        width: 2px;
        height: 20px;
        background: var(--black);
    }

    .level-2::after {
        display: none;
        content: "";
        position: absolute;
        top: 50%;
        left: 0%;
        transform: translate(-100%, -50%);
        width: 20px;
        height: 40px;
        background: var(--black);
    }


    /* LEVEL-3 STYLES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .level-3-wrapper {
        position: relative;
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-column-gap: 20px;
        width: 90%;
        color: #fff0ff;
        margin: 0 auto;
    }

    .level-3-wrapper::before {
        content: "";
        position: absolute;
        top: -20px;
        left: calc(25% - 5px);
        width: calc(50% + 10px);
        height: 2px;
        background: var(--black);
    }

    .level-3-wrapper > li::before {
        content: "";
        position: absolute;
        top: 0;
        left: 50%;
        transform: translate(-50%, -100%);
        width: 2px;
        height: 20px;
        background: var(--black);
    }

    .level-3 {
        margin-bottom: 5px;
        background: var(--level-3);
        align-items: center;
    }


    /* LEVEL-4 STYLES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .level-4-wrapper {
        position: relative;
        width: 80%;
        margin-left: auto;
        font-family: "Esphimere", sans-serif;
    }

    .level-4-wrapper::before {
        content: "";
        position: absolute;
        top: -20px;
        left: -20px;
        width: 2px;
        height: calc(100% + 20px);
        background: var(--black);
    }

    .level-4-wrapper li + li {
        margin-top: 20px;
    }

    .level-4 {
        background: var(--level-4);
        font-family: "Esphimere", sans-serif;
    }

    .level-4::before {
        content: "";
        position: absolute;
        top: 50%;
        left: 0%;
        font-family: "Esphimere", sans-serif;
        transform: translate(-100%, -50%);
        width: 20px;
        height: 2px;
        background: var(--black);
    }

    /* LEVEL-5 STYLES
      –––––––––––––––––––––––––––––––––––––––––––––––––– */
    .level-5-wrapper {
        position: relative;
        width: 80%;
        margin-left: auto;
    }

    .level-5-wrapper::before {
        content: "";
        position: absolute;
        top: -20px;
        left: -20px;
        width: 2px;
        height: calc(100% + 20px);
        background: var(--black);
    }

    .level-5-wrapper li + li {
        margin-top: 20px;
    }

    .level-5 {
        background: var(--level-5);
        font-family: "Esphimere", sans-serif;
    }

    .level-5::before {
        content: "";
        position: absolute;
        top: 50%;
        font-family: "Esphimere", sans-serif;
        left: 0%;
        transform: translate(-100%, -50%);
        width: 20px;
        height: 2px;
        background: var(--black);
    }

    /* MQ STYLES
    –––––––––––––––––––––––––––––––––––––––––––––––––– */
    @media screen and (max-width: 700px) {
        .rectangle {
            /*padding: 20px 10px;*/
        }

        .level-1,
        .level-2 {
            width: 100%;
            align-items: center;
        }

        .level-1 {
            margin-bottom: 10px;
        }

        .level-1::before,
        .level-2-wrapper > li::before {
            display: none;
        }

        .level-2-wrapper,
        .level-2-wrapper::after,
        .level-2::after {
            display: block;
        }

        .level-2-wrapper {
            width: 90%;
            margin-left: 10%;
        }

        .level-2-wrapper::before {
            left: -20px;
            width: 2px;
            height: calc(100% + 40px);
        }

        .level-2-wrapper > li:not(:first-child) {
            margin-top: 10px;
        }
    }



</style>



<div class="mt-5 pt-5 text-capitalize">
    <!--    top level -->


    <ol class="level-2-wrapper">
        <li>
            <ol class="level-3-wrapper ">
                <li>
                    <h5 class="level-5 rectangle text-center text-light">School</h5>
                    <ol class="level-5-wrapper">
                        <!--                        schools start here -->
                        <?php if (Helpers::is('r_sch') || !Helpers::is('non_student'))   : ?>
                            <li>
                                <a href="<?= Url::to(['/schoolcore/core-school/school']); ?>" class="">
                                    <h6 class="level-3 rectangle"> <i class="fa fa-graduation-cap" aria-hidden="true"></i>About School</h6>
                                </a>
                            </li>
                        <?php endif; ?>
                        <!--                        schools end here -->
                        <!--                            terms start here-->
                        <?php if (Helpers::is('r_term')) : ?>
                            <li >
                                <a href="<?= Url::to(['/schoolcore/core-term/index']); ?>" class="">
                                    <h6 class="level-3 rectangle"><i class="fa fa-wrench" aria-hidden="true"></i> School terms</h6>
                                </a>
                            </li>
                        <?php endif; ?>
                        <!--terms end here-->
                        <!--                        classes start here-->
                        <?php if (Helpers::is('r_class')) : ?>
                            <li>
                                <a href="<?= Url::to(['/schoolcore/core-school-class/menu']); ?>" class="">
                                    <h6 class="level-3 rectangle"><i class="fa fa-group" aria-hidden="true"></i> School classes</h6>
                                </a>
                            </li>
                        <?php endif; ?>
                        <!--                        attendance link stops here-->

                        <?php if (Helpers::is('r_timetable') || !Helpers::is('non_student') ) :?>
                            <!-- school planner -->
                            <li>
                                <a href="<?= Url::to(['/planner/planner/menu']); ?>" class="">
                                    <h6 class="level-3 rectangle "><i class="fa fa-check-square-o" aria-hidden="true"></i> School planner</h6>
                                </a>
                            </li>


                        <?php endif; ?>

                            <li>
                                <a href="<?= Url::to(['/e_learning/elearning/menu']); ?>" class="">
                                    <h6 class="level-3 rectangle "><i class="fa fa-server"></i> E-learning</h6>
                                </a>
                            </li>

                        <?php if (\app\components\ToWords::isSchoolUser() || ( !Helpers::is('non_student') ) ||( !Helpers::is('r_library') )) : ?>
                            <li>
                                <a href="<?= Url::to(['/site/library-menu']); ?>" class="">
                                    <h6 class="level-3 rectangle "><i class="fa fa-book"></i> Library</h6>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['/notice/notice-board']); ?>" class="">
                                    <h6 class="level-3 rectangle "><i class="fa fa-book"></i> Notice Board</h6>
                                </a>
                            </li>
                        <?php endif; ?>

                    </ol>
                </li>

                <li>
                    <h5 class="level-5 rectangle text-center text-light">Subjects &amp; Assessments</h5>
                    <ol class="level-5-wrapper">
                        <!--                        subjects-->
                        <?php if (Helpers::is('r_subject') ||  !Helpers::is('non_student') ) : ?>
                            <?php if (Helpers::is('r_subject')) :?>
                                <li>diss
                                    <a href="<?= Url::to(['/schoolcore/core-subject/display-class']); ?>" class="">
                                        <h6 class="level-4 rectangle">
                                            <i class="fa fa-wrench" aria-hidden="true"></i>   Manage subjects
                                        </h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if ( !Helpers::is('non_student') ) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-subject/student-subject']); ?>" class="">
                                        <h6 class="level-4 rectangle">
                                            <i class="fa fa-user" aria-hidden="true"></i> My subjects
                                        </h6>
                                    </a>
                                </li>
                            <?php endif  ?>

                            <?php if (Helpers::is('rw_subject') && \app\components\ToWords::isSchoolUser()) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-subject/create']); ?>" class="">
                                        <h6 class="level-4 rectangle"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add subjects</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-subject/upload-excel']); ?>" class="">
                                        <h6 class="level-4 rectangle"><i class="fa fa-cloud-download" aria-hidden="true"></i> Import subjects</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endif; ?>

                        <!--                        subjects-->

                        <!--                        tests/exams-->
                        <?php if ( !Helpers::is('non_student') || \app\components\ToWords::isSchoolUser()): ?>

                            <?php if (Helpers::is('rw_test')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-test/display-class']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Test Schedule</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-test/display-class-view']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-plus-circle" aria-hidden="true"></i> Manage Tests</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (!Helpers::is('non_student') ): ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-test/student-tests']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-indent" aria-hidden="true"></i> Class Tests</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                           
                        <?php endif ?>
                        <!--                        end of exams-->

                        <!--                        results-->
                        <?php if (Helpers::is('r_results') ||  !Helpers::is('non_student') ) : ?>
                            <?php if (Helpers::is('rw_results')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-marks/tests']); ?>" class="">
                                        <h6 class="level-4 rectangle ">
                                            <i class="fa fa-wrench" aria-hidden="true"></i>  Manage results
                                        </h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if ( !is('non_student')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-marks/student-results']); ?>" class="">
                                        <h6 class="level-4 rectangle ">
                                            <i class="fa fa-graduation-cap" aria-hidden="true"></i> My results
                                        </h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                        <?php endif; ?>

                        <?php if (Helpers::is('r_grade') ||  !Helpers::is('non_student') ) : ?>
                            <?php if (Helpers::is('rw_grade') && \app\components\ToWords::isSchoolUser() ) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-school-class/add-grades']); ?>" class="">
                                        <h6 class="level-4 rectangle ">
                                            <i class="fa fa-plus-circle"></i> Add grades
                                        </h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if (Helpers::is('r_grade') ) : ?>
                                <li>
                                    <a href="<?= Url::to(['/gradingcore/grading/index']); ?>" class="">
                                        <h6 class="level-4 rectangle ">
                                            <i class="fa fa-wrench" aria-hidden="true"></i> Manage grades
                                        </h6>
                                    </a>
                                </li>
                            <?php endif; ?>


                            <?php if (!Helpers::is('non_student') ) : ?>
                                <li>
                                    <a href="<?= Url::to(['/gradingcore/grading/student-class-grades']); ?>" class="">
                                        <h6 class="level-4 rectangle ">
                                            <i class="fa fa-graduation-cap" aria-hidden="true"></i> Class grades
                                        </h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                        <?php endif; ?>

                        <!--            end of resutls-->

                    </ol>
                </li>
            </ol>
        </li>

        <?php if (Helpers::is('r_student')) : ?>

        <li>
            <ol class="level-3-wrapper">
                <!--                staff members -->
                <?php if (Helpers::is('r_staff')) : ?>
                    <li>
                        <h5 class="level-5 rectangle text-center text-light">Staff information</h5>
                        <ol class="level-4-wrapper text-light">
                            <?php if (Helpers::is('r_staff')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-staff/index']); ?>" class="">
                                        <h6 class="level-3  rectangle"><i class="fa fa-wrench" aria-hidden="true"></i> Manage staff</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (Helpers::is('rw_staff')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-staff/create']); ?>" class="">
                                        <h6 class="level-3 rectangle"><i class="fa fa-user-plus" aria-hidden="true"></i> Add staff</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-staff/upload-excel']); ?>" class="">
                                        <h6 class="level-3 rectangle"><i class="fa fa-cloud-download" aria-hidden="true"></i> Import staff</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-staff/uploaded-files']); ?>" class="">
                                        <h6 class="level-3 rectangle"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Uploaded files</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (Helpers::is('rw_staff0')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-staff/email']); ?>" class="">
                                        <h6 class="level-4 rectangle"><i class="fa fa-envelope-o" aria-hidden="true"></i> Write email</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ol>
                    </li>
                <?php endif; ?>

                <!--                students center completed-->
                <?php if (Helpers::is('r_student')) : ?>
                    <li>
                        <h5 class="level-5 rectangle text-center text-light">Student information</h5>
                        <ol class="level-4-wrapper ">
                            <li>
                                <a href="<?= Url::to(['/schoolcore/core-student/index']); ?>" class="">
                                    <h6 class="level-4 rectangle"><i class="fa fa-wrench" aria-hidden="true"></i> Manage students</h6>
                                </a>
                            </li>
                            <?php if (Helpers::is('rw_student')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-student/create']); ?>" class="">
                                        <h6 class="level-4 rectangle"><i class="fa fa-user-plus" aria-hidden="true"></i> Add student</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/student-upload/upload-excel']); ?>" class="">
                                        <h6 class="level-4 rectangle"><i class="fa fa-cloud-download" aria-hidden="true"></i> Import students</h6>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/student-upload/uploaded-files']); ?>" class="">
                                        <h6 class="level-4 rectangle"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Uploaded files</h6>
                                    </a>
                                </li>
                            <?php endif; ?>


                            <li>
                                <a href="<?= Url::to(['/schoolcore/core-student/archived']); ?>" class="">
                                    <h6 class="level-4 rectangle"><i class="fa fa-archive" aria-hidden="true"></i> Archived students</h6>
                                </a>
                            </li>
                            <?php if (\app\components\ToWords::isSchoolUser()) :?>
                                <a href="<?= Url::to(['/schoolcore/core-student/std-list-wt-passwords']); ?>" class="">
                                    <li>
                                        <h6 class="level-4 rectangle"><i class="fa fa-key" aria-hidden="true"></i>Generate Student Passwords</h6>
                                    </li>
                                </a>
                            <?php endif ?>

                            <?php if (Helpers::is('rw_class')) :?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-school-class/promotion-studio']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-line-chart" aria-hidden="true"></i> Promotion studio</h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if (Helpers::is('schoolsuite_admin') || Helpers::is('rw_student')) : ?>
                                <li>
                                    <a href="<?= Url::to(['/schoolcore/core-student-group/']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-line-chart" aria-hidden="true"></i>Groupings</h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if (Helpers::is('schoolsuite_admin') || (\app\components\ToWords::isSchoolUser()) && Helpers::is('rw_terms_reports') ) : ?>
                                <li>
                                    <a href="<?= Url::to(['/studentreport/student-report/']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-file" aria-hidden="true"></i> Student reports</h6>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <?php if (\app\components\ToWords::isSchoolUser() && Helpers::is('is_teacher') ) : ?>
                                <li>
                                    <a href="<?= Url::to(['/studentreport/student-report/semester-reports']); ?>" class="">
                                        <h6 class="level-4 rectangle "><i class="fa fa-file" aria-hidden="true"></i> Semester reports</h6>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ol>
                    </li>
                <?php endif; ?>
            </ol>
        </li>
        <?php endif; ?>
    </ol>
</div>


<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolCampuses */

$this->title = 'Update : ' . ' ' . $model->campus_name;
$this->params['breadcrumbs'][] = ['label' => 'School Campuses', 'url' => ['campuses']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="school-campus-create hd-title" data-title="Add Campus">
<div class="letter">
  <div class="col-xs-12 no-padding">
    <h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3>
  </div>

    <?= $this->render('_campus_form', [
        'model' => $model, 'model_error'=>$model_error
    ]) ?>

</div>
</div>

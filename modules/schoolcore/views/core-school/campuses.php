<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SchoolCampusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Campuses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id='title' style='display:none'><?= $this->title; ?></div>
<div class="row">
    <div class="col-md-12">

        <div class="col-sm-4 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-sm-5 col-xs-12 no-padding"><?php echo $this->render('_campus_search', ['model' => $searchModel]); ?></div>
        <div class="col-sm-3 col-xs-12 no-padding">
            <ul class="menu-list pull-right">


                <li>
                    <?= Html::a('<i class="fa fa-plus-circle"></i>&nbsp;&nbsp; Add campus', ['/schoolcore/core-school/create-campus'], ['class' => 'btn btn-success btn-sm']) ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><?= $sort->link('campus_name') ?></th>
                    <th><?= $sort->link('school_code') ?></th>
                    <th><?= $sort->link('school_name') ?></th>
                    <th>Address</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">

                        <td class="clink"><?= ($v['campus_name']) ? '<a href="' . Url::to(['/schoolcore/core-school/update-campus', 'id' => $v['id']]) . '">' . $v['campus_name'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['external_school_code']) ? $v['external_school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td class="clink"><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['physical_address']) ? $v['physical_address'] : '<span class="not-set">(not set) </span>' ?></td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]); ?>

    </div>
</div>

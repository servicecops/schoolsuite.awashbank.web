<?php

use app\models\SchoolInformation;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use app\models\SchoolTypes;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */
/* @var $form yii\widgets\ActiveForm */

$action = $model->isNewRecord ? 'create-campus' : ['update-campus', 'id'=>$model->id];
?>



    <?php $form = ActiveForm::begin(['action'=>$action, 'options'=>['class'=>'formprocess']]); ?>
    <div class="row">
        <?php if(isset($model_error) && $model_error) : ?>
        <div class="col-xs-12 col-lg-12 no-padding">
            <div class="alert alert-danger">
                <?= $model_error ?>
            </div>
        </div>
        <?php endif; ?>
    <div class="col-xs-12 col-lg-12 no-padding">

        <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>
        <div class="col-sm-4">
                    <?php
                    $url = Url::to(['/schoolcore/core-school/schoollist']);
                    $selectedSchool = empty($model->school_id) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->school_id)->school_name;
                    $selectedSchoolValue = empty($model->school_id) ? '' : $model->school_id;
                    echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => 'Filter School',
                            'id' => 'school_search',
                            'disabled'=>!$model->isNewRecord,
                            'value'=>$selectedSchoolValue,
                            'onchange' => '$.post( "' . Url::to(['/classes/lists']) . '/"+$(this).val(), function( data ) {
                            $( "select#student_class_drop" ).html(data);
                        });',
                            'class' => 'form-control',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                        ],
                    ])->label('School Name *'); ?>
            </div>
        <?php endif; ?>
        <div class="col-sm-4">
        <?= $form->field($model, 'campus_name', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Campus Name'] ])->textInput()->label('Campus Name *') ?>
        </div>
        <div class="col-sm-4">
        <?= $form->field($model, 'physical_address', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Campus Name'] ])->textInput()->label('Physical Address *') ?>
        </div>

   </div>


   <div class="form-group col-xs-6 no-padding">
    <div class="col-xs-3">
        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
    </div>
    <div class="col-xs-3">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
   </div>

</div>
    <?php ActiveForm::end(); ?>

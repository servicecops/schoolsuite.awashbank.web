<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Information';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letters">


    <?php


    $columns = [

        'external_school_code',
        [
            'label' => 'school name',
            'value' => function ($model) {
                return Html::a($model['school_name'], ['core-school/view', 'id' => $model['id']], ['class' => 'aclink']);
            },
            'format' => 'html',
        ],
        'district_name',

        'phone_contact_1',
        'school_type',
        [
            'label' => 'Assign channel',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-exchange"></i>', ['channels', 'id' => $model['id']], ['class' => 'aclink']);
            },
            'format' => 'html',
        ],


        [
            'label' => '',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-eye"></i>', ['core-school/view', 'id' => $model['id']], ['class' => 'aclink']);
            },
            'format' => 'html',
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa fa-edit"></i>', ['core-school/update', 'id' => $model['id']]);
            },
        ],


    ];
    ?>

    <div class="row">
        <div class="col-md-2" style="font-size:20px;color:#2c3844">&nbsp;<i
                    class="fa fa-th-list"></i> <?php echo $this->title ?></div>
        <div class="col-md-6">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <div class="col-md-4 pull-right">
            <?php if (Yii::$app->user->can('super_admin')) : ?>
                <?= Html::a('Add new school', ['create'], ['class' => 'btn  btn-primary btn-bg  text-white float-right fa fa-plus']) ?>

            <?php endif; ?>
            <?php
            //        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-data/export-to-pdf', 'model' => get_class($searchModel)], [
            //            'class'=>'btn btn-md btn-danger',
            //            'target'=>'_blank',
            //            'data-toggle'=>'tooltip',
            //            'title'=>'Will open the generated PDF file in a new window'
            //        ]);
            echo Html::a('<i class="fa fa-file-excel-o"></i> Download PDF', ['core-school/export-to-pdf', 'model' => get_class($searchModel)], ['class' => 'btn  btn-danger btn-md', 'target' => '_blank']);
            echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-md btn-success',
                'target' => '_blank'
            ]);
            ?>
        </div>
    </div>
        <div class="mt-3">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
                'columns' => $columns,
                'resizableColumns' => true,
//    'floatHeader'=>true,
                'responsive' => true,
                'responsiveWrap' => false,
                'bordered' => false,
                'striped' => true,
            ]); ?>

        </div>
    </div>

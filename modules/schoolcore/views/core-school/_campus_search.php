<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['campuses'],
    'method' => 'get',
    'options'=>['class'=>'formprocess'],
]); ?>
<div class="col-xs-12 no-padding">
<ul class=" menu-list no-padding">
<li class="col-xs-3 no-padding">
    <?= $form->field($model, 'modelSearch',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Code'])->label(false) ?>
</li>
    <li class="col-xs-4 no-padding">
    <?= $form->field($model, 'campus_name',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Campus Name'])->label(false) ?>
 </li>


 <li class="col-xs-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></li>
</ul>
</div>
<?php ActiveForm::end(); ?>

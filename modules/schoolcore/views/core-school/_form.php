<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use app\modules\schoolcore\models\County;
use app\modules\schoolcore\models\District;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">

    <p>All Mandatory fields are marked with *</p>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div class="row">

        <div class="col-sm-4">
            <?= $form->field($model, 'school_name', ['inputOptions' => ['class' => 'form-control star', 'placeholder' => 'School Name']])->textInput()->label('School Name *') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'external_school_code', ['inputOptions' => ['class' => 'form-control star', 'placeholder' => 'Awash School Code', 'disabled'=>!$model->isNewRecord]])->textInput()->label('School Code *') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'contact_person', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Person']])->textInput()->label('Contact Person *') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'phone_contact_1', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Phone']])->textInput()->label('Contact Phone *') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'phone_contact_2', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Another Contact Phone']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'contact_email_1', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput()->label('Contact Email *') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'school_type')->dropDownList(
                ArrayHelper::map(CoreControlSchoolTypes::find()->orderBy('description')->all(), 'id', 'description'), ['prompt' => 'Select Type of School ..']
            ) ->label('School Type *')?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'school_registration_number_format', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Reg No. Format']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'sample_school_registration_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Sample Reg No.']])->textInput() ?>
        </div>


        <div class="col-sm-4">


            <?= $form->field($model, 'bank_name')->dropDownList(
                ArrayHelper::map(CoreBankDetails::find()->all(), 'id', 'bank_name'), ['prompt' => 'Select Primary Bank']
            )->label('Select Primary Bank') ->label('Primary Bank *')?>


        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'transaction_api_password', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Transaction Api Password'] ])->textInput()->label('Transaction API Password') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'sms_sender_id', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'SMS Sender ID'] ])->textInput()->label('SMS Sender ID') ?>
        </div>

        
        <div class="col-sm-4">

            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
            echo $form->field($model, 'branch_region')
                ->dropDownList(
                    $items,           // Flat array ('id'=>'label')
                    ['prompt' => 'Select Branch Region',
                        'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/branches']) . '?id="+$(this).val(), function( data ) {
                            $( "select#branches" ).html(data);
                        });']    // options
                );
            ?>
        </div>
        <div class="col-sm-4">
            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\AwashBranches::find()->where(['branch_region'=>$model->branch_region])->all(), 'id', 'branch_name');
            echo $form->field($model, 'branch_id')->dropDownList($items
                , ['id' => 'branches',
                    ])->label('Branches*')
            ?>
        </div>
    </div>


    <div class="row">
        <br>
        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[active]" value="0">
                <input type="checkbox" id="CoreSchool-active" name="CoreSchool[active]"
                       value="1" <?= ($model->active) ? 'checked' : '' ?> >
                <label for="CoreSchool-active">Active</label>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[default_part_payment_behaviour]" value="0"><input
                        type="checkbox" id="CoreSchool-default_part_payment_behaviour"
                        name="CoreSchool[default_part_payment_behaviour]"
                        value="1" <?= ($model->default_part_payment_behaviour) ? 'checked' : '' ?> >
                <label for="CoreSchool-default_part_payment_behaviour">Allow Part Payments</label>
            </div>
        </div>

        <div class="col-sm-2">

            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[enable_school_sections_logic]" value="0">
                <input type="checkbox" id="CoreSchool-enable_school_sections_logic"
                       name="CoreSchool[enable_school_sections_logic]" value="1"
                       data-checked="negative" <?= ($model->enable_school_sections_logic) ? 'checked' : '' ?>>
                <label for="CoreSchool-enable_school_sections_logic">Enable Sections Logic</label>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[enable_transaction_api]" value="0">
                <input type="checkbox" id="CoreSchool-enable_transaction_api" name="CoreSchool[enable_transaction_api]"
                       value="1" data-checked="negative" <?= ($model->enable_transaction_api) ? 'checked' : '' ?>>
                <label for="CoreSchool-enable_transaction_api">Enable Transaction Api</label>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[enable_bank_statement]" value="0">
                <input type="checkbox" id="CoreSchool-enable_bank_statement" name="CoreSchool[enable_bank_statement]"
                       value="1" data-checked="negative" <?= ($model->enable_bank_statement) ? 'checked' : '' ?>>
                <label for="CoreSchool-enable_bank_statement">Enable Bank Statement</label>
            </div>
        </div>
        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[allow_payments_on_zero_balance]" value="0">
                <input type="checkbox" id="CoreSchool-allow_payments_on_zero_balance" name="CoreSchool[allow_payments_on_zero_balance]"
                       value="1" data-checked="positive" <?= ($model->allow_payments_on_zero_balance) ? 'checked' : '' ?>>
                <label for="CoreSchool-allow_payments_on_zero_balance">Allow Payments on Zero Balance</label>

            </div>
        </div>
        <?php if(Yii::$app->user->can('can_allow_payment_reversal')):?>
        <div class="col-sm-2">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreSchool[allow_payment_reversal]" value="0">
                <input type="checkbox" id="CoreSchool-allow_payment_reversal" name="CoreSchool[allow_payment_reversal]"
                       value="1" data-checked="positive" <?= ($model->allow_payment_reversal) ? 'checked' : '' ?>>
                <label for="CoreSchool-allow_payment_reversal">Allow Payments Reversals</label>

            </div>
        </div>
        <?php endif; ?>


    </div>
    <hr class="l_header mt-3">
    <div class="row">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Address Information</h3></div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\RefRegion::find()->all(), 'id', 'description');
            echo $form->field($model, 'region')
                ->dropDownList(
                    $items,           // Flat array ('id'=>'label')
                    ['prompt' => 'Select region',
                        'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });']    // options
                ); ?>
        </div>
        <div class="col-sm-4">
            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\District::find()->where(['region_id'=>$model->region])->all(), 'id', 'district_name');
            echo $form->field($model, 'district')->dropDownList($items
                , ['id' => 'district',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])->label('District*')
            ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Physical Address']])->textInput() ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'box_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'School box number']])->textInput([
                'type' => 'text'
            ]) ?>
        </div>

        <div class="col-sm-4 " >
            <?= $form->field($model, 'day_boarding_structure')->dropDownList([
                'Day Only' => 'Day Only',
                'Boarding Only' => 'Boarding Only',
                'Day and Boarding' => 'Day and Boarding',


            ],
                ['id'=>'day_boarding_structure', 'prompt' => 'Day Boarding Structure']) ?>
        </div>
    </div>




    <div class="row">


        <div class="col-sm-12 ">
            <div class="checkbox">
                <?php
                foreach ($sp_modules as $k => $v) :
                    $checked = in_array($v['id'], $sch_modules) || ($v['module_code'] == 'SCHOOLPAY_CODE') ? true : false;
                    ?>
                    <div class="checkbox checkbox-inline checkbox-info">
                        <input type="checkbox" class="checkbox" name="sp_modules[]" value="<?= $v['id'] ?>"
                               id="<?= $v['module_code'] ?>" <?= $checked ? 'checked' : ''; ?> >
                        <label for="<?= $v['module_code'] ?>">&nbsp;<?= $v['module_name'] ?></label>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>


        <?php if ($model->enable_school_sections_logic) : ?>
            <div class="col-sm-12  no-padding">
                <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;School Sections</h3></div>
            </div>
            <div class="col-sm-12 ">
                <?= $this->render('_school_sections', ['form' => $form, 'model' => $model,
                    'sections' => $sections, 'section_info_error' => $section_info_error, 'banks' => $banks, 'accounts' => $accounts,
                    'school_banks' => $school_banks]); ?>
            </div>
        <?php endif; ?>

    </div>
    <hr>



    <div class="card-footer">
        <div class="row">
            <div class="col-xs-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update <i class="fa fa-save"></i>', ['class' => 'btn btn-lg btn-block btn-primary']) ?>
            </div>
            <div class="col-xs-6">
                <?php if ($model->isNewRecord): ?>
                    <?= Html::resetButton('Reset <i class="fa fa-undo"></i>', ['class' => 'btn btn-lg  btn-default btn-block']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS
    $('document').ready(function () {
        if (!$("input#CoreSchool-enable_daily_stats").attr('checked')) {
            $('input#CoreSchool-enable_daily_stats').attr('data-checked', 'negative');
            $('div#daily_stats_recipients_div').css('display', 'none');
        } else {
            $('input#CoreSchool-enable_daily_stats').attr('data-checked', 'positive');
            $('div#daily_stats_recipients_div').css('display', 'block');
        }

        $('body').on('change', 'input#CoreSchool-enable_daily_stats', function () {
            if ($(this).attr('data-checked') == 'negative') {
                $('div#daily_stats_recipients_div').css('display', 'block');
                $(this).attr('data-checked', 'postive');
            } else {
                $('div#daily_stats_recipients_div').css('display', 'none');
                $(this).attr('data-checked', 'negative');
            }
        });
        
        
         $('#tertiary').hide();
    $('#non_tertiary').hide();
    $('#other').hide();
    $('#other_legal').hide();
    $('#other_source_of_funding').hide();
    
     let thisSourceSelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

        if (selector.val() === 'Tertiary') {

            $('#tertiary').show();
        } else {
             $('#tertiary').hide();
        }

        if (selector.val() === 'Other') {

             $('#other').show();
        } else {
            $('#other').hide();
        }
        if (selector.val() === 'Post-Primary/Non tertiary') {

            $('#non_tertiary').show();
        } else {
            $('#non_tertiary').hide();
        }


    }
    
        let thisTertiarySelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

   
        if (selector.val() === 'Other') {

             $('#other').show();
        } else {
            $('#other').hide();
        }
     
        
    }
      let thisNonTertiarySelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

   
        if (selector.val() === 'Other') {

             $('#other').show();
        } else {
            $('#other').hide();
        }
     

    }
    
    
        let thisLegalSelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

   
        if (selector.val() === 'Other') {

             $('#other_legal').show();
        } else {
            $('#other_legal').hide();
        }
     

    }
    
    
        let thisSourceOfFunding = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

   
        if (selector.val() === 'Others') {

             $('#other_source_of_funding').show();
        } else {
            $('#other_source_of_funding').hide();
        }
     

    }
    
    
     $("#school_level_id").on("change", function () {
       // alert('inside');
        thisSourceSelected($(this));
        let selector = $(this);
     
    });
     $("#tertiary_level_id").on("change", function () {
       // alert('inside');
        thisTertiarySelected($(this));
        let selector = $(this);
     
    });  
     
     $("#non_tertiary_level_id").on("change", function () {
       // alert('inside');
        thisNonTertiarySelected($(this));
        let selector = $(this);
     
    });
     
     $("#legal_ownership").on("change", function () {
       // alert('inside');
        thisLegalSelected($(this));
        let selector = $(this);
     
    }); 
     $("#source_of_funding").on("change", function () {
       // alert('inside');
        thisSourceOfFunding($(this));
        let selector = $(this);
     
    });

    
    
    
    
    
    
    });
JS;
$this->registerJs($script);
?>

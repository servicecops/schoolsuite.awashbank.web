<?php

use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Generator QR';
/* @var $this yii\web\View */
/* @var $model app\modules\studentreport\models\CoreSchoolCircular */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="formz">

    <div class=" col-xs-12 no-padding">
        <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
    </div>

    <?php /** @var TYPE_NAME $school_id */
    $form = ActiveForm::begin([
        'action' => ['circular-generator', 'school_id' => $school_id],
        'method' => 'post',
    ]); ?>

    <div style="padding: 10px;width:100%"></div>

    <div class="row">
        <?php
        if (isset($classes)) : ?>
            <div class="col-md-12 col-sm-6 no-padding-right">
                <?php
                $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                echo $form->field($model, 'classes')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'class_code'),
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Find Class',
                        'id' => 'group_class_select',
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Choose Classes'); ?>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="label-default">You could exclude some students(Paste payment codes (Separate with
                        spaces or commas)) </label>
                    <textarea class="form-control" id="exclude_student_codes" name="exclude_student_codes" rows=""
                              placeholder="Paste student codes"></textarea>

                </div>
            </div>
        <?php endif; ?>
        <?php if (isset($student_codes)) : ?>
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="label-default">Paste payment codes (Separate with spaces or commas)</label>
                    <textarea class="form-control" id="student_codes" name="student_codes" rows=""
                              placeholder="Paste student codes"></textarea>

                </div>
            </div>
        <?php endif; ?>

    </div>


    <hr/>


</div>
<hr/>

<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton('Generate QR codes', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>

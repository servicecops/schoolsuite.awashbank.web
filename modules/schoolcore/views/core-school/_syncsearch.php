<?php

use app\modules\schoolcore\models\CoreSchoolSearch;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\widgets\ActiveForm */
?>



<?php $form = ActiveForm::begin([
    'action' => ['sync-report'],
    'method' => 'get',
    'options'=>['class'=>'formprocess'],
]); ?>
<div class="row">
    <ul class=" menu-list no-padding" style="list-style: none;">
        <li class="col-md-1 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_from',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                'clientOptions' => [
                    'class' => 'form-control',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1900:' . (date('Y') + 1),
                    'autoSize' => true,
                ],
            ]); ?></li>
        <li class="col-md-1 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                'model' => $model,
                'attribute' => 'date_to',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                'clientOptions' => [
                    'class' => 'form-control',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1900:' . (date('Y') + 1),
                    'autoSize' => true,
                ],
            ]); ?></li>
        <li class="col-xs-2 no-padding">
            <?= $form->field($model, 'school_code',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Suite Code'])->label(false) ?>
        </li>
        <li class="col-xs-2 no-padding">
            <?= $form->field($model, 'schoolpay_schoolcode',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'SchoolPay Code'])->label(false) ?>
        </li>
        <li class="col-xs-2 no-padding">
            <?= $form->field($model, 'school_name',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Name'])->label(false) ?>
        </li>

        <li class="col-xs-2 no-padding"><?= $form->field($model, 'bank_name', ['inputOptions'=>[ 'class'=>'form-control input-sm']] )->dropDownList(ArrayHelper::map(BankDetails::findAllNominatedBanks(), 'id', 'bank_name'),
                ['prompt'=>'Primary Bank'])->label(false) ?></li>

        <li class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></li>
    </ul>
</div>
<?php ActiveForm::end(); ?>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Update School: ' . $model->school_name;

?>
<div class="core-school-update">

    <div class="letters">
        <div class="row">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="row">
            <?= $this->render('_form', ['sch_modules' => $sch_modules,
                'model' => $model, 'banks' => $banks, 'accounts' => $accounts,
                'bank_info_error' => $bank_info_error, 'sections' => $sections,
                'section_info_error' => $section_info_error, 'school_banks' => $school_banks, 'model_error' => $model_error, 'sp_modules' => $sp_modules
            ]) ?>
        </div>
    </div>

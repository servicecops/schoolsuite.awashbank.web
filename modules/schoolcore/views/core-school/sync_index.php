<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sync Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">



    <?php


    $columns = [
        [
            'label' => 'Sync Date',
            'value'=>function($model){
                return Html::a($model['date_created']);
            },
            'format'=>'html',
        ],
        'schoolpay_schoolcode',
        [
            'label' => 'Suite School Code',
            'value'=>function($model){
                return Html::a($model['school_code']);
            },
            'format'=>'html',
        ],
        [
            'label' => 'school name',
            'value'=>function($model){
                return Html::a($model['school_name'], ['core-school/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'district_name',

        'phone_contact_1',
        'school_type',



    ];
    ?>
    <div class="letters">


            <div class="col-md-2 col-xs-12 no-padding"><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            <div class="col-md-8 col-xs-12 no-padding"><?php echo $this->render('_syncsearch', ['model' => $searchModel]); ?></div>
            <div class="col-md-2 col-xs-12 no-padding">
                <div class="float-right">

                    <?php


                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'target' => '_blank',
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            ExportMenu::FORMAT_EXCEL => false,

                        ],
                        'dropdownOptions' => [
                            'label' => 'Export schools',
                            'class' => 'btn btn-outline-secondary'
                        ]
                    ])
                    ?>
                </div>

            </div>

    </div>

<div class="mt-3">
    <div class="float-right">

    </div>

    <?= GridView::widget([
        'dataProvider'=> $dataProvider,
//    'filterModel' => $searchModel,
        'columns' => $columns,
        'resizableColumns'=>true,
//    'floatHeader'=>true,
        'responsive'=>true,
        'responsiveWrap' => false,
        'bordered' => false,
        'striped' => true,
    ]); ?>

</div>
</div>

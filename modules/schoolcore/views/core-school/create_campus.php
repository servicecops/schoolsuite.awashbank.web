<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = 'Add Campus';
$this->params['breadcrumbs'][] = ['label' => 'Campuses', 'url' => ['campuses']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-campus-create hd-title" data-title="Add Campus">
  <div class="letter">
  <div class=" col-xs-12 no-padding">
    <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
  </div>
    <?= $this->render('_campus_form', [
        'model' => $model,
        'model_error' => $model_error
    ]) ?>

</div>
</div>

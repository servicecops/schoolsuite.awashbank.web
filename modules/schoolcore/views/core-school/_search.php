<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchoolSearch;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\widgets\ActiveForm */
?>



<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options'=>['class'=>'formprocess'],
]); ?>
<div class="row">
    <ul class=" menu-list no-padding" style="list-style: none;">
        <li class="col-md-3 ">
            <?= $form->field($model, 'school_code',  ['inputOptions'=>[ 'class'=>'form-control input-md']])->textInput(['placeHolder'=>'School Code'])->label(false) ?>
        </li>
        <li class="col-md-4 ">
            <?= $form->field($model, 'school_name',  ['inputOptions'=>[ 'class'=>'form-control input-md']])->textInput(['placeHolder'=>'School Name'])->label(false) ?>
        </li>
        <li class="col-md-3"><?= $form->field($model, 'bank_name', ['inputOptions'=>[ 'class'=>'form-control input-md']] )->dropDownList(ArrayHelper::map(CoreBankDetails::findAllNominatedBanks(), 'id', 'bank_name'),

                ['prompt'=>'Primary Bank'])->label(false) ?></li>

        <li class="col-md-2 ">
            <?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-md']) ?>
        </li>
    </ul>
</div>
<?php ActiveForm::end(); ?>

<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Information';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letters">


    <?php


    $columns = [

        'branch_name',

        'description',

        'schools',



    ];
    ?>

    <div class="row">
        <div class="col-md-12" style="font-size:20px;color:#2c3844">&nbsp;<i
                    class="fa fa-th-list"></i> <?php echo $this->title ?></div>

        <div class="col-md-12 pull-right">

            <?php
            //        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-data/export-to-pdf', 'model' => get_class($searchModel)], [
            //            'class'=>'btn btn-md btn-danger',
            //            'target'=>'_blank',
            //            'data-toggle'=>'tooltip',
            //            'title'=>'Will open the generated PDF file in a new window'
            //        ]);
            echo Html::a('<i class="fa fa-file-excel-o"></i> Download PDF', ['core-school/export-to-pdf', 'model' => get_class($searchModel)], ['class' => 'btn  btn-danger btn-md', 'target' => '_blank']);
            echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-md btn-success',
                'target' => '_blank'
            ]);
            ?>
        </div>
    </div>
        <div class="mt-3">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
                'columns' => $columns,
                'resizableColumns' => true,
//    'floatHeader'=>true,
                'responsive' => true,
                'responsiveWrap' => false,
                'bordered' => false,
                'striped' => true,
            ]); ?>

        </div>
    </div>

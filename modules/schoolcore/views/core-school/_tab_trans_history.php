<?php
use yii\helpers\Html;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
?>

<table class="table table-striped no-margin" id="tb_accountHistories">
    <thead>
    <tr><th>Date</th><th> Description </th><th> Channel code </th> <th> Channel memo </th> <th> Payment Type</th> <th> Trans Type</th><th> Amount</th></tr>
    </thead>
    <tbody>
    <?php
    $data = $model->schoolAccount->accountHistories;
    foreach($data['query'] as $trans): ?>
        <tr>
            <td><?= $trans->date_created ?></td>
            <td><?= $trans->description ?></td>
            <td><?= ($trans->payment_id) ? $trans->payment->paymentChannel->channel_code : "--";  ?></td>
            <td><?= ($trans->payment_id) ? $trans->payment->channel_memo : "--" ?></td>
            <td><?= ($trans->payment_id) ? $trans->payment->channel_payment_type : "--" ?></td>
            <td><?= $trans->trans_type ?></td>
            <td <?= ($trans->amount < 0) ? "style='color:red'" : " "?> ><?=  number_format($trans->amount, 2) ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php
echo LinkPager::widget([
    'pagination' => $data['pages'],
    'options'=>['class'=>'pagination',
        'data-for'=>'tb_accountHistories',
        'id'=>'pg_tb_accountHistories']
]);
?>




<?php


?>


<table class="table table-striped no-margin" id="sch_payments_recvd">
    <thead>
    <tr><th>Campus Name</th><th> Location</th> </tr>
    </thead>
    <tbody>
    <?php
    foreach($campuses as $k=>$v): ?>
        <tr>
            <td><?= (isset($v->campus_name)) ? $v->campus_name : " --" ?></td>
            <td><?= (isset($v->physical_address)) ? $v->physical_address : " --" ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
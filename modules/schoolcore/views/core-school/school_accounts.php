<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\SchoolTypes;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */
/* @var $form yii\widgets\ActiveForm */
$accounts = (!$model->isNewRecord) ? $accounts : '';
?>



<?php $form = ActiveForm::begin([
        'action' => ['add-account',
    'id' => $model->id],
    'id'=>'school_form',
    'options'=>['class'=>'formprocess']]); ?>
<div class="row">
    <?php if(isset($model_error) && $model_error) : ?>
        <div class="col-md-12 col-lg-12 no-padding">
            <div class="alert alert-danger">
                <?= $model_error ?>
            </div>
        </div>
    <?php endif; ?>


    <?php if (Yii::$app->user->can('add_bank_account')) :?>
        <div class="col-md-12  no-padding">
            <div class="col-sm-12"> <h3><i class="fa fa-check-square-o"></i>&nbsp;Provide Bank Information</h3> </div>
        </div>
        <div class="col-md-12 no-padding">
            <?= $this->render('_bank_info', ['form'=>$form, 'model'=>$model, 'banks'=>$banks, 'accounts'=>$accounts, 'bank_info_error'=>$bank_info_error]); ?>
        </div>
    <?php  endif;?>


    <?php if($model->enable_school_sections_logic) : ?>
        <div class="col-md-12  no-padding">
            <div class="col-sm-12"> <h3><i class="fa fa-check-square-o"></i>&nbsp;School Sections</h3> </div>
        </div>
        <div class="col-md-12 no-padding">
            <?= $this->render('_school_sections', ['form'=>$form, 'model'=>$model,
                'sections'=>$sections, 'section_info_error'=>$section_info_error, 'banks'=>$banks, 'accounts'=>$accounts,
                'school_banks'=>$school_banks]); ?>
        </div>
    <?php endif; ?>



    <div class="form-group col-md-12 col-sm-12 col-lg-12">
        <br>
        <hr class="l_header" style="margin-top:15px;">

    </div>
    <div class="form-group col-md-6 no-padding">
        <div class="col-md-6">

            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>
<?php
$script = <<< JS
 $('document').ready(function(){
     
        let schoolForm = $('#school_form');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.accounts_control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val()) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });
    })
JS;
$this->registerJs($script);
?>


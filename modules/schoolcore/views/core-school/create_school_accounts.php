<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = 'Update : ' . ' ' . $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

?>
<p>ADD ACCOUNTS</p>

<div class="letter" data-title="Add School">


    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12 no-padding">
            <h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="col-md-12 no-padding">

            <?= $this->render('school_accounts', [
                'model' => $model, 'sp_modules' => $sp_modules, 'sch_modules' => $sch_modules, 'banks' => $banks, 'accounts' => $accounts,
                'bank_info_error' => $bank_info_error, 'sections' => $sections,
                'section_info_error' => $section_info_error, 'school_banks' => $school_banks, 'model_error' => $model_error
            ]) ?>
        </div>
        <div style="clear: both"></div>

    </div>
</div>

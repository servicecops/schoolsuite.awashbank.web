<?php

use yii\helpers\Html;

?>

<div class="row">
    <div class=" bg-transparent border-0">
        <h3 class="mb-0"><i class="fa fa-clone pr-1"></i>General Information</h3>
    </div>

        <table class="table table-striped">
            <tr>
                <th width="40%">Contact person</th>
                <td width="2%">:</td>
                <td><?= ($model->contact_person) ? $model->contact_person : " --" ?></td>
            </tr>
            <tr>
                <th width="40%">Bank Name	</th>
                <td width="2%">:</td>
                <td><?= ($model->bank_name) ? $model->bankName->bank_name : " --" ?></td>
            </tr>
            <tr>
                <th width="40%">Other contact</th>
                <td width="2%">:</td>
                <td><?= ($model->phone_contact_2) ? $model->phone_contact_2 : "--" ?></td>
            </tr>
            <tr>
                <th width="40%">Date created</th>
                <td width="2%">:</td>
                <td><?= $model->date_created ?></td>
            </tr>
            <tr>
                <th width="40%"><?= $model->getAttributeLabel('school_registration_number_format') ?></th>
                <td width="2%">:</td>
                <td>  <?= ($model->school_registration_number_format) ? $model->school_registration_number_format : " --" ?></td>
            </tr>

            <tr>
                <th width="40%"><?= $model->getAttributeLabel('sample_school_registration_number') ?></th>
                <td width="2%">:</td>
                <td>  <?= ($model->sample_school_registration_number) ? $model->sample_school_registration_number : " --" ?></td>
            </tr>
            <tr>
                <th width="40%">Active</th>
                <td width="2%">:</td>
                <td> <?= ($model->active) ? "Yes" : "No" ?></td>
            </tr>
            <tr>
                <th width="40%"><?= $model->getAttributeLabel('default_part_payment_behaviour') ?></th>
                <td width="2%">:</td>
                <td> <?= ($model->default_part_payment_behaviour) ? "Yes" : "No" ?></td>
            </tr>

            <tr>
                <th width="40%"><?= $model->getAttributeLabel('transaction_api_password') ?></th>
                <td width="2%">:</td>
                <td>  <?= ($model->transaction_api_password) ? $model->transaction_api_password : "--" ?></td>
            </tr>

            <tr>
                <th width="40%">Enable transaction Api</th>
                <td width="2%">:</td>
                <td>  <?= ($model->enable_transaction_api) ? "Yes" : "No" ?></td>
            </tr>
            <tr>
                <th width="40%">Enable Bank statement</th>
                <td width="2%">:</td>
                <td>   <?= ($model->enable_bank_statement) ? "Yes" : "No" ?></td>
            </tr>
            <tr>
                <th width="40%">Enable Sections logic</th>
                <td width="2%">:</td>
                <td>   <?= ($model->enable_school_sections_logic) ? "Yes" : "No" ?></td>
            </tr>
 <tr>
                <th width="40%">Enable Allow Payment on Zero Balance</th>
                <td width="2%">:</td>
                <td>   <?= ($model->allow_payments_on_zero_balance) ? "Yes" : "No" ?></td>
            </tr>
 <tr>
                <th width="40%">Enable Reversal</th>
                <td width="2%">:</td>
                <td>   <?= ($model->allow_payment_reversal) ? "Yes" : "No" ?></td>
            </tr>



        </table>

</div>



<div class="row">
    <table class="table table-striped" style="">
        <thead>
        <tr>
            <th colspan="4" style="text-align:center"><h5>Bank Information</h5></th>
        </tr>
        <tr>
            <th> Bank</th>
            <th> Account Title</th>
            <th> Account Type</th>
            <th> Account Number</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($accounts) :
            foreach ($accounts as $k => $v): ?>
                <tr>
                    <td><?= $v['bank_name'] ?></td>
                    <td><?= $v['account_title'] ?></td>
                    <td><?= $v['account_type'] ?></td>
                    <td><?= $v['account_number'] ?></td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr>
                <td colspan="4">No account provided for this school</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<?php if ($model->enable_school_sections_logic) : ?>
    <div class="col-md-12">
        <table class="table" style="">
            <thead>
            <tr>
                <th colspan="4" style="text-align:center"><h3>School Sections</h3></th>
            </tr>
            <tr>
                <th> Section Code</th>
                <th> Section Name</th>
                <th> Section Pri. Bank</th>
                <th> Section Account</th>

            </tr>
            </thead>
            <tbody>
            <?php
            if ($sections) :
                foreach ($sections as $k => $v): ?>
                    <tr>
                        <td><?= $v['section_code'] ?></td>
                        <td><?= $v['section_name'] ?></td>
                        <td><?= $v['bank_name'] ?></td>
                        <td><?= $v['section_bank_account'] ?></td>
                    </tr>
                <?php endforeach;
            else :
                ?>
                <tr>
                    <td colspan="2">No sections available for this school</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>



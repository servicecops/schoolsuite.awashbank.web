<?php

use app\components\Helpers;
use yii\helpers\Url; ?>

<div class="row card my-1 bg-light">
    <h3 class="card-header">School center</h3>
    <div class="row card-body justify-content-center">
        <?php if (Helpers::is('rw_sch')) : ?>
            <?php if (Helpers::is('rw_sch')) : ?>
                <a class="card col-md-4 " href="<?= Url::to(['/schoolcore/core-school/create']); ?>">
                    <div class="display-4 text-center"> <i class="fa fa-plus-circle"></i> <span>Add school</span> </div>
                </a>
            <?php endif; ?>
        <?php endif; ?>
        <?php if (Helpers::is('schoolsuite_admin')) :?>
            <a class="card col-md-4 " href="<?= Url::to(['/schoolcore/core-school/index']); ?>">
                <div class="display-4 text-center"><i class="fa fa-eye" aria-hidden="true"></i> <span>View schools</span>  </div>
            </a>
            <a class="card col-md-4 " href="<?= Url::to(['/schoolcore/core-school/sections']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-tripadvisor" aria-hidden="true"><span>School sections</span>  </i></div>
            </a>

        <?php endif; ?>
        <?php if (\app\components\ToWords::isSchoolUser()) :
            $schoolId = Yii::$app->user->identity->school_id;
            ?>
            <a class="card col-md-4 " href="<?= Url::to(['/studentreport/social-media/']); ?>">
                <div class="display-4 text-center"> <i class="fa fa-google-plus" aria-hidden="true"></i><span>Manage Social Medias </span> </div>
            </a>
            <a class="card col-md-4 " href="<?= Url::to(['/studentreport/signature-seal/']); ?>">
                <div class="display-4 text-center"><i class="fa fa-pencil-square-o" aria-hidden="true"> <span>Manage School Signatures </span> </i></div>
            </a>

        <?php endif; ?>
        <?php if (Helpers::is('own_sch' ) || !Helpers::is('non_student')) :
            $schoolId = Yii::$app->user->identity->school_id;
            ?>
            <a class="card col-md-4 " href="<?= Url::to(['/schoolcore/core-school/view', 'id'=>$schoolId]); ?>">
                <div class="display-4 text-center"><i class="fa fa-eye" aria-hidden="true"> <span>View school</span> </i></div>
            </a>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('can_approve_workflow')) : ?>
        <a class="card col-md-4 " href="<?= Url::to(['/workflow/workflow/index']); ?>">
            <div class="display-4 text-center"> <i class="fa fa-user-secret" aria-hidden="true"></i> <span>Workflow &
                                                Approvals</span></div>
        </a>
        <?php endif; ?>

        <?php if (Yii::$app->user->can('create_campus')) : ?>
            <a class="card col-md-4 " href="<?= Url::to(['/schoolcore/core-school/campuses']); ?>">
                <div class="display-4 text-center"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <span>School campuses</span> </div>
            </a>
        <?php endif ?>


    </div>

</div>

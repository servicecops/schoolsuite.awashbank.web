<?php

use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'School Information';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="letters">

    
    
    <div class="row">

        <?php if (Yii::$app->session->hasFlash('Reversal')) : ?>
            <div class="notify notify-success">
                <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
                <div class="notify-content">
                    <?= Yii::$app->session->getFlash('Reversal'); ?>
                </div>
            </div>
        <?php endif; ?>


        <?php if (Yii::$app->user->can('edit_school_logo')) : ?>

            <?= Html::a('<i class="fa fa-edit" ></i> <span >Edit Logo</span>', ['logo', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary ']) ?>

        <?php endif; ?>
        <?php if (Yii::$app->user->can('own_sch') && Yii::$app->user->can('can_allow_payment_reversal')) : ?>
            <?php

        if($model->allow_payment_reversal){?>
            <?= Html::a('<i class="fa fa-edit" ></i> <span >Disable Payments Reversals</span>', ['allow-reversal', 'id' => $model->id, 'allow'=>false], ['class' => 'btn btn-sm btn-primary ']) ?>

        <?php }else{?>
            <?= Html::a('<i class="fa fa-edit" ></i> <span >Allow Payments Reversals</span>', ['allow-reversal', 'id' => $model->id,'allow'=>true], ['class' => 'btn btn-sm btn-primary ']) ?>


        <?php }
        ?>


        <?php endif; ?>
        <?php if (Yii::$app->user->can('add_bank_account')): ?>
            <?= Html::a('<i class="fa fa-edit"></i> Add Account', ['add-account', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary aclink']) ?>
        <?php endif; ?>
        <?php if (\Yii::$app->user->can('rw_sch')) : ?>
            <?php if (Yii::$app->user->can('schoolsuite_admin') && Yii::$app->user->can('rw_sch')) : ?>
                <?= Html::a('<i class="fa fa-edit" ></i> <span >Edit Details</span>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary ']) ?>
            <?php endif; ?>


            <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                <?= Html::a('<i class="fa fa-trash" ></i><span > Delete School</span>', ['delete', 'id' => $model->school_code], ['class' => 'btn btn-sm btn-danger', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to delete this school"]) ?>
            <?php endif; ?>


        <?php endif; ?>


    </div>
    <hr class="l_header mt-3">
    <!-- Student Profile -->
    <div class="student-profile py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card shadow-sm border-primary">
                        <div class="card-header bg-transparent text-center">
                            <!--                                <img class="profile_img" src="https://placeimg.com/640/480/arch/any" alt="">-->

                            <?php if ($model->school_logo): ?>
                                <img class="profile_img" src="data:image/jpeg;base64,<?= $model->logo->image_base64 ?>"
                                />
                            <?php else : ?>
                                <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'profile_img', 'alt' => 'school logo']) ?>

                            <?php endif; ?>
                            <h3><?= $model->school_name ?></h3>
                        </div>
                        <!--  <div class="card-body">
                              <p class="mb-0"><strong class="pr-1">Student ID:</strong>321000001</p>
                              <p class="mb-0"><strong class="pr-1">Class:</strong>4</p>
                              <p class="mb-0"><strong class="pr-1">Section:</strong>A</p>
                          </div>-->
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card border-primary">
                        <div class="card-header bg-transparent border-0">
                            <h3 class="mb-0"><i class="far fa-clone pr-1"></i>School Information</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%">School code</th>
                                    <td width="2%">:</td>
                                    <td><?= ($model->external_school_code) ? $model->external_school_code : "--" ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">Physical Address</th>
                                    <td width="2%">:</td>
                                    <td><?= ($model->village) ? $model->village : "--" ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">District</th>
                                    <td width="2%">:</td>
                                    <td><?= ($model->districtName->district_name) ? $model->districtName->district_name : "--" ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">Region</th>
                                    <td width="2%">:</td>
                                    <td><?= ($model->regionName->description) ? $model->regionName->description : "--" ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">School Type</th>
                                    <td width="2%">:</td>
                                    <td> <?= ($model->school_type) ? $model->schoolType->description : "-- " ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">Email</th>
                                    <td width="2%">:</td>
                                    <td><?= ($model->contact_email_1) ? $model->contact_email_1 : "--" ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">Phone</th>
                                    <td width="2%">:</td>
                                    <td> <?= ($model->phone_contact_1) ? $model->phone_contact_1 : "--" ?></td>
                                </tr>

                                <tr>
                                    <th width="30%">SMS Sender ID</th>
                                    <td width="2%">:</td>
                                    <td> <?= ($model->sms_sender_id) ? $model->sms_sender_id : "--" ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">Branch</th>
                                    <td width="2%">:</td>
                                    <td> <?= ($model->branch_id) ? $model->branches->branch_name : "-- " ?></td>
                                </tr>
                                <tr>
                                    <th width="30%">Branch Region</th>
                                    <td width="2%">:</td>
                                    <td> <?= ($model->branch_region) ? $model->regions->bank_region_name : "-- " ?></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="card border-primary">
        <div class="card-body">


            <main class="tab-main">
                <?php if (Yii::$app->user->can('can_view_account')) : ?>
                <input id="tab1" type="radio" name="tabs" checked>
                <label for="tab1">About</label>
 <?php endif?>
                <input id="tab5" type="radio" name="tabs">
                <label for="tab5">Campuses</label>

                <?php if (Yii::$app->user->can('payments_received_list')) : ?>
                    <input id="tab2" type="radio" name="tabs">
                    <label for="tab2"> Classes</label>

                    <input id="tab3" type="radio" name="tabs">
                    <label for="tab3">Transaction History</label>

                    <input id="tab4" type="radio" name="tabs">
                    <label for="tab4">Payments Received</label>
                <?php endif; ?>

                <?php if (Yii::$app->user->can('can_view_account')) : ?>
                <section id="content1">
                    <p>
                        <?= $this->render('_tab_school_info', ['model' => $model, 'accounts' => $accounts, 'sections' => $sections]) ?>
                    </p>

                </section>
                <?php endif;?>
                <section id="content5">
                    <p>
                        <?= $this->render('campus_info_tab', ['model' => $model, 'campuses' => $campuses, 'sections' => $sections]) ?>
                    </p>

                </section>
                <?php if (Yii::$app->user->can('payments_received_list')) : ?>
                    <section id="content2">

                        <p>
                            <?= $this->render('_tab_school_classes', ['model' => $model]) ?>
                        </p>
                    </section>

                    <section id="content3">

                        <p>
                            <?= $this->render('_tab_trans_history', ['model' => $model]) ?>
                        </p>
                    </section>

                    <section id="content4">
                        <p>
                            <?= $this->render('_tab_payments_received', ['model' => $model]) ?>
                        </p>

                    </section>
                <?php endif; ?>


            </main>
        </div>
    </div>
</div>
<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#schoolInfo").addClass('active');
    $("#schools").addClass('active');
  });
JS;
$this->registerJs($script);
?>

<?php $this->registerJs("(function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);", yii\web\View::POS_END, 'responsive-tab'); ?>

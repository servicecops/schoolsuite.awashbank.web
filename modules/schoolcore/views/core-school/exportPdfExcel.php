<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
    if($type == 'Excel') {
        echo "<table><tr> <th colspan='7'><h3> School Info Datasheet</h3> </th> </tr> </table>";
    }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>School Code</th><th>School Name</th><th>District</th><th>Phone number</th><th>School Type</th></tr>
        </thead>
        <tbody>
        <?php
        $no = 1;
        foreach($query as $sinfo) : ?>
            <tr>
                <?php if($type == 'Pdf') {
                    echo "<td>".$no."</td>";
                } ?>
                <td><?= ($sinfo['school_code']) ? $sinfo['school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                <td><?= ($sinfo['school_name']) ? $sinfo['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                <td><?= ($sinfo['district_name']) ? $sinfo['district_name'] : '<span class="not-set">(not set) </span>' ?></td>
                <td><?= ($sinfo['phone_contact_1']) ? $sinfo['phone_contact_1'] : '<span class="not-set">(not set) </span>' ?></td>
                <td><?= ($sinfo['school_type']) ? $sinfo['school_type'] : '<span class="not-set">(not set) </span>' ?></td>
            </tr>
            <?php $no++; ?>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>

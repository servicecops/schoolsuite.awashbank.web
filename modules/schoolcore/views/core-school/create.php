<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Create School';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mt-5">
    <div class="school-information-create hd-title mt-5" data-title="Add School">
        <div class="letters">
            <div class="row">
                <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
            </div>
 <div class="row">
     <div id="flash_message">
         <?php if (\Yii::$app->session->hasFlash('actionFailed')) : ?>
             <?= \Yii::$app->session->getFlash('actionFailed'); ?>
         <?php endif; ?>
     </div>
 </div>


            <div class="row">
                <?= $this->render('_form', [
                    'model' => $model,  'sch_modules'=>[], 'banks'=>$banks,
                    'bank_info_error'=>$bank_info_error, 'accounts'=>$accounts,
                    'model_error' => $model_error, 'sp_modules'=>$sp_modules
                ]) ?>
            </div>


        </div>
    </div>
</div>


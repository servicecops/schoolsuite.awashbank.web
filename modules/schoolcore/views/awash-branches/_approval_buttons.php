<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>



    <?php
    $form = ActiveForm::begin([
        'action' => ['do'],
        'method' => 'post',
        'options' => ['class' => 'formprocess', 'id' => 'workflowActionsForm']
    ]); ?>
    <div class="col-md-12 ">
        <?= $form->field($actionModel, 'notes')->textarea()->label('Approval / Rejection Notes') ?>
        <input type="hidden" name="Dissertation[item]" value="1" />

    </div>

    <div class="col-md-12">
        <?= Html::button('Approve <i class="fa fa-check-circle"></i>', ['class' => 'btn btn-success',
            'onclick' => 'confirmAndSubmitWorkflow("workflowActionsForm", "Are you sure you want to approve this dissertation, action can not be undone", "do?action=approve")',
            'value' => 'Approve', 'name' => 'submit']) ?>

        <?= Html::button('Reject <i class="fa fa-times-circle"></i>', ['class' => 'btn btn-danger pull-right',
            'onclick' => 'confirmAndSubmitWorkflow("workflowActionsForm", "Are you sure you want to reject this dissertation, action can not be undone", "do?action=reject")',
            'value' => 'Reject', 'name' => 'submit']) ?>

    </div>
    <?php ActiveForm::end(); ?>
<div style="clear: both"></div>




<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use dosamigos\ckeditor\CKEditor;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\web\View;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class=" row ">

    <div class="col-md-12">
        <?= ($model->isNewRecord) ? '<h3><i class="fa fa-plus"></i> Add New Branch</h3>' : '<h3><i class="fa fa-edit"></i> Edit Branch</h3>' ?>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>

    <div class="col-md-12">
        <p>All fields marked with * are required</p>

        <?php

        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => 'create']);

        if (!$model->isNewRecord) { //edit mode
            $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => ['update', 'id' => $model->id]]);
        }

        ?>
        <div class="col-md-12">
            <div class="col-md-4">

                <?php
                $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
                echo $form->field($model, 'branch_region')
                    ->dropDownList(
                        $items,           // Flat array ('id'=>'label')
                        ['prompt' => 'Select region',
                            'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });']    // options
                    ); ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'branch_code', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Branch Code *') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'branch_name', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Branch Name *') ?>
            </div>







        </div>
        <div class="col-md-12">
            <div class="card-footer">
                <div class="row">
                    <div class="col-xm-6">
                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                    </div>
                    <div class="col-xm-6">
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

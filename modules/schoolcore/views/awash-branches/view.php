<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;
use app\modules\dissertation\models\Dissertation;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->branch_name;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->branch_name;

?>


<div class="row ">
    <div class="col-md-12">
        <h3><i class="fa fa-plus"></i> <?= ($model->branch_name) ? $model->branch_name : "--" ?></h3>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>



    <div class="col-md-12">
        <?php if (Yii::$app->user->can('rw_branches')) : ?>
        <div class="pull-right">

                <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this announcement?',
                        'method' => 'post',
                    ],
                ]) ?>




        </div>
        <?php endif; ?>
    </div>


    <br><br>

    <div class="col-md-12 ">
        <div class="col-md-12 " style="padding:15px">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => $attributes
            ]) ?>
        </div>


        <br>
    </div>


</div>

<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

?>



    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'branch_code', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Search By Code'])->label(false) ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'branch_name', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Search By Branch'])->label(false) ?>

    </div>


    <div class="col-md-3">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>



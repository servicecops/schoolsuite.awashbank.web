<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreGuardian */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Core Guardians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="core-guardian-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_created',
            'date_updated',
            'name',
            'phone_contact_1',
            'phone_contact_2',
            'address',
            'email_address:email',

            [
                'label' => 'School Name',
                'value'=>function($model){return $model->schoolName->school_name;},
            ],
            [
                'label' => 'Created By',
                'value'=>function($model){return $model->userName->firstname.' '.$model->userName->lastname;},
            ],

        ],
    ]) ?>

</div>

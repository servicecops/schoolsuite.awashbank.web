<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreGuardianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Guardians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-guardian-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Core Guardian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_created',
            'date_updated',
            'created_by',
            'name',
            //'phone_contact_1',
            //'phone_contact_2',
            //'address',
            //'email_address:email',
            //'school_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

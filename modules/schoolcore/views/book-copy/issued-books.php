<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\BookCopySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book Copies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-copy-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'date_created',
//            'date_updated',
//            'created_by',
            [
                'attribute'=>'Book Name',
                'value'=>function($model){
                    return $model->bookName->book_title;

                },
                'format'=>'html',

            ],
            'issue_date',
            'return_date',
            'ISBN_number',
            [
                'attribute'=>'Issued To',
                'value'=>function($model){
                    return $model->student->first_name .'  '. $model->student->last_name;

                },
                'format'=>'html',

            ],
            [
                'attribute'=>'Student Code',
                'value'=>function($model){
                    return $model->student->student_code;

                },
                'format'=>'html',

            ],
            [
                'attribute'=>'Overdue',
                'contentOptions' => ['class' => 'badge bg-danger font-12 text-white font-weight-medium badge-pill ml-2 d-md-none d-lg-block'],
                'value'=>function($model){
                    if ($model->return_date > date('Y-m-d H:i')){

                        return 0 .' '.'days';

                    }else{
                        return round((( strtotime(date('Y-m-d H:i')))-(strtotime($model->return_date)))/(60 * 60 * 24)).' '.'days';

                    }

                },
                'format'=>'html',

            ],

        ],
    ]); ?>


</div>

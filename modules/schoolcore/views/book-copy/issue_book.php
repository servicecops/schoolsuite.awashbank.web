<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\BookCopy */
/* @var $form yii\widgets\ActiveForm */


$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
'<div class="row">' + 
    '<div class="col-sm-5">' +
        '<img src="' + repo.passport_photo + '" class="img-rounded" style="width:30px" />' +
        '<b style="margin-left:5px">' + repo.id + '</b>' + 
    '</div>' +
    '<div class="col-sm-3"><i class="fa fa-user"></i> ' + repo.last_name + '</div>' +
    '<div class="col-sm-3"><i class="fa fa-book"></i> ' + repo.class_id + '</div>' +
'</div>';
    if (repo.student_email) {
      markup += '<p>' + repo.student_code + '</p>';
    }
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.first_name || repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
     return {
            // Change `data.items` to `data.results`.
            // `results` is the key that you have been selected on
            // `actionJsonlist`.
            results: data.results
        };
}
JS;

$url = Url::to(['student-list']);
?>

<div class="book-copy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $listData=ArrayHelper::map(\app\modules\schoolcore\models\CoreStudent::find()->all(),'id','student_code');
    echo $form->field($model, 'student_id')->dropDownList(
        $listData,
        ['prompt'=>'Select...']
    );
    ?>

    <?= $form->field($model, 'issue_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter issue date ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'return_date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter return date ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

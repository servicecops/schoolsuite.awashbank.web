<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\BookCopy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-copy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ASN_No')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ISBN_number')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\BookCopy */
/* @var $form yii\bootstrap4\ActiveForm */


$this->title = 'Check return book';
?>

<div class="container">
    <h3 class="text-center text-dark"><?= Html::encode($this->title) ?></h3>
    <div class="row">
        <div class="col">

        </div>
        <div class="col">
            <div class="row card">
                <div class="card-body">
                    <div>
                        <h4>Issued To: <?= Html::encode($model->student->first_name) .' '. ($model->student->last_name) ?></h4>
                    </div>
                    <div>
                        <h4>Student Code: <?= Html::encode($model->student->student_code) ?></h4>
                    </div>
                    <div>
                        <h4>ISBN Number: <?= Html::encode($model->ISBN_number) ?></h4>
                    </div>

                    <?php $form = ActiveForm::begin(); ?>

                    <?php
                    $model->student_id;
                    ?>

                    <?= $form->field($model, 'date_returned')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'Enter return date ...'],
                        'pluginOptions' => [
                            'autoclose' => true
                        ],
                        'value'=>''
                    ]) ?>
                    <div class="form-group">
                        <?= Html::submitButton('Confirm Return', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div>
        <div class="col">

        </div>
    </div>
</div>

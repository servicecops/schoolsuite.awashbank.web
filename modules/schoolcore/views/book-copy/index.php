<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\BookCopySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book Copies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-copy-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Book Copy', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'date_created',
//            'date_updated',
//            'created_by',
            'ISBN_number',
            //'book_id',

            ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>


</div>

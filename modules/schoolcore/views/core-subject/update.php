<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Update Core Subject: ' . $model->subject_name;
$this->params['breadcrumbs'][] = ['label' => 'Core User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-user-update">

   <div class="model_titles">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Update Subject</h3></div>
</div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

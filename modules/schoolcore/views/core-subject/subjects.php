<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-student-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Core Student', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'subject_name',
            'subject_code',
            'school_name',
            'class_description',
                        [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Add Test', ['core-test/add-test', 'id' => $model['id']]);
                },
            ],
        ],
    ]); ?>


</div>

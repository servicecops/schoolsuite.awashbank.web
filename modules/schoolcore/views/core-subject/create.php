<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Core Subject';
?>
<div class="core-student-create">

    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Add Subject Information</h3></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

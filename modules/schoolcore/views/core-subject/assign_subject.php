
<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
?>
<?php $form = ActiveForm::begin([
    'action' => ['view', 'id'=>$model->id],
    'method' => 'post',
    'options'=>['class'=>'formprocess'],
]);

 $schoolId;
?>
<div class=" container mb-5">



            <div class="col-sm-6">
                <?php
                // $url = Url::to(['../schoolcore/core-school/active-schoollist']);
                $url = Url::to(['core-school/active-schoollist']);

                $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                $selectedSchoolId =empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->id;
                echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'size' => 'sm',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'class' => 'form-control  input-sm inputRequired',
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'school_search',
                        'onchange' => '$.post( "' . Url::to(['core-subject/class-lists']) . '?id="+$(this).val(), function( data ) {
                          // alert("yes"+JSON.stringify(data));
                            schoolId =value;
                           alert("check"+schoolId);
                            $( "select#coresubject-class_id" ).html(data);
                        });'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false); ?>

            </div>

        <div class="col-lg-5 col-sm-5 col-xs-12">
            <b>Assign Classes</b>:

            <div class="col-sm-6" id="school_div">

                <?php

                $schoolId =27;
                $url2 = Url::to(['core-subject/class-lists', 'id'=>$selectedSchoolId]);
                  echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                    'initValueText' => 'check', // set the initial display text
                    'theme'=>Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Select Class',
                        'id'=>'class_id',
                        'multiple'=>true,
                        'class'=>'form-control',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url'=>$url2,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                                         ],

                ])->label(''); ?>
            </div>
        </div>



    </div>
    <div class="form-group col-md-12 ">
        <div class="col-md-6 col-md-offset-3">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php

$script = <<< JS



$("document").ready(function(){
function evaluateRecurrentVisibility() {
if($('#fee_type_select').val() === '1') {
$('#recurrent-settings-container').show()
}else {
$('#recurrent-settings-container').hide()
}
}

}

);

JS;
$this->registerJs($script);
?>


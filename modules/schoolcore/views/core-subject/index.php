<?php

use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSubjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subjects Information';

$columns = [
    'subject_code',
    [
        'label' => 'Subject Name',
        'value' => function ($model) {
            return \yii\helpers\Html::a($model['subject_name'], ['core-subject/view', 'id' => $model['id']], ['class' => 'aclink']);
        },
        'format' => 'html',
    ],
    'class_description',
    'school_name',
    [

        'label' => '',
        'format' => 'raw',
        'value' => function ($model) {
            return Html::a('Add Lesson', ['core-subject/create-lesson', 'subjectId' => $model['id']]);
        },
    ],
    ////
];

?>


<div class="col-xs-12">
    <div>
        <div class="col-sm-4 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-sm-5 col-xs-12 no-padding"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <?= GridView::widget([
                'dataProvider'=> $dataProvider,

                'columns' => $columns,
                'resizableColumns'=>true,
//    'floatHeader'=>true,
                'responsive'=>true,
                'responsiveWrap' => false,
                'bordered' => false,
                'striped' => true,
            ]); ?>

        </div>


    </div>
</div>

</div>

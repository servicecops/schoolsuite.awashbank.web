<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> School Info Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Subject Code</th><th>Subject Name</th><th>Class Name</th><th>School Name</th></tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['subject_code']) ? $sinfo['subject_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['subject_name']) ? $sinfo['subject_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['class_description']) ? $sinfo['class_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['school_name']) ? $sinfo['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

<?php 
use yii\helpers\Url;

        if($status['state']){
            echo $status['message'];
        }
        ?>
        
        <div class="row">
        <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">
           <?php  if(!$status['state']) : ?>

            <table class="table" style="background-color:#fff; margin-bottom:3px;" >
                <tr>
                    <th class="pull-right no-padding"><a class='aclink' href="<?= Url::to(['/schoolcore/core-subject/file-details', 'id'=>$file]) ?>" data-confirm="Please confirm to save subject" data-method="post" data-params='{"action":"save"}'><span class="btn btn-primary btn-sm"><b>Click to Submit Subject </b>&nbsp; &nbsp;</span></a></th>

                </tr>

            </table>
            <?php

            endif;
            ?>
        <div >
        <table class="table table-striped ">
        <thead>
        <tr>
            <th>Sr.No</th>
            <th>Subject Name</th>
            <th>Subject Code</th>
            <th>Class Name</th>
            <th>School Name </th>

            <th>&nbsp;&nbsp;&nbsp;</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            $srNo = 1;
            foreach($data as $k=>$v) : ?>
                <tr><td><?= $srNo; ?></td>
                    <td><?= ($v['subject_name']) ? $v['subject_name'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['subject_code']) ? $v['subject_code'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['class_description']) ? $v['class_description'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set"> -- </span>' ?></td>

                </tr>
            <?php 
            $srNo++;
            endforeach; ?>
        </tbody>
        </table>
        </div>
    </div>
    </div>
    </div>

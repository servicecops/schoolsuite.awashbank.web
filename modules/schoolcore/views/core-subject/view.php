<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSubject */

$this->title;
$this->params['breadcrumbs'][] = ['label' => 'Core Subject', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
<main class="container">
        <div class="title">
            <div>

                <?php
                if (\Yii::$app->user->can('rw_subject')) :

                    ?>
                    <p>
                    <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
                    <?= Html::a('<i class="fa fa-trash" style="color:#fff;">Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-md btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>

                    <p class="pull-right">
                        <?= Html::a('<i class="fa fa-edit"></i>Create New Subject', ['/schoolcore/core-subject/create', 'id' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
                    </p>

                <?php endif; ?>
            </div>
            <div class="align-content-center">
                <h2></h2><i class="fa fa-book" style="color: red"></i>
                <h2 style="color: #000" ><?= ($model->subject_name) ? $model->subject_name : ' --' ?>
                </h2>
            </div>



            <article class="details">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="col-lg-6 col-sm-6 col-xs-12 no-padding bg-row">
                        <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Created
                                By: </b></div>
                        <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->userName->firstname . ' ' . $model->userName->lastname) ?></div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
                        <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Date
                                Created: </b></div>
                        <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->date_created) ?></div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="col-lg-6 col-sm-6 col-xs-12 no-padding bg-row">
                        <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Subject Code </b></div>
                        <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->subject_code) ? $model->subject_code : ' --' ?></div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
                        <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Class Code
                            </b></div>
                        <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->className->class_code) ? $model->className->class_code : ' --' ?></div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="col-md-12 col-sm-6 col-xs-12 no-padding bg-row">
                        <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Subject Outline </b></div>
                        <div class="col-lg-6 col-xs-6 "><?= ($model->subject_outline) ? $model->subject_outline : ' --' ?></div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12 no-padding">
                        <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Subject description</b></div>
                        <div class="col-lg-6 col-xs-6" ><?= ($model->subject_outline) ? $model->subject_description : ' --' ?></div>
                    </div>
                </div>
            </div>


            </article>



</main>
</div>
<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="formz">

    <div class="row card">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <p>All fields marked with * are required</p>

        <div class="card-body">
            <div class="row">
                <?php if (\app\components\ToWords::isSchoolUser()) : ?>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php
                        $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id ,'active'=>true])->all();

                        echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map($data, 'id', 'class_code'),
                            'language' => 'en',
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Find Class'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Class *'); ?>
                    </div>

                <?php endif; ?>
                <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>

                    <div class="col-sm-6 ">
                        <?php
                        $url = Url::to(['core-school/active-schoollist']);
                        $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                        echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                            'initValueText' => $selectedSchool, // set the initial display text
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => 'Filter School',
                                'id' => 'school_search',
                                'class' => 'form-control',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                            ],
                        ])->label(' School *'); ?>

                    </div>

                    <div class="col-sm-6">
                        <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
                            ['prompt' => 'Filter class'])->label('Class *') ?>            </div>

                <?php endif; ?>

            </div>
            <div class="row">

                <div class="col">
                    <?= $form->field($model, 'subject_code', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Subject Code']])->textInput() ->label('Subject Code *')?>


                </div>

                <div class="col">
                    <?= $form->field($model, 'subject_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Subject Name ']])->textInput() ->label('Subject Name *')?>

                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'subject_description')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'preset' => 'basic'
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'subject_outline')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'preset' => 'basic'
                    ]) ?>
                </div>
            </div>

            <div class="row">

                <div class="col">
                    <?= $form->field($model, 'subject_compulsory')->radioList([1 => 'Yes', 0 => 'No']); ?>
                </div>


            </div>

            <div class="row">

            </div>


            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::resetButton('Reset <i class="fa fa-save"></i>', ['class' => 'btn btn-default btn-block']) ?>
                    </div>
                </div>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$url = Url::to(['core-school-class/lists']);
$cls = $model->class_id;

$campusurl = Url::to(['/school-information/campuslists']);

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
    
    };
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\BookCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Book Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category_name',
            ['class' => 'kartik\grid\ActionColumn'],
        ],
    ]); ?>


</div>

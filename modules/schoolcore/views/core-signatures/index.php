<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core Signatures';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">


    <?php

    $columns = [
        [
            'label' => 'Name',
            'value' => function ($model) {
                return Html::a($model['name'], ['core-signatures/view', 'id' => $model['id']], ['class' => 'aclink']);
            },
            'format' => 'html',
        ],
        [
            'label' => 'Position',
            'value' => function ($model) {
                return $model['position'];
            },
        ], [
            'label' => 'Email',
            'value' => function ($model) {
                return $model['email'];
            },
        ], [
            'label' => 'Contact',
            'value' => function ($model) {
                return $model['contact'];
            },
        ],
        [
            'label' => 'School Name',
            'value' => function ($model) {
                return $model['school_name'];
            },
        ],
        [
            'label' => 'Digital Signature',
            'format' => 'raw',
            'value' => function ($model) {
                if ($model['file_attachment'] != '')
                    return '<img src="' . Yii::$app->homeUrl . '/web/uploads/signatures/' . $model['file_attachment'] . '" width="50px" height="auto">'; else return 'no image';
            },
        ],
        'date_created',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-signatures/view/', 'id' => $model['id']]);
            },
        ],
        ////
    ];
    ?>
    <div class="row">
        <div class="col-xs-12">

            <div class="col-sm-3 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i
                            class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            <div class="col-sm-6 col-xs-12 no-padding"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
            <div class="col-sm-3 col-xs-12 no-padding">
                <div class="float-right">

                    <?php


                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'target' => '_blank',
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            ExportMenu::FORMAT_EXCEL => false,

                        ],
                        'dropdownOptions' => [
                            'label' => 'Export',
                            'class' => 'btn btn-outline-secondary'
                        ]
                    ])
                    ?>
                </div>
                <?php if (Yii::$app->user->can('rw_term')) : ?>
                    <?= Html::a('Add new Signature', ['create'], ['class' => 'btn btn-sm btn-primary  float-right']) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="mt-3">
        <div class="float-right">

        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
            'columns' => $columns,
            'resizableColumns' => true,
//    'floatHeader'=>true,
            'responsive' => true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>

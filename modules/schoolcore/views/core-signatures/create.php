<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSignatures */
//
$this->title = 'Create Core Signatures';
$this->params['breadcrumbs'][] = ['label' => 'Core Signatures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-signatures-create">
    <div class=" col-xs-12 no-padding">
        <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

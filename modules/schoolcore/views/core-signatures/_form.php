<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\District;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSignatures */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">


    <?php
    $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']]); // important
    ?>

    <div style="padding: 10px;width:100%"></div>

    <div class="row">

        <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>

            <div class="col-sm-6 ">
                <?php
                $url = Url::to(['core-school/active-schoollist']);
                $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'school_search',
                        'class' => 'form-control',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json'
                        ],
                    ],
                ])->label('School Name'); ?>

            </div>
        <?php endif; ?>
    </div>
    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Full Name']])->textInput() ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Email address']])->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'contact', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Phone Number']])->textInput() ?>
        </div>

        <div class="col-sm-6">
        <?= $form->field($model, 'position', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Job Description']])->textInput() ?>
        </div>


        <div class="col-sm-4">
            <?=
            $form->field($model, 'image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'gif', 'png'], 'showUpload' => false,],
            ]);
            ?>
        </div>

    </div>


    <hr/>


</div>
<hr/>

<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSignatures */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Core Signatures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="core-signatures-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'position',
            'contact',
            'email:email',
            'date_created',
            'id',
            'date_updated',
        ],
    ]) ?>

    <?php
    if ($model->file_attachment!='') {
        echo '<br /><p><img src="'.Yii::$app->homeUrl. '/web/uploads/signatures/'.$model->file_attachment.'" style="height: 100px;"></p>';
    }
    ?>

</div>

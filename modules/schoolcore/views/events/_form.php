<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'event_date')->widget(DatePicker::className(),
        [
            'model'=>$model,
            'attribute'=>'date_of_birth',
            'dateFormat'=>'yyyy-MM-dd',
            'clientOptions' =>[
                'changeMonth'=> true,
                'changeYear'=> true,
                'yearRange'=>'1980:'.(date('Y')),
                'autoSize'=>true,
                'dateFormat'=>'yyyy-MM-dd',
            ],
            'options'=>[
                'class'=>'form-control',
                'placeholder'=>'Choose Date'
            ],])->label('Event Date')?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataprovider yii\data\ActiveDataProvider */


$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="shade">
    <div class="blackboard">
        <div class="form">
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => 'event_item'
            ]);

            ?>
        </div>
    </div>
</div>

<?php

use app\modules\schoolcore\models\EventsSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\schoolcore\models\Events */

?>
<div class="container">

    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $dataProvider,
   'itemView' => 'event_item'
    ]); ?>
</div>

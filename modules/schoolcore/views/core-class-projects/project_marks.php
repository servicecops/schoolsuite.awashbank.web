<?php

use kartik\grid\EditableColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreProjectAssessment */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);


//$studentId = $model['id'];
$columns = [
    'student_code',
    'first_name',
    'last_name',
    'other_names',
    [
        'label' => '',
        'format' => 'raw',
        'value' => function ($model) {
            $rowId = $model['id'];
            return Html::textInput("marks_obtained[$rowId]", $model['marks_obtained'], ['class' => 'formprocess', 'placeholder' => 'Enter marks']);
        },
    ],

];

?>


<div class="core-school-view">
    <h2>Add Student project Marks</h2>

    <?php /** @var TYPE_NAME $id */

    $form = ActiveForm::begin([
        'action' => ['project-marks', 'id' => $id],
        'method' => 'post',
        'options' => ['class' => 'formprocess']
    ]); ?>
    <div class="row">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'floatHeaderOptions' => ['top' => '50'],
            'responsive' => true,
            'hover' => true,
            'bordered' => false,
            'striped' => true,
            'options' => [
                'class' => 'bg-colorz',
            ],
        ]); ?>
    </div>
    <div class="row">
        <?= Html::submitButton('<span style="color: #fff">submit Marks</span>', ['class' => 'btn bg-primary btn-sm']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>



<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTest */

//$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-9 pull-right">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php if (\app\components\ToWords::isSchoolUser()): ?>

            <?= Html::a('Add project Marks', ['core-class-projects/project-marks', 'id' => $model->id], ['class' => 'btn  btn-primary']) ?>
        <?php endif; ?>

        <?php if (\app\components\ToWords::isSchoolUser()): ?>

            <?= Html::a('View Project Marks', ['core-class-projects/marks', 'id' => $model->id], ['class' => 'btn  btn-primary']) ?>
        <?php endif; ?>

        <?php if (\app\components\ToWords::isSchoolUser()): ?>

            <?= Html::a('<i class="fa fa-edit" style="color:#fff;">Update</i>', ['update', 'id' => $model->id], ['class' => 'btn bg-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" style="color:#fff;">Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn bg-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

        <?php endif; ?>
        <?php if (!Yii::$app->user->can('non_student')): ?>

            <?= Html::a($model->test_description, ['update', 'id' => $model->id], ['class' => 'btn bg-primary']) ?>
        <?php endif; ?>

    </div>
</div>
<div class="row card">
    <div class="card-body">
        <div class="row">
            <div class="col border-right bg-gradient-light">
                <div class="card bg-gradient-light">
                    <div class="card-body">

                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Project Name</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model['project_name'] ?>
                            </div>
                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Project assessment category</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->projectCategory->name ?>
                            </div>
                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Description</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model['description'] ?>
                            </div>


                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Date Created</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->date_created ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col bg-gradient-light">
                <div class="card bg-gradient-light">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Subject:</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->subjectName->subject_name ?>
                            </div>


                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Class Name:</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->className->class_description ?>
                            </div>


                        </div>

                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Term Name</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->termName->term_name ?>
                            </div>
                        </div>

                        <hr class="style14">
                        <div class="row">

                            <div class="col">
                                <b style="color: #000">Created By</b>
                            </div>
                            <div class="col" style="color: #21211f">,
                                <?= $model->userName->firstname . ' ' . $model->userName->lastname ?>
                            </div>

                        </div>

                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Date Modified</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <span><?= ($model->date_modified) ? $model->date_modified : ' Not yet modified' ?></span>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

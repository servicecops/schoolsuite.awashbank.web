<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
///* @var $searchModel app\modules\schoolcore\models\CoreMarks */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Project Results';
$this->params['breadcrumbs'][] = $this->title;
$columns = [
    'student_code',
    'first_name',
    'last_name',
    'marks_obtained',
    [

        'label' => '',
        'format' => 'raw',
        'value' => function ($model) {
            return Html::a('<i class="fas fa-pen"></i>', ['core-project-assessment/edit', 'id' => $model['id']]);
        },
    ],
];
?>
<div class="core-project-assessment-index">
    <div class="row">
        <div class="col-xs-12">

            <div class="col-sm-3 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            <div class="col-sm-6 col-xs-12 no-padding"><?php echo $this->render('search_project_marks', ['model' => $searchModel]); ?></div>
            <div class="col-sm-3 col-xs-12 no-padding">
                <div class="float-right">

                    <?php


                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'target' => '_blank',
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            ExportMenu::FORMAT_EXCEL => false,

                        ],
                        'dropdownOptions' => [
                            'label' => 'Export School circulars',
                            'class' => 'btn btn-outline-secondary'
                        ]
                    ])
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
    ]); ?>


</div>

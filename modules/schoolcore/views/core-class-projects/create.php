<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreClassProjects */

$this->title = 'Create Class Projects';
$this->params['breadcrumbs'][] = ['label' => 'Core Class Projects', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-class-projects-create">

    <div class=" col-xs-12 no-padding">
        <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

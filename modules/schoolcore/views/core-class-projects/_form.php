<?php

use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use Itstructure\CKEditor\CKEditor;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreClassProjects */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-group">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div style="padding: 10px;width:100%"></div>

    <div class="row">
        <?php if (\app\components\ToWords::isSchoolUser()) : ?>
            <div class="col-sm-4">
                <?php
                $data = CoreTerm::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'term_name'),
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['placeholder' => 'Find Term'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Term *'); ?>
            </div>
        <?php endif; ?>

        <div class="col-md-4">
            <?php
            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Class', 'id' => 'class_search'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Class *'); ?>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="label-default">You could exclude some students(Paste payment codes (Separate with
                    spaces or commas)) </label>
                <textarea class="form-control" id="student_codes" name="student_codes" rows=""
                          placeholder="Paste student codes"></textarea>

            </div>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'subject_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'class_subject_drop']])->dropDownList(
                ['prompt' => 'Filter subjects'])->label('Subject') ?>
        </div>

    </div>
    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'project_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Project Name']])->textInput() ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'project_category')
                ->dropDownList(ArrayHelper::map(\app\modules\schoolcore\models\ClassProjectCategories::find()->all(), 'id', 'name'))
            ?>
        </div>

        <div class="col-sm-12">
            <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Project Name']])->textArea(['rows' => '6']) ?>
        </div>


    </div>


    <hr/>


</div>
<hr/>

<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?php
$url = Url::to(['/schoolcore/core-subject/lists']);
$cls = $model->subject_id;

$script = <<< JS
var classChanged = function() {
        var subject = $("#class_search").val();
        $.get('$url', {id : subject}, function( data ) {
                    $('select#class_subject_drop').html(data);
                    $('#class_subject_drop').val('$cls');
                });
        
        
         $.get('$url', {id : subject}, function( data ) {
                    $('select#class_subject_drop').html(data);
                    $('#class_subject_drop').val('$cls');
                });
        
    
    }
    
$("document").ready(function(){
    
    if($("#class_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#class_search', function(){
         classChanged();
    });
  });
JS;
$this->registerJs($script);
?>


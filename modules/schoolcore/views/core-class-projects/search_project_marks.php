<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreClassProjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<div >
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['core-class-projects/marks/'.$model->id],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row mt-12">


            <div class="col">
                <?= $form->field($model, 'student_id', ['inputOptions'=>[ 'class'=>'form-control'],])->textInput(['title' => 'Enter student code',

                    'data-toggle' => 'tooltip',

                    'data-trigger' => 'hover',

                    'data-placement' => 'bottom'])->label(false) ?>

            </div>
            <div>

                <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

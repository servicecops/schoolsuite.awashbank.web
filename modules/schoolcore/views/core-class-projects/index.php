<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\studentreport\models\CoreSchoolCircularSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Class projects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">

    <?php

    $columns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {

                return Html::a($model['project_name'], ['core-class-projects/view', 'id' => $model['id']], ['class' => 'aclink']);

            },
        ],
        'class_description',
        'subject_name',
        'term_name',
        'date_modified',
        'attachment_file',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fas fa-edit"></i>', ['school-circular/update', 'id' => $model['id']]);
            },
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {

                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-class-projects/view', 'id' => $model['id']], ['class' => 'aclink']);

            },
        ]
    ];
    ?>
    <div class="row">
        <div class="col-xs-12">

            <div class="col-sm-3 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i
                            class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            <div class="col-sm-6 col-xs-12 no-padding"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
            <div class="col-sm-3 col-xs-12 no-padding">
                <div class="float-right">

                    <?php


                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'target' => '_blank',
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            ExportMenu::FORMAT_EXCEL => false,

                        ],
                        'dropdownOptions' => [
                            'label' => 'Export School circulars',
                            'class' => 'btn btn-outline-secondary'
                        ]
                    ])
                    ?>
                </div>
                <?php if (\app\components\ToWords::isSchoolUser()) : ?>

                    <div class="pull-right">
                        <a class="btn btn-sm btn-primary  float-right"
                           href="/schoolsuite/schoolcore/core-class-projects/create">Add New Class project</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="mt-3">
        <div class="float-right">

        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
            'columns' => $columns,
            'resizableColumns' => true,
//    'floatHeader'=>true,
            'responsive' => true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>

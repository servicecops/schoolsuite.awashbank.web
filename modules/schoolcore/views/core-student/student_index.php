<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataprovider yii\data\ActiveDataProvider */


$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;


$DragJS = <<<EOF
/* initialize the external events
-----------------------------------------------------------------*/

$('#external-events .fc-event').each(function() {
    // store data so the calendar knows to render an event upon drop
    $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        stick: true // maintain when user navigates (see docs on the renderEvent method)
    });
    // make the event draggable using jQuery UI
    $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
    });
});

EOF;

$this->registerJs($DragJS);
?>
<?php
$class = \app\modules\schoolcore\models\CoreStudent::findOne(['student_code'=>Yii::$app->user->identity->username])

?>

<div class="row" id="student_index">
    <div class="col-md-12">
        <div class="container">
            <div class="col-md-6">
                <div class="cards">
                    <p class="container">


                        <a href="<?= \yii\helpers\Url::to(['/schoolcore/core-school-class/view','id'=>$class->class_id]) ?>" style="font-size: 18px">
                            <!--                        <i class="fa fa-folder-open fa-5x" style="color: #f7c41b"></i>-->
                            <?= Html::img('@web/web/img/classes.jpg', ['alt' => '', 'class' => 'img-fluids']); ?>

                            My Classes
                        </a>
                    </p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="cards">

                    <p class="container">
                        <a href="<?= Url::to(['/e_learning/elearning/menu']); ?>" style="font-size: 18px">
                            <!--                        <i class="fa fa-folder-open fa-5x" style="color: #f7c41b"></i>-->
                            <?= Html::img('@web/web/img/e_learning.jpg', ['alt' => '', 'class' => 'img-fluids']); ?>

                            E-learning
                        </a>
                    </p>
                </div>
            </div>
            <hr/>
        </div>
        <div class="col-md-12"><br/></div>
    </div>

    <div class="col-md-6">
        <div class="container">

            <h4>Subject Overview</h4>


        </div>

        <?php foreach ($subjects as $subject) { ?>

            <div class="cards">

                <p class="container">
                    <a href="<?= \yii\helpers\Url::to(['/schoolcore/core-subject/view', 'id' => $subject->id]) ?>" style="font-size: 18px">
                        <!--                        <i class="fa fa-folder-open fa-5x" style="color: #f7c41b"></i>-->
                        <?= Html::img('@web/web/img/my_subject.jpg', ['alt' => '', 'class' => 'img-fluids']); ?>

                        <?= $subject->subject_code ?>: <?= $subject->subject_name ?>
                    </a>
                </p>

            </div>
            <br/>
        <?php } ?>

    </div>
    <div class="col-md-6">



        <!--upcoming events-->
        <!--upcoming events-->
        <div class="col-md-4"></div>
        <div class="col-md-8">
            <div class="cards_calender">
                <div class="card-header">
                    Calendar
                </div>
                <div class="card-body">
                    <?php

                    $JSCode = <<<EOF
function(start, end) {
    var title = prompt('Event Title:');
    var eventData;
    if (title) {
        eventData = {
            title: title,
            start: start,
            end: end
        };
        $('#w0').fullCalendar('renderEvent', eventData, true);
    }
    $('#w0').fullCalendar('unselect');
}
EOF;

                    $JSDropEvent = <<<EOF
function(date) {
    alert("Dropped on " + date.format());
    if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
    }
}
EOF;

                    $JSEventClick = <<<EOF
function(calEvent, jsEvent, view) {

    alert('Event: ' + calEvent.title);
   
    // change the border color just for fun
    $(this).css('border-color', 'yellow');
     $(this).css('color', 'blue');

}

EOF;

                    ?>
                    <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                        'clientOptions' => [
                            'selectable' => true,
                            'selectHelper' => true,
                            'droppable' => true,
                            'editable' => true,
                            'drop' => new JsExpression($JSDropEvent),
                            'select' => new JsExpression($JSCode),
                            'eventClick' => new JsExpression($JSEventClick),
                            'defaultDate' => date('Y-m-d')
                        ],
                        'events'=> $events,
                    ));?>
                </div>
            </div>
        </div>
    </div>



    <div id="ad-features">
    <span class="ad-btn1">
        <i class="fa fa-times" aria-hidden="true"></i>
        <!--        <button aria-hidden="true"></button>-->
    </span>

        <!--REPLACE THIS IMAGE LINK-->
        <a href="#" target="_blank">
            <?= $this->render('all_school_notices.php') ?>

        </a>
    </div>
    <div style="clear: both"></div>
</div>
<?php
$script = <<< JS
    $(document).ready(function(){

        $("#ad-features").fadeIn()
        .animate({bottom:0,right:0}, 1500, function() {
            //callback
        });      

        $(".ad-btn1").click(function(e){
            e.preventDefault();
            e.stopPropagation()
            $('#ad-features').fadeOut();
        });
      
    });
JS;
$this->registerJs($script);
?>

<?php
use yii\helpers\Html;
/* @var $model app\modules\schoolcore\models\CoreSubject */
?>

<div class="container">

    <div class="row">


            <div style ="width:30%;float:left; padding:10px; margin:10px;background: #fff;">

                <div>
                    <a href="<?=\yii\helpers\Url::to(['/schoolcore/events/view', 'id'=>$model->id])?>">
                        <i class="fa fa-folder-open fa-5x" style="color: #f2d61b"></i>

                        <h5 class="text-dark event-title"><?=$model->subject_code. '  '.  Html::encode($model->subject_name) ?></h5>
                    </a>
                </div>
            </div>


    </div>


</div>
<div class="container">
    <div class="row">
        <div class="col-sm">
            One of three columns
        </div>
        <div class="col-sm">
            One of three columns
        </div>
        <div class="col-sm">
            One of three columns
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm">
            One ofd three columns
        </div>
        <div class="col-sm">
            One ofd three columns
        </div>
        <div class="col-sm">
            One ofd three columns
        </div>
    </div>
</div>

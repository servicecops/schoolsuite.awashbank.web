<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
$this->title = "Change Password for";
?>
<div class="container">
    <div>
        <h3 class="box-title text-center" style="color: #000"><b><i class="fa  fa-user-lock"></i> <?= Html::encode($this->title.'  '.($model->fullname))?></b></h3>
<hr class="style14">
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'change-password-form',
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
    ]); ?>
    <div class="row">
        <div class="col-sm">

        </div>
        <div class="col-sm">
            <div class="card mt-5">
                <div class="card-body">
                    <?= $form->field($model, 'current_pass',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['class'=>'form-control accounts_control','maxlength' => 60, 'placeholder' => 'Enter Current Password'])->label('Current Password') ?>
                    <?= $form->field($model, 'new_pass',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['class'=>'form-control accounts_control','maxlength' => 60, 'placeholder' =>'New Password' ])->label('New Password') ?>
                    <?= $form->field($model, 'password2',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['class'=>'form-control accounts_control','maxlength' => 60, 'placeholder' =>'Confirm New Password'])->label('Re-enter New password') ?>

                    <?= Html::submitButton(Yii::t('app', 'Save'),['class' =>'btn btn-primary'] ) ?>
                    <?= Html::a('Cancel', ['/site/index'], ['class' => 'btn btn-default']) ?>

                </div>
            </div>
        </div>
        <div class="col-sm">
            <div class="alert alert-danger invalid-accounts-error" style="display: none">
                Please supply all passwords information to continue
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>


<?php
$script = <<< JS
 $('document').ready(function(){
     
        let schoolForm = $('#change-password-form');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.accounts_control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val()) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });
    })
JS;
$this->registerJs($script);
?>
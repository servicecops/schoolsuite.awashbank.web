<?php

use app\modules\feesdue\models\FeesDue;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<?php

//$fees = FeesDue::find()
//    ->where(['school_id' => $model->school_id, 'approval_status' => true, 'recurrent' => false])
//    ->andWhere("effective_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "'")
//    ->orderBy('date_created')
//    ->all();


$fees = FeesDue::find()
    ->where(['school_id' => $model->school_id, 'approval_status' => true, 'recurrent' => false])
    ->andWhere("end_date >= '" . date("Y-m-d") . "'")
    ->orderBy('date_created')
    ->all();


$form = ActiveForm::begin([
    'action' => ['core-student/assign-fee', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess', 'id' => 'assign_fee_student_form_modal'],
    'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
]); ?>


<div class="row">
    <div class="col-md-12">
        <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Important: </b> A fee will only be available
            if
            it's <b>APPROVED</b> and its validity period has not passed
        </div>

        <div class="col-md-6">
            <?=
            $form->field($model2, 'fee')->dropDownList(
                ArrayHelper::map($fees, 'id', 'selectDesc'), ['prompt' => 'Select Fee', 'class' => 'form-control input-md',
                    //'onchange' => 'return checkCrditHr()',
                    'id' => 'feediv']

            )->label('Fees *') ?>
        </div>
        <div class="col-md-6" id='credithrsdiv'>
            <?= $form->field($model2, 'credit_hours', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Credit Hours', 'id' => 'credithrs'
            ],

            ])->textInput()->label('Credit Hours*') ?>
        </div>


    </div>
    <div class="col-md-12">
        <div class="col-md-6  alert alert-danger invalid-accounts-error" style="display: none">
            Please enter credit hours to continue
        </div>
    </div>


    <div class="col-md-12">
        <div class="col-md-6">
            <?= Html::submitButton('Assign Fee', ['class' => 'btn btn-block btn-info', 'data-confirm' => 'Are you sure you want Assign this fee']) ?>
        </div>
        <div class="col-md-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>


</div>
<?php ActiveForm::end(); ?>
<?php
$url = Url::to(['/feesdue/fees-due/credithr-fee']);

$script = <<< JS
    $(document).ready(function(){
       var isCrdit=false;
       // alert("ready");
          $('#credithrsdiv').hide();
        $('form#assign_fee_student_form_modal').yiiActiveForm('validate');
 
             $('#feediv').change(function(){
                 var feeId =$(this).val();
                  

             $.get('$url?id='+feeId, function( data ) {
                if(data == true ){
                    isCrdit = true;
                      $('#credithrsdiv').show();
                } else{
                      $('#credithrsdiv').hide();
                }
                
                });
        });
             
             
        let schoolForm = $('#assign_fee_student_form_modal');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.form-control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val() && isCrdit == true ) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });     
   
   });
JS;
$this->registerJs($script);
?>

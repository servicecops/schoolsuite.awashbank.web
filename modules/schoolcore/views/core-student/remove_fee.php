<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\FeesDue;
use yii\widgets\ActiveForm;

$fees = [''=>'Select Fee'];
$optionAttributes = []; //To hold extra attributes for the options
foreach($model->payable as $k=>$v){
    Yii::trace($v);
    $fees[$v['id']] = $v['desc']." - ".$v['fee'];

    $optionAttributes[$v['id']] = ['child_of_recurrent' => (int) $v['child_of_recurrent']];
}


?>

<?php $form = ActiveForm::begin([
    'action' => ['remove-fee', 'id'=>$model->id],
    'method' => 'post',
    'options'=>['class'=>'formprocess', 'id'=>'remove_fee_student_form_modal']
]); ?>

<div class="col-xs-12"><b style="color:#D82625; font-size: 17px;">Important: </b> A fee will be available for selection if it is applied to this student.</div>

<div class="col-xs-12">
    <?= $form->field($model2, 'fee', ['inputOptions'=>['id'=>'removeFeeDropDown', 'class'=>'form-control', 'placeholder'=> 'Select Fee'] ])->dropDownList($fees, ['options'=>$optionAttributes])->label('') ?>
</div>

<div class="col-xs-12">
    <?= $form->field($model2, 'adjust_bal', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Adjust Balance'] ])->dropDownList([''=>'Adjust Balance', '1'=>'True', '0'=>'False'])->label('') ?>
</div>

<div class="col-xs-12" id="showExempt">
    <?= $form->field($model2, 'exempt', ['inputOptions'=>['id' => 'exempt_checkbox']])->checkbox(['label' => 'Prevent fee from applying to the student in the next period'])->label(false) ?>
</div>

<div class="form-group col-xs-12 no-padding" style="margin-top:20px;">
    <div class="col-xs-6">
        <?= Html::submitButton('Remove Fee', ['class' => 'btn btn-block btn-info', 'data-confirm'=>'Are you sure you want to Remove this fee']) ?>
    </div>
    <div class="col-xs-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
</div>
</div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
        var showShowCheckbox = function() {
            let dropDown = $('#removeFeeDropDown');
            
            var child_of_recurrent = $('option:selected', dropDown).attr('child_of_recurrent');
              if(child_of_recurrent === '1') {
                  $('#showExempt').show();
              }else {
                  $('#showExempt').hide();
              }
        }
        
        $('form#remove_fee_student_form_modal').yiiActiveForm('validate');
          $("form#remove_fee_student_form_modal").on("afterValidate", function (event, messages) {
              if($(this).find('.has-error').length) {
                        return false;
                } else {
                    $('.modal').modal('hide');
                }
            });
          
          $('#removeFeeDropDown').on('change', function(event) {
              showShowCheckbox();
          })
          
          //Evaluate whether to show check box by default
          showShowCheckbox();
   });
JS;
$this->registerJs($script);
?>

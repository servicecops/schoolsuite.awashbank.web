<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Student';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">

    <div class="letter">
        <div class="row">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

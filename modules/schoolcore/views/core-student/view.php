<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

//use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = $model->fullname;


$schedule = $model->paymentSchedule;
$cancelledSchedule = $model->cancelledPaymentSchedule;
$agreements = $model->StudentAgreements;
$agreements['striped'] = false;
$schedule['striped'] = false;

//$studentCampus = null;
//if($model->campus_id) {
//  $studentCampus = SchoolCampuses::findOne(['school_id'=>$model->school_id, 'id'=>$model->campus_id]);
//}
?>
<p>STUDENT INFORMATION</p>
<div class="letter">
<div>


    <?php if (\Yii::$app->session->hasFlash('stuAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='stuAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('stuAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('viewError'); ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="row mt-5">


        <div class="pull-right">
            <?php if (\Yii::$app->user->can('send_mail'))  : ?>
            <?= Html::a('<i class="fa fa-envelope"></i> Send email', ['send-email', 'id' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
            <?php endif; ?>

            <?php if (!$model->archived) : ?>
                <?= Html::a('<i class="fa fa-upload"></i>Upload Photo', ['/schoolcore/core-student/profileimage-upload', 'id' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
            <?php endif; ?>
            <?php if (\Yii::$app->user->can('rw_student'))  : ?>
                <?= Html::a('<i class="fa fa-edit"></i>Create Student', ['/schoolcore/core-student/create', 'id' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
            <?php endif; ?>
            <?php if (\Yii::$app->user->can('r_student'))  : ?>
                <?= Html::a('<i class="fa fa-users"></i>All students', ['/schoolcore/core-student/index', 'id' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
            <?php endif; ?>

            <?php /* ?>
            <?= Html::a('<i class="fa fa-weight-hanging"></i>Grading', ['/schoolcore/core-student/student-report', 'studentNumber' => $model->id], ['class' => 'btn btn-md btn-primary']) ?>
            <?php */ ?>
            <?= Html::a('<i class="fa fa-print"></i> Print', ['print', 'id' => $model->id], ['class' => 'btn btn-md btn-primary', 'target' => '_blank']) ?>

            <?php /* ?>
            <?= Html::a('<i class="fa fa-qrcode"></i> QR Code', ['qr-code', 'id' => $model->id], ['class' => 'btn btn-md btn-primary', 'target' => '_blank']) ?>
            <?php */ ?>

            <?php if (Yii::$app->user->can('rw_student') ) : ?>
                <?= Html::a('<i class="fa fa-edit" style="color: #fff;background: #0b93d5"></i> <span >Edit Details</span>', ['update', 'id' => $model->id], ['class' => 'modal_link btn btn-md btn-info']) ?>
            <?php endif; ?>
            <?php if (\Yii::$app->user->can('rw_student')) : ?>


            <span class="btn btn-md btn-primary dropdown-toggle" data-toggle="dropdown">More &nbsp;<i
                        class="fa  fa-caret-square-o-down "></i></span>

            <ul class="col-md-2 dropdown-menu dropdown-menu-right">

                <li class="clink"><a href="<?= Url::to(['/schoolcore/core-student/assign-fee', 'id' => $model->id]) ?>">Assign
                        A Fee</a></li>
                <?php if ($model->schoolId->hasModule('SCHOOLPAY_PAYMENT_PLANS')) : ?>
                    <li>
                    <li class="clink"><a
                                href="<?= Url::to(['/plan/default/assign-student-plan', 'id' => $model->id]) ?>">Assign
                            Payment Plan</a></li>
                    <li>
                    <li class="clink"><a
                                href="<?= Url::to(['/plan/default/cancel-student-plan', 'id' => $model->id]) ?>">Cancel
                            Payment Plan</a></li>

                <?php endif; ?>

                <?php if ($model->payable) : ?>

                    <li class="clink"><a
                                href="<?= Url::to(['/schoolcore/core-student/remove-fee', 'id' => $model->id]) ?>">Remove
                            Fee</a></li>

                <?php endif; ?>
                <?php if ($model->archived) : ?>


                    <li class="clink"><a
                                href="<?= Url::to(['/schoolcore/core-student/unarchive-student', 'id' => $model->id]) ?>">Un-archive
                            Student</a></li>

                <?php endif; ?>
                <?php if (!$model->archived) : ?>

                    <li class="clink"><a href="<?= Url::to(['core-student/archive-student', 'id' => $model->id]) ?>">Archive
                            Student</a></li>

                    <?php if (\Yii::$app->user->can('adjust_student_bal')) : ?>

                        <li class="clink"><a href="<?= Url::to(['core-student/adjust-bal', 'id' => $model->id]) ?>">Adjust
                                Balance</a></li>

                    <?php endif ?>
                <?php endif ?>

                <li><?= Html::a('Delete Student', ['core-student/delete', 'id' => $model->student_code], ['style' => 'color:#CD0C0C;', 'data-confirm' => 'Are you sure you want to delete this Student?', 'data-method' => 'post']) ?></li>
            </ul>
        </div>


        <?php endif; ?>

    </div>
    <hr class="l_header mt-3">

</div>

<!--forms-->

    <div class="student-profile py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card border-primary">
                        <div class="card-header bg-transparent text-center">

                            <?php if ($model->passport_photo): ?>

                                <?= '<img src="'.Url::to(['/import/import/image-link', 'id'=>$model->passport_photo]).'" width="130px" class="profile_img" /> ' ?>

                            <?php else : ?>
                                <?= Html::img('@web/web/img/user.png', ['class' => 'profile_img', 'alt' => 'profile photo']) ?>

                            <?php endif; ?>
                            <h3><?= !$model->archived ? $model->fullname : '<span style="color:red">' . $model->fullname . '</span>'; ?></h3>
                        </div>
                        <div class="card-body">
                            <p class="mb-0"><strong class="pr-1">Student Code:</strong> <?= ($model->student_code) ? $model->student_code : ' --' ?></p>
                            <?php if (!$model->archived) : ?>
                                <p class="mb-0"><strong class="pr-1">Class:</strong> <?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?></p>

                            <?php elseif ($model->class_when_archived) : ?>

                                <p class="mb-0"><strong class="pr-1">Last Class /Course (before
                                        Archiving):</strong> <?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --' ?></p>
                            <?php endif; ?>

                            <p class="mb-0"><strong class="pr-1">Reg Number:</strong><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?></p>
                            <p class="mb-0"><strong class="pr-1">Date of Birth:</strong>
                                <?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">School:</strong>
                                <?= ($model->school_id) ? $model->schoolName->school_name : ' --' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">Gender:</strong>
                                <?= ($model->gender == 'M') ? 'Male' : 'Female' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">Email:</strong>
                                <?= ($model->student_email) ? $model->student_email : ' --' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">Nationality:</strong>
                                <?= (isset($model->nationality)) ? $model->nationality : ' --' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">Day/Boarding:</strong>
                                <?= (isset($model->day_boarding)) ? $model->day_boarding : ' --' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">Contact Phone:</strong>
                                <?= ($model->student_phone) ? $model->student_phone : ' --' ?>
                            </p>
                            <p class="mb-0"><strong class="pr-1">Allow Part Payments:</strong>
                                <?= ($model->allow_part_payments) ? "Yes" : "No" ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="card border-primary">
                        <div class="card-header bg-transparent border-0">
                            <h3 class="mb-0"><i class="fa fa-money pr-1"></i>Payment summary</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%">Total Active Fees</th>
                                    <td width="2%">:</td>
                                    <td><?= ($model->payableSum) ? number_format($model->payableSum, 2) : "  --"; ?></td>
                                </tr>
                                <?php if ($model->studentAccount->account_balance ) : ?>
                                    <tr>
                                        <th width="30%">Student Account Balance	</th>
                                        <td width="2%">:</td>
                                        <td>
                                            <?= ($model->studentAccount->account_balance < 0) ? '<span style="color:red">' . number_format(abs($model->studentAccount->account_balance), 2) . '</span>' : '<span style="color:lightseagreen">' . number_format(abs($model->studentAccount->account_balance), 2) . '</span>' ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
<!--                                 <!--  <tr>
                                    <th width="30%">Outstanding Balance</th>
                                    <td width="2%">:</td>
                                    <td>< ($model->studentAccount->outstanding_balance < 0) ? '<span style="color:red">' . number_format($model->studentAccount->outstanding_balance, 2) . '</span>' : number_format($model->studentAccount->outstanding_balance, 2) ?></td>
                                </tr>
                               <tr>
                                      <th width="30%">Religion</th>
                                      <td width="2%">:</td>
                                      <td>Group</td>
                                  </tr>
                                  <tr>
                                      <th width="30%">blood</th>
                                      <td width="2%">:</td>
                                      <td>B+</td>
                                  </tr>-->
                            </table>
                        </div>
                    </div>
                    <div style="height: 26px"></div>
                    <div class="card border-primary">
                        <div class="card-header bg-transparent border-0">
                            <h3 class="mb-0"><i class="far fa-clone pr-1"></i>Guardian Information</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table class="table table-bordered">
                                <tr>
                                    <th width="30%">Name</th>
                                    <td width="2%">:</td>
                                    <td>
                                        <?= ($model->guardian_name) ? $model->guardian_name : "--" ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="30%">Relationship	</th>
                                    <td width="2%">:</td>
                                    <td>
                                        <?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th width="30%">Phone contact</th>
                                    <td width="2%">:</td>
                                    <td>
                                        <?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th width="30%">Email</th>
                                    <td width="2%">:</td>
                                    <td>
                                        <?= ($model->guardian_email) ? $model->guardian_email : "--" ?>
                                    </td>
                                </tr>
                                <!-- <tr>
                                     <th width="30%">blood</th>
                                     <td width="2%">:</td>
                                     <td>B+</td>
                                 </tr>-->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <hr class="l_header mt-3">
<!--nav-->

    <div class="container mt-3">
        <div class="row">
            <div class="col-sm">

            </div>

            <div class="col-sm">

                <div class="float-right">
                    <?php
                    echo Html::a('<i class="fa fa-file-pdf"></i> Download Pdf', ['export-pdf-trans', 'model' => get_class($searchModel)], [
                        'class' => 'btn btn-md btn-danger',
                        'target' => '_blank',
                        'data-toggle' => 'tooltip',
                        'title' => 'Will open the generated PDF file in a new window'
                    ]);
                    echo Html::a('<i class="fa fa-file-excel"></i> Download Excel', ['export-data/export-excel-old', 'model' => get_class($searchModel)], [
                        'class' => 'btn btn-md btn-success',
                        'target' => '_blank'
                    ]);


                    ?>
                </div>
            </div>
        </div>
    </div>

    <main class="tab-main">

        <input id="tab1" type="radio" name="tabs" checked>
        <label for="tab1">Payments History</label>


        <input id="tab4" type="radio" name="tabs">
        <label for="tab4">Active Fees</label>

        <input id="tab5" type="radio" name="tabs">
        <label for="tab5">Exempted Fees</label>
        <li class="pull-right">
            <?php $form = ActiveForm::begin([
                'action' => ['view', 'id' => $model->id],
                'method' => 'get',
                'options' => ['class' => 'formprocess', 'id' => 'student_payments'],
            ]); ?>
            <?= $form->field($searchModel, 'only_received', ['inputOptions' => ['class' => 'form-control input-sm', 'style' => 'border:none; box-shadow:none; color:#3c8dbc;', "onchange" => "$(form).submit()"]])->dropDownList(['1' => 'Only Payments', '0' => 'All Transactions'])->label(false) ?>
            <?php ActiveForm::end(); ?>

        </li>

        <input id="tab3" type="radio" name="tabs">
        <label for="tab3">Supplementary Payment History</label>

        <section id="content1">
            <p>
                <?= $this->render('_tab_transactions', ['model'=>$model,'dataProvider' => $dataProvider,
                    'searchModel'=> $searchModel]) ?>
            </p>

        </section>


        <section id="content4">
            <p>
                <?= $this->render('_tab_fees_due', ['payable'=>$model->payable]) ?>
            </p>

        </section>

        <section id="content5">
            <p>
                <?= $this->render('_tab_exemptions', ['exempts'=>$model->exempts]) ?>
            </p>

        </section>


        <section id="content3">
            <p>
                <?= $this->render('_tab_supplementary_transactions', ['model'=>$model,'dataProvider' => $supplementaryFee,
                    'searchModel'=> $searchModel]) ?>            </p>

        </section>

    </main>
</div>




<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>





<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \app\models\SignupForm */

use kartik\file\FileInput;

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Update profile picture';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>


    <div class="row">
        <div class="col">

        </div>
        <div class="col">

                <?= Html::img('@web/img/PNG1.png', ['class' => 'sch-icon', 'alt' => 'profile photo', ]) ?>

          <br>
            <span class=""> <?= Html::encode($this->title) ?></span><br>

            <div class="row">

<div><i class="fa fa-phone"></i>704567890333</div>



                    <div><i class="fa fa-globe"></i>www.site.com</div>





                    <div><i class="fa fa-envelope"></i>7info@school.support</div>
                </div>
            </div>


        <div class="col">

        </div>
    </div>
<div class="row mt-5">
    <div class="col">

    </div>
    <div class="col">
<div><h4>STUDENT REPORT CARD</h4></div>
    </div>
    <div class="col">

    </div>
</div>
<hr>
    <div class="row ">
        <div class="col">
<b class="-underline">Student Name: </b><br><hr class="mt-0">
            <b class="-underline">Student Reg No: </b><br><hr class="mt-0">
            <b class="-underline">Student Class: </b><br><hr class="mt-0">
            <b class="-underline">Guardian/Parent's name: </b><hr class="mt-0">

        </div>
        <div class="col">

        </div>
        <div class="col">
            <b class="-underline">Year: </b><br><hr class="mt-0">
            <b class="-underline">Term: </b><br><hr class="mt-0">
            <b class="-underline">Gender: </b><br><hr class="mt-0">
            <b class="-underline">Term: </b><hr class="mt-0">

        </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>


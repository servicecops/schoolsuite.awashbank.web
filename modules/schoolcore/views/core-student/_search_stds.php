<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
?>


<div class="student-search">
    <div class="card-body">

    <?php $form = ActiveForm::begin([
        'action' => ['search'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>

        <div class="row">
            <?php //if(\Yii::$app->user->can('schoolpay_admin')) : ?>
            <div class="col-md-3 no-padding">
                <?php
                $url = Url::to(['core-school/active-schoollist']);
                $selectedSchool = empty($studentModel->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                echo $form->field($studentModel, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'size' => 'sm',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'class' => 'form-control  input-sm inputRequired',
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'school_search',
                        'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#corestudentsearch-class_id" ).html(data);
                        });'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false); ?>
            </div>
            <div class="col-md-3 no-padding">
                <?= $form->field($studentModel, 'class_id', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label(false) ?>
            </div>

            <div class="col-md-2 no-padding"><?= $form->field($studentModel, 'school_student_registration_number', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Ente Reg No.'])->label(false) ?></div>

            <div class="col-md-3 no-padding"><?= $form->field($studentModel, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeholder'=>'Enter Student Name /Payment Code'])->label(false) ?></div>
            <div class="col-md-1 no-padding"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-md']) ?></div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
        </div>

<?php
$url = Url::to(['classes/lists']);
$cls = $studentModel->class_id;



$script = <<< JS
$("document").ready(function(){ 
    var sch = $("#student_selected_school_id").val();
    if(sch){
        $.get('$url', {id : sch}, function( data ) {
            $('select#studentsearch-student_class').html(data);
            $('#studentsearch-student_class').val('$cls');
        });
        

    }

    $('body').on('change', '#student_selected_school_id', function(){

        $.get('$url', {id : $(this).val()}, function( data ) {
            $('select#studentsearch-student_class').html(data);
        });
        

    });

  });
JS;
$this->registerJs($script);
?>

<?php

use app\models\SchoolInformation;
use kartik\select2\Select2;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClassesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-search">

    <?php $form = ActiveForm::begin([
        'action' => Url::to(['core-student/uploaded-files']),
        'method' => 'post',
    ]); ?>

    <div class="row">
        <div class="col-md-8 no-padding">
            <?php
            $url = Url::to(['core-school/schoollist']);
            $selectedSchool = empty($searchModel->schsearch) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($searchModel->schsearch)->school_name;
            echo $form->field($searchModel, 'schsearch')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size' => 'sm',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id' => 'student_selected_school_id',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>
        </div>

        <div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

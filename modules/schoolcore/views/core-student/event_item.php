<?php

use app\modules\schoolcore\models\EventsSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $model app\modules\schoolcore\models\Events */

?>
<aside class="note-wrap note-green">
    <a href="<?=\yii\helpers\Url::to(['/schoolcore/events/view', 'id'=>$model->id])?>">
        <h2 class="event-title"><?= Html::encode($model->title) ?></h2>
    </a>

    <h4>

        <?= \yii\helpers\StringHelper::truncateWords($model->getEncodedDescription(), '10')?>
    </h4>
    <hr>
    <h5 class="text-muted">
                <i> <small>Created On  <b><?=Yii::$app->formatter->asDatetime($model->created_at)?></b> </small></i>
            </h5>
</aside>

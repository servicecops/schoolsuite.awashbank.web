<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */


$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";

use kartik\grid\GridView;
use yii\helpers\Html;

?>

<?php /** @var TYPE_NAME $output */
foreach ($output as $res) {
    ?>

    <div style="width:100%;background-color: #e6ecff;padding:20px">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h3><?php echo $res['schoolDetails']['school_name'] ?></h3>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Location: <?php echo $res['schoolDetails']['region_name'] ?><br>
                District: <?php echo $res['schoolDetails']['district_name'] ?><br>
                Address: <?php echo $res['schoolDetails']['village'] ?><br>
            </div>
            <div class="col-md-4">
                <?php if ($res['schoolDetails']['school_logo']): ?>
                    <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->logo->image_base64 ?>"
                         height="100" width="100" style="margin-left: 25px;"/>
                <?php else : ?>
                    <div style="margin-left: 25px">
                        <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                Tel:<?php echo $res['schoolDetails']['phone_contact_1'] ?><br>
                MOBILE: <?php echo $res['schoolDetails']['phone_contact_2'] ?><br>
                EMAIL:<?php echo $res['schoolDetails']['contact_email_1'] ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr class="l_header">
                <table style="margin: 0px auto;width: 753px;background: #e6ecff;">
                    <tr>
                        <td>
                            <span style="font-weight: 600;font-size: 17px;">Student Name: </span>
                            <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; "><?php echo $res['studentInfo']['first_name'] . ' ' . $res['studentInfo']['middle_name'] . ' ' . $res['studentInfo']['last_name'] ?></span>
                        </td>
                        <td>
                            <span style="font-weight: 600;font-size: 17px;">Student Code: </span>
                            <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; "><?php echo $res['studentInfo']['student_code'] ?></span>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <span style="font-weight: 600;font-size: 17px;">Class Name: </span>
                            <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; ">
                           <?php echo $res['studentInfo']['class_description'] ?>
                        </span>
                        </td>
                        <td>
                            <span style="font-weight: 600;font-size: 17px;">Term: </span>
                            <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; ">
                            <?php echo "....."; ?>
                        </span>
                        </td>
                    </tr>
                </table>
                <hr class="l_header">
            </div>

        </div>

        <div style="width:150px"></div>
        <div class="row">

            <table class="table table-striped table-bordered detail-view">
                <thead>
                <tr>
                    <th>Subject Code</th>
                    <th>Subject Name</th>
                    <th>Begin</th>
                    <th>Mid</th>
                    <th>End</th>
                    <th>Average</th>
                    <th>Grade</th>
                    <th>Comment</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($res['results'] as $data) {
                    $subjects = $data["subject_code"];

                    if (isset($subjects['Begin'])) {
                        $begin = $subjects['Begin'];
                    } else {
                        $begin = '--';
                    }

                    if (isset($subjects['Mid'])) {
                        $mid = $subjects['Mid'];
                    } else {
                        $mid = '--';
                    }


                    if (isset($subjects['Exam'])) {
                        $end = $subjects['Exam'];
                    } else {
                        $end = '--';
                    }

                    if (isset($data['subjectName'])) {
                        $subject = $subjects['Begin'];
                    } else {
                        $subject = '--';
                    }


                    ?>

                    <tr>
                        <td> <?php echo $data['subject_code'] ?> </td>
                        <td> <?php echo $data['subject_name'] ?> </td>
                        <?php
                        $sum_marks = 0;
                        foreach ($data["results"] as $marks):
                            $sum_marks += $marks['score_avg'];
                            ?>
                            <td><?= $marks['score_avg']; ?></td>
                        <?php endforeach; ?>
                        <td><?php echo round($sum_marks / 3); ?></td>
                        <td> <?php echo $res['model']->getGrades(round($sum_marks / 3), $data['subject_id']) ?></td>
                        <td> <?php echo "--" ?></td>
                    </tr>
                    <?php
                }
                ?>


                </tbody>
            </table>

        </div>

        <div class="row">
            <div class="col-md-6" style="padding:10px;border:1px solid #110508;margin-top:10px">
                <?php foreach ($res['grading'] as $the_grade) {
                    ?>
                    <div class="col-md-1">
                        <?php echo $the_grade['grades'] . ': ' ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $the_grade['min_mark'] . ' - ' . $the_grade['max_mark'] ?>
                    </div>

                <?php } ?>
            </div>
            <div class="col-md-6"></div>
        </div>
        <div style="clear:both"></div>

        <div class="row">
            <br><br>
            <h5>Powered By</h5>
        </div>
        <div class="row">
            <?= Html::img('@web/web/img/no_bg_logo.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '65px']); ?>
        </div>
        <div class="row"
             style="height:50px; margin-top:70px;background-image: linear-gradient(180deg,#2B3990 10%,#0068AD 100%);">
            <div style="display: table;color:white;margin: auto;"><i>All rights reserved</i></div>
        </div>
    </div>
    <br>
<?php }

?>


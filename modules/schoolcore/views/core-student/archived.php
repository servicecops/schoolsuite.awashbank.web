<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Archived Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">Student Information</p>

<div class="student-index hd-title" data-title="Archived Students">

        <div class="row" style="margin-top:20px">

            <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            <div class="col-md-8"><?php echo $this->render('_arc_search', ['model' => $searchModel]); ?></div>
        </div>


        <div class="row mt-3">



            <div class="pull-right">
                <?php
                echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                    'class'=>'btn btn-sm btn-danger',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class'=>'btn btn-sm btn-success',
                    'target'=>'_blank'
                ]);
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box-body table table-responsive no-padding">
                    <table class="table table-striped">
                        <thead>
                        <tr><th class='clink'><?= $sort->link('student_code') ?></th><th class='clink'><?= $sort->link('first_name') ?></th><th class='clink'><?= $sort->link('last_name') ?></th><?php if(Yii::$app->user->can('rw_sch')){ echo "<th class='clink'>".$sort->link('school_name')."</th>"; } ?> <th class='clink'><?= $sort->link('class_code', ['label'=>'Last Class']) ?></th><th class='clink'><?= $sort->link('student_email') ?></th><th class='clink'><?= $sort->link('student_phone') ?></th><th class='clink'>&nbsp;</th></tr>
                        </thead>
                        <tbody>
                        <?php
                        if($dataProvider) :
                            foreach($dataProvider as $k=>$v) : ?>
                                <tr data-key="0">
                                    <td class="clink"><?= ($v['student_code']) ? '<a href="'.Url::to(['core-student/view', 'id'=>$v['id']]).'">'.$v['student_code'].'</a>' : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <?php if(Yii::$app->user->can('rw_sch')) : ?>
                                        <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <?php endif; ?>
                                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['student_email']) ? $v['student_email'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set">(not set) </span>'?></td>
                                    <td><a href="javascript:;" onclick="clink('<?= Url::to(['core-student/view', 'id'=>$v['id']]) ?>')" title="View" data-pjax="0"><span class="fa fa-search"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="javascript:;" onclick="clink('<?= Url::to(['core-student/update', 'id'=>$v['id']]) ?>')" title="Update" data-pjax="0"><span class="glyphicon glyphicon-edit"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach;
                        else :?>
                            <td colspan="8">No student found</td>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <?= LinkPager::widget([
                    'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                    'lastPageLabel'  => 'Last'
                ]); ?>

            </div>
        </div>

</div>



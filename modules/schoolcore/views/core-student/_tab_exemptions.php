<?php
echo "<table class ='table table-striped'>";
echo "<thead>";
echo "<tr>";
echo "<th>Fee </th>";
echo "<th>Amount </th>";
echo "</tr>";
echo "</thead>";
echo "<tbody>";
if($exempts){
    foreach($exempts as $k=>$v)
    {
        echo "<tr>";
        echo "<td >".$v['desc']."</td>";
        echo "<td >".number_format($v['fee'], 2)."</td>";

        echo "</tr>";

    }
} else {
    echo "<tr> <td>No Fees Exempted</td><td></td></tr>";
}
echo "</tbody>";

echo "</table>";
?>
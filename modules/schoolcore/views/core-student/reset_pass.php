<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
$this->title = "Change Password for";

?>




<div class="container">
    <div>
        <h3 class="box-title text-center" style="color: #000"><b><i class="fa  fa-user-lock"></i> <?= Html::encode($this->title).' - '.$model->fullname; ?></b></h3>
        <hr class="style14">
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'change-password-form',
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
    ]); ?>
    <div class="row">
        <div class="col-sm">

        </div>
        <div class="col-sm">
            <div class="card mt-5">
                <div class="card-body">
                    <?= $form->field($model, 'new_pass',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['maxlength' => 60, 'placeholder' => 'New Password'])->label('New Password') ?>
                    <?= $form->field($model, 'password2',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['maxlength' => 60, 'placeholder' => 'Confirm Password'])->label('Confirm Passord') ?>
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' =>'btn btn-primary']) ?>
                    <?= Html::a('Cancel', ['/site/index'], ['class' => 'btn btn-default']) ?>

                </div>
            </div>
        </div>
        <div class="col-sm">

        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
<?php 
$script = <<< JS
$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#user").addClass('active');
  });
JS;
$this->registerJs($script);
?>

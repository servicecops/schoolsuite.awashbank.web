<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\District;
use app\modules\schoolcore\models\SchoolCampuses;
use yii\jui\DatePicker;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<?php
$value = 'abc123'
?>
<div class="formz">

    <p>All fields marked with * are required</p>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div style="padding: 10px;width:100%"></div>
    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;<strong>Student Basic
                    Information</strong></h3></div>
    </div>
    <div class="row">
        <?php if (\app\components\ToWords::isSchoolUser()) : ?>

            <?php if (Yii::$app->user->identity->school->hasModule('SCHOOL_CAMPUSES')) : ?>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <?php
                    $data = SchoolCampuses::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                    echo $form->field($model, 'campus_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($data, 'id', 'campus_name'),
                        'language' => 'en',
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Find Campus'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label('Campus *'); ?>
                </div>
            <?php endif; ?>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php
                $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'class_code'),
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['placeholder' => 'Find Class'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Class *'); ?>
            </div>

        <?php endif; ?>
        <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>

            <div class="col-sm-4 ">
                <?php
                $url = Url::to(['core-school/active-schoollist']);
                $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'school_search',
                        'class' => 'form-control',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                    ],
                ])->label('School Name *'); ?>

            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
                    ['prompt' => 'Filter class'])->label('Class *') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'campus_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_campus_drop']])->dropDownList(
                    ['0' => 'Campus'])->label('Campus') ?>
            </div>

        <?php endif; ?>
    </div>
    <div class="row">

        <div class="col-sm-4">
            <?= $form->field($model, 'first_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'First Name']])->textInput()->label('First Name *') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'middle_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Middle Name']])->textInput()->label('Middle Name') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Last Name']])->textInput()->label('Last Name *') ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'date_of_birth')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'date_of_birth',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1980:' . (date('Y')),
                        'autoSize' => true,
                        'dateFormat' => 'yyyy-MM-dd',
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('date_of_birth') ? ' is-invalid' : ''),
                        'placeholder' => 'Date of Birth'
                    ],])->label('Date of birth *') ?>
        </div>
        <div class="col-md-4 ">
            <?= $form->field($model, 'gender')->dropDownList(['M' => 'Male', 'F' => 'Female'], ['prompt' => 'Gender'])->label('Gender *') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'student_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Student Phone']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'student_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'nin_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'National ID NIN']])->textInput()->label('ID Number ') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'school_student_registration_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Registration Number','disabled'=>!$model->isNewRecord]])->textInput()->label('Student Registration number *') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'orphan_type')->dropDownList([
                'Only Mother Dead' => 'Only Mother Dead',
                'Only Father Dead ' => 'Only Father Dead',
                'Both Parents Dead' => 'Both Parents Dead',


            ],
                ['id' => 'orphan_type', 'prompt' => 'Orphan Type', ]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'year_of_joining')->dropDownList($model->getYearsList(), ['prompt' => 'Year of Joining ']) ?>

        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'disability_nature')->dropDownList([
                'Visual Impairment' => 'Visual Impairment',
                'Hearing Impairment' => 'Hearing Impairment',
                'Self-Care' => 'Self-Care',
                'Remembering/Concentration difficulty' => 'Remembering/Concentration difficulty',
                'Communication' => 'Communication',


            ],
                ['id' => 'disability_nature', 'prompt' => 'Disability Nature']) ?>
        </div>
    </div>


    <div class="row">
        <br>
        <div class="col-sm-4">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreStudent[active]" value="0">
                <input type="checkbox" id="CoreStudent-active" name="CoreStudent[active]"
                       value="1" <?= ($model->active) ? 'checked' : '' ?> >
                <label for="CoreStudent-active">Active</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreStudent[disability]" value="0"><input
                        type="checkbox" id="CoreStudent-disability"
                        name="CoreStudent[disability]"
                        value="1" <?= ($model->disability) ? 'checked' : '' ?> >
                <label for="CoreStudent-disability">Disability</label>


            </div>

        </div>
        <div class="col-sm-4">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="CoreStudent[allow_part_payments]" value="0">
                <input type="checkbox" id="CoreStudent-allow_part_payments"
                       name="CoreStudent[allow_part_payments]" value="1"
                       data-checked="negative" <?= ($model->allow_part_payments) ? 'checked' : '' ?>>
                <label for="CoreStudent-enable_daily_stats">Allow Part Payments</label>
            </div>
        </div>

    </div>
    <div class="row">
        <br>
        <?php if (!$model->isNewRecord && Yii::$app->user->can('schoolsuite_admin')): ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'locked', ['labelOptions' => ['style' => 'color:#041f4a']])->checkbox() ?>

            </div>
        <?php endif; ?>
        <div class="col-sm-4">

        </div>
        <div class="col-sm-4">

        </div>

    </div>
    <hr class="l_header mt-3">
    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;<strong>Address
                    Information</strong></h3></div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\RefRegion::find()->all(), 'id', 'description');
            echo $form->field($model, 'region')
                ->dropDownList(
                    $items,           // Flat array ('id'=>'label')
                    ['prompt' => 'Select region',
                        'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });']    // options
                )->label('Region *'); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'district')->dropDownList(['prompt' => 'Select district'
                ]
                , ['id' => 'district',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])->label('District *')
            ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Physical Address']])->textInput() ?>
        </div>
    </div>


    <hr class="l_header mt-3">
    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Guardian
                    Information</strong></h3></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_relation', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone']])->textInput() ?>
        </div>

    </div>
    <hr class="l_header mt-3">
    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>Second Guardian
                    Information</strong></h3></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian2_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian2_relation', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian2_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian2_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone']])->textInput() ?>
        </div>

    </div>

    <div class="row">
        <br>
        <hr class="l_header" style="margin-top:15px;">

    </div>


    <div class="row">
        <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save <i class="fa fa-save"></i>' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::resetButton('Reset <i class="fa fa-save"></i>', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

<!--    --><?php
//    $url = Url::to(['core-school-class/lists']);
//    $cls = $model->class_id;
//
//    $campusurl = Url::to(['/schoolcore/core-school/campuslists']);
//
//    $script = <<< JS
//var schoolChanged = function() {
//        var sch = $("#school_search").val();
//        $.get('$url', {id : sch}, function( data ) {
//                    $('select#student_class_drop').html(data);
//                    $('#student_class_drop').val('$cls');
//                });
//
//    }
//
//$("document").ready(function(){
//
//    if($("#school_search").val()){
//        schoolChanged();
//    }
//
//    $('body').on('change', '#school_search', function(){
//         schoolChanged();
//    });
//  });
//JS;
//    $this->registerJs($script);
//    ?>

    <?php
    $url = Url::to(['/schoolcore/core-school-class/lists']);
    $cls = $model->class_id;

    $campusurl = Url::to(['/schoolcore/core-school/campuslists']);
    $campus = $model->campus_id;
    $script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
        
        $.get('$campusurl', {id : sch}, function( data ) {
                    $('select#student_campus_drop').html(data);
                    $('#student_campus_drop').val('$campus');
                });
    }
    
$("document").ready(function(){
    
    if($('input#CoreStudent-disability').attr('checked')== 'positive'){
        
        $('input#CoreStudent-disability_nature').css('display', 'block');
        $(this).attr('data-checked', 'positive');
    }

    $('body').on('change', 'input#disabiltity', function(){
        if($(this).attr('data-checked') == 'negative'){
            $('input#student-disability_nature').css('display', 'block');
            $(this).attr('data-checked', 'positive');
        }else{
            $('input#student-disability_nature').val('').css('display', 'none');
            $(this).attr('data-checked', 'negative');
        }
    });

    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
    $this->registerJs($script);
    ?>


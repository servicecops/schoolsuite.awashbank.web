<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;


Yii::trace($model);
?>

<div style="color:#F7F7F7">Student Information</div>
<?php $form = ActiveForm::begin([
    'action' => ['std-list-wt-passwords'],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>


<div class="row">
    <?php if (\app\components\ToWords::isSchoolUser()) : ?>

        <div class="col-md-4 ">
            <?php
            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id,'active'=>true])->all();

            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Class'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false); ?>
        </div>

    <?php endif; ?>

    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
        <div class="col-md-6">
            <div class="col-sm-6 ">
                <?php
                $url = Url::to(['core-school/active-schoollist']);
                $selectedSchool = empty($model->schsearch) ? '' : CoreSchool::findOne($model->schsearch)->school_name;
                echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'school_search',
                        'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#corestudentsearch-class_id" ).html(data);
                        });'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false); ?>

            </div>

            <div class="col-sm-6">
                <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label(false) ?>
            </div>

        </div>
    <?php endif; ?>


    <div class="col-md-2">
        <?= $form->field($model, 'school_student_registration_number', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter Reg No.'])->label(false) ?>

    </div>
    <div class="col-md-2">
        <?= $form->field($model, 'modelSearch', ['inputOptions' => ['class' => 'form-control'],])->textInput(['title' => 'Enter student name or payment code',

            'data-toggle' => 'tooltip',

            'data-trigger' => 'hover',

            'data-placement' => 'bottom'])->label(false) ?>

    </div>
    <div class="col-md-2">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>



<?php
$url = Url::to(['core_school_class/lists']);
$cls = $model->class_id;


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

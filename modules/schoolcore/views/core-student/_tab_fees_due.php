
<?php
echo "<table class ='table table-striped'>";
echo "<thead>";
echo "<tr>";
echo "<th>Active </th>";
echo "<th>Fee </th>";
echo "<th>Fee Amount </th>";
echo "<th>Paid So Far </th>";
echo "<th>Fee Balance </th>";
echo "<th>Penalty </th>";
echo "<th>Total Outstanding </th>";
echo "</tr>";
echo "</thead>";
if($payable){
    foreach($payable as $k=>$v)
    {
        $total_outstanding = $v['fee_outstanding_balance'] + $v['penalties_outstanding_balance'];
        $class = $total_outstanding < 0 ? 'style="color:red"' : '';
        $active = !$v['applied'] ? 'Starting ' . $v['effective_date'] : 'Active';
        $rowStyle = !$v['applied'] ? ' style="color:#ccc" ' : '';
        echo "<tr $rowStyle>";
        echo "<td>".$active."</td>";
        echo "<td>".$v['desc']."</td>";
        echo "<td>".number_format($v['fee'], 2)."</td>";
        echo "<td>".number_format($v['fee'] + $v['fee_outstanding_balance'], 2)."</td>";
        echo "<td $class>".number_format($v['fee_outstanding_balance'], 2)."</td>";
        echo "<td $class>".number_format($v['penalties_outstanding_balance'], 2)."</td>";
        echo "<td $class>".number_format($total_outstanding, 2)."</td>";
        echo "</tr>";

    }
} else {
    echo "<tr> <td colspan='7'>No Fees Found for this period. Probably it's past all deadline dates for making payments OR still in HOLIDAY</td></tr>";
}

echo "</table>";
?>

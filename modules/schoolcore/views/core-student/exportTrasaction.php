<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="payment_aggregators_index">
    <?php  
	$model->sort = false; 

	if($type == 'Excel') {
		echo "<table><tr> <th colspan='7'><h3> Students Datasheet</h3> </th> </tr> </table>";
	}
    ?>
    <?= GridView::widget([
        'dataProvider' => $model,
		'layout' => '{items}',
        'columns' => [
            [
            'header'=>'Sr.No',
            'class' => 'yii\grid\SerialColumn'],
            [
            'attribute'=>'date_created',
              'value'=>function($model){
                return date('d/m/y H:i', strtotime($model->date_created));
              },

            ],
            [
              'attribute'=>'description',
              'value'=>'description',
            ],
            [
              'attribute'=>'trans_type',
              'value'=>'trans_type',
            ],
            [
              'attribute'=>'payment_id',
              'options'=>['class'=>'col-xs-1'],
              'value'=>function($model){
                if($model->payment_id){
                  return $model->payment->paymentChannel->channel_code;
                }
              },
            ],
            [
              'attribute'=>'Channel Memo',
              'value'=>function($model){
              if($model->payment_id)
                return $model->payment->channel_memo;
              }
            ],
            [
              'attribute'=>'amount',
              'value'=>function($model){
                if($model->amount < 0)
                    return '<span style="color:red">'.number_format($model->amount, 2).'</span>';
                else
                    return number_format($model->amount, 2);
              },
              'format'=>'html',
            ],
            [
              'attribute'=>'balance_after',
              'value'=>function($model){
                if($model->amount < 0)
                    return '<span style="color:red">'.number_format($model->balance_after, 2).'</span>';
                else
                    return number_format($model->balance_after, 2);
              },
              'format'=>'html',
            ],
            ],
    ]); ?>

</div>
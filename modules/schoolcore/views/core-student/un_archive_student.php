<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'action' => ['core-student/unarchive-student', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess', 'id' => 'unarchive_student_form_modal']
]); ?>


<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model2, 'reason', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Give Reason ']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-6">
        <?= Html::submitButton('Un-Archive', ['class' => 'btn btn-block btn-info', 'data-confirm' => 'Are you sure you want un-archive this student']) ?>
    </div>
    <div class="col-sm-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
        $('form#unarchive_student_form_modal').yiiActiveForm('validate');
          $("form#unarchive_student_form_modal").on("afterValidate", function (event, messages) {
              // if($(this).find('.has-error').length) {
              //           return false;
              //   } else {
              //       $('.modal').modal('hide');
              //   }
            });
   });
JS;
$this->registerJs($script);
?>

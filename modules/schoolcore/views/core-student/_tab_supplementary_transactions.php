<?php

use app\components\ToWords;
use app\modules\paymentscore\models\PaymentChannels;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\bootstrap4\LinkPager;


?>

<?php Yii::trace($dataProvider);?>

<div class="row">

    <div class="col-md-12" style="color:#fff">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class='clink'>Date</th>
                <th class='clink'>Details</th>
                <th class='clink'>Description</th>
                <th class='clink'>Channel Id</th>
                <th class='clink'>Receipt</th>
                <th class='clink'>Channel Code</th>
                <th class='clink'>Channel Memo</th>
                <th class='clink'>Amount</th>


            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>

                    <tr data-key="0" <?= $v['reversal'] ? 'style="text-decoration: line-through"' : ''; ?>>
                        <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['reversal']) ? 'REVERSED':'PAYMENT RECEIVED' ?></td>

                        <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['reciept_number']) ? $v['reciept_number'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_code']) ? '<img src="' . Url::to(['/import/import/image-link2', 'id' => $v['payment_channel_logo']]) . '" width="30px" /> ' . $v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['channel_memo']) ? $v['channel_memo'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['id']) ? Html::a('<span class="pull-right"><i class="fa fa-print"></i>&nbsp;&nbsp;&nbsp;</span>', Url::to(['/site/sf', 'i' => base64_encode($v['id'])]), ['target' => '_blank']) : ''; ?></td>

                    </tr>
                <?php endforeach;
            else : ?>
                <td colspan="16">No Record found</td>
            <?php endif; ?>

            </tbody>
        </table>
    </div>
</div>

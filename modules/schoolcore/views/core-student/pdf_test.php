<?php

use app\components\ToWords;
use app\modules\paymentscore\models\PaymentChannels;
use kartik\grid\GridView;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;

//use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = $model->fullname;


$schedule = $model->paymentSchedule;
$cancelledSchedule = $model->cancelledPaymentSchedule;
$agreements = $model->StudentAgreements;
$agreements['striped'] = false;
$schedule['striped'] = false;

//$studentCampus = null;
//if($model->campus_id) {
//  $studentCampus = SchoolCampuses::findOne(['school_id'=>$model->school_id, 'id'=>$model->campus_id]);
//}
?>
<div style="margin-top:20px"></div>
<div class="letter">
    <div>


        <?php if (\Yii::$app->session->hasFlash('stuAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:" class='close-notify' data-flash='stuAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('stuAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('viewError'); ?>
                </div>
            </div>
        <?php endif; ?>


        <div class="row mt-5">


            <div class="pull-right">
                <?= Html::a('<i class="fa fa-envelope"></i> Send email', ['send-email', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>


                <?php if (!$model->archived) : ?>
                    <?= Html::a('<i class="fa fa-upload"></i>Upload Photo', ['/schoolcore/core-student/profileimage-upload', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('rw_student'))  : ?>
                    <?= Html::a('<i class="fa fa-edit"></i>Create Student', ['/schoolcore/core-student/create', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('r_student'))  : ?>
                    <?= Html::a('<i class="fa fa-users"></i>All students', ['/schoolcore/core-student/index', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?php endif; ?>

                <?= Html::a('<i class="fa fa-weight-hanging"></i>Grading', ['/schoolcore/core-student/student-report', 'studentNumber' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-print"></i> Print', ['print', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary', 'target' => '_blank']) ?>

                <?= Html::a('<i class="fa fa-qrcode"></i> QR Code', ['generate-qr', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-light', 'target' => '_blank']) ?>

                <?php if (Yii::$app->user->can('rw_student') ) : ?>
                    <?= Html::a('<i class="fa fa-edit" style="color: #fff;background: #0b93d5"></i> <span >Edit Details</span>', ['update', 'id' => $model->id], ['class' => 'modal_link btn btn-sm btn-info']) ?>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('rw_student')) : ?>


                <span class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">More &nbsp;<i
                            class="fa  fa-caret-square-o-down "></i></span>

                <ul class="col-md-2 dropdown-menu dropdown-menu-right">

                    <li class="clink"><a href="<?= Url::to(['/schoolcore/core-student/assign-fee', 'id' => $model->id]) ?>">Assign
                            A Fee</a></li>
                    <?php if ($model->schoolId->hasModule('SCHOOLPAY_PAYMENT_PLANS')) : ?>
                        <li>
                        <li class="clink"><a
                                    href="<?= Url::to(['/plan/default/assign-student-plan', 'id' => $model->id]) ?>">Assign
                                Payment Plan</a></li>
                        <li>
                        <li class="clink"><a
                                    href="<?= Url::to(['/plan/default/cancel-student-plan', 'id' => $model->id]) ?>">Cancel
                                Payment Plan</a></li>

                    <?php endif; ?>

                    <?php if ($model->payable) : ?>

                        <li class="clink"><a
                                    href="<?= Url::to(['/schoolcore/core-student/remove-fee', 'id' => $model->id]) ?>">Remove
                                Fee</a></li>

                    <?php endif; ?>
                    <?php if ($model->archived) : ?>


                        <li class="clink"><a
                                    href="<?= Url::to(['/schoolcore/core-student/unarchive-student', 'id' => $model->id]) ?>">Un-archive
                                Student</a></li>

                    <?php endif; ?>
                    <?php if (!$model->archived) : ?>

                        <li class="clink"><a href="<?= Url::to(['core-student/archive-student', 'id' => $model->id]) ?>">Archive
                                Student</a></li>

                        <?php if (\Yii::$app->user->can('adjust_student_bal')) : ?>

                            <li class="clink"><a href="<?= Url::to(['core-student/adjust-bal', 'id' => $model->id]) ?>">Adjust
                                    Balance</a></li>

                        <?php endif ?>
                    <?php endif ?>


                    <li><?= Html::a('Delete Student', ['core-student/delete', 'id' => $model->student_code], ['style' => 'color:#CD0C0C;', 'data-confirm' => 'Are you sure you want to delete this Student?', 'data-method' => 'post']) ?></li>
                </ul>
            </div>


            <?php endif; ?>

        </div>
        <hr>

    </div>

    <!--forms-->

    <div class="row">
        <div class="col bg-gradient-light" style="color: #000">


            <div class="row">

                <div class="col">

                </div>
                <div class="col-6">
                    <?php if ($model->passport_photo): ?>

                        <?= '<img src="data:image/jpeg;base64,'.\app\models\ImageBank::findOne(['id'=>$model->passport_photo])->image_base64.'" width="130px" /> ' ?>

                    <?php else : ?>
                        <?= Html::img('@web/web/img/user.png', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>

                    <?php endif; ?><br>
                    <b>
                        Name: <?= !$model->archived ? $model->fullname : '<span style="color:red">' . $model->fullname . '</span>'; ?></b><br>
                    <b> Student code: &nbsp; <?= ($model->student_code) ? $model->student_code : ' --' ?></b><br>
                    <b>Reg No
                        : </b><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?>
                    <br>
                </div>
                <div class="col">

                </div>
            </div>
            <p>
                <span style="font-size:11px; color:#444"><i><?= $model->studentClass->institution0->school_name . ', ' . $model->studentClass->institution0->location . ', TEL : ' . $model->studentClass->institution0->phone_contact_1; ?></i></span>
            </p>

            <div class="row">
                <div class="col">
                    <b>Date of Birth</b>
                </div>
                <div class="col">
                    <?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Class /Course</b>
                </div>
                <div class="col">
                    <?php if (!$model->archived) : ?>
                        <?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?><br>
                    <?php elseif ($model->class_when_archived) : ?>
                        <b>Last Class /Course (before
                            Archiving): </b><?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --' ?>
                        <br>
                    <?php endif; ?>                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>School</b>
                </div>
                <div class="col">
                    <?= ($model->school_id) ? $model->schoolName->school_name : ' --' ?><br>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Gender</b>
                </div>
                <div class="col">
                    <?= ($model->gender == 'M') ? 'Male' : 'Female' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Email</b>
                </div>
                <div class="col">
                    <?= ($model->student_email) ? $model->student_email : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Nationality</b>
                </div>
                <div class="col">
                    <?= (isset($model->nationality)) ? $model->nationality : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Day/Boarding</b>
                </div>
                <div class="col">
                    <?= (isset($model->day_boarding)) ? $model->day_boarding : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Contact Phone</b>
                </div>
                <div class="col">
                    <?= ($model->student_phone) ? $model->student_phone : ' --' ?>
                </div>
            </div>
        </div>
        <div class="col-md-6 container" style="padding:30px">

            <div class="row">

                <div class="col-8" style="padding: 20px">
                    <h3 style="color: #000"> Payment Summary</h3>
                    <div class="" style="border: 1px dashed #ccc;padding:25px;">
                        <div style="padding: 20px">
                            <tt>Total Active Fees
                                : <?= ($model->payableSum) ? number_format($model->payableSum, 2) : "  --"; ?></tt><br>
                            <?php if ($model->studentAccount->account_balance > 0) : ?>
                                <tt>Student Account Balance
                                    :<b> <?= ($model->studentAccount->account_balance < 0) ? '<span style="color:red">' . number_format(abs($model->studentAccount->account_balance), 0) . '</span>' : '<span style="color:lightseagreen">' . number_format(abs($model->studentAccount->account_balance), 0) . '</span>' ?></b></tt>
                                <br>
                            <?php endif; ?>
                            <tt>Outstanding Balance
                                :<b> <?= ($model->studentAccount->outstanding_balance < 0) ? '<span style="color:red">' . number_format($model->studentAccount->outstanding_balance, 0) . '</span>' : number_format($model->studentAccount->outstanding_balance, 0) ?></b></tt><br>

                        </div>
                    </div>
                </div>
                <div class="col">

                </div>
            </div>

            <!--        exempted fees-->

            <div class="" style="padding: 20px">

                <div class="">
                    <div class="">
                        <h3 style="color: #000"> Guardian Info</h3>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Name</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_name) ? $model->guardian_name : "--" ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Relationship</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Phone Contact</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Email</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_email) ? $model->guardian_email : "--" ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <!--nav-->

    <div class="container">
        <div class="row">
            <div class="col-sm">

            </div>

            <div class="col-sm">

                <div class="float-right">
                    <?php
                    echo \yii\bootstrap4\Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-pdf-trans', 'model' => get_class($searchModel)], [
                        'class' => 'btn btn-sm btn-danger',
                        'target' => '_blank',
                        'data-toggle' => 'tooltip',
                        'title' => 'Will open the generated PDF file in a new window'
                    ]);
                    echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel-old', 'model' => get_class($searchModel)], [
                        'class' => 'btn btn-sm btn-success',
                        'target' => '_blank'
                    ]);


                    ?>
                </div>
            </div>
        </div>
    </div>



    <div class= "row">
        <div class="col-md-12">
            <ul class="nav nav-tabs responsive">
                <li class="active" id = "fees-tab"><a href="#fees" data-toggle="tab"><i class="fa fa-inr"></i> Payments History</a></li>
                <?php if($model->schoolId->hasModule('SCHOOLPAY_PAYMENT_PLANS')) : ?>
                    <li><a href="#paymentSchedule" data-toggle="tab"><i class="fa fa-inr"></i> Payment Schedule</a></li>
                    <li><a href="#paymentPlanAgreements" data-toggle="tab"><i class="fa fa-inr"></i> Agreements</a></li>
                <?php endif; ?>
                <li class="" id = "payable_fees"><a href="#payableFees" data-toggle="tab"><i class="fa fa-inr"></i> Active Fees</a></li>
                <li class=" " id = "Exemptions"><a href="#exemptions" data-toggle="tab"><i class="fa fa-inr"></i> Exempted Fees</a></li>

                <li class="pull-right">
                    <?php $form = ActiveForm::begin([
                        'action' => ['view', 'id' => $model->id],
                        'method' => 'get',
                        'options' => ['class' => 'formprocess', 'id' => 'student_payments'],
                    ]); ?>
                    <?= $form->field($searchModel, 'only_received', ['inputOptions' => ['class' => 'form-control input-sm', 'style' => 'border:none; box-shadow:none; color:#3c8dbc;', "onchange" => "$(form).submit()"]])->dropDownList(['1' => 'Only Payments', '0' => 'All Transactions'])->label(false) ?>
                    <?php ActiveForm::end(); ?>

                </li>

            </ul>

            <div id='content' class="tab-content responsive">

                <div class="tab-pane active" id="fees">
                    <?= $this->render('_tab_transactions', ['model'=>$model,'dataProvider' => $dataProvider,
                        'searchModel'=> $searchModel]) ?>
                </div>
                <?php if($model->schoolId->hasModule('SCHOOLPAY_PAYMENT_PLANS')) : ?>
                    <div class="tab-pane " id="paymentSchedule">
                        <?= $this->render('@app/views/common/_table', ['data'=>$schedule, 'mod'=>'plan']) ?>
                    </div>
                    <div class="tab-pane " id="paymentPlanAgreements">
                        <?= $this->render('@app/views/common/_table', ['data'=>$agreements, 'mod'=>'plan']) ?>
                    </div>
                <?php endif; ?>
                <div class="tab-pane " id="payableFees">
                    <?= $this->render('_tab_fees_due', ['payable'=>$model->payable]) ?>
                </div>
                <div class="tab-pane " id="exemptions">
                    <?= $this->render('_tab_exemptions', ['exempts'=>$model->exempts]) ?>
                </div>


            </div>
        </div>
    </div>
</div>


<div class="student-profile py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card shadow-sm">
                    <div class="card-header bg-transparent text-center">

                        <?php if ($model->passport_photo): ?>

                            <?= '<img src="data:image/jpeg;base64,'.\app\models\ImageBank::findOne(['id'=>$model->passport_photo])->image_base64.'" width="130px" class="profile_img" /> ' ?>

                        <?php else : ?>
                            <?= Html::img('@web/web/img/user.png', ['class' => 'profile_img', 'alt' => 'profile photo']) ?>

                        <?php endif; ?>
                        <h3><?= !$model->archived ? $model->fullname : '<span style="color:red">' . $model->fullname . '</span>'; ?></h3>
                    </div>
                    <div class="card-body">
                        <p class="mb-0"><strong class="pr-1">Student Code:</strong> <?= ($model->student_code) ? $model->student_code : ' --' ?></p>
                        <?php if (!$model->archived) : ?>
                            <p class="mb-0"><strong class="pr-1">Class:</strong> <?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?></p>

                        <?php elseif ($model->class_when_archived) : ?>

                            <p class="mb-0"><strong class="pr-1">Last Class /Course (before
                                    Archiving):</strong> <?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --' ?></p>
                        <?php endif; ?>

                        <p class="mb-0"><strong class="pr-1">Reg Number:</strong><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?></p>
                        <p class="mb-0"><strong class="pr-1">Reg Number:</strong>
                            <?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card shadow-sm">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="mb-0"><i class="far fa-clone pr-1"></i>General Information</h3>
                    </div>
                    <div class="card-body pt-0">
                        <table class="table table-bordered">
                            <tr>
                                <th width="30%">Roll</th>
                                <td width="2%">:</td>
                                <td>125</td>
                            </tr>
                            <tr>
                                <th width="30%">Academic Year	</th>
                                <td width="2%">:</td>
                                <td>2020</td>
                            </tr>
                            <tr>
                                <th width="30%">Gender</th>
                                <td width="2%">:</td>
                                <td>Male</td>
                            </tr>
                            <tr>
                                <th width="30%">Religion</th>
                                <td width="2%">:</td>
                                <td>Group</td>
                            </tr>
                            <tr>
                                <th width="30%">blood</th>
                                <td width="2%">:</td>
                                <td>B+</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style="height: 26px"></div>
                <div class="card shadow-sm">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="mb-0"><i class="far fa-clone pr-1"></i>Other Information</h3>
                    </div>
                    <div class="card-body pt-0">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>





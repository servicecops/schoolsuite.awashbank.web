<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<p> Files Details</p>
<div class="row">
    <div class="col-xs-12">
        <?php if (!$imported['imported'] && $imported['uploaded_by'] == Yii::$app->user->identity->id) : ?>
            <p style="font-size:15px;color:grey;"><b>Note:</b> This file will be approved and imported in the student's
                database by <b>another admin</b>. For more information contact schoolpay on +256-414-597599</p>
        <?php elseif (!$imported['imported'] && (Yii::$app->user->can('approve_students')) && ($imported['uploaded_by'] != Yii::$app->user->identity->id)) : ?>
            <p style="font-size:15px;color:grey;"><b>Upload Notice:</b> Hello Admin, Please Confirm data before import
                for any irregularities.</p>
        <?php elseif ($imported['imported']) : ?>
            <p style="font-size:15px;color:grey;"><b>Notice:</b> This data is already imported. You are free to remove
                it from our temporary database</p>
        <?php elseif (!$imported['imported'] && $imported['uploaded_by'] != Yii::$app->user->identity->id) : ?>
            <p style="font-size:15px;color:grey;"><b>Note:</b> This file is not yet approved. <b>An admin</b> will
                approve and import this data into the student's database. For more information contact schoolpay on
                +256-414-597599</p>
        <?php endif; ?>
    </div>
</div>
<!--        -->
<?php
if ($status['state']) {
    echo $status['message'];
}
?>


<div class="row">
    <div class="col-md-12">
        <div id="loading"></div>
        <div id="errorMessage"></div>
        <div id="response">
            <div id="flash_message">
                <?php  if (\Yii::$app->session->hasFlash('uploadFailed')) : ?>
                    <?= \Yii::$app->session->getFlash('uploadFailed'); ?>
                <?php endif; ?>
            </div>
            <table class="table" style="background-color:#fff; margin-bottom:3px;">
                <tr>
                    <th>
                        <span style="font-size:17px;"><strong>&nbsp;<?= $imported['description'] . " (" . $imported['school_name'] . ")"; ?></strong></span>
                    </th>

                    <th class="pull-right no-padding">
                        <button class="btn btn-danger btn-sm text-white"
                                onclick="clinkConf('<?= Url::to(['core-student/delete-file', 'id' => $file]) ?>', 'Are you sure you want to delete this file')">
                            <b>Remove file</b>&nbsp; &nbsp;
                        </button>
                    </th>
                    <?php if (!$imported['imported'] && Yii::$app->user->can('approve_students') && $imported['uploaded_by'] != Yii::$app->user->identity->id): ?>
                        <th class="pull-right no-padding"><a class='aclink'
                                                             href="<?= Url::to(['core-student/file-details', 'id' => $file]) ?>"
                                                             data-confirm="Please confirm to save students"
                                                             data-method="post" data-params='{"action":"save"}'><span
                                        class="btn btn-primary btn-sm disabled"><b>Click to Submit Students </b>&nbsp; &nbsp;</span></a>
                        </th>

                    <?php endif; ?>

                    <th class="clink pull-right no-padding"><a href="<?= Url::to(['core-student/uploaded-files']) ?>">
                            <button class="btn btn-info btn-sm"><b>Back</b>&nbsp; &nbsp;</button>
                        </a></th>


                </tr>
            </table>
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>Sr.No</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>DoB</th>
                        <th>Reg No</th>
                        <th>Gender</th>
                        <th>Student Email</th>
                        <th>Student Phone</th>
                        <th>Nationality</th>
                        <th>Disability</th>
                        <th>Disability Nature</th>
                        <th>Guardian Name</th>
                        <th>Guardian Relation</th>
                        <th>Guardian Email</th>
                        <th>Guardian Phone</th>
                        <th>D/B</th>
                        <th>&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $srNo = 1;
                    foreach ($data as $k => $v) : ?>
                        <tr>
                            <td><?= $srNo; ?></td>
                            <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['middle_name']) ? $v['middle_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['dob']) ? $v['dob'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['reg_no']) ? $v['reg_no'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['student_email']) ? $v['student_email'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['nationality']) ? $v['nationality'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['disability']) ? 'Yes' : 'No' ?></td>
                            <td><?= ($v['disability_nature']) ? $v['disability_nature'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gurdian_name']) ? $v['gurdian_name'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['guardian_relation']) ? $v['guardian_relation'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gurdian_email']) ? $v['gurdian_email'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gurdian_phone']) ? $v['gurdian_phone'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['day_boarding']) ? $v['day_boarding'] : '<span class="not-set"> -- </span>' ?></td>
                            <td class="clink"><a href="<?= Url::to(['/classes/file-details', 'id' => $file]) ?>" ,
                                                 data-confirm="Are you sure you want to delete this student"
                                                 data-method="post" ,
                                                 data-params='{"action":"del_student", "stu":"<?= $v['id'] ?>"}'><i
                                            class="fa  fa-remove"></i>&nbsp;&nbsp;&nbsp;</a>
                            </td>
                        </tr>
                        <?php
                        $srNo++;
                    endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>
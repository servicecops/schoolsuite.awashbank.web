<?php 
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'action' => ['core-student/archive-student', 'id'=>$model->id],
        'method' => 'post',
        'options'=>['class'=>'formprocess', 'id'=>'archive_student_form_modal']
    ]); ?>
<div class="row">
        <div class="col-sm-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Only archive if <b><?= $model->fullname.' ( '.$model->studentClass->class_code.' )' ?></b> is no longer with the school. If archived, student won't be able to make anymore payments.</div>
        
        <div class="col-sm-12">
        <?= $form->field($model2, 'reason', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Give Reason'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
            <?= Html::submitButton('Archive', ['class' => 'btn btn-block btn-info', 'data-confirm'=>'Are you sure you want archive this student']) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
   </div>

<?php ActiveForm::end(); ?>

<?php 
$script = <<< JS
    $(document).ready(function(){
        $('form#archive_student_form_modal').yiiActiveForm('validate');
          $("form#archive_student_form_modal").on("afterValidate", function (event, messages) {
              // if($(this).find('.has-error').length) {
              //           return false;
              //   } else {
              //       $('.modal').modal('hide');
              //   }
            });
   });
JS;
$this->registerJs($script);
?>

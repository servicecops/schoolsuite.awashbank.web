<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\ImageBank;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\jui\DatePicker;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

$this->title = $model->fullname;
?>

<script>window.print();</script>

<div class="container">

    <div class="row">
        <div class="col ">
            <div class="mt-3">
                <?php if ($model->schLogo): ?>
                    <img class="img-thumbnail" , src="data:image/jpeg;base64,<?= $model->logo ?>"
                         height="150" width="130"/>
                <?php else : ?>
                    <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'img-thumbnail', 'alt' => 'profile photo', 'height' => 150, 'width' => 130]) ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col ">
            <div class="mt-3">
                <h3> <?= $model->studentClass->institution0->school_name; ?></h3>
                <h4> Name: &nbsp; <?= $model->fullname; ?></h4>
                <h4> Payment code: &nbsp; <?= $model->student_code; ?></h4>
                <b>Reg No
                    : </b><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?>
                <br>
                <b>Class /Course : </b><?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?>
            </div>
        </div>
        <div class="col ">
            <div class="mt-3">
                <?php if ($model->student_image): ?>
                    <img class="img-thumbnail" , src="data:image/jpeg;base64,<?= $model->image->image_base64 ?>"
                         height="150" width="130"/>
                <?php else : ?>
                    <?= Html::img('@web/web/img/dummy-user.jpg', ['class' => 'img-thumbnail', 'alt' => 'profile photo', 'height' => 150, 'width' => 130]) ?>
                <?php endif; ?>
            </div>
        </div>


    </div>
    <div class="container mt-5 mb-5">

        <hr class="l_header">
        <span style="font-size:11px; color:#444"><i><?= $model->studentClass->institution0->school_name . ', ' . $model->studentClass->institution0->district . ', TEL : ' . $model->studentClass->institution0->phone_contact_1; ?></i></span>

        <hr class="l_normal">
        <br>


        <div class="row container">
            <div class="col border" style="padding:10px">
                <h3 class="text-center mt-3">Student Bio</h3>
                <hr>
                <div class="row">
                    <div class="col"><b>Full Name: &nbsp; </b></div>
                    <div class="col"><?= $model->fullname; ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Student Code: &nbsp; </b></div>
                    <div class="col"><?= $model->student_code; ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Registration Number : </b></div>
                    <div class="col"><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Class : </b></div>
                    <div class="col"><?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Date of Birth : </b></div>
                    <div class="col"><?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Gender : </b></div>
                    <div class="col"><?= ($model->gender == 'M') ? 'Male' : 'Female' ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">Email : </b></div>
                    <div class="col"><?= ($model->student_email) ? $model->student_email : ' --' ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Phone Contact : </b></div>
                    <div class="col"><?= ($model->student_phone) ? $model->student_phone : ' --' ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Nationality : </b></div>

                    <div class="col"><?= (isset($model->nationality)) ? $model->nationality : ' --' ?></div>
                    <hr>
                </div>
            </div>

            <div class="col border" style="padding:10px">
                    <h3 class="text-center mt-3">Fee Summary</h3>
                    <hr>
                <div class="row">
                    <div class="col"><b>Total Active Fees : </b></div>
                    <div class="col"><?= ($model->payableSum) ? number_format($model->payableSum, 2) : "  --"; ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Total Active Fees : </b></div>
                    <div class="col"><?= ($model->payableSum) ? number_format($model->payableSum, 2) : "  --"; ?></div>
                </div>
               <br/>
                <?php if ($model->studentAccount->account_balance > 0) : ?>
                    <div class="row">
                        <div class="col"><b>Student Account Balance : </b></div>
                        <div class="col"> <?= ($model->studentAccount->account_balance < 0) ? '<span style="color:red">' . number_format(abs($model->studentAccount->account_balance), 0) . '</span>' : '<span style="color:lightseagreen">' . number_format(abs($model->studentAccount->account_balance), 0) . '</span>' ?></div>
                    </div>
                <?php endif; ?>
                <hr>
                <div class="row">
                    <div class="col"><b>Student Account Balance : </b></div>
                    <div class="col"> <?= ($model->studentAccount->account_balance < 0) ? '<span style="color:red">' . number_format(abs($model->studentAccount->account_balance), 0) . '</span>' : '<span style="color:lightseagreen">' . number_format(abs($model->studentAccount->account_balance), 0) . '</span>' ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Outstanding Balance : </b></div>
                    <div class="col"> <?= ($model->studentAccount->outstanding_balance < 0) ? '<span style="color:red">' . number_format($model->studentAccount->outstanding_balance, 0) . '</span>' : number_format($model->studentAccount->outstanding_balance, 0) ?></div>
                </div>
                <hr>
                <h3 class="text-center mt-3">Guardian Info</h3>
                <hr>
                <div class="row">
                    <div class="col"><b>Name : </b></div>
                    <div class="col"><?= ($model->guardian_name) ? $model->guardian_name : "--" ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Phone Contact : </b></div>
                    <div class="col"><?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Email : </b></div>
                    <div class="col"><?= ($model->guardian_email) ? $model->guardian_email : "--" ?></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col"><b>Relationship : </b></div>
                    <div class="col"><?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?></div>
                </div>

            </div>
        </div>
        <div class="row mt-5">

            <div class="col-md-12">
                <h3 class="text-center">Transactions</h3>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'tableOptions' => ['class' => 'table table-striped', 'style' => 'font-size:12px;'],
                    'layout' => "{items}",
                    'columns' => [
                        [
                            'attribute' => 'Date',
                            'value' => function ($model) {
                                return date('d/m/y H:i', strtotime($model->date_created));
                            }
                        ],
                        [
                            'attribute' => 'Description',
                            'value' => function ($model) {
                                return Html::decode($model->description);
                            },
                            'filter' => false,
                        ],
                        [
                            'attribute' => 'Tans Type',
                            'value' => 'trans_type',
                            'filter' => false
                        ],
                        [
                            'attribute' => 'Channel',
                            'options' => ['class' => 'col-xs-1'],
                            'value' => function ($model) {
                                if ($model->payment_id) {
                                    return $model->payment->paymentChannel->channel_code;
                                }
                            },

                        ],
                        [
                            'attribute' => 'Channel Memo',
                            'value' => function ($model) {
                                if ($model->payment_id)
                                    return Html::decode($model->payment->channel_memo);
                            }
                        ],
                        [
                            'attribute' => 'Amount',
                            'value' => function ($model) {
                                if ($model->amount < 0)
                                    return '<span style="color:red">' . number_format($model->amount, 2) . '</span>';
                                else
                                    return number_format($model->amount, 2);
                            },
                            'format' => 'html',
                            'filter' => false,
                        ],
//            [
//              'attribute'=>'Balance',
//              'value'=>function($model){
//                if($model->balance_after < 0)
//                    return '<span style="color:red">'.number_format($model->balance_after).'</span>';
//                else
//                    return number_format($model->balance_after);
//              },
//              'format'=>'html',
//              'filter'=>false,
//            ],

                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

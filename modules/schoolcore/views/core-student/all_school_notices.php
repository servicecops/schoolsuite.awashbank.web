<?php

use app\modules\noticeboard\models\NoticeBoard;
use yii\db\Expression;

$schoolId = Yii::$app->user->identity->school_id;
if ($schoolId) {
    $notices = Yii::$app->db->createCommand('select * from notice_board nb 
where (:current_datetim between start_date and end_date) and school_id =:school_id')
        ->bindValue(':current_datetim', new Expression('NOW()'))
        ->bindValue(':school_id', Yii::$app->user->identity->school_id)
        ->queryAll();
//    Yii::trace($notices);
    ?>

    <div class="container letter" style="font-size: larger">

        <p style="background: #293897;padding: 10px">
            <strong class="text-uppercase text-center" style="color:white">ANNOUNCEMENT</strong>
        </p>
        <?php
        if ($notices) {
            ?>

            <marquee direction="up" scrollamount="5">
                <div class="container letter" style="font-size: larger">


                    <center>
                        <?php

                        foreach ($notices as $notice) {
                            ?>
                            <div class="alert alert-info">
                                <strong class="text-uppercase text-center"><?php echo $notice['title'] ?></strong>
                            </div>
                            <?php echo $notice['announcement'] ?>
                            <br>
                            <hr class="l_header" style="padding: 5px; margin-bottom: 5px !important;">

                            <?php
                        } ?>


                    </center>
                </div>
            </marquee>
        <?php } else {
            ?>
            <div class="alert alert-info">
                <strong class="text-uppercase text-center">No Announcement Currently</strong>
            </div>
        <?php }
        ?>
    </div>
    <?php
}


?>




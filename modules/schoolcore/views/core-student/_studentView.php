<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$schedule = $model->paymentSchedule;
$agreements = $model->StudentAgreements;
$searchModel = new \app\modules\schoolcore\models\CoreStudentSearch();
$agreements['striped']= false;
$schedule['striped']= false;


?>
<div class="row hd-title" data-title="<?= $model->fullname ?>">

    <?php  if (\Yii::$app->session->hasFlash('stuAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='stuAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('stuAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php  if (\Yii::$app->session->hasFlash('viewError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('viewError'); ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="col-md-12">
        <div class="letter">
            <div class="row">
                <div class="col-md-12" style="margin-top:-20px;">
                    <div class="row">
                        <button class="btn btn-sm btn-primary" onclick="clink('<?= Url::to(['/student/create']) ?>')"><i class="fa fa-user-plus"></i> New Student</button>
                        <button class="btn btn-sm btn-primary" onclick="clink('<?= Url::to(['/student/index']) ?>')"><i class="fa fa-list"></i> All Students</button>

                        <div class="pull-right">
                            <?php if(\Yii::$app->user->can('schoolsuite_admin'))  : ?>
                                <a class="modal_link btn btn-sm btn-info" href="javascript:;" data-href="<?= Url::to(['/student/reply-message', 'id' => $model->id]) ?>" data-title="Reply To - <?= $model->fullname; ?>"> <i class="fa fa-comment "></i>&nbsp; Reply</a>
                            <?php endif; ?>

                            <?= Html::a('<i class="fa fa-print"></i> Print', ['print', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary', 'target'=>'_blank']) ?>

                            <span class="dropdown">
                  <span class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">More &nbsp;<i class="fa  fa-caret-square-o-down "></i></span>
                          <ul class="dropdown-menu dropdown-menu-right">
                          <li><a class="modal_link" href="javascript:;" data-href="<?= Url::to(['/student/assign-fee', 'id' => $model->id]) ?>" data-title="Assign A Fee to - <?= $model->fullname?>">Assign A Fee</a>
                            </li>

                              <?php if($model->payable) : ?>
                                  <li><a class="modal_link" href="javascript:;" data-href="<?= Url::to(['/student/remove-fee', 'id' => $model->id]) ?>" data-title="Remove Fee to - <?= $model->fullname?>">Remove Fee</a>
                            </li>
                              <?php endif; ?>
                              <?php if($model->archived) : ?>
                                  <li><a class="modal_link" href="javascript:;" data-href="<?= Url::to(['/student/unarchive-student', 'id' => $model->id]) ?>" data-title="Unarchive Student - <?= $model->fullname?>">Un-archive Student</a>
                              </li>
                              <?php endif; ?>
                              <?php if(!$model->archived) : ?>
                                  <li><a class="modal_link" href="javascript:;" data-href="<?= Url::to(['/student/archive-student', 'id' => $model->id]) ?>" data-title="Archive Student - <?= $model->fullname?>">Archive Student</a>
                            </li>
                          
                          <?php if(\Yii::$app->user->can('adjust_student_bal')) : ?>
                                      <li><a class="modal_link" href="javascript:;" data-href="<?= Url::to(['/student/adjust-bal', 'id' => $model->id]) ?>" data-title="Adjust Balance">Adjust Balance</a>
                            </li>
                                  <?php endif ?>
                              <?php endif ?>


                            <li><?= Html::a('Delete Student', ['/student/delete', 'id' => $model->student_code], [ 'style'=>'color:#CD0C0C;',  'data-confirm'=>'Are you sure you want to delete this Student?', 'data-method'=>'post']) ?></li>
                          </ul>
                    </span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-sm-2 col-xs-12 no-padding-right">
                            <?php if($model->student_image): ?>
                                <img class="img-thumbnail" src="<?= Url::to(['/import/import/image-link', 'id'=>$model->student_image]) ?>" width="130px" />
                            <?php else : ?>
                                <?= Html::img('@web/web/img/dummy-user.jpg', ['class'=>'img-thumbnail', 'alt' => 'profile photo', 'width'=>130, 'data-toggle'=>'tooltip', 'data-original-title'=>'Tooltip on right']) ?>
                            <?php endif; ?>
                        </div>
                        <!-- <div  style="margin-top:-150px; margin-left:160px;"> -->
                        <div class="col-sm-4 no-padding" style="margin-top:-20px;">
                            <h3> <?= !$model->archived ? $model->fullname : '<span style="color:red">'.$model->fullname.'</span>'; ?></h3>
                            <h4> Student code: &nbsp; <?= $model->student_code; ?></h4>
                            <b>Reg No : </b><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?><br>
                            <?php if(!$model->archived) : ?>
                                <b>Class /Course : </b><?= ($model->class_id) ? $model->studentClass->class_description : ' --'  ?><br>
                            <?php elseif($model->class_when_archived) : ?>
                                <b>Last Class /Course (before Archiving): </b><?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --'  ?><br>
                            <?php endif; ?>
                            <div id="stu_barcode" ></div>
                        </div>
                        <div class="col-sm-5 col-xs-12 pull-right" style="text-align:center;">
                            <div class="col-xs-12">

                                <h3 class="lined-heading"> <span>Payment Summary</span></h3>

                                <span class="col-xs-12" style="border: 1px dashed #ccc; padding:10px; font-size:16px;">
                  <tt>Total Active Fees : <?= ($model->payableSum) ? number_format($model->payableSum, 2) : "  --"; ?></tt><br>

                  <?php if($model->studentAccount->account_balance > 0) : ?>
                      <tt>Student Account Balance :<b> <?= ($model->studentAccount->account_balance < 0) ? '<span style="color:red">'. number_format(abs($model->studentAccount->account_balance), 2).'</span>' : '<span style="color:lightseagreen">'. number_format(abs($model->studentAccount->account_balance), 2).'</span>'  ?></b></tt><br>
                  <?php endif; ?>

                  <tt>Outstanding Balance :<b> <?= ($model->studentAccount->outstanding_balance < 0) ? '<span style="color:red">'. number_format($model->studentAccount->outstanding_balance, 2).'</span>' : number_format($model->studentAccount->outstanding_balance, 2)  ?></b></tt><br>
              </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="l_header" style="margin-top:15px;">
            <span style="font-size:11px; color:#444"><i><?= $model->studentClass->institution0->school_name.', '.$model->studentClass->institution0->village.', TEL : '.$model->studentClass->institution0->phone_contact_1; ?></i></span>
            <p></p>
            <div class="row">
                <div class="col-md-12" style="text-align:center">
                    <h3> Personal Information</h3><hr class="l_normal">
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                        <h4>Student Information</h4>
                        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <b>Date of Birth : </b><?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth) ): ' --' ?><br>
                                <b>Gender : </b><?= ($model->gender == 'M') ? 'Male' : 'Female'  ?><br>
                                <b>Email : </b><?= ($model->student_email) ? $model->student_email : ' --'  ?>
                            </div>
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <b>Phone Contact : </b><?= ($model->student_phone) ? $model->student_phone : ' --'  ?><br>
                                <b>Nationality : </b><?= (isset($model->nationality)) ? $model->nationality : ' --'  ?><br>
                                <b>Day /Boarding : </b><?= (isset($model->day_boarding)) ? $model->day_boarding : ' --'  ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12"  style="border-left:1px solid #ccc; padding: 0 0 10px 0;">
                        <h4>Parent /Guardian Information</h4>
                        <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class='col-md-6 col-sm-6 col-xs-6'>
                                <b>Name : </b><?= ($model->guardian_name) ? $model->guardian_name : "--" ?><br>
                                <b>Phone Contact : </b><?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?><br>
                                <b>Email : </b><?= ($model->guardian_email) ? $model->guardian_email : "--" ?><br>
                                <b>Relationship : </b><?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?>
                            </div>

                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <?php $optionsArray = array(
                'elementId'=> 'stu_barcode', /* div or canvas id*/
                'value'=> $model->student_code, /* value for EAN 13 be careful to set right values for each barcode type */
                'type'=>'code39',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
            );
            echo BarcodeGenerator::widget($optionsArray); ?>
            <hr class="l_normal"><br>
            <div class= "row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs responsive">
                        <li class="active" id = "fees-tab"><a href="#fees" data-toggle="tab"><i class="fa fa-inr"></i> Payments History</a></li>

                        <li id = "payable_fees"><a href="#payableFees" data-toggle="tab"><i class="fa fa-inr"></i> Active Fees</a></li>
                        <li id = "Exemptions"><a href="#exemptions" data-toggle="tab"><i class="fa fa-inr"></i> Exempted Fees</a></li>


                    </ul>


                </div>
            </div>
        </div>

        <?php
        $script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
        $this->registerJs($script);
        ?>


 

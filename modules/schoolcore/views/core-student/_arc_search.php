<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

?>
<p style="color:#F7F7F7">Student Information</p>

<div class="container bg-white">
    <?php $form = ActiveForm::begin([
        'action' => ['archived'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>


    <div class="row mt-3">

        <div class="row">
    <?php if(\Yii::$app->user->can('schoolpay_admin')) : ?>
    <div class="col-sm-3 no-padding">
        <?php
            $url = Url::to(['core-school/schoollist']);
            $selectedSchool = empty($model->schsearch) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->schsearch)->school_name;
            echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size'=>'sm',
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id'=>'student_selected_school_id',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>
            </div>
    <div class="col-sm-3 no-padding"><?= $form->field($model, 'class_when_archived', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->dropDownList(
    	['prompt'=>'Select Last class'])->label(false) ?></div>
    <?php elseif(\app\components\ToWords::isSchoolUser()) : ?>
    <div class="col-sm-3 no-padding"><?= $form->field($model, 'class_when_archived')->dropDownList(
            ArrayHelper::map(\app\modules\schoolcore\models\CoreSchoolClass::find()->where(['school_id'=>Yii::$app->user->identity->school_id])->all(), 'id', 'class_description'), ['prompt'=>'Select Last class', 'class'=>'form-control input-sm']
            )->label(false) ?></div>
    <?php endif; ?>
    <div class="col-sm-2 no-padding"><?= $form->field($model, 'school_student_registration_number', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Ente Reg No.'])->label(false) ?></div>

    <div class="col-sm-3 no-padding"><?= $form->field($model, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Enter Student Name /Payment Code'])->label(false) ?></div>
 	<div class="col-sm-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$url = Url::to(['classes/lists']);
$cls = $model->class_id;
$script = <<< JS
$("document").ready(function(){ 
    var sch = $("#student_selected_school_id").val();
    if(sch){
        $.get('$url', {id : sch}, function( data ) {
            $('select#studentsearch-class_when_archived').html(data);
            $('#studentsearch-class_when_archived').val('$cls');
        });
    }

    $('body').on('change', '#student_selected_school_id', function(){

        $.get('$url', {id : $(this).val()}, function( data ) {
            $('select#studentsearch-class_when_archived').html(data);
        });
    });

  });
JS;
$this->registerJs($script);
?>

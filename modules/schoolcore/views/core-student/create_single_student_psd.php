<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
$this->title = "Create Passwords";

?>




<div class="container">
    <div>
        <h3 class="box-title text-center" style="color: #000"><b><i class="fa  fa-user-lock"></i> <?= Html::encode($this->title) ?></b></h3>
        <hr class="style14">
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'generate-std-password',
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
    ]); ?>
    <div class="row">
        <div class="col-sm">

        </div>
        <div class="col-sm">
            <div class="card mt-5">
                <div class="card-body">


                    <?php if (\app\components\ToWords::isSchoolUser()) : ?>


                        <?= $form->field($model, 'new_pass',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['maxlength' => 60, 'class'=>'form-control accounts_control','placeholder' => 'New Password'])->label('New Password *') ?>
                        <?= $form->field($model, 'password2',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['maxlength' => 60,'class'=>'form-control accounts_control', 'placeholder' => 'Confirm Password'])->label('Confirm Passord *') ?>

                        <div class="alert alert-danger invalid-accounts-error" style="display: none">
                            Please supply all information to continue
                        </div>
                        <?= Html::submitButton(Yii::t('app', 'Save'),['class' =>'btn btn-primary'] ) ?>
                        <?= Html::a('Cancel', ['/site/index'], ['class' => 'btn btn-default']) ?>


                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="col-sm">

        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
<?php
$script = <<< JS
 $('document').ready(function(){
     
        let schoolForm = $('#generate-std-password');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.accounts_control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val()) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });
    })
JS;
$this->registerJs($script);
?>


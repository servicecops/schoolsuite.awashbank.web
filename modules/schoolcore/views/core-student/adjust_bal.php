<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'action' => ['core-student/adjust-bal', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess modal_form', 'id' => 'adjust_bal_form_modal']
]); ?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model2, 'new_balance', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter New Balance']])->textInput()->label('') ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model2, 'balance_type', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Select Balance Adjustment Type']])->dropDownList(
            [
                    'OUTSTANDING' => 'This is the student outstanding balance (Negative)',
                    'OVERPAYMENT' => 'This is the student postitive account balance',
            ],
            ['id' => 'balance_type_select', 'prompt' => 'Select Balance Adjustment Type']
        )->label('Select Balance Adjustment Type') ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model2, 'reason', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Give Reason']])->textInput()->label('') ?>
    </div>



        <div class="col-sm-6">
            <?= Html::submitButton('Adjust', ['class' => 'btn btn-block btn-info', 'data-confirm' => 'Are you sure you want Adjust balance']) ?>
        </div>
        <div class="col-sm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>

    </div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
    $('form#adjust_bal_form_modal').yiiActiveForm('validate');
      $("form#adjust_bal_form_modal").on("afterValidate", function (event, messages) {
        //   if($(this).find('.has-error').length) {
        //                 return false;
        //         } else {
        //             $('.modal').modal('hide');
        //         }
         });
   });
JS;
$this->registerJs($script);
?>

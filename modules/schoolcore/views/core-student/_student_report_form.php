<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = 'Generate Student Report';
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-group-information-create row hd-title" data-title="New Report Group">

    <div class="row" style="background: #efefef">
        <div class="wizard">
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12"><h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3></div>
    <p>&nbsp;</p>
</div>
<?php /** @var TYPE_NAME $student_info */
Yii::trace($student_info);

$form = ActiveForm::begin([
    'method' => 'POST',
    'action' => ['student-report', 'studentNumber' => $student_info['student_number']],
    'options' => ['class' => 'formprocess'],
]); ?>


<div class="row">
    <?php if (Yii::$app->user->can('schoolpay_admin')): ?>
        <div class="col-md-3">
            <?php
            $url = Url::to(['/schoolcore/core-school/active-schoollist']);
            $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id' => 'school_search',
                    'class' => 'form-control',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ])->label('Select School'); ?>
        </div>

        <div class="col-md-3 col-sm-4 no-padding-right">
            <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
                ['prompt' => 'Filter class'])->label('Class') ?>
        </div>
    <?php endif; ?>

    <?php if (\app\components\ToWords::isSchoolUser()) : ?>
        <div class="col-md-3 col-sm-4 no-padding-right">
            <?php
            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Class'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Class *'); ?>
        </div>
    <?php endif; ?>

    <?php if (\app\components\ToWords::isSchoolUser()) : ?>
        <div class="col-md-3 col-sm-4 no-padding-right">
            <?php
            $data = CoreTerm::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

            echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'term_name'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Term'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Term *'); ?>
        </div>
    <?php elseif (Yii::$app->user->can('schoolpay_admin')): ?>
        <div class="col-md-3 col-sm-4 no-padding-right">
            <?= $form->field($model, 'term_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
                ['prompt' => 'Filter terms'])->label('term') ?>
        </div>
    <?php endif; ?>

</div>

<?= Html::submitButton("Generate Report", ['class' => 'btn btn-success btn-sm']) ?>



<?php ActiveForm::end(); ?>




<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$classes = $model->classes ? $model->classes : [];
$script = <<< JS
    $("document").ready(function(){ 
       var sch = $("#group_school_search").val();
        if(sch){
            $.post( '$url/'+sch, function( data ) {
                $( "select#group_class_select" ).html(data);
            })
        }
      
    });
JS;
$this->registerJs($script);
?>
<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->class_id;


$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
        
        
         $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
        
    
    }
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>

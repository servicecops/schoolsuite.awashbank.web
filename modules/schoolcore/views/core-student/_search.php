<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\SchoolCampuses;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;


//Yii::trace($model);
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>


<div class="row ">
    <?php if (\app\components\ToWords::isSchoolUser()) : ?>

        <div class="col-md-4 ">
            <?php
            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id,'active'=>true])->all();

            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Class'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false); ?>
        </div>

        <?php if(Yii::$app->user->identity->school->hasModule('SCHOOL_CAMPUSES')): ?>
            <div class="col-md-4 "><?= $form->field($model, 'campus_id')->dropDownList(
                    ArrayHelper::map(SchoolCampuses::find()->where(['school_id'=>Yii::$app->user->identity->school_id])->all(), 'id', 'campus_name'), ['prompt'=>'Filter Campus', 'class'=>'form-control input-sm']
                )->label(false) ?></div>
        <?php endif; ?>

    <?php endif; ?>

    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
        <div class="col-md-8">
            <div class="col-sm-4 ">
                <?php
                $url = Url::to(['core-school/active-schoollist']);
                $selectedSchool = empty($model->schsearch) ? '' : CoreSchool::findOne($model->schsearch)->school_name;
                echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id'=>'corestudent_selected_school_id',
                        'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#corestudentsearch-class_id" ).html(data);
                        });'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false); ?>

            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'campus_id', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->dropDownList(
                    ['prompt'=>'Filter Campus'])->label(false) ?>
            </div>

        </div>
    <?php endif; ?>


    <div class="col-md-2">
        <?= $form->field($model, 'school_student_registration_number', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter Reg No.'])->label(false) ?>

    </div>
    <div class="col-md-2">
        <?= $form->field($model, 'modelSearch', ['inputOptions' => ['class' => 'form-control'],])->textInput(['title' => 'Enter student name or payment code',

            'placeHolder' => 'Name or student code',
            'data-toggle' => 'tooltip',

            'data-trigger' => 'hover',

            'data-placement' => 'bottom'])->label(false) ?>

    </div>
    <div class="col-md-2">

        <?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-md']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>




<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->class_id;

$campusurl = Url::to(['/schoolcore/core-school/campuslists']);
$campus = $model->campus_id;

$script = <<< JS
$("document").ready(function(){ 
    
    var sch = $("#corestudent_selected_school_id").val();
    if(sch){
        console.log("here");
        $.get('$url', {id : sch}, function( data ) {
            $('select#corestudentsearch-class_id').html(data);
            $('#corestudentsearch-class_id').val('$cls');
        });
        
        $.get('$campusurl', {id : sch}, function( data ) {
            $('select#corestudentsearch-campus_id').html(data);
            $('#corestudentsearch-campus_id').val('$campus');
        });
    }

    $('body').on('change', '#corestudent_selected_school_id', function(){

        console.log("body");
        $.get('$url', {id : $(this).val()}, function( data ) {
            $('select#corestudentsearch-class_id').html(data);
        });
        
        $.get('$campusurl', {id : $(this).val()}, function( data ) {
            $('select#corestudentsearch-campus_id').html(data);
        });
    });

  });
JS;
$this->registerJs($script);
?>
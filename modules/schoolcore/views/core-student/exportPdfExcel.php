<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Students Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Payment Code</th><th>First Name</th><th>Middle Name</th><th>Last Name</th><?php if(Yii::$app->user->can('rw_sch')){ echo "<th>School Name</th>"; } ?> <th>Class code</th><th>Student Phone</th><th>&nbsp;</th></tr>
        </thead>
        <tbody>
            <?php 
            $no = 1; 
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['student_code']) ? $sinfo['student_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['first_name']) ? $sinfo['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['middle_name']) ? $sinfo['middle_name'] : '<span class="not-set">(not set) </span>' ?></td>

                    <td><?= ($sinfo['last_name']) ? $sinfo['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <?php if(Yii::$app->user->can('rw_sch')) : ?>
                    <td><?= ($sinfo['school_name']) ? $sinfo['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <?php endif; ?>
                    <td><?= ($sinfo['class_code']) ? $sinfo['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['student_phone']) ? $sinfo['student_phone'] : '<span class="not-set">(not set) </span>'?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

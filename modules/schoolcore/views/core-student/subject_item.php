<?php

use app\modules\schoolcore\models\CoreSubjectSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $model app\modules\schoolcore\models\CoreSubject */

?>
<div class="container event-title">

    <p class="text-muted">
        <i> <small>Due date  <b><?=$model->subject_name?></b> </small></i>
    </p>

    <hr>
</div>

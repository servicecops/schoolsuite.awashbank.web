<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
$this->title = "Create Passwords for a class";

?>




<div class="container">
    <div>
        <h3 class="box-title text-center" style="color: #000"><b><i class="fa  fa-user-lock"></i> <?= Html::encode($this->title) ?></b></h3>
        <p class="box-title text-center">This will generate passwords for only those that do not have passwords in the selected class</p>
        <hr class="style14">
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'generate-std-password',
        'fieldConfig' => [
            'template' => "{label}{input}{error}",
        ],
    ]); ?>
    <div class="row">
        <div class="col-sm">

        </div>
        <div class="col-sm">
            <div class="card mt-5">
                <div class="card-body">


                    <?php if (\app\components\ToWords::isSchoolUser()) : ?>


                            <?php
                            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                                'language' => 'en',
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Find Class'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('Class *'); ?>

                        <?= $form->field($model, 'new_pass',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['maxlength' => 60,'class'=>'form-control accounts_control', 'placeholder' => 'New Password'])->label('New Password *') ?>
                        <?= $form->field($model, 'password2',['labelOptions'=>['style'=>'color:#000']])->passwordInput(['maxlength' => 60,'class'=>'form-control accounts_control', 'placeholder' => 'Confirm Password'])->label('Confirm Password *') ?>

                        <div class="alert alert-danger invalid-accounts-error" style="display: none">
                            Please supply all information to continue
                        </div>
                        <div>
                            <?= Html::submitButton(Yii::t('app', 'Change Password <i class="fa fa-lock"></i>'), ['class' =>'btn-info btn-sm col-5']) ?>
                            <?= Html::a('Cancel', ['/schoolcore/core-student/std-list-wt-passwords'], ['class' => 'btn btn-default col-5']) ?>
                        </div>

                    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="col-sm">

        </div>
        <?php ActiveForm::end(); ?>
    </div>

</div>
<?php

$script = <<< JS
 $('document').ready(function(){
     
        let schoolForm = $('#generate-std-password');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.accounts_control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val()) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });
    })
JS;
$this->registerJs($script);
?>


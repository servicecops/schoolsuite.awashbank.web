<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\Html;
use yii\bootstrap4\LinkPager;
//use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\helpers\Url;

use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Information';
?>


<div class="row" style="margin-bottom: 15px">
    <div class="col "><span style="font-size:20px;color:#2c3844">&nbsp;<i
                    class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
</div>
    <div class="row">


        <div class="col-md-10"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
        <div class="col-md-2">
            <div class="pull-right">
            <?php

            echo Html::a('<i class="fa far fa-file-pdf-o"></i> Pdf', ['core-student/export-to-pdf', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-md btn-danger',
                'target' => '_blank',

            ]);


            echo Html::a('<i class="fa far fa-file-excel-o"></i> Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                'class' => 'btn btn-md btn-success',
                'target' => '_blank'
            ]);



            ?>
            </div>
        </div>

    </div>

    <div class="row">


        <table class="table table-striped">
            <thead>
            <tr>

                <th class='clink'><?= $sort->link('student_code') ?></th>
                <th class='clink'><?= $sort->link('first_name', ['label' => 'Student Name']) ?></th>
                <th class='clink'><?= $sort->link('school_student_registration_number', ['label' => 'Reg No.']) ?></th>
                <?php if (Yii::$app->user->can('r_sch')) {
                    echo "<th class='clink'>" . $sort->link('school_name') . "</th>";
                } ?>
                <th class='clink'><?= $sort->link('class_code') ?></th>
                <th class='clink'><?= $sort->link('student_phone') ?></th>
                <th class='clink'>Reset student Password</th>
                <th class='clink'>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">
                        <td class="clink"><?= ($v['student_code']) ? '<a href="' . Url::to(['core-student/view', 'id' => $v['id']]) . '">' . $v['student_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['school_student_registration_number']) ? $v['school_student_registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                        <?php if (Yii::$app->user->can('rw_student')) : ?>
                            <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                        <?php endif; ?>
                        <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td>
                            <?= ($v['web_user_id']) ? Html::a('<i class="fa  fa-unlock"></i>', ['core-student/reset', 'id' => $v['web_user_id']], ['class' => 'aclink']) : '<span class="not-set">(no password set) </span>' ?>

                        </td>
                        <td>

                            <?= Html::a('<i class="fa fa-edit"></i>', ['core-student/update', 'id' => $v['id']]); ?>
                            <?= Html::a('<i class="fa  fa-eye"></i>', ['core-student/view', 'id' => $v['id']], ['class' => 'aclink']) ?>
                        </td>
                    </tr>
                <?php endforeach;
            else :?>
                <tr>
                    <td colspan="8">No student found</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

        <?= LinkPager::widget([
            'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last'
        ]); ?>


    </div>
<div style="clear: both"></div>

<?php if(Yii::$app->user->can('sch_teacher')):?>


<div id="ad-features">
    <span class="ad-btn1">
        <i class="fa fa-times" aria-hidden="true"></i>
        <!--        <button aria-hidden="true"></button>-->
    </span>

    <!--REPLACE THIS IMAGE LINK-->
    <a href="#" target="_blank">
        <?= $this->render('all_school_notices.php') ?>

    </a>
</div>
<?php endif;?>
<?php
$script = <<< JS
    $(document).ready(function(){

        $("#ad-features").fadeIn()
        .animate({bottom:0,right:0}, 1500, function() {
            //callback
        });      

        $(".ad-btn1").click(function(e){
            e.preventDefault();
            e.stopPropagation()
            $('#ad-features').fadeOut();
        });
      
    });
JS;
$this->registerJs($script);
?>

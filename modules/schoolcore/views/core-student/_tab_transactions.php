<?php

use app\components\ToWords;
use app\modules\paymentscore\models\PaymentChannels;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\bootstrap4\LinkPager;


?>

<div class="row">
    <div class="col-md-12" style="color:#fff">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table table-striped'],
            'layout' => "{items}\n{pager}",
            'rowOptions' => function ($model, $key, $index, $grid) {
                //Style the reversal rows to have a gray color
//                if($model->reversed || $model->reversal_flag) {
//                    return ['style' => 'color: #ccc'];
//                }

                if (strcasecmp('REVERSAL', $model->trans_type) == 0) {
                    return ['style' => 'text-decoration: line-through'];
                }
            },
            'columns' => [
                [
                    'attribute' => 'date_created',
                    'value' => function ($model) {
                        return date('d/m/y H:i', strtotime($model->date_created));
                    },
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_created',
                        'dateFormat' => 'yyyy-MM-dd',
                        'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'Filter Date'],
                        'clientOptions' => [
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                    ]),

                ],
                [
                    'attribute' => 'description',
                    'value' => function ($model) {
                        return Html::decode($model->description);
                    },
                    'filter' => false,
                ],
                [
                    'attribute' => 'trans_type',
                    'value' => 'trans_type',
                    'filter' => false
                ],
                [
                    'attribute' => 'payment_id',
                    'options' => ['class' => 'col-xs-1'],
                    'value' => function ($model) {
                        if ($model->payment_id) {
                            return '<img src="' . Url::to(['/import/import/image-link', 'id' => $model->payment->paymentChannel->payment_channel_logo]) . '" width="25px" /> ' . $model->payment->paymentChannel->channel_name;
                        }
                    },

                    'filter' => Html::activeDropDownList($searchModel, 'payment_id', ArrayHelper::htmlDecode(ArrayHelper::map(PaymentChannels::find()->all(), 'id', 'channel_name')), ['class' => 'col-xs-1 form-control input-sm', 'prompt' => 'Select Channel']),
                    'format' => 'html',

                ],
                [
                    'attribute' => 'Channel Memo',
                    'value' => function ($model) {
                        if ($model->payment_id)
                            return Html::decode($model->payment->channel_memo);
                    }
                ],
                [
                    'attribute' => 'amount',
                    'value' => function ($model) {
                        if ($model->amount < 0)
                            return '<span style="color:red">' . number_format($model->amount, 2) . '</span>';
                        else
                            return number_format($model->amount, 2);
                    },
                    'format' => 'html',
                    'filter' => false,
                ],
                [
                    'attribute' => 'balance_after',
                    'value' => function ($model) {
                        if ($model->balance_after < 0)
                            return '<span style="color:red">' . number_format(abs($model->balance_after), 2) . '</span>';
                        else if ($model->balance_after > 0)
                            return '<span style="color:green"><b>' . number_format(abs($model->balance_after), 2) . '</b></span>';
                        else
                            return number_format(abs($model->balance_after), 2);
                    },
                    'format' => 'html',
                    'filter' => false,
                ], [
                    'attribute' => 'outstanding_balance_after',
                    'value' => function ($model) {
                        if ($model->outstanding_balance_after < 0)
                            return '<span style="color:red">' . number_format(abs($model->outstanding_balance_after), 2) . '</span>';
                        else
                            return number_format(abs($model->outstanding_balance_after), 2);
                    },
                    'format' => 'html',
                    'filter' => false,
                ],
                [
                    'attribute' => 'Receipt',
                    'value' => function ($model) {
                        return $model->payment_id ? Html::a('<span class="pull-right"><i class="glyphicon glyphicon-print"></i></span>', Url::to(['/site/r', 'i' => base64_encode($model->payment_id)]), ['target' => '_blank']) : '';


                    },
                    'format' => 'raw',
                ]

            ],
        ]); ?>

    </div>
</div>

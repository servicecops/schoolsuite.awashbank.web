<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<div style="color:#F7F7F7">Student Information</div>

<div class="container">
<div class="letters">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Set Questions</h3></div>
    </div>

    <?php $form = ActiveForm::begin(['method'=>'Post','action' => ['create-online-test', 'id' => $id], 'options' => ['class' => 'formprocess', 'style' => 'width:100%']]); ?>
    <div class="body">
        <?php $i = 1;
        $answerId = 0;

        ?>
        <div id="submission_Contacts">

            <div style="padding:20px 0 10px 0;" class="row">
                <div class="col no-padding-left"><span style ="margin-right:5px" class="text-uppercase">Add Question</span><span class="add_contact_button" style="font-size: 22px;"><i
                                class="fa fa-plus-circle text-primary"></i></span></div>
                <div class="col no-padding-left"><span class="text-uppercase">&nbsp;</span></div>
                <div class="col-auto no-padding-left"><span style="padding-left: 24px;">&nbsp;</span></div>
            </div>

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-xm-6">

                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                </div>
                <div class="col-xm-6">
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
<?php
$script = <<< JS
$("document").ready(function () {
      var row = $i;
   console.log("this one");
   
    let thisBut = function (selector) {
        console.log("selectedButonValue is: " + selector.val());
    };
    
   
    let thisAnswerOption = function (selector) {

        let singleAswerContainer = $('#singleAnswer1');
        singleAswerContainer.find('.but[anserId=0]');
        let theAnswerId = selector.attr('answerId');
        let selectedOptionA = selector.val();
        console.log("answerId " + theAnswerId);
        console.log("rowId " + theAnswerId);
        console.log("selectedOptionA " + selectedOptionA);
        $('.but[answerId=' + theAnswerId + ']').val(selectedOptionA);
    };

    let thisSourceSelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

        if (selector.val() === 'singleselect') {

            $('#singleSelect' + rowId).show();
        } else {
            $('#singleSelect' + rowId).hide();
        }

        if (selector.val() === 'radioButton') {

            $('#singleAnswer' + rowId).show();
        } else {
            $('#singleAnswer' + rowId).hide();
        }
        if (selector.val() === 'checkbox') {

            $('#multipleAnswer' + rowId).show();
        } else {
            $('#multipleAnswer' + rowId).hide();
        }


    }

    var single_answer_question =function(rw){
        
        var html= '<div class="form-group" id="singleAnswer' + rw + '" >' +
                            '<div class="row">' +
                                '<div class="col-9">' +
                                    '<label for="otherField5">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test][' + rw + '][answer][answer11]" type="text" answerId="0" class="form-control w-100 answerOptions"   id="otherField1">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input type="radio" class="but" name="CoreTest[test][' + rw + '][correct_answer][correct_answer]" answerId="0" value="" style="margin-top: 45px;">' +
                                    '<label for="male">Correct</label><br>' +
                                '</div>' +
                                '<div class="col-9">' +
                                    '<label for="otherField6">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test][' + rw + '][answer][answer12]" type="text" class="form-control w-100 answerOptions" answerId="1"  id="otherField2">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input type="radio" class="but"  name="CoreTest[test][' + rw + '][correct_answer][correct_answer]" answerId="1" value="" style="margin-top: 45px;">' +
                                    '<label for="male">Correct</label><br>' +
                                '</div>' +
                                '<div class="col-9">' +
                                    '<label for="otherField7">Enter an answer choice</label>' +
                                   '<input name="CoreTest[test][' + rw + '][answer][answer13]" type="text" answerId="2" class="form-control w-100 answerOptions" id="otherField2">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input type="radio" class="but" name="CoreTest[test][' + rw + '][correct_answer][correct_answer]" answerId="2"' +
                                           'value="" style="margin-top: 45px;">' +
                                    '<label for="male">Correct</label><br>' +
                                '</div>' +
                               '<div class="col-9">' +
                                    '<label for="otherField8">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test][' + rw + '][answer][answer14]" type="text" answerId="3"' +
                                           'class="form-control w-100 answerOptions"  id="otherField2">' +
                                '</div>' +
                               '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                   ' <input type="radio" class="but" name="CoreTest[test][' + rw + '][correct_answer][correct_answer]" answerId="3" value="" style="margin-top: 45px;">' +
                                    '<label for="male">Correct</label><br>' +
                                '</div>'+
                            '</div>'
            +'</div>'
            return html;
    }
    var multi_answer_question =function(rw){
        
        var the_html= '<div class="form-group" id="multipleAnswer' + rw + '">' +
            
             '<div class="row">' +
                                '<div class="col-9">' +
                                    '<label for="otherField5">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test]['+ rw +'][answer][answer15]" type="text" class="form-control w-100 answerOptions" answerId="5"  id="otherField1">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input class="but" type="checkbox" name="CoreTest[test]['+ rw +'][correct_answer][correct_answer1]" answerId="5"  value="" style="margin-top: 45px;">' +
                                    '<label for="vehicle1"> Correct</label><br>' +
                                '</div>' +
                                '<div class="col-9">' +
                                    '<label for="otherField6">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test]['+ rw +'][answer][answer16]" type="text" class="form-control w-100 answerOptions" answerId="6"  id="otherField2">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input class="but" type="checkbox" name="CoreTest[test]['+ rw +'][correct_answer][correct_answer2]" answerId="6" value="" style="margin-top: 45px;">' +
                                    '<label for="vehicle1"> Correct</label><br>' +
                                '</div>' +
                                '<div class="col-9">' +
                                    '<label for="otherField7">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test]['+ rw +'][answer][answer17]" type="text" class="form-control w-100 answerOptions" answerId="7" id="otherField2">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input class="but" type="checkbox" name="CoreTest[test]['+ rw +'][correct_answer][correct_answer3]" answerId="7" value="" style="margin-top: 45px;">' +
                                    '<label for="vehicle1"> Correct</label><br>' +
                                '</div>' +
                                '<div class="col-9">' +
                                    '<label for="otherField8">Enter an answer choice</label>' +
                                    '<input name="CoreTest[test]['+ rw +'][answer][answer18]" type="text"class="form-control w-100 answerOptions" answerId="8"id="otherField2">' +
                                '</div>' +
                                '<div class="col-3">' +
                                    '<label for="otherField5"></label>' +
                                    '<input class="but" type="checkbox" name="CoreTest[test]['+ rw +'][correct_answer][correct_answer4]" answerId="8" value="" style="margin-top: 45px;">' +
                                    '<label for="vehicle1"> Correct</label><br>' +
                                '</div>' +
                            '</div>' +
            '</div>' 
            return the_html;
    }  
    var single_input_question =function(rw){
        
        var the_s_html= '<div class="form-group" id="singleSelect' + rw + '" >' +
            '<div class="row">' +
            '<div class="col-md-12">' +
            '<label for="otherField1">Enter an answer choice</label>' +
            '<input name="CoreTest[test][' + rw + '][answer][answer5]" type="text" class="form-control w-100 answerOptions" answerId="9"  id="otherField1">' +
            '</div>' +
            
            '</div>' +
            '</div>'
            return the_s_html;
    }
    


    
    //main div
    var row_html = function (rw) {
        var this_html = '<div style="padding-bottom:20px;" id="add_contact_row_' + rw + '" class="row"><div class="col-md-6">' +
            '<input  class="form-control" type="text" placeholder="Enter your question" name="CoreTest[test][' + rw + '][question]" >' +
            '</div><div class="col-md-5">' +
            '<div class="form-group">' +
            '<select name="CoreTest[test][' + rw + '][question_type]" class="form-control source_questions" id="question_type_selector'+rw+'" rowId=' + rw + '>' +
                '<option value="">-- Select Question Type--</option>' +
                '<option value="radioButton">Single Select</option>' +
                '<option value="checkbox">Multi Select</option>' +
                '<option value="singleselect">Single Input</option>'+
            '</select>' +
            '</div>' +
                
            '</div>' +

            '<div id ="answers_container'+rw+'"></div>'+
            '<div class="col-auto no-padding-left">' +
            '<span class="remove_contact_button" style="font-size:22px;" data-for="add_contact_row_' + rw + '"><i class="fa fa-minus-circle text-danger"></i></span>' +
            '</div>'+
            
            
          '</div>'
        return this_html;

    }
    
    let addQuestion = function(){
        row++;
        var new_row_html = row_html(row);
        $('div#submission_Contacts').append(new_row_html);
        let singleAswerContainer = $('#add_contact_row_' + row);
        singleAswerContainer.find(".source_questions").on("change", function () {
            console.log("messaf");
          //  thisSourceSelected($(this));
          let selector = $(this);
        let rowId = selector.attr('rowId'); 
        
        
        populateAnswers(rowId);
        
             singleAswerContainer.find(".answerOptions").on("change", function () {
                thisAnswerOption($(this));
    
            });
        });
       
        // singleAswerContainer.find(".but").on("change", function () {
        //     thisBut($(this));
        // });
        
    }
    
    let populateAnswers = function(row){
        let selector = $("#question_type_selector"+row);
       // console.log(selector );
        
        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        
         var answer_html ='';  
        if (selector.val() === 'singleselect') {
            answer_html =single_input_question(row);
            
        } else if(selector.val() === 'radioButton') {
            answer_html =single_answer_question(row);
        } else if(selector.val() === 'checkbox') {
           answer_html =multi_answer_question(row);
        }
        console.log($("#answers_container"+row));

        $("#answers_container"+row).html(answer_html);
       
    }
 
    $('.add_contact_button').on('click', function (e) {
       
        e.preventDefault();
        addQuestion();
        
    });


    $(".source_questions").on("change", function () {
        
        //thisSourceSelected($(this));
        let selector = $(this);
        let rowId = selector.attr('rowId');        
        populateAnswers(rowId);
    });
    
     $('#theevent').on("click", function () {
         console.log("init");
        firstBlock((this));
    });

    $(".answerOptions").on("change", function () {
        thisAnswerOption($(this));
    });

    $(".but").on("change", function () {
        thisBut($(this));
    });


    $('body').on('click', '.remove_contact_button', function (e) {
        e.preventDefault();
        $('div#' + $(this).attr('data-for')).remove();
    });
    
   addQuestion();
   
   
  
    
});

JS;
$this->registerJs($script);
?>



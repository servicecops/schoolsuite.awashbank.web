<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTest */

//$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-3"></div>

</div>
<div class="row card">
    <div class="col-md-9 pull-right">
        <h1><?= Html::encode($this->title) ?></h1>

        <?php if (\Yii::$app->user->can('r_test')): ?>


            <?= Html::a('<i class="fa fa-plus" style="color:#fff;"> Add Marks</i>', ['core-marks/marks', 'id' => $model->id], ['class' => 'btn  btn-primary']) ?>

            <?= Html::a('<i class="fa fa-file" style="color:#fff;"> Import Marks</i>', ['core-test/get-student-details', 'testId' => $model->id], ['class' => 'btn btn-primary']) ?>



        <?php endif; ?>


        <?php if (\Yii::$app->user->can('r_test')): ?>

            <?= Html::a('<i class="fa fa-edit" style="color:#fff;"> Update</i>', ['update', 'id' => $model->id], ['class' => 'btn bg-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" style="color:#fff;"> Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn bg-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>

        <?php endif; ?>


    </div>
    <div class="card-body">


        <div class="row">

            <div class="col border-right bg-gradient-light">
                <div class="card bg-gradient-light">
                    <div class="card-body">

                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Subject Name</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->subjectName->subject_name ?>
                            </div>


                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Description</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->test_description ?>
                            </div>


                        </div>

                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Total Marks</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->total_marks ?>
                            </div>


                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Date Created</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= date('Y-m-d h:m:s', strtotime($model->date_created)) ?>
                            </div>

                            <div class="col">
                                <b style="color: #000">Created By</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <span><?= $model->userName->firstname . ' ' . $model->userName->lastname ?></span>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="col bg-gradient-light">
                <div class="card bg-gradient-light">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Test Start Date:</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->start_date ?>
                            </div>


                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Test End Date</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->end_date ?>
                            </div>


                        </div>

                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Assessment Type</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= $model->assessmentType->assessment_type ?>
                            </div>
                        </div>

                        <hr class="style14">
                        <div class="row">

                            <div class="col">
                                <b style="color: #000">Test Grouping</b>
                            </div>
                            <div class="col" style="color: #21211f">,
                                <?php echo isset($model->testGrouping->name) ? $model->testGrouping->name : '' ?>
                            </div>

                        </div>

                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Date Modified</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <span><?= ($model->date_modified) ? $model->date_modified : ' Not yet modified' ?></span>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

use kartik\export\ExportMenu;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\Html;

/*
 * @var $dataProvider yii\data\ActiveDataProvider ;
 */


$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";
?>


<div class="float-right">
    <!--        data download or export widget-->

    <?php
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        'dropdownOptions' => [
            'label' => 'Export',
        ]

    ])
    ?>
</div>
<p style="float:right">
    <?= Html::a('Upload Marks', ['core-marks/upload-excel', 'id' =>$testId], ['class' => 'btn btn-sm btn-primary']) ?>

</p>

<p style="font-size:15px;color:grey;"><b>Note:</b> Only excel data sheets that follow the <b>template
        format provided by schoolPay ERP</b> will be allowed.
    <b>**And follow column Order !important** Click on Export button to get CSV version</p>
<b>Please <span style="color:red">DO NOT CHANGE</span> student Id listed below;</b>
<?= GridView::widget([
    'dataProvider'=> $dataProvider,
//    'filterModel' => $searchModel,
    'columns' => $columns,
    'resizableColumns'=>true,
//    'floatHeader'=>true,
    'options' => [
        'class' => 'bg-colorz',
    ],
    'floatHeaderOptions'=>['top'=>'50'],
    'responsive'=>true,
    'hover'=>true,
    'bordered' => false,
    'striped' => true,
]); ?>



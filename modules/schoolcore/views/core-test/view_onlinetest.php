<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTest */

//$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-nav">

    <?php $form = ActiveForm::begin(['method' => 'Post', 'action' => ['award-online-marks', 'id' => $id], 'options' => ['class' => 'formprocess', 'style' => 'width:100%']]); ?>

    <?php
    $i = 0;
    foreach ($data as $k => $v) {
        Yii::trace("dataProvider" . $k);
        Yii::trace($v);
        $i++;


        ?>
        <div>
            <div>
                <p><?php echo $i . '. ' ?><?php echo $v['question'] ?></p>
                <input type="hidden" name="CoreTest[answer][<?= $i; ?>][question_type]"  value="<?= $v['question_type'] ?>">
                <input type="hidden" name="CoreTest[answer][<?= $i; ?>][question_id]"  value="<?= $v['id'] ?>">
                <input type="hidden" name="CoreTest[answer][<?= $i; ?>][auto_assessment]"  value="<?= $v['auto_assessment'] ?>">
                <?php  if ($v['question_type'] === 'singleselect') {
                    ?>
                    <label for="male">Enter Answer</label><br>
                    <input type="text" name="CoreTest[answer][<?= $i; ?>][student_answer]" value="">
                <?php } else
                {?>


                <?php
                $correct_answer = json_decode($v['correct_answer']);
                foreach ($correct_answer as $a => $b) {
                    Yii::trace($b);

                    ?>
                    <input type="hidden" name="CoreTest[answer][<?= $i; ?>][correct-answer]" value="<?= $b ?>">

                    <?php

                } ?>
            </div>
            <div>
                <?php
                $answers = json_decode($v['answer_options']);




                foreach ($answers as $l => $o) {
                    Yii::trace("answerop" . $l);
                    Yii::trace($l);
                    Yii::trace($o);
                    if ($v['question_type'] === 'radioButton') {
                        ?>
                        <div>
                            <?php
                            if ($o) {
                                ?>
                                <input type="radio" name="CoreTest[answer][<?= $i; ?>][student_answer]"
                                       value="<?php echo $o ?>">
                                <label for="answer"><?php echo $o ?></label><br>
                                <?php
                            }

                            ?>

                        </div>
                        <?php

                    } else if ($v['question_type'] === 'checkbox') {
                        ?>

                        <div>
                            <?php
                            if ($o) {

                                ?>
                                <input type="checkbox"
                                       name="CoreTest[answer][<?= $i; ?>][student_answer][<?= $l; ?>]"
                                       value="<?php echo $o ?>">
                                <label for="answer"><?php echo $o ?></label><br>
                                <?php
                            }

                            ?>

                        </div>

                        <?php

                    } else {

                    }
                }
                }


                ?>
            </div>
        </div>
        <hr style="">

        <?php
    }
    ?>

    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>


<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Student Submissions';
$this->params['breadcrumbs'][] = $this->title;
?>
<p>Students to be marked</p>
<div class="core-school-index">

    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;<?= Html::encode($this->title) ?></h3></div>
    </div>


    <div class="row" >
        <?php

        Yii::trace($data);
        foreach($data as $k=>$v){

            ?>



            <div style ="width:30%;float:left; padding:10px; margin:10px;background: #fff; border-radius:5px;">

                <div>
                    <p><b><?= $searchModel->getAttributeLabel('student_id') ?></b>:  <?= ($v['student_code']) ? ($v['student_code']) : "--"  ?> </p>
                    <p><b><?= $searchModel->getAttributeLabel('test_description') ?></b>:   <?=   ($v['test_description']) ? ($v['test_description']) : "--"  ?></p>
                    <p><?= Html::a('View', ['core-test/teacher-award-marks', 'id' => $v['id'],'studentId'=>$v['student_id']], ['class' => 'btn btn-sm btn-primary'])?></p>
                </div>
                <div style="clear:both;"></div>
            </div>

        <?php  }

        ?>

    </div>



</div>

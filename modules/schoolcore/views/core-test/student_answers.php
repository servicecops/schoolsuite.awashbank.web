<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTest */

//$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-nav">
    <p>Quiz Results</p>
    <?php
    $i = 0;
    foreach ($data

             as $k => $v) {
        Yii::trace("dataProvider" . $k);
        Yii::trace($v);
        Yii::trace($data);
        $i++;


        ?>
        <div style="width: 100%;float:left">

            <p><?php echo $i . '. ' ?><?php echo $v['question'] ?></p>

            <div>  <?php if ($v['is_correct_answer'] === true) { ?>
                    <i class="fas fa-check text-primary"></i>
                <?php } else { ?>
                    <i class="fas fa-window-close text-danger"></i>


                <?php } ?>

            </div>
            <div style="margin-left:15px;background-color: rgba(192,192,192,0.23);width:100%;">


                <?php
                $answer_options = json_decode($v['answer_options']);
                foreach ($answer_options

                         as $a => $b) {
                    if ($b === $v['student_answer']) {
                        $yourAnswer = $b;
                    }


                    ?>
                    <div style="width:90%;margin-left:15px;; float:left">

                        <p style="background-color: rgba(233, 233, 233, 0.23);border: 1px solid #e3e3e317;"><?php echo $b ?>
                            <span style="margin-left:30%;"><?php if ($b === $v['student_answer']) echo ' Your answer'; ?></span>
                        </p>


                    </div>

                    <?php

                } ?>


            </div>


        </div>
        <div style="width: 100%;float:left;">
            <?php



            $correctAns = json_decode($v['correct_answer']);

            if(!$correctAns){
                echo "";
            }else{?>
                 <p>Correct Answers:</p>
            <?php foreach ($correctAns as $ansK => $ansV) { ?>

            <span><?php echo $ansV ?></span><br>
            <?php }

            }
            ?>


            <hr style="border-top: 1px dotted rgba(221,211,255,0.9);"/>
        </div>



        <?php
    }

    ?>

    <div style="clear:both"></div>

</div>

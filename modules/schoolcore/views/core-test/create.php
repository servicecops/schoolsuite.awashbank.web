<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchoolClass */

$this->title = 'Create Online Test';
$this->params['breadcrumbs'][] = ['label' => 'Create Online Test', 'url' => ['index']];

?>
<div class="core-school-class-create">

    <?= $this->render('create_online_test', [
        'model' => $model,
    ]) ?>

</div>

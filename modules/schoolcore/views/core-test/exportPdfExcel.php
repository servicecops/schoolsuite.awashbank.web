<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> School staff Info Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Subject name</th><th>Test Description</th><th>Start date</th><th>End date</th><th>Term Name</th><th>School name</th><th>Class description</th><th>total marks</th></tr>
        </thead>
        <tbody>
            <?php
            $no = 1;
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['subject_name']) ? $sinfo['subject_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['test_description']) ? $sinfo['test_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['start_date']) ? $sinfo['start_date'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['end_date']) ? $sinfo['end_date'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['term_description']) ? $sinfo['term_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['school_name']) ? $sinfo['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['class_description']) ? $sinfo['class_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['total_marks']) ? $sinfo['total_marks'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

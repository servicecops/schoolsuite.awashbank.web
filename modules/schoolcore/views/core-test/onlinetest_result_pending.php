<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTest */

//$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="core-student-view">


    <h1><?= Html::encode($this->title) ?></h1>


</div>
<nav class="menu-nav">
    <ul>
        <p href="">
            Results Pending teacher's approval
        </p>
    </ul>


    <div>
        <div>

            <?php $form = ActiveForm::begin([
                'action' => ['check-result', 'studentId'=>$studentId, 'testId'=>$id],
                'method' => 'post',
                'options' => ['class' => 'formprocess','enctype' => 'multipart/form-data']]); ?>

            <div class="form-group">
                <?= Html::submitButton('Check the answers', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</nav>

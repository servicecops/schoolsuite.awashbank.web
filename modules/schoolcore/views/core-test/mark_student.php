<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTest */

//$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Marks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-nav">

    <?php $form = ActiveForm::begin(['method' => 'Post', 'action' => ['teacher-award-marks', 'id' => $testId, 'studentId'=>$studentId], 'options' => ['class' => 'formprocess', 'style' => 'width:100%']]); ?>

    <?php
    $i = 0;
    foreach ($data as $k => $v) {
        Yii::trace("dataProvider" . $k);
        Yii::trace($v);
        $i++;


        ?>
        <div>
            <div>
                <p><?php echo $i . '. ' ?><?php echo $v['question'] ?></p>
                <input type="hidden" name="CoreTest[answer][<?= $i; ?>][answer_id]"  value="<?= $v['id'] ?>">
                <input type="hidden" name="CoreTest[answer][<?= $i; ?>][question_id]"  value="<?= $v['question_id'] ?>">

                <?php $studentanswer = json_decode($v['student_answer']);


                   foreach($studentanswer as $kA=>$vA){?>


                       <p>Student Answer:<br> <b><?php echo $vA?></b></p>
                <input type="radio"  name="CoreTest[answer][<?= $i; ?>][is_correct_answer]" value="correct">
                <label for="correct">Correct</label>
                <input type="radio"  name="CoreTest[answer][<?= $i; ?>][is_correct_answer]" value="incorrect">
                <label for="incorrect">Incorrect</label><br>


               <label for="correct_answer">Correct Answer</label>
                <input type="text" style="width:100%" name="CoreTest[answer][<?= $i; ?>][correct_answer]" placeholder="Type correct answer "/>
                  <?php }

                   ?>

            </div>

        </div>
        <hr style="">

        <?php
    }
    ?>
    <div style="clear: both"></div>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>
    <div style="margin-bottom: 20px;"></div>
</div>

<?php ActiveForm::end(); ?>


<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use Yii;
use app\modules\schoolcore\models\CoreGuardian;
use app\modules\schoolcore\models\CoreGuardianSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoreGuardianController implements the CRUD actions for CoreGuardian model.
 */
class CoreGuardianController extends BaseController
{
    /**
     * {@inheritdoc}
     */

//    Allow access for these actions by authenticated users
    public function beforeAction($action) {


        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {

            Yii::$app->user->loginRequired();

        }

//something code right here if user valid

        return true;



    }
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CoreGuardian();

        if ($model->load(Yii::$app->request->post()) ) {
            $post =$model->load(Yii::$app->request->post());

            $transaction = CoreGuardian::getDb()->beginTransaction();
            try{
                //$newId =$this->saveUser();
                $model->created_by =Yii::$app->user->identity->getId();

                $model->save(false);
                $transaction->commit();
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel( $model->id);
                $modeler = $schoolData['models'];
                $attributes =$schoolData['attributes'];
                return $this->render('@app/views/common/modal_view', [
                    'model' => $modeler,
                    'attributes'=>$attributes
                ]);
            } catch (\Exception $e){
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: '.$error, 'CREATE  GUARDIAN ROLLBACK');
            }

        }



        $formConfig = [
            'model' => $model,
            'formTitle'=>$this->createModelFormTitle(),
        ];

        return  $this->modelForm($model, $formConfig );
    }






    /**
     * Updates an existing CoreGuardian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        $searchModel = $this->newSearchModel();
//        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
//        $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
//        $modeler = $schoolData['models'];
//        $attributes =$schoolData['attributes'];
//        return $this->render('@app/views/common/modal_view', [
//            'model' => $modeler,
//            'attributes'=>$attributes
//        ]);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//
//            return $this->render('@app/views/common/modal_view', [
//                'model' =>  $model->id,
//                //'attributes'=>$attributes
//            ]);
//           // return $this->redirect(['@app/views/common/modal_view', 'id' => $model->id]);
//        }
//        $formConfig = [
//            'model' => $model,
//            'formTitle'=>$this->createModelUpdateFormTitle(),
//        ];
//
//        return  $this->updateForm($model, $formConfig );
//
//    }

    public function createModelUpdateFormTitle()
    {
        return "Update Guardian";
    }

    /**
     * Deletes an existing CoreGuardian model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreGuardian model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreGuardian the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreGuardian::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function newModel()
    {
        return new CoreGuardian();
    }

    public function newSearchModel()
    {
        return new CoreGuardianSearch();
    }

    public function createModelFormTitle()
    {
        return "Create Guardian";
    }

}

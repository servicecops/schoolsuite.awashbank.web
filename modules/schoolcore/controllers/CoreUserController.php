<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use Yii;
use app\modules\schoolcore\models\CoreUser;
use app\modules\schoolcore\models\CoreUserSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoreStudentController implements the CRUD actions for CoreStudent model.
 */
class CoreUserController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

//    /**
//     * Lists all CoreStudent models.
//     * @return mixed
//     */
//    public function actionIndex()
//    {
//        $searchModel = $this->newSearchModel();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    /**
     * Displays a single CoreStudent model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }


    /**
     * Updates an existing CoreUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }

    /**
     * Deletes an existing CoreStudent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreStudent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function newModel()
    {
        // TODO: Implement newModel() method.
        return new CoreUser();
    }

    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new CoreUserSearch();
    }

    public function createModelFormTitle()
    {
        return 'New User';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CoreUser();

        if ($model->load(Yii::$app->request->post()) ) {
            $post =$model->load(Yii::$app->request->post());

            $transaction = CoreUser::getDb()->beginTransaction();
            try{
                //$newId =$this->saveUser();
                $model->created_by =Yii::$app->user->identity->getId();

                //$user->user_level = 1;
                $model->setPassword($post['password']);
                $model->save(false);
                $transaction->commit();
               // return $this->redirect(['view', 'id' => $model->id]);
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel( $model->id);
                $modeler = $schoolData['models'];
                $attributes =$schoolData['attributes'];
                return $this->render('@app/views/common/modal_view', [
                    'model' => $modeler,
                    'attributes'=>$attributes
                ]);
            } catch (\Exception $e){
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: '.$error, 'CREATE  USER ROLLBACK');
            }

        }



        $formConfig = [
            'model' => $model,
            'formTitle'=>$this->createModelFormTitle(),
        ];

        return  $this->modelForm($model, $formConfig );
    }



//    public function actionIndex()
//    {
//
//        $viewConfig = [
//            $searchModel = $this->newSearchModel(),
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams),
//        ];
//
//        return  $this->modelView($searchModel, $viewConfig );
//    }

}

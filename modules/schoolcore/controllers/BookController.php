<?php

namespace app\modules\schoolcore\controllers;

use app\models\ImageBank;
use app\models\Model;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\BookCopy;
use app\modules\schoolcore\models\BookCopySearch;
use http\Exception;
use Yii;
use app\modules\schoolcore\models\Book;
use app\modules\schoolcore\models\BookSearch;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new BookCopySearch();
        $dataProvider = $searchModel->searchCopy(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();
        $modelBookCopies = [new BookCopy];
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $modelBookCopies = Model::createMultiple(BookCopy::classname());
            Model::loadMultiple($modelBookCopies, Yii::$app->request->post());



            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelBookCopies) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelBookCopies as $modelBookCopies) {
                            $modelBookCopies->book_id = $model->id;
                            if (! ($flag = $modelBookCopies->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelBookCopies' => (empty($modelBookCopies)) ? [new BookCopy] : $modelBookCopies
        ]);
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */



    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelBookCopies = BookCopy::find()->where(['book_id'=>$id])->all();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $oldIDs = ArrayHelper::map($modelBookCopies, 'id', 'id');

            $modelBookCopies = Model::createMultiple(BookCopy::classname(), $modelBookCopies);
            Model::loadMultiple($modelBookCopies, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelBookCopies, 'id', 'id')));

            print_r($deletedIDs);
            // exit();


            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($modelBookCopies) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs)) {
                            BookCopy::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelBookCopies as $modelBookCopies) {
                            $modelBookCopies->book_id = $model->id;
                            if (! ($flag = $modelBookCopies->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }


        } else {
            return $this->render('update', [
                'model' => $model,
                'modelBookCopies' => (empty($modelBookCopies)) ? [new BookCopy] : $modelBookCopies,
                //'id' => $id,
            ]);
        }
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionBookCover($id)
    {


        $model = $this->findModel($id);

        $request = Yii::$app->request;

        $model->scenario = 'photo';
        $imageBank = null;
        $imageBank = $model->book_cover ? ImageBank::find()->where(['id' => $model->book_cover])->limit(1)->one() : new ImageBank();
        try {
            if ($imageBank->load(Yii::$app->request->post())) {
                $data = $_POST['ImageBank']['image_base64'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $imageBank->image_base64 = $data;
                $imageBank->description = 'Book Cover Uploaded: ID - ' . $model->id;
                if ($imageBank->save()) {
                    $model->book_cover = $imageBank->id;
                    $model->save(false);
                    Logs::logEvent("Book Cover Photo: " . $model->book_title, null, $model->id);
                    $this->redirect(['view', 'id' => $model->id]);
                }
            }
            $res = ['imageBank' => $imageBank, 'model' => $model];
            return $this->render('book_cover', $res);
        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to upload book cover: " . $model->book_title . "(" . $model->id . ")", $error, $model->id);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/schoolcore/core-student/error');
        }
    }
}

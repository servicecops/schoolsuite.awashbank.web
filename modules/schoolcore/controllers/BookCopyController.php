<?php

namespace app\modules\schoolcore\controllers;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use app\modules\schoolcore\models\BookCopy;
use app\modules\schoolcore\models\BookCopySearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookCopyController implements the CRUD actions for BookCopy model.
 */
class BookCopyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BookCopy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookCopySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BookCopy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BookCopy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BookCopy();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BookCopy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BookCopy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BookCopy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BookCopy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BookCopy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionIssueBook($id)
    {

        $model = $this->findModel($id);
        $model->scenario = 'issue_book';
        if ($model->load(Yii::$app->request->post()) ) {
//            $model->student_id=90;
            $model->available =false;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('issue_book', [
            'model' => $model,
        ]);
    }

    public function actionReturnBook($id)
    {

        $model = $this->findModel($id);
        $model->scenario = 'return_book';
        if ($model->load(Yii::$app->request->post()) ) {
//            $model->student_id=90;
            $model->available =true;
            $model->student_id=null;
            $model->issue_date=null;
            $model->return_date=null;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('return_book', [
            'model' => $model,
        ]);
    }


    protected function findStudentModel(){
        if (isset($_POST['student_id'])) {
            $action = $_POST['student_id'];
            if ($action !=null) {
                $status = $action;
                $model = CoreStudent::findOne(['first_name'=>$status]);
                return $model->id;
            } else {
                throw new NotFoundHttpException('The requested page does not exist');            }
        }
//        if (($model = CoreStudent::findOne(['first_name'=>$status])) !=null){
//            return $model->id;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist');
//        }
    }

    public function actionStudentList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, first_name,last_name,student_code,class_id')
                ->from('core_student')
                ->where(['ilike', 'first_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = [['id' => $id, 'text' => CoreStudent::find($id)->first_name]];
        }
        return $out;
    }

    public function actionIssuedBooks()
    {
        $searchModel = new BookCopySearch();
        $dataProvider = $searchModel->searchIssued(Yii::$app->request->queryParams);

        return $this->render('issued-books', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

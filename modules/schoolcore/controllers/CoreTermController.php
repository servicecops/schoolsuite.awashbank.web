<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreTest;
use kartik\mpdf\Pdf;
use Yii;
use app\modules\schoolcore\models\CoreTermSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoreTermController implements the CRUD actions for CoreTerm model.
 */

if (!Yii::$app->session->isActive){
    session_start();
}
class CoreTermController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Deletes an existing CoreTerm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreTerm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreTerm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($id)
//    {
//        if (($model = CoreTerm::findOne($id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException('The requested page does not exist.');
//    }

    /**
     * @return CoreTerm
     */
    public function newModel()
    {
        $model = new CoreTerm();
        $model->created_by =Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return CoreTermSearch
     */
    public function newSearchModel()
    {
        $searchModel = new CoreTermSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create A Term';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;

        $model = new CoreTerm();
        $searchModel = $this->newSearchModel();

        if ($model->load(Yii::$app->request->post()) ) {
            $post =$model->load(Yii::$app->request->post());

            $transaction = CoreTerm::getDb()->beginTransaction();
            try{
                //$newId =$this->saveUser();
                if(\app\components\ToWords::isSchoolUser()){
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $model->created_by =Yii::$app->user->identity->getId();;

                $data = Yii::$app->request->post();
                if(isset($data['CoreTerm']['term_type']) && (strcasecmp('SEMESTER', $data['CoreTerm']['term_type']) == 0)){
                    $model->term_type_detail = $data['CoreTerm']['semester_type_detail'];
                }
                else if (isset($data['CoreTerm']['term_type']) && (strcasecmp('QUARTER', $data['CoreTerm']['term_type']) == 0)){
                    $model->term_type_detail = $data['CoreTerm']['quarter_detail'];
                }

                if(!$model->save(true)) {
                    Yii::trace($model->errors);
                    $template = "create";
                    $res=['model' => $model,];
                    return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
                }

                $transaction->commit();
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel( $model->id);
                $modeler = $schoolData['models'];
                $attributes =$schoolData['attributes'];
                $template='@app/views/common/modal_view';
                $res=[
                    'model' => $modeler,
                    'attributes'=>$attributes
                ];
                return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

                // return $this->redirect (['view', 'id' => $model->id]);
            } catch (\Exception $e){

                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: '.$error, 'CREATE  USER ROLLBACK');
            }

        }


        $template = "create";
        $res=['model' => $model,];
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();

        $updateData = $searchModel->viewModel($id);
        $model = $updateData['models'];
        $attributes =$updateData['attributes'];

        if ($model->load(Yii::$app->request->post()) ) {
            $model->date_updated =date('Y-m-d H:i:s');
            $data = Yii::$app->request->post();
            if(isset($data['CoreTerm']['term_type']) && (strcasecmp('SEMESTER', $data['CoreTerm']['term_type']) == 0)){
                $model->term_type_detail = $data['CoreTerm']['semester_type_detail'];
            }
            else if (isset($data['CoreTerm']['term_type']) && (strcasecmp('QUARTER', $data['CoreTerm']['term_type']) == 0)){
                $model->term_type_detail = $data['CoreTerm']['quarter_detail'];
            }

            if(!$model->save(true)) {
                $template = "update";
                $res=['model' => $model,'searchModel'=>$searchModel,'attributes'=>$attributes,
                ];
                return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
            }

            $template = "@app/views/common/modal_view";
            $res=['model' => $model,'searchModel'=>$searchModel,'attributes'=>$attributes,
                ];
            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

//            return $this->render('@app/views/common/modal_view', [
//                'model' => $model,
//                'attributes' => $attributes
//            ]);
        }

//        $formConfig = [
//            'model' => $model,
//            'formTitle'=>$this->createModelUpdateFormTitle(),
//        ];
//
//        return  $this->updateForm($model, $formConfig );

        $template = "update";
        $res=['model' => $model,'searchModel'=>$searchModel,'attributes'=>$attributes,
            ];
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }



    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->searchNewIndex(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionLists($id = 0)
    {
        if(!$id) $id = 0;
        $countTerms = CoreTerm::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'term_name', '__ARCHIVE__'])
            ->count();

        $terms = CoreTerm::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'term_name', '__ARCHIVE__'])
            ->orderBy('term_name')
            ->all();

        Yii::trace($terms);

        if ($countTerms > 0) {
            echo "<option value=''> Select Term </option>";
            foreach ($terms as $myTerm) {
                echo "<option value='" . $myTerm->id . "'>" . $myTerm->term_description . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreTermSearch();
        $dataProvider = $searchModel->searchNewIndex(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }
    /**
     * Displays a single CoreSchool model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_term', ['sch' => $model->id])) {
        // Yii::trace($module);
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
                'model' => $model  ]) :
                $this->render('view', ['model' => $model]);


        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }

    /**
     * Finds the CoreStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreTerm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreTerm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionExportToPdf($model)
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial($data['exportFile'], ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => [$data['title'] . '||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

}

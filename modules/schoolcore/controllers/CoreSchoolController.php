<?php

namespace app\modules\schoolcore\controllers;

use app\components\ToWords;
use app\controllers\BaseController;
use app\models\ImageBank;
use app\modules\logs\models\Logs;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\SchoolAccountTransactionHistorySearch;
use app\modules\schoolcore\models\AwashBranches;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreSchoolSearch;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\County;
use app\modules\schoolcore\models\District;
use app\modules\schoolcore\models\Parish;
use app\modules\schoolcore\models\SchoolCampuses;
use app\modules\schoolcore\models\SchoolCampusSearch;
use app\modules\schoolcore\models\SchoolChannel;
use app\modules\schoolcore\models\SchoolModules;
use app\modules\schoolcore\models\SchoolSectionClassAssociation;
use app\modules\schoolcore\models\SchoolSections;
use app\modules\schoolcore\models\SubCounty;
use app\modules\workflow\controllers\WorkflowController;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\base\InvalidArgumentException;
use yii\base\UserException;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CoreSchoolController implements the CRUD actions for CoreSchool model.
 */

if (!Yii::$app->session->isActive) {
    session_start();
}

class CoreSchoolController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    /*  public function behaviors()
      {
          return [
              'verbs' => [
                  'class' => VerbFilter::className(),
                  'actions' => [
                     // 'delete' => ['POST'],
                  ],
              ],
          ];
      }*/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['template-list', 'schoollist'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreSchool models.
     * @return mixed
     */
//    public function actionIndex()
////    {
////        $searchModel = new CoreSchoolSearch();
////        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
////        return $this->render('index', [
////            'searchModel' => $searchModel,
////            'dataProvider' => $dataProvider,
////        ]);
////    }

    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreSchoolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }

    /**
     * Displays a single CoreSchool model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_sch', ['sch' => $model->id]) || !Yii::$app->user->can('non_student')) {
            $accounts = $this->getSchoolAccounts($id);
            Yii::trace('Number of accounts is ' . count($accounts));
            $sections = $this->getSections($id);
            $module = SchoolModules::find()->where(['school_id' => $id])->all();
            $campuses = SchoolCampuses::find()->where(['school_id' => $id])->all();
            Yii::trace($campuses);
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
                'model' => $model, 'accounts' => $accounts, 'sections' => $sections, 'module' => $module,'campuses'=>$campuses]) :
                $this->render('view', ['model' => $model, 'accounts' => $accounts, 'sections' => $sections, 'module' => $module,'campuses'=>$campuses]);


        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }

    public function actionAllowReversal($id,$allow)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('own_sch')) {

            $model->allow_payment_reversal = $allow;
            $model->save(false);

            //log who changed
            Logs::logActionEvent("Modified by school user to  : " . $allow ." by ".Yii::$app->user->identity->getId() , null,"AllowReversal", null);
            Yii::$app->session->setFlash('Reversal', "Payment Reversal Modified  successfully");
            $this->redirect(['view', 'id' => $model->id]);

        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }

    private function getSchoolAccounts($schId)
    {
        $accounts = (new Query())->from('core_bank_account_details ba')->select(['ba.id as bid', 'nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId]);
        return $accounts->all();
    }


    private function getBanks()
    {
        $banks = (new Query())->from('core_nominated_bank')->select(['id', 'bank_name']);
        return $banks->all();
    }

    private function validateBankInfo($bank_accounts)

    {

        $error = null;

        foreach ($bank_accounts as $k => $v) {
            if (!$v) {
                $error = $k;

                Yii::trace($error);
            }
        }
        return $error;

    }

    /**
     * Finds the CoreSchool model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreSchool the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($id)
//    {
//        if (($model = CoreSchool::findOne($id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException('The requested page does not exist.');
//    }

    private function getSchoolBanks($schId)
    {
        $accounts = (new Query())->from('core_bank_account_details ba')->select(['nb.id', 'nb.id as bank_id', 'nb.bank_name'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId])
            ->distinct();
        return $accounts->all();
    }

    /**
     * Deletes an existing CoreSchool model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = CoreSchool::find()->where(['school_code' => $id])->limit(1)->one();
        if (Yii::$app->user->can('rw_sch', ['sch' => $model->id]) && Yii::$app->user->can('schoolsuite_admin') && Yii::$app->user->can('del_sch')) {

            try {
                Yii::$app->db->createCommand("select drop_school_details(:school_id)", [
                    ':school_id' => $id
                ])->execute();

                Logs::logEvent("Delete school: " . $model->school_name, null, null);
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Delete Student: " . $model->school_name . "(" . $model->school_code . ")", $error, null);
                Yii::$app->session->setFlash('actionFailed', $error);
                return Yii::$app->runAction('/schoolcore/core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to delete school.');
        }
    }

    /**
     * @return CoreSchool
     */
    public function newModel()
    {
        $model = new CoreSchool();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    public function createModelFormTitle()
    {
        return 'Create A School';
    }

    private function saveAccounts($posted_banks, $sch_id)
    {
        foreach ($posted_banks as $k => $v) {
            $bankAcc = new CoreBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateStudent()
    {

        $model = new CoreStudent();
        $request = Yii::$app->request;
        Yii::trace($model->load(Yii::$app->request->post()));
        if ($model->load(Yii::$app->request->post())) {

            $post = $model->load(Yii::$app->request->post());
            Yii::trace($post);
            $transaction = CoreStudent::getDb()->beginTransaction();
            try {
                //$newId =$this->saveUser();
                $model->created_by = Yii::$app->user->identity->getId();
                $model->save();


                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel($model->id);
                $modeler = $schoolData['models'];
                $attributes = $schoolData['attributes'];
                return $this->render('@app/views/common/modal_view', [
                    'model' => $modeler,
                    'attributes' => $attributes
                ]);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE  School ROLLBACK');
            }


            //$res = ['model' => $model,  'banks' => $banks, 'bank_info_error' => $bank_info_error];
            //turn ($request->isAjax) ? $this->renderAjax('create') : $this->render('create');

        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
    }

    /**
     * @return CoreSchoolSearch
     */
    public function newSearchModel()
    {
        $searchModel = new CoreSchoolSearch();
        return $searchModel;
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $theSearchModel = $this->newSearchModel();
        $allData = $theSearchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $searchModel = $allData['searchModel'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionSchoollist($q = null, $id = null, $bank = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('core_school')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand(Yii::$app->db); //Search schools via backup db
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }

    public function actionActiveSchoollist($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('core_school')
                ->where(['active' => true])
                ->andWhere(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }

//filter school

    public function actionSchList($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('core_school')
                ->where(['ilike', 'school_name', $q])
                ->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find($id)->school_name];
        }
        return $out;
    }

//    county

    public function actionDistList($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, district_name AS text')
                ->from('district')
                ->where(['ilike', 'district_name', $q])
                ->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => District::find($id)->district_name];
        }
        return $out;
    }


    public function actionDistricts($id)
    {
        $districts = District::find()
            ->where(['region_id' => $id])
            ->all();

        if (!empty($districts)) {
            echo '<option value="">Select district</option>';
            foreach ($districts as $district) {
                echo "<option value='" . $district->id . "'>" . $district->district_name . "</option>";
            }
        } else {
            echo "<option>No District found</option>";
        }

    }
    public function actionBranches($id)
    {
        $branches = AwashBranches::find()
            ->where(['branch_region' => $id])
            ->all();
//        if (Yii::$app->user->can('view_by_branch')) {
//
//            $branches = AwashBranches::find()
//                ->where(['id' => Yii::$app->user->identity->branch_id])
//                ->all();
//        }
//
//        if (Yii::$app->user->can('view_by_region')) {
//
//            $branches = AwashBranches::find()
//                ->where(['branch_region' => Yii::$app->user->identity->region_id])
//                ->all();
//        }

        if (!empty($branches)) {
            echo '<option value="">Select branch</option>';
            foreach ($branches as $branche) {
                echo "<option value='" . $branche->id . "'>" . $branche->branch_name . "</option>";
            }
        } else {
            echo "<option>No Branch found</option>";
        }

    }

    public function actionCountyLists($id)
    {
        $counties = County::find()
            ->where(['district_id' => $id])
            ->all();

        if (!empty($counties)) {
            echo '<option value="">choose county</option>';
            foreach ($counties as $county) {
                echo "<option value='" . $county->id . "'>" . $county->county_name . "</option>";
            }
        } else {
            echo "<option>No county found</option>";
        }

    }

    public function actionSubLists($id)
    {
        $subcounties = SubCounty::find()
            ->where(['county_id' => $id])
            ->all();

        if (!empty($subcounties)) {
            echo '<option value="">Select sub-County</option>';
            foreach ($subcounties as $subcounty) {
                echo "<option value='" . $subcounty->id . "'>" . $subcounty->sub_county_name . "</option>";
            }
        } else {
            echo "<option>No sub county found</option>";
        }

    }

    public function actionParishLists($id)
    {
        $parishes = Parish::find()
            ->where(['sub_county_id' => $id])
            ->all();

        if (!empty($parishes)) {
            echo '<option value="">Select Parish</option>';
            foreach ($parishes as $parish) {
                echo "<option value='" . $parish->id . "'>" . $parish->parish_name . "</option>";
            }
        } else {
            echo "<option>No Parishes found</option>";
        }

    }

    public function actionLogo($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('edit_school_logo')  || Yii::$app->user->can('own_sch', ['sch' => $model->id])) {
            throw new ForbiddenHttpException('No permissions to create or update school logo.');
        }



//        if ( || Yii::$app->user->can('edit_sch_logo')) {
            $request = Yii::$app->request;
            $model->scenario = 'photo';
            $imageBank = null;
            $imageBank = $model->school_logo ? ImageBank::find()->where(['id' => $model->school_logo])->limit(1)->one() : new ImageBank();
            try {
                if ($imageBank->load(Yii::$app->request->post())) {
                    $data = $_POST['ImageBank']['image_base64'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $imageBank->image_base64 = $data;
                    $imageBank->description = 'School Logo: ID - ' . $model->id;
                    if ($imageBank->save()) {
                        $model->school_logo = $imageBank->id;
                        $model->save(false);
                        Logs::logEvent("School logo changed: " . $model->school_name . "(" . $model->school_code . ")", null, null);
                        $this->redirect(['view', 'id' => $model->id]);
                    }
                }
                $res = ['imageBank' => $imageBank, 'model' => $model];
                return ($request->isAjax) ? $this->renderAjax('_sch_logo_croppie', $res) : $this->render('_sch_logo_croppie', $res);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to upload school photo: " . $model->school_name . "(" . $model->school_code . ")", $error, null);
                Yii::$app->session->setFlash('actionFailed', $error);
                return Yii::$app->runAction('/schoolcore/core-student/error');
            }


    }


    private function updateSchAccounts($sch_id, $accounts, $posts)
    {
        Yii::trace('Db accounts is ' . print_r($accounts, true));
        Yii::trace('Posted accounts is ' . print_r($posts, true));
        $db_accounts = array_column($accounts, 'bid'); //id is in bid
        $post_accounts = array_column($posts, 'id');
        $diff_del = array_diff($db_accounts, $post_accounts);
        if (!empty($diff_del)) {
            foreach ($diff_del as $v) {
                $schAcc = CoreBankAccountDetails::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                $schAcc->delete();
            }
        }

        foreach ($posts as $k => $v) {
            $bankAcc = isset($v['id']) ? CoreBankAccountDetails::find()->where(['id' => $v['id']])->limit(1)->one() : new CoreBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }


    public function actionSchoolDetail()
    {
        if (isset($_POST['expandRowKey'])) {
            $model = CoreSchool::findOne($_POST['expandRowKey']);
            return $this->renderPartial('_expand_row', ['model' => $model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    public function actionSchools($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('school_account_id as id, school_name AS text')
                ->from('core_school')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['school_account_id' => $id])->school_name];
        }
        return $out;
    }


    public function actionAccounthistory()
    {
        Yii::trace("in");
        $request = Yii::$app->request;
        if (Yii::$app->user->can('payments_received_list')) {
            $searchModel = new SchoolAccountTransactionHistorySearch();
            if (ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $model = $this->findModel($schId);
                $data = $searchModel->search($model->school_account_id, Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->search('', Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }

            return ($request->isAjax) ? $this->renderAjax('account_histories', [
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('account_histories', ['dataProvider' => $dataProvider,
                    'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }


    public function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfSchoolTransactionHistory', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'School transaction history',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['School transaction history||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionChannels($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_channel_assign', ['sch' => $model->id])) {
            $request = Yii::$app->request;
            $available = array();
            $assigned = array();
            $channels = PaymentChannels::find()->all();

            foreach ($model->schChannels as $chn) {
                $assigned[$chn->payment_channel] = $chn->paymentChannel->channel_name;
            }

            foreach ($channels as $chn) {
                if (!isset($assigned[$chn->id])) {
                    $available[$chn->id] = $chn->channel_name;
                }
            }

            return ($request->isAjax) ? $this->renderAjax('channels', ['model' => $model,
                'available' => $available, 'assigned' => $assigned]) :
                $this->render('channels', ['model' => $model, 'available' => $available,
                    'assigned' => $assigned]);
        } else {
            throw new ForbiddenHttpException('No permissions to perform this action.');
        }
    }

    public function actionAssign($id, $action)
    {
        $error = [];
        if ($channels = $_POST['selected']) {
            if ($action == 'assign') {
                foreach ($channels as $i => $chn) {
                    try {
                        $item = new SchoolChannel();
                        $item->institution = $id;
                        $item->payment_channel = $chn;
                        $item->save(false);
                    } catch (\Exception $exc) {
                        $error[] = $exc->getMessage();
                    }
                }
            } else if ($action == 'delete') {
                Yii::trace(json_encode($channels), 'asign /delete channels');
                foreach ($channels as $i => $chn) {
                    try {
                        Yii::trace(json_encode($chn), 'delete channels');
                        $item = SchoolChannel::find()->where(['payment_channel' => $chn, 'institution' => $id])->one();
                        $item->delete();
                    } catch (\Exception $exc) {
                        $error[] = $exc->getMessage();
                    }
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$this->actionChannelSearch($id, 'available', ''),
            $this->actionChannelSearch($id, 'assigned', ''),
            $error];
    }

    public function actionChannelSearch($id, $target, $term = '')
    {
        $channels = PaymentChannels::find()->all();
        $model = $this->findModel($id);
        $available = [];
        $assigned = [];

        foreach ($model->schChannels as $chn) {
            $assigned[$chn->payment_channel] = $chn->paymentChannel->channel_name;
        }

        foreach ($channels as $chn) {
            if (!isset($assigned[$chn->id])) {
                $available[$chn->id] = $chn->channel_name;
            }
        }

        $result = [];
        $var = ${$target};
        if (!empty($term)) {
            foreach ($var as $i => $descr) {
                if (stripos($descr, $term) !== false) {
                    $result[$i] = $descr;
                }
            }
        } else {
            $result = $var;
        }

        return Html::renderSelectOptions('', $result);
    }

    public function actionFeeslists($id = 0)
    {
        if (!$id) $id = 0;
        $db = Yii::$app->db;
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $fees = (new Query())->select(['id', 'description'])
            ->from('institution_fees_due')
            ->where(['school_id' => $id])
            ->andWhere(['recurrent' => false])
            ->orderBy('description ASC')
            ->all($db);
        return ArrayHelper::map($fees, 'id', 'description');
    }


    /**
     * Creates a new SchoolInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_sch') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a school.');
        }

        $connection = Yii::$app->db;

        $request = Yii::$app->request;
        $model = new CoreSchool();
        $model->default_part_payment_behaviour = false;
        $model->active = true;
        $model->enable_bank_statement = true;

        $sp_modules = (new Query())->from('suite_modules')->orderBy('module_name')->all();
        $banks = $this->getBanks();
        $bank_info_error = [];
        $model_error = '';

        // Yii::trace($banks);

        try {
            if ($model->load(Yii::$app->request->post())) {

//
//                    //TODO: Begin transaction here
                $transaction = $connection->beginTransaction();
                $foundSchool = CoreSchool::findOne(['school_name' => strtoupper(trim($model->school_name)),
                    'phone_contact_1' => $model->phone_contact_1]);
                if ($foundSchool)
                    throw new Exception('School ' . $model->school_name . ' already exists');

                $model->external_school_code = strtoupper(trim(str_replace(' ', '', $model->external_school_code)));
                $existingCode = CoreSchool::findOne(['external_school_code' => strtoupper(trim($model->external_school_code))]);
                if ($existingCode) {
                    throw new Exception('School Code  ' . $model->external_school_code . ' already exists');

                }
                //  $model->save(true);
               $saved = $model->save(true);

                if ($saved) {
                    Logs::logEvent("Created New School: " . $model->school_name, null, null);


//                    if ($posted_banks = $_POST['bank_name']) {
//                        Yii::trace( $posted_banks);
//
//
//                        $this->saveAccounts($posted_banks, $model->id);
//                    }

                    $schAssoc = $_POST['sp_modules'];
                    foreach ($schAssoc as $v) {
                        $schModule = new SchoolModules();
                        $schModule->school_id = $model->id;
                        $schModule->module_id = $v;
                        $schModule->save('false');
                    }

                    Yii::trace($model->id);

                    $connection->createCommand("select associate_school_with_default_payment_channels(:school_id)", [
                        ':school_id' => $model->id
                    ])->execute();
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id], 200);

                }
            }
        } catch (\Exception $e) {

            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::trace("Failed to add new School: ", $model_error);
            Logs::logEvent("Failed to add new School: ", $model_error, null);
            Yii::$app->session->setFlash('actionFailed', "<div style='color: red'><i class='fa fa-remove'></i>&nbsp Creating School Error: " . $e->getMessage() . "</div>");


        }

        $res = ['model' => $model, 'sp_modules' => $sp_modules, 'accounts' => $this->getSchoolAccounts($model->id), 'model_error' => $model_error, 'banks' => $banks, 'bank_info_error' => $bank_info_error];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }

    public function actionExportToPdf($model)
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial($data['exportFile'], ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => [$data['title'] . '||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }


    /**
     * Finds the SchoolInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreSchool the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreSchool::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing SchoolInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        Yii::trace($model);
//
//        if (!Yii::$app->user->can('rw_sch', ['sch' => $model->id]) && !Yii::$app->user->can('schoolsuite_admin')) {
//            throw new ForbiddenHttpException('No permissions to update or create a school.');
//        }
//        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
//
//
//        $request = Yii::$app->request;
//       // $sp_modules = (new \yii\db\Query())->from('suite_modules')->orderBy('module_name')->all();
//        $banks = $this->getBanks();
//        $accounts = $this->getSchoolAccounts($id);
//      //  $sch_assoc = (new \yii\db\Query())->select(['module_id'])->from('school_module_association')->where(['school_id' => $id])->all();
//      //  $sch_modules = array_column($sch_assoc, 'module_id');
//
//        $school_sections = $this->getSections($id);
//        $section_info_error = '';
//
//        $bank_info_error = [];
//        $section_info_error = '';
//        $model_error = '';
//        try {
//            if ($model->load(Yii::$app->request->post()) ) {
//
//                    $bank_info_error = $this->validateBankInfo($_POST['bank_name']);
//
//                    if ($bank_info_error) {
//                        //Bank info validation failed
////                        return $this->redirect(['view', 'id' => $model->id]);
//                        throw new Exception("All bank information must be filled");
//                    }
//                    //Save the details
//                    $model->save(false);
//                    Logs::logEvent("School updated: " . $model->school_name, null, null);
//
//                    if ($_POST['bank_name']) {
//                        $this->updateSchAccounts($id, $accounts, $_POST['bank_name']);
//                    }
//
//
//                    if (isset($_POST['section_name'])) {
//                        $this->updateSchSections($id, $school_sections, $_POST['section_name']);
//                    }
//
//
//
//                    $transaction->commit();
//                    return $this->redirect(['view', 'id' => $model->id]);
//               // }
//            }
//
//        } catch (\Exception $e) {
//            Yii::trace($e);
//            $transaction->rollBack();
//            $model_error = $e->getMessage();
//        }
//
//
//        $school_banks = $this->getSchoolBanks($model->id);
//
//        $res = ['model' => $model,  'banks' => $banks, 'accounts' => $accounts, 'bank_info_error' => $bank_info_error,
//            'sections' => $school_sections, 'section_info_error' => $section_info_error, 'school_banks' => $school_banks, 'model_error' => $model_error];
//        return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);
//
//    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_sch', ['sch' => $model->id]) && Yii::$app->user->can('schoolpay_admin')) {

            //check if branch user
//
            if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
                if ($model->branch_id != Yii::$app->user->identity->branch_id) {
                    throw new ForbiddenHttpException('You dont have rights to edit ' . $model->school_name );
                }
            }




            $request = Yii::$app->request;
            $sp_modules = (new Query())->from('suite_modules')->orderBy('module_name')->all();
            $banks = $this->getBanks();
            $accounts = $this->getSchoolAccounts($id);
            $sch_assoc = (new Query())->select(['module_id'])->from('school_module_association')->where(['school_id' => $id])->all();

            $sch_modules = array_column($sch_assoc, 'module_id');

            $school_sections = $this->getSections($id);
            $section_info_error = '';
            $model_error = '';

            $bank_info_error = [];
            $section_info_error = '';

            $connection = Yii::$app->db;

            try {
                if ($model->load(Yii::$app->request->post()) && isset($_POST['sp_modules'])) {
                    $transaction = $connection->beginTransaction();
                    if (!empty($_POST['sp_modules'])) {
//                    $bank_info_error = $this->validateBankInfo($_POST['bank_name']);
//
//                    if ($bank_info_error) {
//                        //Bank info validation failed
//                        return $this->redirect(['view', 'id' => $model->id]);
//                    }
                        //Save the details
                        if ($model->save(true)) {

                            Logs::logEvent("School updated: " . $model->school_name, null, null);

                            if (isset($_POST['bank_name'])) {
                                $this->updateSchAccounts($id, $accounts, $_POST['bank_name']);
                            }

                            if (isset($_POST['section_name'])) {
                                $this->updateSchSections($id, $school_sections, $_POST['section_name']);
                            }


                            $post_assoc = $_POST['sp_modules'];
                            Yii::trace('CREATE module: ' . print_r($post_assoc, true), 'SCHOOL');
                            $dv_assoc = (new Query())->select(['module_id'])->from('school_module_association')->where(['school_id' => $id])->all();
                            $dv_assoc = array_column($dv_assoc, 'module_id');
                            $diff_create = array_diff($post_assoc, $dv_assoc);
                            $diff_del = array_diff($dv_assoc, $post_assoc);
                            Yii::trace('stored: ' . print_r($dv_assoc, true), 'SCHOOL');
                            Yii::trace('diff create: ' . print_r($diff_create, true), 'SCHOOL');
                            Yii::trace('diff del: ' . print_r($diff_del, true), 'SCHOOL');

                            if (!empty($diff_create)) {
                                foreach ($diff_create as $v) {

                                    Yii::trace($v);
                                    Yii::trace($model->id);
                                    $schModule = new SchoolModules();
                                    $schModule->school_id = $model->id;
                                    $schModule->module_id = $v;
                                    $schModule->save(true);
                                }
                            }
                            if (!empty($diff_del)) {
                                foreach ($diff_del as $v) {
                                    $schModule = SchoolModules::find()->where(['school_id' => $model->id, 'module_id' => $v])->limit(1)->one();
                                    $schModule->delete();
                                }
                            }

                            $transaction->commit();

                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }

                }
            } catch (\Exception $e) {
                Yii::trace($e);
                $transaction->rollBack();
                $model_error = $e->getMessage();
            }

            $school_banks = $this->getSchoolBanks($model->id);

            $res = ['model' => $model, 'sp_modules' => $sp_modules, 'model_error' => $model_error, 'sch_modules' => $sch_modules, 'banks' => $banks, 'accounts' => $accounts, 'bank_info_error' => $bank_info_error, 'sections' => $school_sections, 'section_info_error' => $section_info_error, 'school_banks' => $school_banks];
            return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to update or create a school.');
        }
    }


    /**
     * Gets the assigned and unassgined class lists for this section
     *
     */
    private function getSectionClassGroups($schoolId, $sectionId)
    {
        //Get assigned association records for this school and section_id
        $assigned = (new Query())->select(['class_id'])->from('school_section_class_association')->where(['school_id' => $schoolId, 'section_id' => $sectionId])->all();
        //Build array of assigned classes to this section
        $assigned_class_ids = array_column($assigned, 'class_id');

        //Get available un assigned classes for this school
        $assignedClassesResult = CoreSchoolClass::find()->where(['school_id' => $schoolId, 'id' => $assigned_class_ids])->all();


        //Get available un assigned classes for this school
        $availableClassesResult = CoreSchoolClass::find()->where(['school_id' => $schoolId])->andWhere(['not in', 'id', $assigned_class_ids])->all();

        $section_class_groups = [];
        $assigned = [];
        foreach ($assignedClassesResult as $cls) {
            $assigned[$cls->id] = $cls->class_description;
        }

        $section_class_groups['assigned'] = $assigned;

        $available = [];
        foreach ($availableClassesResult as $cls) {
            $available[$cls->id] = $cls->class_description;
        }

        $section_class_groups['available'] = $available;

        return $section_class_groups;
    }


    /**
     * Search method for sections
     * @param $queryParams
     */
    private function searchSections($searchModel)
    {
        $query = new Query();
        $query->select(['ss.id', 'ss.section_code', 'ss.section_name', 'si.school_name'])
            ->from('school_sections ss')
            ->innerJoin('core_school si', 'si.id=ss.school_id');

        if (isset($searchModel->schsearch)) {
            $query->where(['school_id' => $searchModel->schsearch]);
        }


        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'section_code', 'section_name', 'school_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ss.section_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

    public function actionSetclasssection($section, $school_id, $action)
    {
        if (!Yii::$app->user->can('rw_sch')) {
            throw new ForbiddenHttpException('No permissions');
        }
        $post = Yii::$app->request->post();
        $perms = $post['selected'];

        $error = [];
        if ($action == 'assign') {
            //Assigned the selected
            foreach ($perms as $v) {
                //If class exists for same school, simply update the record with this section id
                //Else create entry
                $existingAssociationForClass = SchoolSectionClassAssociation::findOne(['class_id' => $v]);
                if ($existingAssociationForClass) {
                    $existingAssociationForClass->section_id = $section;
                } else {
                    $existingAssociationForClass = new SchoolSectionClassAssociation();
                    $existingAssociationForClass->school_id = $school_id;
                    $existingAssociationForClass->class_id = $v;
                    $existingAssociationForClass->section_id = $section;
                }
                //Now save
                $existingAssociationForClass->save();

            }
        } else {
            //Remove the selected
            if (count($perms) > 0) {
                Yii::$app
                    ->db
                    ->createCommand()
                    ->delete('school_section_class_association', ['in', 'class_id', $perms])
                    ->execute();
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sectionClassGroups = $this->getSectionClassGroups($school_id, $section);
        $arr = [Html::renderSelectOptions('', $sectionClassGroups['available']),
            Html::renderSelectOptions('', $sectionClassGroups['assigned']),
            $error];

        Yii::trace($arr);
        return $arr;

    }


    /**The school sections controller
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionSections()
    {
        if (Yii::$app->user->can('rw_sch')) {
            $searchModel = new DynamicModel(['schsearch']);
            $searchModel->addRule(['schsearch'], 'integer');

            $request = Yii::$app->request;
            $queryParams = Yii::$app->request->queryParams;
            $searchModel->load($queryParams);

            Yii::trace($searchModel->schsearch);

            $data = $this->searchSections($searchModel);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('sections', [
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'], 'searchModel' => $searchModel]) :
                $this->render('sections', [
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'], 'searchModel' => $searchModel]);
        } else {
            throw new ForbiddenHttpException('No permissions to view sections.');
        }

    }


    /**
     * Loads the section class assignment page for this section
     * @param $id
     */
    public function actionVwsection($id)
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('rw_sch')) {
            throw new ForbiddenHttpException('No permissions to view sections.');
        }

        $model = SchoolSections::findOne(['id' => $id]);
        if (!$model) {
            throw new NotFoundHttpException('No permissions to view sections.');
        }

        $section_class_groups = $this->getSectionClassGroups($model->school_id, $model->id);


        return ($request->isAjax) ? $this->renderAjax('view_section', [
            'model' => $model, 'available' => $section_class_groups['available'], 'assigned' => $section_class_groups['assigned']]) :
            $this->render('view_section', [
                'model' => $model, 'available' => $section_class_groups['available'], 'assigned' => $section_class_groups['assigned']]);
    }


    private function getSections($schId)
    {
        $sections = (new Query())->from('school_sections ss')->select(['ss.id', 'section_name', 'section_code', 'section_primary_bank', 'nb.bank_name', 'section_account_number', 'bad.account_number as section_bank_account'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ss.section_primary_bank')
            ->innerJoin('core_bank_account_details bad', 'bad.id=ss.section_account_number')
            ->where(['ss.school_id' => $schId]);
        return $sections->all();
    }

    private function updateSchSections($sch_id, $existing_sections, $posted_sections)
    {
        Yii::trace('Posted sections is ' . print_r($posted_sections, true));

        $db_accounts = array_column($existing_sections, 'id');
        $post_accounts = array_column($posted_sections, 'id');
        $unset_section_ids = array_diff($db_accounts, $post_accounts); //these are the ones which have been deleted
        //Delete sections the user has deleted from the interface
        //TODO Delete associations too

        if (!empty($unset_section_ids)) {
            foreach ($unset_section_ids as $v) {
                $schAcc = SchoolSections::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                //$schAcc->delete();
            }
        }

        foreach ($posted_sections as $k => $v) {
            $section = isset($v['id']) ? SchoolSections::find()->where(['id' => $v['id'], 'school_id' => $sch_id])->limit(1)->one() : new SchoolSections();
            Yii::trace('The section is ' . print_r($section, true));
            $section->school_id = $sch_id;
            $section->section_name = $v['section_name'];
            $section->active = true;
            $section->section_primary_bank = $v['section_primary_bank'];
            $section->section_account_number = $v['section_account_number'];
            if (!$section->validate()) {
                throw new Exception('Invalid or missing section details');
            }
            $section->save(true);
        }
    }

    public function actionSyncReport()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreSchoolSearch();
        $dataProvider = $searchModel->searchSync(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('sync_index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('sync_index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }


    /**
     * @param null $classes
     * @param null $student_codes
     * @return string
     * @throws InvalidArgumentException
     * @throws Exception
     */
    public function actionCircularGenerator($school_id, $classes = null, $student_codes = null)
    {
        $searchModel = CoreSchool::findOne($school_id);
        $model = new CoreSchool();
        $request = Yii::$app->request;
        $studentIds = [];
//        try {
        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            Yii::trace($data);
            if (isset($data['CoreSchool']['classes'])) {
                Yii::trace("..... classses.........");
                //get student classes
                $query = (new Query())->select(['id'])->from('core_student')
                    ->andWhere(['in', 'class_id', array_values($data['CoreSchool']['classes'])])
                    ->all();

                if (isset($_POST['exclude_student_codes'])) {
                    Yii::trace($_POST['exclude_student_codes']);
                    //get student codes
                    $codes = $_POST['exclude_student_codes'];
                    $p_codes = preg_split("/[\s,]+/", $codes);
                    $p_codes = array_unique(array_filter($p_codes));
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->andWhere(['in', 'class_id', array_values($data['CoreSchool']['classes'])])
                        ->andWhere(['not in', 'student_code', $p_codes])->all();
                }

                foreach ($query as $v) {
                    array_push($studentIds, $v['id']);
                }
            } else if (isset($_POST['student_codes'])) {
                Yii::trace("..... codes.........");
                Yii::trace($_POST['student_codes']);
//                    get student codes
                $codes = $_POST['student_codes'];
                $p_codes = preg_split("/[\s,]+/", $codes);
                $p_codes = array_unique(array_filter($p_codes));
                $query = (new Query())->select(['id'])->from('core_student')
                    ->where(['in', 'student_code', $p_codes])->all();
                foreach ($query as $k => $v) {
                    array_push($studentIds, $v['id']);
                }
            }


            // get signatures attached
            /*  $db = Yii::$app->db;
              $signature = $db->createCommand('select cs.* from core_signatures cs left join circular_signatures  css on (cs.id=css.signature_id) where css.circular_id=:circular_id',
                  [':circular_id' => $circular_id])->queryOne();
              Yii::trace($signature);*/
            //now generator the PDF's
            $student_query = new Query();
            $student_query->select(['c_s.first_name', 'c_s.last_name', 'c_s.student_code', 'cs.school_name', 'cs.box_no', 'cs.contact_email_1', 'cs.contact_email_2', 'phone_contact_1', 'phone_contact_2', 'cs.vision', 'cs.school_reference_no'])
                ->from('core_student c_s')
                ->innerJoin('core_school cs', 'c_s.school_id=cs.id')
                ->where(['in', 'c_s.id', $studentIds]);
            $command = $student_query->createCommand();
            $student_data = $command->queryAll();


            $qr_data = [];

            foreach ($student_data as $v) {
                $qr_data[] = array(
                    'school_name' => $v['school_name'],
                    'box_no' => $v['box_no'],
                    'phone_contact_1' => $v['phone_contact_1'],
                    'phone_contact_2' => $v['phone_contact_2'],
                    'school_reference_no' => $v['school_reference_no'],
                    'id' => $searchModel->id,
                    'contact_email_1' => $v['contact_email_1'],
                    'first_name' => $v['first_name'],
                    'last_name' => $v['last_name'],

                    'vision' => $v['vision']
                );
            }
//
//                return $this->render('export_circular_pdf', ['circular_data' => $circular_data
//                ]);

            Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('export_circular_pdf', ['circular_data' => $circular_data
                ]),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print student report',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
//                    'SetHeader' => ['Reports||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => [],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            return $pdf->render();

        }
        /*   } catch (\Exception $e) {
               $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
               Yii::trace('Error: ' . $error, 'Generate circular callback');
           }*/
        $res = ['student_codes' => $student_codes, 'classes' => $classes, 'model' => $model];
        return ($request->isAjax) ? $this->renderAjax('circular_selector', $res) : $this->render('circular_selector', $res);

    }

    public function actionTemplateList($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '', 'img' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('sch.id as id, sch.school_name AS text, img.image_base64 as img')
                ->from('core_school sch')
                ->leftJoin('image_bank img', 'img.id=sch.school_logo')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $school = CoreSchool::find()->where(['id' => $id])->limit(1)->one();
            $logo = $school->school_logo ? $school->logo->image_base64 : '';
            $out['results'] = ['id' => $id, 'text' => $school->school_name, 'img' => $logo];
        }
        return $out;
    }

    public function actionSchoolsAndChannels()
    {
        $this->layout = '@app/views/layouts/web_site';
        $model = new DynamicModel(['school']);
        $query = (new Query())->select(['school_name'])->from('core_school');
        $query2 = (new Query())->select(['channel_name', 'payment_instructions AS instructions', 'payment_channel_logo'])->from('payment_channels')->orderBy('channel_type DESC', 'channel_name');
        $pages = new Pagination(['totalCount' => $query->count()]);
        $query->offset($pages->offset)->orderBy('school_name')->limit($pages->limit);
        return $this->render('schools_and_channels', ['schools' => $query->all(), 'channels' => $query2->all(), 'pages' => $pages, 'model' => $model]);
    }


    public function actionAddAccount($id)
    {
        $model = $this->findModel($id);
        Yii::trace($model);

        if (!Yii::$app->user->can('add_bank_account', ['sch' => $model->id])) {
            throw new ForbiddenHttpException('No permissions to update or create a school account.');
        }
        $connection = Yii::$app->db;
        //  $transaction = $connection->beginTransaction();


        $request = Yii::$app->request;
        $sp_modules = (new Query())->from('schoolpay_modules')->orderBy('module_name')->all();
        $banks = $this->getBanks();
        $accounts = $this->getSchoolAccounts($id);
        $sch_assoc = (new Query())->select(['module_id'])->from('school_module_association')->where(['school_id' => $id])->all();
        $sch_modules = array_column($sch_assoc, 'module_id');

        $school_sections = $this->getSections($id);
        $section_info_error = '';

        $bank_info_error = [];
        $section_info_error = '';
        $model_error = '';
        $bankname = '';
        $sectionname = '';
        try {
            if (Yii::$app->request->post()) {

                Yii::trace(Yii::$app->request->post());



                if (isset($_POST['bank_name'])) {
                    $bank_info_error = $this->validateBankInfo($_POST['bank_name']);

                    if ($bank_info_error) {
                        //Bank info validation failed
//                        return $this->redirect(['view', 'id' => $model->id]);
                        throw new Exception("All bank information must be filled");
                    }
                }

                if (isset($_POST['bank_name'])) {
                    $bankname = $_POST['bank_name'];

                }
                if (isset($_POST['section_name'])) {
                    $sectionname = $_POST['section_name'];

                }

                $branch = null;
                if(Yii::$app->user->can('view_by_branch')){
                    $branch = Yii::$app->user->identity->branch_id;
                }

                $workflowRequest = [
                    'approval_required_permission' => json_encode(array('sch_admin', 'branch_chekers', 'super_admin', 'sys_admin')),
                    'record_id' => $id,
                    'request_params' => json_encode([

                        'bank_details' => $bankname,
                        'section_details' => $sectionname,
                    ]),
                    'request_notes' => 'Add/Update Bank details',
                    'school_id' => $id,
                    'workflow_record_type' => 'ADD_BANK_ACCOUNT',
                    'branch_id' => $branch
                ];


                $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
                Yii::$app->session->setFlash('workflowInfo', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
                return Yii::$app->runAction('/workflow/workflow/view', ['id' => $insert_id]);
////


            }
            $school_banks = $this->getSchoolBanks($model->id);

            $res = ['model' => $model, 'sp_modules' => $sp_modules, 'sch_modules' => $sch_modules,
                'banks' => $banks, 'accounts' => $accounts, 'bank_info_error' => $bank_info_error,
                'sections' => $school_sections, 'section_info_error' => $section_info_error, 'school_banks' => $school_banks, 'model_error' => $model_error];


        } catch (\Exception $e) {
            Yii::trace($e->getMessage());
            //$transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('workflowError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
            // return \Yii::$app->runAction('school-information/add-account', ['id'=>$id]);
        }
        return ($request->isAjax) ? $this->renderAjax('create_school_accounts', $res) : $this->render('create_school_accounts', $res);


    }


    /**
     * Lists allISchoolInformation models.
     * @return mixed
     */
    public function actionCampuses()
    {
        if (!Yii::$app->user->can('create_campus'))
            throw new ForbiddenHttpException('No permissions to view campuses.');

        $request = Yii::$app->request;
        $searchModel = new SchoolCampusSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        $params = [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']];
        return ($request->isAjax) ? $this->renderAjax('campuses', $params) :
            $this->render('campuses', $params);

    }


    public function actionCreateCampus()
    {
        if (!Yii::$app->user->can('create_campus')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a campus.');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolCampuses();

        $model_error = '';

        try {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save();
                $transaction->commit();
                return $this->redirect(['campuses', 'id' => $model->id], 200);
            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
        }

        $res = ['model' => $model, 'model_error' => $model_error,];
        return ($request->isAjax) ? $this->renderAjax('create_campus', $res) : $this->render('create_campus', $res);

    }


    /**
     * Updates an existing SchoolInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateCampus($id)
    {
        if (!Yii::$app->user->can('create_campus')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a campus.');
        }

        $schoolUser = ToWords::isSchoolUser();
        $schoolId = $schoolUser ? Yii::$app->user->identity->school : null;

        $model = $this->findCampusModel($id, $schoolId);


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $request = Yii::$app->request;

        $model_error = '';
        try {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save(true);
                $transaction->commit();
                return $this->redirect(['campuses']);
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            $model_error = $e->getMessage();
        }


        $school_banks = $this->getSchoolBanks($model->id);

        $res = ['model' => $model, 'model_error' => $model_error];
        return ($request->isAjax) ? $this->renderAjax('update_campus', $res) : $this->render('update_campus', $res);

    }

    protected function findCampusModel($id, $schoolId)
    {
        $condition = $schoolId ? ['id' => $id, 'school_id' => $schoolId] : ['id' => $id];

        $campus = SchoolCampuses::findOne($condition);
        if (($model = $campus) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCampuslists($id = 0)
    {
        if (!$id) $id = 0;
        $countCampuses = SchoolCampuses::find()
            ->where(['school_id' => $id])
            ->count();

        $campuses = SchoolCampuses::find()
            ->where(['school_id' => $id])
            ->orderBy('campus_name')
            ->all();

        if ($countCampuses > 0) {
            echo "<option value='0'> All Campuses </option>";
            foreach ($campuses as $myClass) {
                echo "<option value='" . $myClass->id . "'>" . $myClass->campus_name . "</option>";
            }
        } else {
            echo "<option value='0'> -- </option>";
        }
    }


    public function actionMenu()
    {
        $request = Yii::$app->request;
      //  return $this->render('school_menu');
        return ($request->isAjax) ? $this->renderAjax('school_menu') :
            $this->render('school_menu');
    }

    public function actionSchool()
    {
        //return $this->render('menu');
        Yii::trace(Yii::$app->user);
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('menu') :
            $this->render('menu');
    }


    public function actionSchoolReport(){

        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreSchoolSearch();
        $dataProvider = $searchModel->searchReport(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('schools_report', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('schools_report', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);

    }


    public function actionActiveSchools()
    {

        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreSchoolSearch();
        $dataProvider = $searchModel->searchReport(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('schools_report', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('schools_report', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }



}


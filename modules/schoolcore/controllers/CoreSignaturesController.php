<?php

namespace app\modules\schoolcore\controllers;

use app\models\ImageBank;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Yii;
use app\modules\schoolcore\models\CoreSignatures;
use app\modules\schoolcore\models\CoreSignaturesSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CoreSignaturesController implements the CRUD actions for CoreSignatures model.
 */
class CoreSignaturesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreSignatures models.
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (!\app\components\ToWords::isSchoolUser())
            throw new ForbiddenHttpException('No permissions to view school signatories.');

        $searchModel = new CoreSignaturesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) : $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CoreSignatures model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) : $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the CoreSignatures model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreSignatures the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreSignatures::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new CoreSignatures model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidArgumentException|\yii\db\Exception
     */
    public function actionCreate()
    {
        if (!\app\components\ToWords::isSchoolUser())
            throw new ForbiddenHttpException('No permissions to create school signatories.');
        $model = new CoreSignatures();
        $request = Yii::$app->request;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = CoreSignatures::getDb()->beginTransaction();
            try {
                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $image = UploadedFile::getInstance($model, 'image');
                Yii::trace($image);
                if (!is_null($image)) {
                    $model->file_attachment = $image->name;
                    Yii::trace($image->name);
                    $tmp_img = explode('.', $image->name);
                    $ext = end($tmp_img); //new image extension

                    $model->file_attachment = Yii::$app->security->generateRandomString() . ".{$ext}";
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/signatures/';
                    $path = Yii::$app->params['uploadPath'] . $model->file_attachment;
                    $image->saveAs($path);
                } else {
                    Yii::trace("No image attached onto the form");
                }
                $model->created_by = Yii::$app->user->identity->getId();
                $model->save(false);
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE Signature callback');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return ($request->isAjax) ? $this->renderAjax('create', [
            'model' => $model,
        ]) : $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing CoreSignatures model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!\app\components\ToWords::isSchoolUser())
            throw new ForbiddenHttpException('No permissions to update school signatories.');
        $model = $this->findModel($id);
        try {
            if ($model->load(Yii::$app->request->post())) {
                $image = UploadedFile::getInstance($model, 'image');
                if (!is_null($image)) {
                    $model->file_attachment = $image->name;
                    Yii::trace($image->name);
                    $tmp_img = explode('.', $image->name);
                    $ext = end($tmp_img); //new image extension

                    $model->file_attachment = Yii::$app->security->generateRandomString() . ".{$ext}";
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/signatures/';
                    $path = Yii::$app->params['uploadPath'] . $model->file_attachment;
                    $image->saveAs($path);
                }
                $model->date_updated = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['index']);
            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CoreSignatures model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!\app\components\ToWords::isSchoolUser())
            throw new ForbiddenHttpException('No permissions to delete school signatories.');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}

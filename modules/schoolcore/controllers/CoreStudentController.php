<?php

namespace app\modules\schoolcore\controllers;

use app\components\ToWords;
use app\models\ContactForm;
use app\models\ImageBank;
use app\models\User;
use app\modules\feesdue\models\FeesDue;
use app\modules\logs\models\Logs;
use app\modules\paymentscore\models\StudentAccountHistorySearch;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreSubjectSearch;
use app\modules\schoolcore\models\Events;
use app\modules\schoolcore\models\EventsSearch;
use app\modules\studentreport\models\Report;
use app\modules\workflow\controllers\WorkflowController;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * CoreStudentController implements the CRUD actions for CoreStudent model.
 */
class CoreStudentController extends Controller
{
    public $password;

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * searches fields.
     * @return mixed
     */
//    public function actionSearch()
//    {
//        $theSearchModel = $this->newSearchModel();
//        $allData = $theSearchModel->search(Yii::$app->request->queryParams);
//
//        $dataProvider = $allData['dataProvider'];
//        $columns = $allData['columns'];
//        $searchForm = $allData['searchForm'];
//        $searchModel = $allData['searchModel'];
//
//        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
//        // return $request->isAjax ? 9$this->renderAjax('card_summary', $res)
//
//        return $this->render('@app/views/common/grid_view', $res);
//    }

//    public function newSearchModel()
//    {
//        // TODO: Implement newSearchModel() method.
//        return new CoreStudentSearch();
//    }

    /**
     * Creates a view.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $searchModel = new CoreStudentSearch();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }

    }


    public function actionArchived()
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $searchModel = new CoreStudentSearch();
            $data = $searchModel->searchArchived(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('archived', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('archived', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }

    }


    /**
     * Deletes an existing CoreStudent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = CoreStudent::find()->where(['student_code' => $id])->limit(1)->one();

        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {

            try {
                Logs::logEvent("Delete Student: " . $model->fullname, null, $model->id);
                Yii::$app->db->createCommand("select drop_student_details(:student)", [':student' => $id])
                    ->execute();
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Delete Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                $decoded =explode(": ",$e->errorInfo[2]);

                \Yii::$app->session->setFlash('actionFailed', $decoded[1]);
                Yii::trace($e->errorInfo);
                return \Yii::$app->runAction('schoolcore/core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to delete this students.');
        }
    }
    /**
     *
     * /**
     * Updates an existing CoreStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $oldModel = clone $model;

            if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
                $sch = CoreSchool::findOne(['id'=>$model->school_id]);

                if ($sch->branch_id != Yii::$app->user->identity->branch_id) {
                    throw new ForbiddenHttpException('You dont have rights to edit students in  ' . $sch->school_name );
                }
            }
            try {
                if ($model->load(Yii::$app->request->post())) {
                    $model->school_student_registration_number = trim(strtoupper($model->school_student_registration_number));
                    $model->first_name = ucwords(strtolower($model->first_name));
                    $model->middle_name = ucwords(strtolower($model->middle_name));
                    $model->last_name = ucwords(strtolower($model->last_name));
                    //check if registration exists

//                    $modelReg = CoreStudent::findOne(['school_student_registration_number'=>$model->school_student_registration_number, 'school_id'=> $model->school_id]);
//
//                    if($modelReg){
//                        throw new Exception("Failed: Submitted Student Registration Number already exists in this school");
//                    }

                    if ($model->save(true)) {
                        $edited = $this->diffObjects($oldModel, $model);
                        Logs::logEvent("Edited Student (" . $model->student_code . "): " . $edited, null, $model->id);
                        return $this->redirect(['view', 'id' => $model->id]);
                    }

                }
                return ($request->isAjax) ? $this->renderAjax('update', ['model' => $model]) :
                    $this->render('update', ['model' => $model]);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Update Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('schoolcore/core-student/error');

            }

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }

    /**
     * Finds the CoreStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreStudent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreStudent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function diffObjects($oldModel, $newModel)
    {
        $newModel->date_of_birth = Date('Y-m-d', strtotime($oldModel->date_of_birth));
        $oldModel->allow_part_payments = (!$oldModel->allow_part_payments) ? 0 : 1;
        $oldModel->disability = (!$oldModel->disability) ? 0 : 1;
        $oldArray = ArrayHelper::toArray($oldModel, [], false);
        $newArray = ArrayHelper::toArray($newModel, [], false);
        $oldvalues = array_diff_assoc($oldArray, $newArray);
        $newvalues = array_diff_assoc($newArray, $oldArray);
        $edited = '';
        foreach ($oldvalues as $ok => $ov) {
            $edited .= " |<b>" . $ok . "</b> from: " . $ov . " to: " . $newvalues[$ok];
        }
        return $edited;
    }


//    public function actionView($id)
//    {
//        //Only schoolpay admins and users with rw_user can access this area
//        if (!Yii::$app->user->can('r_student')) {
//            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
//        }
//        $model = $this->findModel($id);
//        Yii::trace($model->student_code);
//
//        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
//            'model' => $model]) :
//            $this->render('view', ['model' => $model]);
//    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {
            $searchModel = new StudentAccountHistorySearch();
            $searchModel->only_received = true;
            $dataProvider = $searchModel->search($model->student_account_id, Yii::$app->request->queryParams);
            $supplementaryFee = $this->searchSupplementary($id);
           // $supplementaryFee = $dataProvider['query'];

            Yii::trace("supplementaryFee");
            Yii::trace($supplementaryFee);
            $res = ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel,'supplementaryFee'=>$supplementaryFee];
            return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }


    public function searchSupplementary($params)
    {
        Yii::trace($params);
        //$query1 = \app\models\StudentGroupStudent::find()->select(['student_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['pr.date_created' ,'pr.reversal', 'pr.id','pr.reciept_number' ,'pc.payment_channel_logo','pc.channel_code', 'pr.channel_trans_id' ,'pr.channel_memo' ,'pr.amount','isf.description','pc.channel_name'])
            ->from('supplementary_fee_payments_received pr')
            ->innerJoin('core_student cs', 'cs.id=pr.student_id')
            ->innerJoin('institution_supplementary_fees isf', 'isf.id =pr.fee_id')
            ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel')

        ->andFilterWhere([
                'pr.student_id' => $params]);
        // 'si.school_id'=>$grp['sch']]);

        $query->all(Yii::$app->db);
        $command = $query->createCommand();
        $data = $command->queryAll();
        return $data;





    }




    public function newModel()
    {
        // TODO: Implement newModel() method.
        return new CoreStudent();
    }

    public function createModelFormTitle()
    {
        return 'New Student';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return
     */

    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }
        $request = Yii::$app->request;
        $model = new CoreStudent();
        $model->active = true;
        $model->allow_part_payments = false;
        $model->nationality = 'Ethiopian';
        $model->created_by = Yii::$app->user->identity->getId();
        $transaction = CoreStudent::getDb()->beginTransaction();

        try {
            if ($model->load(Yii::$app->request->post())) {
                $model->school_student_registration_number = trim(strtoupper($model->school_student_registration_number));


                $model->first_name = ucwords(strtolower($model->first_name));
                $model->middle_name = ucwords(strtolower($model->middle_name));
                $model->last_name = ucwords(strtolower($model->last_name));
                $model->is_password_created = false;
                // $model->setPassword($model->password);

                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }


                //check if registration exists
//                $modelReg = CoreStudent::findOne(['school_student_registration_number'=>$model->school_student_registration_number, 'school_id'=> $model->school_id]);
//
//                if($modelReg){
//                    throw new Exception("Failed: Submitted Student Registration Number already exists in this school");
//                }

                if($model->validate() && $model->save(true)) {
                    Logs::logEvent("Created New Student: " . $model->fullname, null, $model->id);
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                }else{
                    Yii::trace($model->errors);
                }



            }
            return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) :
                $this->render('create', ['model' => $model]);

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollback();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            //\Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Logs::logEvent("Error attaching device to staff: ", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }

    }


    public function actionUploadExcel()
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        $model = new DynamicModel(['importFile', 'description', 'school', 'student_class']);
        $model->addRule(['student_class', 'description'], 'required')
            ->addRule(['school'], 'integer')
            ->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx']);
        if (\app\components\ToWords::isSchoolUser()) {
            $model->school = Yii::$app->user->identity->school_id;
        }
        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($inputFile && $model->student_class && $model->description && $model->school) {
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    throw new Exception("Your file was uploaded but could not be processed. Confirm that its a valid excel file");
                }
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestcolumn();

                $upload_by = Yii::$app->user->identity->getId();

                //Selected class should belong to the selected school
                //This is a data integrity check
                $schoolRecord = CoreSchool::findOne($model->school);
                $classRecord = CoreSchoolClass::findOne($model->student_class);

                if ($schoolRecord->id != $classRecord->school_id) {
                    throw new Exception("Operation forbidden: Selected class should belong to the selected school");
                }


                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                //Get a school model for this school, we wil use it to validate the registration numbers


                try {
                    $sql1 = "INSERT INTO student_upload_file_details (
                              file_name, 
                              hash, 
                              uploaded_by, 
                              date_uploaded, 
                              description, 
                              destination_school, 
                              destination_class
                              )
                                VALUES (
                              :file_name, 
                              :hash, 
                              :uploaded_by, 
                              NOW(), 
                              :description, 
                              :destination_school, 
                              :destination_class
                                )";
                    $fileQuery = $connection->createCommand($sql1);
                    $fileQuery->bindValue(':file_name', $file_name);
                    $fileQuery->bindValue(':hash', $hash);
                    $fileQuery->bindValue(':uploaded_by', $upload_by);
                    $fileQuery->bindValue(':description', $model->description);
                    $fileQuery->bindValue(':destination_school', $model->school);
                    $fileQuery->bindValue(':destination_class', $model->student_class);
                    //Insert file
                    $fileQuery->execute();
                    $file_id = $connection->getLastInsertID('student_upload_file_details_id_seq');
                    $regNos = [];
                    $numberOfRecords = 1;
                    for ($row = 1; $row <= $highestRow; $row++) {
                        if ($row == 1) {
                            continue;
                        }
                        $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        //Collection information from excel row
                        $excelLastName = !isset($v[0][0]) ? null : trim(ucwords(strtolower($v[0][0])));
                        $excelFirstName = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));
                        $excelMiddleName = !isset($v[0][2]) ? null : trim(ucwords(strtolower($v[0][2])));
                        $excelDob = !isset($v[0][3]) ? null : $v[0][3];
                        $excelGender = !isset($v[0][5]) ? null : strtoupper(trim($v[0][5]));
                        $excelNationality = !isset($v[0][8]) ? null : $v[0][8];
                        $excelDisability = !isset($v[0][9]) ? null : $v[0][9];

                        $excelRegistrationNumber = !isset($v[0][4]) ? null : trim(strtoupper($v[0][4]));
                        //Remove spaces from registration numbers
                        if ($excelRegistrationNumber != null) $excelRegistrationNumber = str_replace(' ', '', $excelRegistrationNumber);

                        $excelStudentEmail = !isset($v[0][6]) ? null : trim($v[0][6]);
                        $excelStudentPhone = !isset($v[0][7]) ? null : trim($v[0][7]);
                        $excelDisabilityNature = !isset($v[0][10]) ? null : trim(ucwords($v[0][10]));
                        $excelGuardianName = !isset($v[0][11]) ? null : trim(ucwords(strtolower($v[0][11])));
                        $excelGuardianRelation = !isset($v[0][12]) ? null : trim(ucwords($v[0][12]));
                        $excelGuardianEmail = !isset($v[0][13]) ? null : trim($v[0][13]);
                        $excelGuardianPhone = !isset($v[0][14]) ? null : trim($v[0][14]);
                        $excelDayBoarding = !isset($v[0][15]) ? null : trim(strtoupper($v[0][15]));


                        //Check that both names are present
                        if (empty($excelFirstName) || empty($excelLastName)) {
                            throw new Exception("Both first name and last name are required. Check record $numberOfRecords");
                        }

                        //Validate gender
                        if ($excelGender != 'M' && $excelGender != 'F') {
                            throw new Exception("Specify M or F for gender. For student $excelLastName $excelFirstName");
                        }


                        if (empty($excelNationality)) {
                            throw new Exception("Nationality is required. Check record $numberOfRecords");
                        }

                        if (!ToWords::validateDate($excelDob, 'Y-m-d')) {
                            throw new Exception("Specify a valid date of birth in the format YYYY-mm-dd. For student $excelLastName $excelFirstName");
                        }

                        if (!empty($excelDayBoarding) && $excelDayBoarding != 'D' && $excelDayBoarding != 'B') {
                            throw new Exception("Specify D or B for day boarding field. For student $excelLastName $excelFirstName");
                        }


                        $disability = ($excelDisability) ? 'TRUE' : 'FALSE';

                        //Validate reg no for this school
                        // $schoolRecord->validateRegNoAgainstSchoolFormat($excelRegistrationNumber);

                        //$query = $excelRegistrationNumber ? (new Query())->from('student_information')->where(['school_id' => $model->school, 'school_student_registration_number' => $excelRegistrationNumber])->limit(1)->one() : [];
                        //  $uqRegNo = $excelRegistrationNumber ? in_array($excelRegistrationNumber, $regNos) : false;

                        if ($excelLastName && $excelFirstName && $excelDob && $excelGender && $excelNationality) {
                            $regNos[] = $excelRegistrationNumber;

                            $sql2 = "INSERT INTO student_upload_file_data (
                                        file_id, 
                                        date_created, 
                                        school_id, 
                                        student_class, 
                                        last_name,  
                                        first_name, 
                                        middle_name, 
                                        date_of_birth, 
                                        school_student_registration_number, 
                                        gender, student_email, 
                                        student_phone, 
                                        nationality,  
                                        disability, 
                                        disability_nature,
                                        gurdian_name, 
                                        guardian_relation, 
                                        gurdian_email, 
                                        gurdian_phone,
                                        day_boarding
                                     
                                        )
                                      VALUES (
                                        :file_id, 
                                        NOW(), 
                                        :school_id, 
                                        :student_class, 
                                        :last_name,  
                                        :first_name, 
                                        :middle_name, 
                                        :date_of_birth, 
                                        :school_student_registration_number, 
                                        :gender, 
                                        :student_email, 
                                        :student_phone, 
                                        :nationality,  
                                        :disability, 
                                        :disability_nature,
                                        :gurdian_name, 
                                        :guardian_relation, 
                                        :gurdian_email, 
                                        :gurdian_phone,
                                        :day_boarding
                                         
                                      )";

                            //Prepare and populate params
                            $query = $connection->createCommand($sql2);
                            $query->bindValue(':file_id', $file_id);
                            $query->bindValue(':school_id', $model->school);
                            $query->bindValue(':student_class', $model->student_class);
                            $query->bindValue(':last_name', $excelLastName);
                            $query->bindValue(':first_name', $excelFirstName);
                            $query->bindValue(':middle_name', $excelMiddleName);
                            $query->bindValue(':date_of_birth', $excelDob);
                            $query->bindValue(':school_student_registration_number', $excelRegistrationNumber);
                            $query->bindValue(':gender', $excelGender);
                            $query->bindValue(':student_email', $excelStudentEmail);
                            $query->bindValue(':student_phone', $excelStudentPhone);
                            $query->bindValue(':nationality', $excelNationality);
                            $query->bindValue(':disability', $disability);
                            $query->bindValue(':disability_nature', $excelDisabilityNature);
                            $query->bindValue(':gurdian_name', $excelGuardianName);
                            $query->bindValue(':guardian_relation', $excelGuardianRelation);
                            $query->bindValue(':gurdian_email', $excelGuardianEmail);
                            $query->bindValue(':gurdian_phone', $excelGuardianPhone);
                            $query->bindValue(':day_boarding', $excelDayBoarding);
                            //Excecute update now
                            $query->execute();

                            //Increment no of records
                            $numberOfRecords++;
                        } else {
                            $transaction->rollBack();
                            $ln = $excelLastName ? " Last Name: " . $excelLastName : " <b>Last Name: (not set)</b>";
                            $fn = $excelFirstName ? " First Name: " . $excelFirstName : " <b>First Name: (not set)</b>";
                            $dob = $excelDob ? " Date of Birth: " . date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($excelDob)) : " <b>Date of Birth: (not set)</b>";
                            $gender = $excelGender ? " Gender: " . $excelGender : " <b>Gender: (not set)</b>";
                            $country = $excelNationality ? " Nationality: " . $excelNationality : " <b>Nationality: (not set)</b>";
                            //$reg = $query ? "  <b>Reg No: " . $excelRegistrationNumber . " Already Taken</b>" : "";
                            // $rpRegNo = $uqRegNo ? "  <b>Reg No: " . $excelRegistrationNumber . " Repeated</b>" : "";
                            // Logs::logEvent("Failed to upload Students: ", "<b>Fix Row - " . $row . "</b> " . $ln . $fn . $dob . $gender . $country . $reg . $rpRegNo, null);
                            return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . $fn . $dob . $gender . $country . $reg . $rpRegNo . " </p></span>";
                        }
                    }

                    $imported = (new Query())->select(['imported', 'uploaded_by'])->from('student_upload_file_details')->where(['id' => $file_id])->limit(1)->one();
                    $transaction->commit();
                    Logs::logEvent("Temporary Students imported for class: " . $model->student_class, null, null);

                    //Send email
                    $subject = "New Schoolpay File Uploaded $file_name";
                    $emailMessage = "New file uploaded for $schoolRecord->school_name \n
                    Uploaded by: " . Yii::$app->user->identity->username . "\n
                    File Hash: $hash \n
                    Number of records: $numberOfRecords";

                    return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                    //return $this->renderAjax('file_details', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $imported['imported'], 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Logs::logEvent("Failed to upload Students: ", $error, null);
                    return "<div class='alert alert-danger' style='    background-color: #dd4b39 !important;'>Excel file File upload failed<br><br>
                    <p>" . $error . "</p> </div>";
                }

            } else {
                return "<div class='alert alert-danger' >File, Class or Description can't be blank</div>";
            }


        } else {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }
    }

    protected
    function fileData($id)
    {
        $query = new Query();
        $query->select(['id', 'last_name', 'first_name', 'middle_name', 'date_of_birth AS dob', 'school_student_registration_number AS reg_no', 'gender', 'student_email', 'student_phone', 'nationality', 'disability', 'disability_nature', 'gurdian_name', 'guardian_relation', 'gurdian_email', 'gurdian_phone', 'day_boarding'])
            ->from('student_upload_file_data')
            ->where(['file_id' => $id])
            ->orderBy('last_name')
            ->limit(1000);
        return $query->all();
    }

    protected function fileDetail($id)
    {
        $query = (new Query())->select(['imported', 'uploaded_by', 'description', 'sch.school_name', 'destination_school'])->from('student_upload_file_details fd')->innerJoin('core_school sch', 'sch.id=fd.destination_school')->where(['fd.id' => $id])->limit(1)->one();
        return $query;
    }

    public function actionFileDetails($id)
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'save') {
                $status = $this->saveStudents($id);
            } else if ($action == 'del_student' && $_POST['stu']) {
                $status = $this->deleteRecord($_POST['stu']);
            }
        }

        return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]);
    }

    protected function saveStudents($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        $select = "first_name , middle_name, last_name, school_id, student_class, student_email, student_phone, gender, date_of_birth, school_student_registration_number, nationality, disability, disability_nature, gurdian_name, guardian_relation, gurdian_email, gurdian_phone, day_boarding ";
        try {
            $fileDetails = $this->fileDetail($id);
            $sql2 = "UPDATE student_upload_file_details SET imported = true WHERE id = :fileid";
            $temp_students = $connection->createCommand("SELECT " . $select . " FROM student_upload_file_data WHERE file_id = :fileid", [
                ':fileid' => $id
            ])->queryAll();
            foreach ($temp_students as $record) {
                $reg_no = $record['school_student_registration_number'];
                if ($reg_no) {
                    $thisStudent = $connection->createCommand("SELECT id, first_name, last_name FROM core_student WHERE school_id=" . $fileDetails['destination_school'] . " AND school_student_registration_number ='" . $reg_no . "'")->queryOne();

                    if ($thisStudent) {
                        $transaction->rollBack();
                        $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>Student: " . $thisStudent['first_name'] . " " . $thisStudent['last_name'] . " with  Reg No: " . $reg_no . " Already exists</div>"];
                        return $status;
                    }
                }
                $userId = Yii::$app->user->identity->getId();

                // $password = Yii::$app->security->generatePasswordHash("abc123");

                $connection->createCommand("INSERT INTO core_student (is_password_created,first_name , middle_name, last_name, school_id, class_id, student_email, student_phone, 
                              gender, date_of_birth, nationality, disability, school_student_registration_number,disability_nature, 
                              guardian_name, guardian_relation, guardian_email, guardian_phone, day_boarding, created_by) 
                              VALUES(false,:first_name  ,:middle_name ,:last_name ,:school_id ,:student_class ,:student_email ,:student_phone ,
                              :gender , :date_of_birth ,:nationality ,:disability ,:school_student_registration_number,
                              :disability_nature ,:gurdian_name ,
                              :guardian_relation ,:gurdian_email ,:gurdian_phone ,:day_boarding,:created_by )")
                    ->bindParam("first_name", $record['first_name'])
                    ->bindParam("middle_name", $record['middle_name'])
                    ->bindParam("last_name", $record['last_name'])
                    ->bindParam("school_id", $record['school_id'])
                    ->bindParam("student_class", $record['student_class'])
                    ->bindParam("student_email", $record['student_email'])
                    ->bindParam("student_phone", $record['student_phone'])
                    ->bindParam("gender", $record['gender'])
                    ->bindParam("date_of_birth", $record['date_of_birth'])
                    ->bindParam("nationality", $record['nationality'])
                    ->bindParam("disability", $record['disability'])
                    ->bindParam("school_student_registration_number", $reg_no)
                    ->bindParam("disability_nature", $record['disability_nature'])
                    ->bindParam("gurdian_name", $record['gurdian_name'])
                    ->bindParam("guardian_relation", $record['guardian_relation'])
                    ->bindParam("gurdian_email", $record['gurdian_email'])
                    ->bindParam("gurdian_phone", $record['gurdian_phone'])
                    ->bindParam("day_boarding", $record['day_boarding'])
                    ->bindParam("created_by", $userId)
                    ->execute();


            }
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            //Logs::logEvent("New Students imported from file id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Students successfully imported</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            // Logs::logEvent("Failed to import Students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    protected function deleteRecord($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql = "DELETE FROM student_upload_file_data WHERE id = :id";
            $connection->createCommand($sql)->bindValue(':id', $id)->execute();
            $transaction->commit();
            //Logs::logEvent("Deleted Temporary student with id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Student successfully deleted</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            //   Logs::logEvent("Failed to delete Temporary students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    public function actionDeleteFile($id)
    {
        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $error = "";
        $state = ['status' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql1 = "DELETE FROM student_upload_file_data WHERE file_id = :fileid";
            $sql2 = "DELETE FROM student_upload_file_details WHERE id = :fileid";
            $connection->createCommand($sql1)->bindValue(':fileid', $id)->execute();
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            //  Logs::logEvent("Deleted Temporary students for file_id: " . $id, null, null);

            //After succesful deleting, redirect to the file list
            \Yii::$app->session->setFlash('fileListAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; File has been deleted succesfully! ");
            return \Yii::$app->runAction('schoolcore/core-student/uploaded-files');
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            //      Logs::logEvent("Failed to delete uploaded file(" . $id . "): ", $error, null);
            $error = "<div class='alert alert-danger'>" . $error . "</div>";
            return $error;
        }

    }


    public function actionUploadedFiles()
    {
        $request = Yii::$app->request;

        $searchModel = new DynamicModel(['schsearch']);
        $searchModel->addRule(['schsearch'], 'safe');
        $searchModel->load(Yii::$app->request->post());

        $query = new Query();
        $query->select(['fl.id', 'file_name', 'description', 'wsu.username', 'date_uploaded', 'school_name', 'si.id as school_id', 'class_code', 'imported'])
            ->from('student_upload_file_details fl')
            ->innerJoin('core_school_class cl', 'cl.id=fl.destination_class')
            ->innerJoin('user wsu', 'fl.uploaded_by=wsu.id')
            ->innerJoin('core_school si', 'si.id=cl.school_id');
        if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['si.id' => Yii::$app->user->identity->school_id]);
        }

        $query->andFilterWhere(['si.id' => $searchModel->schsearch]);
        $pages = new \yii\data\Pagination(['totalCount' => $query->count()]);
        $sort = new \yii\data\Sort([
            'attributes' => [
                'file_name', 'description', 'date_uploaded', 'username', 'school_name', 'class_code', 'imported'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('school_name ASC, class_code ASC, date_uploaded DESC');
        return ($request->isAjax) ? $this->renderAjax('files_uploaded', ['model' => $query->all(), 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $sort]) : $this->render('files_uploaded', ['model' => $query->all(), 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $sort]);
    }

    public function actionProfileimage($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('schoolsuite_admin', ['sch' => $model->school_id])) {

            $request = Yii::$app->request;
            $model->scenario = 'photo';
            $imageBank = null;
            $model->passport_photo ? $imageBank = ImageBank::find()->where(['id' => $model->passport_photo])
                ->limit(1)->one() : $imageBank = new ImageBank();
            try {
                if ($imageBank->load(Yii::$app->request->post())) {
                    $image = $_POST['ImageBank']['image_base64'];
                    $imageBank->image_base64 = $image;
                    $imageBank->description = 'Student Photo: ID - ' . $model->id;
                    if ($imageBank->save()) {
                        $model->passport_photo = $imageBank->id;
                        $model->save(false);
                        Logs::logEvent("Taken Student Photo: " . $model->fullname, null, $model->id);
                        $this->redirect(['core-student/view', 'id' => $model->id]);
                        // return Yii::$app->redirect('core-student/view', ['id'=>$model->id]);
                    }
                }
                $res = ['imageBank' => $imageBank, 'model' => $model];
                return ($request->isAjax) ? $this->renderAjax('_stu_image', $res) :
                    $this->render('_stu_image', $res);

            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to take photo: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->redirect('core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }

    public function actionProfileimageUpload($id)
    {
        $model = $this->findModel($id);
//        if (Yii::$app->user->can('schoolsuite_admin', ['sch' => $model->school_id])) {
        $request = Yii::$app->request;

        $model->scenario = 'photo';
        $imageBank = null;
        $imageBank = $model->passport_photo ? ImageBank::find()->where(['id' => $model->passport_photo])->limit(1)->one() : new ImageBank();
        try {
            if ($imageBank->load(Yii::$app->request->post())) {
                $data = $_POST['ImageBank']['image_base64'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $imageBank->image_base64 = $data;
                $imageBank->description = 'Student Photo Uploaded: ID - ' . $model->id;
                if ($imageBank->save()) {
                    $model->passport_photo = $imageBank->id;
                    $model->save(false);
                    Logs::logEvent("Uploaded Student Photo: " . $model->fullname, null, $model->id);
                    $this->redirect(['/schoolcore/core-student/view', 'id' => $model->id]);
                }
            }
            $res = ['imageBank' => $imageBank, 'model' => $model];
            return $this->render('_student_photo', $res);
        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to upload student photo: " . $model->fullname . "(" . $model->id . ")", $error, $model->id);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->redirect('core-student/error');
        }
//        } else {
//            throw new ForbiddenHttpException('No permissions to create or update school logo.');
//        }

    }

//    public function actionPrint($id)
//    {
//        $this->layout = 'printLayout';
//        $request = Yii::$app->request;
//        $model = $this->findModel($id);
////        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {
//
//        $bal = (new \yii\db\Query())
//            ->select(['account_balance AS bal'])
//            ->from('school_account_gl')
//            ->where(['id' => $model->student_account_id])
//            ->limit(1)->one();
//
//        $searchModel = new StudentAccountHistorySearch();
//        $searchModel->only_received = true;
//        $dataProvider = $searchModel->search($id, Yii::$app->request->queryParams);
//        return ($request->isAjax) ? $this->renderAjax('print', ['model' => $model,
//            'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
//            'bal' => $bal]) :
//            $this->render('print', ['model' => $model,
//                'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
//                'bal' => $bal]);
////        } else {
////            throw new ForbiddenHttpException('No permissions to view this student.');
////        }
//    }


    public function actionPrint($id)
    {
        $this->layout = 'printLayout';
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {

            $bal = (new \yii\db\Query())
                ->select(['account_balance AS bal'])
                ->from('school_account_gl')
                ->where(['id' => $model->student_account_id])
                ->limit(1)->one();

            $searchModel = new StudentAccountHistorySearch();
            $searchModel->only_received = true;
            $dataProvider = $searchModel->search($model->student_account_id, Yii::$app->request->queryParams);
            return ($request->isAjax) ? $this->renderAjax('print', ['model' => $model,
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
                'bal' => $bal]) :
                $this->render('print', ['model' => $model,
                    'dataProvider' => $dataProvider, 'searchModel' => $searchModel,
                    'bal' => $bal]);
        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }

    public function actionQrCode($id)
    {

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {

            return ($request->isAjax) ? $this->renderAjax('qr_code', ['model' => $model]) :
                $this->render('qr_code',  ['model' => $model]);
        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }

    public function actionStudentReport($studentNumber, $export_pdf = null)
    {
        $studentInfo = Yii::$app->db->createCommand('SELECT CS.id as student_number, CS.first_name,CS.middle_name,CS.last_name, CS.student_code,CS.class_id, CC.class_description FROM core_student CS inner join core_school_class CC on CC.id =CS.class_id WHERE CS.id=:studentId')
            ->bindValue(':studentId', $studentNumber)
            ->queryOne();
        $request = Yii::$app->request;
        $model = new Report();
        if ($model->load(Yii::$app->request->post()) || $export_pdf != null) {

            try {

                $output = [];
                $studentInfo = [];
                $subject_results_arry = [];
                $output_result = [];
                $test_grouping_arry = [];

                $studentInfo = Yii::$app->db->createCommand('SELECT CS.id as student_number, CS.first_name,CS.middle_name,CS.last_name, CS.student_code,CS.class_id, CC.class_description FROM core_student CS inner join core_school_class CC on CC.id =CS.class_id WHERE CS.id=:studentId')
                    ->bindValue(':studentId', $studentNumber)
                    ->queryOne();

                $schoolId = Yii::$app->db->createCommand('SELECT school_id FROM core_student WHERE id=:studentId')
                    ->bindValue(':studentId', $studentNumber)
                    ->queryOne();

                $schoolDetails = Yii::$app->db->createCommand(
                    'SELECT c_s.*,r_g.description as region_name,d.district_name FROM core_school c_s 
                                   left join ref_region r_g on(c_s.region=r_g.id)  left join district d on(c_s.district=d.id)  WHERE c_s.id=:schoolId')
                    ->bindValue(':schoolId', $schoolId['school_id'])
                    ->queryOne();

                $grouping = (new Query())->select(['id'])
                    ->from('test_grouping')
                    ->all();
                foreach ($grouping as $key) {
                    array_push($test_grouping_arry, $key['id']);
                }

                $results = (new Query())->select(['subject_name', 'subject_code', 'subject_id', 't_group.name', '((sum(marks_obtained))/COUNT(marks_obtained)) AS "score_avg"'])
                    ->from('core_marks cm')
                    ->innerJoin('core_test ct', 'ct.id = cm.test_id')
                    ->innerJoin('core_subject cs', ' cs.id = ct.subject_id')
                    ->innerJoin('core_assessment_type ast', ' ast.id =ct.assessment_id')
                    ->innerJoin('test_grouping t_group', ' t_group.id =ct.test_grouping_id')
                    ->innerJoin('report_subject_composition rsc', ' rsc.test_id = ct.id')
                    ->where(['student_id' => $studentNumber,
                        'ct.class_id' => $model->class_id,
                        'ct.term_id' => $model->term_id,
                    ])
                    ->andWhere(['in', 'test_grouping_id', $test_grouping_arry])
                    ->groupBy(["subject_name", "subject_id", "subject_code", "test_grouping_id", "t_group.name"])
                    ->all();


                //add only the subjects to the array
                foreach ($results as $result) {
                    if (!in_array($result['subject_code'], $subject_results_arry)) {
                        array_push($subject_results_arry, $result['subject_code']);
                    }
                }

                //now filter the data per subject
                foreach ($subject_results_arry as $subject) {
                    $records = array_filter($results, function ($row) use ($subject) {
                        return $row['subject_code'] === $subject;
                    });
                    $out_subject = array();
                    $subject_id = null;
                    $subject_name = null;
                    foreach ($records as $record) {
                        $out_subject[] = array("name" => $record['name'], "score_avg" => floor($record['score_avg']));
                        $subject_id = $record['subject_id'];
                        $subject_name = $record['subject_name'];
                    }
                    $output_result[] = array("subject_code" => $subject, "subject_name" => $subject_name, "subject_id" => $subject_id, "results" => $out_subject);
                    unset($out_subject);
                }


                $grading = Yii::$app->db->createCommand('SELECT * FROM core_grades WHERE class_id=:classId order by max_mark desc')
                    ->bindValue(':classId', $studentInfo['class_id'])
                    ->queryAll();

                if ($output_result) {
                    array_push($output, ['results' => $output_result, 'model' => $model, 'schoolDetails' => $schoolDetails, 'studentInfo' => $studentInfo, 'grading' => $grading]);
                    unset($output_result);
                }

                Yii::$app->session->setFlash('output_session', $output);
                Yii::$app->session->setFlash('result_model_session', $model);

//                return $this->redirect(['view-privacy']);


                return ($request->isAjax) ? $this->renderAjax('student_report', ['output' => $output, 'model' => $model,]) :
                    $this->render('student_report', ['output' => $output, 'model' => $model,]);

            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace($error);
            }
        }

        return ($request->isAjax) ? $this->renderAjax('_student_report_form', ['student_info' => $studentInfo, 'model' => $model]) :
            $this->render('_student_report_form', ['student_info' => $studentInfo, 'model' => $model]);
        //return $this->render('_student_report_form', ['student_info' => $studentInfo, 'model' => $model]);
    }


    public function actionViewPrivacy()
    {

        $output = Yii::$app->session->getFlash('output_session');
        $model = Yii::$app->session->getFlash('result_model_session');

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('export_student_report', ['output' => $output, 'model' => $model]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students transactions',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }


    public function actionError()
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('student_error') : $this->render('student_error');
    }

//$request = Yii::$app->request;
//$model = $this->findModel($id);
//$searchModel = new StudentAccountHistorySearch();
//$searchModel->only_received = true;
//$dataProvider = $searchModel->search($id, Yii::$app->request->queryParams);
//$res = ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel'=> $searchModel];
//return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);

    public function actionStudentProfile()
    {


        $searchModel = new StudentAccountHistorySearch();
        $searchModel->only_received = true;
        $dataProvider = $searchModel->search(Yii::$app->user->id, Yii::$app->request->queryParams);
        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
            'model' => $this->findModel(Yii::$app->user->id)]) :
            $this->render('view', [
                'model' => $this->findModel(Yii::$app->user->id),
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
    }

    public function actionChangePassword()
    {

        $request = Yii::$app->request;
        $std = CoreStudent::findOne(['web_user_id' => Yii::$app->user->id]);
        $model = $this->findModel([$std['id']]);
        $model->scenario = 'change';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $passwd = $data['CoreStudent']['new_pass'];
            $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
            $model->setPassword($passwd);
            if ($model->save(false)) {
                Logs::logEvent("User password reset(" . $model->id . "): " . $model->student_code, null, null);
                Yii::$app->session->setFlash('success', 'Thank you for using schoolsuite. Your password has been successfully reset.');
                return $this->redirect(['/site/index']);
            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('change_pass', ['model' => $model]) :
                $this->render('change_pass', ['model' => $model]);
        }


    }


    public function actionStudentIndex()
    {


        if (Yii::$app->user->can('non_student')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }
        $request = Yii::$app->request;
        $events = Events::find()->all();
        foreach ($events as $event) {
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $event->id;
            $Event->title = $event->title;
            $Event->backgroundColor = '#e6838e';
            $Event->start = $event->event_date;
            $events[] = $Event;
        }

        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->id])->one();
        Yii::trace($studentModel);
        if ($studentModel) {
            $subjects = CoreSubject::find()->where(['class_id' => $studentModel->class_id])->all();
//            return $this->render('student_index', [
//                'events' => $events,
//                'searchModel' => $searchModel,
//                'dataProvider' => $dataProvider,
//                'subjects' => $subjects,
//            ]);

            return ($request->isAjax) ? $this->renderAjax('student_index', ['events' => $events,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'subjects' => $subjects,]) :
                $this->render('student_index', ['events' => $events,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'subjects' => $subjects,]);

        } else {
            return Yii::$app->runAction('/site/logout');
        }

    }

    public function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExporfindDatatQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionEmail()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('student_email')
                    ->from('core_student');
                $command = $query->createCommand();
                $emails = $command->queryAll(\PDO::FETCH_COLUMN);
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if ($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                } else {
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to students: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->redirect('schoolcore/core-student/error');
            }

        }
        return $this->render('contact', ['model' => $model]);
    }

    public function actionSendEmail($id)
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('student_email')
                    ->from('core_student')
                    ->where(['id' => $id])->limit(1)->one();
                $command = $query->createCommand();
                $emails = $command->queryAll(\PDO::FETCH_COLUMN);

//                $email = (new Query())->select(['student_email'])->from('core_student')->where(['id' => $id])->limit(1)->one();
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if ($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                } else {
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            } catch (\Exception $e) {
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                $error = 'No email is attached to this student. Please add an email to the student and try again';
                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to student: ", $error, $id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->redirect('schoolcore/core-student/error');
            }

        }
        return $this->render('contact', ['model' => $model]);
    }

    public function actionAssignFee($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $user_id = Yii::$app->user->identity->id;
            $user_name = Yii::$app->user->identity->username;
            $user_ip = Yii::$app->request->userIP;
            $model2 = new DynamicModel(['fee','credit_hours']);
            $model2->addRule(['fee'], 'required');
            $model2->addRule(['fee'], 'integer');
            $model2->addRule(['credit_hours'], 'integer');

            if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
                $sch = CoreSchool::findOne(['id'=>$model->school_id]);

                if ($sch->branch_id != Yii::$app->user->identity->branch_id) {
                    throw new ForbiddenHttpException('You dont have rights to edit students in  ' . $sch->school_name );
                }
            }

            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
                $fee =FeesDue::find()->where(['id'=>$model2->fee])->limit(1)->one();
                $creditHrs =1;
                Yii::trace($fee);
                Yii::trace($fee->credit_hour);
                Yii::trace($model2->credit_hours);
                if($fee->credit_hour){
                    $creditHrs = $model2->credit_hours;
                }
                $branch = null;
                if(Yii::$app->user->can('view_by_branch')){
                    $branch = Yii::$app->user->identity->branch_id;
                }
Yii::trace($creditHrs);

                $workflowRequest = [
                    'approval_required_permission' => json_encode(array('sch_admin', 'branch_chekers', 'super_admin', 'sys_admin')),
                    'record_id' => $model->id,
                    'request_params' => json_encode([
                        'fee_id'=>$fee->id,
                        'credit_hours'=>$creditHrs
                    ]),
                    'request_notes'=>'assign fee to student',
                    'school_id'=>$model->school_id,
                    'workflow_record_type'=>'STUDENT_FEE_ASSIGNMENT',
                    'branch_id'=>$branch,

                ];

                try {
                    $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
                    return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
                }catch (\Exception $e) {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    return \Yii::$app->runAction('/view', ['id'=>$insert_id]);
                }

            }
            return ($request->isAjax) ? $this->renderAjax('assign_fee', ['model2' => $model2, 'model' => $model]) : $this->render('assign_fee', ['model2' => $model2, 'model' => $model]);
        } else {
            throw new ForbiddenHttpException('No permissions to Assign Student fees');
        }

    }
//  public function actionAssignFee($id)
//    {
//        $model = $this->findModel($id);
//        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
//            $request = Yii::$app->request;
//            $user_id = Yii::$app->user->identity->id;
//            $user_name = Yii::$app->user->identity->username;
//            $user_ip = Yii::$app->request->userIP;
//            $model2 = new DynamicModel(['fee']);
//            $model2->addRule(['fee'], 'required');
//            $model2->addRule(['fee'], 'integer');
//
//            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
//                $connection = Yii::$app->db;
//                $transaction = $connection->beginTransaction();
//                $fee = FeesDue::find()->where(['id' => $model2->fee])->limit(1)->one();
//                Yii::trace($fee);
//                try {
//                    $connection->createCommand("select apply_fee_to_student(
//                        :student_id,
//                        :fee,
//                        :userid,
//                        :username,
//                        :userip)", [
//                        ':student_id' => $model->id,
//                        ':fee' => $model2->fee,
//                        ':userid' => $user_id,
//                        ':username' => $user_name,
//                        ':userip' => $user_ip,
//                    ])->execute();
//
//                    $transaction->commit();
//                    Logs::logEvent("Attaching " . ucwords($fee->description) . " (" . number_format($fee->due_amount, 2) . ") to -" . $model->fullname . " ", null, $model->id);
//                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; " . ucwords($fee->description) . " of " . number_format($fee->due_amount, 2) . " has been assigned to " . $model->fullname);
//                    return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $model->id]);
//                    //  return \Yii::$app->runAction('schoolcore/core-staff/error');
//
//                } catch (\Exception $e) {
//                    $transaction->rollBack();
//                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                    Logs::logEvent("Error on assigning a fee: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
//                    \Yii::$app->session->setFlash('actionFailed', $error);
//                    //return $this->redirect('core-student/error');
//                    return \Yii::$app->runAction('schoolcore/core-student/error');
//
//                }
//            }
//            return ($request->isAjax) ? $this->renderAjax('assign_fee', ['model2' => $model2, 'model' => $model]) : $this->render('assign_fee', ['model2' => $model2, 'model' => $model]);
//        } else {
//            throw new ForbiddenHttpException('No permissions to Assign Student fees');
//        }
//
//    }

    public function actionUnarchiveStudent($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $user_id = Yii::$app->user->identity->id;
            $user_name = Yii::$app->user->identity->username;
            $user_ip = Yii::$app->request->userIP;
            $model2 = new DynamicModel(['reason']);
            $model2->addRule(['reason'], 'required');
            if ($model2->load(Yii::$app->request->post())) {
                if ($model->archived || $model2->validate()) {
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $connection->createCommand("select unarchive_student(
                        :this_student_id,  
                        :reason, 
                        :webuser, 
                        :username, 
                        :userip)",
                            [
                                ':this_student_id' => $id,
                                ':reason' => $model2->reason,
                                ':webuser' => $user_id,
                                ':username' => $user_name,
                                ':userip' => $user_ip
                            ])->execute();
                        $transaction->commit();
                        Logs::logEvent("Unarchived Student : " . $model->fullname, null, $model->id);
                        \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $model->fullname . "  successfully un-archived");
                        return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $id]);

                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                        Logs::logEvent("Error on un-archiving Students: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                        \Yii::$app->session->setFlash('actionFailed', $error);
                        return \Yii::$app->runAction('schoolcore/core-student/error');
                    }
                } else {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp Please provide reason for un-archiving OR student already un-archived");
                    return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $id]);
                }
            }
            return ($request->isAjax) ? $this->renderAjax('un_archive_student', ['model2' => $model2, 'model' => $model]) : $this->render('un_archive_student', ['model2' => $model2, 'model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to un-archive student');
        }
    }

//    public function actionRemoveFee($id)
//    {
//        $model = $this->findModel($id);
//        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
//            $request = Yii::$app->request;
//            $user_id = Yii::$app->user->identity->id;
//            $user_name = Yii::$app->user->identity->username;
//            $user_ip = Yii::$app->request->userIP;
//            $model2 = new DynamicModel(['fee', 'adjust_bal']);
//            $model2->addRule(['fee', 'adjust_bal'], 'required');
//            $model2->addRule(['fee'], 'integer');
//
//            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
//                $connection = Yii::$app->db;
//                $transaction = $connection->beginTransaction();
//                $fee = FeesDue::find()->where(['id' => $model2->fee])->limit(1)->one();
//                try {
//                    $connection->createCommand("select remove_fee_from_student(
//                                :student_id,
//                                :fee,
//                                :userid,
//                                :username,
//                                :userip,
//                                :adjbal)", [
//                        ':student_id' => $model->id,
//                        ':fee' => $model2->fee,
//                        ':userid' => $user_id,
//                        ':username' => $user_name,
//                        ':userip' => $user_ip,
//                        ':adjbal' => $model2->adjust_bal
//                    ])->execute();
//                    $transaction->commit();
//                    Logs::logEvent("Removing " . ucwords($fee->description) . " (" . number_format($fee->due_amount, 2) . ") from -" . $model->fullname . " ", null, $model->id);
//                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; " . ucwords($fee->description) . " of " . number_format($fee->due_amount, 2) . " has been removed from " . $model->fullname);
//                    return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $model->id]);
//                } catch (\Exception $e) {
//                    $transaction->rollBack();
//                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                    // Logs::logEvent("Error on removing a fee: ".$model->fullname."(".$model->student_code.")", $error, $model->id);
//                    \Yii::$app->session->setFlash('actionFailed', $error);
//                    return \Yii::$app->runAction('schoolcore/core-student/error');
//                }
//            }
//            return ($request->isAjax) ? $this->renderAjax('remove_fee', ['model2' => $model2, 'model' => $model]) : $this->render('remove_fee', ['model2' => $model2, 'model' => $model]);
//        } else {
//            throw new ForbiddenHttpException('No permissions to remove Student fee');
//        }
//
//    }

    public function actionRemoveFee($id)
    {
        $model = $this->findModel($id);
        if(Yii::$app->user->can('rw_student', ['sch'=>$model->school_id])){
            $request = Yii::$app->request;
            $user_id = Yii::$app->user->identity->id;
            $user_name = Yii::$app->user->identity->username;
            $user_ip = Yii::$app->request->userIP;

            $model2 = new DynamicModel(['fee', 'adjust_bal', 'exempt']);
            $model2->addRule(['fee'], 'required');
            $model2->addRule(['fee', 'adjust_bal'], 'required');
            $model2->addRule(['fee'], 'integer');
            $model2->addRule(['exempt'], 'boolean');

            if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
                $sch = CoreSchool::findOne(['id'=>$model->school_id]);

                if ($sch->branch_id != Yii::$app->user->identity->branch_id) {
                    throw new ForbiddenHttpException('You dont have rights to edit students in  ' . $sch->school_name );
                }
            }
            if($model2->load(Yii::$app->request->post()) && $model2->validate()){
                $fee = FeesDue::find()->where(['id'=>$model2->fee])->limit(1)->one();


                $branch = null;
                if(Yii::$app->user->can('view_by_branch')){
                    $branch = Yii::$app->user->identity->branch_id;
                }


                $workflowRequest = [
                    'approval_required_permission' => json_encode(array('sch_admin','branch_chekers',  'super_admin', 'sys_admin')),
                    'record_id' => $model->id,
                    'request_params' => json_encode([
                        'amount'=>$fee->due_amount,
                        'fee_id'=>$fee->id,
                        'adjust_bal'=>$model2->adjust_bal,
                        'exempt'=>$model2->exempt
                    ]),
                    'request_notes'=>'remove fee from student',
                    'school_id'=>$model->school_id,
                    'workflow_record_type'=>'REMOVE_STUDENT_FEE',
                    'branch_id'=>$branch
                ];

                try {
                    $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
                    return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
                }catch (\Exception $e) {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    Yii::trace($e->getMessage());
                    return \Yii::$app->runAction('/schoolcore/core-student/view', ['id'=>$insert_id]);
                }
            }
            return ($request->isAjax) ? $this->renderAjax('remove_fee', ['model2' => $model2, 'model'=>$model]) : \Yii::$app->runAction('/view', ['id'=>$model->id]);
        } else {
            throw new ForbiddenHttpException('No permissions to remove Student fee');
        }


    }




    public function actionArchiveStudent($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $user_id = Yii::$app->user->identity->id;
            $user_name = Yii::$app->user->identity->username;
            $user_ip = Yii::$app->request->userIP;
            $model2 = new DynamicModel(['reason']);
            $model2->addRule(['reason'], 'required');

            if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
                $sch = CoreSchool::findOne(['id'=>$model->school_id]);

                if ($sch->branch_id != Yii::$app->user->identity->branch_id) {
                    throw new ForbiddenHttpException('You dont have rights to edit students in  ' . $sch->school_name );
                }
            }
            if ($model2->load(Yii::$app->request->post())) {


                if (!$model->archived || $model2->validate()) {
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    try {
                        $connection->createCommand("select archive_student(
                         :userid, 
                         :reason, 
                         :webuser, 
                         :username, 
                         :userip)", [
                            ':userid' => $id,
                            ':reason' => $model2->reason,
                            ':webuser' => $user_id,
                            ':username' => $user_name,
                            ':userip' => $user_ip
                        ])->execute();
                        $transaction->commit();
                        Logs::logEvent("Archived Student : " . $model->fullname, null, $model->id);
                        \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $model->fullname . "  successfully archived");
                        return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $id]);

                    } catch (\Exception $e) {
                        $transaction->rollBack();
                        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                        Logs::logEvent("Error on Archive Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                        \Yii::$app->session->setFlash('actionFailed', $error);
                        return \Yii::$app->runAction('schoolcore/core-student/error');
                    }
                } else {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp Please provide reason for archiving OR student already archived");
                    return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $id]);
                }
            }
            return ($request->isAjax) ? $this->renderAjax('archive_student', ['model2' => $model2, 'model' => $model]) : $this->render('archive_student', ['model2' => $model2, 'model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to adjust Banlance');
        }
    }

//    public function actionAdjustBal($id)
//    {
//        $request = Yii::$app->request;
//        $user_name = Yii::$app->user->identity->username;
//        $user_id = Yii::$app->user->identity->id;
//        $ip = Yii::$app->request->userIP;
//        $studentModel = $this->findModel($id);
//        Yii::trace($studentModel);
//
//        if (Yii::$app->user->can('rw_student', ['sch' => $studentModel->school_id])) {
//            $balanceRequestModel = new DynamicModel(['new_balance', 'reason', 'balance_type']);
//            $balanceRequestModel->addRule(['new_balance', 'reason', 'balance_type'], 'required')
//                ->addRule(['new_balance'], 'number');
//
//
//            if ($balanceRequestModel->load(Yii::$app->request->post())) {
//
//                //Since we are openning balance adjustment to every one, assume negative is not admin
//                if ($balanceRequestModel->balance_type == 'OUTSTANDING') {
//                    $balanceRequestModel->new_balance = -1 * abs($balanceRequestModel->new_balance);
//                } else {
//                    $balanceRequestModel->new_balance = abs($balanceRequestModel->new_balance);
//                }
//
//
//                $connection = Yii::$app->db;
//                $transaction = $connection->beginTransaction();
//                if ($balanceRequestModel->validate()) {
//                    try {
//                        //select * from adjust_student_balance(1000013519, -1000, 10, 'jm', 'paid ko a little', '.0.0.0.0');
//                        $connection->createCommand("select adjust_student_balance(
//                        :student_code,
//                        :new_balance,
//                        :userid,
//                        :username,
//                        :reason,
//                        :ip)", [
//                            ':student_code' => $studentModel->student_code,
//                            ':new_balance' => $balanceRequestModel->new_balance,
//                            ':userid' => $user_id,
//                            ':username' => $user_name,
//                            ':reason' => $balanceRequestModel->reason,
//                            ':ip' => $ip,
//                        ])->execute();
//
//                        $transaction->commit();
//                        Logs::logActionEvent($studentModel->fullname . "'s Balance Adjusited to " . $balanceRequestModel->new_balance, null, Logs::ADJUST_BALANCE, $studentModel->id);
//                        \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $studentModel->fullname . "'s  balance adjusited to " . $balanceRequestModel->new_balance);
//                        return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $studentModel->id]);
//                    } catch (\Exception $e) {
//                        $transaction->rollBack();
//                        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                        Logs::logEvent("Error on Adjust Bal: " . $studentModel->fullname . "(" . $studentModel->student_code . ")", $error, $studentModel->id);
//                        \Yii::$app->session->setFlash('actionFailed', $error);
//                        return \Yii::$app->runAction('schoolcore/core-student/error');
//                    }
//                } else {
//                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp Balance not adjusited. Please Both fields are required");
//                    return \Yii::$app->runAction('schoolcore/core-student/view', ['id' => $studentModel->id]);
//                }
//            }
//            Yii::trace("no post");
//            return ($request->isAjax) ? $this->renderAjax('adjust_bal', ['model2' => $balanceRequestModel, 'model' => $studentModel]) : $this->render('adjust_bal', ['model2' => $balanceRequestModel, 'model' => $studentModel]);
//            //  return ($request->isAjax) ? $this->renderAjax('adjust_bal', ['model2' => $balanceRequestModel, 'model' => $studentModel]) : \Yii::$app->runAction('core-student/view', ['id' => $studentModel->id]);
//
//        } else {
//            throw new ForbiddenHttpException('No permissions to adjust Banlance');
//        }
//
//    }


    public function actionAdjustBal($id)
    {
        $request = Yii::$app->request;
        $user_name = Yii::$app->user->identity->username;
        $user_id = Yii::$app->user->identity->id;
        $ip = Yii::$app->request->userIP;
        $studentModel = $this->findModel($id);


        if(!Yii::$app->user->can('rw_student', ['sch'=>$studentModel->school_id])) {
            throw new ForbiddenHttpException('No permissions to adjust Balance');
        }

        $balanceRequestModel = new DynamicModel(['new_balance', 'reason', 'balance_type']);
        $balanceRequestModel->addRule(['new_balance', 'reason', 'balance_type'], 'required')
            ->addRule(['new_balance'], 'number');

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $sch = CoreSchool::findOne(['id'=>$studentModel->school_id]);

            if ($sch->branch_id != Yii::$app->user->identity->branch_id) {
                throw new ForbiddenHttpException('You dont have rights to edit students in  ' . $sch->school_name );
            }
        }

        if(!$balanceRequestModel->load(Yii::$app->request->post())) {
            return ($request->isAjax) ? $this->renderAjax('adjust_bal', ['model2' => $balanceRequestModel, 'model'=>$studentModel]) : \Yii::$app->runAction('schoolcore/core-student/view', ['id'=>$studentModel->id]);
        }

        //Since we are openning balance adjustment to every one, assume negative is not admin
        if($balanceRequestModel->balance_type == 'OUTSTANDING') {
            $balanceRequestModel->new_balance = -1* abs($balanceRequestModel->new_balance);
        }else {
            $balanceRequestModel->new_balance = abs($balanceRequestModel->new_balance);
        }

        if(!$balanceRequestModel->validate()){
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp Balance not adjusted. Please Both fields are required");
            return \Yii::$app->runAction('schoolcore/core-student/view', ['id'=>$studentModel->id]);
        }
        $branch = null;
        if(Yii::$app->user->can('view_by_branch')){
            $branch = Yii::$app->user->identity->branch_id;
        }

        $workflowRequest = [
            'approval_required_permission' => json_encode(array('sch_admin','branch_chekers',  'super_admin', 'sys_admin')),
            'record_id' => $studentModel->id,
            'request_params' => json_encode([
                'mode'=>'TO',
                'amount'=>$balanceRequestModel->new_balance,
            ]),
            'request_notes'=>$balanceRequestModel->reason,
            'school_id'=>$studentModel->school_id,
            'workflow_record_type'=>'STUDENT_BALANCE_ADJUSTMENT',
            'branch_id'=>$branch
        ];

        try {
            $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
            \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
            return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
        }catch (\Exception $e) {
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
            Yii::trace($e->getMessage());
            return \Yii::$app->runAction('/schoolcore/core-student/view', ['id'=>$insert_id]);
        }
    }


















    public function actionExportPdfTrans($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportData();
        //print_r($data); exit;
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportTrasaction', ['model' => $data['data'], 'type' => $type,
                ]
            ),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students transactions',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionGenerateQr($id)
    {
        $model = $this->findModel($id);

        $request = Yii::$app->request;


        $res = ['model' => $model];
        return $this->render('generate_qr', $res);

    }

    public function actionStdListWtPasswords()
    {

        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            Yii::trace(Yii::$app->user);
            $searchModel = new CoreStudentSearch();
            $data = $searchModel->searchStudentsWtPswd(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('students_without_pswds', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('students_without_pswds', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }


    }

    public function actionGenerateStdPasswords()
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = new CoreStudent();

            $connection = Yii::$app->db;

            $transaction = $connection->beginTransaction();
            try {
                if ($model->load(Yii::$app->request->post())) {

                    $data = Yii::$app->request->post();
                    $passwd = $data['CoreStudent']['new_pass'];
                    $clsId = $data['CoreStudent']['class_id'];

                    $stdList = CoreStudent::find()->where(['school_id' => Yii::$app->user->identity->school_id, 'class_id' => $clsId, 'is_password_created' => false])->all();

                    foreach ($stdList as $k => $v) {
                        Yii::trace($v['gender']);
                        $modelUser = new User();
                        $modelUser->username = $v['student_code'];
                        if (!$v['student_email']) {
                            $email = 'suitestds@gmail.com';

                        } else {
                            $email = $v['student_email'];
                        }

                        $modelUser->setPassword($passwd);
                        //$modelUser->password=$password;
                        $modelUser->email = $email;
                        $modelUser->firstname = $v['first_name'];
                        $modelUser->lastname = $v['last_name'];
                        $modelUser->mobile_phone = $v['student_phone'];
                        $modelUser->gender = $v['gender'];
                        $modelUser->invalid_login_count = 0;
                        $modelUser->school_id = $v['school_id'];
                        $modelUser->locked = false;
                        $modelUser->user_level = 'is_student';
                        $modelUser->school_user_id = $v['id'];
                        if(isset($v['schoolpay_paymentcode'])){
                            $modelUser->schpay_student_code = $v['schoolpay_paymentcode'];
                        }
                        $modelUser->save(false);
//                Yii::trace($password);
//                Yii::trace($modelUser->save());
                        $modelUser->setAuthAssignment($modelUser->user_level, strval($modelUser->id));
                        Logs::logEvent("New Std User Created(" . $modelUser->id . "): " . $modelUser->username, null, null);

                        $modelStd = $this->findModel($v['id']);
                        Yii::trace($modelStd);
                        $modelStd->is_password_created = true;
                        $modelStd->web_user_id = $modelUser->id;
                        $modelStd->save(false);

                    }

                    $transaction->commit();

                    //   Logs::logEvent("Created New Student Psw: " . $model->fullname, null, $model->id);
                    return $this->redirect(['core-student/std-list-wt-passwords']);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Delete Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('schoolcore/core-student/error');
            }

            return ($request->isAjax) ? $this->renderAjax('create_student_psd', ['model' => $model]) :
                $this->render('create_student_psd', ['model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }


    }

    public function actionGenerateSingleStdPassword($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = new CoreStudent();

            $connection = Yii::$app->db;

            $transaction = $connection->beginTransaction();
            try {
                if ($model->load(Yii::$app->request->post())) {

                    $data = Yii::$app->request->post();
                    $passwd = $data['CoreStudent']['new_pass'];

                    $stdDet = CoreStudent::findOne(['id' => $id]);

                    //print_r($stdDet);
                    $modelUser = new User();
                    $modelUser->username = $stdDet['student_code'];
                    if (!$stdDet['student_email']) {
                        $email = 'suitestds@gmail.com';

                    } else {
                        $email = $stdDet['student_email'];
                    }

                    $modelUser->setPassword($passwd);
                    //$modelUser->password=$password;
                    $modelUser->email = $email;
                    $modelUser->firstname = $stdDet['first_name'];
                    $modelUser->lastname = $stdDet['last_name'];
                    $modelUser->mobile_phone = $stdDet['student_phone'];
                    $modelUser->gender = $stdDet['gender'];
                    $modelUser->invalid_login_count = 0;
                    $modelUser->school_id = $stdDet['school_id'];
                    $modelUser->locked = false;
                    $modelUser->user_level = 'is_student';
                    $modelUser->school_user_id = $stdDet['id'];

                    if(isset($stdDet['schoolpay_paymentcode'])){
                        $modelUser->schpay_student_code = $stdDet['schoolpay_paymentcode'];
                    }
                    $modelUser->save(false);
//                Yii::trace($password);
//                Yii::trace($modelUser->save());
                    $modelUser->setAuthAssignment($modelUser->user_level, strval($modelUser->id));
                    Logs::logEvent("New Std User Created(" . $modelUser->id . "): " . $modelUser->username, null, null);

                    $modelStd = $this->findModel($stdDet['id']);
                    Yii::trace($modelStd);
                    $modelStd->is_password_created = true;
                    $modelStd->web_user_id = $modelUser->id;
                    $modelStd->save(false);


                    $transaction->commit();

                    //   Logs::logEvent("Created New Student Psw: " . $model->fullname, null, $model->id);
                    return $this->redirect(['core-student/std-list-wt-passwords']);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Delete Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('schoolcore/core-student/error');
            }

            return ($request->isAjax) ? $this->renderAjax('create_single_student_psd', ['model' => $model]) :
                $this->render('create_single_student_psd', ['model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }


    }

    public function actionReset($id)
    {
        //Only schoolsuite admins and users with rw_user can access this area
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }

        $request = Yii::$app->request;
        $std = CoreStudent::findOne(['web_user_id' => $id]);
        $model = $this->findModel($std['id']);
        $model->scenario = 'reset';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $userModel = User::findOne($id);
            Yii::trace($userModel);
            $passwd = $data['CoreStudent']['new_pass'];
            $userModel->locked = false;
            $userModel->password_expiry_date = date('Y-m-d');
            $userModel->setPassword($passwd);
            try {
                $userModel->save(false);
                Logs::logEvent("Passwd reset for (" . $userModel->id . ") By Admin: " . $userModel->username, null, null);
                //Send email to user on reset

                Yii::$app->session->setFlash('success', 'Student Password has been reset successfully.');

                return $this->redirect(['view', 'id' => $model->id]);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                \Yii::$app->session->setFlash('actionFailed', $error);
//                return \Yii::$app->runAction('/student/error');
            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('reset_pass', ['model' => $model]) :
                $this->render('reset_pass', ['model' => $model]);
        }
    }


    public function actionExportToPdf($model)
    {
        /*		$data = $model::getExportQuery();
                $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
                $query = $data['data'];
                $query = $query->limit(200)->all(Yii::$app->db);
                $type = 'Pdf';

                $html = $this->renderPartial($data['exportFile'],
                        ['query'=>$query,'type' => $type, 'subSessionIndex'=>$subSessionIndex
                    ]);
        //        try{
        //        $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
        //        Logs::logEvent("Failed to create pdf", $error, null);
                    return Yii::$app->pdf->exportData($data['title'], $data['fileName'],$html);
        //        } catch(\Exception $e){
        //
        //            \Yii::$app->session->setFlash('actionFailed', $error);
        //            return \Yii::$app->runAction('/core-student/error');
        //        }*/

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial($data['exportFile'], ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => [$data['title']. '||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }


}

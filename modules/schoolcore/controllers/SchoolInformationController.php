<?php

namespace app\modules\schoolcore\controllers;


use app\components\ToWords;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\SchoolAccountTransactionHistorySearch;
use app\modules\reports\models\Reports;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\SchoolSectionClassAssociation;
use app\modules\schoolcore\models\SchoolSections;
use Yii;
use yii\base\DynamicModel;

use app\models\ImageBank;
use yii\data\Pagination;
use yii\data\Sort;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use app\modules\logs\models\Logs;
use app\modules\banks\models\BankAccountDetails;
use yii\db\Query;

/**
 * SchoolInformationController implements the CRUD actions for SchoolInformation model.
 */
class SchoolInformationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['template-list', 'schoollist'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionChannels($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_sch', ['sch' => $model->id])) {
            $request = Yii::$app->request;
            $available = array();
            $assigned = array();
            $channels = \app\models\PaymentChannels::find()->all();

            foreach ($model->schChannels as $chn) {
                $assigned[$chn->payment_channel] = $chn->paymentChannel->channel_name;
            }

            foreach ($channels as $chn) {
                if (!isset($assigned[$chn->id])) {
                    $available[$chn->id] = $chn->channel_name;
                }
            }

            return ($request->isAjax) ? $this->renderAjax('channels', ['model' => $model,
                'available' => $available, 'assigned' => $assigned]) :
                $this->render('channels', ['model' => $model, 'available' => $available,
                    'assigned' => $assigned]);
        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }

    public function actionAssign($id, $action)
    {
        $error = [];
        if ($channels = $_POST['selected']) {
            if ($action == 'assign') {
                foreach ($channels as $i => $chn) {
                    try {
                        $item = new SchoolChannel();
                        $item->institution = $id;
                        $item->payment_channel = $chn;
                        $item->save(false);
                    } catch (\Exception $exc) {
                        $error[] = $exc->getMessage();
                    }
                }
            } else if ($action == 'delete') {
                Yii::trace(json_encode($channels), 'asign /delete channels');
                foreach ($channels as $i => $chn) {
                    try {
                        Yii::trace(json_encode($chn), 'delete channels');
                        $item = SchoolChannel::find()->where(['payment_channel' => $chn, 'institution' => $id])->one();
                        $item->delete();
                    } catch (\Exception $exc) {
                        $error[] = $exc->getMessage();
                    }
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$this->actionChannelSearch($id, 'available', ''),
            $this->actionChannelSearch($id, 'assigned', ''),
            $error];
    }

    public function actionChannelSearch($id, $target, $term = '')
    {
        $channels = \app\models\PaymentChannels::find()->all();
        $model = $this->findModel($id);
        $available = [];
        $assigned = [];

        foreach ($model->schChannels as $chn) {
            $assigned[$chn->payment_channel] = $chn->paymentChannel->channel_name;
        }

        foreach ($channels as $chn) {
            if (!isset($assigned[$chn->id])) {
                $available[$chn->id] = $chn->channel_name;
            }
        }

        $result = [];
        $var = ${$target};
        if (!empty($term)) {
            foreach ($var as $i => $descr) {
                if (stripos($descr, $term) !== false) {
                    $result[$i] = $descr;
                }
            }
        } else {
            $result = $var;
        }

        return Html::renderSelectOptions('', $result);
    }


    public function actionAccounthistory()
    {
        $request = Yii::$app->request;
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history';

            $searchModel = new SchoolAccountTransactionHistorySearch();
            if (\app\components\ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $model = $this->findModel($schId);
                $data = $searchModel->searchPayment($model->school_account_id, Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                //Yii::trace($dataProvider);
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->searchPayment('', Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }


            Yii::trace($dataProvider);
            return ($request->isAjax) ? $this->renderAjax('account_histories', [
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('account_histories', ['dataProvider' => $dataProvider,
                    'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }


public function actionSupplementaryhistory()
    {
        $request = Yii::$app->request;
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history_sup';

            $searchModel = new SchoolAccountTransactionHistorySearch();
            if (\app\components\ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $model = $this->findModel($schId);
                $data = $searchModel->searchSupplementaryPayment($model->school_account_id, Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                //Yii::trace($dataProvider);
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->searchSupplementaryPayment('', Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }



            return ($request->isAjax) ? $this->renderAjax('supplementary_histories', [
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('supplementary_histories', ['dataProvider' => $dataProvider,
                    'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }


    public function actionHistory($id)
    {
        $data = SchoolAccountTransactionHistorySearch::findHistory($id);

        return $this->renderPartial('history_details', ['data' => $data]);
    }


    public function getImage($id)
    {
        $data = CoreStudent::getChannelLogos($id);
        Yii::trace($data);
        return $data;
    }


    public function actionReversePayment($id)
    {
        if (!Yii::$app->user->can('reverse_payment')) {
            throw new ForbiddenHttpException('User has no rights to perform this action');
        }
        $request = Yii::$app->request;
        $model = PaymentsReceived::find()->where(['id' => $id])->limit(1)->one();

        $model2 = new DynamicModel(['reason']);
        $model2->addRule(['reason'], 'required');
        $connection = Yii::$app->db;

        if ($model2->load(Yii::$app->request->post())) {
            $transaction = $connection->beginTransaction();
            try {
                $sql = "select reverse_payment_by_receipt_number(:in_payment_code, :in_receipt_number, :reversal_reason)";
                $query = $connection->createCommand($sql);
                $res = $query->bindValues(['in_payment_code' => $model->student->student_code, 'in_receipt_number' => $model->reciept_number, 'reversal_reason' => $model2->reason])
                    ->queryOne();
                $transaction->commit();
                $res = json_decode($res['reverse_payment_by_receipt_number']);
                if ($res->returncode == 0) {
                    Logs::logEvent("Payment Reversed for id: " . $id, null, $model->student->id);
                    \Yii::$app->session->setFlash('successAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $model->reciept_number . "  Payment was successfully reversed");
                } else {
                    Logs::logEvent("Payment Reversal failed for Id: " . $id, $res->returnmessage, $model->student->id);
                    \Yii::$app->session->setFlash('errorAlert', "<i class='fa fa-check-circle-o'></i>&nbsp;" . $res->returnmessage);
                }
                return \Yii::$app->runAction('/schoolcore/school-information/accounthistory');
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Payment Reversal failed for Id: " . $id, "Reversal Failed", $model->student->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/schoolcore/core-student/error');
            }
        }
        return ($request->isAjax) ? $this->renderAjax('reverse_payment', ['model2' => $model2, 'model' => $model]) : $this->render('reverse_payment', ['model2' => $model2, 'model' => $model]);

    }


    private function getBanks()
    {
        $banks = (new Query())->from('nominated_bank_details')->select(['id', 'bank_name']);
        return $banks->all();
    }


    private function getAccounts($id)
    {
        $accounts = (new Query())->from('bank_account_details ba')->select(['nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('nominated_bank_details nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $id]);
        return $accounts->all();
    }

    private function getSections($schId)
    {
        $sections = (new Query())->from('school_sections ss')->select(['ss.id', 'section_name', 'section_code', 'section_primary_bank', 'nb.bank_name'])
            ->innerJoin('nominated_bank_details nb', 'nb.id=ss.section_primary_bank')
            ->where(['ss.school_id' => $schId]);
        return $sections->all();
    }

    private function getSchoolAccounts($schId)
    {
        $accounts = (new Query())->from('bank_account_details ba')->select(['ba.id as bid', 'nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('nominated_bank_details nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId]);
        return $accounts->all();
    }

    private function getSchoolBanks($schId)
    {
        $accounts = (new Query())->from('bank_account_details ba')->select(['nb.id', 'nb.id as bank_id', 'nb.bank_name'])
            ->innerJoin('nominated_bank_details nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId])
            ->distinct();
        return $accounts->all();
    }

    private function saveAccounts($posted_banks, $sch_id)
    {
        foreach ($posted_banks as $k => $v) {
            $bankAcc = new BankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }

    private function updateSchAccounts($sch_id, $accounts, $posts)
    {
        Yii::trace('Db accounts is ' . print_r($accounts, true));
        Yii::trace('Posted accounts is ' . print_r($posts, true));
        $db_accounts = array_column($accounts, 'bid'); //id is in bid
        $post_accounts = array_column($posts, 'id');
        $diff_del = array_diff($db_accounts, $post_accounts);
        if (!empty($diff_del)) {
            foreach ($diff_del as $v) {
                $schAcc = BankAccountDetails::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                $schAcc->delete();
            }
        }

        foreach ($posts as $k => $v) {
            $bankAcc = isset($v['id']) ? BankAccountDetails::find()->where(['id' => $v['id']])->limit(1)->one() : new BankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }


    private function updateSchSections($sch_id, $existing_sections, $posted_sections)
    {
        Yii::trace('Posted sections is ' . print_r($posted_sections, true));

        $db_accounts = array_column($existing_sections, 'id');
        $post_accounts = array_column($posted_sections, 'id');
        $unset_section_ids = array_diff($db_accounts, $post_accounts); //these are the ones which have been deleted
        //Delete sections the user has deleted from the interface
        //TODO Delete associations too

        if (!empty($unset_section_ids)) {
            foreach ($unset_section_ids as $v) {
                $schAcc = SchoolSections::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                //$schAcc->delete();
            }
        }

        foreach ($posted_sections as $k => $v) {
            $section = isset($v['id']) ? SchoolSections::find()->where(['id' => $v['id'], 'school_id' => $sch_id])->limit(1)->one() : new SchoolSections();
            Yii::trace('The section is ' . print_r($section, true));
            $section->school_id = $sch_id;
            $section->section_name = $v['section_name'];
            $section->active = true;
            $section->section_primary_bank = $v['section_primary_bank'];
            $section->save(true);
        }
    }

    public function actionSchools($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('school_account_id as id, school_name AS text')
                ->from('school_information')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['school_account_id' => $id])->school_name];
        }
        return $out;
    }

    public function actionSchoollist($q = null, $id = null, $bank = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text, school_logo')
                ->from('school_information')
                ->where(['ilike', 'school_name', $q])
                ->andFilterWhere(['bank_name' => $bank])
                ->limit(7);
            $command = $query->createCommand(Yii::$app->db2); //Search schools via backup db
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }

    public function actionActiveSchoollist($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('school_information')
                ->where(['active' => true])
                ->andWhere(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }


    public function actionTemplateList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '', 'img' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('sch.id as id, sch.school_name AS text, img.image_base64 as img')
                ->from('school_information sch')
                ->leftJoin('image_bank img', 'img.id=sch.school_logo')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $school = School::find()->where(['id' => $id])->limit(1)->one();
            $logo = $school->school_logo ? $school->logo->image_base64 : '';
            $out['results'] = ['id' => $id, 'text' => $school->school_name, 'img' => $logo];
        }
        return $out;
    }

    public function actionLogo($id)
    {
        if (!Yii::$app->user->can('edit_school_logo')) {
            throw new ForbiddenHttpException('No permissions to create or update school logo.');
        }

        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_sch') || Yii::$app->user->can('own_sch', ['sch' => $model->id]) || Yii::$app->user->can('edit_school_logo')) {
            $request = Yii::$app->request;
            $model->scenario = 'photo';
            $imageBank = null;
            $imageBank = $model->school_logo ? ImageBank::find()->where(['id' => $model->school_logo])->limit(1)->one() : new ImageBank();
            try {
                if ($imageBank->load(Yii::$app->request->post())) {
                    $data = $_POST['ImageBank']['image_base64'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $imageBank->image_base64 = $data;
                    $imageBank->description = 'School Logo: ID - ' . $model->id;
                    if ($imageBank->save()) {
                        $model->school_logo = $imageBank->id;
                        $model->save(false);
                        Logs::logEvent("School logo changed: " . $model->school_name . "(" . $model->school_code . ")", null, null);
                        $this->redirect(['view', 'id' => $model->id]);
                    }
                }
                $res = ['imageBank' => $imageBank, 'model' => $model];
                return ($request->isAjax) ? $this->renderAjax('_sch_logo_croppie', $res) : $this->render('_sch_logo_croppie', $res);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to upload school photo: " . $model->school_name . "(" . $model->school_code . ")", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to create or update school logo.');
        }

    }

    /**
     * Finds the SchoolInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreSchool::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**The school sections controller
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionSections()
    {
        if (Yii::$app->user->can('rw_sch')) {
            $searchModel = new DynamicModel(['schsearch']);
            $searchModel->addRule(['schsearch'], 'integer');

            $request = Yii::$app->request;
            $queryParams = Yii::$app->request->queryParams;
            $searchModel->load($queryParams);

            Yii::trace($searchModel->schsearch);

            $data = $this->searchSections($searchModel);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('sections', [
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'], 'searchModel' => $searchModel]) :
                $this->render('sections', [
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'], 'searchModel' => $searchModel]);
        } else {
            throw new ForbiddenHttpException('No permissions to view sections.');
        }

    }


    /**
     * Loads the section class assignment page for this section
     * @param $id
     */
    public function actionVwsection($id)
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('rw_sch')) {
            throw new ForbiddenHttpException('No permissions to view sections.');
        }

        $model = SchoolSections::findOne(['id' => $id]);
        if (!$model) {
            throw new NotFoundHttpException('No permissions to view sections.');
        }

        $section_class_groups = $this->getSectionClassGroups($model->school_id, $model->id);

        return ($request->isAjax) ? $this->renderAjax('view_section', [
            'model' => $model, 'available' => $section_class_groups['available'], 'assigned' => $section_class_groups['assigned']]) :
            $this->render('view_section', [
                'model' => $model, 'available' => $section_class_groups['available'], 'assigned' => $section_class_groups['assigned']]);
    }

    /**
     * Gets the assigned and unassgined class lists for this section
     *
     */
    private function getSectionClassGroups($schoolId, $sectionId)
    {
        //Get assigned association records for this school and section_id
        $assigned = (new \yii\db\Query())->select(['class_id'])->from('school_section_class_association')->where(['school_id' => $schoolId, 'section_id' => $sectionId])->all();
        //Build array of assigned classes to this section
        $assigned_class_ids = array_column($assigned, 'class_id');

        //Get available un assigned classes for this school
        $assignedClassesResult = CoreSchoolClass::find()->where(['institution' => $schoolId, 'id' => $assigned_class_ids])->all();


        //Get available un assigned classes for this school
        $availableClassesResult = CoreSchoolClass::find()->where(['institution' => $schoolId])->andWhere(['not in', 'id', $assigned_class_ids])->all();

        $section_class_groups = [];
        $assigned = [];
        foreach ($assignedClassesResult as $cls) {
            $assigned[$cls->id] = $cls->class_description;
        }

        $section_class_groups['assigned'] = $assigned;

        $available = [];
        foreach ($availableClassesResult as $cls) {
            $available[$cls->id] = $cls->class_description;
        }

        $section_class_groups['available'] = $available;

        return $section_class_groups;
    }


    /**
     * Search method for sections
     * @param $queryParams
     */
    private function searchSections($searchModel)
    {
        $query = new Query();
        $query->select(['ss.id', 'ss.section_code', 'ss.section_name', 'si.school_name'])
            ->from('school_sections ss')
            ->innerJoin('school_information si', 'si.id=ss.school_id');

        if (isset($searchModel->schsearch)) {
            $query->where(['school_id' => $searchModel->schsearch]);
        }


        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'section_code', 'section_name', 'school_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ss.section_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

    public function actionSetclasssection($section, $school_id, $action)
    {
        if (!Yii::$app->user->can('rw_sch')) {
            throw new ForbiddenHttpException('No permissions');
        }
        $post = Yii::$app->request->post();
        $perms = $post['selected'];

        $error = [];
        if ($action == 'assign') {
            //Assigned the selected
            foreach ($perms as $v) {
                //If class exists for same school, simply update the record with this section id
                //Else create entry
                $existingAssociationForClass = SchoolSectionClassAssociation::findOne(['class_id' => $v]);
                if ($existingAssociationForClass) {
                    $existingAssociationForClass->section_id = $section;
                } else {
                    $existingAssociationForClass = new SchoolSectionClassAssociation();
                    $existingAssociationForClass->school_id = $school_id;
                    $existingAssociationForClass->class_id = $v;
                    $existingAssociationForClass->section_id = $section;
                }
                //Now save
                $existingAssociationForClass->save();

            }
        } else {
            //Remove the selected
            if (count($perms) > 0) {
                Yii::$app
                    ->db
                    ->createCommand()
                    ->delete('school_section_class_association', ['in', 'class_id', $perms])
                    ->execute();
            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $sectionClassGroups = $this->getSectionClassGroups($school_id, $section);
        $arr = [Html::renderSelectOptions('', $sectionClassGroups['available']),
            Html::renderSelectOptions('', $sectionClassGroups['assigned']),
            $error];

        Yii::trace($arr);
        return $arr;

    }

    /**
     * Deletes an existing SchoolInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionEnableSuite($id)
    {
        if (!Yii::$app->user->can('schoolpay_admin'))
            throw new ForbiddenHttpException('No permissions to enable this module');

        $query = new Query();
        $query->select(['*'])
            ->from('school_information si')
            ->andWhere(['si.school_code' => $id]);

        $school = $query->one();
        if (!$school)
            throw new ForbiddenHttpException('School not found');

        Yii::trace($school);
        //Get classes
        $query = new Query();
        $query->select(['*'])
            ->from('institution_student_classes c')
            ->andWhere(['c.institution' => $school['id']]);


        $classes = $query->all();

        $query = new Query();
        $query->select(['*'])
            ->from('student_information s')
            ->andWhere(['s.school_id' => $school['id']])
            ->limit(5);
        $students = $query->all();

        //Attach bank
        $query = new Query();
        $query->select(['*'])
            ->from('bank_account_details b')
            ->andWhere(['b.school_id' => $school['id']]);

        $accounts = $query->all();

        $payload = [
            'schoolInformation' => $school,
            'classes' => $classes,
            'students' => $students,
            'accounts' => $accounts,
        ];

        //TODO: Send payload to schoolsuite for onboarding
        //On success add mapping to integrator

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $syncUrl = Yii::getAlias('@SchoolSuiteSchoolSyncUrl', false);
        if (!$syncUrl) $syncUrl = 'https://suiteuat.schoolpay.co.ug/synccore/schoolpay/sync-schpay';

        $syncRequest = json_encode($payload);
//        Yii::trace('Sending ' . $syncRequest);
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($syncUrl)
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($syncRequest)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        if (!$response->isOk) {
            throw new ForbiddenHttpException('Count not send request to school suite interface => Status Code:' . $response->statusCode);
        }

        $content = $response->getContent();
        $_response = json_decode($content);
        Yii::trace($content);
        if ($_response->returncode != 0)
            throw new ForbiddenHttpException('Error importing data =>' . $_response->returncode . ':' . $_response->returnmessage);


        return json_encode($content);
    }

    public function actionPenaltyReport()
    {
        $request = Yii::$app->request;
        if (Yii::$app->user->can('payments_received_list')) {
            $searchModel = new SchoolAccountTransactionHistorySearch();
            $_SESSION['PENALTY_MODE'] = 'penalty';

            if (\app\components\ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $model = $this->findModel($schId);
                $data = $searchModel->searchPenaltyPayment($model->school_account_id, Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                Yii::trace($dataProvider);
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->searchPenaltyPayment('', Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }


            return ($request->isAjax) ? $this->renderAjax('penalty_histories', [
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('penalty_histories', ['dataProvider' => $dataProvider,
                    'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }


    public function actionBranchPerformance()
    {
        $request = Yii::$app->request;
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history';

            $searchModel = new SchoolAccountTransactionHistorySearch();
            if (\app\components\ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $model = $this->findModel($schId);
                $data = $searchModel->schoolPerfomance($model->school_account_id, Yii::$app->request->queryParams);

                // $dataProvider = $data['query'];
                $activity = $data['activity'];

                // Yii::trace($dataProvider);
                 $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->schoolPerfomance('', Yii::$app->request->queryParams);
                //  $dataProvider = $data['query'];
                $activity = $data['activity'];
                 $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }

            //  Yii::trace($data);
            //Yii::trace($activity)


            return ($request->isAjax) ? $this->renderAjax('school_histories', [
                'data' => $activity, 'searchModel' => $searchModel,'pages' => $pages, 'sort' => $data['sort'],]) :
                $this->render('school_histories', [
                    'searchModel' => $searchModel, 'data' => $activity,'pages' => $pages, 'sort' => $data['sort'],]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }


    public function actionSchoolactivity()
    {
        $request = Yii::$app->request;

        $activity = '';
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history';
            $model = new DynamicModel(['from_date', 'to_date']);
            $model->addRule(['from_date', 'to_date'], 'required');


            try {
             
                $request = Yii::$app->request;
                if (!Yii::$app->session->isActive) {
                    session_start();
                }
//                $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days")) : $_GET['date_from'];
//
//                $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];;

                $searcModel = new Reports();
                $data = $searcModel->branchActivity(Yii::$app->request->queryParams);
                $dataProvider = $data['query'];


                if($this->date_from){
                    $trQuery->andWhere("cs.date_created::date>='$this->date_from'");
                    if($date_to) $trQuery->andWhere("cs.date_created::date<='$date_to'");
                }
                if($this->branch_id){
                    $trQuery->andWhere(['cs.branch_id'=> $this->branch_id]);
                }
                if($this->branch_region){
                    $trQuery->andWhere(['cs.branch_region'=> $this->branch_region]);
                }


                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
                $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'], 'searchModel' => $searcModel, 'model' => $model, 'from_date' => $from_date, 'date_to' => $to_date];
                return ($request->isAjax) ? $this->renderAjax('branch_acitvity', $res) :
                    $this->render('branch_acitvity', $res);

            } catch (Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to branch activity", $error, null);
                Yii::$app->session->setFlash('actionFailed', $error);
                return Yii::$app->runAction('/site/error');
            }

        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }


    public function actionSchoolUseractivity()
    {
        $request = Yii::$app->request;

        $activity = '';
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history';
            $model = new DynamicModel(['from_date', 'to_date']);
//            $model->addRule(['from_date', 'to_date'], 'required');


            try {
//                if ($model->load(Yii::$app->request->post()) && $model->validate()) {

//                    $from_date = date("Y-m-d ", strtotime($model->from_date));
//                    // $to_date = date("Y-m-d", strtotime($model->to_date));
//                    $to_date = date('Y-m-d', strtotime($model->to_date . "+1 days"));
//


                $activity = Yii::$app->db->createCommand('
select school_name,ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser,  ttusers-actuser as inactive from

(select tt.id,tt.school_name, tt.ttusers, tt.branch_id, tt.region, tt.branch_region from
(select cs.id, cs.school_name, count(u.school_id)  as ttusers , cs.branch_id, cs.region, cs.branch_region  from "user" u inner 
    join core_school cs on cs.id = u.school_id 
where u.user_level <> :student
group by cs.school_name , cs.id)tt )tz

inner join

(select count(tr.school_id) as actuser ,tr.school_id from
(select distinct( user_name ) , school_id from web_console_log wcl )tr
group by tr.school_id )ty

on ty.school_id = tz.id

inner join awash_branches ab on ab.id  = tz.branch_id
inner join ref_region rr  on rr.id  = tz.region
inner join awash_bank_region abr on abr.id=tz.branch_region
')
                    ->bindValue(':student', 'is_student')
//                        ->bindValue(':end_date', $to_date)
                    ->queryAll();


                // }
            } catch (Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to user activity", $error, null);
                Yii::$app->session->setFlash('actionFailed', $error);
                return Yii::$app->runAction('/site/error');
            }

            return ($request->isAjax) ? $this->renderAjax('schuser_acitvity', [
                'dataProvider' => $activity, 'model' => $model,]) :
                $this->render('schuser_acitvity', ['dataProvider' => $activity, 'model' => $model,
                ]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }

    public function actionStdUseractivity()
    {
        $request = Yii::$app->request;

        $activity = '';
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history';
            $model = new DynamicModel(['from_date', 'to_date']);
//            $model->addRule(['from_date', 'to_date'], 'required');


            try {
//                if ($model->load(Yii::$app->request->post()) && $model->validate()) {

//                    $from_date = date("Y-m-d ", strtotime($model->from_date));
//                    // $to_date = date("Y-m-d", strtotime($model->to_date));
//                    $to_date = date('Y-m-d', strtotime($model->to_date . "+1 days"));
//


                $activity = Yii::$app->db->createCommand('
select ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser,
       ttusers-actuser as inactive,ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers, count(cs.school_id) as stds from

(select tt.id,tt.school_name, tt.ttusers, tt.branch_id, tt.region, tt.branch_region from
(select cs.id, cs.school_name, count(u.school_id)  as ttusers , cs.branch_id, cs.region, cs.branch_region  
from "user" u inner join core_school cs on cs.id = u.school_id 
where u.user_level =:student
group by cs.school_name , cs.id)tt )tz

inner join

(select count(tr.school_id) as actuser ,tr.school_id from
(select distinct(username ) , u.school_id from "user" u inner join web_console_log wcl on u.school_id = wcl.school_id 
where u.user_level =:student )tr
group by tr.school_id )ty

on ty.school_id = tz.id
inner join core_student cs on cs.school_id = tz.id
inner join awash_branches ab on ab.id  = tz.branch_id
inner join ref_region rr  on rr.id  = tz.region
inner join awash_bank_region abr on abr.id=tz.branch_region
group by ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser

')
                    ->bindValue(':student', 'is_student')
//                        ->bindValue(':end_date', $to_date)
                    ->queryAll();
                // }


            } catch (Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to user activity", $error, null);
                Yii::$app->session->setFlash('actionFailed', $error);
                return Yii::$app->runAction('/site/error');
            }

            return ($request->isAjax) ? $this->renderAjax('stduser_acitvity', [
                'dataProvider' => $activity, 'model' => $model,]) :
                $this->render('stduser_acitvity', ['dataProvider' => $activity, 'model' => $model,
                ]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }

    public function actionSchoolStatus()
    {
        $request = Yii::$app->request;

        $activity = '';
        if (Yii::$app->user->can('payments_received_list')) {
            $_SESSION['PENALTY_MODE'] = 'history';
            $model = new DynamicModel(['from_date', 'to_date']);
//            $model->addRule(['from_date', 'to_date'], 'required');


            try {
//                if ($model->load(Yii::$app->request->post()) && $model->validate()) {

//                    $from_date = date("Y-m-d ", strtotime($model->from_date));
//                    // $to_date = date("Y-m-d", strtotime($model->to_date));
//                    $to_date = date('Y-m-d', strtotime($model->to_date . "+1 days"));
//
                $request = Yii::$app->request;
                if (!Yii::$app->session->isActive) {
                    session_start();
                }

                $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-90 days")) : date('Y-m-d', strtotime($_GET['date_from'] . ' -90 days'));
                Yii::trace($from_date);


                $activity = Yii::$app->db->createCommand(" SELECT tr.ids, tr.date_created, tr.last_transaction_date, tr.external_school_code, tr.school_name, ab.branch_name, tr.status,
abr.bank_region_name, ref.description, count(cs2.school_id) AS Stds from
(select cs.id as ids,cs.date_created, gl.last_transaction_date, cs.id,
cs.external_school_code, cs.school_name, cs.school_code, cs.branch_id, cs.branch_region, cs.region,
case when gl.last_transaction_date is null or gl.last_transaction_date <= :last_txn_date  then 'INACTIVE' else 'ACTIVE' end as status
from core_school cs full outer join school_account_gl gl on cs.school_account_id =gl.id)tr
INNER JOIN core_student cs2 ON cs2.school_id = tr.ids
INNER JOIN awash_branches ab ON ab.id = tr.branch_id
INNER JOIN ref_region ref ON ref.id = tr.region
INNER JOIN awash_bank_region abr ON abr.id=tr.branch_region
GROUP BY tr.ids, tr.external_school_code, tr.last_transaction_date, tr.school_name,
tr.date_created, tr.date_created, ab.branch_name, abr.bank_region_name, tr.external_school_code, ref.description , tr.status
order by tr.last_transaction_date asc")
                    ->bindValue(':last_txn_date', $from_date)
                    ->queryAll();
                // }
                $pages = 2;

            } catch (Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to user activity", $error, null);
                Yii::$app->session->setFlash('actionFailed', $error);
                return Yii::$app->runAction('/site/error');
            }
            $searcModel = new Reports();
            return ($request->isAjax) ? $this->renderAjax('school_status', [
                'dataProvider' => $activity, 'model' => $model, 'from_date' => $from_date, 'pages' => $pages, 'searchModel' => $searcModel]) :
                $this->render('school_status', ['dataProvider' => $activity, 'model' => $model, 'from_date' => $from_date, 'pages' => $pages, 'searchModel' => $searcModel
                ]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }

    public function getColz()
    {
        return [
            'bank_region_name' => ['label' => 'Country Region'],
            'description' => ['label' => 'Region'],
            'branch_name' => ['label' => 'Branch '],
            'school_name' => ['label' => 'School Nme '],
            'stds' => ['label' => 'Number of Students '],

        ];
    }


    public function actionBankStatementPdfexl($export)
    {


        $json = false;
        $activity = '';
        $error = '';

        $model = new DynamicModel(['school', 'acc', 'from_date', 'to_date']);
        $model->addRule(['acc', 'from_date', 'to_date'], 'required')
            ->addRule(['school'], 'safe');

        if ($model->from_date) {


            $from_date = date("Y-m-d ", strtotime($model->from_date));
            // $to_date = date("Y-m-d", strtotime($model->to_date));
            $to_date = date('Y-m-d', strtotime($model->to_date . "+1 days"));


            $activity = Yii::$app->db->createCommand("
                                   select *, active_schools+inactive_schools as total_schools
from (
select branch_name, description, bank_region_name,count(inactive) as inactive_schools, 
count(active) as active_schools
from (
select school_name,branch_name, description, 
count(pid) as number_of_transactions ,
case when count(pid)=0 then true else null end as inactive ,
case when count(pid)>0 then true else null end as active 
from
(
select si.school_name, pr.id as pid , ab.branch_name , rr.description ,bank_region_namefrom 
(select * from payments_received where date_created>= :from_date and date_created < :end_date) pr 
right join core_school si on si.id = pr.school_id 
inner join awash_branches ab on ab.id  = si.branch_id
inner join ref_region rr  on rr.id  = si.region
inner join awash_bank_region abr on abr.id=si.branch_region


)A 

group by school_name,branch_name, description,bank_region_name
) P group by branch_name, description,bank_region_name
) F;")
                ->bindValue(':from_date', $from_date)
                ->bindValue(':end_date', $to_date)
                ->queryAll();

        }

        $result = ['model' => $model, 'dataProvider' => $activity, 'error' => $error, 'type' => $export, 'json' => $json];
        $html = $this->renderPartial('extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/awash_logo2.png');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'Bank Statement (' . $model->from_date . ' - ' . $model->to_date . ')', 'school_bank_statement', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_bank_statement" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }

    public function actionStdUserActivityPdfexl($export)
    {


        $json = false;
        $activity = '';
        $error = '';


        $activity = Yii::$app->db->createCommand('
                                select ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser,
       ttusers-actuser as inactive,ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers, count(cs.school_id) as stds from

(select tt.id,tt.school_name, tt.ttusers, tt.branch_id, tt.region, tt.branch_region from
(select cs.id, cs.school_name, count(u.school_id)  as ttusers , cs.branch_id, cs.region, cs.branch_region  from "user" u inner join core_school cs on cs.id = u.school_id 
where u.user_level =:student
group by cs.school_name , cs.id)tt )tz

inner join

(select count(tr.school_id) as actuser ,tr.school_id from
(select distinct(username ) , u.school_id from "user" u inner join web_console_log wcl on u.school_id = wcl.school_id 
where u.user_level =:student )tr
group by tr.school_id )ty

on ty.school_id = tz.id
inner join core_student cs on cs.school_id = tz.id
inner join awash_branches ab on ab.id  = tz.branch_id
inner join ref_region rr  on rr.id  = tz.region
inner join awash_bank_region abr on abr.id=tz.branch_region
group by ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser')
            ->bindValue(':student', 'is_student')
            ->queryAll();


        $result = ['dataProvider' => $activity, 'error' => $error, 'type' => $export, 'json' => $json];
        $html = $this->renderPartial('std_user_extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/awash_logo2.png');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'school_users', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_users" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }

    public function actionBranchActivityPdfexl($from_date, $date_to)
    {

        $mod = $from_date;
        $to = $date_to;
        $export = 'excel';

        $json = false;
        $activity = '';
        $error = '';


        $activity = Yii::$app->db->createCommand('
                            select *, active_schools+inactive_schools as total_schools
from (
select branch_name, description,bank_region_name, count(inactive) as inactive_schools, 
count(active) as active_schools
from (
select school_name,branch_name, description, bank_region_name,
count(pid) as number_of_transactions ,
case when count(pid)=0 then true else null end as inactive ,
case when count(pid)>0 then true else null end as active 
from
(
select si.school_name, pr.id as pid , ab.branch_name , rr.description,abr.bank_region_name from 
(select * from payments_received where date_created>= :from_date and date_created < :end_date) pr 
right join core_school si on si.id = pr.school_id 
inner join awash_branches ab on ab.id  = si.branch_id
inner join ref_region rr  on rr.id  = si.region
inner join awash_bank_region abr on abr.id=si.branch_region
)A 

group by school_name,branch_name, description,bank_region_name
) P group by branch_name, description,bank_region_name
) F;')
            ->bindValue(':from_date', $mod)
            ->bindValue(':end_date', $to)
            ->queryAll();


        $result = ['dataProvider' => $activity, 'error' => $error, 'type' => $export, 'json' => $json, 'mod' => $mod, 'to' => $to];
        $html = $this->renderPartial('branch_extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/awash_logo2.png');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'school_users', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_users" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }

    public function actionSchoolStatusPdfexl($from_date)
    {


        $export = 'excel';

        $json = false;
        $activity = '';
        $error = '';


        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-90 days")) : date('Y-m-d', strtotime($_GET['date_from'] . ' -90 days'));
        Yii::trace($from_date);


        $activity = Yii::$app->db->createCommand(" SELECT tr.ids, tr.date_created, tr.last_transaction_date, tr.external_school_code, tr.school_name, ab.branch_name, tr.status,
abr.bank_region_name, ref.description, count(cs2.school_id) AS Stds from
(select cs.id as ids,cs.date_created, gl.last_transaction_date, cs.id,
cs.external_school_code, cs.school_name, cs.school_code, cs.branch_id, cs.branch_region, cs.region,
case when gl.last_transaction_date is null or gl.last_transaction_date <= :last_txn_date  then 'inactive' else 'active' end as status
from core_school cs full outer join school_account_gl gl on cs.school_account_id =gl.id)tr
INNER JOIN core_student cs2 ON cs2.school_id = tr.ids
INNER JOIN awash_branches ab ON ab.id = tr.branch_id
INNER JOIN ref_region ref ON ref.id = tr.region
INNER JOIN awash_bank_region abr ON abr.id=tr.branch_region
GROUP BY tr.ids, tr.external_school_code, tr.last_transaction_date, tr.school_name,
tr.date_created, tr.date_created, ab.branch_name, abr.bank_region_name, tr.external_school_code, ref.description , tr.status
order by tr.last_transaction_date desc")
            ->bindValue(':last_txn_date', $from_date)
            ->queryAll();


        $result = ['dataProvider' => $activity, 'error' => $error, 'type' => $export, 'json' => $json, 'mod' => $from_date,];
        $html = $this->renderPartial('school_status_extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/awash_logo2.png');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'school_users', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_status" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }


    public function actionUserActivityPdfexl($export)
    {


        $json = false;
        $activity = '';
        $error = '';


        $activity = Yii::$app->db->createCommand('
                                 select ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser,  ttusers-actuser as inactive from

(select tt.id,tt.school_name, tt.ttusers, tt.branch_id, tt.region, tt.branch_region from
(select cs.id, cs.school_name, count(u.school_id)  as ttusers , cs.branch_id, cs.region, cs.branch_region  from "user" u inner join core_school cs on cs.id = u.school_id 
where u.user_level <> :student
group by cs.school_name , cs.id)tt )tz

inner join

(select count(tr.school_id) as actuser ,tr.school_id from
(select distinct( user_name ) , school_id from web_console_log wcl )tr
group by tr.school_id )ty

on ty.school_id = tz.id

inner join awash_branches ab on ab.id  = tz.branch_id
inner join ref_region rr  on rr.id  = tz.region
inner join awash_bank_region abr on abr.id=tz.branch_region')
            ->bindValue(':student', 'is_student')
            ->queryAll();


        $result = ['dataProvider' => $activity, 'error' => $error, 'type' => $export, 'json' => $json];
        $html = $this->renderPartial('sch_user_extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/awash_logo2.png');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'school_users', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_users" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }

    // student info report export to pdf and excel
    public function actionSummaryPerfList()
    {
        if (!Yii::$app->session->isActive) {
            session_start();
        }

        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from'] : date('Y-m-d', strtotime("-30 days"));
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date . "+1 days"));

        $student_data = Yii::$app->db->createCommand("select  trans.school_name ,trans.school_code,count(std.school_id) as number_of_students, trans.val as transaction_value  from
(select  si.id as schId, si.school_name, si.school_code, count(si.school_name) ,  sum(pr.amount) as val from 
payments_received pr inner join core_school si on pr.school_id = si.id
LEFT JOIN auto_settlement_requests asr on pr.id = asr.payment_id
WHERE pr.date_created >=:start_date AND pr.date_created < :end_date
AND pr.reversal = false AND pr.reversed = false --Exclude reversals and reversed transactions
GROUP BY si.school_name, si.id ,si.school_code
ORDER BY sum(pr.amount) DESC,
count(si.school_name) desc)trans
inner join core_student std  on std.school_id  = trans.schId
group by  trans.school_name , trans.val , std.school_id ,trans.school_code
order by  trans.val desc;")
            ->bindValue(':start_date', $from_date)
            ->bindValue(':end_date', $end_date)
            ->queryAll();

        $search_time = ['to' => $to_date, 'from' => $from_date];
        $sum = $count = 0;
        foreach ($student_data as $v) {
            $sum = $sum + $v['transaction_value'];
            $count = $count + $v['number_of_students'];
        }
        if (!empty($_REQUEST['summerylistexport'])) {
            $html = $this->renderPartial('summary_perf_report_pdf', ['student_data' => $student_data, 'query' => $search_time, 'sum' => $sum, 'count' => $count]);

            ob_clean();
            return Yii::$app->pdf->exportData('School Performance Report', 'summery_report', $html);
        } elseif (!empty($_REQUEST['summerylistexcelexport'])) {

            $file = $this->renderPartial('summary_perf_report_excel', ['student_data' => $student_data, 'query' => $search_time, 'sum' => $sum, 'count' => $count]);
            $fileName = "school_performance_report" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }

    }


    public function actionSchoolsstatus()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $searcModel = new Reports();


        $data = $searcModel->schoolStatus( Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel,'model'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('school_status', $res) :
            $this->render('school_status', $res);


    }
    public function actionStudentUsers()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $searcModel = new Reports();


        $data = $searcModel->stdUserz( Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel,'model'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('stduser_acitvity', $res) :
            $this->render('stduser_acitvity', $res);


    }
    public function actionSchoolUsers()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $searcModel = new Reports();


        $data = $searcModel->schUserz( Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel,'model'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('schuser_acitvity', $res) :
            $this->render('schuser_acitvity', $res);


    }

}

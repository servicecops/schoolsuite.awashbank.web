<?php
/**
 * Created by IntelliJ IDEA.
 * User: GOGO
 * Date: 3/2/2020
 * Time: 4:30 PM
 */

namespace app\modules\schoolcore\controllers;
use app\controllers\BaseController;
use app\modules\gradingcore\models\Grading;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreSchoolClassSearch;
use app\modules\schoolcore\models\CoreStudent;
use kartik\mpdf\Pdf;
use Yii;

use yii\base\DynamicModel;
use yii\base\UserException;
use yii\console\Exception;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Response;


if (!Yii::$app->session->isActive) {
    session_start();
}

class CoreSchoolClassController extends BaseController
{

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function newModel()
    {
        // TODO: Implement newModel() method.
        return new CoreSchoolClassSearch();
    }

    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new CoreSchoolClassSearch();
    }

    public function createModelFormTitle()
    {
        return 'New Student Class';
    }

    public function actionLists($id = 0)
    {
        if(!$id) $id = 0;
        $countClasses = CoreSchoolClass::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'class_code', '__ARCHIVE__'])
            ->count();

        $classes = CoreSchoolClass::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'class_code', '__ARCHIVE__'])
            ->orderBy('class_code')
            ->all();

        if ($countClasses > 0) {
//            Yii::trace($classes);
            echo "<option value=''> All Classes </option>";

            foreach ($classes as $myClass) {
                echo "<option value='" . $myClass->id . "'>" . $myClass->class_description . "</option>";
            }
        } else {
            echo "<option value=''> No classes found </option>";
        }
    }


    /**
     * Creates a new CoreSchoolClass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $request = Yii::$app->request;
//        $model = new CoreSchoolClass();
//        $post = $model->load(Yii::$app->request->post());
//
//
//        if ($model->load(Yii::$app->request->post())) {
//
//            $transaction = CoreSchoolClass::getDb()->beginTransaction();
//            try {
//                //$newId =$this->saveUser();
//                if(\app\components\ToWords::isSchoolUser()){
//                    $model->school_id = Yii::$app->user->identity->school_id;
//                }
//                $model->created_by = Yii::$app->user->identity->getId();
//                $model->save(false);
//                $transaction->commit();
//                //return $this->redirect(['view', 'id' => $model->id]);
//
//                $searchModel = $this->newSearchModel();
//                $schoolData = $searchModel->viewModel($model->id);
//                $modeler = $schoolData['models'];
//                $attributes = $schoolData['attributes'];
//                return $this->render('@app/views/common/modal_view', [
//                    'model' => $modeler,
//                    'attributes' => $attributes
//                ]);
//            } catch (\Exception $e) {
//                $transaction->rollBack();
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                Yii::trace('Error: ' . $error, 'CREATE  STUDENT ROLLBACK');
//            }
//
//        }
//        return ($request->isAjax) ? $this->renderAjax('_class_create', ['model' => $model]) :
//            $this->render('_class_create', ['model' => $model]);
//
////        $formConfig = [
////            'model' => $model,
////            'formTitle' => $this->createModelFormTitle(),
////        ];
////
////        return $this->modelForm($model, $formConfig);
//
//    }

    public function actionCreate()
    {
        $model = new CoreSchoolClass();
        $request = Yii::$app->request;
        Yii::trace($model->load(Yii::$app->request->post()));
        if ($model->load(Yii::$app->request->post())) {

            $post = $model->load(Yii::$app->request->post());
            Yii::trace($post);
            $transaction = CoreSchoolClass::getDb()->beginTransaction();
            try {
                //$newId =$this->saveUser();
                $model->created_by = Yii::$app->user->identity->getId();
                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $model->save(false);


                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel($model->id);
                $modeler = $schoolData['models'];
                $attributes = $schoolData['attributes'];
                return $this->redirect(['view', 'id' => $model->id]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Create  School ROLLBACK');
            }

        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('_class_create', $res) : $this->render('_class_create', $res);

    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;

        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
            if ($model->load(Yii::$app->request->post())) {

                $post = $model->load(Yii::$app->request->post());
                Yii::trace($post);
                $transaction = CoreSchoolClass::getDb()->beginTransaction();
                try {
                    //$newId =$this->saveUser();
                    $model->created_by = Yii::$app->user->identity->getId();
                    $model->save(false);


                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    //Yii::trace('Error: ' . $error, 'Update Class  School ROLLBACK');
                }

            }
            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }
    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreSchoolClassSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }


    public function createModelUpdateFormTitle()
    {
        return "Update School Class";
    }
    public function actionJsonList($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [];
        if ($id && is_numeric($id)) {
            $classes = CoreSchoolClass::find()
                ->where(['school_id' => $id])
                ->andWhere(['<>', 'class_code', '__ARCHIVE__'])
                ->orderBy(['class_code' => SORT_ASC])->all();

            foreach ($classes as $class) {
                $_class = [
                    'id' => $class->id,
                    'class_code' => $class->class_code,
                    'description' => $class->class_description,
                ];
                array_push($response, $_class);
            }
        }

        return json_encode($response);
    }



    public function actionClasslist($q = null, $id = null )
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, class_description AS text')
                ->from('core_school_class')
                ->where(['school_id' => $id])
                ->andWhere(['ilike', 'class_description', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchoolClass::find()->where(['id' => $id])->class_description];
        }
        return $out;
    }
//
//    /**
//     * Displays a single CoreClass model.
//     * @param string $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }


    /**
     * Displays a single Classes model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        $request = Yii::$app->request;
//        $model = $this->findModel($id);
//        if (Yii::$app->user->can('r_class', ['sch' => $model->school_id])) {
//            $findStudents = new CoreSchoolClassSearch();
//            $data = $findStudents->classStudents($id, Yii::$app->request->queryParams);
//            $dataProvider = $data['query'];
//            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
//
//            return ($request->isAjax) ? $this->renderAjax('view', ['model' => $model,
//                'searchModel' => $findStudents, 'dataProvider' => $dataProvider,  'pages' => $pages, 'sort' => $data['sort']]) :
//                $this->render('view', ['model' => $model,
//                    'searchModel' => $findStudents, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
//        } else {
//            throw new ForbiddenHttpException('No Permissions to view a class / course.');
//        }
//
//    }
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_class', ['sch' => $model->school_id]) || !Yii::$app->user->can('non_student')) {
            $findStudents = new CoreSchoolClassSearch();
           // $unebActive = $model->cUnebActive;
            $data = $findStudents->classStudents($id, Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('view', ['model' => $model,
                'searchModel' => $findStudents, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('view', ['model' => $model,
                    'searchModel' => $findStudents, 'dataProvider' => $dataProvider,  'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No Permissions to view a class / course.');
        }

    }


    /**
     * Deletes an existing CoreSchoolClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreSchoolClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreSchoolClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreSchoolClass::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUploadExcel()
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        $model = new DynamicModel(['importFile', 'description', 'school']);
        $model->addRule([ 'description'], 'required')
            ->addRule(['school'], 'integer')
            ->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx']);
        if (\app\components\ToWords::isSchoolUser()) {
            $model->school = Yii::$app->user->identity->school_id;
        }
        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($inputFile && $model->description && $model->school) {
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    throw new Exception("Your file was uploaded but could not be processed. Confirm that its a valid excel file");
                }
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestcolumn();

                $upload_by = Yii::$app->user->identity->getId();

                //Selected class should belong to the selected school
                //This is a data integrity check
                $schoolRecord = CoreSchool::findOne($model->school);

                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                //Get a school model for this school, we wil use it to validate the registration numbers


                try {
                    $sql1 = "INSERT INTO class_upload_file_details (
                              file_name, 
                              hash, 
                              uploaded_by, 
                              date_uploaded, 
                              description, 
                              destination_school 
                              )
                                VALUES (
                              :file_name, 
                              :hash, 
                              :uploaded_by, 
                              NOW(), 
                              :description, 
                              :destination_school
                                )";
                    $fileQuery = $connection->createCommand($sql1);
                    $fileQuery->bindValue(':file_name', $file_name);
                    $fileQuery->bindValue(':hash', $hash);
                    $fileQuery->bindValue(':uploaded_by', $upload_by);
                    $fileQuery->bindValue(':description', $model->description);
                    $fileQuery->bindValue(':destination_school', $model->school);
                    //Insert file
                    $fileQuery->execute();
                    $file_id = $connection->getLastInsertID('class_upload_file_details_id_seq');
                    $numberOfRecords = 1;
                    for ($row = 1; $row <= $highestRow; $row++) {
                        if ($row == 1) {
                            continue;
                        }
                        $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        //Collection information from excel row
                        $excelClassName = !isset($v[0][0]) ? null : trim(ucwords(strtolower($v[0][0])));
                        $excelClassCode = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));



                        if ($excelClassName && $excelClassCode ) {

                            $sql2 = "INSERT INTO class_upload_file_data (
                                        file_id, 
                                        date_created, 
                                        school_id, 
                                        class_name, 
                                        class_code
                          
                                        )
                                      VALUES (
                                        :file_id, 
                                        NOW(), 
                                        :school_id, 
                                        :class_name, 
                                        :class_code
                                      )";

                            //Prepare and populate params
                            $query = $connection->createCommand($sql2);
                            $query->bindValue(':file_id', $file_id);
                            $query->bindValue(':school_id', $model->school);
                            $query->bindValue(':class_name', $excelClassName);
                            $query->bindValue(':class_code', $excelClassCode);
                            //Excecute update now
                            $query->execute();

                            //Increment no of records
                            $numberOfRecords++;
                        } else {
                            $transaction->rollBack();
                            $ln = $excelClassName ? " Class Name: " . $excelClassName : " <b>Class Name: (not set)</b>";
                            $fn = $excelClassCode ? " Class Code: " . $excelClassCode : " <b>Class Code: (not set)</b>";
                            //Logs::logEvent("Failed to upload Students: ", "<b>Fix Row - " . $row . "</b> " . $ln . $fn . $dob . $gender . $country . $reg . $rpRegNo, null);
                            return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . $fn ." </p></span>";
                        }
                    }

                    $imported = (new Query())->select(['imported', 'uploaded_by'])->from('class_upload_file_details')->where(['id' => $file_id])->limit(1)->one();
                    $transaction->commit();
                    //Logs::logEvent("Temporary Students imported for class: " . $model->student_class, null, null);

                    //Send email
                    $subject = "New class File Uploaded $file_name";
                    $emailMessage = "New file uploaded for $schoolRecord->school_name \n
                    Uploaded by: " . Yii::$app->user->identity->username . "\n
                    File Hash: $hash \n
                    Number of records: $numberOfRecords";

                    //  $uploadEmailRecipients = Yii::$app->params['fileUploadedEmailRecipients'];

                    // ToWords::sendEmail($uploadEmailRecipients, $class, $emailMessage);

                    return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    // Logs::logEvent("Failed to upload Students: ", $error, null);
                    return "<div class='alert alert-danger' >Excel file File upload failed<br><br>
                    <p>" . $error . "</p> </div>";
                }

            } else {
                return "<div class='alert alert-danger' >File Or Description can't be blank</div>";
            }


        } else {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }
    }

    protected
    function fileData($id)
    {
        $query = new Query();
        $query->select(['fd.id', 'fd.class_name', 'fd.class_code','csc.school_name'])
            ->from('class_upload_file_data fd')
            ->innerJoin('core_school csc', 'fd.school_id= csc.id')
            ->where(['file_id' => $id])
            ->orderBy('class_name')
            ->limit(1000);
        return $query->all();
    }

    protected function fileDetail($id)
    {
        $query = (new Query())->select(['imported', 'uploaded_by', 'description', 'sch.school_name', 'destination_school'])->from('class_upload_file_details fd')->innerJoin('core_school sch', 'sch.id=fd.destination_school')->where(['fd.id' => $id])->limit(1)->one();
        return $query;
    }

    public function actionFileDetails($id)
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'save') {
                $status = $this->saveClasses($id);
            } else if ($action == 'del_student' && $_POST['stu']) {
                $status = $this->deleteRecord($_POST['stu']);
            }
        }

        return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]);
    }

    protected function saveClasses($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        $select = "class_name , class_code,  school_id ";
        try {

            $sql2 = "UPDATE class_upload_file_details SET imported = true WHERE id = :fileid";
            $temp_class = $connection->createCommand("SELECT " . $select . " FROM class_upload_file_data WHERE file_id = :fileid", [
                ':fileid' => $id
            ])->queryAll();
            Yii::trace($temp_class);
            $userId = Yii::$app->user->identity->getId();
            foreach ($temp_class as $record) {

                $connection->createCommand("INSERT INTO core_school_class (class_description , class_code,  school_id, created_by) 
                              VALUES(:class_name  ,:class_code ,:school_id ,$userId)")
                    ->bindParam("class_name", $record['class_name'])
                    ->bindParam("class_code", $record['class_code'])
                    ->bindParam("school_id", $record['school_id'])
                    ->execute();


            }
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            Yii::trace($temp_class);
            Yii::trace($connection);
            $transaction->commit();
            Logs::logEvent("New Students imported from file id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>class successfully imported</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
             Logs::logEvent("Failed to import Students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    /**
     *
     * /**
     * Updates an existing CoreStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /**
     * Updates an existing Submission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdateGrade($id)
    {
        if(!\app\components\ToWords::isSchoolUser()){
            throw new ForbiddenHttpException('You have no permissions to view this area');
        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->grading = $model->getRetrieveGrades();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction = CoreSchoolClass::getDb()->beginTransaction();
            try {
                //Yii::trace("txn after");
               // $model->uploads = UploadedFile::getInstances($model, 'uploads');
               // $model->save(false);
                $model->updateGrades();
                //$model->saveUploads();
                $transaction->commit();
                Yii::trace('Submission updated successfully', 'SUBMISSION');
                return Yii::$app->runAction('/gradingcore/grading/index');
             //   return Yii::$app->runAction('/feesdue/fees-due/details', ['id' => $id]);

            } catch (\Exception $e){
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: '.$error, 'SUBMISSION ROLLBACK');
            }
        }
        $res = ['model' => $model];
        return $request->isAjax ? $this->renderAjax('update_grades', $res) : $this->render('update_grades', $res);


    }

    public function actionAddGrades()
    {
        if (\app\components\ToWords::isSchoolUser()) {
            $request = Yii::$app->request;

            $model = new CoreSchoolClass();

            Yii::trace(Yii::$app->request->post());
            if ($model->load(Yii::$app->request->post())) {


                try {
                    $schoolId = Yii::$app->user->identity->school_id;
                    $cls = (new Query())
                        ->select(['class_id'])
                        ->from('core_grades')
                        ->where(['school_id' => $schoolId]);

                    Yii::trace("$cls".$cls);

                    $data = Yii::$app->request->post();
                  //  Yii::trace($data);
                    $model->class_id = $data['CoreSchoolClass']['class_id'];
//            $feeClas =array();
                    $class = $model->class_id;
                    Yii::trace($class);

                    if (!empty($class)) {
                        $classs = array_unique($class);
                        Yii::trace($classs);
                        $del_class = [];

                            foreach ($classs as $class) {
//

                                Yii::trace($class);
                                //$model->saveGrades();

                                $model->grading = $data['CoreSchoolClass']['grading'];

                                if(is_array($model->grading) && $model->grading){

                                    foreach ($model->grading as $k=>$v) {
                                        Yii::trace($model->grading);

                                        $grade = new Grading();
                                        //check if grade exists

                               $existingGrade = Grading::find()->orWhere(['grades'=>trim(strtoupper($v['grades'])), 'class_id'=>$class, 'min_mark'=> $v['min'], 'max_mark'=>$v['max'], 'comment'=>$v['comment']])->limit(1)->one();

                                        if($existingGrade){
                                            $className = CoreSchoolClass::find()->where(['id'=>$class])->limit(1)->one();

                                            throw new UserException("Grade with one of the items ".$v['grades'].",".$v['min']." to ".$v['max']."(".$v['comment'].")". "  already exists for ".$className->class_description);
                                        }

                                        $grade->created_by = Yii::$app->user->identity->getId();
                                        if(\app\components\ToWords::isSchoolUser()){
                                            $grade->school_id = Yii::$app->user->identity->school_id;
                                        }else{
                                            $grade->school_id = $this->school_id;
                                        }


                                        $grade->min_mark = $v['min'];
                                        $grade->max_mark = $v['max'];
                                        $grade->comment = $v['comment'];
                                        $grade->grades = trim(strtoupper($v['grades']));
                                        $grade->class_id = $class;
                                        $grade->save(false);
                                        Yii::trace('Saved grade ' . $grade->grades);
                                    }
                                }
                            }
                       // }

                    }


                } catch (\Exception $e) {

                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Yii::trace('Error: ' . $error, 'CREATE  GRADES ROLLBACK');
                    Logs::logEvent("Failed to create grade: ", $error, null);
                    \Yii::$app->session->setFlash('actionFailed', $error);
                    Yii::trace($error);
                    return \Yii::$app->runAction('schoolcore/core-staff/error');

                }

                ?>"<div class='alert alert-success'>Grades successfully created</div>";
                <?php

                return $this->redirect(['/gradingcore/grading/index']);
            }

            return ($request->isAjax) ? $this->renderAjax('_thegrading') :
                $this->render('_thegrading',['model' =>$model]);

        } else {
            throw new ForbiddenHttpException('No permissions to add Grades');
        }


    }

    /**
     * Load the promotion studion
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionPromotionStudio($schoolId = null)
    {
        $request = Yii::$app->request;
        //TODO implement specific permission for promotion studio
        if (!Yii::$app->user->can('rw_class'))
            throw new ForbiddenHttpException('Insufficient privileges to access the promotion studio');

        if ($request->post()) {
            return $this->processPromotionStudioRequest($schoolId);
        }
        $res = [];
        return ($request->isAjax) ? $this->renderAjax('promotion_studio', $res) :
            $this->render('promotion_studio', $res);
    }

    protected function processPromotionStudioRequest($schoolId)
    {
        if (\app\components\ToWords::isSchoolUser())
            $schoolId = Yii::$app->user->identity->school_id;

        $request = Yii::$app->request;
        $sourceList = $_POST['SourceClass'];
        $destinationList = $_POST['DestinationClass'];

        Yii::trace($sourceList);
        Yii::trace($destinationList);

        $processError = '';
        $processSummary = [];

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {

            $school = CoreSchool::findOne(['id' => $schoolId]);
            if(!$school)
                throw new \Exception('School not found while updating details');

            for ($i = 0; $i < sizeof($sourceList); $i++) {
                $_sourceClassId = $sourceList[$i];
                $_destinationClassId = $destinationList[$i];

                //If source is empty, just continue
                if (!$_sourceClassId)
                    continue;


                //Get the classes
                $sourceClass = CoreSchoolClass::findOne(['id' => $_sourceClassId, 'school_id' => $schoolId]);
                if (!$sourceClass)
                    throw new Exception('Source class was not found while promoting - ' . $_sourceClassId);


                if(!$_destinationClassId)
                    throw new Exception('You did not select a destination class in which to promote students of class ' . $sourceClass->class_description);

                //If to archive
                if($_destinationClassId === '___ARCHIVE___') {
                    //Archive students for this class
                    $studentsToArchive = CoreStudent::findAll(['school_id'=>$schoolId, 'class_id'=>$_sourceClassId]);
                    $numberOfStudentsMoved = 0;

                    $user_id = Yii::$app->user->identity->id;
                    $user_name = Yii::$app->user->identity->username;
                    $user_ip = Yii::$app->request->userIP;

                    foreach ($studentsToArchive as $theStudent) {
                        $connection->createCommand("select archive_student(
                        :this_student_id,  
                        :reason, 
                        :webuser, 
                        :username, 
                        :userip)",
                            [
                                ':this_student_id'=>$theStudent->id,
                                ':reason'=>'Archived as part of promotion by: ' . $user_name,
                                ':webuser'=>$user_id,
                                ':username'=>$user_name,
                                ':userip'=>$user_ip
                            ])->execute();
                        $numberOfStudentsMoved ++;
                    }

                    $processSummary[] = [
                        'fromClass' => $sourceClass,
                        'toClass' => null,
                        'toArchive' => true,
                        'numberOfStudents' => $numberOfStudentsMoved
                    ];
                    continue;
                }

                $destinationClass = CoreSchoolClass::findOne(['id' => $_destinationClassId, 'school_id' => $schoolId]);


                if (!$destinationClass)
                    throw new Exception('Destination class was not found while promoting - ' . $_destinationClassId);

                //Now promote
                $sql = "UPDATE core_student set class_id = :new_class where school_id = :school and class_id=:old_class";

                $numberOfStudentsMoved = $connection->createCommand($sql)
                    ->bindValue(':new_class', $destinationClass->id)
                    ->bindValue(':school', $schoolId)
                    ->bindValue(':old_class', $sourceClass->id)
                    ->execute();

                Logs::logEvent("Moved all students of $school->school_name from $sourceClass->class_description to $destinationClass->class_description", $processError, null);

                $processSummary[] = [
                    'fromClass' => $sourceClass,
                    'toClass' => $destinationClass,
                    'numberOfStudents' => $numberOfStudentsMoved
                ];

            }



            $transaction->commit();
        } catch (\Exception $e) {
            $processSummary = []; //In case of error, clear the process results
            Yii::trace($e->getTraceAsString());
            $transaction->rollBack();
            $processError = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to process promotion studio request for school id $schoolId", $processError, null);

            $res = ['processError' => $processError, 'processSummary'=>$processSummary, 'school'=>$school];
            return ($request->isAjax) ? $this->renderAjax('promotion_studio_results', $res) :
                $this->render('promotion_studio_results', $res);
        }

        $res = ['processError' => null, 'processSummary'=>$processSummary, 'school'=>$school];
        return ($request->isAjax) ? $this->renderAjax('promotion_studio_results', $res) :
            $this->render('promotion_studio_results', $res);
    }

    public function actionClasses($id)
    {
        $cls = CoreSchoolClass::find()
            ->where(['school_id' => $id])
            ->all();
        Yii::trace($cls);
        if (!empty($cls)) {

            echo '<option value="">Select district</option>';
            foreach ($cls as $class) {
                echo "<option value='" . $class->id . "'>" . $class->class_description . "</option>";
            }
        } else {
            echo "<option>No class found</option>";
        }

    }

    public function actionMenu(){
        //return $this->render('class_menu');
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('class_menu') :
            $this->render('class_menu');
    }


    public function actionExportToPdf($model)
    {

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial($data['exportFile'], ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print Class',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => [$data['title'] . '||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

}

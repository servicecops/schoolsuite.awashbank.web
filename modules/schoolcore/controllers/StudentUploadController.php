<?php

namespace app\modules\schoolcore\controllers;

use app\components\ToWords;

use app\modules\logs\models\Logs;
use app\modules\paymentscore\models\StudentAccountHistorySearch;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;

use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * CoreStudentController implements the CRUD actions for CoreStudent model.
 */
class StudentUploadController extends Controller
{
    public $password;

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUploadExcel()
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        $model = new DynamicModel(['importFile', 'description', 'school', 'student_class', 'class_import_behaviour', 'campus_id']);
        $model->addRule(['description', 'class_import_behaviour', 'importFile', 'school'], 'required')
            ->addRule(['school'], 'integer')
            ->addRule(['student_class', 'importFile', 'campus_id'], 'safe')
            ->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx']);
        if (\app\components\ToWords::isSchoolUser()) {
            $model->school = Yii::$app->user->identity->school_id;
        }

        if (!Yii::$app->request->post()) {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }

        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }


        if (!$model->load(Yii::$app->request->post()) || !$model->validate(['description', 'school', 'student_class', 'class_import_behaviour'])) {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }

        $schoolRecord = CoreSchool::findOne($model->school);
//If school has campuses module, then campus is required
        if ($schoolRecord->hasModule('SCHOOL_CAMPUSES') && !$model->campus_id) {
            $model->addError('campus_id', 'Please select a campus');
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }

        if (!is_numeric($model->campus_id)) $model->campus_id = null;


        $useClassesFromExcel = $model->class_import_behaviour == 'FROM_EXCEL';

        if (!$useClassesFromExcel && !$model->student_class) {
            $model->addError('student_class', 'Student class to import records to is required');
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }

//If from excel, set student class to 0
        if ($useClassesFromExcel) {
            $model->student_class = '0';
        }


//Get a school model for this school, we wil use it to validate the registration numbers


        $transaction = null;
        try {
            try {
                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFile);
            } catch (Exception $e) {
                Yii::trace($e);
                throw new Exception("Your file was uploaded but could not be processed. Confirm that its a valid excel file");
            }
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestcolumn();

            $upload_by = Yii::$app->user->identity->id;

//Selected class should belong to the selected school
//This is a data integrity check

//If importing to single exisitng, validate as school against this class
            if (!$useClassesFromExcel) {
                $classRecord = CoreSchoolClass::findOne($model->student_class);
                if (!$classRecord) throw new Exception("Operation forbidden: Selected class does not exist");
                if ($schoolRecord->id != $classRecord->school_id) {
                    throw new Exception("Operation forbidden: Selected class should belong to the selected school");
                }
            }


            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();

            $sql1 = "INSERT INTO student_upload_file_details (
file_name,
hash,
uploaded_by,
date_uploaded,
description,
destination_school,
destination_class,
student_campus
)
VALUES (
:file_name,
:hash,
:uploaded_by,
NOW(),
:description,
:destination_school,
:destination_class,
:student_campus
)";
            $fileQuery = $connection->createCommand($sql1);
            $fileQuery->bindValue(':file_name', $file_name);
            $fileQuery->bindValue(':hash', $hash);
            $fileQuery->bindValue(':uploaded_by', $upload_by);
            $fileQuery->bindValue(':description', $model->description);
            $fileQuery->bindValue(':destination_school', $model->school);
            $fileQuery->bindValue(':destination_class', $model->student_class);
            $fileQuery->bindValue(':student_campus', $model->campus_id);
//Insert file
            $fileQuery->execute();
            $file_id = $connection->getLastInsertID('student_upload_file_details_id_seq');
            $regNos = [];
            $numberOfRecords = 1;
            for ($row = 1; $row <= $highestRow; $row++) {
                if ($row == 1) {
                    continue;
                }
                $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
//Collection information from excel row
                $excelFirstName = !isset($v[0][0]) ? null : trim(ucwords(strtolower($v[0][0])));
                $excelMiddleName = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));
                $excelLastName = !isset($v[0][2]) ? null : trim(ucwords(strtolower($v[0][2])));
                $excelDob = !isset($v[0][3]) ? null : $v[0][3];
                $excelGender = !isset($v[0][5]) ? null : strtoupper(trim($v[0][5]));
                $excelNationality = !isset($v[0][8]) ? null : $v[0][8];
                $excelDisability = !isset($v[0][9]) ? null : $v[0][9];

                $excelRegistrationNumber = !isset($v[0][4]) ? null : trim(strtoupper($v[0][4]));
//Remove spaces from registration numbers
                if ($excelRegistrationNumber != null) $excelRegistrationNumber = str_replace(' ', '', $excelRegistrationNumber);

                $excelStudentEmail = !isset($v[0][6]) ? null : trim($v[0][6]);
                $excelStudentPhone = !isset($v[0][7]) ? null : trim($v[0][7]);
                $excelDisabilityNature = !isset($v[0][10]) ? null : trim(ucwords($v[0][10]));

                $excelGuardianName = !isset($v[0][11]) ? null : trim(ucwords(strtolower($v[0][11])));
                $excelGuardianRelation = !isset($v[0][12]) ? null : trim(ucwords($v[0][12]));
                $excelGuardianEmail = !isset($v[0][13]) ? null : trim($v[0][13]);
                $excelGuardianPhone = !isset($v[0][14]) ? null : trim($v[0][14]);

                $excelGuardianName2 = !isset($v[0][15]) ? null : trim(ucwords(strtolower($v[0][15])));
                $excelGuardianRelation2 = !isset($v[0][16]) ? null : trim(ucwords($v[0][16]));
                $excelGuardianEmail2 = !isset($v[0][17]) ? null : trim($v[0][17]);
                $excelGuardianPhone2 = !isset($v[0][18]) ? null : trim($v[0][18]);

                $excelDayBoarding = !isset($v[0][19]) ? null : trim(strtoupper($v[0][19]));

                if (!$excelDayBoarding)
                    throw new Exception(" Specify either D or B for Day boarding. Check record:  $numberOfRecords");

                $recordStudentBalance = $v[0][20];
                $excelStudentBalance = 0;
                if (isset($recordStudentBalance)) {
                    if (!is_numeric($recordStudentBalance)) {
//Balance ought to be  numeric
                        throw new Exception(" Invalid student balance. The student balance for student on record $numberOfRecords should be numeric");
                    }
                    $excelStudentBalance = abs($recordStudentBalance); //Save as absolute
                }

                $excelStudentClassCode = !isset($v[0][21]) ? null : trim($v[0][21]);
                if ($excelStudentClassCode) {
//Remove spaces and change to upper case
                    $excelStudentClassCode = str_replace(' ', '', strtoupper($excelStudentClassCode));
                }
                if ($useClassesFromExcel && !$excelStudentClassCode) {
                    throw new Exception(" Student Class Not Specified for a record $numberOfRecords");
                }

//Check that both names are present
                if (empty($excelFirstName) || empty($excelLastName)) {
                    throw new Exception("Both first name and last name are required. Check record $numberOfRecords");
                }

//Validate gender
                if ($excelGender != 'M' && $excelGender != 'F') {
                    throw new Exception("Specify M or F for gender. For student $excelLastName $excelFirstName.  Check record $numberOfRecords");
                }


                if (empty($excelNationality)) {
                    throw new Exception("Nationality is required. Check record $numberOfRecords");
                }

                if (!ToWords::validateDate($excelDob, 'Y-m-d')) {
                    throw new Exception("Specify a valid date of birth in the format YYYY-mm-dd. For student $excelLastName $excelFirstName");
                }

                if (!empty($excelDayBoarding) && $excelDayBoarding != 'D' && $excelDayBoarding != 'B') {
                    throw new Exception("Specify D or B for day boarding field. For student $excelLastName $excelFirstName");
                }

                if (empty($excelRegistrationNumber)) {
                    throw new Exception("Student code / registration number is required. For student $excelLastName $excelFirstName");
                }


                $disability = ($excelDisability) ? 'TRUE' : 'FALSE';

//Validate reg no for this school
                $schoolRecord->validateRegNoAgainstSchoolFormat($excelRegistrationNumber);

                $query = $excelRegistrationNumber ? (new Query())->from('core_student')->where(['school_id' => $model->school, 'school_student_registration_number' => $excelRegistrationNumber])->limit(1)->one() : [];
                $uqRegNo = $excelRegistrationNumber ? in_array($excelRegistrationNumber, $regNos) : false;

                if ($excelLastName && $excelFirstName && $excelDob && $excelGender && $excelNationality && !$query && !$uqRegNo) {
                    $regNos[] = $excelRegistrationNumber;

                    $sql2 = "INSERT INTO student_upload_file_data (
file_id,
date_created,
school_id,
student_class,
last_name,
first_name,
middle_name,
date_of_birth,
school_student_registration_number,
gender, student_email,
student_phone,
nationality,
disability,
disability_nature,
gurdian_name,
guardian_relation,
gurdian_email,
gurdian_phone,
day_boarding,
outstanding_balance,
guardian2_name,
guardian2_relation,
guardian2_email,
guardian2_phone,
class_code
)
VALUES (
:file_id,
NOW(),
:school_id,
:student_class,
:last_name,
:first_name,
:middle_name,
:date_of_birth,
:school_student_registration_number,
:gender,
:student_email,
:student_phone,
:nationality,
:disability,
:disability_nature,
:gurdian_name,
:guardian_relation,
:gurdian_email,
:gurdian_phone,
:day_boarding,
:outstanding_balance,
:guardian2_name,
:guardian2_relation,
:guardian2_email,
:guardian2_phone,
:class_code
)";

//Prepare and populate params
                    $query = $connection->createCommand($sql2);
                    $query->bindValue(':file_id', $file_id);
                    $query->bindValue(':school_id', $model->school);
                    $query->bindValue(':student_class', $model->student_class);
                    $query->bindValue(':last_name', $excelLastName);
                    $query->bindValue(':first_name', $excelFirstName);
                    $query->bindValue(':middle_name', $excelMiddleName);
                    $query->bindValue(':date_of_birth', $excelDob);
                    $query->bindValue(':school_student_registration_number', $excelRegistrationNumber);
                    $query->bindValue(':gender', $excelGender);
                    $query->bindValue(':student_email', $excelStudentEmail);
                    $query->bindValue(':student_phone', $excelStudentPhone);
                    $query->bindValue(':nationality', $excelNationality);
                    $query->bindValue(':disability', $disability);
                    $query->bindValue(':disability_nature', $excelDisabilityNature);
                    $query->bindValue(':gurdian_name', $excelGuardianName);
                    $query->bindValue(':guardian_relation', $excelGuardianRelation);
                    $query->bindValue(':gurdian_email', $excelGuardianEmail);
                    $query->bindValue(':gurdian_phone', $excelGuardianPhone);
                    $query->bindValue(':day_boarding', $excelDayBoarding);
                    $query->bindValue(':outstanding_balance', $excelStudentBalance);
                    $query->bindValue(':guardian2_name', $excelGuardianName2);
                    $query->bindValue(':guardian2_relation', $excelGuardianRelation2);
                    $query->bindValue(':guardian2_email', $excelGuardianEmail2);
                    $query->bindValue(':guardian2_phone', $excelGuardianPhone2);
                    $query->bindValue(':class_code', $excelStudentClassCode);
//Excecute update now
                    $query->execute();

//Increment no of records
                    $numberOfRecords++;
                } else {
                    $transaction->rollBack();
                    $ln = $excelLastName ? " Last Name: " . $excelLastName : " <b>Last Name: (not set)</b>";
                    $fn = $excelFirstName ? " First Name: " . $excelFirstName : " <b>First Name: (not set)</b>";
//                    $dob = $excelDob ? " Date of Birth: " . date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($excelDob)) : " <b>Date of Birth: (not set)</b>";
                    $gender = $excelGender ? " Gender: " . $excelGender : " <b>Gender: (not set)</b>";
                    $country = $excelNationality ? " Nationality: " . $excelNationality : " <b>Nationality: (not set)</b>";
                    $reg = $query ? "  <b>Reg No: " . $excelRegistrationNumber . " Already Taken</b>" : "";
                    $rpRegNo = $uqRegNo ? "  <b>Reg No: " . $excelRegistrationNumber . " Repeated</b>" : "";
                    Logs::logEvent("Failed to upload Students: ", "<b>Fix Row - " . $row . "</b> " . $ln . $fn  . $gender . $country . $reg . $rpRegNo, null);
//                    Logs::logEvent("Failed to upload Students: ", "<b>Fix Row - " . $row . "</b> " . $ln . $fn . $dob . $gender . $country . $reg . $rpRegNo, null);
//                    return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . $fn . $dob . $gender . $country . $reg . $rpRegNo . " </p></span>";
//                    return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . $fn . $gender . $country . $reg . $rpRegNo . " </p></span>";
                    throw new Exception("Fix Row - " . $row . $ln . $fn . $gender . $country . $reg . $rpRegNo);
                }
            }

            $imported = (new Query())->select(['imported', 'uploaded_by'])->from('student_upload_file_details')->where(['id' => $file_id])->limit(1)->one();
            $transaction->commit();
            Logs::logEvent("Temporary Students imported for class: " . $model->student_class, null, null);

//Send email
            $subject = "New  Awash ESchool File Uploaded $file_name";
            $emailMessage = "New file uploaded for $schoolRecord->school_name \n
Uploaded by: " . Yii::$app->user->identity->username . "\n
File Hash: $hash \n
Number of records: $numberOfRecords";

            $uploadEmailRecipients = Yii::$app->params['fileUploadedEmailRecipients'];

            ToWords::sendEmail($uploadEmailRecipients, $subject, $emailMessage);

            $res = ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $imported['imported'], 'imported' => $this->fileDetail($file_id), 'status' => $status];

            return ($request->isAjax) ? $this->renderAjax('import_table', $res) : $this->render('import_table', $res);


        } catch (\Exception $e) {
            Yii::trace($e);
            if($transaction)
                $transaction->rollBack();


            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to upload Students: ", $error, null);
            $params = ['model' => $model, 'error' => $error];
            return ($request->isAjax) ? $this->renderAjax('upload_file', $params) :
                $this->render('upload_file', $params);
        }


    }

    protected
    function fileData($id)
    {
        $query = new Query();
        $query->select(['fdata.id', 'last_name', 'first_name', 'middle_name', 'date_of_birth AS dob',
            'school_student_registration_number AS reg_no', 'gender', 'student_email', 'student_phone', 'nationality', 'disability',
            'disability_nature', 'gurdian_name', 'guardian_relation', 'gurdian_email', 'gurdian_phone', 'day_boarding',
            'outstanding_balance', 'guardian2_name', 'guardian2_relation', 'guardian2_email', 'guardian2_phone',
            'cl.class_code as file_class_code', 'fdata.class_code as import_class_code', 'fdetails.destination_class as file_destination_class', 'fdetails.student_campus', 'camp.campus_name'])
            ->from('student_upload_file_data fdata')
            ->innerJoin('student_upload_file_details fdetails', 'fdetails.id = fdata.file_id')
            ->leftJoin('core_school_class cl', 'fdetails.destination_class = cl.id')
            ->leftJoin('school_campuses camp', 'fdetails.student_campus = camp.id')
            ->where(['file_id' => $id])
            ->orderBy('last_name')
            ->limit(1000);
        return $query->all();
    }

    protected function fileDetail($id)
    {
        $query = (new Query())->select(['imported', 'uploaded_by', 'description', 'sch.school_name', 'destination_school', 'student_campus', 'sch.id as school_id'])->from('student_upload_file_details fd')->innerJoin('core_school sch', 'sch.id=fd.destination_school')->where(['fd.id' => $id])->limit(1)->one();
        return $query;
    }

    public function actionFileDetails($id)
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'save') {
                $status = $this->saveStudents($id);
            } else if ($action == 'del_student' && $_POST['stu']) {
                $status = $this->deleteRecord($_POST['stu']);
            }
        }

        return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]);
    }

    protected function deleteRecord($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql = "DELETE FROM student_upload_file_data WHERE id = :id";
            $connection->createCommand($sql)->bindValue(':id', $id)->execute();
            $transaction->commit();
            Logs::logEvent("Deleted Temporary student with id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Student successfully deleted</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to delete Temporary students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }


    protected function saveStudents($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        $select = "first_name , middle_name, last_name, school_id, student_class, student_email, student_phone,
gender, date_of_birth, school_student_registration_number, nationality, disability, disability_nature,
gurdian_name, guardian_relation, gurdian_email, gurdian_phone, day_boarding, outstanding_balance,
guardian2_name, guardian2_relation,guardian2_email, guardian2_phone, class_code";
        try {
            $fileDetails = $this->fileDetail($id);
            Yii::trace($fileDetails);
            $sql2 = "UPDATE student_upload_file_details SET imported = true WHERE id = :fileid";
            $temp_students = $connection->createCommand("SELECT " . $select . " FROM student_upload_file_data WHERE file_id = :fileid", [
                ':fileid' => $id
            ])->queryAll();

//Create a dynamic class map to be used for autocreated classes
            $dynamicClassMap = [];

            $schoolModel = CoreSchool::findOne(['id' => $fileDetails['school_id']]);
            $partPaymentsBehaviour = $schoolModel->default_part_payment_behaviour;

            foreach ($temp_students as $record) {
                $reg_no = $record['school_student_registration_number'];
                if ($reg_no) {
                    $thisStudent = $connection->createCommand("SELECT id, first_name, last_name FROM core_student WHERE school_id=" . $fileDetails['destination_school'] . " AND school_student_registration_number ='" . $reg_no . "'")->queryOne();

                    if ($thisStudent) {
                        $transaction->rollBack();
                        $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>Student: " . $thisStudent['first_name'] . " " . $thisStudent['last_name'] . " with  Reg No: " . $reg_no . " Already exists</div>"];
                        return $status;
                    }
                }

//Try to correct phones
                $validatedGuardianPhone = ToWords::validatePhoneNumber($record['gurdian_phone']);
                if ($validatedGuardianPhone) {
                    $record['gurdian_phone'] = $validatedGuardianPhone;
                }

                $validatedStudentPhone = ToWords::validatePhoneNumber($record['student_phone']);
                if ($validatedStudentPhone) {
                    $record['student_phone'] = $validatedStudentPhone;
                }

                $studentClassId = $record['student_class'];
//If student class = 0, then we are to resolve the class from the import template
                if ($studentClassId == '0') {
//If array already contains this class, just use it
                    $theClassModel = null;
                    $record['class_code'] = str_replace(' ', '', strtoupper($record['class_code']));
                    if (array_key_exists($record['class_code'], $dynamicClassMap)) {
                        $theClassModel = $dynamicClassMap[$record['class_code']];
                    } else {
//Auto create
                        $theClassModel = CoreSchoolClass::getOrAutoCreateClassByCode($record['school_id'], $record['class_code']);
                    }
                    $studentClassId = $theClassModel->id;
                }


                $connection->createCommand("INSERT INTO core_student (first_name , middle_name, last_name, school_id, class_id, student_email, student_phone,
gender, date_of_birth, school_student_registration_number, nationality, disability, disability_nature,
guardian_name, guardian_relation, guardian_email, guardian_phone, day_boarding,
guardian2_name, guardian2_relation,guardian2_email, guardian2_phone, campus_id, allow_part_payments)
VALUES(:first_name  ,:middle_name ,:last_name ,:school_id ,:student_class ,:student_email ,:student_phone ,
:gender , :date_of_birth ,:school_student_registration_number ,:nationality ,:disability ,
:disability_nature ,:gurdian_name ,
:guardian_relation ,:gurdian_email ,:gurdian_phone ,:day_boarding,
:guardian2_name, :guardian2_relation, :guardian2_email, :guardian2_phone, :campus_id, :allow_part_payments)")
                    ->bindParam("first_name", $record['first_name'])
                    ->bindParam("middle_name", $record['middle_name'])
                    ->bindParam("last_name", $record['last_name'])
                    ->bindParam("school_id", $record['school_id'])
                    ->bindParam("student_class", $studentClassId)
                    ->bindParam("student_email", $record['student_email'])
                    ->bindParam("student_phone", $record['student_phone'])
                    ->bindParam("gender", $record['gender'])
                    ->bindParam("date_of_birth", $record['date_of_birth'])
                    ->bindParam("school_student_registration_number", $record['school_student_registration_number'])
                    ->bindParam("nationality", $record['nationality'])
                    ->bindParam("disability", $record['disability'])
                    ->bindParam("disability_nature", $record['disability_nature'])
                    ->bindParam("gurdian_name", $record['gurdian_name'])
                    ->bindParam("guardian_relation", $record['guardian_relation'])
                    ->bindParam("gurdian_email", $record['gurdian_email'])
                    ->bindParam("gurdian_phone", $record['gurdian_phone'])
                    ->bindParam("day_boarding", $record['day_boarding'])
                    ->bindParam("guardian2_name", $record['guardian2_name'])
                    ->bindParam("guardian2_relation", $record['guardian2_relation'])
                    ->bindParam("guardian2_email", $record['guardian2_email'])
                    ->bindParam("guardian2_phone", $record['guardian2_phone'])
                    ->bindParam("campus_id", $fileDetails['student_campus'])
                    ->bindParam("allow_part_payments", $partPaymentsBehaviour)
                    ->execute();


//If outstanding balance is greater than 0
//Adjust student balance
                Yii::trace($record['outstanding_balance']);
                if (isset($record['outstanding_balance']) && $record['outstanding_balance'] > 0) {
//Account id for the new student
                    $student_payment_code = $connection->getLastInsertID('core_student_code_seq');
                    if (!$student_payment_code)
                        throw new \Exception('Student id not found while adjusting balance for ' . $record['first_name']);

                    $abs_outstanding = -1 * abs($record['outstanding_balance']);

                    $connection->createCommand("select adjust_student_balance(:student_code, :new_bal, :userid, :username, :reason, :ip, 'TO')")
                        ->bindValue(':student_code', $student_payment_code)
                        ->bindValue(':new_bal', $abs_outstanding)
                        ->bindValue(':username', Yii::$app->user->identity->username)
                        ->bindValue(':reason', 'Initial Outstanding Balance')
                        ->bindValue(':userid', Yii::$app->user->identity->id)
                        ->bindValue(':ip', Yii::$app->request->userIP)
                        ->execute();
                }

            }
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            Logs::logEvent("New Students imported from file id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Students successfully imported</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to import Students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    public function actionDeleteFile($id)
    {
        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $error = "";
        $state = ['status' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql1 = "DELETE FROM student_upload_file_data WHERE file_id = :fileid";
            $sql2 = "DELETE FROM student_upload_file_details WHERE id = :fileid";
            $connection->createCommand($sql1)->bindValue(':fileid', $id)->execute();
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            Logs::logEvent("Deleted Temporary students for file_id: " . $id, null, null);

//After succesful deleting, redirect to the file list
            \Yii::$app->session->setFlash('fileListAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; File has been deleted succesfully! ");
            return \Yii::$app->runAction('/schoolcore/student-upload/uploaded-files');
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to delete uploaded file(" . $id . "): ", $error, null);
            $error = "<div class='alert alert-danger'>" . $error . "</div>";
            return $error;
        }

    }


    public function actionUploadedFiles()
    {
        $request = Yii::$app->request;

        $searchModel = new DynamicModel(['schsearch']);
        $searchModel->addRule(['schsearch'], 'safe');
        $searchModel->load(Yii::$app->request->post());

        $query = new Query();
        $query->select(['fl.id', 'file_name', 'description', 'wsu.username', 'date_uploaded', 'school_name', 'si.id as school_id', 'class_code', 'imported', 'destination_class'])
            ->from('student_upload_file_details fl')
            ->leftJoin('core_school_class cl', 'cl.id=fl.destination_class')
            ->innerJoin('user wsu', 'fl.uploaded_by=wsu.id')
            ->innerJoin('core_school si', 'si.id=fl.destination_school');
        if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['si.id' => Yii::$app->user->identity->school_id]);
        }


        $query->andFilterWhere(['si.id' => $searchModel->schsearch]);
        $pages = new \yii\data\Pagination(['totalCount' => $query->count()]);
        $sort = new \yii\data\Sort([
            'attributes' => [
                'file_name', 'description', 'date_uploaded', 'username', 'school_name', 'class_code', 'imported'
            ],
        ]);

        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('school_name ASC, class_code ASC, date_uploaded DESC');
        return ($request->isAjax) ? $this->renderAjax('files_uploaded', ['model' => $query->all(), 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $sort]) : $this->render('files_uploaded', ['model' => $query->all(), 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $sort]);
    }

}

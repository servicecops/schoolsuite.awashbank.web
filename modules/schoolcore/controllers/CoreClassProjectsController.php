<?php

namespace app\modules\schoolcore\controllers;

use app\modules\schoolcore\models\CoreMarks;

use app\modules\logs\models\Logs;
use vsk\editableColumn\module\EditableColumnModule;
use Yii;
use app\modules\schoolcore\models\CoreClassProjects;
use app\modules\schoolcore\models\CoreClassProjectsSearch;
use yii\db\Query;
use yii\debug\models\timeline\DataProvider;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CoreClassProjectsController implements the CRUD actions for CoreClassProjects model.
 */
class CoreClassProjectsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreClassProjects models.
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex()
    {
        if (\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('is_teacher')) {
            $request = Yii::$app->request;
            $searchModel = new CoreClassProjectsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) : $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ]
            );
        }
        throw new ForbiddenHttpException('No permissions to view student projects.');
    }

    /**
     * Displays a single CoreClassProjects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) : $this->render('view', [
                'model' => $this->findModel($id),
            ]
        );
    }

    /**
     * Finds the CoreClassProjects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreClassProjects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreClassProjects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new CoreClassProjects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        if (\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('is_teacher')) {


            $model = new CoreClassProjects();
            $request = Yii::$app->request;
            if ($model->load(Yii::$app->request->post())) {
                $transaction = CoreClassProjects::getDb()->beginTransaction();
                try {
                    $model->created_by = Yii::$app->user->identity->getId();
                    if (\app\components\ToWords::isSchoolUser()) {
                        $model->school_id = Yii::$app->user->identity->school_id;
                    }
                    $model->created_by = Yii::$app->user->identity->getId();
                    $model->date_modified = date('Y-m-d H:i:s');
                    $model->save(false);

                    $students = (new Query())->select(['id'])->from('core_student')
                        ->where(['class_id' => $model->class_id])->all();
                    if (isset($_POST['exclude_student_codes'])) {
                        Yii::trace($_POST['exclude_student_codes']);
                        //get student codes
                        $codes = $_POST['exclude_student_codes'];
                        $p_codes = preg_split("/[\s,]+/", $codes);
                        $p_codes = array_unique(array_filter($p_codes));

                        $students = (new Query())->select(['id'])->from('core_student')
                            ->where(['class_id' => $model->class_id])
                            ->andWhere(['not in', 'student_code', $p_codes])->all();
                    }
                    Yii::trace($students);
                    // insert into student projects assessment table
                    foreach ($students as $key => $value) {
                        $marks = new CoreMarks();
                        $marks->project_id = $model->id;
                        $marks->student_id = $value['id'];
                        $marks->marks_obtained = 0;
                        $marks->date_created = date('Y-m-d H:i:s');
                        $marks->created_by = Yii::$app->user->identity->getId();
                        $marks->save(false);
                    }
                    $transaction->commit();
                    Logs::logEvent("Created student class project with  id: " . $model->id, null, null);
                    return $this->redirect(['view', 'id' => $model->id]);
                } catch (\Exception $exception) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $exception->getMessage();
                    Yii::trace('Error: ' . $error, 'Create  School circular');
                }
            }

            return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) : $this->render('create', ['model' => $model]
            );
        }
        throw new ForbiddenHttpException('No permissions to create student project');
    }

    /**
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\db\Exception
     */
    public function actionProjectMarks($id)
    {
        $model = new CoreMarks();
        $model->project_id = $id;

        $request = Yii::$app->request;
        Yii::trace($model->load($request->post()));
        if (!$request->post()) {
            $dataProvider = $model->searchStudentsAssessments($id);
            $res = ['dataProvider' => $dataProvider, 'model' => $model, 'id' => $id];
            return $this->render('project_marks', $res);
        }
        $transaction = CoreMarks::getDb()->beginTransaction();
        try {

            $marks_obtained = $request->post('marks_obtained');
            Yii::trace($marks_obtained);
            if (is_array($marks_obtained)) {
                foreach ($marks_obtained as $k => $v) {

                    if (isset($v)) {
                        $model = CoreMarks::findOne(['project_id' => $id, 'student_id' => $k]);
                        $model->created_by = Yii::$app->user->identity->getId();
                        $model->project_id = $id;
                        $model->marks_obtained = $v;
                        $model->student_id = $k;
                        $model->save(false);

                        //now also update the marks table


                        Yii::trace('Saved Marks ');
                    }

                }
            }

            $transaction->commit();

            return $this->redirect(['core-class-projects/marks', 'id' => $id]);

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace('Error: ' . $error, 'CREATE PROJECT MARKS ROLLBACK');
        }

    }

    public function actionMarks($id)
    {
        $model = new CoreMarks();
        $dataProvider = $model->searchProjectMarks($id, Yii::$app->request->queryParams);
        return $this->render('display_project_marks', [
            'searchModel' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing CoreClassProjects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CoreClassProjects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}

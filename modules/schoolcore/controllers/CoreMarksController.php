<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreMarks;
use app\modules\schoolcore\models\CoreMarksSearch;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreUploadedFiles;
use Yii;
use yii\base\DynamicModel;
use yii\base\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CoreMarksController implements the CRUD actions for CoreMarks model.
 */

if (!Yii::$app->session->isActive) {
    session_start();
}
class CoreMarksController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreMarks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreMarks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreMarks::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function newModel()
    {
        //$model = new CoreMarks();
        // TODO: Implement newModel() method.
        //$model->created_by =Yii::$app->user->identity->getId();
        return new CoreMarks();
    }

    public function createModelFormTitle()
    {
        return 'New Test';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionMarks($id)
    {
        $model = new CoreMarks();
        $model->test_id = $id;

        $request = Yii::$app->request;
        Yii::trace($model->load($request->post()));
        if (!$request->post()) {
            $studentSearchModel = new CoreMarksSearch();
            $studentSearchModel->load(Yii::$app->request->get());
            Yii::trace($studentSearchModel);
            $schoolData = $model->addMarks($id, $studentSearchModel);
            $_SESSION['SCORE_MODE'] = 'marks';
            $dataProvider = $schoolData['dataProvider'];
            $columns = $schoolData['columns'];
            $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $studentSearchModel, 'id' => $id];
          //  return $this->render('marks', $res);

            return ($request->isAjax) ? $this->renderAjax('marks', $res) :
                $this->render('marks', $res);
        }


        try {
            $transaction = CoreMarks::getDb()->beginTransaction();
            $marks_obtained = $request->post('marks_obtained');
            //$newId =$this->saveUser();
            if (is_array($marks_obtained)) {
                foreach ($marks_obtained as $k => $v) {

                    if (isset($v)) {
                        $model = new CoreMarks();
                        $model->created_by = Yii::$app->user->identity->getId();
                        $model->test_id = $id;
                        $model->marks_obtained = $v;
                        $model->student_id = $k;
                        $model->save(false);

                        Yii::trace('Saved Marks ');

                    }

                }
            }

            $transaction->commit();

            $searchModel = $this->newSearchModel();
            $testData = $searchModel->studentTestMarksObtained($id);

            $dataProvider = $testData['dataProvider'];
            $columns = $testData['columns'];
            $searchForm = $testData['searchForm'];

            $res = ['id' => $id, 'dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];

            //return $this->render('_subjectmarks', $res);
            $template = "_subjectmarks";

            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
//            return $this->render('score', $id);

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace('Error: ' . $error, 'CREATE  Test ROLLBACK');
        }

    }


    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new CoreMarksSearch();
    }

    /**
     * Updates an existing CoreGuardian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $schoolData = $searchModel->viewModel($id);
        $modeler = $schoolData['models'];
        $attributes = $schoolData['attributes'];

        if ($modeler->load(Yii::$app->request->post()) && $modeler->save()) {
//            return $this->render('@app/views/common/modal_view', [
//                'model' => $modeler,
//                'attributes' => $attributes
//            ]);
            $res = ['model' => $modeler,
                'attributes' => $attributes];

            $template = "@app/views/common/modal_view";

            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        }

        $formConfig = [
            'model' => $modeler,
            'formTitle' => $this->createModelUpdateFormTitle(),
        ];

        return $this->updateForm($modeler, $formConfig);

    }


    /**
     * Creates a view.
     * @return mixed
     */
    public function actionStudent()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $schoolData = $searchModel->searchStudent(Yii::$app->request->queryParams);
        $dataProvider = $schoolData['dataProvider'];
        $columns = $schoolData['columns'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        //return $this->render('@app/views/common/grid_view', $res);
    }

    /**
     * Lists all CoreStudent models.
     * @return mixed
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->searchStudentInfo(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        //return $this->render('view', $res);
    }

    /**
     * Creates a view.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->searchMarksIndex(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel,];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('index', $res);
        $template = "view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    /**
     * Displays a single subject score marks model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionScore($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $testData = $searchModel->studentTestMarksObtained($id);

        $dataProvider = $testData['dataProvider'];
        $columns = $testData['columns'];
        $searchForm = $testData['searchForm'];

        $res = ['id' => $id, 'dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        $template = "_subjectmarks";
        $_SESSION['SCORE_MODE'] = 'scores';

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        // return $this->render('_subjectmarks', $res);
    }

    /**
     * Creates a view for all tests.
     * @return mixed
     */
    public function actionTests()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->displayAllTest(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        Yii::trace($dataProvider);
        $columns = $allData['columns'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel,];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "index";
        $_SESSION['SCORE_MODE'] = 'tests';
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        //  return $this->render('index', $res);
    }


    /*
     * returns students marks per subject
     */
    public function actionDisplay($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $testData = $searchModel->studentTestMarksObtained($id);

        $dataProvider = $testData['dataProvider'];
        $columns = $testData['columns'];
        $searchForm = $testData['searchForm'];

        $res = ['id' => $id, 'dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];

        //return $this->render('_subjectmarks', $res);
        $template = "_subjectmarks";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    /*
     * edit single students marks per subject
     */
    public function actionEdit($id)
    {
        $searchModel = $this->newSearchModel();
        try {
            $request = Yii::$app->request;
            $updateData = $searchModel->editStudentMarks($id);
            $model = $updateData['models'];
            $attributes = $updateData['attributes'];

            if ($model->load(Yii::$app->request->post())) {
                $model->date_modified = date('Y-m-d H:i:s');
                $model->save();

                $searchModel = $this->newSearchModel();
                $subjectScore = $searchModel->studentTestMarksObtained($model->test_id);
                $dataProvider = $subjectScore['dataProvider'];
                $columns = $subjectScore['columns'];
                $searchForm = $subjectScore['searchForm'];
                $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
                // non-ajax - render the grid by default
                $template = "@app/views/common/grid_view";

                return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

                //  return $this->render('@app/views/common/grid_view', $res);
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Yii::trace('Error: ' . $error, 'CREATE  MArks ROLLBACK');
            Logs::logEvent("Error creating Marks: ", $error, $model->student_id, $model->school_id);
            \Yii::$app->session->setFlash('actionFailed', $error);
            Yii::trace($error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }

        $formConfig = [
            'model' => $model,
            'formTitle' => $this->createModelEditFormTitle(),
        ];
        // return $this->render('_viewEditTestScore', $formConfig);
        return $this->editForm($model, $formConfig);
    }

    public function actionUpload($id)
    {
        $model = new CoreMarks();

        if (Yii::$app->request->isPost) {
//            $model->file = UploadedFile::getInstance($model, 'file');
//
//            if ($model->file && $model->validate()) {
//                $model->file->saveAs('uploadsi/' . $model->file->baseName . '.' . $model->file->extension);
//            }
            $transaction = CoreMarks::getDb()->beginTransaction();
            try {
                $model->uploads = UploadedFile::getInstances($model, 'uploads');
                // $model->save(false);
                $model->saveUploads($id);
                $transaction->commit();
                Yii::trace('Submission successfully', 'SUBMISSION');
                $res = ['uploads' => $model->storedFiles, 'id' => [$model->id]];
                return $this->redirect('schoolcore/view-file', $res);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'SUBMISSION ROLLBACK');
                Logs::logEvent("Error creating Marks: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }
        }
        $res = ['model' => $model];
        return $this->render('_bulkUpload', $res);
    }

    public function actionViewFile($id)
    {
        $response = Yii::$app->response;
        $model = CoreUploadedFiles::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->headers->set('Content-type', $ext);
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);
        return $response;
    }

    public function actionRemoveFile($id)
    {
        $model = CoreUploadedFiles::find()->where(['id' => $id])->limit(1)->one();
        $submission_id = $model->submission;
        $model->delete();
        return $this->redirect(['/coremarks/marks', 'id' => $submission_id]);
//        return  \Yii::$app->runAction('/submission/view', ['id'=>$submission_id]);
    }


    public function actionUploadExcel($id)
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        $model = new DynamicModel(['importFile', 'description', 'test_id', 'school', 'student_class']);
        $model->addRule(['description'], 'required')
            ->addRule(['test_id', 'school', 'student_class'], 'integer')
            ->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx']);
        if (\app\components\ToWords::isSchoolUser()) {
            $model->school = Yii::$app->user->identity->school_id;
        }
        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($inputFile) {
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    throw new Exception("Your file was uploaded but could not be processed. Confirm that its a valid excel file");
                }
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestcolumn();

                $upload_by = Yii::$app->user->identity->getId();;

                //Selected class should belong to the selected school
                //This is a data integrity check
//                $schoolRecord = SchoolInformation::findOne($model->school);
//                $classRecord = InstitutionStudentClasses::findOne($model->student_class);

//                if ($schoolRecord->id != $classRecord->institution) {
//                    throw new Exception("Operation forbidden: Selected class should belong to the selected school");
//                }


                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                //Get a school model for this school, we wil use it to validate the registration numbers


                try {
                    $sql1 = "INSERT INTO upload_file_details (
                              file_name, 
                              hash, 
                              uploaded_by, 
                              description,  
                              test_id,
                              school_id,
                              student_class
                              )
                                VALUES (
                              :file_name, 
                              :hash, 
                              :uploaded_by, 
                              :description, 
                              :test_id,
                              :school_id, 
                              :class_id
                                )";
                    $fileQuery = $connection->createCommand($sql1);
                    $fileQuery->bindValue(':file_name', $file_name);
                    $fileQuery->bindValue(':hash', $hash);
                    $fileQuery->bindValue(':uploaded_by', $upload_by);
                    $fileQuery->bindValue(':description', $model->description);
                    $fileQuery->bindValue(':test_id', $id);
                    $fileQuery->bindValue(':school_id', $model->school);
                    $fileQuery->bindValue(':class_id', $model->student_class);
                    //Insert file
                    $fileQuery->execute();


                    $file_id = $connection->getLastInsertID('upload_file_details_id_seq');

                    $numberOfRecords = 1;
                    for ($row = 1; $row <= $highestRow; $row++) {
                        if ($row == 1) {
                            continue;
                        }
                        $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        //Collection information from excel row
                        // $excelStudentId = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][0])));
                        $excelStudentId = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));
                        $excelFirstName = !isset($v[0][2]) ? null : trim(ucwords(strtolower($v[0][2])));
                        $excelMiddleName = !isset($v[0][3]) ? null : trim(ucwords(strtolower($v[0][3])));
                        $excelMarksObtained = !isset($v[0][4]) ? null : trim(ucwords(strtolower($v[0][4])));
                        // = !isset($v[0][5]) ? null : trim(ucwords(strtolower($v[0][5])));


                        //Check that both names are present
                        if (empty($excelMarksObtained) || empty($excelStudentId)) {
                            throw new Exception("Student Code , Student marks and Marks Obtained are required. Check record $numberOfRecords");
                        }
//
                        Yii::trace('Error: ' . $excelMarksObtained, '$query  marks ROLLBACK');

                        if ($excelStudentId && $excelMarksObtained) {
//
                            $sql2 = "INSERT INTO upload_file_data (
                                        file_id,
                                        test_id,
                                        marks_obtained,
                                        student_id
                                        )
                                      VALUES (
                                        :file_id,
                                        :test_id,
                                        :marks_obtained,
                                        :student_id

                                      )";

                            //Prepare and populate params
                            $query = $connection->createCommand($sql2);
                            $query->bindValue(':file_id', $file_id);
                            $query->bindValue(':test_id', $id);
                            $query->bindValue(':marks_obtained', $excelMarksObtained);
                            $query->bindValue(':student_id', $excelStudentId);

                            //Excecute update now
                            $query->execute();
                            //Yii::trace('Error: ' . $query, '$query  marks ROLLBACK');
                            //Increment no of records
                            $numberOfRecords++;
                        } else {
                            $transaction->rollBack();
                            $ln = $excelStudentId ? " Student Id: " . $excelStudentId : " <b>Student Id: (not set)</b>";

                            Logs::logEvent("Failed to upload Students: ", "<b>Fix Row - " . $row . "</b> " . $ln, null);
                            return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . " </p></span>";
                        }
                    }

                    $imported = (new Query())->select(['imported', 'uploaded_by'])->from('upload_file_details')->where(['id' => $file_id])->limit(1)->one();
                    $transaction->commit();
                    Logs::logEvent("Temporary Students imported for class: " . $model->student_class, null, null);

                    //Send email

                    //return $this->renderAjax('file_details', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $imported['imported'], 'imported' => $this->fileDetail($file_id), 'status' => $status]);
                    return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    // Logs::logEvent("Failed to upload Marks: ", $error, null);
                    Yii::trace('Error: ' . $e, 'CREATE  marks ROLLBACK');

                    Logs::logEvent("Error creating Marks: ", $error, null);
                    \Yii::$app->session->setFlash('actionFailed', $error);
                    Yii::trace($error);
                    return \Yii::$app->runAction('schoolcore/core-staff/error');
                }

            } else {
                return "<div class='alert alert-danger' >File, or Description can't be blank</div>";
            }


        } else {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }
    }

    protected
    function fileData($id)
    {
        $query = new Query();
        $query->select(['id', 'student_id', 'test_id', 'marks_obtained'])
            ->from('upload_file_data')
            ->where(['file_id' => $id])
            ->orderBy('student_id')
            ->limit(1000);
        return $query->all();
    }

    protected function fileDetail($id)
    {
        $query = (new Query())->select(['imported', 'uploaded_by', 'fd.description as file_desc', 'ct.test_description as test_desc', 'ct.id as test_id'])->from('upload_file_details fd')->innerJoin('core_test ct', 'ct.id=fd.test_id')->where(['fd.id' => $id])->limit(1)->one();
        return $query;
    }

    public function actionFileDetails($id)
    {

        $request = Yii::$app->request;

        $status = ['state' => '', 'message' => ''];
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'save') {


                $status = $this->saveStudentMarks($id);

            } else if ($action == 'del_student' && $_POST['stu']) {
                $status = $this->deleteRecord($_POST['stu']);
            }
        }

        return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]);
    }

    protected function saveStudentMarks($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        $select = "test_id , student_id, marks_obtained";
        try {
            //check if test belongs to selected class
             

            $fileDetails = $this->fileDetail($id);
            $sql2 = "UPDATE upload_file_details SET imported = true WHERE id = :fileid";
            $temp_students = $connection->createCommand("SELECT " . $select . " FROM upload_file_data WHERE file_id = :fileid", [
                ':fileid' => $id
            ])->queryAll();
            $userId = Yii::$app->user->identity->getId();
            foreach ($temp_students as $record) {
                $std_id = $record['student_id'];
                if ($std_id) {
                    $thisStudent = $connection->createCommand("SELECT cs.id, cs.first_name, cs.last_name FROM core_student cs inner join core_marks cm on cm.student_id= cs.id WHERE cm.test_id=" . $fileDetails['test_id'] . " AND cm.student_id ='" . $std_id . "'")->queryOne();

                    if ($thisStudent) {
                        $transaction->rollBack();
                        $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>Student: " . $thisStudent['first_name'] . " " . $thisStudent['last_name'] . " marks already exists</div>"];
                        return $status;
                    }
                }
                $upload_by = Yii::$app->user->identity->getId();
                $connection->createCommand("INSERT INTO core_marks (created_by,student_id , test_id, marks_obtained) 
                              VALUES($upload_by,:student_id  ,:test_id ,:marks_obtained )")
                    ->bindParam("student_id", $record['student_id'])
                    ->bindParam("test_id", $record['test_id'])
                    ->bindParam("marks_obtained", $record['marks_obtained'])
                    ->execute();


            }
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            // Logs::logEvent("New Students imported from file id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Students successfully imported</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            //  Logs::logEvent("Failed to import Students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    protected function deleteRecord($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql = "DELETE FROM student_upload_file_data WHERE id = :id";
            $connection->createCommand($sql)->bindValue(':id', $id)->execute();
            $transaction->commit();
            Logs::logEvent("Deleted Temporary student with id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Student successfully deleted</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to delete Temporary students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    public function template($id)
    {
        $query = (new Query())->select(['student_code', 'first_name', 'last_name'])->from('core_students')->where(['class_id' => $id]);
        return $query;
    }

    public function actionStudentResults()
    {
        $request = Yii::$app->request;
        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->id])->one();

        $searchModel = new CoreMarks();

        $allData = $searchModel->StudentMarks($studentModel->id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title ="My Results";

        $res = ['dataProvider' => $dataProvider,'title'=>$title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        // return $this->render('@app/views/common/grid_view', $res);


    }
}

<?php
/**
 * Created by IntelliJ IDEA.
 * User: GOGO
 * Date: 5/3/2020
 * Time: 11:26 PM
 */

namespace app\modules\schoolcore\controllers;


use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreTest;
use Yii;
use yii\base\Controller;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

class CoreStudentReportController  extends Controller
{
    /**
* {@inheritdoc}
*/

public function behaviors()
{
    return [
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ],
        ],
    ];
}

    public function actionStudentReport($studentNumber)
    {
        $request = Yii::$app->request;
        $model = new CoreStudent();
        try {

            $schoolId  =Yii::$app->db->createCommand('SELECT school_id FROM core_student WHERE id=:studentId')
                ->bindValue(':studentId', $studentNumber)
                ->queryOne();
            Yii::trace($schoolId);

            $studentInfo  =Yii::$app->db->createCommand('SELECT CS.first_name,CS.middle_name,CS.last_name, CS.student_code,CS.class_id, CC.class_description FROM core_student CS inner join core_school_class CC on CC.id =CS.class_id WHERE CS.id=:studentId')
                ->bindValue(':studentId', $studentNumber)
                ->queryOne();
            Yii::trace($studentInfo);

            $schoolDetails =Yii::$app->db->createCommand('SELECT * FROM core_school WHERE id=:schoolId')
                ->bindValue(':schoolId', $schoolId['school_id'])
                ->queryOne();
            Yii::trace($schoolDetails);



            $results = Yii::$app->db->createCommand('SELECT subject_code,subject_name,subject_id,
       json_object_agg(assessment_type,markkk) as results, "avg"(marks_obtained) as average
   FROM (
     select
    core_marks.*, test_description, subject_name, subject_code,assessment_type, subject_id,(CAST( marks_obtained as FLOAT)/total_marks)*100 as markkk
  from core_marks INNER JOIN core_test CT on CT.id = test_id INNER JOIN core_subject CS on CS.id = CT.subject_id inner join core_assessment_type AST on AST.id =CT.assessment_id where student_id = :studentId
   ) s
  GROUP BY subject_code,subject_name,subject_id')
                ->bindValue(':studentId', $studentNumber)
                ->queryAll();
            Yii::trace($results);


            $grading =Yii::$app->db->createCommand('SELECT * FROM core_grades WHERE class_id=:classId order by max_mark desc')
                ->bindValue(':classId', $studentInfo['class_id'])
                ->queryAll();
            Yii::trace($grading);

            return ($request->isAjax) ? $this->renderAjax('student_report', ['results' => $results, 'model' => $model, 'schoolDetails'=>$schoolDetails, 'studentInfo'=>$studentInfo, 'grading'=>$grading ]) :
                $this->render('student_report', ['results' => $results, 'model' => $model, 'schoolDetails'=>$schoolDetails, 'studentInfo'=>$studentInfo, 'grading'=>$grading ]);
           // return $this->render("student_report", );

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }


    }

    public function actionGetTests(){



        if(!Yii::$app->user->can('r_student'))
            throw new ForbiddenHttpException('No permissions to view students.');

            $request = Yii::$app->request;
        $model = new CoreTest();

        $allTest = $model->getAllTests();
            $dataProvider = $allTest['query'];
            $pages = ['pages'=>$allTest['pages'], 'page'=>$allTest['cpage']];

            return ($request->isAjax) ? $this->renderAjax('display_all_tests', [
                'searchModel' => $model,'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$allTest['sort'] ]) :
                $this->render('display_all_tests', ['searchModel' => $model,
                    'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$allTest['sort'] ]);

    }

    public function actionGetClass(){


    }

}

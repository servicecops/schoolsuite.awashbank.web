<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use app\modules\schoolcore\models\CoreMarks;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreTest;
use app\modules\schoolcore\models\CoreTestSearch;
use app\modules\schoolcore\models\OnlineStudentAnswers;
use app\modules\schoolcore\models\OnlineTest;
use app\modules\schoolcore\models\OnlineTestQuestions;
use app\modules\schoolcore\models\OnlineTestSearch;
use app\modules\schoolcore\models\StudentOnlineMarksAssociation;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * CoreTestController implements the CRUD actions for CoreTest model.
 */

if (!Yii::$app->session->isActive) {
    session_start();
}

class CoreTestController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionDelete($id)
    {
        $testDetails = CoreTest::findOne($id);
        $classId = $testDetails->class_id;
        $this->findModel($id)->delete();

        return $this->redirect(['core-test/the-index', 'classId' => $classId]);
    }

    /**
     * Finds the CoreTest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreTest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreTest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function newModel()
    {
        //$model = new CoreTest();
        // TODO: Implement newModel() method.
        //$model->created_by =Yii::$app->user->identity->getId();
        return new CoreTest();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateTest($subjectId)
    {
        $request = Yii::$app->request;
        $model = new CoreTest();

        if ($model->load(Yii::$app->request->post())) {
            $post = $model->load(Yii::$app->request->post());

            $transaction = CoreTest::getDb()->beginTransaction();
            try {
                $results = Yii::$app->db->createCommand('SELECT * FROM core_subject WHERE id=:id and school_id = :school_id')
                    ->bindValue(":id", $subjectId)
                    ->bindValue(":school_id", Yii::$app->user->identity->school_id)
                    ->queryAll();
                if (count($results) == 0) {
                    $model->addError('term_id', 'Subject not found');
                    throw new Exception('Subject is required and should be existing');
                }
                $subjects = $results[0];

                $model->subject_id = $subjectId;
                $model->school_id = $subjects['school_id'];
                $model->class_id = $subjects['class_id'];
                $model->created_by = Yii::$app->user->identity->getId();

                if (!$model->save(true)) {
                    $formConfig = [
                        'model' => $model,
                        'formTitle' => $this->createModelFormTitle(),
                    ];

                    return $this->modelForm($model, $formConfig);
                }

                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel($model->id);
                $modeler = $schoolData['models'];
                $attributes = $schoolData['attributes'];
//                return $this->render('view', [
//                    'model' => $modeler,
//                    'attributes' => $attributes
//                ]);
                $res = ['model' => $modeler,
                    'attributes' => $attributes];


                return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);

            } catch (\Exception $e) {
                Yii::trace($e);
                $formConfig = [
                    'model' => $model,
                    'formTitle' => $this->createModelFormTitle(),
                ];

                return $this->modelForm($model, $formConfig);
            }
        }


        $formConfig = [
            'model' => $model,
            'formTitle' => $this->createModelFormTitle(),
        ];

        return $this->modelForm($model, $formConfig);
    }

    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new CoreTestSearch();
    }

    public function createModelFormTitle()
    {
        return 'New Test';
    }

    /**
     * Updates an existing CoreGuardian model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $testData = $searchModel->viewModel($id);
        $model = $testData['models'];
        $attributes = $testData['attributes'];

        if ($model->load(Yii::$app->request->post())) {
            $model->date_modified = date('Y-m-d H:i:s');
            if(!$model->save(true)) {
                $formConfig = [
                    'model' => $model,
                    'formTitle' => $this->createModelUpdateFormTitle(),
                ];

                return $this->updateForm($model, $formConfig);
            }
//            return $this->render('@app/views/common/modal_view', [
//                'model' => $model,
//                'attributes' => $attributes
//            ]);

            $res = ['model' => $model,
                'attributes' => $attributes];
            $template = "@app/views/common/modal_view";

            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


        }

        $formConfig = [
            'model' => $model,
            'formTitle' => $this->createModelUpdateFormTitle(),
        ];

        return $this->updateForm($model, $formConfig);

    }

    /**
     * Displays a single CoreStudent model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $schoolData = $searchModel->viewModel($id);
        $modeler = $schoolData['models'];
        $attributes = $schoolData['attributes'];
//        return $this->render('view', [
//            'model' => $modeler,
//            'attributes' => $attributes
//        ]);
        $res = ['model' => $modeler,
            'attributes' => $attributes];
        Yii::trace($res);


        return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);


    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    public function actionLists($id)
    {
        $countTests = CoreTest::find()
            ->where(['class_id' => $id])
            //->andWhere(['<>', 'class_code', '__ARCHIVE__'])
            ->count();

        $tests = CoreTest::find()
            ->where(['class_id' => $id])
            //->andWhere(['<>', 'class_code', '__ARCHIVE__'])
            ->orderBy('test_description')
            ->all();

        if ($countTests > 0) {
            echo "<option value=''> All Classes </option>";
            foreach ($tests as $myTest) {
                echo "<option value='" . $myTest->id . "'>" . $myTest->test_description . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }

    public function actionAddTest($id)
    {
        $request = Yii::$app->request;
        $model = new CoreTest();

        if ($model->load(Yii::$app->request->post())) {

            $transaction = CoreTest::getDb()->beginTransaction();
            try {
                $subjects = Yii::$app->db->createCommand('SELECT * FROM core_subject WHERE id=:id')
                    ->bindValue(":id", $id)
                    ->queryAll();


                $subject = $subjects[0];

                $model->class_id = $subject['class_id'];
                $model->school_id = $subject['school_id'];
                $model->subject_id = $id;
                $model->created_by = Yii::$app->user->identity->getId();
                $model->save(false);
                $transaction->commit();

                //return $this->redirect(['view', 'id' => $model->id]);

                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->searchView($model->id);
                $modeler = $model;
                $attributes = $schoolData['attributes'];
//                return $this->render('@app/views/common/modal_view', [
//                    'model' => $modeler,
//                    'attributes' => $attributes
//                ]);

                $res = ['model' => $modeler,
                    'attributes' => $attributes];

                $template = "@app/views/common/modal_view";

                return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE  TEST ROLLBACK');
            }

        }


        $formConfig = [
            'model' => $model,
            'formTitle' => $this->createModelFormTitle(),
        ];

        return $this->modelForm($model, $formConfig);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateOnlineTest($id)
    {
        $model = new CoreTest();
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {


            $transaction = OnlineTest::getDb()->beginTransaction();
            try {
                $data = Yii::$app->request->post();

                $testDetails = Yii::$app->db->createCommand('SELECT * FROM core_test where id =:id')
                    ->bindValue(':id', $id)
                    ->queryOne();
                Yii::trace($testDetails);
                $test = new OnlineTest();
                $coreTest = $data['CoreTest']['test'];
                $test->testing = $coreTest;


                if (is_array($test->testing) && $test->testing) {

                    foreach ($test->testing as $k => $v) {
                        $test = new OnlineTest();
                        if ($v['question_type'] === 'singleselect') {
                            $test->auto_assessment = false;
                            $test->correct_answer = '';
                        } else {
                            $test->correct_answer = $v['correct_answer'];
                            $test->auto_assessment = true;

                        }
                        Yii::trace($test->testing);
                        Yii::trace($k);
                        Yii::trace($v);
                        Yii::trace($v['question']);

                        $test->created_by = Yii::$app->user->identity->getId();
                        $test->test_id = $id;
                        $test->class_id = $testDetails['class_id'];
                        $test->school_id = $testDetails['school_id'];
                        $test->subject_id = $testDetails['subject_id'];
                        $test->question = $v['question'];
                        $test->question_type = $v['question_type'];
                        $test->answer_options = $v['answer'];

                        $test->save(false);

                        Yii::trace('Saved Test ' . $test->test_id);
                    }
                }


                $transaction->commit();
                $res = ['id' => $id];
                return $this->redirect(['view-online-test', 'id' => $id]);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE  Test ROLLBACK');
            }

        }

        $res = ['model' => $model, 'id' => $id];
        return ($request->isAjax) ? $this->renderAjax('create_online_test', $res) : $this->render('create_online_test', $res);

    }

    public function actionViewOnlineTest($id)
    {

        $request = Yii::$app->request;
        if (!Yii::$app->user->can('non_student')) {
            $existingTest = Yii::$app->db->createCommand('SELECT count(*) FROM online_test_answers where test_id =:id and student_id = :studentId')
                ->bindValue(':id', $id)
                ->bindValue(':studentId', Yii::$app->user->identity->getId())
                ->queryOne();
            Yii::trace($existingTest);

            $testModel = CoreTest::find()->where(['id' => $id])->one();

            $testStart = $testModel->start_date;
            $testEnd = $testModel->end_date;
            if ((time()) > strtotime($testEnd)) {
                throw new ForbiddenHttpException('Test time Expired');

            }

            if ((time()) < strtotime($testStart)) {
                throw new ForbiddenHttpException('This test is not yet active');

            }


            if ($existingTest['count'] > 0)
                throw new ForbiddenHttpException('Student  already took the  test');


        }


        $searchModel = new OnlineTestSearch();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $allData = $searchModel->viewOnlineTest($id);
        $data = $allData['data'];
        $columns = $allData['columns'];
        $model = new OnlineTest();
        $studentId = Yii::$app->user->identity->getId();
        $res = ['data' => $data, 'columns' => $columns, 'id' => $id, 'model' => $model, 'studentId' => $studentId];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "view_onlinetest";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
        // return $this->render('view_onlinetest', $res);

    }

    public function actionAwardOnlineMarks($id)
    {
        $request = Yii::$app->request;

        $single_input = false;
        $correctAnswers = 0;
        $totalmarks = 0;
        //  $outOf =count($answers);
        $model = new CoreTest();
        Yii::trace($model->load(Yii::$app->request->post()));
        if (!$model->load(Yii::$app->request->post()))
            yii::trace("failed");

        $transaction = OnlineTest::getDb()->beginTransaction();
        try {

            $data = Yii::$app->request->post();
            $answers = $data['CoreTest']['answer'];
            $testDetails = Yii::$app->db->createCommand('SELECT * FROM core_test where id =:id')
                ->bindValue(':id', $id)
                ->queryOne();
            $outOf = count($answers);


            foreach ($answers as $k => $v) {
                // Yii::trace($answers);
                //Yii::trace($k);
                //Yii::trace($v);

                $questionDetails = Yii::$app->db->createCommand('SELECT * FROM online_test where id =:id')
                    ->bindValue(':id', $v['question_id'])
                    ->queryOne();

                Yii::trace($questionDetails);


                if ($v['question_type'] === 'checkbox') {

                    $questionanswersArr = array();
                    $questionanswers = json_decode($questionDetails['correct_answer']);
                    foreach ($questionanswers as $key => $value) {
                        //  Yii::trace($value);

                        array_push($questionanswersArr, $value);
                        sort($questionanswersArr);
                        //
                    }

                    $studentanswersArr = array();
                    $studentanswers = $v['student_answer'];

                    foreach ($studentanswers as $keyS => $valueS) {
                        //  Yii::trace($valueS);

                        array_push($studentanswersArr, $valueS);
                        sort($studentanswersArr);

                    }
                    if ($studentanswersArr === $questionanswersArr) {
                        $correctAnswers++;
                        $is_correct_answer = true;
                        // echo "got it";
                    } else {
                        $is_correct_answer = false;
                    }
                    $savedAnswer = $studentanswers;
                    $isMarked = true;
                } else if ($v['question_type'] === 'radioButton') {

                    $questionanswers = $questionDetails['correct_answer'];
                    $questionanswers = json_decode($questionanswers);
                    // print_r($questionanswers);

                    foreach ($questionanswers as $kRadio => $vRadio) {
                        $radioQuestionAnswers = $vRadio;
                    }

                    Yii::trace($radioQuestionAnswers);

                    $studentanswers = $v['student_answer'];

//                            Yii::trace($questionanswers);
//                            Yii::trace($studentanswers);
                    if ($radioQuestionAnswers === $studentanswers) {
                        $correctAnswers++;
                        $is_correct_answer = true;

                        //echo "got it";
                    } else {
                        $is_correct_answer = false;
                        // echo "not matching";
                    }
                    $savedAnswer = (object)['student_answer' => $studentanswers];
                    $isMarked = true;
                    Yii::trace($savedAnswer);

                } else if ($v['question_type'] === 'singleselect') {
                    $single_input = true;
                    $isMarked = false;
                    $is_correct_answer = false;

                    $savedAnswer = (object)['student_answer' => $v['student_answer']];
                } else {

                }


                /* Save into Student online Marks*/
                $marksModel = new OnlineStudentAnswers();
                $marksModel->student_answer = $savedAnswer;
                $marksModel->is_correct_answer = $is_correct_answer;
                $marksModel->student_id = Yii::$app->user->identity->getId();
                $marksModel->test_id = $id;
                $marksModel->question_id = $v['question_id'];
                $marksModel->is_marked = $isMarked;
                $marksModel->save(false);

            }

            $totalmarks = $correctAnswers / $outOf * 100;

            /* Save into Student Marks Association*/
            $sMAModel = new StudentOnlineMarksAssociation();
            $sMAModel->test_id = $id;
            $sMAModel->correct_answers = $correctAnswers;
            $sMAModel->student_id = Yii::$app->user->identity->getId();
            $sMAModel->out_of = $outOf;
            $sMAModel->marks_obtained = $totalmarks;
            $sMAModel->save(false);

            /* Save into General Marks Table*/
            $generalMarksModel = new CoreMarks();
            $generalMarksModel->marks_obtained = $totalmarks;
            $generalMarksModel->student_id = Yii::$app->user->identity->getId();
            $generalMarksModel->created_by = 100;
            $generalMarksModel->test_id = $id;
            $generalMarksModel->save(false);

            $transaction->commit();
            $test_name = $testDetails['test_description'];
            $studentId = Yii::$app->user->identity->getId();
            Yii::trace("Succesfully saved student result,  Marks obtained," . $correctAnswers);
            $res = ['studentId' => $studentId, 'correctAnswers' => $correctAnswers, 'totalmarks' => $totalmarks, 'id' => $id, 'outOf' => $outOf, 'test_name' => $test_name];

            if (!$single_input) {
                $template = "onlinetest_result";

                return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
                // return $this->render('onlinetest_result', $res);


            }
            //return $this->render('onlinetest_result_pending', $res);
            $template = "onlinetest_result_pending";

            return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace('Error: ' . $error, 'CREATE  Test ROLLBACK');
        }

    }

    public function actionCheckResult($studentId, $testId)
    {
        $request = Yii::$app->request;
        $searchModel = new OnlineTestSearch();

        if (!Yii::$app->user->can('non_student')) {
            $existingTest = Yii::$app->db->createCommand('SELECT * FROM online_test_answers where test_id =:id and student_id = :studentId')
                ->bindValue(':id', $testId)
                ->bindValue(':studentId', Yii::$app->user->identity->getId())
                ->queryOne();

            if (!$existingTest)
                throw new ForbiddenHttpException('Student  hasnt taken the test yet');
        }

        $allData = $searchModel->getStudentResult($studentId, $testId);
        $data = $allData['data'];
        $columns = $allData['columns'];
        $model = new OnlineTest();
        $res = ['data' => $data, 'columns' => $columns];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "student_answers";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        // return $this->render('student_answers', $res);

    }

    public function actionMarkTest($id)
    {
        if (\Yii::$app->user->can('r_test')) {
            $model = new CoreTest();
            $request = Yii::$app->request;
            $searchModel = new OnlineTest();
            $data = $searchModel->studentSubmissions($id);
            Yii::trace($data);
            $data = $data['data'];
            $res = ['data' => $data, 'searchModel' => $searchModel];
            return ($request->isAjax) ? $this->renderAjax('_stds_tobe_marked.php', $res) :
                $this->render('_stds_tobe_marked.php', $res);
        } else {
            throw new ForbiddenHttpException("You dont have previllages to view this file");
        }


    }

    public function actionTeacherAwardMarks($id, $studentId)
    {

        // if (Yii::$app->user->can('rw_lesson')) {
        $request = Yii::$app->request;
        $post = $request->post();

        $model = new CoreTest();

        if ($model->load(Yii::$app->request->post())) {
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            Yii::trace($request->post());
            try {

                $data = Yii::$app->request->post();
                $answers = $data['CoreTest']['answer'];

                $testDetails = Yii::$app->db->createCommand('SELECT * FROM core_test where id =:id')
                    ->bindValue(':id', $id)
                    ->queryOne();

                $savedDetails = Yii::$app->db->createCommand('SELECT * FROM student_online_result_association where test_id =:testId and student_id=:studentId')
                    ->bindValue(':testId', $id)
                    ->bindValue(':studentId', $studentId)
                    ->queryAll();

                if (!$savedDetails)
                    Yii::trace("No details for test saved in online student association");


                $savedDetails = $savedDetails[0];
                $correctAnswer = $savedDetails['correct_answers'];
                foreach ($answers as $Ak => $Av) {
                    Yii::trace($Av);
                    if ($Av['is_correct_answer'] === 'correct') {
                        $correctAnswer++;
                        $is_correct = true;

                    } else {
                        $is_correct = false;
                    }

                    $updateStdAns = Yii::$app->db->createCommand()->update('online_test_answers', ['is_correct_answer' => $is_correct, 'is_marked' => true], 'id = :id', [':id' => $Av['answer_id']])->execute();;
                    if ($Av['correct_answer']) {
                        $tr_correct = (object)['correct_answer' => $Av['correct_answer']];
                        $updateCorAns = Yii::$app->db->createCommand()->update('online_test', ['correct_answer' => $tr_correct], 'id = :id', [':id' => $Av['question_id']])->execute();;

                    }


                }
                $outOf = $savedDetails['out_of'];
                $total = $correctAnswer / $outOf * 100;
                $user = Yii::$app->user->getId();
                $date = new Expression('NOW()');


                Yii::trace($outOf);
                Yii::trace($total);
                Yii::trace($user);
                Yii::trace($date);
                Yii::trace($id);
                $updateStdMksAss = Yii::$app->db->createCommand()->update('student_online_result_association', ['correct_answers' => $correctAnswer, 'marks_obtained' => $total], 'id = :id', [':id' => $savedDetails['id']])->execute();;
                $updateGeneralMksAss = Yii::$app->db->createCommand()->update('core_marks', ['created_by' => $user, 'marks_obtained' => $total, 'date_modified' => $date], 'test_id = :id AND  student_id = :studentId', [':id' => $id, ':studentId' => $studentId])->execute();;
                $test_name = $testDetails['test_description'];
                $transaction->commit();
                $res = ['studentId' => $studentId, 'correctAnswers' => $correctAnswer, 'totalmarks' => $total, 'id' => $id, 'outOf' => $outOf, 'test_name' => $test_name];

                // return $this->render('onlinetest_result', $res);
                $template = "onlinetest_result";

                return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE  FEES DUE ROLLBACK');

            }

        }
        $testModel = new OnlineTest();
        $data = $testModel->getQtnAnswerDetails($id, $studentId);
        $data = $data['data'];
        $res = ['data' => $data, 'searchModel' => $model, 'testId' => $id, 'studentId' => $studentId];
        return ($request->isAjax) ? $this->renderAjax('mark_student.php', $res) :
            $this->render('mark_student.php', $res);

//        } else {
//            throw new ForbiddenHttpException('No permissions to create a Lesson.');
//        }

    }

    public function actionDisplaySubject($classId)
    {
        $request = Yii::$app->request;

        $searchModel = new CoreTestSearch();
        $allData = $searchModel->searchSubject($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionDisplaySubjectView($classId)
    {
        $request = Yii::$app->request;
        $searchModel = new CoreTestSearch();
        $allData = $searchModel->searchSubjectView($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        // return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionDisplayClass()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreTestSearch();
        $allData = $searchModel->searchClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Class';
        $res = ['dataProvider' => $dataProvider, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        // return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionDisplayClassView()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreTestSearch();
        $allData = $searchModel->searchClassView(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Class';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false, 'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionGetStudentDetails($testId)
    {
        $request = Yii::$app->request;
        $searchModel = new CoreTestSearch();

        $test = Yii::$app->db->createCommand('SELECT * FROM core_test WHERE id=:id')
            ->bindValue(":id", $testId)
            ->queryAll();
        $test = $test[0];
        Yii::trace($test);

        $allData = $searchModel->generateStudentList($test['class_id']);

        Yii::trace($allData);
        $dataProvider = $allData['dataProvider'];


        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $res = ['testId' => $testId, 'dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('student_list_for_marks', $res);
//        $template = "card_summary";
        $template = "student_list_for_marks";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    /**
     * Creates a view.
     * @return mixed
     */
    public function actionTheIndex($classId)
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->searchIndex($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'All Tests';
        $res = ['dataProvider' => $dataProvider, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionStudentTests()
    {
        $request = Yii::$app->request;

        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->id])->one();
        $searchModel = new CoreTest();
        $allData = $searchModel->StudentTests($studentModel->class_id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = "Available Tests";

        $res = ['dataProvider' => $dataProvider, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        // return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }
}

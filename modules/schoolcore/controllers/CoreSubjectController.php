<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreSubjectSearch;
use Yii;

use yii\base\DynamicModel;
use yii\base\Exception;
use yii\base\UserException;
use yii\db\Query;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CoreSubjectController implements the CRUD actions for CoreSubject model.
 */
class CoreSubjectController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreSubject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('schoolpay_admin')) {
            $searchModel = new CoreSubjectSearch();
            $dataProvider = $searchModel->searchSubject(Yii::$app->request->queryParams);

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider]);

        } else {
            throw new ForbiddenHttpException('No permissions to view the subjects.');
        }
    }

    public function actionFileDetails($id)
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'save') {
                $status = $this->saveSubject($id);
            } else if ($action == 'del_student' && $_POST['stu']) {
                $status = $this->deleteRecord($_POST['stu']);
            }
        }


        return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status])
            : $this->render('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]);
    }


    /**
     *
     * /**
     * Updates an existing CoreSubject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the CoreSubject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreSubject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreSubject::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes an existing CoreSubject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['display-class']);
    }

    public function newModel()
    {
        // TODO: Implement newModel() method.
        return new CoreSubject();
    }

    public function createModelFormTitle()
    {
        return 'New Subject';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) :
            $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * @throws \yii\base\InvalidRouteException
     * @throws \yii\console\Exception
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\base\UserException
     */
    public function actionCreate()
    {
        if (\app\components\ToWords::isSchoolUser()) {
            $request = Yii::$app->request;
            $model = new CoreSubject();

            $model->created_by = Yii::$app->user->identity->getId();
     /*       try {*/
                if ($model->load(Yii::$app->request->post())) {
                    $post = Yii::$app->request->post();
                    $data = $post['CoreSubject'];
                    $existingSubject = CoreSubject::find()->where(['subject_code' => $data['subject_code'], 'class_id' => $data['class_id']])->limit(1)->one();
                    if ($existingSubject) {
                        $className = CoreSchoolClass::find()->where(['id' => $data['class_id']])->limit(1)->one();

                        throw new UserException("Subject Code  " . $data['subject_code'] . "  already exists in " . $className->class_description);
                    }

                    if (\app\components\ToWords::isSchoolUser()) {
                        $model->school_id = Yii::$app->user->identity->school_id;
                    }
                    //attach the subject outline if it exists
                    $outline_attachment_file = UploadedFile::getInstance($model, 'attachment');
                    Yii::trace($outline_attachment_file);
                    if (!is_null($outline_attachment_file)) {
                        $model->outline_attachment = $outline_attachment_file->name;
                        $tmp = explode('.', $outline_attachment_file->name);
                        $ext = end($tmp);
                        Yii::trace($outline_attachment_file->name);

                        $model->outline_attachment = Yii::$app->security->generateRandomString() . ".{$ext}";
                        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/subject_outlines/';
                        $path = Yii::$app->params['uploadPath'] . $model->outline_attachment;
                        $outline_attachment_file->saveAs($path);
                    }
                    if ($model->save(false)) {
                        Logs::logEvent("Created New Subject: " . $model->subject_name, null, $model->id);
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
                return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) :
                    $this->render('create', ['model' => $model]);
         /*   } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace("Subject Creation failed" . $error);
                Logs::logEvent("Failed to create Subject: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }*/
        } else {
            throw new ForbiddenHttpException('No permissions to create or update subject.');
        }
    }

    public function actionUploadExcel()
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        $model = new DynamicModel(['importFile', 'description', 'school', 'student_class']);
        $model->addRule(['student_class', 'description'], 'required')
            ->addRule(['school'], 'integer')
            ->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx']);
        if (\app\components\ToWords::isSchoolUser()) {
            $model->school = Yii::$app->user->identity->school_id;
        }
        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($inputFile && $model->student_class && $model->description && $model->school) {
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    throw new Exception("Your file was uploaded but could not be processed. Confirm that its a valid excel file");
                }
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestcolumn();

                $upload_by = Yii::$app->user->identity->getId();

                //Selected class should belong to the selected school
                //This is a data integrity check
                $schoolRecord = CoreSchool::findOne($model->school);
                $classRecord = CoreSchoolClass::findOne($model->student_class);

                if ($schoolRecord->id != $classRecord->school_id) {
                    throw new Exception("Operation forbidden: Selected class should belong to the selected school");
                }


                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                //Get a school model for this school, we wil use it to validate the registration numbers


                try {
                    $sql1 = "INSERT INTO subject_upload_file_details (
                              file_name, 
                              hash, 
                              uploaded_by, 
                              date_uploaded, 
                              description, 
                              destination_school, 
                              destination_class
                              )
                                VALUES (
                              :file_name, 
                              :hash, 
                              :uploaded_by, 
                              NOW(), 
                              :description, 
                              :destination_school, 
                              :destination_class
                                )";
                    $fileQuery = $connection->createCommand($sql1);
                    $fileQuery->bindValue(':file_name', $file_name);
                    $fileQuery->bindValue(':hash', $hash);
                    $fileQuery->bindValue(':uploaded_by', $upload_by);
                    $fileQuery->bindValue(':description', $model->description);
                    $fileQuery->bindValue(':destination_school', $model->school);
                    $fileQuery->bindValue(':destination_class', $model->student_class);
                    //Insert file
                    $fileQuery->execute();
                    $file_id = $connection->getLastInsertID('subject_upload_file_details_id_seq');
                    $regNos = [];
                    $numberOfRecords = 1;
                    for ($row = 1; $row <= $highestRow; $row++) {
                        if ($row == 1) {
                            continue;
                        }
                        $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        //Collection information from excel row
                        $excelSubjectName = !isset($v[0][0]) ? null : trim(ucwords(strtolower($v[0][0])));
                        $excelSubjectCode = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));


                        if ($excelSubjectName && $excelSubjectCode) {

                            $sql2 = "INSERT INTO subject_upload_file_data (
                                        file_id, 
                                        date_created, 
                                        school_id, 
                                        subject_name, 
                                        subject_code,  
                                        student_class
                          
                                        )
                                      VALUES (
                                        :file_id, 
                                        NOW(), 
                                        :school_id, 
                                        :subject_name, 
                                        :subject_code,
                                        :student_class 
                                      )";

                            //Prepare and populate params
                            $query = $connection->createCommand($sql2);
                            $query->bindValue(':file_id', $file_id);
                            $query->bindValue(':school_id', $model->school);
                            $query->bindValue(':student_class', $model->student_class);
                            $query->bindValue(':subject_name', $excelSubjectName);
                            $query->bindValue(':subject_code', $excelSubjectCode);
                            //Excecute update now
                            $query->execute();

                            //Increment no of records
                            $numberOfRecords++;
                        } else {
                            $transaction->rollBack();
                            $ln = $excelSubjectName ? " Subject Name: " . $excelSubjectName : " <b>Subject Name: (not set)</b>";
                            $fn = $excelSubjectCode ? " Subject Code: " . $excelSubjectCode : " <b>Subject Code: (not set)</b>";
                            //Logs::logEvent("Failed to upload Students: ", "<b>Fix Row - " . $row . "</b> " . $ln . $fn . $dob . $gender . $country . $reg . $rpRegNo, null);
                            return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . $fn . " </p></span>";
                        }
                    }

                    $imported = (new Query())->select(['imported', 'uploaded_by'])->from('subject_upload_file_details')->where(['id' => $file_id])->limit(1)->one();
                    $transaction->commit();
                    //Logs::logEvent("Temporary Students imported for class: " . $model->student_class, null, null);

                    //Send email
                    $subject = "New Subject File Uploaded $file_name";
                    $emailMessage = "New file uploaded for $schoolRecord->school_name \n
                    Uploaded by: " . Yii::$app->user->identity->username . "\n
                    File Hash: $hash \n
                    Number of records: $numberOfRecords";

                    //  $uploadEmailRecipients = Yii::$app->params['fileUploadedEmailRecipients'];

                    // ToWords::sendEmail($uploadEmailRecipients, $subject, $emailMessage);

                    return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]);

                    // return $this->renderAjax('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $imported['imported'], 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    // Logs::logEvent("Failed to upload Students: ", $error, null);
                    return "<div class='alert alert-danger' >Excel file File upload failed<br><br>
                    <p>" . $error . "</p> </div>";
                }

            } else {
                return "<div class='alert alert-danger' >File Or Description can't be blank</div>";
            }


        } else {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }
    }

    protected
    function fileData($id)
    {
        $query = new Query();
        $query->select(['fd.id', 'fd.subject_name', 'fd.subject_code', 'cc.class_description', 'csc.school_name'])
            ->from('subject_upload_file_data fd')
            ->innerJoin('core_school_class cc', 'fd.student_class= cc.id')
            ->innerJoin('core_school csc', 'fd.school_id= csc.id')
            ->where(['file_id' => $id])
            ->orderBy('subject_name')
            ->limit(1000);
        return $query->all();
    }

    protected function fileDetail($id)
    {
        $query = (new Query())->select(['imported', 'uploaded_by', 'description', 'sch.school_name', 'destination_school'])->from('subject_upload_file_details fd')->innerJoin('core_school sch', 'sch.id=fd.destination_school')->where(['fd.id' => $id])->limit(1)->one();
        return $query;
    }


    protected function saveSubject($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        $select = "subject_name , subject_code,  school_id, student_class ";
        try {

            $sql2 = "UPDATE subject_upload_file_details SET imported = true WHERE id = :fileid";
            $temp_subject = $connection->createCommand("SELECT " . $select . " FROM subject_upload_file_data WHERE file_id = :fileid", [
                ':fileid' => $id
            ])->queryAll();
            Yii::trace($temp_subject);
            $userId = Yii::$app->user->identity->getId();
            foreach ($temp_subject as $record) {

                $connection->createCommand("INSERT INTO core_subject (subject_name , subject_code,  school_id, class_id, created_by) 
                              VALUES(:subject_name  ,:subject_code ,:school_id ,:student_class,:created_by)")
                    ->bindParam("subject_name", $record['subject_name'])
                    ->bindParam("subject_code", $record['subject_code'])
                    ->bindParam("school_id", $record['school_id'])
                    ->bindParam("student_class", $record['student_class'])
                    ->bindParam("created_by", $userId)
                    ->execute();


            }
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            Yii::trace($temp_subject);
            Yii::trace($connection);
            $transaction->commit();
            //Logs::logEvent("New Students imported from file id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Subjects successfully imported</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            // Logs::logEvent("Failed to import Students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    public function actionAssignSubject($id)
    {

        $model = new CoreSubject();

        if ($model->load(Yii::$app->request->post())) {

        }

        return $this->render('assign_subject', [
            'model' => $model, 'subject' => $id,
        ]);
    }

    public function actionClassLists($q = null, $id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        $sv_cl = null;


        $query2 = (new Query())
            ->select(['core_school.id', 'core_school.school_name'])
            ->from('core_school')
            ->where(['id' => $id]);


        //$class_id=array();
        $school_id = array_column($query2->all(), 'id');
//            foreach($cls as $class){
//                $class_id =$class['id'];
//
//            }

        Yii::trace($school_id);
        $query = new Query;
        if (!is_null($q)) {

            $query->select('id, class_description AS text')
                ->from('core_school_class')
                ->where(['in', 'school_id', $school_id])
                ->andWhere(['ilike', 'class_description', $q]);

            $command = $query->createCommand();
            $datas = $command->queryAll();
            $out['results'] = array_values($datas);
            Yii::trace($datas);
        } elseif ($school_id > 0) {
            $query->select('id, class_description AS text')
                ->from('core_school_class')
                ->where(['in', 'school_id', $school_id]);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            Yii::trace($data);
            //$out['results'] = ['id' => $class_id, 'text' => CoreStudent::find()->where(['class_id' => $class_id])->first_name];
        }
        Yii::trace($out);

        return $out;
    }

    public function actionLists($id)
    {
        $countSubjects = CoreSubject::find()
            ->where(['class_id' => $id])
            ->andWhere(['<>', 'subject_code', '__ARCHIVE__'])
            ->count();

        $subjects = CoreSubject::find()
            ->where(['class_id' => $id])
            ->andWhere(['<>', 'subject_code', '__ARCHIVE__'])
            ->orderBy('subject_code')
            ->all();

        if ($countSubjects > 0) {
            echo "<option value=''> All Subjects </option>";
            foreach ($subjects as $mySubject) {
                echo "<option value='" . $mySubject->id . "'>" . $mySubject->subject_name . "</option>";
            }
        } else {
            echo "<option value=''>no subject found</option>";
        }
    }

    /**
     * Creates a view for all tests.
     * @return mixed
     */
    public function actionSubjects()
    {
        $request = Yii::$app->request;
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->displayAllSubjects(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        Yii::trace($dataProvider);
        $columns = $allData['columns'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel];
//         return $request->isAjax ? $this->renderAjax('subjects', $res);
//
//        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax('subjects', $res) : $this->render('subjects', $res);


        //return $this->render('subjects', $res);
    }

    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new CoreSubjectSearch();
    }

    public function actionTeachersList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $id = Yii::$app->user->identity->school_id;
            $query->select(["CONCAT(first_name,' ', last_name) AS text", 'id'])
                ->from('core_staff')
                ->where(['school_id' => $id])
                ->andWhere(['ilike', 'first_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreStaff::find()->where(['school_id' => $id])->first_name];
        }
        return $out;
    }


    public function actionDisplayClass()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $allData = $searchModel->searchClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title ='Select class';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title'=>$title,'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
        //return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionDisplayClassView()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $allData = $searchModel->searchClassView(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title ='Select class';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
        //return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionDisplaySubject($classId)
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $allData = $searchModel->searchIndex($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title ='Select subject';
        $res = ['dataProvider' => $dataProvider,'requiresExport' => false,'title' =>$title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        // return $this->render('@app/views/common/grid_view', $res);

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }

    public function actionStudentSubject()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->id])->one();

        $allData = $searchModel->studentSubject($studentModel->class_id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title ="My Subjects";
        $res = ['dataProvider' => $dataProvider,'title'=>$title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        // return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }

}

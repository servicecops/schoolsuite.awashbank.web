<?php

namespace app\modules\schoolcore\controllers;

use app\models\ImageBank;
use app\models\Model;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\AwashBranches;
use app\modules\schoolcore\models\AwashBranchesSearch;
use app\modules\schoolcore\models\BookCopy;
use app\modules\schoolcore\models\BookCopySearch;
use http\Exception;
use Yii;
use app\modules\schoolcore\models\Book;
use app\modules\schoolcore\models\BookSearch;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookController implements the CRUD actions for Book model.
 */
class AwashBranchesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $searchModel = new AwashBranchesSearch();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }


    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $request = Yii::$app->request;
        $searchModel = new AwashBranches();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $branchData = $searchModel->viewModel($id);

        $modeler = $branchData['models'];
        $attributes = $branchData['attributes'];

        $res = ['model' => $modeler,
            'attributes' => $attributes,
            'id' => $id
        ];


        Yii::trace($branchData);
        Yii::trace($res);


        return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);
    }
    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AwashBranches();

        if ($model->load(Yii::$app->request->post()) ) {
            try {
            //check if branch exists
            $post =Yii::$app->request->post();
            $data = $post['AwashBranches'];
            $branch = AwashBranches::findOne(['branch_code'=>$data['branch_code'],'branch_region'=>$data['branch_region']]);

            if($branch){
               throw new \yii\db\Exception('Branch Already Exits') ;
            }

            $model->branch_region = $data['branch_region'];
             $model->save(false);
            return $this->redirect(['view', 'id' => $model->id], 200);

            // validate all models
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'CREATE  TEACHER ROLLBACK');
                Logs::logEvent("Error creating staff: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }

        }


        return $this->render('create', [
            'model' => $model,

        ]);
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */



    public function actionUpdate($id)
    {
        try {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $post =Yii::$app->request->post();
                $data = $post['AwashBranches'];
                $model->branch_region = $data['branch_region'];

                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Yii::trace('Error: ' . $error, 'CREATE  TEACHER ROLLBACK');
            Logs::logEvent("Error creating staff: ", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            Yii::trace($error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }

        $res = ['model' => $model];
        return (Yii::$app->request->isAjax) ? $this->renderAjax('update', $res) :
            $this->render('update', $res);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AwashBranches::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionBookCover($id)
    {


        $model = $this->findModel($id);

        $request = Yii::$app->request;

        $model->scenario = 'photo';
        $imageBank = null;
        $imageBank = $model->book_cover ? ImageBank::find()->where(['id' => $model->book_cover])->limit(1)->one() : new ImageBank();
        try {
            if ($imageBank->load(Yii::$app->request->post())) {
                $data = $_POST['ImageBank']['image_base64'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $imageBank->image_base64 = $data;
                $imageBank->description = 'Book Cover Uploaded: ID - ' . $model->id;
                if ($imageBank->save()) {
                    $model->book_cover = $imageBank->id;
                    $model->save(false);
                    Logs::logEvent("Book Cover Photo: " . $model->book_title, null, $model->id);
                    $this->redirect(['view', 'id' => $model->id]);
                }
            }
            $res = ['imageBank' => $imageBank, 'model' => $model];
            return $this->render('book_cover', $res);
        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to upload book cover: " . $model->book_title . "(" . $model->id . ")", $error, $model->id);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/schoolcore/core-student/error');
        }
    }
}

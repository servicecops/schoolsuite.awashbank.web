<?php

namespace app\modules\schoolcore\controllers;

use app\controllers\BaseController;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreBankDetailsSearch;
use Yii;

use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use app\models\ImageBank;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\logs\models\Logs;


/**
 * BankDetailsController implements the CRUD actions for BankDetails model.
 */
if (!Yii::$app->session->isActive) {
    session_start();
}
class CoreBankDetailsController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Finds the CoreMarks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreBankDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreBankDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function newModel()
    {
        //$model = new CoreMarks();
        // TODO: Implement newModel() method.
        //$model->created_by =Yii::$app->user->identity->getId();
        return new CoreBankDetails();
    }

    public function createModelFormTitle()
    {
        return 'New Bank';
    }
    public function newSearchModel()
    {
        return new CoreBankDetails();
    }


    /**
     * Lists all BankDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
         $req = Yii::$app->request;
            $searchModel = new CoreBankDetailsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);


    }

    /**
     * Displays a single BankDetails model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
         $req = Yii::$app->request;
            return ($req->isAjax) ? $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]) : $this->render('view', [
                'model' => $this->findModel($id),
            ]);


    }

    /**
     * Creates a new BankDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//                 $req = Yii::$app->request;
//        $model = new CoreBankDetails();
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return Yii::$app->runAction('/core-bank-details/view', ['id' => $model->id]);
//        } else {
//            return ($req->isAjax) ? $this->renderAjax('create', [
//                'model' => $model,
//            ]) : $this->render('create', [
//                'model' => $model,
//            ]);
//        }
        $model = new CoreBankDetails();

        if ($model->load(Yii::$app->request->post()) ) {
            $post =$model->load(Yii::$app->request->post());

            $transaction = CoreBankDetails::getDb()->beginTransaction();
            try{

                $model->save(false);
                $transaction->commit();
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel( $model->id);
                $modeler = $schoolData['models'];
                $attributes =$schoolData['attributes'];
                return $this->render('@app/views/common/modal_view', [
                    'model' => $modeler,
                    'attributes'=>$attributes
                ]);
                // return $this->redirect (['view', 'id' => $model->id]);
            } catch (\Exception $e){
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: '.$error, 'CREATE  USER ROLLBACK');
            }

        }



        $formConfig = [
            'model' => $model,
            'formTitle'=>$this->createModelFormTitle(),
        ];

        return ((Yii::$app->request->isAjax)) ? $this->modelForm($model, $formConfig ) : $this->modelForm($model, $formConfig );

        //return  $this->modelForm($model, $formConfig );




    }

    /**
     * Updates an existing BankDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        if(Yii::$app->user->can('rw_banks')) {
//            $req = Yii::$app->request;
//            $model = $this->findModel($id);
//
//            if ($model->load(Yii::$app->request->post()) && $model->save()) {
//                return Yii::$app->runAction('/core-bank-details/view', ['id' => $model->id]);
//            } else {
//                return ($req->isAjax) ? $this->renderAjax('update', [
//                    'model' => $model,
//                ]) : $this->render('update', [
//                    'model' => $model,
//                ]);
//            }
//        } else {
//            throw new ForbiddenHttpException('No permissions to view this area.');
//        }
//
//
//    }
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return ($request->isAjax) ? $this->renderAjax('update',  ['model' => $model]) : $this->render('update',  ['model' => $model]);




    }
    public function actionLogo($id)
    {

        if (Yii::$app->user->can('rw_banks')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model->scenario = 'photo';
            $imageBank = null;
            $imageBank = $model->bank_logo ? ImageBank::find()->where(['id' => $model->bank_logo])->limit(1)->one() : new ImageBank();
            try {
                if ($imageBank->load(Yii::$app->request->post())) {
                    $data = $_POST['ImageBank']['image_base64'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $imageBank->image_base64 = $data;
                    $imageBank->description = 'Bank Logo: ID - ' . $model->id;
                    if ($imageBank->save()) {
                        $model->bank_logo = $imageBank->id;
                        $model->save(false);
                     //   Logs::logEvent("Bank logo changed: " . $model->bank_name . "(" . $model->bank_code . ")", null, null);
                        $this->redirect(['view', 'id' => $model->id]);
                    }
                }
                $res = ['imageBank' => $imageBank, 'model' => $model];
                return ($request->isAjax) ? $this->renderAjax('_bank_logo_croppie', $res) : $this->render('_bank_logo_croppie', $res);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to upload Bank photo: " . $model->bank_name . "(" . $model->bank_code . ")", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/schoolcore/core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to create or update Bank logo.');
        }

    }

    public function actionList($q = null, $id = null){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, bank_name AS text')
                ->from('nominated_bank_details')
                ->where(['ilike', 'bank_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => BankDetails::find()->where(['id' => $id])->bank_name];
        }
        return $out;
    }

    /**
     * Deletes an existing BankDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        if (!Yii::$app->user->can('rw_banks')) {
//            throw new ForbiddenHttpException('No permissions to delete this item');
//        }
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the BankDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreBankDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($id)
//    {
//        if (!Yii::$app->user->can('rw_banks')) {
//            throw new ForbiddenHttpException('No permissions to delete this item');
//        }
//        if (($model = CoreBankDetails::findOne($id)) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }
//    }


    public function actionBankList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreBankDetails::find()->where(['id' => $id])->bank_name];
            return $out;
        }

        $query = new Query;
        $query->select('id, bank_name AS text, bank_logo')
            ->from('core_nominated_bank');
        if($q) $query->where(['ilike', 'bank_name', $q]);

        $query->limit(20)->orderBy('bank_name');
        $command = $query->createCommand(Yii::$app->db2);
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out;
    }

    public function actionImageLink($id)
    {

        $data = (new Query())->select(['image_base64'])->from('image_bank')->where(['id'=>$id])->limit(1)->one();
        $data = base64_decode($data['image_base64']);

        $im = imagecreatefromstring($data);
        if ($im !== false) {
            header('Content-Type: image/jpeg');
            header('Pragma: public');
            header('Cache-Control: max-age=3600');
            header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
            imagejpeg($im);
            imagedestroy($im);
        }
        else {
            echo '';
        }

    }
}

?>

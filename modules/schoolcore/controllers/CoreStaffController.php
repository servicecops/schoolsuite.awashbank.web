<?php

namespace app\modules\schoolcore\controllers;

use app\components\ToWords;
use app\controllers\BaseController;
use app\models\ContactForm;
use app\models\UserSearch;
use app\modules\schoolcore\models\CoreStaffDeviceAssociation;
use app\modules\schoolcore\models\SubjectGroupSearch;
use app\modules\schoolcore\models\SubjectGroupStudent;
use app\models\User;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreSchoolClassSearch;
use app\modules\schoolcore\models\CoreStaffLevels;
use app\modules\schoolcore\models\CoreStaffType;
use app\modules\schoolcore\models\CoreSubjectSearch;
use app\modules\schoolcore\models\CoreTeacherSubjectClassAssociation;
use Yii;
use app\modules\schoolcore\models\CoreStaff;
use app\modules\schoolcore\models\CoreStaffSearch;
use yii\base\BaseObject;
use yii\base\DynamicModel;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CoreTeacherController implements the CRUD actions for CoreTeacher model.
 */

if (!Yii::$app->session->isActive) {
    session_start();
}

class CoreStaffController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreTeacher models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreStaffSearch();
        $allData = $searchModel->searchIndex(Yii::$app->request->queryParams);
        //Yii::trace($allData);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $title = "List of Teachers";
        $searchForm = $allData['searchForm'];
        $res = ['dataProvider' => $dataProvider, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)


        return ($request->isAjax) ? $this->renderAjax('index', $res) : $this->render('index', $res);

    }

    /**
     * Displays a single CoreTeacher model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $searchModel = new CoreStaff();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $schoolData = $searchModel->viewModel($id);

        $modeler = $schoolData['models'];
        $attributes = $schoolData['attributes'];
        $staffDevice = $schoolData['staffDevice'];
        Yii::trace($staffDevice);

        $res = ['model' => $modeler,
            'attributes' => $attributes,
            'staffDevice' => $staffDevice,
            'id' => $id
        ];

//        return $this->render('view', [
//            'model' => $modeler,
//            'attributes'=>$attributes,
//            'staffDevice'=>$staffDevice
//        ]);


        return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);
    }
//


    /***
     * Attach mobile device to user
     */


    public function actionAttachDevice($id)
    {
        try {
            $model = new CoreStaff();
            $modelFIndUser = User::find()->where(['school_user_id' => $id])->limit(1)->one();
            Yii::trace($modelFIndUser);
            if ($model->load(Yii::$app->request->post())) {

                $data = Yii::$app->request->post();
                $post = $data['CoreStaff'];

                $deviceModel = new CoreStaffDeviceAssociation();

                $deviceModel->device_id = $post['device_id'];
                $deviceModel->staff_web_ser_id = $modelFIndUser['id'];
                $deviceModel->school_id = $modelFIndUser['school_id'];
                $deviceModel->staff_id = $id;

                $deviceModel->save();

                return $this->redirect(['view', 'id' => $id]);
            }


        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Yii::trace('Error: ' . $error, 'ATTACH DEVICE TEACHER ROLLBACK');
            Logs::logEvent("Error attaching device to staff: ", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            Yii::trace($error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }

        $username = $modelFIndUser['username'];
        return $this->render('attach_device', ['model' => $model, 'username' => $username, 'id' => $id]);
    }

    /**
     * Updates an existing CoreTeacher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        try {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
                $modelFIndUser = User::find()->where(['school_user_id' => $model->id])->limit(1)->one();
                Yii::trace($modelFIndUser);
                Yii::trace($modelFIndUser->email);

                $modelFIndUser->email = $model->email_address;
                $modelFIndUser->firstname = $model->first_name;
                $modelFIndUser->lastname = $model->last_name;
                $modelFIndUser->mobile_phone = $model->phone_contact_1;
                $modelFIndUser->school_id = $model->school_id;
                $modelFIndUser->gender = $model->gender;
                $modelFIndUser->locked = false;
                $modelFIndUser->user_level = $model->user_level;
                $modelFIndUser->school_user_id = $model->id;
                $modelFIndUser->save(false);

                $modelFIndUser->setAuthAssignment($model->user_level, strval($modelFIndUser->id));


                if ($model->user_level === 'sch_teacher' && Yii::$app->user->can('sch_admin'))
                    return $this->redirect(['assign', 'id' => $model->id]);
                // return Yii::$app->runAction('/student-group/index');

                return $this->redirect(['view', 'id' => $model->id]);
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Yii::trace('Error: ' . $error, 'CREATE  TEACHER ROLLBACK');
            Logs::logEvent("Error creating staff: ", $error, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            Yii::trace($error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }

        $res = ['model' => $model];
        return (Yii::$app->request->isAjax) ? $this->renderAjax('update', $res) :
            $this->render('update', $res);
    }

    /**
     * Deletes an existing CoreTeacher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreTeacher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreStaff the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreStaff::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function newModel()
    {
        //$model = new CoreTeacher();
        // TODO: Implement newModel() method.
        //$model->created_by =Yii::$app->user->identity->getId();
        return new CoreStaff();
    }

    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new CoreStaffSearch();
    }

    public function createModelFormTitle()
    {
        return 'New Staff';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new CoreStaff();
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {

            $transaction = CoreStaff::getDb()->beginTransaction();
            try {
                //$newId =$this->saveUser();
                $data = Yii::$app->request->post();
                $post = $data['CoreStaff']['user_level'];

                Yii::trace($post);

                $model->created_by = Yii::$app->user->identity->getId();
                if (Yii::$app->user->can('sch_admin')) {

                    $model->school_id = Yii::$app->user->identity->school_id;
                }

                if(!$model->save(true)) {
                    $searchModel = new CoreStaffSearch();
                    return $this->render('create', ['model' => $model, 'searchModel' => $searchModel]);
                }

                /* * create staff in users table* */
                $modelUser = new User();
                $password = 'abc123';
                //use sequence ---lookup
                //check if username exists: 1
                //use schoolcode


                $fullname = $model->first_name . ' ' . $model->last_name;
                $schoolDetails = CoreSchool::findOne(['id' => $model->school_id]);

                $identifier = $schoolDetails['school_code'];
                $username = $this->generateUsername($fullname, $identifier);

                $modelUser->username = $username;
                $mult=false;

//                Yii::trace($username);
                $modelUser->setPassword($password);
                //$modelUser->password=$password;
                $modelUser->email = $model->email_address;
                $modelUser->firstname = $model->first_name;
                $modelUser->lastname = $model->last_name;
                $modelUser->mobile_phone = $model->phone_contact_1;
                $modelUser->gender = $model->gender;
                $modelUser->invalid_login_count = 0;
                $modelUser->school_id = $model->school_id;
                $modelUser->locked = false;
                $modelUser->user_level = $model->user_level;
                $modelUser->school_user_id = $model->id;
                $modelUser->save(false);
//                Yii::trace($password);
//                Yii::trace($modelUser->save());
                $modelUser->setUserSchools($modelUser->school_id, $modelUser->id, $mult);
                $modelUser->setAuthAssignment($modelUser->user_level, strval($modelUser->id));
                Logs::logEvent("New User Created(" . $modelUser->id . "): " . $modelUser->username, null, null);
                //Send user created email
                $fullname = $modelUser->getFullname();
                $emailSubject = 'SchoolSuite Logins | ' . $fullname;

                $url = Url::to('site/login', true);

                $emailText = "Hello $fullname\n
            
            Greetings from Awash ESchool!\n
            The following are your credentials for  Awash ESchool\n
            Username: $modelUser->username \n
            Password: $password \n
            To login, go to $url\n\n
            Thank you for choosing  Awash ESchool.";
                ToWords::sendEmail($modelUser->email,
                    $emailSubject,
                    $emailText);
                Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for your credentials.');

                //

                $next = $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);

                if ($model->user_level === 'sch_teacher' && Yii::$app->user->can('sch_admin'))
                    return $this->redirect(['assign', 'id' => $model->id]);
                // return Yii::$app->runAction('/student-group/index');


                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel($model->id);
                $modeler = $schoolData['models'];
                $attributes = $schoolData['attributes'];
                return $this->render('@app/views/common/modal_view', [
                    'model' => $modeler,
                    'attributes' => $attributes
                ]);


            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'CREATE  TEACHER ROLLBACK');
                Logs::logEvent("Error creating staff: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }

        }
        $searchModel = new CoreStaffSearch();
        return $this->render('create', ['model' => $model, 'searchModel' => $searchModel]);


    }

    public function generateUsername($full_name, $identifier)
    {
        $username_parts = array_filter(explode(" ", strtolower($full_name))); //explode and lowercase name
        $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

        $part1 = (!empty($username_parts[0])) ? substr($username_parts[0], 0) : ""; //get the whole of first name
        $part2 = (!empty($username_parts[1])) ? substr($username_parts[1], 0) : ""; //get the whole of second name
//    $last2digits =substr($rand_no,-2); //only get the two last digits of the staff phone number
        $part3 = ($identifier) ? $identifier : "";

        $username = $part1 . $part2 . '.' . $part3;


        if ($username) {
            $existingUser = User::findOne(['username' => $username]);
            if ($existingUser) {
                $num = 1;
                $username = $part1 . $part2 . $num . '.' . $part3;
            } else {
                $username = $username;

            }
        }
        return $username;
    }


    public function actionStaffLevels($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, level_description AS text')
                ->from('core_staff_levels')
                ->where(['ilike', 'level_description', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreStaffLevels::find()->where(['id' => $id])->level_description];
        }
        return $out;
    }

    public function actionStaffTypes($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, staff_description AS text')
                ->from('core_staff_type')
                ->where(['ilike', 'staff_description', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreStaffType::find()->where(['id' => $id])->staff_description];
        }
        return $out;
    }

    public function actionAssignSubjects($id)
    {
        if (Yii::$app->user->can('assign_classes')) {
            $user_sch = Yii::$app->user->identity->getId();
            $request = Yii::$app->request;
            $model = new CoreStaff();
            $classModel = new CoreTeacherSubjectClassAssociation();

            $cls = (new Query())
                ->select(['class_id'])
                ->from('teacher_class_subject_association')
                ->where(['teacher_id' => $id]);
            Yii::trace($cls);


            $av_classes = CoreSchoolClass::find()->where(['school_id' => $user_sch]);
            $members = SubjectGroupSearch::findMembers($id);


            // Yii::trace($av_classes);
            Yii::trace($members);

            if (!empty($members)) {


                foreach ($members as $k => $v) {
                    $t_subjects = new CoreTeacherSubjectClassAssociation();

                    $t_subjects->teacher_id = $id;
                    $t_subjects->school_id = $v['school_id'];
                    $t_subjects->class_id = $v['class_id'];
                    $t_subjects->subject_id = $v['id'];
                    $t_subjects->save(false);

                }

                $res = ['id' => $id];
                //   $feeClassModel->save(false);
                return $this->redirect(['view', 'id' => $id]);
            }

            $searchModel = new CoreStaffSearch();

            $res = ['model' => $model, 'classModel' => $classModel, 'id' => $id, 'searchModel' => $searchModel];

            return ($request->isAjax) ? $this->renderAjax('assign-classes', $res) :
                $this->render('assign-classes', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to assign classes to this teacher.');
        }


    }

    public function actionClasslist($q = null, $schoolId)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, class_description AS text')
                ->from('core_school_class')
                ->where(['school_id' => $schoolId])
                ->andWhere(['ilike', 'class_description', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($schoolId > 0) {
            $out['results'] = ['id' => $schoolId, 'text' => CoreSchoolClass::find()->where(['school_id' => $schoolId])->class_description];
        }
        return $out;
    }


    public function actionViewClasses($id)
    {

        $request = Yii::$app->request;
        $searchModel = new CoreTeacherSubjectClassAssociation();
        $data = $searchModel->TeacherClasses($id);
        $status = null;
        $data = $data['data'];
        $res = ['data' => $data, 'searchModel' => $searchModel];
        return ($request->isAjax) ? $this->renderAjax('assigned_classes', $res) :
            $this->render('assigned_classes', $res);

    }


    protected function deleteRecord($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql = "DELETE FROM student_upload_file_data WHERE id = :id";
            $connection->createCommand($sql)->bindValue(':id', $id)->execute();
            $transaction->commit();
            //Logs::logEvent("Deleted Temporary student with id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Student successfully deleted</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            //   Logs::logEvent("Failed to delete Temporary students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }


    public function actionAssign($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $searchModel = new CoreStaff();
        $school = CoreSchool::findOne(['id' => $model->school_id]);
        $data = [];
        // $model2 = new DynamicModel(['gender','student_class', 'searchTerm', 'day_boarding']);
        if ($searchModel->load(Yii::$app->request->get())) {

            $grp = ['grId' => $model->id, 'sch' => $model->school_id];
            Yii::trace($grp);
            Yii::trace($_GET['CoreStaff']);

            $data = $searchModel->findSubjects($grp, $_GET['CoreStaff']);
        }

        if (isset($_POST['empty-group']) && $_POST['empty-group'] == 'empty-group') {
            $this->emptyGroup($model->id);
        } else if (isset($_POST['selected_stu'])) {
            $this->addMembers($model->id, $_POST['selected_stu'], $model->school_id);
        } else if (isset($_POST['remove_stu'])) {
            $this->removeMembers($model->id, $_POST['remove_stu']);
        }

        //$members =[];
        $members = $searchModel->findMembers($model->id);
        $res = ['model' => $model, 'searchModel' => $searchModel, 'data' => $data, 'members' => $members, 'school' => $school];
        return $request->isAjax ? $this->renderAjax('assign-classes', $res) :
            $this->render('assign-classes', $res);

    }

    public function actionUnAssign($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $searchModel = new CoreStaff();
        $school = CoreSchool::findOne(['id' => $model->school_id]);
        $data = [];


        if (isset($_POST['empty-group']) && $_POST['empty-group'] == 'empty-group') {
            $this->emptyGroup($model->id);
        } else if (isset($_POST['selected_stu'])) {
            $this->addMembers($model->id, $_POST['selected_stu'], $model->school_id);
        } else if (isset($_POST['remove_stu'])) {
            $this->removeMembers($model->id, $_POST['remove_stu']);
        }

        //$members =[];
        $members = $searchModel->findMembers($model->id);
        $res = ['model' => $model, 'searchModel' => $searchModel, 'data' => $data, 'members' => $members, 'school' => $school];
        return $request->isAjax ? $this->renderAjax('assign-classes', $res) :
            $this->render('assign-classes', $res);

    }


    private function addMembers($id, $sel)
    {
        if ($sel) {
            foreach ($sel as $k => $v) {
                $model2 = new SubjectGroupStudent();
                $model2->group_id = $id;
                $model2->subject_id = $v;
                $model2->save(false);
            }
        }
    }

    private function removeMembers($id, $sel)
    {
        if ($sel) {
            foreach ($sel as $k => $v) {
                $model2 = SubjectGroupStudent::find()->where(['group_id' => $id, 'subject_id' => $v])
                    ->limit(1)->one();
                $model2->delete();
            }
        }
    }

    private function emptyGroup($id)
    {
        Yii::$app->db->createCommand("delete from subject_group_subject where group_id = :group_id ")
            ->bindValue(':group_id', $id)
            ->execute();
    }


    private function findUser($id)
    {
        if (($model = User::find()->where('school_user_id = ' . $id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionError()
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('staff_error') : $this->render('staff_error');
    }

    public function actionRemoveFlash()
    {

        if (\Yii::$app->session->hasFlash('stuAlert')) {
            \Yii::$app->session->removeFlash('stuAlert');
            return 'removed';
        } else {
            return 'not removed';
        }
    }

    public function actionEmail()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('email_address')
                    ->from('core_staff');
                $command = $query->createCommand();
                $emails = $command->queryAll(\PDO::FETCH_COLUMN);
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if ($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                } else {
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to staff: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }

        }
        return $this->render('contact', ['model' => $model]);
    }

    public function actionSendEmail($id)
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('email_address')
                    ->from('core_staff')
                    ->where(['id' => $id])->limit(1)->one();
                $command = $query->createCommand();
                $emails = $command->queryAll(\PDO::FETCH_COLUMN);
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $cc = $model->cc;
                $bcc = $model->bcc;

                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if ($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setCC($cc)
                        ->setBCC($bcc)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                } else {
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setCC($cc)
                        ->setBCC($bcc)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                //$error = 'No email is attached to this staff member. Please add the email to the staff and try again';
                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to staff: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }

        }
        return $this->render('contact', ['model' => $model]);
    }

    public function actionGenerateQr($id)
    {
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $e) {
        }

        $request = Yii::$app->request;
        $modelStaff = User::findOne(['school_user_id' => $model->id]);
        $res = ['model' => $model, 'staffModel' => $modelStaff];
        return $this->render('generate_qr', $res);

    }

    public function actionUploadExcel()
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        $model = new DynamicModel(['importFile', 'description', 'school', 'staff_type_id']);
        $model->addRule(['staff_type_id', 'description'], 'required')
            ->addRule(['school'], 'integer')
            ->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx']);
        if (\app\components\ToWords::isSchoolUser()) {
            $model->school = Yii::$app->user->identity->school_id;
        }
        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($inputFile && $model->description && $model->school) {
                try {
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (\yii\console\Exception $e) {
                    throw new Exception("Your file was uploaded but could not be processed. Confirm that its a valid excel file");
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestcolumn();

                $upload_by = Yii::$app->user->identity->getId();

                //Selected class should belong to the selected school
                //This is a data integrity check
                $schoolRecord = CoreSchool::findOne($model->school);


                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();

                //Get a school model for this school, we wil use it to validate the registration numbers


                try {
                    $sql1 = "INSERT INTO staff_upload_file_details (
                              file_name, 
                              hash, 
                              uploaded_by, 
                              date_uploaded, 
                              description, 
                              destination_school, 
                              staff_type
                              )
                                VALUES (
                              :file_name, 
                              :hash, 
                              :uploaded_by, 
                              NOW(), 
                              :description, 
                              :destination_school, 
                              :staff_type
                                )";
                    $fileQuery = $connection->createCommand($sql1);
                    $fileQuery->bindValue(':file_name', $file_name);
                    $fileQuery->bindValue(':hash', $hash);
                    $fileQuery->bindValue(':uploaded_by', $upload_by);
                    $fileQuery->bindValue(':description', $model->description);
                    $fileQuery->bindValue(':destination_school', $model->school);
                    $fileQuery->bindValue(':staff_type', $model->staff_type_id);
                    //Insert file
                    $fileQuery->execute();
                    $file_id = $connection->getLastInsertID('staff_upload_file_details_id_seq');
                    $numberOfRecords = 1;
                    for ($row = 1; $row <= $highestRow; $row++) {
                        if ($row == 1) {
                            continue;
                        }
                        $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        //Collection information from excel row
                        $excelLastName = !isset($v[0][0]) ? null : trim(ucwords(strtolower($v[0][0])));
                        $excelFirstName = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));
                        $excelMiddleName = !isset($v[0][2]) ? null : trim(ucwords(strtolower($v[0][2])));
                        $excelDob = !isset($v[0][3]) ? null : $v[0][3];
                        $excelGender = !isset($v[0][4]) ? null : strtoupper(trim($v[0][4]));
                        //Remove spaces from registration numbers
                        $excelTeachersEmail = !isset($v[0][5]) ? null : trim($v[0][5]);
                        $excelTeachersPhone = !isset($v[0][6]) ? null : trim($v[0][6]);
                        $excelNinNo = !isset($v[0][7]) ? null : trim($v[0][7]);
                        $excelOtherPhone = !isset($v[0][8]) ? null : trim($v[0][8]);
                        $userRole = 'sch_teacher';


                        //Check that both names are present
                        if (empty($excelFirstName) || empty($excelLastName)) {
                            throw new Exception("Both first name and last name are required. Check record $numberOfRecords");
                        }

                        //Validate gender
                        if ($excelGender != 'M' && $excelGender != 'F') {
                            throw new Exception("Specify M or F for gender. For teacher $excelLastName $excelFirstName");
                        }


                        if (empty($excelTeachersEmail)) {
                            throw new Exception("Teachers Email is required. Check record $numberOfRecords");
                        }

                        if (!ToWords::validateDate($excelDob, 'Y-m-d')) {
                            throw new Exception("Specify a valid date of birth in the format YYYY-mm-dd. For teacher $excelLastName $excelFirstName");
                        }


                        if ($excelLastName && $excelFirstName && $excelDob && $excelGender && $excelTeachersEmail && $excelTeachersPhone) {

                            $sql2 = "INSERT INTO staff_upload_file_data (
                                        file_id, 
                                        date_created, 
                                        school_id, 
                                        last_name,  
                                        first_name, 
                                        middle_name, 
                                        date_of_birth, 
                                        gender, 
                                        teacher_email, 
                                        teachers_phone, 
                                        other_phone, 
                                        nin_no
                                        )
                                      VALUES (
                                        :file_id, 
                                        NOW(), 
                                        :school_id, 
                                        :last_name,  
                                        :first_name, 
                                        :middle_name, 
                                        :date_of_birth, 
                                        :gender, 
                                        :teacher_email, 
                                        :teachers_phone, 
                                        :other_contact,            
                                        :nin_no
                                      )";

                            //Prepare and populate params
                            $query = $connection->createCommand($sql2);
                            $query->bindValue(':file_id', $file_id);
                            $query->bindValue(':school_id', $model->school);
                            $query->bindValue(':last_name', $excelLastName);
                            $query->bindValue(':first_name', $excelFirstName);
                            $query->bindValue(':middle_name', $excelMiddleName);
                            $query->bindValue(':date_of_birth', $excelDob);
                            $query->bindValue(':gender', $excelGender);
                            $query->bindValue(':teachers_phone', $excelTeachersPhone);
                            $query->bindValue(':other_contact', $excelOtherPhone);
                            $query->bindValue(':teacher_email', $excelTeachersEmail);
                            $query->bindValue(':nin_no', $excelNinNo);
                            //Excecute update now
                            $query->execute();

                            //Increment no of records
                            $numberOfRecords++;


                        } else {
                            $transaction->rollBack();
                            $ln = $excelLastName ? " Last Name: " . $excelLastName : " <b>Last Name: (not set)</b>";
                            $fn = $excelFirstName ? " First Name: " . $excelFirstName : " <b>First Name: (not set)</b>";
                            $dob = $excelDob ? " Date of Birth: " . date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($excelDob)) : " <b>Date of Birth: (not set)</b>";
                            $gender = $excelGender ? " Gender: " . $excelGender : " <b>Gender: (not set)</b>";
                            //$reg = $query ? "  <b>Reg No: " . $excelRegistrationNumber . " Already Taken</b>" : "";
                            // $rpRegNo = $uqRegNo ? "  <b>Reg No: " . $excelRegistrationNumber . " Repeated</b>" : "";
                            Logs::logEvent("Failed to upload teacher: ", "<b>Fix Row - " . $row . "</b> " . $ln . $fn . $dob . $gender, null);
                            return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $ln . $fn . $dob . $gender . " </p></span>";
                        }
                    }

                    $imported = (new Query())->select(['imported', 'uploaded_by'])->from('student_upload_file_details')->where(['id' => $file_id])->limit(1)->one();
                    $transaction->commit();
                    Logs::logEvent("Temporary staff imported in school id : " . $schoolRecord->school_name, null, null);

                    //Send email
                    $subject = "New Schoolpay File Uploaded $file_name";
                    $emailMessage = "New file uploaded for $schoolRecord->school_name \n
                    Uploaded by: " . Yii::$app->user->identity->username . "\n
                    File Hash: $hash \n
                    Number of records: $numberOfRecords";

                    return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                    //return $this->renderAjax('file_details', ['data' => $this->fileData($file_id), 'file' => $file_id, 'imported' => $imported['imported'], 'imported' => $this->fileDetail($file_id), 'status' => $status]);


                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Logs::logEvent("Failed to upload Staff: ", $error, null);
                    return "<div class='alert alert-danger' style='    background-color: #dd4b39 !important;'>Excel file File upload failed<br><br>
                    <p>" . $error . "</p> </div>";
                }

            } else {
                return "<div class='alert alert-danger' >File,  staff type or Description can't be blank</div>";
            }


        } else {
            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }
    }

    protected
    function fileData($id)
    {
        $query = new Query();
        $query->select(['id', 'last_name', 'first_name', 'middle_name', 'date_of_birth AS dob', 'gender', 'teacher_email', 'teachers_phone', 'other_phone', 'nin_no'])
            ->from('staff_upload_file_data')
            ->where(['file_id' => $id])
            ->orderBy('last_name')
            ->limit(1000);
        return $query->all();
    }

    protected function fileDetail($id)
    {
        $query = (new Query())->select(['imported', 'uploaded_by', 'description', 'sch.school_name', 'destination_school'])->from('staff_upload_file_details fd')->innerJoin('core_school sch', 'sch.id=fd.destination_school')->where(['fd.id' => $id])->limit(1)->one();
        return $query;
    }

    public function actionFileDetails($id)
    {
        $request = Yii::$app->request;
        $status = ['state' => '', 'message' => ''];
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'save') {
                $status = $this->saveStaff($id);
            } else if ($action == 'del_student' && $_POST['stu']) {
                $status = $this->deleteRecord($_POST['stu']);
            }
        }

        return ($request->isAjax) ? $this->renderAjax('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]) : $this->render('import_table', ['data' => $this->fileData($id), 'file' => $id, 'imported' => $this->fileDetail($id), 'status' => $status]);
    }

    protected function saveStaff($id)
    {
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        $select = "first_name , middle_name, last_name, school_id, teacher_email, teachers_phone, gender, date_of_birth, other_phone, nin_no ";
        try {
            $fileDetails = $this->fileDetail($id);
            $sql2 = "UPDATE staff_upload_file_details SET imported = true WHERE id = :fileid";
            $temp_staff = $connection->createCommand("SELECT " . $select . " FROM staff_upload_file_data WHERE file_id = :fileid", [
                ':fileid' => $id
            ])->queryAll();
            foreach ($temp_staff as $record) {

                $userId = Yii::$app->user->identity->getId();

                $auth_key = \Yii::$app->security->generateRandomString();
                $verification_token = Yii::$app->security->generateRandomString() . '_' . time();
                $password = Yii::$app->security->generatePasswordHash("abc123");

                $connection->createCommand("INSERT INTO core_staff (first_name , other_names, last_name, school_id, email_address, 
                              gender, date_of_birth, phone_contact_1, phone_contact_2,nin_no,  created_by,  staff_type_id, user_level) 
                              VALUES(:first_name  ,:middle_name ,:last_name ,:school_id ,:teacher_email ,
                              :gender , :date_of_birth ,:teachers_phone ,:other_phone ,:nin_no ,:created_by  ,1 ,'sch_teacher')")
                    ->bindParam("first_name", $record['first_name'])
                    ->bindParam("middle_name", $record['middle_name'])
                    ->bindParam("last_name", $record['last_name'])
                    ->bindParam("school_id", $record['school_id'])
                    ->bindParam("teacher_email", $record['teacher_email'])
                    ->bindParam("gender", $record['gender'])
                    ->bindParam("date_of_birth", $record['date_of_birth'])
                    ->bindParam("teachers_phone", $record['teachers_phone'])
                    ->bindParam("other_phone", $record['other_phone'])
                    ->bindParam("nin_no", $record['nin_no'])
                    ->bindParam("created_by", $userId)
                    ->execute();


                /* * create staff in users table* */

                $modelUser = new User();
                $password = 'abc123';

                $fullname = $record['first_name'] . ' ' . $record['last_name'];
                $schoolDetails = CoreSchool::findOne(['id' => $record['school_id']]);

                $identifier = $schoolDetails['school_code'];
                $username = $this->generateUsername($fullname, $identifier);

                $modelUser->username = $username;

                $staff_id = $connection->getLastInsertID('core_staff_id_seq');

                //                Yii::trace($username);
                $modelUser->setPassword($password);
                //$modelUser->password=$password;
                $modelUser->email = $record['teacher_email'];
                $modelUser->firstname = $record['first_name'];
                $modelUser->lastname = $record['last_name'];
                $modelUser->mobile_phone = $record['teachers_phone'];
                $modelUser->gender = $record['gender'];
                $modelUser->invalid_login_count = 0;
                $modelUser->school_id = $record['school_id'];
                $modelUser->locked = false;
                $modelUser->user_level = 'sch_teacher';
                $modelUser->school_user_id = $staff_id;
                $modelUser->save(false);
//                Yii::trace($password);
//                Yii::trace($modelUser->save());
                $mult= false;
                $modelUser->setUserSchools($modelUser->school_id, $modelUser->id,$mult);
                $modelUser->setAuthAssignment($modelUser->user_level, strval($modelUser->id));
                Logs::logEvent("New User Created(" . $modelUser->id . "): " . $modelUser->username, null, null);
                //Send user created email
                $fullname = $modelUser->getFullname();
                $emailSubject = 'SchoolSuite Logins | ' . $fullname;

                $url = Url::to('site/login', true);

                $emailText = "Hello $fullname\n
            
            Greetings from Schoolsuite!\n
            The following are your credentials for  Awash ESchool\n
            Username: $modelUser->username \n
            Password: $password \n
            To login, go to $url\n\n
            Thank you for choosing  Awash ESchool.";
                ToWords::sendEmail($modelUser->email,
                    $emailSubject,
                    $emailText);
                Yii::$app->session->setFlash('success', 'Imported, please ask teachers to check their emails for credentials.');


            }
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            Logs::logEvent("New Students imported from file id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'Staff successfully imported</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            // Logs::logEvent("Failed to import Students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $status;

    }

    public function actionRemoveSubject($id)
    {

        Yii::trace($id);
        $connection = Yii::$app->db;
        $status = ['state' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $otherdetails = CoreTeacherSubjectClassAssociation::findOne($id);
            $sql = "DELETE FROM teacher_class_subject_association WHERE id = :id";
            $connection->createCommand($sql)->bindValue(':id', $id)->execute();
            $transaction->commit();
            //Logs::logEvent("Deleted Temporary student with id: " . $id, null, null);
            $status = ['state' => 'SUCCESS', 'message' => "<div class='alert alert-success'>Subject successfully removed</div>"];
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            //   Logs::logEvent("Failed to delete Temporary students: ", $error, null);
            $status = ['state' => 'UNSUCCESSFULL', 'message' => "<div class='alert alert-danger'>" . $error . "</div>"];
        }
        return $this->redirect(['view-classes', 'id' => $otherdetails['teacher_id']]);


    }

    public function actionDeleteFile($id)
    {
        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $error = "";
        $state = ['status' => '', 'message' => ''];
        $transaction = $connection->beginTransaction();
        try {
            $sql1 = "DELETE FROM student_upload_file_data WHERE file_id = :fileid";
            $sql2 = "DELETE FROM student_upload_file_details WHERE id = :fileid";
            $connection->createCommand($sql1)->bindValue(':fileid', $id)->execute();
            $connection->createCommand($sql2)->bindValue(':fileid', $id)->execute();
            $transaction->commit();
            //  Logs::logEvent("Deleted Temporary students for file_id: " . $id, null, null);

            //After succesful deleting, redirect to the file list
            \Yii::$app->session->setFlash('fileListAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; File has been deleted succesfully! ");
            return \Yii::$app->runAction('schoolcore/core-student/uploaded-files');
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            //      Logs::logEvent("Failed to delete uploaded file(" . $id . "): ", $error, null);
            $error = "<div class='alert alert-danger'>" . $error . "</div>";
            return $error;
        }

    }


    public function actionUploadedFiles()
    {
        $request = Yii::$app->request;

        $searchModel = new DynamicModel(['schsearch']);
        $searchModel->addRule(['schsearch'], 'safe');
        $searchModel->load(Yii::$app->request->post());

        $query = new Query();
        $query->select(['fl.id', 'file_name', 'description', 'wsu.username', 'date_uploaded', 'school_name', 'si.id as school_id', 'imported'])
            ->from('staff_upload_file_details fl')
            ->innerJoin('user wsu', 'fl.uploaded_by=wsu.id')
            ->innerJoin('core_school si', 'si.id=fl.destination_school');

        if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['si.id' => Yii::$app->user->identity->school_id]);
        }

        $query->andFilterWhere(['si.id' => $searchModel->schsearch]);
        $pages = new \yii\data\Pagination(['totalCount' => $query->count()]);
        $sort = new \yii\data\Sort([
            'attributes' => [
                'file_name', 'description', 'date_uploaded', 'username', 'school_name', 'imported'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('school_name ASC, date_uploaded DESC');
        return ($request->isAjax) ? $this->renderAjax('files_uploaded', ['model' => $query->all(), 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $sort]) : $this->render('files_uploaded', ['model' => $query->all(), 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $sort]);
    }

    public function actionStaffLogins()
    {
        //Only schoolsuite admins and users with rw_user can access this area

        $request = Yii::$app->request;
        $searchModel = new CoreStaff();
        $model = new CoreStaff();
        $dataProvider = $model->searchUser(Yii::$app->request->queryParams);

        // validate if there is a editable input saved via AJAX
        if (Yii::$app->request->post('hasEditable')) {
            if (!Yii::$app->user->can('rw_user') && !Yii::$app->user->can('schoolsuite_admin')) {
                throw new ForbiddenHttpException('Insufficient privileges to access this area.');
            }
            // instantiate your book model for saving
            $bookId = Yii::$app->request->post('editableKey');
            $model = User::findOne($bookId);

            // store a default json response as desired by editable
            $out = Json::encode(['output' => '', 'message' => '']);

            // fetch the first entry in posted data (there should only be one entry
            // anyway in this array for an editable submission)
            // - $posted is the posted data for Book without any indexes
            // - $post is the converted array for single model validation
            $posted = current($_POST['User']);
            $post = ['User' => $posted];

            // load model like any single model validation
            if ($model->load($post)) {
                // can save model or do something before saving model
                $model->save();

                // custom output to return to be displayed as the editable grid cell
                // data. Normally this is empty - whereby whatever value is edited by
                // in the input by user is updated automatically.
                $output = '';

                // specific use case where you need to validate a specific
                // editable column posted when you have more than one
                // EditableColumn in the grid view. We evaluate here a
                // check to see if buy_amount was posted for the Book model
//                if (isset($posted['email'])) {
//                    $output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
//                }

                // similarly you can check if the name attribute was posted as well
                if (isset($posted['email'])) {
                    $output = ''; // process as you need
                }
                $out = Json::encode(['output' => $output, 'message' => '']);
            }
            echo $out;
            return;
        }
        return ($request->isAjax) ? $this->renderAjax('index_staff_login', ['dataProvider' => $dataProvider,
            'searchModel' => $searchModel]) :
            $this->render('index_staff_login', ['dataProvider' => $dataProvider,
                'searchModel' => $searchModel]);


    }


}

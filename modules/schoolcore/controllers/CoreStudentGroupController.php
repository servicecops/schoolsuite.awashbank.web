<?php

namespace app\modules\schoolcore\controllers;

//use app\models\Classes;
//use app\models\SchoolInformation;
//use app\models\Student;
use app\modules\feesdue\models\StudentGroupInformation;
use app\modules\payment_plan\models\PlanLogs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\schoolcore\models\CoreStudentGroupSearch;
use app\modules\schoolcore\models\CoreStudentGroupStudent;
use app\modules\workflow\controllers\WorkflowController;
use kartik\mpdf\Pdf;
use Yii;
//use app\models\CoreStudentGroupInformation;
//use app\models\CoreStudentGroupSearch;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use app\models\CoreStudentGroupStudent;
use yii\filters\AccessControl;
use yii\base\DynamicModel;
use yii\db\Query;
use app\modules\logs\models\Logs;


/**
 * StudentGroupController implements the CRUD actions for CoreStudentGroupInformation model.
 */
class CoreStudentGroupController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreStudentGroupInformation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreStudentGroupSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']];
        return $request->isAjax ? $this->renderAjax('index', $res) : $this->render('index', $res);
    }

    /**
     * Displays a single CoreStudentGroupInformation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        return $request->isAjax ? $this->renderAjax('view', ['model' => $model]) :
            $this->render('view', ['model' => $model]);
    }

    /**
     * Creates a new CoreStudentGroupInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new CoreStudentGroupInformation();

        if ($model->load(Yii::$app->request->post())) {
            if (\app\components\ToWords::isSchoolUser()) {
                $model->school_id = Yii::$app->user->identity->school_id;
            }
            if (!isset($_POST['CoreStudentGroupInformation']['active'])) {
                $model->active = false;
            }
            if ($model->save()) {
                return $this->redirect(['core-student-group/assign', 'id' => $model->id]);
                // return Yii::$app->runAction('/student-group/index');
            }
        } else {
            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('create', $res) :
                $this->render('create', $res);
        }
    }


    public function actionClassGroup()
    {
        $request = Yii::$app->request;
        $model = new CoreStudentGroupInformation();

        if ($model->load(Yii::$app->request->post())) {
            if (\app\components\ToWords::isSchoolUser()) {
                $model->school_id = Yii::$app->user->identity->school_id;
            }
            if (!isset($_POST['CoreStudentGroupInformation']['active'])) {
                $model->active = false;
            }
            $transaction = CoreStudentGroupInformation::getDb()->beginTransaction();
            try {
                if ($model->validate()) {
                    $model->save(false);
                    if ($model->classes) {
                        $query = (new Query())->select(['id'])->from('core_student')
                            ->andWhere(['in', 'class_id', array_values($model->classes)])->all();

                        foreach ($query as $k => $v) {
                            $student = new CoreStudentGroupStudent();
                            $student->group_id = $model->id;
                            $student->student_id = $v['id'];
                            $student->save(false);
                        }

                    }
                    $transaction->commit();
                    Logs::logEvent("Class group created: (" . $model->id . ")", null, null);
                    return $this->redirect(['core-student-group/assign', 'id' => $model->id]);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error creating class group: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('core-student-group/error');
            }
        }
        //setting initial values of class searchncreate
        $selectedVal = [];
        $model->classes = !$model->classes ? [] : $model->classes;
        if (count($model->classes) > 0) {
            foreach ($model->classes as $v) {
                $selectedVal[$v] = CoreSchoolClass::findOne($v)->class_code;
            }
        }
        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return ($request->isAjax) ? $this->renderAjax('create_from_class', $res) : $this->render('create_from_class', $res);

    }

    /**
     * Updates an existing CoreStudentGroupInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if (\app\components\ToWords::isSchoolUser()) {
                $model->school_id = Yii::$app->user->identity->school_id;
            }
            if ($model->save()) {
                 return $this->redirect(['/schoolcore/core-student-group/index']);
              //  return Yii::$app->runAction(['/schoolcore/core-student-group/index']);
            }
        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);
    }

    public function actionAssign($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $searchModel = new CoreStudentGroupSearch();
        $school = CoreSchool::findOne(['id' => $model->school_id]);
        $data = [];
        // $model2 = new DynamicModel(['gender','student_class', 'searchTerm', 'day_boarding']);
        if ($searchModel->load(Yii::$app->request->get())) {
            $grp = ['grId' => $model->id, 'sch' => $model->school_id];
            $data = $searchModel->findStudents($grp, $_GET['CoreStudentGroupSearch']);
        }

        if (isset($_POST['empty-group']) && $_POST['empty-group'] == 'empty-group') {
            $this->emptyGroup($model->id);
        }

        else if (isset($_POST['payment_codes'])) {
            $this->addWithPcodes($model->id, $_POST['payment_codes'], $model->school_id);
        }

        else if (isset($_POST['selected_stu'])) {
            $this->addMembers($model->id, $_POST['selected_stu'], $model->school_id);
        }

        else if (isset($_POST['remove_stu'])) {
            $this->removeMembers($model->id, $_POST['remove_stu']);
        }

        $members = $searchModel->findMembers($model->id);
        $res = ['model' => $model, 'searchModel' => $searchModel, 'data' => $data, 'members' => $members, 'school'=>$school];
        return $request->isAjax ? $this->renderAjax('assign_students', $res) :
            $this->render('assign_students', $res);

    }

    private function addMembers($id, $sel)
    {
        Yii::trace($sel);
        if ($sel) {
            foreach ($sel as $k => $v) {
                $model2 = new CoreStudentGroupStudent();
                $model2->group_id = $id;
                $model2->student_id = $v;
                $model2->save(false);
            }
        }
    }

    private function removeMembers($id, $sel)
    {
        if ($sel) {
            Yii::trace($sel);
            foreach ($sel as $k => $v) {
                $model2 = CoreStudentGroupStudent::find()->where(['group_id' => $id, 'student_id' => $v])
                    ->limit(1)->one();
                $model2->delete();
            }
        }
    }

    private function emptyGroup($id)
    {
        Yii::$app->db->createCommand("delete from student_group_student where group_id = :group_id ")
            ->bindValue(':group_id', $id)
            ->execute();
    }

    private function addWithPcodes($id, $codes, $schid)
    {
        if ($codes) {
            $query1 = CoreStudentGroupStudent::find()->select(['student_id'])->where(['group_id' => $id]);
            $p_codes = preg_split("/[\s,]+/", $codes);
            $p_codes = array_unique(array_filter($p_codes));
            $query = (new Query())->select(['id'])->from('core_student')
                ->where(['school_id' => $schid])
                ->andWhere(['not in', 'id', $query1])
                ->andWhere(['in', 'student_code', $p_codes])->all();

            foreach ($query as $k => $v) {
                $model = new CoreStudentGroupStudent();
                $model->group_id = $id;
                $model->student_id = $v['id'];
                $model->save(false);
            }
        }
    }



    public function actionAssignPlan($id)
    {
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('No permissions to Assign payment plan');
        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model2 = new DynamicModel(['fee_id', 'payment_plan']);
        $model2->addRule(['fee_id', 'payment_plan'], 'required');
        $members = CoreStudentGroupSearch::findMembers($id);
        $state = '';
        if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
            $state = $this->assignPaymentPlan($id, $members, $model2->fee_id, $model2->payment_plan);
        }
        $res = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state];
        return $request->isAjax ? $this->renderAjax('assign_plan', $res) : $this->render('assign_plan', $res);

    }


    public function actionCancelPlan($id)
    {
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('No permissions to Assign payment plan');
        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model2 = new DynamicModel(['fee_id', 'payment_plan','cancellation_reason']);
        $model2->addRule(['fee_id', 'payment_plan','cancellation_reason'], 'required');
        $members = CoreStudentGroupSearch::findMembers($id);
        $state = '';
        if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
            $state = $this->cancelPaymentPlan($id, $members, $model2->fee_id, $model2->payment_plan,$model2->cancellation_reason);
        }
        $res = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state];
        return $request->isAjax ? $this->renderAjax('cancel_plan', $res) : $this->render('cancel_plan', $res);

    }

    public function actionPromote($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['class_to', 'emptyafter']);
            $model2->addRule(['class_to'], 'required');
            $model2->addRule(['emptyafter'], 'boolean');

            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
                $state = $this->promoteClass($id, $members, $model2);
                if ($state['status'] == "SUCCESS") {
                    $members = CoreStudentGroupSearch::findMembers($id);
                }
            }else {
                $model2->emptyafter = true;
            }

            return $request->isAjax ? $this->renderAjax('promote_class', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
                $this->render('promote_class', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
        } else {
            throw new ForbiddenHttpException('No permissions to promote students');
        }
    }

//    public function actionAdjustBal($id)
//    {
//        if (Yii::$app->user->can('rw_student')) {
//            $request = Yii::$app->request;
//            $model = $this->findModel($id);
//            $model2 = new DynamicModel(['new_balance', 'reason', 'balance_type']);
//            $model2->addRule(['new_balance', 'reason', 'balance_type'], 'required')
//                ->addRule(['new_balance'], 'integer');
//            $members = CoreStudentGroupSearch::findMembers($id);
//            $state = '';
//            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
//                if($model2->balance_type == 'OUTSTANDING') {
//                    $model2->new_balance = -1* abs($model2->new_balance);
//                }else {
//                    $model2->new_balance = abs($model2->new_balance);
//                }
//                $state = $this->changeBal($id, $members, $model2->new_balance, $model2->reason);
//                if ($state['status'] == "SUCCESS") {
//                    $members = CoreStudentGroupSearch::findMembers($id);
//                }
//            }
//
//            return $request->isAjax ? $this->renderAjax('adjust_bal', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
//                $this->render('adjust_bal', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
//        } else {
//            throw new ForbiddenHttpException('No permissions to promote students');
//        }
//    }


    public function actionAdjustBal($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['new_balance', 'reason', 'adjustment_mode']);
            $model2->addRule(['new_balance', 'reason', 'adjustment_mode'], 'required')
                ->addRule(['new_balance'], 'integer');
            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
//                if ($model2->balance_type == 'OUTSTANDING') {
//                    $model2->new_balance = -1 * abs($model2->new_balance);
//                } else {
//                    $model2->new_balance = abs($model2->new_balance);
//                }

                //  Yii::trace($model2->new_balance);

                            $branch = null;
                if(Yii::$app->user->can('view_by_branch')){
                    $branch = Yii::$app->user->identity->branch_id;
                }

                $workflowRequest = [
                    'approval_required_permission' => json_encode(array('sch_admin','branch_chekers',  'super_admin', 'sys_admin')),
                    'record_id' => $model->id,
                    'request_notes'=>'Ajdust Group Fee',
                    'request_params' => json_encode([
                        'new_balance'=>$model2->new_balance,
                        'reason'=>$model2->reason,
                        'adjustment_mode'=>$model2->adjustment_mode,
                    ]),
                    'school_id'=>$model->school_id,
                    'workflow_record_type'=>'GROUP_BALANCE_ADJUSTMENT',
                    'branch_id'=>$branch

                ];

                try {
                    $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
                    return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
                }catch (\Exception $e) {
                    Yii::trace($e->getMessage());
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    //   return \Yii::$app->runAction('/student-group/assign-fee', ['id'=>$id]);
                }



//                $state = $this->changeBal($id, $members, $model2->new_balance, $model2->reason);
//                if ($state['status'] == "SUCCESS") {
//                    $members = StudentGroupSearch::findMembers($id);
//                }
            }

            return $request->isAjax ? $this->renderAjax('adjust_bal', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
                $this->render('adjust_bal', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
        } else {
            throw new ForbiddenHttpException('No permissions to promote students');
        }
    }




//    public function actionAssignFee($id)
//    {
//        if (Yii::$app->user->can('rw_student')) {
//            $request = Yii::$app->request;
//            $model = $this->findModel($id);
//            $model2 = new DynamicModel(['fee_id','credit_hours']);
//            $model2->addRule(['fee_id'], 'required');
//            $model2->addRule(['credit_hours'], 'required');
//            $members = CoreStudentGroupSearch::findMembers($id);
//            $state = '';
//            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
//                $state = $this->attachFee($id, $members, $model2->fee_id, $model2->credit_hours);
//                // if($state['status']=="SUCCESS"){
//                //  $members = CoreStudentGroupSearch::findMembers($id);
//                // }
//            }
//            return $request->isAjax ? $this->renderAjax('attach_fee', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
//                $this->render('attach_fee', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
//        } else {
//            throw new ForbiddenHttpException('No permissions to promote students');
//        }
//
//    }

    public function actionAssignFee($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['fee_id','credit_hours']);
            $model2->addRule(['fee_id'], 'required');
            $model2->addRule(['credit_hours'], 'safe');
            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {

                $credithr =1;

                   Yii::trace(isset($model2->credit_hours));
                   Yii::trace(empty($model->credit_hours));

                if ( !empty($model2->credit_hours)){
                    $credithr =$model2->credit_hours;
                }

                Yii::trace($credithr);

                //TODO: Work on penalty too
                $branch = null;
                if(Yii::$app->user->can('view_by_branch')){
                    $branch = Yii::$app->user->identity->branch_id;
                }

                $workflowRequest = [
                    'approval_required_permission' => json_encode(array('sch_admin', 'super_admin', 'sys_admin')),
                    'record_id' => $model->id,
                    'request_params' => json_encode([
                        'fee_id'=>$model2->fee_id,
                        'credit_hours'=>$credithr
                    ]),
                    'request_notes'=>'assign fee to a group',
                    'school_id'=>$model->school_id,
                    'workflow_record_type'=>'GROUP_FEE_ASSIGNMENT',
                    'branch_id'=>$branch
                ];

                try {
                    $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
                    return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
                }catch (\Exception $e) {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    return \Yii::$app->runAction('/view', ['id'=>$insert_id]);
                }


            }
//            $this->description." - ".number_format($this->due_amount, 2)
            $fees = (new Query())->select(['*',
                new Expression("case when recurrent = true then 'RECURRENT' else 'ONE TIME' end as fee_recurrent_type"),
                new Expression("format('%s -%s (%s)', trim(description), to_char(due_amount, '999,999,999,999,999'),(case when recurrent = true then 'RECURRENT' else 'ONE TIME' end)) as \"selectDesc\"")
            ])
                ->from('institution_fees_due ifd')
                ->andWhere(['school_id' => $model->school_id, 'approval_status' => true])
                ->andWhere("end_date >= '" . date("Y-m-d") . "'")
                ->orderBy('date_created')
                ->all();


            //Get the group fees
            $group_fees = $this->groupFees($id);
            $params = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state, 'fees' => $fees, 'group_fees' => $group_fees];
            return $request->isAjax ? $this->renderAjax('attach_fee', $params) :
                $this->render('attach_fee', $params);
        } else {
            throw new ForbiddenHttpException('No permissions to perform this action');
        }

    }

    /**To allow or disallow part payments for a group
     * @param $id
     * @return string
     * @throws ForbiddenHttpExceptionT
     */
    public function actionPartPayments($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['allow_part_payments']);
            $model2->addRule(['allow_part_payments'], 'required');
            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
                $state = $this->changePartPaymentsBehaviour($id, $members, $model2->allow_part_payments);
            }
            return $request->isAjax ? $this->renderAjax('adjust_part_payments', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
                $this->render('adjust_part_payments', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
        } else {
            throw new ForbiddenHttpException('No permissions to promote students');
        }

    }

//    public function actionRemoveFee($id)
//    {
//        if (Yii::$app->user->can('rw_student')) {
//            $request = Yii::$app->request;
//            $model = $this->findModel($id);
//            $model2 = new DynamicModel(['fee_id', 'adjust_balance']);
//            $model2->addRule(['fee_id', 'adjust_balance'], 'required');
//            $members = CoreStudentGroupSearch::findMembers($id);
//            $state = '';
//            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
//                $state = $this->removeFee($id, $members, $model2->fee_id, $model2->adjust_balance);
//                // if($state['status']=="SUCCESS"){
//                //  $members = CoreStudentGroupSearch::findMembers($id);
//                // }
//            };
//            $res = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state];
//            return $request->isAjax ? $this->renderAjax('remove_fee', $res) : $this->render('remove_fee', $res);
//        } else {
//            throw new ForbiddenHttpException('No permissions to promote students');
//        }
//
//    }
    public function actionRemoveFee($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['fee_id', 'adjust_balance', 'exempt']);
//            $model2->addRule(['fee_id', 'adjust_balance'], 'required');
            $model2->addRule(['fee_id'], 'required');
            $model2->addRule(['exempt'], 'boolean');
            $model2->addRule(['exempt','adjust_balance'], 'safe');
            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {

                $branch = null;
                if(Yii::$app->user->can('view_by_branch')){
                    $branch = Yii::$app->user->identity->branch_id;
                }

                $workflowRequest = [
                    'approval_required_permission' => json_encode(array('sch_admin','branch_chekers',  'super_admin', 'sys_admin')),
                    'record_id' => $model->id,
                    'request_params' => json_encode([
                        'fee_id'=>$model2->fee_id,
                        'adjust_balance'=>$model2->adjust_balance,
                        'exempt'=>$model2->exempt

                    ]),
                    'request_notes'=>'Remove fee from a group',
                    'school_id'=>$model->school_id,
                    'workflow_record_type'=>'REMOVE_GROUP_FEE',
                    'branch_id'=>$branch
                ];

                try {
                    $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
                    \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
                    return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
                }catch (\Exception $e) {
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    Yii::trace($e->getMessage());
                    //   return \Yii::$app->runAction('/student-group/assign-fee', ['id'=>$id]);
                }
            };
            $group_fees = $this->groupFees($id);
            $res = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state, 'group_fees' => $group_fees];
            return $request->isAjax ? $this->renderAjax('remove_fee', $res) : $this->render('remove_fee', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to promote students');
        }

    }

    public function actionArchive($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['reason_to_archive']);
            $model2->addRule(['reason_to_archive'], 'required');
            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
                $state = $this->archive($id, $members, $model2->reason_to_archive);
            }
            return $request->isAjax ? $this->renderAjax('archive_students', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
                $this->render('archive_students', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
        } else {
            throw new ForbiddenHttpException('No permissions to promote students');
        }

    }

    public function actionRemoveStudents($id)
    {
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('No permissions to drops students');
        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model2 = new DynamicModel(['reason']);
        $model2->addRule(['reason'], 'required');
        $state = '';
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'del_student' && $_POST['stu']) {
                $model2->reason = "No reason";
                $student = CoreStudentGroupStudent::find()->where(['group_id' => $id, 'student_id' => $_POST['stu']])
                    ->limit(1)->one();
                if ($student) {
                    $message = '<div class="alert alert-success"> Student successfully removed from group </div>';
                    $student->delete();
                    $state = ['status' => 'SUCCESS', 'message' => $message];
                }
            }
        }

        $members = CoreStudentGroupSearch::findMembers($id);
        if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
            $res = $this->deleteStudents($id, $members, $model2->reason);
            $state = $res['state'];
            $members = $res['members'];
        };
        $res = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state];
        return $request->isAjax ? $this->renderAjax('drop_students', $res) : $this->render('drop_students', $res);

    }

    public function actionUnArchive($id)
    {
        if (Yii::$app->user->can('rw_student')) {
            $request = Yii::$app->request;
            $model = $this->findModel($id);
            $model2 = new DynamicModel(['reason']);
            $model2->addRule(['reason'], 'required');
            $members = CoreStudentGroupSearch::findMembers($id);
            $state = '';
            if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
                $state = $this->unArchive($id, $members, $model2->reason);
            }
            return $request->isAjax ? $this->renderAjax('unarchive_students', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]) :
                $this->render('unarchive_students', ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state]);
        } else {
            throw new ForbiddenHttpException('No permissions to UnArchive students');
        }

    }


    private function promoteClass($grp, $members, $model)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_name = Yii::$app->user->identity->username;
        $user_ip = Yii::$app->request->userIP;
        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 0;
            foreach ($members as $k => $v) {

                $connection->createCommand("select change_student_class(:student_id, :new_class, :user_id, :username, :ip)")
                    ->bindValue(':student_id', $v['id'])
                    ->bindValue(':new_class', $model->class_to)
                    ->bindValue(':user_id', $user_id)
                    ->bindValue(':username', $user_name)
                    ->bindValue(':ip', $user_ip)
                    ->execute();
                $count++;
            }
            //check if we should empty
            $message = '<div class="alert alert-success"> You have successfully Changed Class for ' . $count . ' Students</div>';
            if($model->emptyafter) {
                $connection->createCommand("delete from student_group_student where group_id = :group_id ")
                    ->bindValue(':group_id', $grp)
                    ->execute();
                $message = '<div class="alert alert-success"> You have successfully Changed Class for ' . $count . ' Students<br><b>Group has been automatically cleared</b></div>';
            }
//            $transaction->commit();
            Logs::logEvent("Promoted group (" . $grp . ") to class id (" . $model->class_to . ")", null, null);

            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function attachFee($grp, $members, $fee_id, $credithrs)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_name = Yii::$app->user->identity->username;
        $user_ip = Yii::$app->request->userIP;
        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 0;
            foreach ($members as $k => $v) {

                $connection->createCommand("select apply_fee_to_student(:student_id, :fee_id, :user_id, :username, :credit_hrs,:ip)")
                    ->bindValue(':student_id', $v['id'])
                    ->bindValue(':fee_id', $fee_id)
                    ->bindValue(':user_id', $user_id)
                    ->bindValue(':username', $user_name)
                    ->bindValue(':credit_hrs', $credithrs)
                    ->bindValue(':ip', $user_ip)
                    ->execute();
                $count++;
            }
//            $transaction->commit();
            Logs::logEvent("Attaching a fee in students in group (" . $grp . ")", null, null);
            $message = '<div class="alert alert-success"> Fee successfully applied to <b>' . $count . '</b> Students</div>';
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function assignPaymentPlan($grp, $members, $fee_id, $payment_plan)
    {
        $connection = Yii::$app->db;
        try {
            $count = 0;
            foreach ($members as $k => $v) {
                $result = $connection->createCommand("select create_student_payment_plan_schedule(
                         :in_fee_id, :in_student_id, :in_payment_plan_id)",
                        [
                            ':in_fee_id' => $fee_id,
                            ':in_student_id' => $v['id'],
                            ':in_payment_plan_id' => $payment_plan
                        ])->queryOne();
                $count++;
                $result = json_decode($result['create_student_payment_plan_schedule']);
                if($result->returncode ==0){
                    PlanLogs::logEvent($result->returnmessage, $payment_plan, $v['id']);
                } else {
                    PlanLogs::logEvent($result->returnmessage, $payment_plan, $v['id'], true);
                }
            }
            $message = '<div class="alert alert-success"> Fee successfully applied to <b>' . $count . '</b> Students</div>';
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function cancelPaymentPlan($grp, $members, $fee_id, $payment_plan,$reason)
    {
        $connection = Yii::$app->db;

        try {
            $count = 0;
            foreach ($members as $k => $v) {

                $sql2 = "select * from payment_plan_fee_student_association where student_id = :stdId and payment_plan_id =:planId and fee_id =:feeId;";

                $planResult = $connection->createCommand($sql2)
                    ->bindValue(':stdId', $v['id'])
                    ->bindValue(':planId', $payment_plan)
                    ->bindValue(':feeId', $fee_id)
                    ->execute();
                Yii::trace($planResult);

                if ($planResult) {

                    $result = $connection->createCommand("select create_student_payment_plan_schedule(
                         :in_fee_id, :in_student_id, :in_payment_plan_id)",
                        [
                            ':in_fee_id' => $fee_id,
                            ':in_student_id' => $v['id'],
                            ':in_payment_plan_id' => $payment_plan
                        ])->queryOne();


                    $sql = "update payment_plan_student_payment_schedule set cancelled =true , cancellation_reason =:reason where student_id = :stdId and payment_plan_id =:planId and fee_id =:feeId";

                    $update = $connection->createCommand($sql)
                        ->bindValue(':stdId', $v['id'])
                        ->bindValue(':planId', $payment_plan)
                        ->bindValue(':reason', $reason)
                        ->bindValue(':feeId', $fee_id)
                        ->execute();

                    $sql3 = "update payment_plan_fee_student_association set cancelled =true , cancellation_reason =:reason where student_id = :stdId and payment_plan_id =:planId and fee_id =:feeId";

                    $update = $connection->createCommand($sql3)
                        ->bindValue(':stdId', $v['id'])
                        ->bindValue(':planId', $payment_plan)
                        ->bindValue(':reason', $reason)
                        ->bindValue(':feeId', $fee_id)
                        ->execute();

                    $count++;
                    if ($update) {
                        $message = '<div class="alert alert-success"> Fee Cancalled fee from <b>' . $count . '</b> Students</div>';
                        return ['status' => 'SUCCESS', 'message' => $message];
                    }
                } else {

                    $message = '<div class="alert alert-success"> Error, Student not subscribed to plan selected <b>' . $payment_plan . '</b></div>';
                    return ['status' => 'SUCCESS', 'message' => $message];

                     }
            }

        } catch (\Exception $e) {
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }


    private function changePartPaymentsBehaviour($grp, $members, $allow)
    {
        $flag = $allow == '0' ? 'false' : 'true';
        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 0;
            foreach ($members as $k => $v) {
                $connection->createCommand("update core_student set allow_part_payments = :flag where id = :id")
                    ->bindValue(':id', $v['id'])
                    ->bindValue(':flag', $flag)
                    ->execute();
                $count++;
            }
//            $transaction->commit();
            Logs::logEvent("Adjust allow part payments for group (" . $grp . ") to $flag", null, null);
            $message = "<div class='alert alert-success'> Allow part payments flag changed to $flag to <b> $count </b> Students</div>";
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function removeFee($grp, $members, $fee_id, $adj)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_name = Yii::$app->user->identity->username;
        $user_ip = Yii::$app->request->userIP;
        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 0;
            foreach ($members as $k => $v) {
                $connection->createCommand("select remove_fee_from_student(:student_id, :fee_id, :user_id, :username, :ip, :adj)")
                    ->bindValue(':student_id', $v['id'])
                    ->bindValue(':fee_id', (int) $fee_id)
                    ->bindValue(':user_id', $user_id)
                    ->bindValue(':username', $user_name)
                    ->bindValue(':ip', $user_ip)
                    ->bindValue(':adj', $adj)
                    ->execute();
                $count++;
            }
//            $transaction->commit();
            Logs::logEvent("Removing a fee in students in group (" . $grp . ")", null, null);
            $message = '<div class="alert alert-success"> Fee removed from <b>' . $count . '</b> Students</div>';
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function archive($grp, $members, $reason)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_name = Yii::$app->user->identity->username;
        $user_ip = Yii::$app->request->userIP;
        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 0;
            foreach ($members as $k => $v) {

                $connection->createCommand("select archive_student(:student_id, :reason, :user_id, :username, :ip)")
                    ->bindValue(':student_id', $v['id'])
                    ->bindValue(':reason', $reason)
                    ->bindValue(':user_id', $user_id)
                    ->bindValue(':username', $user_name)
                    ->bindValue(':ip', $user_ip)
                    ->execute();
                $count++;
            }
//            $transaction->commit();
            Logs::logEvent("Archiving students in group (" . $grp . ")", null, null);
            $message = '<div class="alert alert-success"> <b>' . $count . '</b> Students successfully archived </div>';
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function unArchive($grp, $members, $reason)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_name = Yii::$app->user->identity->username;
        $user_ip = Yii::$app->request->userIP;
        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 0;
            foreach ($members as $k => $v) {
                $connection->createCommand("select unarchive_student(:student_id, :reason, :user_id, :username, :ip)")
                    ->bindValue(':student_id', $v['id'])
                    ->bindValue(':reason', $reason)
                    ->bindValue(':user_id', $user_id)
                    ->bindValue(':username', $user_name)
                    ->bindValue(':ip', $user_ip)
                    ->execute();
                $count++;
            }
//            $transaction->commit();
            Logs::logEvent("Un-Archiving students in group (" . $grp . ")", null, null);
            $message = '<div class="alert alert-success"> <b>' . $count . '</b> Students successfully un-archived </div>';
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    private function deleteStudents($grp, $members, $reason)
    {
        if(count($members) > 10000) {
            $state = ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">Batch deletion cannot be done for more than 100 students. Reduce the group size and try again.</div>'];
            return ['state' => $state, 'members' => $members];
        }
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $processedMember = null;
        $count = 0;
        try {
            foreach ($members as $k => $v) {
                $processedMember = $v;
                $connection->createCommand("select drop_student_details(:student)")
                    ->bindValue(':student', $v['student_code'])
                    ->execute();
                $count++;
            }
            $transaction->commit();
            Logs::logEvent("Group students deleted: <b>" . $reason . "</b>", null, null);
            $message = '<div class="alert alert-success"> <b>' . $count . '</b> Students successfully dropped (' . $reason . ') </div>';
            $members = CoreStudentGroupSearch::findMembers($grp);
            $state = ['status' => 'SUCCESS', 'message' => $message];
            return ['state' => $state, 'members' => $members];
        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            if ($v = $processedMember) {
                $count = $count + 1;
                $error = "Row: " . $count . " " . $v['last_name'] . " " . $v['middle_name'] . " " . $v['first_name'] . " " . $error;
            }
            $transaction->rollBack();
            $state = ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $error . '</div>'];
            return ['state' => $state, 'members' => $members];
        }
    }

    private function changeBal($grp, $members, $new_bal, $reason)
    {
        $user_name = Yii::$app->user->identity->username;
        $user_id = Yii::$app->user->identity->id;
        $connection = Yii::$app->db;
        $ip = Yii::$app->request->userIP;
//        $transaction = $connection->beginTransaction();
        try {
            $count = 1;
            foreach ($members as $k => $v) {
                $connection->createCommand("select adjust_student_balance(
                        :student_code, 
                        :new_balance, 
                        :userid, 
                        :username, 
                        :reason,
                        :ip)", [
                    ':student_code'=>$v['student_code'],
                    ':new_balance'=>$new_bal,
                    ':userid'=>$user_id,
                    ':username'=>$user_name,
                    ':reason'=>$reason,
                    ':ip'=>$ip,
                ])->execute();

                $count++;
            }
//            $transaction->commit();
            Logs::logActionEvent("Adjust balance applied to group (" . $grp . ")", null, Logs::ADJUST_BALANCE, null);
//            Logs::logEvent("Adjust balance applied to group (" . $grp . ")", null, null);
            $message = '<div class="alert alert-success"> You have successfully adjusted balance for <b>' . $count . '</b> students to <b>' . $new_bal . '</b> </div>';
            return ['status' => 'SUCCESS', 'message' => $message];
        } catch (\Exception $e) {
//            $transaction->rollBack();
            return ['status' => 'UNSUCCESSFUL', 'message' => '<div class="alert alert-danger">' . $e->getMessage() . '</div>'];
        }
    }

    /**
     * Deletes an existing CoreStudentGroupInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $members = CoreStudentGroupStudent::find()->where(['group_id' => $id])->all();
//        if ($members) {
//            foreach ($members as $k => $v) {
//                $v->delete();
//            }
//        }
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    public function actionDelete($id)
    {
        $connection = Yii::$app->db;

        $groupDetails = StudentGroupInformation::findOne(['id'=>$id]);

        $branch = null;
        if(Yii::$app->user->can('view_by_branch')){
            $branch = Yii::$app->user->identity->branch_id;
        }


        $workflowRequest = [
            'approval_required_permission' => json_encode(array('sch_admin','branch_chekers',  'super_admin', 'sys_admin')),
            'record_id' => $id,
            'request_params' => json_encode([
                'gp_id'=>$id
            ]),
            'request_notes'=>'Delete a group',
            'school_id'=>$groupDetails->school_id,
            'workflow_record_type'=>'DELETE_A_GROUP',
            'branch_id'=>$branch
        ];
        try {
            $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
            \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
            return  \Yii::$app->runAction('/workflow/workflow/view', ['id'=>$insert_id]);
        }catch (\Exception $e) {
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
            //   return \Yii::$app->runAction('/student-group/assign-fee', ['id'=>$id]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoreStudentGroupInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreStudentGroupInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = null;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $model = CoreStudentGroupInformation::findOne(['id' => $id, 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $model = CoreStudentGroupInformation::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionError(){
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('student_error') : $this->render('student_error');
    }

    public function actionRemoveFlash(){

        if(\Yii::$app->session->hasFlash('stuAlert')){
            \Yii::$app->session->removeFlash('stuAlert');
            return 'removed';
        } else {
            return 'not removed';
        }
    }

    public function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    /**Returns fees assigned to this group
     * @param $id
     */
    private function groupFees($id)
    {
        return (new Query())->select(['ifd.*'])
            ->from('institution_fees_due ifd')
            ->innerJoin('institution_fee_group_association fga', 'fga.fee_id=ifd.id')
            ->andWhere(['fga.group_id' => $id, 'approval_status' => true])
            ->andWhere("ifd.end_date >= '" . date("Y-m-d") . "'")
            ->orderBy('date_created')
            ->all();
    }

}

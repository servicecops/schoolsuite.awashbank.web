<?php
namespace app\modules\synccore;

class SyncCoreModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\synccore\controllers';
    public function init() {
        parent::init();
    }
}

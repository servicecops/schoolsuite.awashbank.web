<?php

namespace app\modules\synccore\models;

use Yii;

/**
 * This is the model class for table "awash_api_login_control".
 *
 * @property string $control_id
 * @property string|null $token
 * @property string|null $expires
 * @property string|null $date_updated
 * @property string|null $field1
 */
class AwashApiLoginControl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'awash_api_login_control';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['control_id'], 'required'],
            [['control_id', 'token', 'field1'], 'string'],
            [['expires', 'date_updated'], 'safe'],
            [['control_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'control_id' => 'Control ID',
            'token' => 'Token',
            'expires' => 'Expires',
            'date_updated' => 'Date Updated',
            'field1' => 'Field 1',
        ];
    }
}

<?php
use yii\helpers\Html;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
?>

   <table class="table table-striped no-margin" id="sch_payments_recvd">
    <thead>
    <tr><th>Receipt Number</th><th> Student Name</th> <th> Payment Channel</th> <th> Process Date</th><th> Amount</th></tr>
   </thead>
   <tbody>
   <?php 
   $data = $model->paymentsReceiveds;
   foreach($data['query'] as $payments): ?>
   <tr>
        <td><?= Html::a($payments->reciept_number, ['/payments-received/view', 'id' => $payments->id]) ?></td>
        <td><?= Html::a($payments->student->fullname, ['/payments-received/view', 'id' => $payments->id], ['style'=>"color:black"]) ?></td>
        <td><?= Html::a($payments->paymentChannel->channel_name, ['/payments-received/view', 'id' => $payments->id], ['style'=>"color:black"]) ?></td>
        <td><?= Html::a($payments->channel_process_date, ['/payments-received/view', 'id' => $payments->id], ['style'=>"color:black"]) ?></td>
        <td><?= Html::a(number_format($payments->amount, 2), ['/payments-received/view', 'id' => $payments->id], ['style'=>"color:black"]) ?></td>
   </tr>   
   <?php endforeach; ?>
   </tbody>
</table>

<?php
echo LinkPager::widget([
    'pagination' => $data['pages'],
    'options'=>['class'=>'pagination', 
                'data-for'=>'sch_payments_recvd',
                'id'=>'pg_sch_payments_recvd']
]);

?>
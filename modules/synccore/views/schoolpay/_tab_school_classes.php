<?php
use yii\helpers\Html;
?>
<table class="table">
    <thead>
    <tr><th> Class</th><th>Name</th></tr>
    </thead>
    <tbody>
    <?php foreach($model->classes as $schClasses): ?>
    <tr>
        <td><?= Html::a($schClasses->class_code, ['core-school-class/view', 'id' => $schClasses->id]) ?></td>
        <td><?= Html::a($schClasses->class_description, ['core-school-class/view', 'id' => $schClasses->id], ['style'=>"color:black"]) ?></td>
    </tr>
        
   <?php endforeach; ?>
    </tbody>
   </table>

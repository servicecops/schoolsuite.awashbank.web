<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

<div class="row">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Add Student Information</h3></div>
</div>
<div class="row">

    <div class="col-sm-6 ">
        <?php
        $url = Url::to(['core-school/active-schoollist']);
        $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
        echo $form->field($model, 'school_id')->widget(Select2::classname(), [
            'initValueText' => $selectedSchool, // set the initial display text
            'size' => 'sm',
            'theme' => Select2::THEME_BOOTSTRAP,
            'class' => 'form-control  input-sm inputRequired',
            'options' => [
                'placeholder' => 'Filter School',
                'id' => 'school_search',
                'onchange' => '$.post( "' . Url::to(['core-school-class/lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#corestudent-class_id" ).html(data);
                        });'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label(false); ?>

    </div>

</div>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'first_name',['inputOptions' => ['class' => 'form-control', 'placeholder' => 'First Name']])->textInput()->label('') ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Last Name']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'middle_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Middle Name']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'date_of_birth')->widget(DatePicker::className(),
            [
                'model'=>$model,
                'attribute'=>'date_of_birth',
                'dateFormat'=>'yyyy-MM-dd',
                'clientOptions' =>[
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=>'1980:'.(date('Y')),
                    'autoSize'=>true,
                    'dateFormat'=>'yyyy-MM-dd',
                ],
                'options'=>[
                    'class'=>'form-control',
                    'placeholder'=>'Date of Birth'
                ],])->label('') ?>
    </div>
    <div class="col-md-4 ">
        <?= $form->field($model, 'gender')->dropDownList(['M'=>'Male', 'F'=>'Female'], ['prompt'=>'Gender'])->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'student_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Student Phone']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'student_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'nin_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'National ID NIN']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'school_student_registration_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Registration Number']])->textInput()->label('') ?>
    </div>
</div>


<div class="row">
    <br>
    <div class="col-sm-4">
        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[active]" value="0">
            <input type="checkbox" id="schoolinformation-active" name="SchoolInformation[active]"
                   value="1" <?= ($model->active) ? 'checked' : '' ?> >
            <label for="schoolinformation-active">Active</label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[default_part_payment_behaviour]" value="0"><input
                    type="checkbox" id="schoolinformation-default_part_payment_behaviour"
                    name="SchoolInformation[default_part_payment_behaviour]"
                    value="1" <?= ($model->disability) ? 'checked' : '' ?> >
            <label for="schoolinformation-default_part_payment_behaviour">Disability</label>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="SchoolInformation[enable_daily_stats]" value="0">
            <input type="checkbox" id="schoolinformation-enable_daily_stats"
                   name="SchoolInformation[enable_daily_stats]" value="1"
                   data-checked="negative" <?= ($model->allow_part_payments) ? 'checked' : '' ?>>
            <label for="schoolinformation-enable_daily_stats">Allow Part Payments</label>
        </div>
    </div>

</div>
<hr/>
<div class="row">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Address Information</h3></div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'region', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Region']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'district', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'District/City']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'county', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'County/Division']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'sub_county', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Sub County']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'parish', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Parish']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Village']])->textInput()->label('') ?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Guardian Information</h3></div>
</div>
<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_relation', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone']])->textInput()->label('') ?>
    </div>

</div>


<div class="form-group col-xs-12 col-sm-12 col-lg-12">
    <br>
    <hr class="l_header" style="margin-top:15px;">

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<script type="text/javascript">
    $('document').ready(function () {
        if (!$("input#schoolinformation-enable_daily_stats").attr('checked')) {
            $('input#schoolinformation-enable_daily_stats').attr('data-checked', 'negative');
            $('div#daily_stats_recipients_div').css('display', 'none');
        } else {
            $('input#schoolinformation-enable_daily_stats').attr('data-checked', 'positive');
            $('div#daily_stats_recipients_div').css('display', 'block');
        }

        $('body').on('change', 'input#schoolinformation-enable_daily_stats', function () {
            if ($(this).attr('data-checked') == 'negative') {
                $('div#daily_stats_recipients_div').css('display', 'block');
                $(this).attr('data-checked', 'postive');
            } else {
                $('div#daily_stats_recipients_div').css('display', 'none');
                $(this).attr('data-checked', 'negative');
            }
        });
    });

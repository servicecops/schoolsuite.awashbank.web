<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use app\modules\schoolcore\models\County;
use app\modules\schoolcore\models\District;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">


<?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div class="col-md-12">
        <div id="flash_message">
            <?php  if (\Yii::$app->session->hasFlash('school')) : ?>
                <?= \Yii::$app->session->getFlash('school'); ?>
            <?php endif; ?>
        </div>
    </div>

<div class="row">

    <div class="col-sm-4">
        <?= $form->field($model, 'school_name', ['inputOptions' => ['class' => 'form-control star', 'placeholder' => 'School Name']])->textInput() ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model, 'contact_person', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Person']])->textInput() ?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model, 'phone_contact_1', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Phone']])->textInput() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'phone_contact_2', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Another Contact Phone']])->textInput() ?>
</div>
    <div class="col-sm-4">
        <?= $form->field($model, 'contact_email_1', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput()?>
    </div>

    <div class="col-sm-4">
        <?= $form->field($model, 'school_type')->dropDownList(
            ArrayHelper::map(CoreControlSchoolTypes::find()->orderBy('description')->all(), 'id', 'description'), ['prompt' => 'Select Type of School ..']
        ) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'school_registration_number_format', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Reg No. Format']])->textInput() ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'sample_school_registration_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Sample Reg No.']])->textInput() ?>
    </div>

    <div class="col-sm-4">


        <?= $form->field($model, 'bank_name')->dropDownList(
            ArrayHelper::map(CoreBankDetails::find()->all(), 'id', 'bank_name'), ['prompt'=>'Select Primary Bank']
        )->label('') ?>


    </div>
</div>


<div class="row">
    <br>

    <div class="col-sm-2">
        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="CoreSchoolSelfRegistration[default_part_payment_behaviour]" value="0"><input
                    type="checkbox" id="CoreSchool-default_part_payment_behaviour"
                    name="CoreSchoolSelfRegistration[default_part_payment_behaviour]"
                    value="1" <?= ($model->default_part_payment_behaviour) ? 'checked' : '' ?> >
            <label for="CoreSchool-default_part_payment_behaviour">Allow Part Payments</label>
        </div>
    </div>

</div>
<hr/>
<div class="row">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Address Information</h3></div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?php
        $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\RefRegion::find()->all(), 'id', 'description');
        echo $form->field($model, 'region')
            ->dropDownList(
                $items,           // Flat array ('id'=>'label')
                ['prompt'=>'Select region',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });'  ]    // options
            );?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'district')->dropDownList(['prompt' => 'Select district'
            ]
            ,['id'=>'district',
                'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])
        ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'county')->dropDownList(['prompt' => 'Select county'
            ]
            ,['id'=>'county',
                'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/sub-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#subcounty" ).html(data);
                        });'])
        ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'sub_county')->dropDownList(['placeholder' => 'Select subcounty'],
            ['id'=>'subcounty',
                'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/parish-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#parish" ).html(data);
                        });']) ?>

    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'parish')->dropDownList(['placeholder' => 'Select parish'],['id'=>'parish']) ?>

    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Village']])->textInput() ?>
    </div>
</div>

<hr>
<div class="row">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Provide Bank Information</h3></div>
    <div class="col-sm-12">
        <?= $this->render('_bank_info', ['form'=>$form, 'model'=>$model, 'banks'=>$banks, 'accounts'=>$accounts, 'bank_info_error'=>$bank_info_error]); ?>
    </div>
    </div>
    <div class="row">



    </div>


<div class="form-group col-xs-12 col-sm-12 col-lg-12">
    <br>

    <h5 class="form-header">Attach Files</h5>
    <div class="form-desc">Please attach all files needed i.e. students list(doc, docx, pdf, xls, or xlsx) and school
        badge(png, jpg, jpeg, pdf)
    </div>
    <?= FileInput::widget([
        'model' => $model,
        'attribute' => 'uploads[]',
        'options' => ['multiple' => true],
        'pluginOptions' => [
            'previewFileType' => 'any',
            'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'docx', 'doc', 'xls', 'xlsx', 'pdf'],
            'showCancel' => false,
            'showUpload' => false,
            'maxFileCount' => 5,
            'fileActionSettings' => [
                'showZoom' => false,
            ]
        ]
    ]); ?>

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS
    $('document').ready(function () {
        if (!$("input#CoreSchool-enable_daily_stats").attr('checked')) {
            $('input#CoreSchool-enable_daily_stats').attr('data-checked', 'negative');
            $('div#daily_stats_recipients_div').css('display', 'none');
        } else {
            $('input#CoreSchool-enable_daily_stats').attr('data-checked', 'positive');
            $('div#daily_stats_recipients_div').css('display', 'block');
        }

        $('body').on('change', 'input#CoreSchool-enable_daily_stats', function () {
            if ($(this).attr('data-checked') == 'negative') {
                $('div#daily_stats_recipients_div').css('display', 'block');
                $(this).attr('data-checked', 'postive');
            } else {
                $('div#daily_stats_recipients_div').css('display', 'none');
                $(this).attr('data-checked', 'negative');
            }
        });
    });
JS;
$this->registerJs($script);
?>

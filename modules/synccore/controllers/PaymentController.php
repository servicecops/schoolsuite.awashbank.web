<?php


namespace app\modules\synccore\controllers;


use app\components\ToWords;
use app\modules\logs\models\Logs;


use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\PaymentRequestLog;
use app\modules\paymentscore\models\PaymentsReceived;

use Yii;
use yii\base\BaseObject;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class PaymentController extends Controller
{


    public function beforeAction($action)
    {
        return true;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

        ];
    }


    /**
     * Will receive a payment from an external payment source e.g schoolpay
     * It expects an array with {auth: {}, payment: {}}
     * auth{"externalSource": 'SCHOOLPAY', externalSourcePassword: "xxxxx", "sourceChannel": 'MTN'} -- Example of source
     * @return array|string
     *
     */
    public function actionPostPayment()
    {
        try {
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);

            if (!isset($incomingRequest['auth']))
                throw new ForbiddenHttpException('Authorization is required but not supplied ');

            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            if (!isset($incomingRequest['payment']))
                throw new ForbiddenHttpException('Payment is required but not supplied ');

            $payment = $incomingRequest['payment']; //The actual payment object
            //Authenticate external source here
            //check if all required fields are provided;
            $auth = $this->validateAuthFields($auth);

            //Now get the payment channel
            $paymentChannel = $this->authenticatePaymentChannel($auth);

            $reversal = isset($payment['reversal']) ? $payment['reversal'] : false;

            if($reversal) {
                return $this->processReversal($paymentChannel, $payment);
            }

            if (!isset($payment['amount'])) {
                //and should be numeric
                //External source not found in our db
                throw new ForbiddenHttpException('Amount is required but not supplied ');
            }
            if (!is_numeric($payment['amount'])) {
                //and should be numeric
                //External source not found in our db
                throw new ForbiddenHttpException('Amount must be a number ');
            }
            if (!isset($payment['schoolCode'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('School code is required but not supplied ');
            }

            $payment['schoolCode'] = strtoupper(str_replace(' ', '', trim($payment['schoolCode'])));

            if (!isset($payment['transactionId'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Transaction ID is required but not supplied ');
            }
            if (!isset($payment['reference'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Student Code is required but not supplied ');
            }

            // authenticate payment Channel



            $today = @date('Y-m-d', time() + 86400);
            $paymentType = isset($payment['paymentType']) ? $payment['paymentType'] : "CASH";
            $payerName = isset($payment['payerName']) ? $payment['payerName'] : "";
            $paymentWrapper = [
                "amount" => $payment['amount'],
                "channelDepositorName" => $payerName,
                "channelDepositorPhone" => @$payment['payerPhone'],
                "channelTealId" => @$payment['channelTealId'],
                "channelTransactionId" => $payment['transactionId'],
                "paymentType" => $paymentType,
                "channelMemo" => @$payment['channelMemo'],
                "processDate" => $today,
                "studentPaymentCode" => $payment['reference'],
                "schoolCode" => $payment['schoolCode'],
                "reversal" => $reversal,
                "originalTransactionId" => @$payment['originalTransactionId'],
                "channelDepositorBranch" => @$payment['channelDepositorBranch'],

            ];
            // Yii::trace($paymentWrapper);
            $response = $this->receiveSchoolpayPayment($paymentChannel, $paymentWrapper);
            //Yii::trace($response);
            $result = $response[0]['result'];

            // Yii::trace($result->returnCode);
            return $result;
        } catch (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student payment: ", $error, null);

            return json_encode([
                'returnCode' => 9091,
                'returnMessage' => $e->getMessage(),
            ]);

        }


    }

    private function processReversal($paymentChannel, $payment) {
        $today = @date('Y-m-d', time() + 86400);

        if (!isset($payment['originalTransactionId']))
            throw new ForbiddenHttpException('Original transaction id is required but not supplied ');

        if (!isset($payment['channelTransactionId']))
            throw new ForbiddenHttpException('Transaction id is required but not supplied ');

        $payerName = isset($payment['payerName']) ? $payment['payerName'] : "";

        $reversalObject = [
            "channelTealId" => @$payment['channelTealId'],
            "originalChannelTransactionToReverseId" => $payment['originalTransactionId'],
            "channelTransactionId" => $payment['channelTransactionId'],
            "processDate" => $today,
            "channelDepositorBranch" => @$payment['channelDepositorBranch'],
            "channelReasonForReversal" => "Reversal of " . $payment['originalTransactionId'],
            "channelDepositorName" => $payerName,
            "channelDepositorPhone" => @$payment['payerPhone'],

        ];

        $payment_json = json_encode($reversalObject);
        //call sp to post payment

        $response = \Yii::$app->db->createCommand("Select * from reverse_payment(:chnId,:jsonText) AS result")
            ->bindValue(':chnId', $paymentChannel->id)
            ->bindValue(':jsonText', $payment_json)
            ->queryAll();
        //$transaction->commit();
        $result = $response[0]['result'];
        return $result;
    }


    private function receiveSchoolpayPayment($paymentChannel, $payment)
    {
        try {
            $payment_json = json_encode($payment);
            //call sp to post payment

            $res = \Yii::$app->db->createCommand("Select * from sp_process_student_payment(:chnId,:jsonText) AS result")
                ->bindValue(':chnId', $paymentChannel->id)
                ->bindValue(':jsonText', $payment_json)
                ->queryAll();
            //$transaction->commit();
            return $res;

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student payment: ", $error, null);

            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }
    }

    private function receiveSupplementaryPayment($paymentChannel, $payment)
    {
        try {
            $payment_json = json_encode($payment);
            //call sp to post payment

            $res = \Yii::$app->db->createCommand("Select * from sp_process_student_supplementary_payment(:chnId,:jsonText) AS result")
                ->bindValue(':chnId', $paymentChannel->id)
                ->bindValue(':jsonText', $payment_json)
                ->queryAll();
            //$transaction->commit();
            return $res;

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting supplementary fee payment: ", $error, null);

            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }
    }

    public function actionQueryStudentsInformation()
    {
        try {
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);

            if (!isset($incomingRequest['auth']))
                throw new ForbiddenHttpException('Authorization is required but not supplied ');

            if (!isset($incomingRequest['student']))
                throw new ForbiddenHttpException('Query information is required but not supplied ');

            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            $student = $incomingRequest['student']; //The actual payment object
            //Authenticate external source here
            Yii::trace($auth);


            //check if all required fields are provided;
            $auth = $this->validateAuthFields($auth);


            if (!isset($student['reference'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Student Code is required but not supplied');
            }

            if (!isset($student['schoolCode'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('School Code is required but not supplied');
            }

            $student['schoolCode'] = strtoupper(str_replace(' ', '', trim($student['schoolCode'])));
            // authenticate payment Channel
            $paymnetChannel = $this->authenticatePaymentChannel($auth);


            if (isset($student['amount']) && !preg_match("#^[0-9]{1,}$#", $student['amount'])) {
                //and should be numeric
                //External source not found in our db
                throw new ForbiddenHttpException('Amount is required but not supplied ');
            }

            Yii::trace("Calling sp");
            $response = \Yii::$app->db->createCommand("Select * from sp_student_inquiry(:chnId,:studentCode,:amount, :schoolCode) AS result")
                ->bindValue(':chnId', $paymnetChannel->id)
                ->bindValue(':studentCode', $student['reference'])
                ->bindValue(':amount', @$student['amount'])
                ->bindValue(':schoolCode', $student['schoolCode'])
                ->queryAll();
            $result = $response[0]['result'];

            return $result;
        } catch (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student payment: ", $error, null);

            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }

    }

    /**
     * @param $auth
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function validateAuthFields($auth)
    {
        if (!isset($auth['hash'])) {
            //External source not found in our db
            throw new ForbiddenHttpException('Hash is required but not supplied ');
        }
        if (!isset($auth['nonce'])) {
            //External source not found in our db
            throw new ForbiddenHttpException('Nonce is required but not supplied ');
        }
        if (!isset($auth['accessCode'])) {
            //External source not found in our db
            throw new ForbiddenHttpException('Access Code is required but not supplied ');
        }
        return $auth;
    }

    /**
     * @param $auth
     * @return PaymentChannels
     * @throws ForbiddenHttpException
     */
    public function authenticatePaymentChannel($auth)
    {
        //Now get the payment channel
        $paymentChannel = PaymentChannels::findOne(['channel_code' => $auth['accessCode']]);
        if (!$paymentChannel)
            throw new ForbiddenHttpException('Payment channel ' . $auth['accessCode'] . ' not found');


        //compare hash
        $password = $paymentChannel->password;
        $nonce = $auth['nonce'];
        $hash = md5($password . $nonce);

        if (strtoupper($hash) != strtoupper($auth['hash'])) {
            //External source not found in our db
            throw new ForbiddenHttpException('Invalid hash, access denied');
        }

        //Check if nonce was already used
        $existingNonce = PaymentRequestLog::findOne(['payment_channel' => $paymentChannel->id, 'channel_request_id' => $auth['nonce']]);
        if ($existingNonce) {
            throw new ForbiddenHttpException('Dirty nonce. Security nonce was already used for a previous request');

        } else {
            $record = new PaymentRequestLog();
            $record->payment_channel = $paymentChannel->id;
            $record->channel_request_id = $auth['nonce'];
            $record->signature = $auth['hash'];
            $record->save(false);

        }
        return $paymentChannel;
    }

    private function queryStudentForPayment(PaymentChannels $paymentChannel, array $paymentWrapper)
    {

        $studentPaymentCode = $paymentWrapper['studentPaymentCode'];
        $schoolCode = $paymentWrapper['schoolCode'];
        if (!isset($studentPaymentCode)) {
            //and should be numeric
            //External source not found in our db
            throw new ForbiddenHttpException('STUDENT PAYMENT CODE IS REQUIRED AND WAS NOT SUPPLIED ');
        }

        $studentCode = trim(strtoupper($studentPaymentCode));
        $operationReturnObject = $this->getStudentByPaymentCodeOrRegistrationNumber($studentCode, $schoolCode);
        return $operationReturnObject;


    }

    private function getStudentByPaymentCodeOrRegistrationNumber($studentCode, $schoolCode)
    {
        $studentDetails = (new Query())
            ->select(['si.id', 'si.first_name', 'si.middle_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'sch.school_code', 'si.active', 'si.school_id', 'si.student_code',
                'si.allow_part_payments',
                'sag.outstanding_balance',
                'si.school_student_registration_number'])
            ->from('core_student si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            ->innerJoin('student_account_gl sag', 'sag.id=si.id')
//            ->where(['si.student_code'=>$studentCode])
            ->andWhere(['si.archived' => false])
            ->andWhere(['si.school_student_registration_number' => $studentCode])
            ->andWhere(['sch.external_school_code' => $schoolCode])
            ->all();

        if (!$studentDetails)
            throw new ForbiddenHttpException('No student was found with the specified payment code or registration number ');

        if (count($studentDetails) > 1)
            throw new ForbiddenHttpException('Student selection failed. Use the 10 digit payment code to select specific student');

        return $studentDetails;
    }

    public function actionLookupStudentsByGuardianPhoneNumber()
    {
        try {
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);


            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            $student = $incomingRequest['student']; //The actual payment object
            //Authenticate external source here
            $auth = $this->validateAuthFields($auth);

            if (!isset($student['phoneNumber'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Phone Number is required but not supplied ');
            }

            // authenticate payment Channel


            //Now get the payment channel
            $paymentChannel = $this->authenticatePaymentChannel($auth);
            $studentWrapper = [
                "phoneNumber" => $student['phoneNumber'],
            ];

            $correctedPhone = ToWords::validatePhoneNumber($student['phoneNumber']);

            $orPhoneChecker = [
                'or',
                ['si.guardian_phone' => $studentWrapper['phoneNumber']],
                ['si.guardian2_phone' => $studentWrapper['phoneNumber']],
            ];
            if($correctedPhone) {
                array_push($orPhoneChecker,
                    ['si.guardian_phone' => $correctedPhone],
                    ['si.guardian2_phone' => $correctedPhone]);
            }

            $studentDetails = (new Query())
                ->select(['si.first_name as firstName',
                    'si.last_name as lastName',
                    'si.middle_name as middleName',
                    "concat_ws(' ', si.first_name, si.middle_name, si.last_name) as studentFullName",
                    'si.student_code as paymentCode',
                    'sch.external_school_code as schoolCode',
                    'sch.school_name as schoolName',
                    "format('%s - %s', clss.class_code, clss.class_description) as studentClass",
                    'si.school_student_registration_number as registrationNumber'])
                ->from('core_student si')
                ->innerJoin('core_school sch', 'si.school_id=sch.id')
                ->innerJoin('core_school_class clss', 'si.class_id=clss.id')
                ->andFilterWhere($orPhoneChecker)
                ->andWhere(['si.archived' => false])
                ->all();
            $response = $studentDetails;
            if (!$response)
                throw new ForbiddenHttpException('No students found for this guardian phone number');


            //Yii::trace($response);
            $result = $response;

            return json_encode([
                'returnCode' => 0,
                'returnMessage' => "Student details found",
                'studentDetails' => $result
            ]);


        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }

    }



    /**
     * Will receive a payment from an external payment source e.g schoolpay
     * It expects an array with {auth: {}, payment: {}}
     * auth{"externalSource": 'SCHOOLPAY', externalSourcePassword: "xxxxx", "sourceChannel": 'MTN'} -- Example of source
     * @return array|string
     *
     */
    public function actionPostSupplementaryFeePayment()
    {
        try {
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);

            if (!isset($incomingRequest['auth']))
                throw new ForbiddenHttpException('Authorization is required but not supplied ');

            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            if (!isset($incomingRequest['payment']))
                throw new ForbiddenHttpException('Payment is required but not supplied ');

            $payment = $incomingRequest['payment']; //The actual payment object
            //Authenticate external source here
            //check if all required fields are provided;
            $auth = $this->validateAuthFields($auth);

            //Now get the payment channel
            $paymentChannel = $this->authenticatePaymentChannel($auth);

            $reversal = isset($payment['reversal']) ? $payment['reversal'] : false;

            if($reversal) {
                return $this->processSupplementaryReversal($paymentChannel, $payment);
            }

            if (!isset($payment['amount'])) {
                //and should be numeric
                //External source not found in our db
                throw new ForbiddenHttpException('Amount is required but not supplied ');
            }
            if (!is_numeric($payment['amount'])) {
                //and should be numeric
                //External source not found in our db
                throw new ForbiddenHttpException('Amount must be a number ');
            }
            if (!isset($payment['schoolCode'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('School code is required but not supplied ');
            }

            $payment['schoolCode'] = strtoupper(str_replace(' ', '', trim($payment['schoolCode'])));

            if (!isset($payment['transactionId'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Transaction ID is required but not supplied ');
            }
            if (!isset($payment['reference'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Student Code is required but not supplied ');
            }

            if (!isset($payment['feeId'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Fee ID is required ');
            }

            // authenticate payment Channel



            $today = @date('Y-m-d', time() + 86400);
            $paymentType = isset($payment['paymentType']) ? $payment['paymentType'] : "CASH";
            $payerName = isset($payment['payerName']) ? $payment['payerName'] : "";
            $paymentWrapper = [
                "amount" => $payment['amount'],
                "channelDepositorName" => $payerName,
                "channelDepositorPhone" => @$payment['payerPhone'],
                "channelTealId" => @$payment['channelTealId'],
                "channelTransactionId" => $payment['transactionId'],
                "paymentType" => $paymentType,
                "channelMemo" => @$payment['channelMemo'],
                "processDate" => $today,
                "studentPaymentCode" => $payment['reference'],
                "schoolCode" => $payment['schoolCode'],
                "reversal" => $reversal,
                "originalTransactionId" => @$payment['originalTransactionId'],
                "channelDepositorBranch" => @$payment['channelDepositorBranch'],
                "feeId" => @$payment['feeId'],

            ];
            // Yii::trace($paymentWrapper);
            $response = $this->receiveSupplementaryPayment($paymentChannel, $paymentWrapper);
            //Yii::trace($response);
            $result = $response[0]['result'];

            // Yii::trace($result->returnCode);
            return $result;
        } catch (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student payment: ", $error, null);

            return json_encode([
                'returnCode' => 9091,
                'returnMessage' => $e->getMessage(),
            ]);

        }


    }



    private function processSupplementaryReversal($paymentChannel, $payment) {
        $today = @date('Y-m-d', time() + 86400);

        if (!isset($payment['originalTransactionId']))
            throw new ForbiddenHttpException('Original transaction id is required but not supplied ');

        if (!isset($payment['channelTransactionId']))
            throw new ForbiddenHttpException('Transaction id is required but not supplied ');

        $payerName = isset($payment['payerName']) ? $payment['payerName'] : "";

        $reversalObject = [
            "channelTealId" => @$payment['channelTealId'],
            "originalChannelTransactionToReverseId" => $payment['originalTransactionId'],
            "channelTransactionId" => $payment['channelTransactionId'],
            "processDate" => $today,
            "channelDepositorBranch" => @$payment['channelDepositorBranch'],
            "channelReasonForReversal" => "Reversal of " . $payment['originalTransactionId'],
            "channelDepositorName" => $payerName,
            "channelDepositorPhone" => @$payment['payerPhone'],

        ];

        $payment_json = json_encode($reversalObject);
        //call sp to post payment

        $response = \Yii::$app->db->createCommand("Select * from sp_reverse_supplementary_fee_payment(:chnId,:jsonText) AS result")
            ->bindValue(':chnId', $paymentChannel->id)
            ->bindValue(':jsonText', $payment_json)
            ->queryAll();
        //$transaction->commit();
        $result = $response[0]['result'];
        return $result;
    }



    public function actionSearchSchoolByName()
    {
        try {
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);


            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            $search = $incomingRequest['search']; //The actual payment object
            //Authenticate external source here
            $auth = $this->validateAuthFields($auth);

            if (!isset($search['schoolName'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Search name is required');
            }

            //Now get the payment channel
            $paymentChannel = $this->authenticatePaymentChannel($auth);

            $schoolList = (new Query())
                ->select([
                    'sch.id as id',
                    'sch.external_school_code as schoolCode',
                    'sch.school_name as schoolName'])
                ->from('core_school sch')
                ->filterWhere(['ilike', 'school_name', $search['schoolName']])
                ->orderBy(['school_name' => SORT_ASC])
                ->limit(100)
                ->all();
            $response = $schoolList;
            if (!$response) {
                $response = [];
            }

            return json_encode([
                'returnCode' => 0,
                'returnMessage' => "Search completed",
                'schoolList' => $response
            ]);


        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }
    }

    public function actionSearchStudentByName()
    {
        try {
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);


            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            $search = $incomingRequest['search']; //The actual payment object
            //Authenticate external source here
            $auth = $this->validateAuthFields($auth);

            if (!isset($search['schoolCode'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Search school code is required');
            }

            if (!isset($search['studentName'])) {
                //External source not found in our db
                throw new ForbiddenHttpException('Search student name is required');
            }

            //Now get the payment channel
            $paymentChannel = $this->authenticatePaymentChannel($auth);

            $selector = [
                'or',
                ['ilike', 'si.first_name', $search['studentName']],
                ['ilike', 'si.last_name', $search['studentName']],
                ['ilike', 'si.middle_name', $search['studentName']]
            ];


            $studentList = (new Query())
                ->select(['si.first_name as firstName',
                    'si.last_name as lastName',
                    'si.middle_name as middleName',
                    "concat_ws(' ', si.first_name, si.middle_name, si.last_name) as studentFullName",
                    'si.student_code as paymentCode',
                    'sch.external_school_code as schoolCode',
                    'sch.school_name as schoolName',
                    "format('%s - %s', clss.class_code, clss.class_description) as studentClass",
                    'si.school_student_registration_number as registrationNumber'])
                ->from('core_student si')
                ->innerJoin('core_school sch', 'si.school_id=sch.id')
                ->innerJoin('core_school_class clss', 'si.class_id=clss.id')
                ->where(['sch.external_school_code' => strtoupper($search['schoolCode'])])
                ->andFilterWhere($selector)
                ->andWhere(['si.archived' => false])
                ->andWhere(['si.active' => true])
                ->orderBy(['school_name' => SORT_ASC])
                ->limit(100)
                ->all();


            if (!$studentList) {
                $studentList = [];
            }

            return json_encode([
                'returnCode' => 0,
                'returnMessage' => "Search completed",
                'studentList' => $studentList
            ]);


        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }
    }


}

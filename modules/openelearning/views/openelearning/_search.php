<?php

use app\modules\schoolcore\models\CoreSchoolSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container bg-white">
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row mt-3">
            <div class="col ml-lg-5">
                <?= $form->field($model, 'school_code',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Code'])->label(false) ?>

            </div>
            <div class="col ml-0">
                <?= $form->field($model, 'school_name',  ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'School Name'])->label(false) ?>

            </div>

            <div class="col"><?= Html::submitButton('Search', ['class' => 'btn bg-gradient-primary']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

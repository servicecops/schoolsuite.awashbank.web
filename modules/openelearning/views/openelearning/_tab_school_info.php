<?php

use yii\helpers\Html;

?>


<div class="row">

    <div class="col border-right bg-gradient-light">
        <div class="card bg-gradient-light">
            <div class="card-body">

                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('school_code') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->school_code) ? $model->school_code : "--" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('village') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->village) ? $model->village : "--" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('district_id') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->districtName->district_name) ? $model->districtName->district_name : "--" ?>
                    </div>


                </div>

                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('region') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->regionName->description) ? $model->regionName->description : "--" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('school_type') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->school_type) ? $model->schoolType->description : "-- " ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('contact_email') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->contact_email_1) ? $model->contact_email_1 : "--" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('contact_person') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->contact_person) ? $model->contact_person : " --" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('contact_phone1') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->phone_contact_1) ? $model->phone_contact_1 : "--" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('bank_name') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->bank_name) ? $model->bankName->bank_name : " --" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('contact_phone2') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->phone_contact_2) ? $model->phone_contact_2 : "--" ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col bg-gradient-light">
        <div class="card bg-gradient-light">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('date_created') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= $model->date_created ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('school_registration_number_format') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->school_registration_number_format) ? $model->school_registration_number_format : " --" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('sample_school_registration_number') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->sample_school_registration_number) ? $model->sample_school_registration_number : " --" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('active') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->active) ? "Yes" : "No" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('default_part_payment_behaviour') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->default_part_payment_behaviour) ? "Yes" : "No" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('daily_stats_recipients') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->daily_stats_recipients) ? $model->daily_stats_recipients : "--" ?>
                    </div>


                </div>

                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000"><?= $model->getAttributeLabel('transaction_api_password') ?></b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->transaction_api_password) ? $model->transaction_api_password : "--" ?>
                    </div>


                </div>

                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000">Enable Transaction Api</b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->enable_transaction_api) ? "Yes" : "No" ?>
                    </div>


                </div>
                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000">Enable Bank Statement</b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->enable_bank_statement) ? "Yes" : "No" ?>
                    </div>


                </div>

                <hr class="style14">
                <div class="row">
                    <div class="col">
                        <b style="color: #000">Enable Sections Logic</b>
                    </div>
                    <div class="col" style="color: #21211f">
                        <?= ($model->enable_school_sections_logic) ? "Yes" : "No" ?>
                    </div>


                </div>



            </div>
        </div>
    </div>
</div>
<div class="col-xs-12">
    <table class="table" style="">
        <thead>
        <tr>
            <th colspan="4" style="text-align:center"><h5>Bank Information</h5></th>
        </tr>
        <tr>
            <th> Bank</th>
            <th> Account Title</th>
            <th> Account Type</th>
            <th> Account Number</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($accounts) :
            foreach ($accounts as $k => $v): ?>
                <tr>
                    <td><?= $v['bank_name'] ?></td>
                    <td><?= $v['account_title'] ?></td>
                    <td><?= $v['account_type'] ?></td>
                    <td><?= $v['account_number'] ?></td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr>
                <td colspan="4">No account provided for this school</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<?php if ($model->enable_school_sections_logic) : ?>
    <div class="col-xs-12">
        <table class="table" style="">
            <thead>
            <tr>
                <th colspan="4" style="text-align:center"><h3>School Sections</h3></th>
            </tr>
            <tr>
                <th> Section Code</th>
                <th> Section Name</th>
                <th> Section Pri. Bank</th>
                <th> Section Account</th>

            </tr>
            </thead>
            <tbody>
            <?php
            if ($sections) :
                foreach ($sections as $k => $v): ?>
                    <tr>
                        <td><?= $v['section_code'] ?></td>
                        <td><?= $v['section_name'] ?></td>
                        <td><?= $v['bank_name'] ?></td>
                        <td><?= $v['section_bank_account'] ?></td>
                    </tr>
                <?php endforeach;
            else :
                ?>
                <tr>
                    <td colspan="2">No sections available for this school</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>



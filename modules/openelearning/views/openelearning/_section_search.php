<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

use yii\web\JsExpression;
?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['sections'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>


    <div class="col-xs-10 no-padding">
        <?php 
            $url = Url::to(['/school-information/sections']);
            $selectedSchool = empty($model->schsearch) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->schsearch)->school_name;
            echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size'=>'sm',
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School', 
                    'id'=>'section_selected_school_id',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/school-information/schoollist']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>
            </div>
    <div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
$("document").ready(function(){ 
    var sch = $("#section_selected_school_id").val();
   
    $('body').on('change', '#section_selected_school_id', function(){

    });

  });
JS;
$this->registerJs($script);
?>

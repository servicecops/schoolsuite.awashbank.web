<?php
namespace app\modules\openelearning;

class OpenLearningModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\openelearning\controllers';
    public function init() {
        parent::init();
    }
}

<?php

namespace app\modules\library\controllers;

use app\components\Helpers;
use app\components\ToWords;
use app\modules\library\models\BorrowBook;
use app\modules\library\models\BorrowBookTracker;
use app\modules\library\models\CatalogueUploads;
use app\modules\library\models\Category;
use app\modules\library\models\CategorySearch;
use app\modules\library\models\Catalogue;
use app\modules\library\models\CatalogueSearch;
use app\modules\schoolcore\models\Book;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Yii;
use app\controllers\BaseController;
use yii\db\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\logs\models\Logs;

use app\modules\schoolcore\models\CoreStudent;
use yii\web\UploadedFile;


if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * CategoryController that handles the lesson plan/syllabus
 */
class CatalogueController extends BaseController
{


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Display the category
     */
    public function actionIndex()
    {

        $request = Yii::$app->request;
        if (ToWords::isSchoolUser() || Yii::$app->user->can('schoolpay_admin') || (!Helpers::is('non_student'))) {
            $searchModel = new CatalogueSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider]);

        } else {
            throw new ForbiddenHttpException('No permissions to view the subjects.');
        }
    }


    //you must override these methods to use BaseControler.

    /**
     * Creates a new Category model
     * @return Catalogue
     */
    public function newModel()
    {
        $model = new Catalogue();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    public function createModelFormTitle()
    {
        return 'Create Catalogue';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    /**
     * @return CatalogueSearch
     */
    public function newSearchModel()
    {
        $searchModel = new CatalogueSearch();
        return $searchModel;
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

//        if (Yii::$app->user->can('non_student')) {
//            throw new ForbiddenHttpException('No permission to create or upload the Category / publications. Only students are authorised to do so');
//        }

        if (empty(Yii::$app->user->identity)) {
            return Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new Catalogue();

        try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();
                $model->school_id = Yii::$app->user->identity->school_id;

                $model->uploads = UploadedFile::getInstances($model, 'uploads');
                if ($model->save(false)) {
                    Logs::logEvent("Created New Catalogue : " . $model->id, null, null);
                    $model->saveUploads(); //fix the upload issue here
                    $this->createBorrowerTracker($model);
                }


                $transaction->commit();

                //Yii::trace($model);
                return $this->redirect(['view', 'id' => $model->id]);

            }


        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            Yii::$app->session->setFlash('viewError', $error);
            Logs::logEvent("Failed create new Category : ", $error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }


    /**
     * Displays a single Category  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') )
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');

        if (empty(Yii::$app->user->identity)) {

            return Yii::$app->runAction('/site/login');
        }

        $model = $this->findModel($id);


        $responseData = [
            'model' => $model
        ];

        return ((Yii::$app->request->isAjax)) ?
            $this->renderAjax('view', $responseData)
            :
            $this->render('view', $responseData);


    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Catalogue::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rw_library')) {
            throw new ForbiddenHttpException('No permission to Update Library Category.');
        }

        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post())) {


                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                if ($model->save(false)) {
                    Logs::logEvent("Updated Library Catalogue: " . $model->id, null, null);
                    $model->saveAndUpdateUploads(); //ofcourse first delete old uploads n upload new ones
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);

        }


        //$model->date_created = ($model->date_created)? date('Y-m-d', strtotime($model->date_created)) : '' ;

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Retrieves the staff information for particular school.
     * @return mixed
     */
    public function actionAuthorList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'author_name'])
                ->from('library_book_author')
                ->where(['ilike', 'author_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['author_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }

    /**
     * Retrieves the publisher information for particular school.
     * @return mixed
     */
    public function actionPublisherList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'publisher_name'])
                ->from('library_book_publisher')
                ->where(['ilike', 'publisher_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['publisher_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }

    /**
     * Retrieves the book category information for particular school.
     * @return mixed
     */
    public function actionCategoryList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'category_name'])
                ->from('lib_book_category')
                ->where(['ilike', 'category_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['category_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }


    /**
     * Retrieves the book category information for particular school.
     * @return mixed
     */
    public function actionShelfList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'shelf_name'])
                ->from('library_book_shelf')
                ->where(['ilike', 'shelf_name', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])

                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['shelf_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }

    public function actionStdList($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;
            $conct = "CONCAT(student_code,' ', first_name,' ',last_name) as student_code";
            $query->select(['id', $conct])
                ->from('core_student')
                ->where(['student_code' => $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['student_code']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }


    /**
     * Remove the uploaded file / Document
     * @param integer $id
     * @return mixed
     */
    public function actionRemoveFile($id)
    {
        //allow only the student to delete the file
        if (Yii::$app->user->can('rw_library')) {
            $model = CatalogueUploads::find()->where(['id' => $id])->limit(1)->one();
            if ($model) {
                $catId = $model->submission_id;
                $model->delete();
                return $this->redirect(['view', 'id' => $catId]);
            } else {
                throw new NotFoundHttpException('The uploaded dissertation or publication does not exist');
            }
        } else {
            throw new ForbiddenHttpException('You have no permission to delete the uploaded files.');
        }
    }


    /**
     * Remove the uploaded file / Document
     * @param integer $id
     * @return mixed
     */
    public function actionBorrowCatalogue($id)
    {
        //allow only the student to delete the file
        if (!Yii::$app->user->can('rw_library')) {
            throw new ForbiddenHttpException('You have no permission to perform this action.');
        }
        $model = Catalogue::find()->where(['id' => $id])->limit(1)->one();
        Yii::trace($model);
        if (!$model) {
            throw new NotFoundHttpException('The selected catalogue does not exist');
        }
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model2 = new BorrowBook();
        try {
            if ($model2->load(Yii::$app->request->post())) {

                $model2->issued_by = Yii::$app->user->identity->getId();
                $model2->school_id = Yii::$app->user->identity->school_id;

                //if u school user
                $existingContent = BorrowBookTracker::findOne(['catalogue_id' => $id]);
                //  Yii::trace("wapi ".$existingContent);
                Yii::trace("wawelu here");
                // check if sch or std code are supplied
                if((isset($model2->student_code) && $model2->student_code == null) && (isset($model2->staff_id) && $model2->staff_id == null)){
                    Yii::trace("here");
                    Yii::$app->session->setFlash('actionFailed', 'Failed: Borrower cannot be empty, please enter either Student Code or Staff Name');
                    return Yii::$app->runAction('/library/catalogue/view', ['id' => $model->id]);

                }
                //check if books are out of store

                if (isset($existingContent) || isset($model2->number_of_copies_borrowed)) {


                    if (($existingContent->total_remaining - $model2->number_of_copies_borrowed) < 1 || $existingContent->total_remaining < 1) {
                        Yii::$app->session->setFlash('actionFailed', 'Failed: Sorry, we out of this catalogue or the number of copies requested for this catalogue exceed what we currently have');
                        return Yii::$app->runAction('/library/catalogue/view', ['id' => $model->id]);
                        //return \Yii::$app->runAction('/library/core-student/error');
                    }
                }


                //check if student hasnot yet returned this book and return
                if (isset($model2->student_code) && $model2->student_code != null) {
                    $exisitingRecord = BorrowBook::findOne(['student_code' => $model2->student_code, 'book_returned' => false, 'catalogue_id' => $id]);
                    if ($exisitingRecord) {

                        Yii::$app->session->setFlash('actionFailed', 'Failed: Student has already borrowed this book and has not yet returned it');
                        return Yii::$app->runAction('/library/catalogue/view', ['id' => $model->id]);
                        //return \Yii::$app->runAction('/library/core-student/error');

                    }
                }

                $schoolId = Yii::$app->user->identity->school_id;

                $model2->school_id = $schoolId;
                $model2->catalogue_id = $id;
                $model2->book_returned = false;


                if ($model2->save(false)) {
                    Logs::logEvent("Lent out catalogue : " . $model->id . 'to' . $model2->student_code, null, null);

                    //update borrwing tracker

                    $this->updateBorrowingTracker($model2);


                }

                $transaction->commit();

                //Yii::trace($model);
                return $this->redirect(['view', 'id' => $model->id]);

            }


        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            Yii::$app->session->setFlash('viewError', $error);
            Logs::logEvent("Failed create new Category : ", $error, null);
        }

        $res = ['model' => $model, 'model2' => $model2];
        return ($request->isAjax) ? $this->renderAjax('borrow_book', $res) : $this->render('borrow_book', $res);

    }

    private function updateBorrowingTracker($model)
    {
        //exisiting content


        $cat = Catalogue::findOne(['id' => $model->catalogue_id]);
        $remaining = 0;
        $total = 0;
        if ($cat) {
            $copies = $cat->number_of_copies;
            Yii::trace($copies);
            Yii::trace($model->number_of_copies_borrowed);
            $remaining = $copies - $model->number_of_copies_borrowed;
            Yii::trace($remaining);


        }

        $existingContent = BorrowBookTracker::findOne(['catalogue_id' => $model->catalogue_id]);

        if ($existingContent) {
            Yii::trace("found");


            if (!$existingContent->number_of_copies_borrowed) {
                $remaining = $copies - $model->number_of_copies_borrowed;
            } else {
                $remaining = $copies - ($model->number_of_copies_borrowed + $existingContent->number_of_copies_borrowed);
            }

            Yii::trace($remaining);

            $existingContent->catalogue_id = $model->catalogue_id;
            $existingContent->number_of_copies_borrowed = $model->number_of_copies_borrowed + $existingContent->number_of_copies_borrowed;
            $existingContent->total_remaining = $remaining;
            $existingContent->save(false);

        } else {
            Yii::trace("not found");
            Yii::trace($remaining);

            $newRecord = new BorrowBookTracker();
            $newRecord->catalogue_id = $model->catalogue_id;
            $newRecord->number_of_copies_borrowed = $model->number_of_copies_borrowed;
            $newRecord->total_remaining = $remaining;
            $newRecord->total_copies = $copies;

            $newRecord->save(false);


        }

    }

    private function createBorrowerTracker(Catalogue $model)
    {

        $newRecord = new BorrowBookTracker();
        $newRecord->catalogue_id = $model->id;
        $newRecord->total_remaining = $model->number_of_copies;
        $newRecord->number_of_copies_borrowed = 0;
        $newRecord->total_copies = $model->number_of_copies;
        $newRecord->school_id = Yii::$app->user->identity->school_id;


        $newRecord->save(false);

    }

    public function actionReturnCatalogue($id)
    {

        if (!Yii::$app->user->can('rw_library')) {
            throw new ForbiddenHttpException('You have no permission to perform this action.');
        }
        $model = Catalogue::find()->where(['id' => $id])->limit(1)->one();
        Yii::trace($model);
        if (!$model) {
            throw new NotFoundHttpException('The selected catalogue does not exist');
        }
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model2 = new BorrowBook();
        try {
            if ($model2->load(Yii::$app->request->post())) {


                //check if student borrowed
                if (isset($model2->student_code) && $model2->student_code != null) {
                    $exisitingRecord = BorrowBook::findOne(['student_code' => $model2->student_code, 'book_returned' => false, 'catalogue_id' => $id]);
                    if (!$exisitingRecord) {

                        Yii::$app->session->setFlash('actionFailed', 'Failed, The Student has no records of borrowing this books');
                        return Yii::$app->runAction('/library/catalogue/view', ['id' => $model->id]);
                        //return \Yii::$app->runAction('/library/core-student/error');

                    }
                }

                if (isset($model2->staff_id) && $model2->staff_id != null) {
                    $exisitingRecord = BorrowBook::findOne(['staff_id' => $model2->staff_id, 'book_returned' => false, 'catalogue_id' => $id]);
                    if (!$exisitingRecord) {

                        Yii::$app->session->setFlash('actionFailed', 'Failed, This Staff member has no records of borrowing this books');
                        return Yii::$app->runAction('/library/catalogue/view', ['id' => $model->id]);
                        //return \Yii::$app->runAction('/library/core-student/error');

                    }
                }


                //check if staff borrowed

                //check user exists
                $existingContent = BorrowBookTracker::findOne(['catalogue_id' => $id]);
                $existingBorrowRecord = BorrowBook::findOne(['catalogue_id' => $id]);


                if ($existingBorrowRecord) {
                    if ($model2->number_of_copies_returned > $existingBorrowRecord->number_of_copies_borrowed) {
                        Yii::$app->session->setFlash('actionFailed', 'Number of books returned are greater than number of books borrowed');
                        return Yii::$app->runAction('/library/catalogue/view', ['id' => $model->id]);
                        //return \Yii::$app->runAction('/library/core-student/error');

                    }


                    //$existingContent->return_date = new Date();
                    $existingBorrowRecord->number_of_copies_returned = $model2->number_of_copies_returned;
                    $existingBorrowRecord->return_remarks = $model2->return_remarks;
                    $existingBorrowRecord->actual_return_date = new Expression('NOW()');
                    $existingBorrowRecord->book_returned = true;

                    $existingBorrowRecord->save(false);
                }

                if ($existingContent) {

                    $existingContent->number_of_copies_returned = $model2->number_of_copies_returned;
                    $existingContent->total_remaining += $model2->number_of_copies_returned;
                    $existingContent->save(false);
                }


                $transaction->commit();

                //Yii::trace($model);
                return $this->redirect(['view', 'id' => $model->id]);

            }


        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            Yii::$app->session->setFlash('viewError', $error);
            Logs::logEvent("Failed create new Category : ", $error, null);
        }

        $res = ['model' => $model, 'model2' => $model2];
        return ($request->isAjax) ? $this->renderAjax('return_book', $res) : $this->render('return_book', $res);

    }

    public function actionBorrowinghistory()
    {
        $request = Yii::$app->request;
        if (Yii::$app->user->can('library_reports') || (!Helpers::is('non_student'))) {

            $searchModel = new BorrowBook();
            if (ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $data = $searchModel->searchPBorrowingHistory($schId, Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                Yii::trace($dataProvider);
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->searchPBorrowingHistory('', Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }


            return ($request->isAjax) ? $this->renderAjax('borrowing_histories', [
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('borrowing_histories', ['dataProvider' => $dataProvider,
                    'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);

        } else {
            throw new ForbiddenHttpException("Hello,  you have no permission to view the Library history");
        }
    }


public
function actionBorrowingtracker()
{
    $request = Yii::$app->request;
    if (!Yii::$app->user->can('library_reports')) {
        throw new ForbiddenHttpException("Hello,  you have no permission to view the Library history");
    }
    $searchModel = new BorrowBook();
    if (ToWords::isSchoolUser()) {
        $schId = Yii::$app->user->identity->school->id;
        $data = $searchModel->searchBorrowingTracker($schId, Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        Yii::trace($dataProvider);
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
    } else {
        $data = $searchModel->searchBorrowingTracker('', Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
    }


    return ($request->isAjax) ? $this->renderAjax('borrowing_tracker', [
        'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
        $this->render('borrowing_tracker', ['dataProvider' => $dataProvider,
            'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);

}


    /**
     * View/Download Document
     * @param integer $id
     * @return mixed
     */
    public function actionViewFile($id){
        $response = Yii::$app->response;
        $model = CatalogueUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'pdf']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        if ($ext == 'pdf') {
            $response->headers->set('Content-type', 'application/pdf');
        } else {
            $response->headers->set('Content-type', $ext);
        }

        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);

        return $response;
    }



}

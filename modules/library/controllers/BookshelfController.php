<?php

namespace app\modules\library\controllers;

use app\components\Helpers;
use app\modules\library\models\BookShelf;
use app\modules\library\models\BookShelfSearch;


use app\modules\schoolcore\models\Book;
use Yii;
use app\controllers\BaseController;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\logs\models\Logs;

use app\modules\schoolcore\models\CoreStudent;
use yii\web\UploadedFile;




if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * BookshelfController that handles the lesson plan/syllabus
 */
class BookshelfController extends BaseController
{
     

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Display the Bookshelf
     */
    public function actionIndex(){

        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('schoolpay_admin') ||( !Helpers::is('non_student') ) ) {
            $searchModel = new BookShelfSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider]);

        } else {
            throw new ForbiddenHttpException('No permissions to view the subjects.');
        }
    }


    //you must override these methods to use BaseControler.
    /**
     * Creates a new Bookshelf model
     * @return Bookshelf
     */
    public function newModel()
    {
        $model = new Bookshelf();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return BookshelfSearch
     */
    public function newSearchModel()
    {
        $searchModel = new BookshelfSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create Bookshelf';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }


     /**
     * Creates a new Bookshelf model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {   

//        if (Yii::$app->user->can('non_student')) {
//            throw new ForbiddenHttpException('No permission to create or upload the Bookshelf / publications. Only students are authorised to do so');
//        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new BookShelf();

         try {
             if ($model->load(Yii::$app->request->post())) {

                 $model->created_by = Yii::$app->user->identity->getId();
                 //if u own the school
                 $schoolId = Yii::$app->user->identity->school_id;

                $model->school_id = $schoolId;

                 if( $model->save(false) ){
                     Logs::logEvent("Created New Bookshelf : " . $model->id, null, null);
                 }

                 $transaction->commit();

                 //Yii::trace($model);
                 return $this->redirect(['view', 'id' => $model->id]);

             }


        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

             \Yii::$app->session->setFlash('viewError', $error);
            Logs::logEvent("Failed create new Bookshelf : ", $error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }


    /**
     * Displays a single Bookshelf  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {  
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') )
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);


        $responseData = [
            'model' => $model
        ];

        return ((Yii::$app->request->isAjax)) ? 
            $this->renderAjax('view',$responseData) 
                :
            $this->render('view', $responseData);


    }


    /**
     * Updates an existing Bookshelf model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        if (!Yii::$app->user->can('rw_library')) {
            throw new ForbiddenHttpException('No permission to Update Library Bookshelf.');
        }

        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post())) {


                if( $model->save(false) ){
                    Logs::logEvent("Updated Library Bookshelf: " . $model->id, null, null);
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);

        }


        //$model->date_created = ($model->date_created)? date('Y-m-d', strtotime($model->date_created)) : '' ;

        return $this->render('update', [
            'model' => $model,
        ]);
    }




    /**
     * Deletes an existing Bookshelf model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bookshelf the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bookshelf::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

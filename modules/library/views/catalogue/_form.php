<?php

use app\models\User;
use app\modules\library\models\Author;
use app\modules\library\models\BookShelf;
use app\modules\library\models\Category;
use app\modules\library\models\Publisher;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class=" row ">

    <div class="col-md-12">
        <?= ($model->isNewRecord) ? '<h3><i class="fa fa-plus"></i> Create Catalogue</h3>' : '<h3><i class="fa fa-edit"></i> Edit Catalogue</h3>' ?>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>

    <div class="col-md-12">
        <?php

        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => 'create']);

        if (!$model->isNewRecord) { //edit mode
            $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => ['update', 'id' => $model->id]]);
        }

        ?>
        <p>All fields marked with * are required</p>

        <div class="col-md-12 ">
            <div class="col-md-6">
                <?= $form->field($model, 'item_type')->dropDownList(['Hard Copy' => 'Hard Copy','Ebook' => 'Ebook', 'Movie' => 'Movie', 'Music' => 'Music'], ['prompt' => 'Select Type *']) ?>
            </div>
        </div>
        <div class="col-md-12">



            <div class="col-md-6">
                <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Title *') ?>

            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'language', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Language *') ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'number_of_copies', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput(['type' => 'number'])->label('Number Of Copies *') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'ISBN', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('ISBN') ?>
            </div>
            <div class="col-md-6">
                <?php
                $url = Url::to(['/library/catalogue/author-list']);
                $author = empty($model->author) ? '' : Author::findOne($model->author);
                $selectedAuthor = $author ? $author->author_name : '';


                echo $form->field($model, 'author')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search By First Name'],
                    'initValueText' => $selectedAuthor, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'style' => 'color:#041f4a',
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Author Name *');
                ?>
            </div>
            <div class="col-md-6">

                <?php
                $url = Url::to(['/library/catalogue/category-list']);
                $category = empty($model->category) ? '' : Category::findOne($model->category);
                $selectedCategory = $category ? $category->category_name : '';


                echo $form->field($model, 'category')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search Category'],
                    'initValueText' => $selectedCategory, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'style' => 'color:#041f4a',
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Select Category *');
                ?>
            </div>


            <div class="col-md-6">
                <?php
                $url = Url::to(['/library/catalogue/shelf-list']);
                $shelf = empty($model->shelf) ? '' : BookShelf::findOne($model->shelf);
                $selectedShelf = $shelf ? $shelf->shelf_name : '';


                echo $form->field($model, 'shelf')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search Shelf'],
                    'initValueText' => $selectedShelf, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'style' => 'color:#041f4a',
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Choose Book Shelf *');
                ?>
            </div>
            <div class="col-md-6">

                <?php
                $url = Url::to(['/library/catalogue/publisher-list']);
                $publisher = empty($model->publisher) ? '' : Publisher::findOne($model->publisher);
                Yii::trace($publisher);
                $selectedPublisher = $publisher ? $publisher->publisher_name : '';


                echo $form->field($model, 'publisher')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search By First Name'],
                    'initValueText' => $selectedPublisher, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'style' => 'color:#041f4a',
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Publisher Name *');
                ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'remarks', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Remarks') ?>
            </div>

            <div class="col-md-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;Attach Ebook / Book Cover/ Music or Movie Cover</h3></div>

            <div class="col-md-6">
                <?= $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'pdf'],
                        'showUpload' => false,
                        'showCancel' => false,
                        'maxFileCount' => 5,
                        'fileActionSettings' => [
                            'showZoom' => false,
                        ]
                    ],
                ])->label(false);
                ?>
            </div>
            <div class="col-md-6">
                <h3>1.To upload multiple files, just select all files and then drag and drop there</h3>
                <h3>2.If you do not want to edit the uploaded files/documents, just leave it blank.. </h3>
            </div>

        </div>
        <div class="col-md-12">
            <div class="card-footer">
                <div class="row">
                    <div class="col-xm-6">
                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                    </div>
                    <div class="col-xm-6">
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

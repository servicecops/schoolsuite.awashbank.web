<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;
use app\modules\dissertation\models\Dissertation;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Catalogue', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="row">

    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('successAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('actionFailed')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('actionFailed'); ?>
            </div>
        </div>
    <?php endif; ?>
</div>

<div class="row ">
    <div class="col-md-12">
        <h3><i class="fa fa-plus"></i> <?= ($model->title) ? $model->title : "--" ?></h3>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pull-right">

            <?php if (\Yii::$app->user->can('rw_library'))  : ?>
                <?= Html::a('<i class="fa fa-edit" >Return</i>', ['catalogue/return-catalogue', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-edit" >Borrow</i>', ['catalogue/borrow-catalogue', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this catalogue?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>




        </div>
    </div>


    <br><br>

    <div class="col-md-12 ">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Title</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->title) ? $model->title : ' --' ?></div>
            </div>
           <hr>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Language</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->language) ? $model->language : ' --' ?></div>
            </div>

            <hr class="l_header">

            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Number of Copies</div>
                <div class="col-md-6"
                     style=" background: #fff;padding:10px"><?= ($model->number_of_copies) ? $model->number_of_copies : "--" ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">ISBN
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->ISBN) ? $model->ISBN : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Author
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->author) ? \app\modules\library\models\Author::findOne([$model->author ])->author_name : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Publisher
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->publisher) ? \app\modules\library\models\Publisher::findOne([$model->publisher ])->publisher_name : ' --' ?></div>
            </div>

            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Category
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->category) ? \app\modules\library\models\Category::findOne([$model->category ])->category_name : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Book Shelf
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->shelf) ? \app\modules\library\models\BookShelf::findOne([$model->shelf ])->shelf_name : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Status
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->status) ? $model->status : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Remarks
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->remarks) ? $model->remarks : ' --' ?></div>
            </div>
        </div>

        <div class="col-md-12 ">
            <?php if ($model->storedFiles): ?>
                <!-- <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;Attach Supporting Documents</h3></div> -->
                <?= $this->render('files_list', ['uploads' => $model->storedFiles, 'model'=>$model]); ?>
            <?php endif; ?>
        </div>

</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>
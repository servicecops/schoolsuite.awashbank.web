<?php

use app\modules\schoolcore\models\CoreStaff;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
?>



<?php $form = ActiveForm::begin([
    'action' => ['borrow-catalogue', 'id'=>$model->id],
    'method' => 'post',
    'id'=>'borrow_form_modal',
    'options'=>['class'=>'formprocess']
]); ?>
<div class="row">


    <div class="col-md-6">
        <?php
        $url = Url::to(['/library/catalogue/std-list']);
        $std= empty($model2->student_code) ? '' : \app\modules\schoolcore\models\CoreStudent::findOne($model->student_code);
        $selectedAuthor = $std ? $std->student_code : '';


        echo $form->field($model2, 'student_code')->widget(Select2::classname(), [
            'options' => ['multiple' => false, 'placeholder' => 'Search By Payment Code', 'class'=>'form-control accounts_controls'],
            'initValueText' => $selectedAuthor, // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'style' => 'color:#041f4a',
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label('Student');
        ?>
    </div>
    <div class="col-md-6">

        <?php
        $url = Url::to(['/planner/planner/staff-list']);
        $staffInfo = empty($model2->staff_id) ? '' : CoreStaff::findOne($model2->staff_id);
        $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';


        echo $form->field($model2, 'staff_id')->widget(Select2::classname(), [
            'options' => ['multiple' => false, 'placeholder' => 'Search By Name','class'=>'form-control accounts_controls'],
            'initValueText' => $selectedStaff, // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'style' => 'color:#041f4a',
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label('Staff ');
        ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model2, 'number_of_copies_borrowed', ['inputOptions'=>[ 'class'=>'form-control accounts_control', 'placeholder'=> 'Number of Copies'] ])->textInput(['type'=>'number'])->label('NUmber of Copies Borrowed *') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model2, 'remarks', ['inputOptions'=>[ 'class'=>'form-control accounts_control', 'placeholder'=> 'Remarks'] ])->textarea()->label('Remarks *') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model2, 'borrow_date')->widget(DatePicker::className(),
            [
                'model' => $model2,
                'attribute' => 'borrow_date',
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1980:' . (date('Y')),
                    'autoSize' => true,
                    'dateFormat' => 'yyyy-MM-dd',
                ],
                'options' => [
                    'class' => 'form-control accounts_control',
                    'placeholder' => 'Date Borrowed'
                ],])->label('Date Borrowed *') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model2, 'return_date')->widget(DatePicker::className(),
            [
                'model' => $model2,
                'attribute' => 'return_date',
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1980:' . (date('Y')),
                    'autoSize' => true,
                    'dateFormat' => 'yyyy-MM-dd',
                ],
                'options' => [
                    'class' => 'form-control accounts_control',
                    'placeholder' => 'Return Date'
                ],])->label('Expected Return Date *') ?>
    </div>


    <div class="col-sm-6 alert alert-danger invalid-accounts-error" style="display: none">
        Please supply all information to continue
    </div>
</div>
    <div class="row">
    <div class="col-sm-6">
        <?= Html::submitButton('Borrow', ['class' => 'btn btn-block btn-info', 'data-confirm'=>'Are you sure you want lend out this book']) ?>
    </div>
    <div class="col-sm-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php
$script = <<< JS
 $('document').ready(function(){
     
        let schoolForm = $('#borrow_form_modal');
        schoolForm.on('beforeSubmit', function(e) {

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.accounts_control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val()) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });
    })
JS;
$this->registerJs($script);
?>


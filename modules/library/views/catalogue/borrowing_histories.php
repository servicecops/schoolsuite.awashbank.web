<?php

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

?>

<div style="color:#F7F7F7">Account Histories</div>

<div class="letter">
    <div class="row hd-title" data-title="School Account History">

        <?php if (Yii::$app->session->hasFlash('successAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
                <div class="notify-content">
                    <?= Yii::$app->session->getFlash('successAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('errorAlert')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
                <div class="notify-content">
                    <?= Yii::$app->session->getFlash('errorAlert'); ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="col-md-12 text-center">
            <?php $form = ActiveForm::begin([
                'action' => ['/library/catalogue/borrowinghistory'],
                'method' => 'get',
                'options' => ['class' => 'formprocess'],
            ]); ?>


            <div class="col-md-2 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></div>
            <div class="col-md-2 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_to',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></div>
            <?php if (Yii::$app->user->can('rw_sch')) {
                $url = Url::to(['core-school/schools']);
                $selectedSchool = empty($searchModel->school_id) ? '' : CoreSchool::findOne(['school_account_id' => $searchModel->account_id])->school_name;
                echo '<div class="col-md-3 col-sm-3 col-xs-3 no-padding">';
                echo $form->field($searchModel, 'account_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'size' => 'sm',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Search School',
                        'id' => 'student_selected_school_id',
                        'class' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false);
                echo '</div>';

            } ?>

            <div class="col-md-2 col-sm-2 col-xs-2 no-padding"><?= $form->field($searchModel, 'ISBN', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'ISBN'])->label(false) ?></div>
            <div class="col-md-1 col-sm-2 col-xs-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
<div class="row mt-5">


    <div class="col">



    </div>
</div>

<div class="row">
    <div class="box-body table table-responsive no-padding">
        <table class="table table-striped">
            <thead>
            <tr>
                <th class='clink'><?= $sort->link('date_created', ['label' => 'Date']) ?></th>
                <th class='clink'>Student Code</th>
                <th class='clink'>Staff Name</th>
                <th class='clink'>Title</th>
                <th class='clink'>ISBN</th>
                <th class='clink'>Returned</th>
                <th class='clink'>Copies Borrowed</th>
                <th class='clink'>Copies returned</th>
                <th class='clink'>Date Borrowed</th>
                <th class='clink'>Date Returned</th>

                <th></th>

            </tr>
            </thead>
            <tbody>
            <?php
            if ($dataProvider) :
                foreach ($dataProvider as $k => $v) : ?>
                    <tr data-key="0">
                        <?php
                        $stf = CoreStaff::findOne(['id' => $v['staff_id']]);
                        $std = CoreStudent::findOne(['id' => $v['student_code']]);

                        ?>
                        <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['student_code']) ? $std->student_code : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['staff_id']) ? $stf->first_name . ' ' . $stf->last_name : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['title']) ? $v['title'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['ISBN']) ? $v['ISBN'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['book_returned']) ? '<span class="not-set">Yes </span>' : '<span class="not-set">No </span>' ?></td>
                        <td><?= ($v['number_of_copies_borrowed']) ? $v['number_of_copies_borrowed'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['number_of_copies_returned']) ? $v['number_of_copies_returned'] : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['borrow_date']) ? date('d/m/y H:i', strtotime($v['borrow_date'])) : '<span class="not-set">(not set) </span>' ?></td>
                        <td><?= ($v['actual_return_date']) ? date('d/m/y H:i', strtotime($v['actual_return_date'])) : '<span class="not-set">(not set) </span>' ?></td>


                        <td>

                            <?php if (Yii::$app->user->can('reverse_payment')) : ?>
                                <a class="modal_link" href="javascript:"
                                   data-href="<?= Url::to(['/schoolcore/school-information/reverse-payment', 'id' => $v['payment_id']]) ?>"
                                   data-title="Reverse Payment - <?= number_format($v['amount'], 2) ?>"><i
                                            class="fa fa-window-close text-danger"></i>&nbsp;</a>
                                <?= Html::a('<i class="fa fa-edit text-danger"></i>&nbsp;', ['/schoolcore/school-information/reverse-payment', 'id' => $v['payment_id']]); ?>

                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach;
            else : ?>
                <td colspan="14">No Record found</td>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?= LinkPager::widget([
        'pagination' => $pages['pages'],
    ]); ?>

</div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

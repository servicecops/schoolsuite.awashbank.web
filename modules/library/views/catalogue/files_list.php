<?PHP

use app\components\Helpers;
use yii\helpers\Url;

?>

<br>
<div class="row">
    <table class="table">
        <thead>
        <tr>
            <th colspan="4"><h3><span class="colprofile-text">Uploads</span></h3></th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($uploads) :
            foreach ($uploads as $k => $v):
                $name = explode('.', $v['file_name']);
                $ext = $name[count($name) - 1];
                //$target = in_array($ext, ['png', 'jpg', 'jpeg', 'gif']) ? '_blank' : '_self';
                ?>
                <tr>
                    <td><?= $v['file_name'] ?></td>
                    <td>
                        <div class="float-right">
                            <a style="font-size:15px;" href="<?= Url::to(['view-file', 'id' => $v['id']]) ?>"
                               target="_blank"><i class="fa fa-eye">View</i></a>
                            <?php if (\Yii::$app->user->can('rw_library'))  : ?>
                                <a class="aclink confirm" style="font-size:15px; padding-left: 10px;"
                                   href="<?= Url::to(['remove-file', 'id' => $v['id']]) ?>" data-method="post"
                                   data-confirm="Confirm to delete file"><i class="fa fa-trash">Delete</i></a>
                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr>
                <td colspan="4">No uploads provided</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

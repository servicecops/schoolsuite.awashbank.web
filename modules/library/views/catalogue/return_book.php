<?php

use app\modules\schoolcore\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'action' => ['return-catalogue', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess', 'id' => 'archive_user_form_modal']
]); ?>
<div class="row">
    <div class="col-md-6">
        <?php
        $url = Url::to(['/library/catalogue/std-list']);
        $std = empty($model2->student_code) ? '' : CoreStudent::findOne($model->student_code);
        $selectedAuthor = $std ? $std->student_code : '';


        echo $form->field($model2, 'student_code')->widget(Select2::classname(), [
            'options' => ['multiple' => false, 'placeholder' => 'Search By First Name'],
            'initValueText' => $selectedAuthor, // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'style' => 'color:#041f4a',
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label('Student');
        ?>
    </div>
    <div class="col-md-6">

        <?php
        $url = Url::to(['/planner/planner/staff-list']);
        $staffInfo = empty($model2->staff_id) ? '' : CoreStaff::findOne($model2->staff_id);
        $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';


        echo $form->field($model2, 'staff_id')->widget(Select2::classname(), [
            'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
            'initValueText' => $selectedStaff, // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'style' => 'color:#041f4a',
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label('Staff');
        ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model2, 'number_of_copies_returned', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Number of Copies']])->textInput(['type' => 'number'])->label('') ?>
    </div>
    <div class="col-sm-6">
        <?= $form->field($model2, 'return_remarks', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Remarks']])->textarea()->label('') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model2, 'actual_return_date')->widget(DatePicker::className(),
            [
                'model' => $model2,
                'attribute' => 'borrow_date',
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1980:' . (date('Y')),
                    'autoSize' => true,
                    'dateFormat' => 'yyyy-MM-dd',
                ],
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'Date Returned'
                ],])->label('Date Returned') ?>
    </div>


</div>
<div class="row">
    <div class="col-sm-6">
        <?= Html::submitButton('Returned', ['class' => 'btn btn-block btn-info', 'data-confirm' => 'Are you sure you want return this book']) ?>
    </div>
    <div class="col-sm-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>

</div>

<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
        $('form#archive_user_form_modal').yiiActiveForm('validate');
          $("form#archive_user_form_modal").on("afterValidate", function (event, messages) {
              // if($(this).find('.has-error').length) {
              //           return false;
              //   } else {
              //       $('.modal').modal('hide');
              //   }
            });
   });
JS;
$this->registerJs($script);
?>

<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Library Book Publisher';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class=" row core-school-index">
    <?php

    $columns = [
        [
            'label' => 'Publisher Name',
            'value' => function ($model) {
                return Html::a($model['publisher_name'], [
                    'view', 'id' => $model['id']
                ]);

            },
            'format' => 'raw',
        ],
        'phone_number',
        'email',
        'country',
        'website',


        ////
    ];
    ?>


    <div class="col-md-12">

        <div class="col-md-12">
            <span style="font-size:20px;color:#2c3844">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
    </div>
    <div class="col-md-12">

        <div class="col-md-2 "></div>
        <div class="col-md-8 "><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
        <div class="col-md-2">

            <div class="float-right">
                <?php
                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $columns,
                    'target' => '_blank',
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_EXCEL => false,

                    ],
                    'dropdownOptions' => [
                        'label' => 'Export Publisher',
                        'class' => 'btn btn-outline-secondary'
                    ]
                ])
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-12">

        <div class="float-right">
            <?php if (Yii::$app->user->can('rw_library')) : ?>

                <?= Html::a('<i class="fa fa-plus"></i> Add Publisher', ['create',], ['class' => 'btn btn-sm btn-primary aclink']) ?>
            <?php endif; ?>

        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'resizableColumns' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>
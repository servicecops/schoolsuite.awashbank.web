<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;

?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>
    <div class="col-md-12">
        <div class="col-md-9">
            <?= $form->field($model, 'category_name', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter Category Name'])->label(false) ?>

        </div>

        <div class="col-md-2">

            <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>


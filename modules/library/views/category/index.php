<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Library Category';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="core-school-index">
<?php

    $columns = [
        [
            'label' => 'Category Name',
            'value'=>function($model){
                return Html::a( $model['category_name'], [
                    'view', 'id' => $model['id']
                ]);
                
            },
            'format'=>'raw',
        ],
        'category_description',



        ////
    ];
    ?>


<div style="margin-top:20px"></div>
    <div class="letter">

        <div class="row" style="margin-top:20px">

            <div class="col"><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        </div>
        <div class="row">

            <div class="col-md-3 col-xs-12 no-padding"></div>
            <div class="col-md-6 col-xs-12 no-padding"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
            <div class="col-md-3 col-xs-12 no-padding">

                <div class="float-right">
                    <?php
                         echo ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                            'target' => '_blank',
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
    
                            ],
                            'dropdownOptions' => [
                                'label' => 'Export Category',
                                'class' => 'btn btn-outline-secondary'
                            ]
                        ])
                    ?>
                </div>
            </div>
        </div>
    </div>
    


    <div class="mt-3">

        <div class="float-right">
            <?php if (Yii::$app->user->can('rw_library')) : ?>

            <?= Html::a('<i class="fa fa-plus"></i> Add Category', ['create', ], ['class' => 'btn btn-sm btn-primary aclink']) ?>
            <?php endif; ?>

        </div>

        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
            'columns' => $columns,
            'resizableColumns'=>true,
            'responsive'=>true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>
</div>

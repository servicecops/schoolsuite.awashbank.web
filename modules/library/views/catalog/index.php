<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\TimetableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Dissertation';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="core-school-index">
<?php

    $columns = [
        [
            'label' => 'Title',
            'value'=>function($model){
                return Html::a( $model['title'], [
                    'view', 'id' => $model['id']
                ]);
                
            },
            'format'=>'raw',
        ],
        [
            'label' => 'Student',
            'value' => function($model){
                $studentInfo = empty($model->student_id) ? '' : CoreStudent::findOne($model->student_id);
                $supervisor = $studentInfo ? $studentInfo->first_name . ' ' . $studentInfo->last_name : '';
                return $supervisor;   
            },
            'format' => 'raw'
        ],
        [
            'label' => 'Department',
            'value' => function($model){
                return $model['department'];
            },
            'format' => 'raw'
        ],
        // [
            
        //     'label' => '',
        //     'format' => 'raw',
        //     'value' => function ($model) {
        //         return Html::a('<i class="fa  fa-eye"></i>', ['view', 'id' => $model['id']]);
        //     },
        // ],
        [
            'label' => 'Supervisor',
            'value' => function($model){
                $staffInfo = empty($model->supervisor) ? '' : CoreStaff::findOne($model->supervisor);
                $supervisor = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';
                return $supervisor;
            },
            'format' => 'raw'
            
        ],
        [
            
            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-edit"></i>', ['update', 'id' => $model['id']]);
            },
        ]
        ////
    ];
    ?>


<div style="margin-top:20px"></div>
    <div class="letter">

        <div class="row" style="margin-top:20px">

            <div class="col"><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        </div>
        <div class="row">

            <div class="col-md-3 col-xs-12 no-padding"></div>
            <div class="col-md-6 col-xs-12 no-padding"><?php //echo $this->render('_search', ['model' => $searchModel]); ?></div>
            <div class="col-md-3 col-xs-12 no-padding">

                <div class="float-right">
                    <?php
                         echo ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                            'target' => '_blank',
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
    
                            ],
                            'dropdownOptions' => [
                                'label' => 'Export Catalogue',
                                'class' => 'btn btn-outline-secondary'
                            ]
                        ])
                    ?>
                </div>
            </div>
        </div>
    </div>
    
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="mt-3">

        <div class="float-right">
            <?php if (!Yii::$app->user->can('non_student')) : ?>

            <?= Html::a('<i class="fa fa-edit"></i> Add Dissertation', ['create', ], ['class' => 'btn btn-sm btn-primary aclink']) ?>
            <?php endif; ?>

        </div>

        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
            'columns' => $columns,
            'resizableColumns'=>true,
            'responsive'=>true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>
</div>

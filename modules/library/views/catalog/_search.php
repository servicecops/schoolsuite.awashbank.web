<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;

?>

<div class="row student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>

    <div class="col-md-3">
        <?= $form->field($model, 'title', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter title'])->label(false) ?>

    </div>
    <div class="col-md-3">

        <?= $form->field($model, 'supervisor', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter the supervisor'])->label(false) ?>
    </div>
    <div class="col-md-2">

        <?= $form->field($model, 'submission_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('datetime-local')->label(false) ?>
    </div>
    <div class="col-md-2">

        <?= $form->field($model, 'submission_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('datetime-local')->label(false) ?>
    </div>
    <div class="col-md-2">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>


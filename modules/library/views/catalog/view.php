<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;
use app\modules\dissertation\models\Dissertation;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$staffInfo = empty($model->supervisor) ? '' : CoreStaff::findOne($model->supervisor);
$supervisor = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

?>


<div class="row ">
    <div class="col-md-12">
        <h3><i class="fa fa-plus"></i> <?= ($model->title) ? $model->title : "--" ?></h3>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pull-right">

            <?php if (!Yii::$app->user->can('non_student') && !$model->approved) : ?>
                <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this catalog?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>




        </div>
    </div>


    <br><br>

    <div class="col-md-12 ">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Status</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->status) ? $model->status : ' --' ?></div>
            </div>
            <hr>
            <hr class="l_header">

            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Uploaded By</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= $model->created_by ? User::findOne($model->created_by)->username : "--" ?></div>
            </div>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Student Name</div>
                <div class="col-md-6"
                     style=" background: #fff;padding:10px"><?= ($model->student_id) ? $model->stdName->first_name .' '. $model->stdName->last_name: ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Department</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->department) ? $model->department : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Degree Program</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->degree_program) ? $model->degree_program : ' --' ?></div>
            </div>
            <hr class="l_header">
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Summary/Abstract
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->summary) ? $model->summary : ' --' ?></div>
            </div>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Supervisor</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->supervisor) ? $supervisor : ' --' ?></div>
            </div>
            <hr class="l_header">
        </div>

        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Submission Date
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->submission_date) ? date('Y-m-d', strtotime($model->submission_date)) : ' --' ?></div>
            </div>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Uploaded By</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= $model->created_by ? User::findOne($model->created_by)->username : "--" ?></div>
            </div>

            <?php if ($model->approved) : ?>

                <div class="col-md-12 ">
                    <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Uploaded By</div>
                    <div class="col-md-6"
                         style="background: #fff; padding:10px"><?= $model->approved ? 'APPROVED' : "REJECTED" ?></div>
                </div>
            <?php endif;?>


            <hr class="l_header">
        </div>

        <br>
    </div>
    <div class="col-md-12 ">
        <?php if ($model->storedFiles): ?>
            <!-- <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;Attach Supporting Documents</h3></div> -->
            <?= $this->render('files_list', ['uploads' => $model->storedFiles, 'model'=>$model]); ?>
        <?php endif; ?>
    </div>
    <div class="col-md-12 ">
    <?php if (Yii::$app->user->can('approve_dissertation') && !$model->approved) : ?>
        <?= $this->render('_approval_buttons', ['actionModel' => $model]); ?>


    <?php endif; ?>
    </div>

</div>
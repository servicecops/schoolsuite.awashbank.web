<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;

?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>

        <div class="col-md-10">
            <?= $form->field($model, 'author_name', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter Name'])->label(false) ?>
        </div>


        <div class="col-md-2">

            <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>


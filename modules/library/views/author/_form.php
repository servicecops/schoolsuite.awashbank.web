<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class=" row ">

    <div class="col-md-12">
        <?= ($model->isNewRecord) ? '<h3><i class="fa fa-plus"></i> Create Author</h3>' : '<h3><i class="fa fa-edit"></i> Edit Author</h3>' ?>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>

    <div class="col-md-12">
        <?php

        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => 'create']);

        if (!$model->isNewRecord) { //edit mode
            $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => ['update', 'id' => $model->id]]);
        }

        ?>



        <div class="col-md-12">

            <p>All fields marked with * are required</p>
            <div class="col-md-6">
                <?= $form->field($model, 'author_name', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Author Name *') ?>
            </div>



            <div class="col-md-6">
                <?= $form->field($model, 'country', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'biography', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
            </div>

        </div>
        <div class="col-md-12">
            <div class="card-footer">
                <div class="row">
                    <div class="col-xm-6">
                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                    </div>
                    <div class="col-xm-6">
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;
use app\modules\dissertation\models\Dissertation;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->shelf_name;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="row ">
    <div class="col-md-12">
        <h3><i class="fa fa-plus"></i> <?= ($model->shelf_name) ? $model->shelf_name : "--" ?></h3>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pull-right">

            <?php if (\Yii::$app->user->can('rw_library'))  : ?>
                <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this bookshelf?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>




        </div>
    </div>


    <br><br>

    <div class="col-md-12 ">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Shelf Name</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->shelf_name) ? $model->shelf_name : ' --' ?></div>
            </div>
           <hr>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Shelf Description</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->shelf_description) ? $model->shelf_description : ' --' ?></div>
            </div>

            <hr class="l_header">

            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Created By</div>
                <div class="col-md-6"
                     style=" background: #fff;padding:10px"><?= ($model->created_by) ? $model->created_by : "--" ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">School Id
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->school_id) ? $model->school_id : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Date Created
                </div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->date_created) ? $model->date_created : ' --' ?></div>
            </div>
        </div>



</div>
<?php
namespace app\modules\library;

class LibraryModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\library\controllers';
    public function init() {
        parent::init();
    }
}
<?php

namespace app\modules\library\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $category_name
 * @property int $student_id
 * @property string|null $category_description

 * @property int|null $created_by
 */
class Catalogue extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'library_catalogue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'language', 'school_id','number_of_copies','shelf','publisher','category','author','item_type'], 'required'],
            [[ 'status','remarks','item_type','ISBN'], 'string'],
            [['publisher','shelf','category' ,'author'], 'integer'],
            [['date_created'], 'safe'],
            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf,jpeg ', 'maxFiles' => 10],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Category Description',
            'language' => 'Category Name',
            'number_of_copies' => 'Number of Copies',
            'created_by' => 'Created By',
            'date_created' => 'Date Created ',
            'ISBN' => 'ISBN ',
            'publisher' => 'Publisher ',
            'category' => 'Category ',
            'shelf' => 'Shelf ',
        ];
    }




    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('library_catalogue_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {

        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
                $upload = new CatalogueUploads();
                $fname = $file->baseName . '.' . $file->extension;

                $tmpfile_contents = file_get_contents($file->tempName);
                Yii::trace($tmpfile_contents);
                $upload->submission_id = $this->id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);

            }
        }
    }


    public function saveAndUpdateUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            //ofcourse first delete old uploads n then upload the new ones
            //simplest way to effect file uploads


            foreach ($this->uploads as $file) {
                $upload = new CatalogueUploads();
                $fname = $file->baseName . '.' . $file->extension;

                $tmpfile_contents = file_get_contents($file->tempName);
                Yii::trace($tmpfile_contents);
                $upload->submission_id = $this->id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);

            }
        }
    }

}

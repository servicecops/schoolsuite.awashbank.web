<?php

namespace app\modules\library\models;

use app\modules\e_learning\models\ElearningSearch;
use app\modules\library\models\Category;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * DissertationSearch represents the model behind the search form of `app\modules\dissertation\models\Dissertation`.
 */
class CategorySearch extends Category
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modelSearch', 'category_description','category_name' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,

        ]);

        $query->andFilterWhere(['ilike', 'category_name', $this->category_name])
            ->andFilterWhere(['ilike', 'category_description', $this->category_description]);
        return $dataProvider;
    }

}
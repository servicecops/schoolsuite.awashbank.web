<?php

namespace app\modules\library\models;

use app\components\Helpers;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\data\Sort;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $category_name
 * @property int $student_id
 * @property string|null $category_description

 * @property int|null $created_by
 */
class BorrowBook extends \yii\db\ActiveRecord
{
    public $date_from, $date_to, $ISBN;
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'library_borrower_book_association';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_of_copies_borrowed','issued_by' ,'borrow_date','return_date','catalogue_id'], 'safe'],
            [[ 'created_by','student_code','school_id','staff_id','catalogue_id','number_of_copies_returned' ], 'integer'],
            [['actual_return_date','book_returned','remarks','return_remarks','date_from','ISBN','date_to'], 'safe'],
//            [
//                'student_code', 'required',
//                'message' => 'Either Staff Name or Student Code is required.',
//                'when' => function($model) { return empty($model->staff_id); }
//            ],
//            [
//                'staff_id', 'required',
//                'message' => 'Either Staff Name or Student Code is required.',
//                'when' => function($model) { return empty($model->student_code); }
//            ],

        ];
    }


    public function either($attribute_name, $params)
    {
        $field1 = $this->getAttributeLabel($attribute_name);
        $field2 = $this->getAttributeLabel($params['other']);
        if (empty($this->$attribute_name) && empty($this->{$params['other']})) {
            $this->addError($attribute_name, Yii::t('user', "either {$field1} or {$field2} is required."));
        }
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_description' => 'Category Description',
            'category_name' => 'Category Name',
            'school_id' => 'School ID',
            'created_by' => 'Created By',
            'date_created' => 'Date Created ',
        ];
    }




    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }




    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchPBorrowingHistory($schId, $queryParams)
    {
        $this->load($queryParams);
        $today = date('Y-m-d', time()+86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if(Yii::$app->user->can('schoolpay_admin') && empty($params)){
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        $query = new Query();
        $query->select(['lb.id', 'lb.date_created', 'lb.book_returned',  'lb.return_date', 'lb.actual_return_date','lc.title','lc.ISBN',
            'lb.number_of_copies_borrowed', 'lb.number_of_copies_returned', 'lb.return_remarks', 'lb.remarks','student_code','staff_id','lb.borrow_date'])


            ->from('library_borrower_book_association lb')
//            ->innerJoin('core_student std', 'std.id=lb.student_code')
//            ->innerJoin('core_staff sinfo', 'sinfo.id=lb.staff_id')
            ->innerJoin('library_catalogue lc', 'lc.id=lb.catalogue_id')
            ->innerJoin('core_school cs', 'cs.id=lb.school_id');
        if(\app\components\ToWords::isSchoolUser()){
            $query = $query->where(['lb.school_id'=>$schId]);
        }
        if(Yii::$app->user->can('schoolpay_admin')){
            $query = $query->andFilterWhere(['lb.school_id'=>$schId]);
        }


        if(( !Helpers::is('non_student') )  ){
            $query = $query->andFilterWhere(['lb.student_code'=>Yii::$app->user->identity->username]);
        }

        if(Yii::$app->user->can('sch_teacher')  ){
            $query = $query->andFilterWhere(['lb.staff_id'=>Yii::$app->user->identity->id]);
        }

        if($this->ISBN){
            $query->andWhere(['lc.ISBN' => $this->ISBN]);
            if($date_to) $query->andWhere("lc.date_created::date<='$date_to'");
        }

        if($this->date_from){
            $query->andWhere("lb.date_created::date>='$this->date_from'");
            if($date_to) $query->andWhere("lb.date_created::date<='$date_to'");
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'book_returned', 'number_of_copies_returned', 'staff_id', 'student_code','borrow_date', 'number_of_copies_returned', 'borrow_date', 'return_date','title','ISBN'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('lb.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        /*unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;*/
        $this->setSession('Payments history', $query, $this->getCols(),  'Library_histories');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page]; //Use slave db connection in reporting mode
    }

    public function getCols()
    {
        return [
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
            'description' => [ 'label'=>'Description'],
            'payment_code' => ['label'=>'Payment Code'],
            'registration_number' => [ 'label'=>'Registration Number'],
            'student_name' => ['label'=>'Student Name'],
            'trans_type' => [ 'label'=>'Trans Type'],
            'channel_code' => [ 'label'=>'Channel code'],
            'channel_memo' => [ 'label'=>'Channel memo'],
            'amount' => [ 'type'=>'number', 'label'=>'Amount'],
        ];}

    protected function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
        $_SESSION['findData']['exportFile'] = '@app/views/common/exportPdfExcel';
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>$_SESSION['findData']['exportFile'],
        ];
        return $data;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchBorrowingTracker($schId, $queryParams)
    {
        $this->load($queryParams);
        $today = date('Y-m-d', time()+86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if(Yii::$app->user->can('schoolpay_admin') && empty($params)){
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        $query = new Query();
        $query->select(['lb.id', 'lb.date_created', 'lb.number_of_copies_borrowed', 'lb.number_of_copies_returned', 'lb.total_remaining',
            'lb.total_copies','lb.total_remaining',
            'lc.title','lc.ISBN','cs.school_name'])


            ->from('library_book_borrower_tracker lb')
            ->innerJoin('library_catalogue lc', 'lc.id=lb.catalogue_id')
            ->innerJoin('core_school cs', 'cs.id=lb.school_id');
        if(\app\components\ToWords::isSchoolUser()){
            $query = $query->where(['lb.school_id'=>$schId]);
        }

        if(Yii::$app->user->can('schoolpay_admin')){
            $query = $query->andFilterWhere(['lb.school_id'=>$schId]);
        }

        if($this->ISBN){
            $query->andWhere(['lc.ISBN' => $this->ISBN]);
            if($date_to) $query->andWhere("lc.date_created::date<='$date_to'");
        }

        if($this->date_from){
            $query->andWhere("lb.date_created::date>='$this->date_from'");
            if($date_to) $query->andWhere("lb.date_created::date<='$date_to'");
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                 'date_created', 'number_of_copies_borrowed', 'number_of_copies_returned', 'total_remaining', 'total_copies',
                'title','ISBN'   ,'school_name'         ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('lb.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        /*unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;*/
        $this->setSession('Payments history', $query, $this->getCols(),  'Library_histories');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page]; //Use slave db connection in reporting mode
    }
}

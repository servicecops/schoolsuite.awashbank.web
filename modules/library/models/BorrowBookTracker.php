<?php

namespace app\modules\library\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $category_name
 * @property int $student_id
 * @property string|null $category_description


 */
class BorrowBookTracker extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'library_book_borrower_tracker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_of_copies_borrowed','number_of_copies_returned' ,'total_remaining','return_date','book_id'], 'safe'],
            [[ 'school_id','total_copies'], 'integer'],
            [['actual_return_date','book_returned','catalogue_id',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_description' => 'Category Description',
            'category_name' => 'Category Name',
            'school_id' => 'School ID',
            'date_created' => 'Date Created ',
        ];
    }




    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['student_code' => 'student_code']);
    }

}

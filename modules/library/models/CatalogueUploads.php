<?php

namespace app\modules\library\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "dissertation_uploads".
 *
 * @property string $id
 * @property string $date_created
 * @property string $submission_id
 * @property string $file_name
 * @property string $file_data
 */
class CatalogueUploads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'library_catalogue_uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['submission_id', 'file_name', 'file_data'], 'required'],
            [['submission_id'], 'integer'],
            [['file_data'], 'string'],
            [['file_name','file_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'submission' => 'Submission',
            'file_name' => 'File Name',
            'file_data' => 'File Data',
        ];
    }
    
}

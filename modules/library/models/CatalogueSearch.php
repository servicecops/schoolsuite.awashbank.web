<?php

namespace app\modules\library\models;

use app\modules\e_learning\models\ElearningSearch;
use app\modules\library\models\Catalogue;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * DissertationSearch represents the model behind the search form of `app\modules\dissertation\models\Dissertation`.
 */
class CatalogueSearch extends Catalogue
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modelSearch', 'title','ISBN' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = Catalogue::find();
        $query = (new Query())
            ->select(['lb.id','lb.title', 'lb.language', 'lb.school_id','lb.number_of_copies',
                'lb.status','lb.remarks','lb.item_type','lb.ISBN',
                'lp.publisher_name','bs.shelf_name','lc.category_name' ,'au.author_name',
                'uz.username as created_by','cs.school_name'])
            ->from('library_catalogue lb')
            ->innerJoin('"user" uz','lb.created_by =uz.id' )
            ->innerJoin('library_book_author au','lb.author =au.id' )
            ->innerJoin('library_book_shelf bs','lb.shelf =bs.id' )
            ->innerJoin('lib_book_category lc','lb.category =lc.id' )
            ->innerJoin('library_book_publisher lp','lb.publisher =lp.id' )
            ->innerJoin('core_school cs','lb.school_id =cs.id' );

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (\app\components\ToWords::isSchoolUser()) {

            $query->andWhere(['cs.id' => Yii::$app->user->identity->school_id]);

        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,

        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
            ->andFilterWhere(['ilike', 'ISBN', $this->ISBN]);
        return $dataProvider;
    }

}

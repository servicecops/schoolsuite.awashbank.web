<?php

namespace app\modules\library\models;

use app\modules\e_learning\models\ElearningSearch;
use app\modules\library\models\Category;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * DissertationSearch represents the model behind the search form of `app\modules\dissertation\models\Dissertation`.
 */
class BookShelfSearch extends BookShelf
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modelSearch', 'shelf_name','category_name' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = BookShelf::find();
        $query = (new Query())
            ->select(['csc.id','csc.shelf_name', 'uz.username as created_by','csc.shelf_description','cs.school_name'])
            ->from('library_book_shelf csc')
            ->innerJoin('"user" uz','csc.created_by =uz.id' )
            ->innerJoin('core_school cs','csc.school_id =cs.id' );

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (\app\components\ToWords::isSchoolUser()) {

                $query->andWhere(['cs.id' => Yii::$app->user->identity->school_id]);

        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,

        ]);

        $query->andFilterWhere(['ilike', 'shelf_name', $this->shelf_name])
            ->andFilterWhere(['ilike', 'shelf_description', $this->shelf_description]);
        return $dataProvider;
    }

}

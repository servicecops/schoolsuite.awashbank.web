<?php

namespace app\modules\library\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $publisher_name
 * @property int $student_id
 * @property string|null $email

 * @property int|null $created_by
 */
class Publisher extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'library_book_publisher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'publisher_name', ], 'required'],
            [['email','phone_number','country','website'], 'string'],
            [['date_created','created_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publisher_name' => 'Publisher Name',
            'phone_number' => 'Phone Number',
            'email' => 'Publisher Email',
            'country' => 'Country',
            'website' => 'Website',
            'created_by' => 'Created By',
            'date_created' => 'Date Created ',
        ];
    }




    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

}

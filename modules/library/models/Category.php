<?php

namespace app\modules\library\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $category_name
 * @property int $student_id
 * @property string|null $category_description

 * @property int|null $created_by
 */
class Category extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lib_book_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_description', 'category_name', 'school_id'], 'required'],
            [['category_description', 'category_name',], 'string'],
            [['student_id', 'school_id', 'created_by', ], 'integer'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_description' => 'Category Description',
            'category_name' => 'Category Name',
            'school_id' => 'School ID',
            'created_by' => 'Created By',
            'date_created' => 'Date Created ',
        ];
    }




    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Create Core School';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">
    <div class="model_titles">
        <div class="col-sm-12 "><h3><i class="fa fa-check-square-o"></i>&nbsp;Add School Information</h3></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,   'banks'=>$banks,
        'bank_info_error'=>$bank_info_error, 'accounts'=>$accounts,
        'model_error' => $model_error
    ]) ?>

</div>

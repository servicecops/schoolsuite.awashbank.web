<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Informations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">

    <h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title) ?></h1>
    <br>


    <?php


    $columns = [
//        [
//            'class' => 'kartik\grid\ExpandRowColumn',
//            'width' => '50px',
//            'value' => function ($model, $key, $index, $column) {
//                return GridView::ROW_COLLAPSED;
//            },
//            // uncomment below and comment detail if you need to render via ajax
//            // 'detailUrl' => Url::to(['/site/book-details']),
//            'detail' => function ($model, $key, $index, $column) {
//                return Yii::$app->controller->renderPartial('_expand_row', ['model' => $model]);
//            },
//            'headerOptions' => ['class' => 'kartik-sheet-style'] ,
//            'expandOneOnly' => true
//        ],
        'school_code',
        [
            'label' => 'school name',
            'value'=>function($model){
                return Html::a($model['school_name'], ['core-school/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'district_name',

        'phone_contact_1',
        'school_type',
        [
            'label' => 'Assign channel',
            'value'=>function($model){
                return Html::a('<i class="fa  fa-exchange-alt"></i>', ['channels', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],




        [
            'label' => '',
            'value'=>function($model){
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-school/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fas fa-edit"></i>', ['core-school/update', 'id' => $model['id']]);
            },
        ],


    ];
    ?>


    <div class="row"> <?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
<div class="mt-3">
    <div class="float-right">
        <?php if (Yii::$app->user->can('rw_sch')) : ?>
            <?= Html::a('Add new school', ['create'], ['class' => 'btn bg-gradient-primary text-white float-right']) ?>
        <?php endif; ?>
    </div>
    <div class="float-right">

        <?php


        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'target' => '_blank',
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_EXCEL => false,

            ],
            'dropdownOptions' => [
                'label' => 'Export schools',
                'class' => 'btn btn-outline-secondary'
            ]
        ])
        ?>
    </div>
    <?= GridView::widget([
        'dataProvider'=> $dataProvider,
//    'filterModel' => $searchModel,
        'columns' => $columns,
        'resizableColumns'=>true,
//    'floatHeader'=>true,
        'responsive'=>true,
        'responsiveWrap' => false,
        'bordered' => false,
        'striped' => true,
    ]); ?>

</div>
</div>

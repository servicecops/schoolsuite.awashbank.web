<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-information-view hd-title" data-title="<?= $model->school_name ?>">

 <div class="content-header">
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">    
        <i class="fa fa-home"></i> <?= Html::encode($this->title) ?>

    </h2>
  </div><!-- /.col -->
</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm">

                <b>Available Channels</b>:
                <div class="form-group has-feedback">
                    <input name="search_av" type="text" class="form-control channel-search" placeholder="Search..." data-target="available"/>
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
                <?php
                echo Html::listBox('selected', '', $available, [
                    'id' => 'available',
                    'multiple' => true,
                    'size' => 20,
                    'class'=>'form-control',
                    'style' => 'width:100%;height:350px;padding:5px']);
                ?>

        </div>
        <div class="col-sm text-center" style="padding-top:100px;" >
                <br><br>
                <?php
                echo Html::a('>>', '#', ['class' => 'btn btn-block btn-primary', 'title' => 'Assign', 'data-action' => 'assign']) . '<br><br>';
                echo Html::a('<<', '#', ['class' => 'btn btn-block btn-danger', 'title' => 'Delete', 'data-action' => 'delete']) . '<br>';
                ?>
                <br><br>

        </div>
        <div class="col-sm">
            <b>Assigned</b>:
            <div class="form-group has-feedback">
                <input name="search_asgn" type="text" class="form-control channel-search" placeholder="Search..." data-target="assigned"/>
                <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
            <?php
            echo Html::listBox('selected', '', $assigned, [
                'id' => 'assigned',
                'multiple' => true,
                'size' => 20,
                'class'=>'form-control',
                'style' => 'width:100%;height:350px;padding:5px']);
            ?>
        </div>
    </div>
    <?php $this->render('_script',['id'=>$model->id]); ?>
</div>

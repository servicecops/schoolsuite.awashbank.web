<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\modules\schoolcore\models\CoreSchool */

?>
<div class="core-school-update">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'school_name',
            'location',
            'school_code',
            'contact_person',
            'account_number'

        ],
    ]) ?>


</div>

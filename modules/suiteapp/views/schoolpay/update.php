<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Update School: ' . $model->school_name;

?>
<div class="core-school-update">

    <div class="model_titles">
        <div class="col-sm-12 "><h3><i class="fa fa-check-square-o"></i>&nbsp;Update School Information</h3></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model, 'banks'=>$banks, 'accounts'=>$accounts,
        'bank_info_error'=>$bank_info_error, 'sections'=>$sections,
        'section_info_error'=>$section_info_error, 'school_banks'=>$school_banks, 'model_error'=>$model_error
    ]) ?>

</div>

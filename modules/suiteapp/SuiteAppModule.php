<?php
namespace app\modules\suiteapp;

class SuiteAppModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\suiteapp\controllers';
    public function init() {
        parent::init();
    }
}

<?php


namespace app\modules\suiteapp\controllers;


use app\modules\banks\models\BankAccountDetails;
use app\modules\banks\models\SelfEnrolledBankAccountDetails;
use app\modules\logs\models\Logs;

use app\modules\online_reg\models\CoreSchoolSelfRegistration;
use app\modules\payment_plan\models\PaymentPlan;
use app\modules\paymentscore\models\ExternalPaymentSources;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\PaymentsReceivedSearch;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

//use app\models\FeesDue;
//use app\models\FeeClass;
//use app\models\Classes;
//use app\models\FeesDueSearch;
//use app\modules\logs\models\Logs;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class SchoolpayController extends Controller
{


    public function beforeAction($action)
    {


//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }

//something code right here if user valid

        return true;

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

        ];
    }


    /**
     * Will receive a json from an external schoolpay
     * It expects an array with {schoolinformation: {}, classes: {},students:{}}
     * @return array|string
     *
     */


    public function actionSyncSchpay()
    {

        $raw_data = Yii::$app->request->getRawBody();
        Yii::trace($raw_data);

        $incomingRequest = json_decode($raw_data, true);
        //Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        $logs= Logs::logEvent("Incoming: ".$raw_data , null, null);
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();


        try {


            Yii::trace($logs);

            if(!$incomingRequest){
                $status = "UNSUCCESSFULL, No request received ";
                return json_encode([
                    'returncode' => 909,
                    'returnmessage' => $status,
                ]);
            }

            $schoolInfo = $incomingRequest['schoolInformation']; //The $schoolInfo, ideally will correspond to a School details

            $classesInfo = $incomingRequest['classes']; //The $classesInfo, ideally will correspond to a Class details

            $studentInfo = $incomingRequest['students']; //The $studentInfo, ideally will correspond to a Student details

            $accountsInfo = $incomingRequest['accounts']; //The $accountsInfo, ideally will correspond to a School bank details


            //Check if Schcode exists


            Yii::trace($incomingRequest);

            $incomingSch = $schoolInfo['school_code'];

            Yii::trace($incomingSch);


            if ($incomingSch) {
                $thisSch = $connection->createCommand("SELECT id, school_name, schoolpay_schoolcode FROM core_school WHERE schoolpay_schoolcode=" . $incomingSch)->queryOne();

                if ($thisSch) {
                    $transaction->rollBack();
                    $status = "UNSUCCESSFULL, School " . $thisSch['school_name'] . " with SchoolPay SchoolCode " . " " . $thisSch['schoolpay_schoolcode'] . " Already exists";
                    return json_encode([
                        'returncode' => 909,
                        'returnmessage' => $status,
                    ]);

                }
            }
            //Add school Information
            $sch = new CoreSchool();
            Yii::trace($schoolInfo['school_name']);

            $sch->schoolpay_schoolcode = $schoolInfo['school_code'];
            $sch->school_name = $schoolInfo['school_name'];
            $sch->village = $schoolInfo['physical_address'];
            $sch->contact_person = $schoolInfo['contact_person'];
            $sch->contact_email_1 = $schoolInfo['contact_email'];
            $sch->phone_contact_1 = $schoolInfo['contact_phone1'];
            $sch->phone_contact_2 = $schoolInfo['contact_phone2'];
            $sch->school_type = $schoolInfo['school_type'];
            $sch->district = 113;
            $sch->region = 1;
            $sch->active = $schoolInfo['active'];
            $sch->bank_account_number = $schoolInfo['bank_account_number'];
            $sch->default_part_payment_behaviour = $schoolInfo['default_part_payment_behaviour'];
            // $sch->school_logo = $schoolInfo['school_logo'];
            $sch->bank_name = $schoolInfo['bank_name'];
            $sch->is_sync= true;
            $sch->enable_daily_stats = $schoolInfo['enable_daily_stats'];
            $sch->school_registration_number_format = $schoolInfo['school_registration_number_format'];
            $sch->sample_school_registration_number = $schoolInfo['sample_school_registration_number'];
            $sch->save(false);


            //Add bankdetails
            if ($posted_banks = $accountsInfo) {
                $this->saveAccounts($posted_banks, $sch->id);
            }

            $connection->createCommand("select associate_school_with_default_payment_channels(:school_id)", [
                ':school_id' => $sch->id
            ])->execute();


            //Add classes Info
            $class = [];


            foreach ($classesInfo as $k => $v) {
                $cls = new CoreSchoolClass();

                $cls->class_code = $v['class_code'];
                $cls->class_description = $v['class_description'];
                $cls->school_id = $sch->id;
                $cls->save();
                Yii::trace($v['class_code']);
                Yii::trace($cls->id);

                // $class += array($v['id'] => $cls->id);
                $classList = ['schpay_class_id' => $v['id'], 'suite_class_id' => $cls->id];
                array_push($class, $classList);

                ///$class =  ArrayHelper::map($class, $v['class_code'], $cls->id);
                Yii::trace($class);

            }


            //Save stds Info

            $returnedStudentarray = array();

            foreach ($studentInfo as $ke => $vl) {
                $std = new CoreStudent();


                foreach ($class as $k => $v) {
                    //Mapping new classId  with Schpay class_id
                    if ($vl['student_class'] === $v['schpay_class_id']) {
                        $std->class_id = $v['suite_class_id'];
                    }

                    //mapping new archived class_id to schpay archived class_id
                    if ($vl['class_when_archived'] === $v['schpay_class_id']) {
                        $std->class_when_archived = $v['suite_class_id'];
                    }


                }
                Yii::trace($std->class_id);
                //   Yii::trace( $std->class_when_archived );
                $password = Yii::$app->security->generatePasswordHash("abc123");

                $std->schoolpay_paymentcode = $vl['payment_code'];
                $std->first_name = $vl['first_name'];
                $std->middle_name = $vl['middle_name'];
                $std->last_name = $vl['last_name'];
                $std->school_student_registration_number = $vl['school_student_registration_number'];
                $std->gender = $vl['gender'];
                $std->date_of_birth = $vl['date_of_birth'];
                $std->active = $vl ['active'];
                $std->student_email = $vl['student_email'];
                $std->student_phone = $vl['student_phone'];
                $std->guardian_name = $vl['gurdian_name'];
                $std->guardian_relation = $vl['guardian_relation'];
                $std->guardian_email = $vl ['gurdian_email'];
                $std->guardian_phone = $vl['gurdian_phone'];
                $std->allow_part_payments = $vl['allow_part_payments'];
                $std->disability = $vl ['disability'];
                $std->disability_nature = $vl ['disability_nature'];
                $std->nationality = $vl ['nationality'];
                $std->school_id = $sch->id;
                $std->day_boarding = $vl ['day_boarding'];
                $std->archived = $vl ['archived'];
                $std->date_archived = $vl ['archive_date'];
                $std->archive_reason = $vl ['archive_reason'];
                if ($std->save(false)) {
                    //Nice its inserted/updated, go ahead
                    Yii::trace($std);
                    Yii::trace($std->student_code);

                    $stdData = CoreStudent::findOne(['id'=>$std->id]);

                    $studentsArray = ['schpay_student_id' => $vl['id'], 'suite_student_id' => $std->id, 'suite_student_code' => $stdData->student_code];

                    array_push($returnedStudentarray, $studentsArray);
                    Yii::trace($returnedStudentarray);

                } else {
                    throw new \yii\db\Exception("Failed to insert student dara");
                }


            }
            $schData =CoreSchool::findOne(['id'=>$sch->id]);

            $data = json_encode(['school_id' => $sch->id, 'school_code' => $schData->school_code,"school_name"=>$schData->school_name]);

            Yii::trace($data);
           // $logs =Logs::logEvent("Successfully posted student payment: ".$data , null, null);
            $logs =Logs::logEvent("Successfully suited up from schoolpay:  ".$data, null,null);

            Yii::trace($logs);
            $transaction->commit();

            return json_encode([
                'returncode' => 0,
                'returnmessage' => "SUCCESSFUL, All data has been imported",
                'School' => $data,
                'student' => $returnedStudentarray
            ]);


        } catch
        (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            $logz= Logs::logEvent("Error sync schoolpay info: " , $error, null);
            Yii::trace($logz);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }


    }

    /**
     * Will receive a student infomation inquiry from an external payment source e.g schoolpay
     * It expects an array with {auth: {}, paymentCode: {}}
     * auth{"externalSource": 'SCHOOLPAY', externalSourcePassword: "xxxxx", "sourceChannel": 'MTN'} -- Example of source
     * @return array|string
     *
     */
    public function actionQueryStudent()
    {

        try {

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);

            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            //Authenticate external source here
            Yii::trace($auth);

            //Uncomment the block below after you have created the ExternalSourceModel then adapt the logic below to your model name

            $dbExternalSource = ExternalPaymentSources::findOne(['code' => $auth['externalSource']]);
            if (!$dbExternalSource) {
                //External source not found in our db
                throw new ForbiddenHttpException('Source ' . $auth['externalSource'] . ' not found');
            }

            //Authenticate password
            if ($dbExternalSource->password != $auth['externalSourcePassword']) {
                //External source not found in our db
                throw new ForbiddenHttpException('Invalid password for ' . $auth['externalSource']);
            }


            //Now get the payment channel
//            $paymentChannel = PaymentChannels::findOne(['channel_code' => $auth['sourceChannel']]);
//            if (!$paymentChannel)
//                throw new ForbiddenHttpException('Payment channel ' . $auth['sourceChannel'] . ' not found');

            //the actual student code

            $paymentCode = $incomingRequest['student']['studentPaymentCode'];

            $searchModel = new PaymentsReceivedSearch();


           if (preg_match("(^88(\d{8}))", $paymentCode)) {
                $data = $searchModel->searchOnlineStudentApplicant($paymentCode);
            }else{
               $amount= null;
               $data = $searchModel->searchStudent($paymentCode);

               //Yii::trace($data);
               $today =date('Y-m-d');

//               if ($incomingRequest['amount']) {
//                   $amount =$incomingRequest['amount'];
//
//                   //Yii::trace($amount);
//
//               }
//               if( $amount ){
//                   //check if school has payment plan module
//                   $paymentPlan = PaymentPlan::findOne(['school_id'=>$data[0]['school_id']]);
//
//                   //Yii::trace($paymentPlan);
//                   if($paymentPlan['fixed_amount_per_payment']){
//                       $result = \Yii::$app->db->createCommand('SELECT "id", "schedule_date", "total_fee_amount", "total_schedule_amount", "schedule_balance", "description", "schedule_paid_so_far" FROM "payment_plan_student_payment_schedule" WHERE "student_id"=:std and schedule_paid_so_far < 1 ORDER BY "id" LIMIT 20;')
//                           ->bindValue(':std', $data[0]['id'])
//                           ->queryOne();
//
//
//                       Yii::trace($result);
//                       //get amount and compare with incoming amount
//
//                       if ($result){
//                            if ( $result['schedule_paid_so_far']<1 && $amount < $result['total_schedule_amount'])
//                                throw new ForbiddenHttpException('Student Code ' . $paymentCode . ' has a payment schedule and is expected to pay UGX '. $result['total_schedule_amount']);
//
//                       }
//                   }
//
//
//
//
//               }
            }

            $result = Json::encode($data);
            Yii::trace($result);
            Yii::trace($data);


            if (!$data)
                throw new ForbiddenHttpException('Student Code ' . $paymentCode . ' not found');
//            Yii::trace($data);
//            Logs::logEvent("Successfully posted student payment: " .$data,null, null);

            return json_encode([
                'returncode' => 0,
                'returnmessage' => "Student details found",
                'student' => $data[0]
            ]);

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Error on posting querying student details: " , $error, null);

            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }

    /**
     * Will receive a payment from an external payment source e.g schoolpay
     * It expects an array with {auth: {}, payment: {}}
     * auth{"externalSource": 'SCHOOLPAY', externalSourcePassword: "xxxxx", "sourceChannel": 'MTN'} -- Example of source
     * @return array|string
     *
     */
    public function actionPostPayment()
    {
        try {
            $model = new PaymentsReceived();
            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);

            $auth = $incomingRequest['auth']; //The auth, ideally will correspond to a payment channel
            //Authenticate external source here
            Yii::trace($auth);

            //Uncomment the block below after you have created the ExternalSourceModel then adapt the logic below to your model name


            $dbExternalSource = ExternalPaymentSources::findOne(['code' => $auth['externalSource']]);
            if (!$dbExternalSource) {
                //External source not found in our db
                throw new ForbiddenHttpException('Source ' . $auth['externalSource'] . ' not found');
            }

            //Authenticate password
            if ($dbExternalSource->password != $auth['externalSourcePassword']) {
                //External source not found in our db
                throw new ForbiddenHttpException('Invalid password for ' . $auth['externalSource']);
            }


            //Now get the payment channel
            $paymentChannel = PaymentChannels::findOne(['channel_code' => $auth['sourceChannel']]);
            if (!$paymentChannel)
                throw new ForbiddenHttpException('Payment channel ' . $auth['sourceChannel'] . ' not found');


            $payment = $incomingRequest['payment']; //The actual payment object
            //Now create payment json
            $payment_json = json_encode($payment);

            //call sp to post payment
            $result = \Yii::$app->db->createCommand("Select * from process_student_transaction(:associd,:jsonText) AS result")
                ->bindValue(':associd', $paymentChannel->id)
                ->bindValue(':jsonText', $payment_json)
                ->queryAll();
            $result=json_decode($result[0]['result']);
            Yii::trace($result);
            Yii::trace($result->returncode);
         //   $result = json_decode($result['create_student_payment_plan_schedule']);
            if ($result->returncode == 0) {
                $logs= Logs::logEvent("Posted student payment details: ".json_encode($result) ."", null, null);

                return json_encode([
                    'returncode' => 0,
                    'returnmessage' => "Student details found",
                    'student' => $result
                ]);
            } else {
                throw new ForbiddenHttpException("Posting Failed ".$result->returnmessage);
            }


        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
             Logs::logEvent("Error on posting student payment: " , $error, null);

            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }


    }


    private function saveAccounts($posted_bank, $sch_id)
    {

        Yii::trace($posted_bank);
        foreach ($posted_bank as $k => $v) {

            $bankAcc = new CoreBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }


    private function saveASelfccounts($posted_bank, $sch_id)
    {

        Yii::trace($posted_bank);
        foreach ($posted_bank as $k => $v) {

            $bankAcc = new SelfEnrolledBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }


    public function actionSchoolSelfRegistration()
    {


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new CoreSchoolSelfRegistration();
        $model->default_part_payment_behaviour = false;
        $model->enrolled = false;
        // $sp_modules = (new \yii\db\Query())->from('schoolpay_modules')->orderBy('module_name')->all();

        $banks = $this->getBanks();
        $bank_info_error = [];
        $model_error = '';

        // Yii::trace($banks);


        try {

            $res = ['model' => $model,  'banks' => $banks, 'bank_info_error' => $bank_info_error, 'accounts' => $this->getSchoolAccounts($model->id), 'model_error' => $model_error,];

            if ($model->load(Yii::$app->request->post()) ) {
                $data = Yii::$app->request->post();

                //TODO: Begin transaction here
                $bank_info_error = $this->validateBankInfo($_POST['bank_name']);

                //  Yii::trace($bank_info_error);
                if ($bank_info_error)
                    throw new Exception("All bank information fields should be filled");

                //Check school with same name and contact phone exists
                $foundSchool = CoreSchoolSelfRegistration::findOne(['school_name' => strtoupper(trim($model->school_name)),
                    'phone_contact_1' => $model->phone_contact_1]);
                if ($foundSchool) {


                    \Yii::$app->session->setFlash('school', "<div class='alert alert-danger'>Duplicate submission, School already submitted<br> </div>");
                    Yii::trace($res);


                    return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
                }


                if ($data['CoreSchoolSelfRegistration']['school_name']) {
                    $model->school_name = strtoupper(trim($data['CoreSchoolSelfRegistration']['school_name']));
                }
                if ($data['CoreSchoolSelfRegistration']['contact_person']) {
                    $model->contact_person = $data['CoreSchoolSelfRegistration']['contact_person'];
                }
                if ($data['CoreSchoolSelfRegistration']['contact_email_1']) {
                    $model->contact_email_1 = $data['CoreSchoolSelfRegistration']['contact_email_1'];

                }
                if ($data['CoreSchoolSelfRegistration']['phone_contact_1']) {
                    $model->phone_contact_1 = $data['CoreSchoolSelfRegistration']['phone_contact_1'];

                }
                if ($data['CoreSchoolSelfRegistration']['phone_contact_2']) {
                    $model->phone_contact_2 = $data['CoreSchoolSelfRegistration']['phone_contact_2'];

                }
                if ($data['CoreSchoolSelfRegistration']['school_type']) {
                    $model->school_type = $data['CoreSchoolSelfRegistration']['school_type'];

                }
                if ($data['CoreSchoolSelfRegistration']['district']) {
                    $model->district = $data['CoreSchoolSelfRegistration']['district'];

                }
                if ($data['CoreSchoolSelfRegistration']['sub_county']) {
                    $model->sub_county = $data['CoreSchoolSelfRegistration']['sub_county'];

                }
                if ($data['CoreSchoolSelfRegistration']['county']) {
                    $model->county = $data['CoreSchoolSelfRegistration']['county'];

                }
                if ($data['CoreSchoolSelfRegistration']['region']) {
                    $model->region = $data['CoreSchoolSelfRegistration']['region'];

                }

                if ($data['CoreSchoolSelfRegistration']['parish']) {
                    $model->parish = $data['CoreSchoolSelfRegistration']['parish'];

                }

                if ($data['CoreSchoolSelfRegistration']['village']) {
                    $model->village = $data['CoreSchoolSelfRegistration']['village'];

                }
                if ($data['CoreSchoolSelfRegistration']['sample_school_registration_number']) {
                    $model->sample_school_registration_number = $data['CoreSchoolSelfRegistration']['sample_school_registration_number'];

                }
                if ($data['CoreSchoolSelfRegistration']['bank_name']) {
                    $model->bank_name = $data['CoreSchoolSelfRegistration']['bank_name'];

                }
                if ($data['CoreSchoolSelfRegistration']['default_part_payment_behaviour']) {
                    $model->default_part_payment_behaviour = $data['CoreSchoolSelfRegistration']['default_part_payment_behaviour'];

                }


                Yii::trace(  $model->save(false));



                if ($posted_banks = $data['bank_name']) {
                    Yii::trace( $posted_banks);


                    $this->saveASelfccounts($posted_banks, $model->id);
                }



                if($model->save(false) ) {
                    $transaction->commit();

                    \Yii::$app->session->setFlash('school', "<div class='alert alert-success'><b>YOU HAVE SUCCESSFULLY SUBMITTED YOUR SCHOOL</b>. Our team will contact you in the shortest time possible!<br><p><b><a style='color:#288594;' href='".Url::to(['/site'])."'>Return to login page</a></b></p> </div>");

                    Logs::logEvent("<b>SCHOOL SELF REG REQUEST SUCCESSFULL</b>: ".$model->school_name, null, null);
                    // $this->refresh();


                }

            }
            return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            \Yii::$app->session->setFlash('business', "<div class='alert alert-success'><b>ERROR SUBMITTING YOUR SCHOOL</b>. Contact Our support team on 0200.502.140<p><b><a style='color:#288594;' href='".Url::to(['/site'])."'>Return to login page</a></b></p> </div>");

        }


    }








    private function getSchoolAccounts($schId)
    {
        $accounts = (new Query())->from('core_bank_account_details ba')->select(['ba.id as bid', 'nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId]);
        return $accounts->all();
    }



    private function getBanks()
    {
        $banks = (new Query())->from('core_nominated_bank')->select(['id', 'bank_name']);
        return $banks->all();
    }

    private function validateBankInfo($bank_accounts)

    {
        $error = null;

        foreach ($bank_accounts as $k => $v) {

            if (!$v) {
                $error = $k;

                Yii::trace($error);
            }
        }
        return $error;

    }


}

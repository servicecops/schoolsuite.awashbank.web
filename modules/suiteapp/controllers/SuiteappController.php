<?php


namespace app\modules\suiteapp\controllers;


use app\components\ToWords;
use app\models\User;
use app\modules\attendance\AttendanceModule;
use app\modules\attendance\models\Rollcall;
use app\modules\attendance\models\StudentDailyAttendance;
use app\modules\attendance\models\TeacherDailyAttendance;
use app\modules\attendance\models\TeacherSubjectAttendance;
use app\modules\banks\models\BankAccountDetails;
use app\modules\banks\models\SelfEnrolledBankAccountDetails;
use app\modules\logs\models\Logs;

use app\modules\online_reg\models\CoreSchoolSelfRegistration;
use app\modules\paymentscore\models\ExternalPaymentSources;
use app\modules\paymentscore\models\ImageBank;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\PaymentsReceivedSearch;
use app\modules\paymentscore\models\StudentAccountGl;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreTeacherSubjectClassAssociation;
use app\modules\studentreport\models\CircularUploads;
use app\modules\studentreport\models\SchoolSocialMedia;
use app\modules\studentreport\models\SignatureUploads;
use app\modules\timetable\models\Timetable;
use app\modules\timetable\models\TimetableSchedule;
use DateTime;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

//use app\models\FeesDue;
//use app\models\FeeClass;
//use app\models\Classes;
//use app\models\FeesDueSearch;
//use app\modules\logs\models\Logs;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class SuiteappController extends Controller
{


    public function beforeAction($action)
    {


//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }

//something code right here if user valid

        return true;

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

        ];
    }


    public function actionLogin(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);
        $schLogo = null;

        try{
            //check userpassword
            $username = User::findByUsernameMob(strtolower($incomingRequest['username']));
            if($username){
                if($username->locked){
                    throw new ForbiddenHttpException('Account is locked, please reset your password');

                }
                if(!$username->school_id){
                    throw new ForbiddenHttpException('Not accessible to non school users');

                }
                    $password = Yii::$app->security->validatePassword($incomingRequest['password'], $username->password);
                    Yii::trace($password);
                    $userDetails = User::findOne(['id'=>$username->id]);

                    /* $user = (new \yii\db\Query())
                         ->select(['first_name', 'last_name','username', 'mobile_number', 'email_address','auth_key','region_id','district_id'])
                         ->from('web_console_users')
                         ->where(['id' => $username->id])
                         ->one();*/
                    Yii::trace($userDetails);
                    $user_id = $username->id;
                    $model = new User();
                    $schId = $userDetails['school_id'];
                    $schData = CoreSchool::findOne(['id'=>$schId]);
                    if ($schData['school_logo']) {
                        $schLogo = ImageBank::findOne(['id'=>$schData['school_logo']])->image_base64;
                    }




                    $userdata =[

                        'username'=>$userDetails->username,
                        'first_name'=>$userDetails->firstname,
                        'last_name'=>$userDetails->lastname,
                        'mobile_number'=>$userDetails->mobile_phone,
                        'email_address'=>$userDetails->email,
                        'user_level'=>$userDetails->user_level,
                        'auth_key'=>$userDetails->auth_key,
                        'id'=>$userDetails->id

                    ];

                    $schInfo =[
                        'school_name'=>$schData->school_name,
                        'school_id'=>$schData->id,
                        'school_location'=>$schData->village,
                        'school_email'=>$schData->contact_email_1,
                        'school_logo'=>$schLogo
                    ];

                    Yii::trace($userDetails);

                    if($password){
                        return json_encode([
                            'returncode' => 0,
                            'returnmessage' => " User loggedin successfully",
                            'userInfo'=>$userdata,
                            'schInfo'=>$schInfo
                        ]);
                    }



                    else {
                        if ($username !== null) {
                            $username->scenario = 'count';
                            $username->invalid_login_count += 1;
                            if ($username->invalid_login_count >= 3) {
                                $username->locked = true;
                                $username->invalid_login_count = 0;
                            }
                            $username->save(false);
                            throw new ForbiddenHttpException('Invalid password, account will be locked after 3rd attempt');
                        }
                    }

            }
            else{


                throw new ForbiddenHttpException('Invalid Username');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }


    public function actionTeacherClasses($teacherId){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $classInfo=[];

            $userDetails = User::findOne($teacherId);

            if($userDetails){
                Yii::trace($userDetails);
            }else{
                throw new ForbiddenHttpException('Teacher doesnot exist');

            }

            //staff table
            $teacherDetails = CoreStaff::findOne($userDetails->school_user_id);
            if($teacherDetails){

                //get teacher classes and subjects

                $teacherClasses = CoreTeacherSubjectClassAssociation::findAll(['teacher_id'=>$teacherDetails->id]);


                if($teacherClasses){

                    Yii::trace($teacherClasses);
                    foreach ($teacherClasses as $class) {
                        $classDetails = CoreSchoolClass::findOne(['id'=>$class->class_id]);
                        $classId =$classDetails->id;
                        $className =$classDetails->class_description;
                        $classCode=$classDetails->class_code;
                        $theclasses =["classId"=>$classId, "className"=>$className,"classCode"=>$classCode];
                        array_push($classInfo ,$theclasses);
                    }
                    Yii::trace($classInfo);
                    return json_encode([
                        'returncode' => 0,
                        'returnmessage' => "SUCCESSFUL, Class details found",
                        'classDetails'=>$classInfo,
                    ]);
                }

                else {

                    throw new ForbiddenHttpException('Teacher has no classes assigned ');
                }

            }
            else{

                throw new ForbiddenHttpException('Teacher doesnot exist');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }


    public function actionClassSubject(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $sbjInfo=[];
            $clsDetails=[];
            //check if teacher exists
            $userDetails = User::findOne($incomingRequest['tr_id']);

            if($userDetails){
                Yii::trace($userDetails);
            }else{
                throw new ForbiddenHttpException('Teacher doesnot exist');

            }

            //staff table
            $teacherDetails = CoreStaff::findOne($userDetails->school_user_id);
            if($teacherDetails){

                //get teacher classes and subjects

                $teacherSubjects = CoreTeacherSubjectClassAssociation::findAll(['teacher_id'=>$teacherDetails->id, 'class_id'=>$incomingRequest['cls_id']]);


                if($teacherSubjects){

                    Yii::trace($teacherSubjects);
                    foreach ($teacherSubjects as $subject) {
                        $subjectDetails = CoreSubject::findOne(['id'=>$subject->subject_id]);
                        $subjectId =$subjectDetails->id;
                        $subjectName =$subjectDetails->subject_description;
                        $subjectCode=$subjectDetails->subject_code;


                        $thesubjects =["subjectId"=>$subjectId, "subjectName"=>$subjectName,"subjectCode"=>$subjectCode];
                        array_push($sbjInfo ,$thesubjects);
                    }

                    Yii::trace($sbjInfo);

                    return json_encode([
                        'returncode' => 0,
                        'returnmessage' => "SUCCESSFUL, subject details found",
                        'subjectDetails'=>$sbjInfo,
                    ]);
                }

                else {
                    throw new ForbiddenHttpException('Teacher has no subjects assigned ');
                }

            }
            else{


                throw new ForbiddenHttpException('Teacher doesnot exist');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }

    public function actionClassStudents($subjectId){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $stdInfo=[];
            $clsDetails=[];
            //check if subject exists

            $subjectDetails = CoreSubject::findOne($subjectId);

            if($subjectDetails){
                Yii::trace($subjectDetails);
                //get teacher classes and subjects

                $students = CoreStudent::findAll(['class_id'=>$subjectDetails->class_id]);


                if($students){

                    Yii::trace($students);
                    foreach ($students as $std) {
                        $firstName =$std->first_name;
                        $lastName =$std->last_name;
                        $studentCode =$std->student_code;
                        $middleName=$std->middle_name;
                        $studentId=$std->id;
                        $gender=$std->gender;
                        $classId=$subjectDetails->class_id;
                        $subjectId=$subjectId;
                        $attendanceStatus = 0;




                        $theStudents =["firstName"=>$firstName, "LastName"=>$lastName,"middleName"=>$middleName,"studentId"=>$studentId, "gender"=>$gender,
                            "studentCode"=>$studentCode,"classId"=>$classId, "subjectId"=>$subjectId, "attendanceStatus"=>$attendanceStatus];
                        array_push($stdInfo ,$theStudents);
                    }

                    Yii::trace($stdInfo);


                    return json_encode([
                        'returncode' => 0,
                        'returnmessage' => "SUCCESSFUL, Class details found",
                        'classStudents'=>$stdInfo,
                    ]);
                }



                else {

                    throw new ForbiddenHttpException('No students found that take this subject ');


                }

            }
            else{


                throw new ForbiddenHttpException('No Subject found');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }

    public function actionSingleClassStudents(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $stdInfo=[];
            $clsDetails=[];
            //check if subject exists



            $students = CoreStudent::findAll(['class_id'=>$incomingRequest['class_id']]);


            if($students){

                Yii::trace($students);
                foreach ($students as $std) {
                    $firstName =$std->first_name;
                    $lastName =$std->last_name;
                    $studentCode =$std->student_code;
                    $middleName=$std->middle_name;
                    $studentId=$std->id;
                    $classId=$incomingRequest['class_id'];
                    $subjectId=null;
                    $attendanceStatus = 0;




                    $theStudents =["firstName"=>$firstName, "LastName"=>$lastName,"middleName"=>$middleName,"studentId"=>$studentId,
                        "studentCode"=>$studentCode,"classId"=>$classId, "subjectId"=>$subjectId,"attendanceStatus"=>$attendanceStatus];
                    array_push($stdInfo ,$theStudents);
                }

                Yii::trace($stdInfo);


                return json_encode([
                    'returncode' => 0,
                    'returnmessage' => "SUCCESSFUL, Class details found",
                    'classStudents'=>$stdInfo,
                ]);
            }



            else {

                throw new ForbiddenHttpException('No students found in this class ');


            }




        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }

    public function actionSaveAttendance(){
        try {
            $transaction = Rollcall::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


            $subjectInfo = $incomingRequest['subjectId']; //The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details

            $attendanceInfo = $incomingRequest['attendanceResults']; //The $classesInfo, ideally will correspond to a Class details



            $teacher = User::findOne($teacherInfo);
            Yii::trace($teacher);
            if(!$teacher)
                throw new Exception("Teacher Id deoesnot exist");


            $subject = CoreSubject::findOne($subjectInfo);
            If($subject){

                foreach ($attendanceInfo as $K => $v) {
                    $model = new Rollcall();
                    $trModel = new TeacherSubjectAttendance();
                    $model->rollcalled_by = $teacherInfo;

                    $model->class_id = $subject->class_id;
                    $model->school_id = $subject->school_id;
                    $model->subject_id = $subjectInfo;




                    Yii::trace($v);
                    $model->student_id = $v['studentId'];
                    if ($v['attendanceStatus'] === 1) {
                        $model->student_attended = true;
                    } else {
                        $model->student_attended = false;

                    }
                    $model->save(false);


                }
                $trModel->teacher_id = $teacherInfo;
                $trModel->present = true;
                $trModel->class_id = $subject->class_id;
                $trModel->subject_id = $subjectInfo;
                $trModel->ip_address = $incomingRequest['userIp'];
                $trModel->school_id = $subject->school_id;
                $trModel->method_used = 'ticking';

                $trModel->save(false);
                $transaction->commit();
                if ($model->save(false)) {
                    Logs::logEvent("Rollcalled Student for : " . $model->subject_id, null, $model->id);
                    return json_encode([
                        'returnCode' => 0,
                        'returnMessage' => "SUCCESSFUL, student rollcall details saved",
                    ]);

                }


            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student rollcall details: " , $error, null);

            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }


    }


    public function actionSaveClassAttendance(){
        try {
            $transaction = StudentDailyAttendance::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


            $classInfo = $incomingRequest['classId']; //The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details

            $attendanceInfo = $incomingRequest['attendanceResults']; //The $classesInfo, ideally will correspond to a Class details



            $teacher = User::findOne($teacherInfo);
            Yii::trace($teacher);
            if(!$teacher)
                throw new Exception("Teacher Id deoesnot exist");


            $attendance = Yii::$app->db->createCommand('select * from student_general_attendance where date_created >= now()::date + interval \'1h\' and class_id =:classId' )
                ->bindValue(':classId', $classInfo)
                ->queryAll();
            Yii::trace($attendance);

            if($attendance){
                throw new Exception("Sorry, today's rollcall has already been made");
            }

            $class = CoreSchoolClass::findOne($classInfo);
            If($class){

                foreach ($attendanceInfo as $K => $v) {
                    $model = new StudentDailyAttendance();
//                    $trModel = new TeacherSubjectAttendance();
                    $model->teacher_id = $teacherInfo;

                    $model->class_id = $classInfo;
                    $model->school_id = $class->school_id;

                    /*
                                        $trModel->teacher_id = $teacherInfo;
                                        $trModel->present = true;
                                        $trModel->class_id = $classInfo;
                                        $trModel->ip_address = $incomingRequest['userIp'];
                                        $trModel->school_id = $class->school_id;
                                        $trModel->method_used = 'ticking';*/

                    Yii::trace($v);
                    $model->student_id = $v['studentId'];
                    if ($v['attendanceStatus'] === 1) {
                        $model->present = true;
                    } else {
                        $model->present = false;

                    }
                    $model->save(false);
//                    $trModel->save(false);

                }
                $transaction->commit();
                if ($model->save(false)) {
                    Logs::logEvent("Rollcalled Student for : " . $model->class_id, null, $model->id);
                    return json_encode([
                        'returnCode' => 0,
                        'returnMessage' => "SUCCESSFUL, student rollcall details saved",
                    ]);

                }


            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student rollcall details: " , $error, null);

            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }


    }

    public function actionScannedStudent(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $stdInfo=[];
            $clsDetails=[];
            //check if subject exists

            $studentDetails = CoreStudent::findOne(['student_code'=>$incomingRequest['std_code']]);

            if($studentDetails){
                Yii::trace($studentDetails);


                $class = CoreSchoolClass::findOne($studentDetails->class_id);
                $firstName =$studentDetails->first_name;
                $lastName =$studentDetails->last_name;
                $middleName=$studentDetails->middle_name;
                $studentId=$studentDetails->id;
                $className = $class->class_description;
                $classId=$class->id;



                $theStudents =["firstName"=>$firstName, "LastName"=>$lastName,"middleName"=>$middleName,
                    "studentId"=>$studentId,"className"=>$className, "classId"=>$classId];
                array_push($stdInfo ,$theStudents);


                Yii::trace($stdInfo);


                return json_encode([
                    'returncode' => 0,
                    'returnmessage' => "SUCCESSFUL, Student details found",
                    'studentDetails'=>$stdInfo,
                ]);





            }
            else{


                throw new ForbiddenHttpException('Student not found');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }


    public function actionSaveQrAttendance(){
        try {
            $transaction = Rollcall::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


//            $subjectInfo = $incomingRequest['subjectId']; //The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details

            $attendanceInfo = $incomingRequest['attendanceResults']; //The $classesInfo, ideally will correspond to a Class details



            $teacher = User::findOne($teacherInfo);
            Yii::trace($teacher);
            if(!$teacher)
                throw new Exception("Teacher Id deoesnot exist");



            foreach ($attendanceInfo as $K => $v) {
                $model = new Rollcall();

                $trModel = new TeacherSubjectAttendance();
                $model->rollcalled_by = $teacherInfo;

                $model->class_id = $v['classId'];
                $model->subject_id = $v['subjectId'];
                $model->school_id = $teacher->school_id;


                Yii::trace($v);
                $model->student_id = $v['studentId'];

                if ($v['attendanceStatus'] === 1) {
                    $model->student_attended = true;
                } else {
                    $model->student_attended = false;

                }




                $model->save(false);


            }
            $trModel->teacher_id = $teacherInfo;
            $trModel->present = true;
            $trModel->class_id = $v['classId'];
            $trModel->subject_id = $v['subjectId'];
            $trModel->ip_address = $incomingRequest['userIp'];
            $trModel->school_id =  $teacher->school_id;
            $trModel->method_used = 'QR';
            $trModel->save(false);

            $transaction->commit();
            if ($model->save(false)) {
                Logs::logEvent("Rollcalled Student for : " . $model->class_id, null, $model->id);
                return json_encode([
                    'returnCode' => 0,
                    'returnMessage' => "SUCCESSFUL, students rollcall details saved",
                ]);

            }



        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student rollcall details: " , $error, null);

            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }


    }



    public function actionSaveQrClassAttendance(){
        try {
            $transaction = StudentDailyAttendance::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


//            $subjectInfo = $incomingRequest['subjectId']; //The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details

            $attendanceInfo = $incomingRequest['attendanceResults']; //The $classesInfo, ideally will correspond to a Class details



            $teacher = User::findOne($teacherInfo);
            Yii::trace($teacher);
            if(!$teacher)
                throw new Exception("Teacher Id deoesnot exist");

            $attendance = Yii::$app->db->createCommand('select * from student_general_attendance where date_created >= now()::date + interval \'1h\' and class_id =:classId' )
                ->bindValue(':classId', $incomingRequest['classId'])
                ->queryAll();
            Yii::trace($attendance);

            if($attendance){
                throw new Exception("Sorry, today's rollcall has already been made");
            }

            foreach ($attendanceInfo as $K => $v) {
                $model = new StudentDailyAttendance();


                $model->teacher_id = $teacherInfo;

                $model->class_id = $v['classId'];
                $model->school_id = $teacher->school_id;



                Yii::trace($v);
                $model->student_id = $v['studentId'];

                if ($v['attendanceStatus'] === 1) {
                    $model->present = true;
                } else {
                    $model->present = false;

                }
                /*
                                $trModel->teacher_id = $teacherInfo;
                                $trModel->present = true;
                                $trModel->class_id = $v['classId'];
                                $trModel->subject_id = $v['subjectId'];
                                $trModel->ip_address = $incomingRequest['userIp'];
                                $trModel->school_id =  $teacher->school_id;
                                $trModel->method_used = 'QR';*/


                $model->save(false);
//                $trModel->save(false);

            }
            $transaction->commit();
            if ($model->save(false)) {
                Logs::logEvent("Rollcalled Student for : " . $model->class_id, null, $model->id);
                return json_encode([
                    'returnCode' => 0,
                    'returnMessage' => "SUCCESSFUL, students rollcall details saved",
                ]);

            }



        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student rollcall details: " , $error, null);

            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }


    }


    public function actionAttendanceReport(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $stdInfo=[];
            $stdAbsent=[];
            $clsDetails=[];
            //check if subject exists

//            $subjectDetails = CoreSubject::findOne($subjectId);

            /* if($subjectDetails){
                 Yii::trace($subjectDetails);*/
            //get teacher classes and subjects
            $student = StudentDailyAttendance::find()->where(['class_id'=>$incomingRequest['class_id']])->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])->all();
            $studentPresent = StudentDailyAttendance::find()->where(['class_id'=>$incomingRequest['class_id'], 'present'=>true])->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])->select('student_id')->distinct()->all();
            $studentAbsent = StudentDailyAttendance::find()->select('student_id')->distinct()->where(['class_id'=>$incomingRequest['class_id'], 'present'=>false])->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])->all();

            if($student){



                Yii::trace($studentPresent);
                foreach ($studentPresent as $std) {
                    $stdDara = CoreStudent::findAll(['id'=>$std->student_id]);
                    foreach ($stdDara as $stdss) {
                        $firstName =$stdss->first_name;
                        $lastName =$stdss->last_name;
                        $gender =$stdss->gender;
                        $attended=$std->present;
                        $studentId=$stdss->id;
                        $dateRollcalled = date("m/d/y",strtotime($std->date_created));
                    }


                    $theStudents =["firstName"=>$firstName, "LastName"=>$lastName,"attended"=>$attended,"studentId"=>$studentId,'dateRollcalled'=>$dateRollcalled, 'gender'=>$gender];
                    array_push($stdInfo ,$theStudents);
                }

                Yii::trace($studentAbsent);
                foreach ($studentAbsent as $std) {
                    $stdDara = CoreStudent::findAll(['id'=>$std->student_id]);
                    foreach ($stdDara as $stdss) {
                        $firstName =$stdss->first_name;
                        $lastName =$stdss->last_name;
                        $gender =$stdss->gender;
                        $attended=$std->present;
                        $studentId=$stdss->id;
                        $dateRollcalled = date("m/d/y",strtotime($std->date_created));
                    }


                    $theStudentAbsent =["firstName"=>$firstName, "LastName"=>$lastName,"attended"=>$attended,"studentId"=>$studentId,'dateRollcalled'=>$dateRollcalled, 'gender'=>$gender];
                    array_push($stdAbsent ,$theStudentAbsent);
                }

                $presentCount = (new Query())
                    ->from('student_general_attendance')
                    ->where(['class_id'=>$incomingRequest['class_id'], 'present'=>true])
                    ->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $presentMaleCount = (new Query())
                    ->select('sg.student_id')
                    ->distinct()
                    ->from('student_general_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.present'=>true, 'si.gender'=> 'M'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $presentFemaleCount = (new Query())
                    ->from('student_general_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.present'=>true, 'si.gender'=> 'F'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $absentFemaleCount = (new Query())
                    ->from('student_general_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.present'=>false, 'si.gender'=> 'F'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $absentMaleCount = (new Query())
                    ->from('student_general_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.present'=>false, 'si.gender'=> 'M'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $absentCount = (new Query())
                    ->from('student_general_attendance')
                    ->where(['class_id'=>$incomingRequest['class_id'], 'present'=>false])
                    ->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                Yii::trace($stdInfo);


                return json_encode([
                    'returncode' => 0,
                    'returnmessage' => "SUCCESSFUL, Class details found",
                    'stdPresent'=>$stdInfo,
                    'stdAbsent'=>$stdAbsent,
                    'presentCount'=>$presentCount,
                    'absentCount'=>$absentCount,
                    'absentMaleCount'=>$absentMaleCount,
                    'absentFemaleCount'=>$absentFemaleCount,
                    'presentFemaleCount'=>$presentFemaleCount,
                    'presentMaleCount'=>$presentMaleCount,
                ]);
            }



            else {

                throw new ForbiddenHttpException('No students found ');


            }

            /*  }
              else{


                  throw new ForbiddenHttpException('No Subject found');
              }*/

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }
    }


    public function actionSubjectAttendanceReport(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $stdInfo=[];
            $stdAbsent=[];
            $clsDetails=[];
            //check if subject exists

//            $subjectDetails = CoreSubject::findOne($subjectId);

            /* if($subjectDetails){
                 Yii::trace($subjectDetails);*/
            //get teacher classes and subjects
            $student = Rollcall::find()->select('student_id')->distinct()->where(['class_id'=>$incomingRequest['class_id'], 'subject_id'=>$incomingRequest['subject_id']])->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])->all();
            $studentPresent = Rollcall::find()->select('student_id')->distinct()->where(['class_id'=>$incomingRequest['class_id'], 'subject_id'=>$incomingRequest['subject_id'], 'student_attended'=>true])->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])->all();
            $studentAbsent = Rollcall::find()->select('student_id')->distinct()->where(['class_id'=>$incomingRequest['class_id'], 'subject_id'=>$incomingRequest['subject_id'], 'student_attended'=>false])->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])->all();

            if($student){



                Yii::trace($studentPresent);
                foreach ($studentPresent as $std) {
                    $stdDara = CoreStudent::findAll(['id'=>$std->student_id]);
                    foreach ($stdDara as $stdss) {
                        $firstName =$stdss->first_name;
                        $lastName =$stdss->last_name;
                        $gender =$stdss->gender;
                        $attended=$std->student_attended;
                        $studentId=$stdss->id;
                        $dateRollcalled = date("m/d/y",strtotime($std->date_created));
                    }


                    $theStudents =["firstName"=>$firstName, "LastName"=>$lastName,"attended"=>$attended,"studentId"=>$studentId,'dateRollcalled'=>$dateRollcalled, 'gender'=>$gender];
                    array_push($stdInfo ,$theStudents);
                }

                Yii::trace($studentAbsent);
                foreach ($studentAbsent as $std) {
                    $stdDara = CoreStudent::findAll(['id'=>$std->student_id]);
                    foreach ($stdDara as $stdss) {
                        $firstName =$stdss->first_name;
                        $lastName =$stdss->last_name;
                        $gender =$stdss->gender;
                        $attended=$std->student_attended;
                        $studentId=$stdss->id;
                        $dateRollcalled = date("m/d/y",strtotime($std->date_created));
                    }
                    $theStudentAbsent =["firstName"=>$firstName, "LastName"=>$lastName,"attended"=>$attended,"studentId"=>$studentId,'dateRollcalled'=>$dateRollcalled, 'gender'=>$gender];
                    array_push($stdAbsent ,$theStudentAbsent);
                }

                $presentCount = Rollcall::find()
                    ->select('student_id')
                    ->distinct()
                    ->where(['class_id'=>$incomingRequest['class_id'], 'student_attended'=>true])
                    ->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $presentMaleCount = (new Query())
                    ->select('student_id')
                    ->distinct()
                    ->from('student_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.student_attended'=>true, 'si.gender'=> 'M'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $presentFemaleCount = (new Query())
                    ->select('student_id')
                    ->distinct()
                    ->from('student_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.student_attended'=>true, 'si.gender'=> 'F'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $absentFemaleCount = (new Query())
                    ->select('student_id')
                    ->distinct()
                    ->from('student_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.student_attended'=>false, 'si.gender'=> 'F'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $absentMaleCount = (new Query())
                    ->select('student_id')
                    ->distinct()
                    ->from('student_attendance sg')
                    ->innerJoin('core_student si','si.id = sg.student_id')
                    ->where(['sg.class_id'=>$incomingRequest['class_id'], 'sg.student_attended'=>false, 'si.gender'=> 'M'])
                    ->andWhere(['between', 'sg.date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                $absentCount = (new Query())
                    ->select('student_id')
                    ->distinct()
                    ->from('student_attendance')
                    ->where(['class_id'=>$incomingRequest['class_id'], 'student_attended'=>false])
                    ->andWhere(['between', 'date_created', $incomingRequest['date_from'], $incomingRequest['date_to']])
                    ->count();

                Yii::trace($stdInfo);


                return json_encode([
                    'returncode' => 0,
                    'returnmessage' => "SUCCESSFUL, Class details found",
                    'stdPresent'=>$stdInfo,
                    'stdAbsent'=>$stdAbsent,
                    'presentCount'=>$presentCount,
                    'absentCount'=>$absentCount,
                    'absentMaleCount'=>$absentMaleCount,
                    'absentFemaleCount'=>$absentFemaleCount,
                    'presentFemaleCount'=>$presentFemaleCount,
                    'presentMaleCount'=>$presentMaleCount,
                ]);
            }



            else {

                throw new ForbiddenHttpException('No students found ');


            }

            /*  }
              else{


                  throw new ForbiddenHttpException('No Subject found');
              }*/

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }
    }



    public function actionPresentAbsentReport(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $stdInfo=[];
            $clsDetails=[];
            //check if subject exists

//            $subjectDetails = CoreSubject::findOne($subjectId);

            /* if($subjectDetails){
                 Yii::trace($subjectDetails);*/
            //get teacher classes and subjects

            $students = Rollcall::findAll(['class_id'=>$incomingRequest['class_id'],'student_attended'=>$incomingRequest['status']]);



            if($students){

                Yii::trace($students);
                foreach ($students as $std) {
                    $stdDara = CoreStudent::findAll(['id'=>$std->student_id]);
                    foreach ($stdDara as $stdss) {
                        $firstName =$stdss->first_name;
                        $lastName =$stdss->last_name;
                        $attended=$std->student_attended;
                        $studentId=$stdss->id;
                    }




                    $theStudents =["firstName"=>$firstName, "LastName"=>$lastName,"attended"=>$attended,"studentId"=>$studentId];
                    array_push($stdInfo ,$theStudents);
                }

                Yii::trace($stdInfo);


                return json_encode([
                    'returncode' => 0,
                    'returnmessage' => "SUCCESSFUL, Class details found",
                    'classStudents'=>$stdInfo,
                ]);
            }



            else {

                throw new ForbiddenHttpException('No students found ');


            }

            /*  }
              else{


                  throw new ForbiddenHttpException('No Subject found');
              }*/

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }
    }

    public function actionSaveBatchAttendance(){
        try {
            $transaction = Rollcall::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


            $subjectInfo = $incomingRequest['subjectId']; //The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details

            $attendanceInfo = $incomingRequest['attendanceResults']; //The $classesInfo, ideally will correspond to a Class details


            $teacher = User::findOne($teacherInfo);
            Yii::trace($teacher);
            if(!$teacher)
                throw new Exception("Teacher Id deoesnot exist");


            $subject = CoreSubject::findOne($subjectInfo);
            If($subject){

                foreach ($attendanceInfo as $K => $v) {
                    $model = new Rollcall();
                    $trModel = new TeacherSubjectAttendance();
                    $model->rollcalled_by = $teacherInfo;

                    $model->class_id = $subject->class_id;
                    $model->school_id = $subject->school_id;
                    $model->subject_id = $subjectInfo;
                    $model->student_attended = $incomingRequest['studentAttended'];

                    $trModel->teacher_id = $teacherInfo;
                    $trModel->present = true;
                    $trModel->class_id = $subject->class_id;
                    $trModel->subject_id = $subjectInfo;
                    $trModel->ip_address = "192.88.3388.4";
                    $trModel->school_id = $subject->school_id;
                    $trModel->method_used = 'batch';

                    Yii::trace($v);
                    $model->student_id = $v['studentId'];

                    $model->save(false);

                }
                $transaction->commit();
                if ($model->save(false)) {
                    Logs::logEvent("Rollcalled Student for : " . $model->subject_id, null, $model->id);
                    return json_encode([
                        'returnCode' => 0,
                        'returnMessage' => "All students have been rollcalled successfully",
                    ]);

                }


            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student rollcall details: " , $error, null);

            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }


    }

    public function actionSaveBatchClassAttendance(){
        try {
            $transaction = StudentDailyAttendance::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


            $classtInfo = $incomingRequest['classId']; //The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details

            $attendanceInfo = $incomingRequest['attendanceResults']; //The $classesInfo, ideally will correspond to a Class details



            $teacher = User::findOne($teacherInfo);
            Yii::trace($teacher);
            if(!$teacher)
                throw new Exception("Teacher Id deoesnot exist");

            $attendance = Yii::$app->db->createCommand('select * from student_general_attendance where date_created >= now()::date + interval \'1h\' and class_id =:classId' )
                ->bindValue(':classId', $incomingRequest['classId'])
                ->queryAll();
            Yii::trace($attendance);

            if($attendance){
                throw new Exception("Sorry, today's rollcall has already been made");
            }

            $class = CoreSchoolClass::findOne($classtInfo);
            If($class){

                foreach ($attendanceInfo as $K => $v) {
                    $model = new StudentDailyAttendance();
                    $trModel = new TeacherSubjectAttendance();
                    $model->teacher_id = $teacherInfo;

                    $model->class_id = $classtInfo;
                    $model->school_id = $class->school_id;
                    $model->present = $incomingRequest['studentAttended'];

                    $trModel->teacher_id = $teacherInfo;
                    $trModel->present = true;
                    $trModel->class_id = $classtInfo;
                    $trModel->ip_address = "192.88.3388.4";
                    $trModel->school_id = $class->school_id;
                    $trModel->method_used = 'batch';

                    Yii::trace($v);
                    $model->student_id = $v['studentId'];

                    $model->save(false);

                }
                $transaction->commit();
                if ($model->save(false)) {
                    Logs::logEvent("Rollcalled Student for : " . $model->class_id, null, $model->id);
                    return json_encode([
                        'returnCode' => 0,
                        'returnMessage' => "All students have been rollcalled successfully",
                    ]);

                }


            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting student rollcall details: " , $error, null);

            return json_encode([
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }


    }

    public function actionTestConnection(){

        $query= new \yii\db\Query();
        $query ->select(['id'])
            ->from('student_attendance pr')
            ->andWhere(['pr.class_id'=>1])
            ->andWhere(['between', 'pr.date_created', '2021-02-22', '2021-02-23']);

        return json_encode([
            'returnCode' => 0,
            'returnMessage' => "Got it",
            'data'=>$query
        ]);

    }

    public function actionSchoolStudents(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $classInfo=[];
            $clsDetails=[];
            //check if teacher exists
            $userDetails = User::findOne($incomingRequest['teacher_id']);

            if($userDetails){
                Yii::trace($userDetails);
            }else{
                throw new ForbiddenHttpException('Teacher doesnot exist');

            }

            //staff table
            $teacherDetails = CoreStaff::findOne($userDetails->school_user_id);
            if($teacherDetails){

                //get teacher classes and subjects

                $schStudents = CoreStudent::findAll(['school_id'=>$incomingRequest['school_id']]);


                if($schStudents){

                    Yii::trace($schStudents);
                    foreach ($schStudents as $student) {
                        $classDetails = CoreSchoolClass::findOne(['id'=>$student->class_id]);
                        $studentId =$student->id;
                        $studentCode = $student->student_code;
                        $studentName =$student->first_name .' '.$student->last_name;
                        $studentClass=$classDetails->class_description;
                        $classId=$classDetails->id;


                        $theclasses =["studentId"=>$studentId, "studentCode"=>$studentCode, "studentName"=>$studentName,"studentClass"=>$studentClass,"classId"=>$classId];
                        array_push($classInfo ,$theclasses);
                    }

                    Yii::trace($classInfo);


                    return json_encode([
                        'returncode' => 0,
                        'returnmessage' => "SUCCESSFUL, students found",
                        'studentsDetails'=>$classInfo,
                    ]);
                }



                else {

                    throw new ForbiddenHttpException('Teacher has no classes assigned ');


                }

            }
            else{


                throw new ForbiddenHttpException('Teacher doesnot exist');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }

    public function actionSchoolStaff(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $staffInfo=[];
            $clsDetails=[];
            //check if teacher exists
            $userDetails = User::findOne($incomingRequest['teacher_id']);

            if($userDetails){
                Yii::trace($userDetails);
            }else{
                throw new ForbiddenHttpException('Teacher doesnot exist');

            }

            //staff table
            $teacherDetails = CoreStaff::findOne($userDetails->school_user_id);
            if($teacherDetails){

                //get teacher classes and subjects

                $schStaff = User::findAll(['school_id'=>$teacherDetails->school_id]);


                if($schStaff){

                    Yii::trace($schStaff);
                    foreach ($schStaff as $staff) {
                        $staffId =$staff->id;
                        $staffName =$staff->firstname .' '.$staff->lastname;



                        $thestaff =["staffId"=>$staffId, "schoolId"=>$teacherDetails->school_id, "staffName"=>$staffName];
                        array_push($staffInfo ,$thestaff);
                    }

                    Yii::trace($staffInfo);


                    return json_encode([
                        'returncode' => 0,
                        'returnmessage' => "SUCCESSFUL, staff found",
                        'staffDetails'=>$staffInfo,
                    ]);
                }



                else {

                    throw new ForbiddenHttpException('Staff not found ');


                }

            }
            else{


                throw new ForbiddenHttpException('Teacher doesnot exist');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returncode' => 909,
                'returnmessage' => $e->getMessage(),
            ]);

        }

    }





    public function actionTrClockin()
    {
        try {
            $transaction = Rollcall::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


//            / $subjectInfo = $incomingRequest['subjectId']; ///The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details
            $submitterInfo = $incomingRequest['createdBy']; //The $schoolInfo, ideally will correspond to a School details


            $teacher = User::findOne($teacherInfo);
            $submitter = User::findOne($submitterInfo);
            Yii::trace($teacher->username);
            if (!$teacher)
                throw new Exception("Teacher Id deoesnot exist");
            if (!$submitter)
                throw new Exception("Submitter Id deoesnot exist");

            $lastClockinRecord = TeacherDailyAttendance::find()
                ->where(['teacher_id'=>$teacherInfo])
                ->orderBy(['date_created'=>SORT_DESC])
                ->limit(1)->one();

            Yii::trace($lastClockinRecord);

            $connection = Yii::$app->db;
            if(!$lastClockinRecord || !$lastClockinRecord->is_clockin){
// $transaction = $connection->beginTransaction();
                $sql2 = "INSERT INTO teacher_clockin_attendance (
rollcalled_by,
clock_in,
teacher_id,
is_clockin,
school_id
)
VALUES (
:rollcalled_by,
NOW() ,
:teacher_id,
true,
:school_id
)";
                $fileQuery = $connection->createCommand($sql2);
                $fileQuery->bindValue(':rollcalled_by', $submitterInfo);
                $fileQuery->bindValue(':teacher_id', $teacherInfo);
                $fileQuery->bindValue(':school_id', $teacher->school_id);
//Insert file
                $fileQuery->execute();
                $msg ="Teacher $teacher->username successfully Clocked In by $submitter->username";


            }
            else {
                $recordId=$lastClockinRecord->id;

                Yii::trace($lastClockinRecord->id);

                $sql = "UPDATE teacher_clockin_attendance set is_clockin = false,clock_out= NOW() where id = :recordId " ;

                $numberOfStudentsMoved = $connection->createCommand($sql)
                    ->bindValue(':recordId', $recordId)
                    ->execute();
                $msg ="Teacher $teacher->username successfully Clocked Out by $submitter->username ";

            }

            Logs::logEvent("Rollcalled teacher for : " .$teacherInfo, null, null);


            $transaction->commit();
            return json_encode([
                'returncode' => 0,
                'returnMessage' => $msg,
            ]);

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting teacher rollcall details: ", $error, null);

            return json_encode([
                'returncode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }}



    public function actionSelfClockin()
    {
        try {
            $transaction = Rollcall::getDb()->beginTransaction();

            $raw_data = Yii::$app->request->getRawBody();
            $incomingRequest = json_decode($raw_data, true);
            Yii::trace($raw_data);
            Yii::trace($incomingRequest);


//            / $subjectInfo = $incomingRequest['subjectId']; ///The $schoolInfo, ideally will correspond to a School details
            $teacherInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details
            $submitterInfo = $incomingRequest['teacherId']; //The $schoolInfo, ideally will correspond to a School details


            $teacher = User::findOne($teacherInfo);

            Yii::trace($teacher->username);
            if (!$teacher)
                throw new Exception("Teacher Id deoesnot exist");


            $lastClockinRecord = TeacherDailyAttendance::find()
                ->where(['teacher_id'=>$teacherInfo])
                ->orderBy(['date_created'=>SORT_DESC])
                ->limit(1)->one();

            Yii::trace($lastClockinRecord);

            $connection = Yii::$app->db;
            if(!$lastClockinRecord || !$lastClockinRecord->is_clockin){
// $transaction = $connection->beginTransaction();
                $sql2 = "INSERT INTO teacher_clockin_attendance (
rollcalled_by,
clock_in,
teacher_id,
is_clockin,
school_id
)
VALUES (
:rollcalled_by,
NOW() ,
:teacher_id,
true,
:school_id
)";
                $fileQuery = $connection->createCommand($sql2);
                $fileQuery->bindValue(':rollcalled_by', $submitterInfo);
                $fileQuery->bindValue(':teacher_id', $teacherInfo);
                $fileQuery->bindValue(':school_id', $teacher->school_id);
//Insert file
                $fileQuery->execute();
                $msg ="Successfully Clocked In ";
                $clockStatus = 'clockin';


            }
            else {
                $recordId=$lastClockinRecord->id;

                Yii::trace($lastClockinRecord->id);

                $sql = "UPDATE teacher_clockin_attendance set is_clockin = false,clock_out= NOW() where id = :recordId " ;

                $numberOfStudentsMoved = $connection->createCommand($sql)
                    ->bindValue(':recordId', $recordId)
                    ->execute();
                $msg =" Successfully Clocked Out";
                $clockStatus = 'clockout';

            }

            Logs::logEvent("Rollcalled teacher for : " .$teacherInfo, null, null);


            $transaction->commit();
            return json_encode([
                'returncode' => 0,
                'returnMessage' => $msg,
                'clockStatus'=>$clockStatus
            ]);

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Logs::logEvent("Error on posting teacher rollcall details: ", $error, null);

            return json_encode([
                'returncode' => 909,
                'returnMessage' => $e->getMessage(),
            ]);

        }}

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionChangePassword()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $new_pass = $incomingRequest['User']['new_password'];
        if($incomingRequest){
            $request = Yii::$app->request;
            $id = $incomingRequest['User']['user_id'];
            $model=$this->findModel($id);
            $model->scenario = 'change';

            try {
                $data = Yii::$app->request->post();

                $user = $model->find()->where(['id'=>$id])->limit(1)->one();

                if(!$user->validatePassword($incomingRequest['User']['current_password'])) {
                    throw new ForbiddenHttpException('The current password you entered is wrong');
                }

                $model->setPassword($incomingRequest['User']['new_password']);

                if($model->save(false)){
                    $transaction->commit();

                    //Send email to user on reset
                    $fullname = $model->getFullname();
                    $emailSubject = 'Your password for schoolsuite mobile has been reset | ' . $fullname;
                    $emailText = "Hello $fullname\n

            Greetings from  Awash ESchool!\n
            Your password has been reset. \n
            New Password: $new_pass\n
            If you did not reset your password, please contact our support team by email: contactcenter@awashbank.com
            Cheers.";
                    ToWords::sendEmail($model->email,
                        $emailSubject,
                        $emailText);

                    return [
                        'returncode' => 0,
                        'returnmessage' => "Your password has been successfully changed",
                    ];
                }
            }
            catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Change password ROLLBACK');
                return [
                    'returncode' => 909,
                    'returnmessage' => $e->getMessage(),
                ];
            }


        } else {
            throw new ForbiddenHttpException('No data received');
        }
    }

    public function actionResetPassword(){
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;

        if($incomingRequest){
            $username = $incomingRequest['User']['username'];
            $user = User::find()->where(['username'=>trim(strtolower($username))])->limit(1)->one();
            try{
                if($user) {
                    $password = $user->randomPassword();
                    $user->setPassword($password);

                    if ($user->save(false)){
                        $transaction->commit();

                        \Yii::$app->mailer->compose()
                            ->setTo($user->email)
                            ->setFrom(['support@schoolsuite.ug' => "Schoolsuite Mobile"])
                            ->setSubject('SCHOOLSUITE MOBILE PASSWORD RESET')
                            ->setTextBody("You successfully reset your password \n Username: " . $user->username . "\n Password: " . $password)
                            ->send();
                        return [
                            'returncode' => 0,
                            'returnmessage' => "Success. Your new password has been sent to your email",
                        ];
                    }
                } else{
                    throw new ForbiddenHttpException('User does not exist. Failed to reset your password.');
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Reset password ROLLBACK');
                return [
                    'returncode' => 909,
                    'returnmessage' => $e->getMessage(),
                ];
            }
        }
        throw new ForbiddenHttpException('No data received');
    }

    public function actionGetPayments(){
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $today = date('Y-m-d', time()+86400);
        $date_to = $incomingRequest['date_to'] ? $incomingRequest['date_to'] : $today;
         $sent_date_from = $incomingRequest['date_from'];
          $date_from = date('Y-m-d', strtotime($date_to . ' -30 days'));
        if($incomingRequest){

            try{

                $query = (new Query())
                    ->select(['th.id', 'th.date_created', 'th.description', 'th.trans_type', 'cl.class_code', 'cl.class_description',
                        'pr.reciept_number', 'pr.channel_trans_id', 'th.payment_id', 'th.reversed', 'pc.channel_code',
                        'pc.channel_name', 'pr.channel_memo', 'th.amount', 'th.balance_after', 'pc.payment_channel_logo',
                        'sinfo.student_code as payment_code', 'sinfo.school_student_registration_number as registration_number'
                    ])
                    ->from('school_account_transaction_history th')
                    ->innerJoin('payments_received pr', 'pr.id=th.payment_id')
                    ->innerJoin('core_student sinfo', 'sinfo.id=pr.student_id')
                    ->innerJoin('core_school_class cl', 'cl.id=sinfo.class_id')
                    ->leftJoin('auto_settlement_requests asr', 'pr.id=asr.payment_id') //LEft join mm payments
                    ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel');
//                    ->andWhere(['sinfo.student_code'=>$incomingRequest['student_code']])->all();

                      $query->andFilterWhere(['sinfo.student_code'=>$incomingRequest['student_code']]);
                    /*  $query->andFilterWhere(['pr.payment_channel' => $incomingRequest['payment_id'] ]);
        $query->andFilterWhere(['pr.channel_trans_id' => $incomingRequest['transaction_id'] ]); */
        //Disable reversed and reversal transactions from the search
        $query->andFilterWhere(['pr.reversal' => false ]);
        $query->andFilterWhere(['pr.reversed' => false ]);
        if($incomingRequest['payment_id']){
         $query->andFilterWhere(['pr.payment_channel' => $incomingRequest['payment_id']]);
        }

         if($incomingRequest['transaction_id']){
                 $query->andFilterWhere(['pr.channel_trans_id' => $incomingRequest['transaction_id']]);
                }
         if($incomingRequest['date_from']){
            $query->andWhere("pr.date_created::date>='$sent_date_from'")->all();
            if($date_to) $query->andWhere("pr.date_created::date<='$date_to'")->all();
        }
                if ($query){
                    return [
                        'returnCode' => 0,
                        'payments' => $query->all(),
                        'returnMessage' => "Success.",
                    ];
                }
                else {
                    throw new ForbiddenHttpException('No data payment history found for given payment code');
                }

            }
            catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Reset password ROLLBACK');
                return [
                    'returnCode' => 909,
                    'returnMessage' => $e->getMessage(),
                ];
            }
        }
        throw new ForbiddenHttpException('No data received');
    }


    public function actionGetLogo(){

        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);

            try{
                $logoDetails = ImageBank::findOne(['id'=>$incomingRequest['logo_id']]);
                if ($logoDetails) {
                    return [
                        'returnCode' => 0,
                        'returnMessage' => "Got it",
                        'logo'=>$logoDetails->image_base64
                    ];
                } else {
                    throw new ForbiddenHttpException('No logo found');
                }


            } catch (\Exception $e){
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Reset password ROLLBACK');
                return [
                    'returnCode' => 909,
                    'returnMessage' => $e->getMessage(),
                ];
            }




    }

    public function actionStudentLogin(){
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try{
            $student = CoreStudent::findOne(['student_code'=>$incomingRequest['studentCode']]);
            if($student){
                if($student->archived){
                    throw new ForbiddenHttpException('This student is archived. Please contact administrator');

                }


                /*payments info*/
                $sumPayable = (new \yii\db\Query())
                    ->from('institution_fee_student_association fs')
                    ->join('join', 'institution_fees_due fd', 'fd.id=fs.fee_id')
                    ->where('fs.student_id=' . $student->id . 'AND fs.applied =true AND fd.end_date >= NOW() AND fd.effective_date <= NOW()')
                    ->sum('fd.due_amount');

                $balance = StudentAccountGl::findOne(['id'=>$student->student_account_id]);

                $schId = $student['school_id'];
                $schData = CoreSchool::findOne(['id'=>$schId]);
                $classInfo= CoreSchoolClass::findOne(['id'=>$student['class_id']]);
                $schLogo = null;
                if ($schData->school_logo){
                    $schLogo = ImageBank::findOne(['id'=>$schData->school_logo])->image_base64;
                }

                $stddata =[

                    'first_name'=>$student->first_name,
                    'last_name'=>$student->last_name,
                    'student_code'=>$student->student_code,
                    'email_address'=>$student->student_email,
                    'verification_token'=>$student->verification_token,
                    'class_name'=>$classInfo->class_description,
                    'class_code'=>$classInfo->class_code,
                    'class_id'=>$student->class_id,
                    'id'=>$student->id,
                    'gender'=>$student->gender,
                    'payableSum'=>$sumPayable,
                    'account_balance'=>$balance->account_balance,
                    'outstanding_balance'=>$balance->outstanding_balance

                ];

                $schInfo =[
                    'school_name'=>$schData->school_name,
                    'school_id'=>$schData->id,
                    'school_location'=>$schData->village,
                    'school_email'=>$schData->contact_email_1,
                    'school_logo'=>$schLogo
                ];


                return json_encode([
                    'returnCode' => 0,
                    'returnMessage' => "SUCCESSFUL, student loggedin",
                    'stdInfo'=>$stddata,
                    'schInfo'=>$schInfo
                ]);

            }
            else{

                throw new ForbiddenHttpException('No student found with the given student code');
            }

        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            return json_encode([
                'returnCode' => 909,
//                'returnMessage' => $e->getMessage(),
                'returnMessage' => "Invalid student code",
            ]);

        }

    }

    public function actionGetStudent()
    {
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

            try {
                $content = null;
                $stdTxn = Yii::$app->db->createCommand(' SELECT si.id, si.student_code as payment_code, si.first_name, si.last_name,gl.outstanding_balance, cls.class_code, sch.school_name, si.student_email, si.student_phone,  si.first_name, si.middle_name,  si.last_name, si.school_student_registration_number FROM core_student si INNER JOIN core_school_class cls ON cls.id=si.class_id INNER JOIN core_school sch ON sch.id=cls.school_id
                    INNER JOIN student_account_gl gl on gl.id = si.student_account_id
                    WHERE ((si.archived=FALSE)  AND (student_code= :code))
                   OR (schoolpay_paymentcode=:code)
                   ORDER BY si.student_code LIMIT 20')
                    ->bindValue(':code', $incomingRequest['student_code'])
                    ->queryAll();

                if ($stdTxn) {


                    $content = $stdTxn[0];
                }

                Yii::trace($content);

                if ($content) {
                    return json_encode([
                        'returnCode' => 0,
                        'returnMessage' => "SUCCESSFUL, student found",
                        'stdInfo'=>$content,
                    ]);

                }
                throw new ForbiddenHttpException('The student number doesn’t exist');
            } catch (\Exception $e){
                Yii::trace($e);

                return json_encode([
                    'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
//                    'returnMessage' => "Service temporarily unavailable, please try again later",
                ]);
            }


    }

    public function actionGetChannels(){
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();

        $channels = PaymentChannels::find()->where(['in', 'channel_code', ['AIRTEL_MONEY_UG', 'MTN_UG']])->all();
            try{
                if($channels) {

                        return [
                            'returnCode' => 0,
                            'channels' => $channels,
                            'returnMessage' => "Success",
                        ];

                } else{
                    throw new ForbiddenHttpException('No payment channels available');
                }

            } catch (\Exception $e) {

                return [
                    'returncode' => 909,
                    'returnmessage' => $e->getMessage(),
                ];
            }
    }


 public function actionAllChannels(){
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();

        $channels = PaymentChannels::find()->all();
            try{
                if($channels) {

                        return [
                            'returnCode' => 0,
                            'channels' => $channels,
                            'returnMessage' => "Success",
                        ];

                } else{
                    throw new ForbiddenHttpException('No payment channels available');
                }

            } catch (\Exception $e) {

                return [
                    'returncode' => 909,
                    'returnmessage' => $e->getMessage(),
                ];
            }
    }

    public function actionConfirmPayment()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

            try {
                $client = new Client();
                $baseUrl = Yii::getAlias('@mtnPaymentApi');

                $url = "{$baseUrl}";
                //$url = "{$baseUrl}/{$chn->channel_code}/{$pay['phone_number']}/{$param}/{$pay['amount']}";
                Yii::trace($url);
                $response = $client->createRequest()
                    ->setHeaders(['content-Type : application/x-www-form-urlencoded' ])
                    ->setUrl($url)
                    ->setMethod('POST')
                    ->setData(['channelCode' =>$incomingRequest['channel_code'], 'phoneNumber' => $incomingRequest['phone_number'],'studentReference'=>$incomingRequest['payment_code'],'amount'=>$incomingRequest['amount']])
                    ->send();
                Yii::trace($response);
                $content = json_decode($response->getContent());
                if ($content->returnCode != 0) {
                    return [
                        'returnCode' => 909,
                        'returnMessage' => $content->returnMessage];
                }
                $data = $content->returnObject;

                $processingNumber = ($incomingRequest['channel_code'] == 'AIRTEL_MONEY_UG') ?
                    $data->airtelProcessingNumber : $data->mtnProcessingNumber;

                return [
                    'returnCode' => 0,
                    'mpn' => $processingNumber,
                    'chn'=>$incomingRequest['channel_code']
                ];

            } catch (\Exception $e) {
//                  return ['code' => 101, 'returnMessage' => $e->getMessage()];
                 return ['code' => 101, 'returnMessage' => "An error occurred while processing your payment. PLease try again later  "];
        }
    }


    public function actionGetGrades()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {
            $query = (new Query())
                ->select(['cg.id', 'cg.grades', 'cg.min_mark','cg.comment', 'cg.max_mark','csc.school_name','cc.class_description as class','cc.class_code'
                ])
                ->from('core_grades cg')
                ->innerJoin('core_school_class cc', 'cg.class_id= cc.id')
                ->innerJoin('core_school csc', 'cg.school_id= csc.id')
                ->andWhere(['cg.class_id'=>$incomingRequest['classId']]);
//            $query->all();

            return [
                'returnCode' => 0,
                'grades' => $query->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }


    public function actionGetCirculars()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {
            $query1 = (new Query())
                ->select(['sc.circular_name','sc.id','sc.circular_subject', 'ct.term_name', 'csc.school_name','sc.school_id'])
                    ->from('core_school_circular sc')
                    ->innerJoin('core_term ct', 'sc.term_id= ct.id')
                    ->innerJoin('core_school csc', 'sc.school_id= csc.id')
                    ->Where(['sc.circular_type' => 'Generic'])
                ->andWhere(['sc.school_id' => $incomingRequest['schoolId']]);
                $query1->orderBy('sc.id desc');

            $query2 = (new Query());
            $query2->select(['sds.id', 'sds.circular_name', 'sds.circular_description', 'sds.upload_id',  'csc.school_name', 'sds.date_created', ])
                ->from('school_uploaded_circulars sds')
                ->innerJoin('core_school csc', 'sds.school_id= csc.id')
                ->andWhere(['sds.school_id' =>$incomingRequest['schoolId'] ]);
            $query2->orderBy('sds.id desc');

            return [
                'returnCode' => 0,
                'generic' => $query1->all(),
                'uploaded' => $query2->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionViewGenericCircular()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        $logo = null;
        try {
            $query = (new Query())->select([
                'sch.school_name', 'sch.school_logo', 'csc.circular_type','csc.signature_id', 'csc.school_stamp',
                "concat_ws(' ', sch.village, sch.district) as physical_address",'sds.individual_name','sds.individual_position',
                'csc.id', 'csc.school_id', 'sch.phone_contact_1', 'sch.contact_email_1', 'circular_ref_no', 'effective_date', 'circular_subject', 'the_body'
            ])
                ->from('core_school_circular csc')
                ->innerJoin('core_school sch', 'sch.id=csc.school_id')
                ->innerJoin('district dst', 'dst.id=sch.district')
                ->innerJoin('school_digital_signature sds', 'sds.id=csc.signature_id')
                ->where(['csc.id' => $incomingRequest['id']])
                ->limit(1)
                ->one();
            if (!$query) {
                throw new NotFoundHttpException('Circular could not generate. Try again later.');
            }

            $socialMedia = SchoolSocialMedia::findOne(['school_id' => $incomingRequest['schoolId']]);
            $sch_sign = $query['signature_id'];
            $signature=SignatureUploads::findOne(['submission_id' => $sch_sign])->file_data;

            if ($query['school_logo']){
                $logo= ImageBank::findOne(['id' => $query['school_logo']])->image_base64;
            }

            Yii::trace($signature);

            return [
                'returnCode' => 0,
                'genericCircular' => $query,
                'signature'=>$signature,
                'schLogo'=>$logo,
                'socialmedia' => $socialMedia
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionViewUploadedCircular()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {

            $circularFile=CircularUploads::findOne(['id' => $incomingRequest['uploadId']])->file_data;
            return [
                'returnCode' => 0,
                'circularFile' => $circularFile,
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionGetClassReports()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {

            $query = new Query();
            $query->select(['gr.id', 'report_name', 'report_description', 'gr.date_created', 'tm.term_name', 'gr.active'])
                ->from('report gr')
                ->innerJoin('core_term tm', 'tm.id=gr.term_id')
                ->andFilterWhere(['gr.school_id'=>$incomingRequest['schoolId']])
                ->andFilterWhere(['gr.class_id'=>$incomingRequest['classId']]);
            $query->orderBy('gr.id desc');
            return [
                'returnCode' => 0,
                'returnData' => $query->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionViewStudentReport()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);
        $output = [];
        $studentInfo = [];
        $grading = [];
        $subject_results_arry = [];
        $output_result = [];
//        $test_grouping_arry = [];

        try {


            $term = Yii::$app->db->createCommand('SELECT ct.term_name FROM report rp  inner join core_term ct on ct.id =rp.term_id WHERE rp.id =:reportId')
                ->bindValue(':reportId', $incomingRequest["termId"])
                ->queryOne();
            $staff = Yii::$app->db->createCommand('SELECT sf.first_name, sf.last_name, bk.image_base64 as signature FROM core_staff sf inner join 
    core_school_class cl on cl.class_teacher = sf.id inner join image_bank bk on bk.id = sf.signature WHERE cl.id =:classId')
                ->bindValue(':classId',$incomingRequest["classId"] )
                ->queryOne();
            $studentInfo = Yii::$app->db->createCommand('SELECT CS.first_name,CS.middle_name,CS.last_name, CS.student_code,CS.class_id, CC.class_description FROM core_student CS inner join core_school_class CC on CC.id =CS.class_id WHERE CS.id =:studentId')
                ->bindValue(':studentId', $incomingRequest["studentId"])
                ->queryOne();

            $schoolDetails = Yii::$app->db->createCommand(
                'SELECT c_s.*,r_g.description as region_name,d.district_name FROM core_school c_s 
                                   left join ref_region r_g on(c_s.region=r_g.id)  left join district d on(c_s.district=d.id)  WHERE c_s.id=:schoolId')
                ->bindValue(':schoolId', $incomingRequest['schoolId'])
                ->queryOne();

          /*  $grouping = (new Query())->select(['id'])
                ->from('test_grouping')
                ->all();
            foreach ($grouping as $key) {
                array_push($test_grouping_arry, $key['id']);
            }*/

            $testIdArray = (new Query())->select(['test_id'])
                ->from('report_subject_composition comp')
                ->innerJoin('report_subjects_association ass', 'ass.id = comp.report_subject')
                ->where(['ass.report_id' => $incomingRequest["reportId"]])
                ->all();

            $theTestIds = [];
            foreach ($testIdArray as $ki => $vi) {

                array_push($theTestIds, $vi['test_id']);
            }

            $results = (new Query())->select(['subject_name', 'subject_code', 'subject_id', '((sum(marks_obtained))/COUNT(marks_obtained)) AS "score_avg"'])
                ->from('core_marks cm')
                ->innerJoin('core_test ct', 'ct.id = cm.test_id')
                ->innerJoin('core_subject cs', ' cs.id = ct.subject_id')
                ->innerJoin('core_assessment_type ast', ' ast.id =ct.assessment_id')
//                ->innerJoin('test_grouping t_group', ' t_group.id =ct.test_grouping_id')
                ->innerJoin('report_subject_composition rsc', ' rsc.test_id = ct.id')
                ->where(['student_id' => $incomingRequest["studentId"]])
//                ->andWhere(['in', 'test_grouping_id', $test_grouping_arry])
                ->andWhere(['in', 'cm.test_id', $theTestIds])
                ->groupBy(["subject_name", "subject_id", "subject_code"])
                ->all();
            Yii::trace($results);
            //add only the subjects to the array
            foreach ($results as $result) {
                if (!in_array($result['subject_code'], $subject_results_arry)) {
                    array_push($subject_results_arry, $result['subject_code']);
                }
            }
            //now filter the data per subject
            foreach ($subject_results_arry as $subject) {
                $records = array_filter($results, function ($row) use ($subject) {
                    return $row['subject_code'] === $subject;
                });
                $out_subject = array();
                $subject_id = null;
                $subject_name = null;
                foreach ($records as $record) {
                    $out_subject[] = array( "score_avg" => floor($record['score_avg']));
                    $subject_id = $record['subject_id'];
                    $subject_name = $record['subject_name'];
                }
                $output_result[] = array("subject_code" => $subject, "subject_name" => $subject_name, "subject_id" => $subject_id, "results" => $out_subject);
                unset($out_subject);
            }

            Yii::trace($output_result);

            $grading = Yii::$app->db->createCommand('SELECT * FROM core_grades WHERE class_id=:classId order by max_mark desc')
                ->bindValue(':classId', $studentInfo['class_id'])
                ->queryAll();
//                    Yii::trace($grading);
            if ($output_result) {
                array_push($output, ['results' => $output_result, 'term'=>$term, 'schoolDetails' => $schoolDetails, 'studentInfo' => $studentInfo, 'grading' => $grading, 'staffInfo'=>$staff]);
                unset($output_result);
            }

            return [
                'returnCode' => 0,
                'returnData' => $output,
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionGetClassTests()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {

            $query = (new Query())
                ->select(['ct.id','sb.subject_name','ct.test_description', 'ct.start_date','ct.end_date','ctm.term_description', 'csc.school_name','cl.class_description' ,'sb.subject_name', 'ct.total_marks','ct.date_created',])
                ->from('core_test ct')
                ->innerJoin('core_school csc', 'ct.school_id= csc.id')
                ->innerJoin('core_school_class cl', 'ct.class_id= cl.id')
                ->innerJoin('core_subject sb', 'ct.subject_id= sb.id')
                ->innerJoin('core_term ctm', 'ct.term_id= ctm.id')
                ->innerJoin('core_marks mks', 'ct.id= mks.test_id')
                ->andFilterWhere(['csc.id'=>$incomingRequest['schId']])
                ->andFilterWhere(['cl.id'=>$incomingRequest['classId']]);
            $query->orderBy('ct.id desc');
            //$query->having('count(mks.test_id)>0');
            if ($incomingRequest['termId']){
                $query->andFilterWhere(['ctm.id'=>$incomingRequest['termId']]);
            }
            $query->groupBy(['ct.id','sb.subject_name','ct.test_description', 'ct.start_date','end_date','ctm.term_description', 'csc.school_name','cl.class_description' ,'sb.subject_name', 'ct.total_marks','ct.date_created']);
            return [
                'returnCode' => 0,
                'returnData' => $query->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionViewTestMarks()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {
            $query = (new Query())
                ->select(['cm.id','cs.student_code','cs.first_name', 'cs.last_name','cs.middle_name','cc.class_description','cm.marks_obtained', ])
                ->from('core_marks cm')
                ->innerJoin('core_student cs', 'cm.student_id =cs.id')
                ->innerJoin('core_school_class cc', 'cc.id= cs.class_id')
                ->innerJoin('core_test ct', 'ct.id = cm.test_id')
                ->where(['cm.test_id'=>$incomingRequest['testId']])
                ->andWhere(['cs.id'=>$incomingRequest['studentId']]);
            $query->orderBy('cm.id desc');
            return [
                'returnCode' => 0,
                'returnData' => $query->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }


    public function actionGetTerms()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {
            $query = (new Query())
                ->select(['cm.id','cm.term_name'])
                ->from('core_term cm')
                ->where(['cm.school_id'=>$incomingRequest['schoolId']]);
            $query->orderBy('cm.id desc');
            return [
                'returnCode' => 0,
                'returnData' => $query->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    /**
     * Fetch the  saved colors for the timetable schedule
     * @param integer $colorId
     * @return mixed
     */
    private function fetchColorForTimetableSchedule($colorId){

        $finalColor = "";
        $connection = Yii::$app->db;

        //check if the project has any saved color codes.
        $sqlQuery = "SELECT * FROM timetable_colors WHERE id=:id";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':id', $colorId);
        $colorData = $sqlQuery->queryOne();

        if (!empty($colorData)) {
            $finalColor = $colorData['color'];

        }else{
            $randomColor = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $finalColor = $randomColor;
        }

        return $finalColor;
    }

    public function actionGetTimeTableIndex()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

        try {
            $query = Timetable::find()
                ->where(['school_id'=> $incomingRequest['schoolId'] , 'active'=> true ]);
            if ($incomingRequest['termId']){
                $query->andFilterWhere(['term_id'=>$incomingRequest['termId']]);
            }
            return [
                'returnCode' => 0,
                'returnData' => $query->all(),
            ];

        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }

    public function actionLoadTimeTableSchedules()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);


        $outResponse = [
            'success' => false,
            'message' => 'Failed to retrieve the timetable schedules data for the timetable'
        ];
        try {

        $request = Yii::$app->request;

        $timetableId = $incomingRequest['timetableId'];
        $selectedClass = $incomingRequest['selectedClass'];
        $timetableType = Timetable::findOne($timetableId);


        if ($selectedClass == 'all-classes') {
            $timetableSchedules = TimetableSchedule::find()
                ->where(['timetable_id' => $timetableId, 'active'=> true ])
                ->orderBy('id')
                ->asArray()
                ->all();
        } else {

            $timetableSchedules = TimetableSchedule::find()
                ->where([
                    'timetable_id' => $timetableId,
                    'active'=> true,
                    'selected_class'=> $selectedClass

                ])
                ->orderBy('id')
                ->asArray()
                ->all();

        }

        $resultData = array();

        if ($incomingRequest['timetableType']== "lesson"){
            foreach ($timetableSchedules as $schedule) {
                $selectedColor = $this->fetchColorForTimetableSchedule($schedule['color']);
                $teacher = CoreStaff::findOne($schedule['selected_teacher']);
                array_push($resultData, [
                    'id' => $schedule['id'],
                    'title' => $schedule['title'],
                    'courseId' => $schedule['title'],
                    'category' => $schedule['category'],
                    'start' => $schedule['starts'],
                    'end' => $schedule['ends'],
                    'startTime' => date('H:i', strtotime($schedule['starts'])),
                    'endTime' => date('H:i', strtotime($schedule['ends'])),
                    //'color' => $schedule['color'],
//                'color' => $selectedColor,
                    'color' => '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT),
                    'day' => date('N', strtotime($schedule['starts'])),
                    'description' => $schedule['description'],
                    'location' => $teacher->first_name. ' '.$teacher->last_name,
                    'meeting_link' => $schedule['meeting_link_id'],
                    'mailing_list_id' => $schedule['mailing_list_id']
                ]);
            }
        } else if ($incomingRequest['timetableType']== "examination"){
            foreach ($timetableSchedules as $schedule) {
                array_push($resultData, [
                    'id' => $schedule['id'],
                    'title' => $schedule['title'],
                    'start' => $schedule['starts'],
                    'end' => $schedule['ends'],
                    'summary' => $schedule['description'],
                ]);
            }
        }


            return [
            'returnCode' => 0,
//            'timetableType' => $timetableType->calendar_category,
            'returnData' => $resultData
        ];


        } catch (\Exception $e) {
            return [
                'returnCode' => 909,
                'returnMessage' => $e->getMessage(),
            ];
        }
    }
}

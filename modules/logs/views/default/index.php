<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClassesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Logs';
?>


<div class="row">
    <h3 class="box-title" style="color: #000"><i class="fa fa-history"></i> <?php echo $this->title ?></h3>
    <?php echo $this->render('_search', ['searchModel' => $searchModel]); ?>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th class='clink'><?= $sort->link('event_date') ?></th>
                    <th class='clink'><?= $sort->link('ip_address') ?></th>
                    <th class='clink'><?= $sort->link('user_name') ?></th>
                    <th class='clink'><?= $sort->link('web_user_id') ?></th>
                    <th class='clink'>Action ID</th>
                    <th class='clink'><?= $sort->link('event_action') ?></th>
                    <th class='clink'><?= $sort->link('errors') ?></th>
                    <th class='clink'><?= $sort->link('affected_student_id') ?></th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($dataProvider) :
                    foreach ($dataProvider as $k => $v) : ?>
                        <tr <?= $v['errors'] ? 'style="color:red"' : ''; ?>>
                            <td><?= ($v['event_date']) ? date('M d, Y - g:i:s A', strtotime($v['event_date'])) : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['ip_address']) ? $v['ip_address'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['user_name']) ? $v['user_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['web_user_id']) ? $v['web_user_id'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['event_action']) ? $v['event_action'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['event_action_id']) ? $v['event_action_id'] : '-' ?></td>
                            <td><?= ($v['errors']) ? $v['errors'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['affected_student_id']) ? $v['affected_student_id'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No records found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]);?>

    </div>
</div>

<?php

use app\modules\logs\models\Logs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\ActiveForm;
use app\models\User;
use yii\jui\DatePicker;

?>




<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>
<div class="row text-center">
    <div class="row mt-3">
        <div class="col ml-lg-5">
            <?= DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'event_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'Date From'],
                'clientOptions' => [
                    'class' => 'form-control',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1900:' . (date('Y') + 1),
                    'autoSize' => true,
                ],
            ]); ?>
        </div>
        <div class="col ml-0">
            <?= $form->field($searchModel, 'user_name', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'User Name']])->textInput()->label(false) ?>

        </div>  <div class="col ml-0">
            <?= $form->field($searchModel, 'ip_address', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'IP Address']])->textInput()->label(false) ?>

        </div>

        <div class="col">
            <?= $form->field($searchModel, 'event_action_id', ['inputOptions' => ['class' => 'form-control input-sm',
                'placeholder' => 'Action ID',]
            ])->dropDownList(
                [Logs::ADJUST_BALANCE => "Student Balance Adjustment",
                ],
                ['id' => 'action_id_select', 'prompt' => 'Action ID']
            )->label(false) ?>
        </div>

        <div><?= Html::submitButton('Search <i class=" fa fa-search"></i>', ['class' => 'btn btn-info btn-md']) ?></div>
    </div>
</div>
<?php ActiveForm::end(); ?>




<?php

namespace app\modules\logs\models;


use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;


/**
 * This is the model class for table "nominated_bank_details".
 *
 * @property integer $id
 * @property string $errors
 * @property string $event_action
 * @property string $event_date
 * @property string $ip_address
 * @property string $user_name
 * @property string $event_action_id
 * @property integer $web_user_id
 * @property integer $affected_student_id
 */
class Logs extends \yii\db\ActiveRecord
{
    const ADJUST_BALANCE = "ADJUST_BALANCE";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'web_console_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['errors', 'event_action', 'event_date', 'ip_address', 'user_name', 'event_action_id'], 'string'],
            [['web_user_id', 'affected_student_id'], 'integer'],
            [['event_date', 'user_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'errors' => 'Error',
            'event_date' => 'Event Date',
            'event_action' => 'Event Action',
            'ip_address' => 'Ip Address',
            'web_user_id' => 'User Id',
            'affected_student_id'=>'Affected Student',
            'user_name'=>'User Name',
            ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if(!Yii::$app->user->isGuest){
                $this->web_user_id = Yii::$app->user->identity->id;
                $this->user_name = Yii::$app->user->identity->username;
            }
            $this->ip_address = Yii::$app->request->userIP;
            $this->event_date = new Expression('NOW()');
            return true;
        } else {
            return false;
        }
    }

    public static function logEvent($event, $error, $affected_student_id = null){
        try {
        $log = new Logs();
        $log->event_action = $event;
        $log->errors = $error ? $error : null;
        $log->affected_student_id = $affected_student_id ? $affected_student_id : null;
            if(\app\components\ToWords::isSchoolUser()){
                $log->school_id = Yii::$app->user->identity->school_id;
            }
        $log->save();
        }catch (\Exception $e){
            Yii::error($e);
        }
    }

    public static function logActionEvent($event, $error, $action_id, $affected_student_id = null){
        try {
            $log = new Logs();
            $log->event_action = $event;
            $log->errors = $error ? $error : null;
            $log->affected_student_id = $affected_student_id ? $affected_student_id : null;
            $log->event_action_id = $action_id;
            $log->save();
        }catch (\Exception $e){
            Yii::error($e);
            Yii::trace($e);
        }

    }

    public static function logFailedLogin($username, $event, $error){
        $log = new Logs();
        $log->user_name = $username;
        $log->event_action = $event;
        $log->errors = $error;
        $log->save();
    }

    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['event_date', 'ip_address', 'user_name', 'event_action', 'web_user_id', 'errors', 'affected_student_id', 'event_action_id'])
              ->from('web_console_log')
              ->filterWhere(['event_action_id'=>$this->event_action_id])
              ->andFilterWhere(['between', 'event_date', $this->event_date, date('Y-m-d', strtotime('tomorrow'))])
              ->andFilterWhere(['ilike', 'user_name', $this->user_name])
              ->andFilterWhere(['ilike', 'ip_address', $this->ip_address]);

        if (\app\components\ToWords::isSchoolUser()) {

            $query->andWhere(['school_id' => Yii::$app->user->identity->school_id]);

        }
        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'event_date', 'ip_address', 'user_name', 'event_action', 'web_user_id', 'errors', 'affected_student_id'
                ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('event_date DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

}

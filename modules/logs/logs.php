<?php

namespace app\modules\logs;

class logs extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\logs\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
<?php

namespace app\modules\logs\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use app\modules\logs\models\Logs;

class DefaultController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
//    	if(Yii::$app->user->can('view_logs')){
        $request = Yii::$app->request;
        $searchModel = new Logs();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages'=>$data['pages'], 'page'=>$data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel,'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ]) : 
            $this->render('index', ['searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ]);
//        } else {
//        	throw new ForbiddenHttpException('No permissions to view students.');
//        }
    }


}

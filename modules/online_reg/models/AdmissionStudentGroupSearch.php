<?php

namespace app\modules\online_reg\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

/**
 * StudentGroupSearch represents the model behind the search form about `app\models\StudentGroupInformation`.
 */
class AdmissionStudentGroupSearch extends AdmissionGroupInformation
{

    public $gender, $student_class, $searchTerm, $day_boarding, $campus_id , $the_level, $class_id, $min_agg, $max_agg, $modelSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id','min_agg','max_agg'], 'integer'],
            [['school_id', 'group_name', 'group_description','modelSearch','the_level','class_id', 'date_created', 'date_modified', 'gender','student_class', 'searchTerm', 'day_boarding', 'campus_id'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['gr.id', 'group_name', 'group_description', 'gr.date_created', 'gr.active', 'sch.school_name'])
              ->from('admissions_student_group gr')
              ->innerJoin('core_school sch', 'sch.id=gr.school_id')
              ->andFilterWhere(['gr.school_id'=>$this->school_id]);
        if(\app\components\ToWords::isSchoolUser()){
            $query->where(['gr.school_id'=>Yii::$app->user->identity->school_id]);
        }
        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'group_name', 'group_description', 'active', 'school_name' ]
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public function findStudents($grp, $params)
    {
        $this->load($params);
        Yii::trace($this->the_level);
        $query1 = StudentAdmissionGroupStudent::find()->select(['student_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['si.id', 'si.first_name', 'si.last_name', 'si.middle_name', 'si.gender', 'cls.class_code',
            'si.ple_aggregates','si.uce_aggregates'])
            ->from('std_online_registration si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id');



        if (!(empty($this->min_agg))  && ($this->the_level == 'OLEVEL')) {
            Yii::trace($this->the_level);
            $query->andWhere(['between', 'si.ple_aggregates', ($this->min_agg+0), ($this->max_agg +1)]);


        }else {
            if (!(empty($this->min_agg)) && ($this->the_level == 'ALEVEL')) {
                $query->andWhere(['between', 'si.uce_aggregates', ($this->min_agg+0), ($this->max_agg + 1)]);

            }
        }
        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }


        if (!(empty($this->class_id))) {

            $query->andWhere(['class_id' => $this->class_id]);

        }


        if(!(empty($params['searchTerm']))){
            $searchTerm = $params['searchTerm'];
            if (is_numeric($searchTerm)) {
                $query->andWhere(['student_code' => $searchTerm]);
            }
            else
            {
                $search_words = explode(' ', $searchTerm);
                $search_words = array_filter($search_words);
                $words = array();
                  if(count($search_words) > 1) {
                    foreach($search_words as $word) {
                        $words[] = $word;
                    }
                        if(isset($words[0])){
                            $name = str_replace(' ', '', $words[0]);
                            $query->andFilterWhere(['or',
                                    ['ilike', 'first_name', $name],
                                    ['ilike', 'middle_name', $name],
                                    ['ilike', 'last_name', $name]
                                    ]);
                        }
                        if(isset($words[1])){
                            $name = str_replace(' ', '', $words[1]);
                            $query->andFilterWhere(['or',
                                    ['ilike', 'first_name', $name],
                                    ['ilike', 'middle_name', $name],
                                    ['ilike', 'last_name', $name]
                                    ]);
                        }
                        if(isset($words[2])){
                            $name = str_replace(' ', '', $words[3]);
                            $query->andFilterWhere(['or',
                                    ['ilike', 'first_name', $name],
                                    ['ilike', 'middle_name', $name],
                                    ['ilike', 'last_name', $name]
                                    ]);
                        }

                    }
                    else{
                        $name = str_replace(' ', '', $searchTerm);
                        $query->andFilterWhere(['or',
                                ['ilike', 'first_name', $name],
                                ['ilike', 'middle_name', $name],
                                ['ilike', 'last_name', $name],

                            ]);
                        }
                }
            }

            $query->orderBy('cls.class_code', 'si.last_name');
            $query->limit(500);
            return $query->all();

    }

    public static function findMembers($id){
        $query = (new Query())->select(['si.id',  'si.first_name', 'si.last_name', 'si.middle_name', 'si.gender', 'cls.class_code','si.ple_aggregates','si.uce_aggregates'])
            ->from('student_admissions_student_group sgr')
            ->innerJoin('std_online_registration si', 'si.id=sgr.student_id')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andWhere(['sgr.group_id'=>$id])
            ->orderBy('cls.class_code', 'si.last_name');
         return $query->all();
    }

    public static function getExportQuery()
    {
    $data = [
            'data'=>$_SESSION['findData'],
            'fileName'=>'StudentGroup',
            'title'=>'Grouped Student Datasheet',
            'exportFile'=>'/core-student-group/exportPdfExcel',
        ];

    return $data;
    }
}

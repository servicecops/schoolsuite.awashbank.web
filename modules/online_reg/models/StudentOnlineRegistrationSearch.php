<?php

namespace app\modules\online_reg\models;

use app\modules\gradingcore\models\Grading;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class StudentOnlineRegistrationSearch extends StudentOnlineRegistration
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['date_created', 'created_by'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.firstname,ct.lastname, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('"user" ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' )
            ->innerJoin('core_school  sch','sch.id =ts.school_id' )
            ->where(['ts.class_id'=>$params]);

        $query->orderBy('ts.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach($data as $k =>$v){
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'sb.id', $the_subjectId]);
                $query->andwhere(['ts.teacher_id'=> Yii::$app->user->identity->getId()]);

        }
      


        $command = $query->createCommand();
        $data = $command->queryAll();

       $searchModel = new StudentOnlineRegistrationSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
       return ['data'=>$data, 'searchModel' => $searchModel];
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.first_name,ct.last_name, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('core_teacher ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' );

        $query->orderBy('ts.id desc');

        $command = $query->createCommand();
        $data = $command->queryAll();

        $searchModel = new StudentOnlineRegistrationSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];


    }
    public function searchSubject($params)
    {
        $teacherId =Yii::$app->user->identity->getId();

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/create-lesson', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Add Lesson', ['elearning/create-lesson', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
    public function searchTeachersSubject($params)
    {
        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/all-class-subject-work', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-class-subject-work', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchTeachersSubjectSubmission($params)
    {

        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/all-student-index', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-student-index', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchTeacher($params)
    {

        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.firstname,ct.lastname, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('"user" ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' )
            ->innerJoin('core_school  sch','sch.id =ts.school_id' )
            ->where(['ts.subject_id'=>$params]);

        $query->orderBy('ts.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach($data as $k =>$v){
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            // $query->andWhere(['tca.class_id' => 18]);
            $query->andwhere(['in', 'sb.id', $the_subjectId]);
            $query->andwhere(['ts.teacher_id'=> Yii::$app->user->identity->getId()]);

        }



        $command = $query->createCommand();
        $data = $command->queryAll();

        $searchModel = new StudentOnlineRegistrationSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];
    }

    public function searchPaid($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name',
            'si.gender', 'si.ple_index_number','si.uce_aggregates','si.ple_aggregates',
            'si.ple_results', 'si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref','si.class_id'
        ])
            ->from('std_online_registration si')
            ->where(['si.online_registration_payment_status'=>'PAID']);

        $query->andWhere(['si.school_id' => Yii::$app->user->identity->school_id]);
        $query->andWhere(['si.application_status' => 'REGISTERED']);
        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code',  'guardian_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }
    public function searchUnpaid($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','former_school_name','si.middle_name','si.channel_name', 'si.first_name', 'si.last_name', 'si.gender', 'si.ple_index_number',
            'si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee','si.uce_aggregates','si.ple_aggregates',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
            ->from('std_online_registration si')
            ->where(['si.online_registration_payment_status'=>'PENDING']);


        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code', 'guardian_email', 'guardian_phone',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function searchPaidAdmission($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name', 'si.gender', 'si.ple_index_number',
            'si.uce_aggregates','si.ple_aggregates', 'si.uce_index_number','si.ple_index_number','si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
            ->from('std_online_registration si')
            ->where(['si.online_admission_payment_status'=>'PAID']);


        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code',  'guardian_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }
    public function searchUnpaidAdmission($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name', 'si.gender', 'si.ple_index_number',
            'si.uce_aggregates','si.ple_aggregates', 'si.uce_index_number','si.ple_index_number','si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
            ->from('std_online_registration si')
            ->andWhere(['si.online_admission_payment_status'=>'PENDING']);

        $query->andWhere(['si.application_status'=>'ADMITTED']);


        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code', 'guardian_email', 'guardian_phone',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }



    public function searchAllAdmission($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name', 'si.gender', 'si.ple_index_number',
            'si.uce_aggregates','si.ple_aggregates', 'si.uce_index_number','si.ple_index_number','si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
            ->from('std_online_registration si')
        ;


        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code', 'guardian_email', 'guardian_phone',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function searchRejected($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name', 'si.gender', 'si.ple_index_number',
            'si.uce_aggregates','si.ple_aggregates', 'si.uce_index_number','si.ple_index_number','si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
            ->from('std_online_registration si');

        $query->andWhere(['si.application_status'=>'REJECTED']);


        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code', 'guardian_email', 'guardian_phone',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }





    public function searchAdmitted($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name', 'si.gender', 'si.ple_index_number',
            'si.uce_aggregates','si.ple_aggregates', 'si.uce_index_number','si.ple_index_number','si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
            ->from('std_online_registration si');
        $query->andWhere(['si.application_status'=>'ADMITTED']);




        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code', 'guardian_email', 'guardian_phone',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function searchPaymentsReceived($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','rq.reg_fees','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name',
            'si.gender', 'si.ple_index_number','si.uce_aggregates','si.ple_aggregates',
            'si.ple_results', 'si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_admission_payment_status',
            'online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref'
        ])
        ->from('std_online_registration si')
           ->innerJoin('sch_online_application_requirements rq' , 'rq.school_id = si.school_id')
        ->where(['si.online_registration_payment_status'=>'PAID']);

        $query->orWhere(['si.online_admission_payment_status' => 'PAID']);
        $query->andWhere(['si.school_id' => Yii::$app->user->identity->school_id]);
        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code',  'guardian_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }


    public function searchProgress($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['si.id', 'si.online_registration_ref','si.application_status','si.reg_receipt_number','rq.reg_fees','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name',
            'si.gender', 'si.ple_index_number','si.uce_aggregates','si.ple_aggregates',
            'si.ple_results', 'si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_admission_payment_status',
            'online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref','has_requested_reg_prn'
        ])
            ->from('std_online_registration si')
            ->innerJoin('sch_online_application_requirements rq' , 'rq.school_id = si.school_id')
            ->where(['si.created_by'=>Yii::$app->user->identity->id]);

        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'first_name', 'last_name', 'school_name', 'class_code',  'guardian_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }


}

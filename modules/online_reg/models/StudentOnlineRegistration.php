<?php

namespace app\modules\online_reg\models;

use app\components\ToWords;
use app\models\BaseModel;

use app\models\ImageBank;
use app\models\Model;
use app\models\User;
use app\modules\payment_plan\models\PaymentPlanSearch;
use app\modules\paymentscore\models\OnlineStudentAccountGl;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\StudentAccountGl;
use app\modules\paymentscore\models\StudentAccountHistory;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\District;
use app\modules\studentreport\models\RegistrationUploads;
use Yii;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\IdentityInterface;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table "core_student".
 *
 * @property string $id
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $first_name
 * @property string $last_name
 * @property string $archive_reason
 * @property string $school_student_registration_number

 * @property string $other_names
 * @property string $date_of_birth
 * @property string $gender
 * @property string $class_id
 * @property string $school_id
 * @property string $phone_contact_1
 * @property string $phone_contact_2
 * @property string $email_address
 * @property integer $student_code
 * @property bool $active
 * @property bool $allow_part_payments
 * @property bool $archived
 * @property string $date_archived
 * @property string $passport_photo
 * @property string $account_id
 * @property string $student_phone
 * @property string $day_boarding
 * @property string $middle_name
 * @property string $nationality
 * @property string $student_email
 * @property string $guardian_name
 * @property string $guardian_relation
 * @property string $guardian_email
 * @property string $guardian_phone
 * @property integer $student_account_id
 * @property string $nin_no
 * @property string $region
 * @property string $district
 * @property string $county
 * @property string $sub_county
 * @property string $parish
 * @property string $level_of_interest
 * @property string $disability_nature
 * @property string $village
 * @property integer $invalid_login_count
 * @property integer $schoolpay_paymentcode
 * @property integer $class_when_archived
 * @property boolean $locked
 * @property boolean $disability
 * @property string $password_reset_token
 * @property PaymentsReceived[] $paymentsReceiveds
 * @property CoreSchoolClass $studentClass
 * @property StudentAccountGl $studentAccount
 *
 */
class StudentOnlineRegistration extends \yii\db\ActiveRecord
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch,$school_name,$uploads;
    public $captcha, $uce_results,$ple_results, $ple_index_number, $uce_index_number, $ple_aggregates, $uce_aggregates,$min_agg, $max_agg;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'std_online_registration';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'date_of_birth', 'date_archived','former_school_name','former_class','school_name', 'former_school_phone','former_school_email'], 'safe'],
            [['created_by', 'class_id', 'school_id', 'passport_photo',], 'default', 'value' => null],
            [['created_by', 'class_id', 'school_id', 'passport_photo','class_when_archived','admission_amount'], 'integer'],
            [['class_id', 'school_id','first_name', 'last_name', 'gender','nationality','student_email','level_of_interest'], 'required'],
            [['active', 'archived'], 'boolean'],
            [['first_name', 'archive_reason','last_name','religion','study_section', 'reg_receipt_number','former_primary_school','former_secondary_school','former_high_school','uce_results','ple_results', 'ple_index_number', 'uce_index_number', 'ple_aggregates', 'uce_aggregates','student_phone','day_boarding','student_email','school_student_registration_number','guardian_email','guardian_relation','nin_no','disability_nature','parish','village','sub_county','county','region','district','application_status'], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1],
            [['date_of_birth', 'date_created', 'school','payment_ref_code','min_agg','max_agg','modelSearch'], 'safe'],
            [[ 'school_id'], 'integer'],
            [['active', 'allow_part_payments', 'archived','disability','reg_fees_paid','has_requested_reg_prn','has_paid_reg_fee'], 'boolean'],
            [['student_email'], 'email'],
            ['captcha', 'captcha'],
            [['disability_nature','middle_name','guardian_name','guardian_phone','allow_part_payments'], 'safe']];
    }



    public function getFullname()
    {
        $fn = $this->first_name ? $this->first_name : '';
        $mn = $this->middle_name ? $this->middle_name : '';
        $ln = $this->last_name ? $this->last_name : '';
        return $fn . ' ' . $mn . ' ' . $ln;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'middle_name' => 'Middle Names',
            'date_of_birth' => 'Date Of Birth',
            'gender' => 'Gender',
            'class_id' => 'Class',
            'school_id' => 'School',
            'nationality' => 'Nationality',
            'disability' => 'Disability',
            'disability_nature' => 'Nature of Disability',
            'student_code' => 'Student Code',
            'active' => 'Active',
            'archived' => 'Archived',
            'date_archived' => 'Date Archived',
            'day_boarding' => 'Passport Photo',
            'account_id' => 'Account ID',
            'student_email' => 'Student Email',
            'student_phone' => 'Student Phone',
            'guardian_name' => 'Guardian Name',
            'guardian_relation' => 'Guardian Relation',
            'guardian_email' => 'Gurdian Email',
            'allow_part_payments' => 'Allow Part Payments',
        ];
    }

    public function formAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
            ['attributeName'=>'middle_name', 'controlType'=>'text'],
            ['attributeName'=>'gender', 'controlType'=>'gender_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'student_phone', 'controlType'=>'text'],
            ['attributeName'=>'student_email', 'controlType'=>'text'],
            ['attributeName'=>'date_of_birth', 'controlType'=>'date_picker'],
            ['attributeName'=>'guardian_name', 'controlType'=>'text'],
            ['attributeName'=>'guardian_phone', 'controlType'=>'text'],
            ['attributeName'=>'guardian_email', 'controlType'=>'text'],
            ['attributeName'=>'guardian_relation', 'controlType'=>'text'],
            ['attributeName'=>'day_boarding', 'controlType'=>'text'],
            ['attributeName'=>'disability', 'controlType'=>'text'],
            ['attributeName'=>'disability_nature', 'controlType'=>'text'],
            ['attributeName'=>'nationality', 'controlType'=>'text'],
            ['attributeName'=>'nin_no', 'controlType'=>'text'],
            ['attributeName'=>'allow_part_payments', 'controlType'=>'text'],

        ];

        return $config;
    }

    public function updateAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
//            ['attributeName'=>'other_names', 'controlType'=>'text'],
            ['attributeName'=>'gender', 'controlType'=>'gender_picker'],
            ['attributeName'=>'school_id', 'controlType'=>'school_id_picker', 'placeholder'=>'Choose school name', 'prompt'=>'Choose school name'],
            ['attributeName'=>'class_id', 'controlType'=>'class_id_picker', 'placeholder'=>'Choose class', 'prompt'=>'Choose class'],
            ['attributeName'=>'nationality', 'controlType'=>'text'],
            ['attributeName'=>'district', 'controlType'=>'text'],
            ['attributeName'=>'disability', 'controlType'=>'disability_picker'],
//            ['attributeName'=>'country_of_residence', 'controlType'=>'text'],
            ['attributeName'=>'student_phone', 'controlType'=>'text'],
            ['attributeName'=>'student_email', 'controlType'=>'text'],
            ['attributeName'=>'date_of_birth', 'controlType'=>'text'],
            ['attributeName'=>'guardian_name', 'controlType'=>'text'],
            ['attributeName'=>'student_phone', 'controlType'=>'text'],
            ['attributeName'=>'guardian_name', 'controlType'=>'text'],
            ['attributeName'=>'guardian_email', 'controlType'=>'text'],
            ['attributeName'=>'guardian_relation', 'controlType'=>'text'],
            ['attributeName'=>'day_boarding', 'controlType'=>'text'],
            ['attributeName'=>'disability', 'controlType'=>'text'],
            ['attributeName'=>'disability_nature', 'controlType'=>'text'],
            ['attributeName'=>'nationality', 'controlType'=>'text'],
            ['attributeName'=>'nin_no', 'controlType'=>'text'],
            ['attributeName'=>'allow_part_payments', 'controlType'=>'text'],
        ];

        return $config;
    }
    public function searchAttributeConfig() {
        $config = [
            ['attributeName'=>'first_name', 'controlType'=>'text'],
            ['attributeName'=>'last_name', 'controlType'=>'text'],
            ['attributeName'=>'other_names', 'controlType'=>'text'],
        ];

        return $config;
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getDistrictName()
    {
        return $this->hasOne(District::className(), ['id' => 'district']);
    }
    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'first_name',
            'last_name',
            'gender',
            'student_phone',
            'guardian_phone',
            'date_created',
            'nin_no',
            'parish','village','sub_county','county','region','district',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreStudent::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');


    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            //Correct phone numbers
            $validatedGuardianPhone = ToWords::validatePhoneNumber($this->guardian_phone);
            if($validatedGuardianPhone) {
                $this->guardian_phone = $validatedGuardianPhone;
            }

            $validatedStudentPhone = ToWords::validatePhoneNumber($this->student_phone);
            if($validatedStudentPhone) {
                $this->student_phone = $validatedStudentPhone;
            }

//            $validatedGuardianPhone2 = ToWords::validatePhoneNumber($this->guardian2_phone);
//            if($validatedGuardianPhone2) {
//                $this->guardian2_phone = $validatedGuardianPhone2;
//            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsReceiveds()
    {
        return $this->hasMany(PaymentsReceived::className(), ['student_id' => 'id']);
    }

    public function whichDisability($attribute, $params)
    {
        if (($this->disability == 1) && empty($this->disability_nature)) {
            $this->addError($attribute, 'Please provide nature of disability');

        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAccount()
    {
        return $this->hasOne(OnlineStudentAccountGl::className(), ['id' => 'student_account_id']);
    }


    public function getPayableSum()
    {
        $sumPayable = (new \yii\db\Query())
            ->from('institution_fee_student_association fs')
            ->join('join', 'institution_fees_due fd', 'fd.id=fs.fee_id')
            ->where('fs.student_id=' . $this->id . 'AND fs.applied =true AND fd.end_date >= NOW() AND fd.effective_date <= NOW()')
            ->sum('fd.due_amount');
        return $sumPayable;
    }
    public function uniqueRegNo($attribute, $params)
    {
        $query = (new Query())->select(['school_student_registration_number', 'student_code'])->from('core_student')->where(['school_id' => $this->school_id, 'school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))])->limit(1)->one();
        $invalid = false;
        if ($this->isNewRecord) {
            $invalid = $query['school_student_registration_number'] ? true : false;
        } else {
            if ($query['school_student_registration_number'] && ($query['payment_code'] != $this->payment_code)) {
                $invalid = true;
            } else {
                $invalid = false;
            }
        }

        if ($invalid) {
            $this->addError($attribute, 'Reg No belongs to another student');

        }
    }

    /**Validate date format for date of birth
     * @param $attribute
     * @param $paramsV
     */
    public function validateDateOfBirthFormat($attribute, $params)
    {
        if (!ToWords::validateDate($this->date_of_birth, 'Y-m-d')) {
            $this->addError($attribute, 'The date of birth is in an invalid format. Enter a valid date of birth in the format yyyy-mm-dd');
        }
    }

    /**
     * Will validate the entered registration number against the school's reg no format
     * @param $attribute
     * @param $params
     */
    public function validateRegNoAgainstSchoolFormat($attribute, $params)
    {
        Yii::trace('Enter student regno validator');
        if (empty($this->school_student_registration_number)) {
            //Reg no not passed so just return;
            return;
        }

        $school_information = (new Query())->select(['school_registration_number_format', 'sample_school_registration_number'])
            ->from('school_information')->where(['id' => $this->school_id])->limit(1)->one();

        if (empty($school_information['school_registration_number_format'])) {
            //For schools without reg format, ban reserved formats
            $bankInterChannelCodes = Yii::$app->params['reservedRegistrationNumberFormats']; //An array of reserved regno formats
            foreach ($bankInterChannelCodes as $format) {
                $patternToUse = "/^" . $format . "$/";
                if (preg_match($patternToUse, $this->$attribute)) {
                    $this->addError($attribute, 'The entered registration number format is reserved for another school and is not acceptable');
                    return;
                }
            }

            //No format enforced so return
            return;
        }

        //First trim it
        $format = trim($school_information['school_registration_number_format']);
        $this->school_student_registration_number = trim(strtoupper($this->school_student_registration_number));

        $patternToUse = "/^" . $format . "$/";
        Yii::trace('Pattern to use to validate student regno is ' . $patternToUse);
        if (!preg_match($patternToUse, $this->$attribute)) {
            $this->addError($attribute, 'The entered registration number does not match the school format ' . $format . '. Enter format such as ' . $school_information['sample_school_registration_number']);
        }
    }

    public function validateGurdian($attribute, $params)
    {
        if ($this->guardian_phone && !$this->gurdian_name) {
            $this->addError($attribute, 'Provide Guardian Name for ' . $this->guardian_phone);
        }
    }

    public function validateGurdian2($attribute, $params)
    {
        if ($this->guardian2_phone && !$this->guardian2_name) {
            $this->addError($attribute, 'Provide Guardian Name for ' . $this->guardian2_phone);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentClass()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getLastArchivedClass()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_when_archived']);
    }

    public function getSchoolId()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }


    public function getSchool()
    {
        return $this->studentClass->institution0->id;
    }

    public function getImage()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'passport_photo']);
    }


    /*
  * Return particular fields for dataprovider
  */
    public function search($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $this->school_student_registration_number = trim($this->school_student_registration_number);

        $query = new Query();
        $query->select(['si.id',  'si.first_name', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",'si.application_status',
            'si.school_student_registration_number'])
            ->from('std_online_reg si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            -> andWhere(['!=', 'si.application_status', 'ACCEPTED']);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $query = $this->nameLogic($query, $this->modelSearch);
            }
        }

        if ($this->school_student_registration_number) {
            $query->andWhere(['school_student_registration_number' => trim(strtoupper($this->school_student_registration_number))]);
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                 'first_name', 'last_name', 'school_name', 'application_status','class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.id');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }


    private function nameLogic($query, $seachName)
    {
        $search_words = preg_split("/[\s,]+/", $seachName);
        $search_words = array_filter($search_words);
        $words = array();
        if (count($search_words) > 1) {
            foreach ($search_words as $word) {
                $words[] = $word;
            }
            if (isset($words[0])) {
                $name = str_replace(' ', '', $words[0]);
                $query->andFilterWhere(['or',
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }
            if (isset($words[1])) {
                $name = str_replace(' ', '', $words[1]);
                $query->andFilterWhere(['or',
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }
            if (isset($words[2])) {
                $name = str_replace(' ', '', $words[2]);
                $query->andFilterWhere(['or',
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }

        } else {
            $name = str_replace(' ', '', $seachName);
            $query->andFilterWhere(['or',
                ['ilike', 'first_name', $name],
                ['ilike', 'middle_name', $name],
                ['ilike', 'last_name', $name],

            ]);
        }
        return $query;

    }

     /*
  * Return particular fields for dataprovider
  */
    public function searchschs(array $params,$mod)
    {
        //Trim the model searches
        $this->load($params);


        $mod= $mod[0];
        Yii::trace($mod);

        $query = new Query();
        $query->select(['sch.id', 'sch.school_code', 'sch.school_name', 'dt.district_name','sch.phone_contact_1','typ.description as school_type'])
            ->from('core_school sch')
            ->innerJoin('core_control_school_types typ', 'typ.id=sch.school_type')
            ->innerJoin('district dt', 'dt.id=sch.district')
            ->innerJoin('school_module_association mod', 'sch.id = mod.school_id' )
            ->andWhere(['mod.module_id'=>$mod['id']])

            ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;

    }


    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('registration_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
                Yii::trace($file);
                $saved = Yii::getAlias('@uploads').'/' . $file->baseName . '.' . $file->extension;
                $upload = new RegistrationUploads();
                $fname = $file->baseName . '.' . $file->extension;
                if ($file->extension == 'ts' || $file->extension == 'mp3' || $file->extension == 'mp4') {
                    Yii::trace("file extension" . $file->extension);
                    $file->saveAs($saved);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_path = $fname;
                    $upload->save(false);
                } else {

                    $tmpfile_contents = file_get_contents($file->tempName);
                    Yii::trace($tmpfile_contents);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_data = base64_encode($tmpfile_contents);
                    $upload->save(false);
                    Yii::trace('Saved file ' . $file->baseName);
                }
            }
        }
    }

    public function findStudents($grp, $params)
    {
        $query1 = StudentOnlineRegistration::find()->select(['id'])->where(['school_id'=>Yii::$app->user->identity->school_id]);
        $query = new Query();
        $query->select(['si.id', 'si.first_name', 'si.last_name', 'si.middle_name', 'si.gender', 'cls.class_code',
            'si.ple_aggregates','si.uce_aggregates'])
            ->from('std_online_registration si')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id');
//            ->andFilterWhere([
//                'si.class_id'=>$params['class_id'],
//                'si.gender'=>$params['gender'],
//                'si.school_id'=>$grp['sch']])
//            ->andWhere(['not in', 'si.id', $query1]);
//        if(!empty($params['day_boarding'])){
//            $query->andWhere(['si.day_boarding'=>$params['day_boarding']]);
//        }



        if (!(empty($this->min_agg))  && ($this->level_of_interest = 'OLEVEL')) {
            Yii::trace($this->level_of_interest);
            $query->andWhere(['between', 'si.ple_aggregates', ($this->min_agg+0), ($this->max_agg +1)]);


        }else {
            if (!(empty($this->min_agg)) && ($this->level_of_interest = 'ALEVEL')) {
                $query->andWhere(['between', 'si.uce_aggregates', ($this->min_agg+0), ($this->max_agg + 1)]);

            }
        }
        if (!(empty($this->modelSearch))) {

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $query->andWhere(['online_registration_ref' => $this->modelSearch]);
                $query->andWhere(['online_admission_ref' => $this->modelSearch]);

            }
        }


        if (!(empty($this->class_id))) {

            $query->andWhere(['class_id' => $this->class_id]);

        }

        $query->orderBy('cls.class_code', 'si.last_name');
        $query->limit(500);
        return $query->all();

    }

    public static function findMembers($id){


        $query=(new Query())->select(['si.id', 'si.online_registration_ref','si.reg_receipt_number','si.channel_name','former_school_name','si.middle_name', 'si.first_name', 'si.last_name',
            'si.gender', 'si.ple_index_number','si.uce_aggregates','si.ple_aggregates',
            'si.ple_results', 'si.guardian_phone','guardian_email','has_paid_reg_fees','has_paid_admission_fee',
            'online_registration_ref','online_registration_payment_status','online_registration_transactionId','online_admission_payment_status',
            'online_admission_transactionId','reg_receipt_number','channel_name','online_admission_ref','si.class_id'
        ])
            ->from('std_online_registration si')
            ->where(['si.online_registration_payment_status'=>'PAID' ]);
        $query->andWhere(['si.school_id'=>Yii::$app->user->identity->getId()]);

        return $query->all();
    }
}


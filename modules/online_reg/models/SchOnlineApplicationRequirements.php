<?php

namespace app\modules\online_reg\models;

use app\models\BaseModel;

use app\models\ImageBank;
use app\models\User;
use app\modules\banks\models\BankDetails;
use app\modules\feesdue\models\InstitutionFeesDue;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\SchoolAccountGl;
use app\modules\paymentscore\models\SchoolAccountTransactionHistory;
use app\modules\schoolcore\models\RefRegion\RefRegion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_school".
 *
 * @property string $id

 * @property int $reg_fees


 */
class SchOnlineApplicationRequirements extends BaseModel
{
    public $uploads,$requirements;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sch_online_application_requirements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id'], 'required'],
            [['reg_fees','school_id','created_by'], 'integer'],
            [['created_by'], 'safe'],
            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx, xls, xlsx', 'maxFiles' => 10],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reg_fees' => 'Registration Fees',
        ];
    }

    public function formAttributeConfig() {

        $config =[
            ['attributeName'=>'school_name', 'controlType'=>'text'],
            ['attributeName'=>'school_type', 'controlType'=>'school_type_picker', 'placeholder'=>'Choosinga a school', 'prompt'=>'Choosinga a school'],
            ['attributeName'=>'location', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_1', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_2', 'controlType'=>'text'],
            ['attributeName'=>'bank_name', 'controlType'=>'bank_name_search', 'placeholder'=>'Select a bank', 'prompt'=>'Select a bank'],
            ['attributeName'=>'account_number', 'controlType'=>'text'],
        ];

        return $config;
    }

    public function updateAttributeConfig() {

        $config =[
            ['attributeName'=>'school_name', 'controlType'=>'text'],
            ['attributeName'=>'school_type', 'controlType'=>'school_type_picker', 'placeholder'=>'Choosinga a school', 'prompt'=>'Choosinga a school'],
            ['attributeName'=>'location', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_1', 'controlType'=>'text'],
            ['attributeName'=>'phone_contact_2', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_1', 'controlType'=>'text'],
            ['attributeName'=>'contact_email_2', 'controlType'=>'text'],
            ['attributeName'=>'account_number', 'controlType'=>'text'],
        ];

        return $config;
    }


    public function getSchoolType()
    {
        return $this->hasOne(CoreControlSchoolTypes::className(), ['id' => 'school_type']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(CoreUser::className(), ['id' => 'created_by']);
    }
    public function getBankName()
    {
        return $this->hasOne(CoreBankDetails::className(), ['id' => 'bank_name']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses()
    {
        return CoreSchoolClass::find()->where(['school_id' => $this->id])
            ->andWhere(['<>', 'class_code', '__ARCHIVE__'])->orderBy('class_code')->all();
    }
    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($id)
    {
        $attributes = [

            'school_name',
            [
                'label' => 'School Type',
                'value' => function ($model) {
                    return $model->schoolType->description;
                },
            ],
            'school_code',
            'phone_contact_1',
            'district',
            'school_registration_number_format',
            'sample_school_registration_number',
            'daily_stats_recipients',
            'account_type',
            'account_title',
            'contact_email_1',
            'region',
            'district',
            'county',
            'sub_county',
            'parish',
            'village',

            [
                'label' => 'District',
                'value' => function ($model) {
                    return $model->districtName->district_name;
                },
            ],
            [
                'label' => 'Bank Name',
                'value' => function ($model) {
                    return $model->bankName->bank_name;
                },
            ],
            'account_number',
            'date_created',
            'date_updated',
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
        ];

        if (($models = CoreSchool::findOne($id)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
    public function getDistricts(){
        return $this->hasOne(District::className(),['id'=>'dist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionFeesDues()
    {
        return $this->hasMany(InstitutionFeesDue::className(), ['school_id' => 'id']);
    }

    public function enableStats($attribute, $params)
    {
        if (($this->enable_daily_stats == 1) && empty($this->daily_stats_recipients)) {
            $this->addError($attribute, 'Enter recipient for Daily Stats');

        }
    }

    public function getDistrictName()
    {
        return $this->hasOne(District::className(), ['id' => 'district']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAccount()
    {
        return $this->hasOne(SchoolAccountGl::className(), ['id' => 'school_account_id']);
    }

    public function getBank()
    {
        return $this->hasOne(BankDetails::className(), ['id' => 'bank_name']);
    }

    public function getAccountHistories()
    {
        $query = SchoolAccountTransactionHistory::find()->where(['account_id' => $this->school_account_id])->orderBy('date_created DESC');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;

    }

    public function getSum()
    {
        return SchoolAccountTransactionHistory::find()->where(['account_id' => $this->school_account_id])->sum('amount');

    }

    public function getTransCount()
    {
        return SchoolAccountTransactionHistory::find()->where(['account_id' => $this->school_account_id])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsReceiveds()
    {
        // return $this->hasMany(PaymentsReceived::className(), ['school_id' => 'id']);
        $query = PaymentsReceived::find()->where(['school_id' => $this->id]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count('*', Yii::$app->db)]);
        $query->offset($pages->offset)->limit($pages->limit)->orderBy('date_created DESC');
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages];
    }

    public function getLogo()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'school_logo']);
    }

    public function getSchChannels()
    {
        return $this->hasMany(SchoolChannel::className(), ['institution' => 'id']);
    }

    public function getRegionName()
    {
        return $this->hasOne(RefRegion::className(), ['id' => 'region']);
    }


    public function hasModule($code){
        if(in_array($code, $this->moduleCodes)) return true;
        else return false;
    }

    /**
     * Evolution don't judge
     */
    public function getModuleCodes(){
        $modules = (new \yii\db\Query())->select('mod.module_code')
            ->from('school_module_association assc')
            ->innerJoin('suite_modules mod', 'mod.id=assc.module_id')
            ->where(['school_id' => $this->id])->all();
        return array_column($modules, 'module_code');
    }

    public function getModules()
    {
        $modules = (new \yii\db\Query())->select('module_id')
            ->from('school_module_association')
            ->where(['school_id' => $this->id])->all();
        return array_column($modules, 'module_id');
    }
    public function getRetrieveRequirements(){
        $contacts = (new Query())->select(['id', 'requirement', 'required', ])
            ->from('school_submission_requiremments')
            ->where(['requirement_id'=>$this->id]);
        return $contacts->all();
    }

    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('sch_requirement_submission_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

}

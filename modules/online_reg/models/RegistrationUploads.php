<?php

namespace app\modules\studentreport\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "submission_uploads".
 *
 * @property string $id
 * @property string $date_created
 * @property string $submission
 * @property string $file_name
 * @property string $file_data
 */
class RegistrationUploads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registration_uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['submission', 'file_name', 'file_data'], 'required'],
            [['submission'], 'integer'],
            [['file_data'], 'string'],
            [['file_name','file_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'submission' => 'Submission',
            'file_name' => 'File Name',
            'file_data' => 'File Data',
        ];
    }

    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('circular_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
                $saved = Yii::getAlias('@uploads').'/' . $file->baseName . '.' . $file->extension;
                $upload = new CircularUploads();
                $fname = $file->baseName . '.' . $file->extension;
                if ($file->extension == 'ts' || $file->extension == 'mp3' || $file->extension == 'mp4') {
                    Yii::trace("file extension" . $file->extension);
                    $file->saveAs($saved);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_path = $fname;
                    $upload->save(false);
                } else {

                    $tmpfile_contents = file_get_contents($file->tempName);
                    Yii::trace($tmpfile_contents);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_data = base64_encode($tmpfile_contents);
                    $upload->save(false);
                    Yii::trace('Saved file ' . $file->baseName);
                }
            }
        }
    }

    public function saveUploadUpdate($id)
    {

        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            foreach ($this->uploads as $file) {

                $fname = $file->baseName . '.' . $file->extension;
                $tmpfile_contents = file_get_contents( $file->tempName );
                $upload = new CircularUploads();
                Yii::trace('The Id'.$id);
                $upload->submission_id = $id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);
            }
        }
    }
}

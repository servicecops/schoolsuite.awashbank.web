<?php

namespace app\modules\online_reg\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;

/**
 * CoreSchoolSearch represents the model behind the search form of `app\modules\schoolcore\models\CoreSchool`.
 */
class CoreSchoolSelfRegistrationSearch extends CoreSchoolSelfRegistration
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'school_type'], 'integer'],
            [['date_created', 'date_updated', 'school_name', 'location', 'account_number', 'phone_contact_1', 'phone_contact_2', 'contact_email_1', 'contact_email_2', 'school_code'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
//    public function search($params)
//    {
////    {
//        Yii::trace($params);
//        $searchForm = 'school_name';
//        $searchModel = new CoreSchoolSearch();
////        // add conditions that should always apply here
//        $columns = [
//            'school_code',
//            'school_name',
//            'description',
//            'location',
//
//
//            [
//                'label' => '',
//                'value'=>function($model){
//                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-school/view', 'id' => $model['id']], ['class'=>'aclink']);
//                },
//                'format'=>'html',
//            ],
//            [
//
//                'label' => '',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    return Html::a('<i class="fas fa-edit"></i>', ['core-school/update', 'id' => $model['id']]);
//                },
//            ],
////            ['class' => 'kartik\grid\ActionColumn',]
//            ////
//        ];
//
//        $this->load($params);
//        $query = new Query();
//        $query->select(['sch.id', 'sch.school_code', 'sch.school_name', 'sch.district', 'typ.description'])
//            ->from('core_school sch')
//            ->innerJoin('core_control_school_types typ', 'typ.id=sch.school_type')
//            ->andFilterWhere(['school_code' => $this->school_code])
//            ->andFilterWhere(['bank_name' => $this->bank_name])
//            ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
//
//        $pages = new Pagination(['totalCount' => $query->count()]);
//        $sort = new Sort([
//            'attributes' => ['school_code', 'school_name', 'physical_address', 'school_type_name'],
//        ]);
//        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('sch.school_code');
//        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;
//        $query->offset($pages->offset)->limit($pages->limit);
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//
//        //return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
//
//    }
//    public function search($params)
//    {
//        $query = CoreSchool::find();
//
//        // add conditions that should always apply here
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        // grid filtering conditions
//        $query->andFilterWhere([
//            'location' => $this->location,
//        ]);
//
//        $query->andFilterWhere(['ilike', 'school_code', $this->school_code])
//            ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
//
//        return $dataProvider;
//    }

    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['sch.id', 'sch.school_code', 'sch.school_name', 'dt.district_name','sch.phone_contact_1','typ.description as school_type'])
            ->from('core_school sch')
            ->innerJoin('core_control_school_types typ', 'typ.id=sch.school_type')
            ->innerJoin('district dt', 'dt.id=sch.district')
            ->andFilterWhere(['school_code'=>$this->school_code])
            ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
    /*
    * Return particular fields for dataprovider
    */
//    public function searchIndex($params)
//    {
//
//        $query = (new Query())
//            ->select(['cs.id', 'cs.school_code', 'cs.school_name', 'account_number', 'ct.description', 'cs.village', 'cs.phone_contact_1', 'cs.contact_email_1', 'cs.date_created', 'cs.location'])
//            ->from('core_school cs')
//            ->innerJoin('core_control_school_types ct', 'cs.school_type= ct.id');
//        $query->orderBy('cs.id desc')
//        ->andFilterWhere(['ilike', 'school_name', $this->school_name]);
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//
//        $columns = [
//            'school_code',
//            'school_name',
//            'description',
//            'location',
//
//
//            [
//                'label' => '',
//                'value'=>function($model){
//                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-school/view', 'id' => $model['id']], ['class'=>'aclink']);
//                },
//                'format'=>'html',
//            ],
//            [
//
//                'label' => '',
//                'format' => 'raw',
//                'value' => function ($model) {
//                    return Html::a('<i class="fas fa-edit"></i>', ['core-school/update', 'id' => $model['id']]);
//                },
//            ],
////            ['class' => 'kartik\grid\ActionColumn',]
//            ////
//        ];
//        $searchForm = 'school_name';
//        $placeHolder = 'Search School Name';
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
//    }
}

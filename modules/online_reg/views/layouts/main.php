<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\DashboardAsset;
use app\assets\LoginAsset;
use app\widgets\Alert;
use kartik\ipinfo\IpInfo;
use kartik\popover\PopoverX;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

DashboardAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
     data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-navbarbg="skin6">
        <nav class="navbar top-navbar navbar-expand-md navbar-light">
            <div class="navbar-header" data-logobg="skin6">
                <!-- This is for the sidebar toggle which is visible on mobile only -->
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-brand">
                    <!-- Logo icon -->
                    <a href="<?= Url::to(['/site/index']); ?>">
                        <b class="logo-icon">
                            <!-- Dark Logo icon -->

                            <?= Html::img('@web/web/img/apple-touch-icon.png', ['alt' =>'logo','class'=>'dark-logo','height' => '50px']);?>
                            <!-- Light Logo icon -->

                            <?= Html::img('@web/web/img/apple-touch-icon.png', ['alt' =>'logo','class'=>'light-logo','height' => '50px']);?>
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span class="logo-text">
                                <!-- dark Logo text -->

                            <?= Html::img('@web/web/img/sch_suite.PNG', ['alt' =>'logo','class'=>'dark-logo','height' => '20px']);?>
                            <!-- Light Logo text -->
                            <?= Html::img('@web/web/img/sch_suite.PNG', ['alt' =>'logo','class'=>'light-logo','height' => '20px']);?>
                            </span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Toggle which is visible on mobile only -->
                <!-- ============================================================== -->
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
                   data-toggle="collapse" data-target="#navbarSupportedContent"
                   aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i
                            class="ti-more"></i></a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse collapse" id="navbarSupportedContent" style="background: white;">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-left mr-auto ml-3 pl-1">
                    <!-- Notification -->
                    <li class="nav-item d-none d-md-block">
                        <a class="nav-link" href="javascript:void(0)">
                            <form>
                                <div class="customize-input">

                                </div>
                            </form>
                        </a>
                    </li>
                    <!-- End Notification -->
                    <!-- ============================================================== -->
                    <!-- create new -->
                    <!-- ============================================================== -->


                </ul>
                <!-- ============================================================== -->
                <!-- Right side toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-right">
                    <!-- ============================================================== -->
                    <!-- Search -->

                    <!-- ============================================================== -->
                    <div  class="mr-5 mt-2">
                        <?php
                        echo IpInfo::widget();
                        ?>
                    </div>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <?= Html::img('@web/web/img/user.png', ['alt' =>'user', 'class'=>'rounded-circle','width'=>40]);
                            ?>
                            <span class="ml-2 d-none d-lg-inline-block"> <span
                                        class="text-dark">
                                    <?= (!\Yii::$app->user->isGuest) ? Yii::$app->user->identity->fullname : Yii::$app->runAction('/site/logout'); ?>
                                </span> <i data-feather="chevron-down"
                                           class="svg-icon"></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                            <?php if(\Yii::$app->user->can('non_student'))  : ?>
                                <a class="dropdown-item" href="<?=Url::to(['/user/my-account-profile']);?>"><i data-feather="user"
                                                                                                               class="svg-icon mr-2 ml-1"></i>
                                    My Profile</a>
                            <?php endif; ?>
                            <?php if(!\Yii::$app->user->can('non_student'))  : ?>
                                <a class="dropdown-item" href="<?=Url::to(['/schoolcore/core-student/student-profile']);?>"><i data-feather="user"
                                                                                                                               class="svg-icon mr-2 ml-1"></i>
                                    My Profile</a>
                            <?php endif; ?>
                            <?php if(\Yii::$app->user->can('non_student'))  : ?>
                                <a class="dropdown-item" href="<?=Url::to(['/user/change']);?>"><i data-feather="lock"
                                                                                                   class="svg-icon mr-2 ml-1"></i>
                                    Change Password</a>
                            <?php endif; ?>
                            <?php if(!\Yii::$app->user->can('non_student'))  : ?>
                                <a class="dropdown-item" href="<?=Url::to(['/schoolcore/core-student/change-password']);?>"><i data-feather="credit-card"
                                                                                                                               class="svg-icon mr-2 ml-1"></i>
                                    Change Password</a>
                            <?php endif; ?>

                            <a class="dropdown-item" href="<?=Url::to(['/site/logout']);?>" data-method="post"><i data-feather="power"
                                                                                                                  class="svg-icon mr-2 ml-1"></i>
                                Logout</a>
                            <div class="dropdown-divider"></div>
                            <div class="pl-4 p-3"><a href="javascript:void(0)" class="btn btn-sm btn-info">View
                                    Profile</a></div>
                        </div>
                    </li>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                </ul>
            </div>
        </nav>
    </header>

    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar scroll-->

        <div class="scroll-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav">

                     <li>
                        <div class="user-panel" style="position: relative;
    width: 100%;
    padding: 10px;margin-bottom: 10px">
                            <div class="pull-left image" style="float:left">
                                 <?= Html::img('@web/web/img/avatar.png', ['alt' =>'user', 'class'=>'rounded-circle','width'=>40]);?>

                            </div>
                            <div class="pull-left info" style="padding: 5px 5px 5px 15px;
    line-height: 1;
    position: absolute;
    left: 55px;">
                                <p style="color:white"><?= (!\Yii::$app->user->isGuest) ? Yii::$app->user->identity->fullname : "Guest User"; ?></p>
                                <a href="javascript:;"><i class="fa fa-circle text-success"></i> Online</a>
                            </div>
                            <div style="clear: both"></div>
                        </div>

                    <li class="list-divider"></li>

                    <!-- search form -->
                    <?php $form = ActiveForm::begin([
                        'action' => ['/schoolcore/core-student/index'],
                        'method' => 'get',
                        'options' => ['class' => 'formprocess sidebar-form', 'id' => 'global_search_student_name'],
                    ]); ?>
                    <div class="input-group container" >

                        <!-- <input type="text" name="q" class="form-control" placeholder="Search..." /> -->
                        <input type="text" class="form-control no-padding-right" name="CoreStudentSearch[modelSearch]"
                               placeholder="Search Name / Code" style="border-radius: 40px"/>
                        <span class="input-group-btn">
                <button type="submit" name="search" id="studentsearch-btn"  style="color:white" class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
              </span>
                    </div>
                        <?php ActiveForm::end(); ?>



                    <li class="list-divider"></li>
                    <li class="nav-small-cap" style="color:white;"><span class="hide-menu">Main Navigation</span></li>
                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?= Url::to(['/site/index']); ?>"
                                                 aria-expanded="false"><i data-feather="home" class="feather-icon"></i><span
                                    class="hide-menu">Home</span></a></li>
                    <!--                    school-->
                    <?php if (\Yii::$app->user->can('r_sch')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i><span
                                        class="hide-menu">School Info </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_sch')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add School
                                        </span></a>
                                    </li>

                                <?php endif; ?>

                                <?php if (\Yii::$app->user->can('schoolsuite_admin')) :

                                    ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school/index']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View Schools
                                        </span></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (\app\components\ToWords::isSchoolUser()) :
                                    $schoolId = Yii::$app->user->identity->school_id;
                                    ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school/view', 'id'=>$schoolId]); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View School
                                        </span></a>
                                    </li>
                                <?php endif; ?>


                            </ul>
                        </li>
                    <?php endif;?>
                    <!--                    students-->
                    <?php if (\Yii::$app->user->can('r_student')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="grid" class="feather-icon"></i><span
                                        class="hide-menu">Students </span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-student/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> View Students
                                        </span></a>
                                </li>
                                <?php if (\Yii::$app->user->can('rw_student')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-student/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Students
                                        </span></a>
                                    </li>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-student/upload-excel']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Import Students
                                        </span></a>
                                    </li>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-student/uploaded-files']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Uploaded Files
                                        </span></a>
                                    </li>


                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-student/archived']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Archived Students
                                        </span></a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>
                    <?php endif;?>
                    <!--                    term-->


                    <?php if (\Yii::$app->user->can('r_term')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="user-check" class="feather-icon"></i><span
                                        class="hide-menu">Term</span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_term')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-term/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Term
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('r_term')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-term/index']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Manage Terms
                                        </span></a>
                                    </li>
                                <?php endif;?>

                            </ul>
                        </li>
                    <?php endif;?>




                    <!--Attendance-->
                    <?php if (\Yii::$app->user->can('r_attendance')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="user-check" class="feather-icon"></i><span
                                        class="hide-menu">Attendance</span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('w_attendance')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/attendance/rollcall/display-class']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Rollcall Students
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('r_attendance')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/attendance/rollcall/view-student-attendance']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Manage Rollcall
                                        </span></a>
                                    </li>
                                <?php endif;?>

                            </ul>
                        </li>
                    <?php endif;?>

                    <!--                    classes-->
                    <?php if (\Yii::$app->user->can('r_class')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="bar-chart" class="feather-icon"></i><span
                                        class="hide-menu">Classes</span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school-class/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> View Classes
                                        </span></a>
                                </li>
                                <?php if (\Yii::$app->user->can('rw_class')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school-class/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Classes
                                        </span></a>
                                    </li>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school-class/upload-excel']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Import Classes
                                        </span></a>
                                    </li>

                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school-class/promotion-studio']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Promotion Studio
                                        </span></a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>
                    <?php endif;?>


                    <!--staff-->
                    <?php if (\Yii::$app->user->can('r_staff')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="user-check" class="feather-icon"></i><span
                                        class="hide-menu">Staff</span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('r_staff')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-staff/index']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View Staff
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('rw_staff')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-staff/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Staff
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('rw_staff')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-staff/email']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Write email
                                        </span></a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>
                    <?php endif;?>
                    <!--                    subjects-->
                    <?php if (\Yii::$app->user->can('r_subject') ||  !Yii::$app->user->can('non_student') ) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="book-open" class="feather-icon"></i><span
                                        class="hide-menu">Subjects</span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('r_subject')) :?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-subject/display-class']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View Subject
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if ( !Yii::$app->user->can('non_student') ) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-subject/student-subject']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> My Subject
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('rw_subject')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-subject/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Add Subject
                                        </span></a>
                                    </li>

                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-subject/upload-excel']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Import Subjects
                                        </span></a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>
                    <?php endif;?>
                    <!--                    tests-->
                    <?php if (\Yii::$app->user->can('r_test') ||  !Yii::$app->user->can('non_student')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="folder" class="feather-icon"></i><span
                                        class="hide-menu">Tests/Exams</span></a>
                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_test')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-test/display-class']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Test/Exam
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('r_test') ): ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-test/display-class-view']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View Test/Exam
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (  !Yii::$app->user->can('non_student') ) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-test/student-tests']); ?>" class="sidebar-link"><span
                                                    class="hide-menu">
                                            Class Tests
                                        </span></a>
                                    </li>
                                <?php endif;?>
                            </ul>
                        </li>
                    <?php endif;?>
                    <!--                    results-->
                    <?php if (\Yii::$app->user->can('r_results') ||  !Yii::$app->user->can('non_student') ) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i><span
                                        class="hide-menu">Results</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_results')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-marks/tests']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Manage Results
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if ( !Yii::$app->user->can('non_student')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-marks/student-results']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> My Results </span></a>
                                    </li>
                                <?php endif;?>
                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    banks-->
                    <?php if (\Yii::$app->user->can('schoolsuite_admin')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="hard-drive" class="feather-icon"></i><span
                                        class="hide-menu">Banks</span></a>
                            <?php if (\Yii::$app->user->can('rw_banks')) : ?>
                                <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-bank-details/create']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add bank
                                        </span></a>
                                    </li>

                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-bank-details/index']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> All banks </span></a>
                                    </li>

                                </ul>
                            <?php endif;?>
                        </li>
                    <?php endif;?>

                    <!--                    Messages-->
                    <?php if (\Yii::$app->user->can('w_msg_cr')) :?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="hard-drive" class="feather-icon"></i><span
                                        class="hide-menu">Messages</span></a>

                                <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                    <li class="sidebar-item"><a href="<?= Url::to(['/messages/e/sch-credits']);?>" class="sidebar-link"><span
                                                    class="hide-menu"> Account History
                                        </span></a>
                                    </li>

                                    <li class="sidebar-item"><a href="<?=  Url::to(['/messages/e/sch-messages']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Messages </span></a>
                                    </li>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/messages/e/sch-outbox']);  ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Message Outbox</span></a>
                                    </li>




                                </ul>
                        </li>
                    <?php endif;?>

                    <?php if ( \app\components\ToWords::isSchoolUser()) : ?>


                    <?php if (Yii::$app->user->identity->school->hasModule('SCHOOLPAY_MESSAGES')): ?>

                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="hard-drive" class="feather-icon"></i><span
                                        class="hide-menu">Messages</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/messages/e/sch-messages/create']);?>" class="sidebar-link"><span
                                                class="hide-menu"> New Sch Message
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/messages/e/sch-credits/view', 'id' => Yii::$app->user->identity->school_id]);?>" class="sidebar-link"><span
                                                class="hide-menu"> Message Account
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?=  Url::to(['/messages/e/sch-messages']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Messages </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/messages/e/sch-outbox']);  ?>" class="sidebar-link"><span
                                                class="hide-menu"> Message Outbox</span></a>
                                </li>




                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php endif; ?>




                    <!--                    grading-->
                    <?php if (\Yii::$app->user->can('r_grade') ||  !Yii::$app->user->can('non_student') ) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="terminal" class="feather-icon"></i><span
                                        class="hide-menu">Grading</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_grade') ) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-school-class/add-grades']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Grades
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('r_grade')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/gradingcore/grading/index']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Manage Grades </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if ( !Yii::$app->user->can('non_student')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/gradingcore/grading/student-class-grades']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Class Grades</span></a>
                                    </li>
                                <?php endif;?>
                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    elearning-->
                    <?php if (\Yii::$app->user->can('r_classwork')  || !\Yii::$app->user->can('non_student')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="edit-3" class="feather-icon"></i><span
                                        class="hide-menu">Elearning</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_classwork')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/e_learning/elearning/display-class']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View Class Work
                                        </span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('r_classwork') || \Yii::$app->user->can('non_student')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/e_learning/elearning/display-teachers-class']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Add Class Work</span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('r_classwork') || \Yii::$app->user->can('non_student')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/e_learning/elearning/display-teachers-class-submission']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Student Submissions</span></a>
                                    </li>
                                <?php endif;?>
                                <?php if (!\Yii::$app->user->can('non_student')) : ?>
                                    <li class="sidebar-item"><a href="<?= Url::to(['/e_learning/elearning/student-class-subject-submission']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> Submit Student Work
                                        </span></a>
                                    </li>

                                    <li class="sidebar-item"><a href="?= Url::to(['/e_learning/elearning/student-class-subject']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> View Class Work</span></a>
                                    </li>

                                    <li class="sidebar-item"><a href="<?= Url::to(['/e_learning/elearning/student-view-subject-submission']); ?>" class="sidebar-link"><span
                                                    class="hide-menu"> My Submissions</span></a>
                                    </li>
                                <?php endif;?>
                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    fees management-->
                    <?php if (\Yii::$app->user->can('rw_fd')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="shopping-bag" class="feather-icon"></i><span
                                        class="hide-menu">Fees Management</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/feesdue/fees-due/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Fees Due
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/feesdue/fees-due/fees-priority']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Priority </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/feesdue/fees-due/fees-percentage-locks']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Percentage Locks </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/plan/e/plan']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Payment Plans </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/plan/e/plan/create']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Add Payment Plan </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/plan/reports/outstanding']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Summary Report </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/plan/e/logs']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Payment Plan Logs</span></a>
                                </li>

                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    payments-->
                    <?php if (\Yii::$app->user->can('r_payments')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="layers" class="feather-icon"></i><span
                                        class="hide-menu">Payments</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/school-information/accounthistory']);?>" class="sidebar-link"><span
                                                class="hide-menu">School Payment History
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/paymentscore/payments-received/list']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Auto Settlement Trans
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?=  Url::to(['/messages/default/topup-payments']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Message Topup Payments
                                        </span></a>
                                </li>



                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    discipliary module-->
                    <?php if (\Yii::$app->user->can('rw_disp')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="package" class="feather-icon"></i><span
                                        class="hide-menu">Disciplinary</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/disciplinary/case/create']); ?>" class="sidebar-link"><span
                                                class="hide-menu">New Case
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/disciplinary/case/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> All Cases </span></a>
                                </li>

                            </ul>

                        </li>
                    <?php endif;?>








                    <!--                    payment channels-->
                    <?php if (\Yii::$app->user->can('rw_chn')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="package" class="feather-icon"></i><span
                                        class="hide-menu">Payment Channels</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/paymentscore/payment-channels/create']); ?>" class="sidebar-link"><span
                                                class="hide-menu">New Payment Channel
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/paymentscore/payment-channels/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Payment Channels </span></a>
                                </li>

                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    grouping-->
                    <?php if (\Yii::$app->user->can('schoolsuite_admin') || \Yii::$app->user->can('rw_student')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="menu" class="feather-icon"></i><span
                                        class="hide-menu">Grouping</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/core-student-group/']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Manage Groups
                                        </span></a>
                                </li>


                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    Student Report-->
                    <?php if (\Yii::$app->user->can('schoolsuite_admin') || \Yii::$app->user->can('rw_student')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="menu" class="feather-icon"></i><span
                                        class="hide-menu">Student Report</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/studentreport/student-report/']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Manage Reports
                                        </span></a>
                                </li>


                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    users-->
                    <?php if (\Yii::$app->user->can('rw_user')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="users" class="feather-icon"></i><span
                                        class="hide-menu">User Management</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <?php if (\Yii::$app->user->can('rw_user')) : ?>
                                <li class="sidebar-item"><a href="<?= Url::to(['/user/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu">View Users
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/user/create']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Add User </span></a>
                                </li>
                                <?php endif;?>
                                <?php if (\Yii::$app->user->can('rw_role')) : ?>
                                <li class="sidebar-item"><a href="<?= Url::to(['/roles/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu"> Roles and Permissions</span></a>
                                </li>
                                <?php endif; ?>

                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    logs-->
                    <?php if (\Yii::$app->user->can('view_logs')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="map" class="feather-icon"></i><span
                                        class="hide-menu">System logs</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/logs/default/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu">View Logs
                                        </span></a>
                                </li>


                            </ul>

                        </li>
                    <?php endif;?>
                    <!--                    events-->
                    <?php if (\Yii::$app->user->can('view_logs')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="calendar" class="feather-icon"></i><span
                                        class="hide-menu">Events</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">

                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/events/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Calendar
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/events/all-events']); ?>" class="sidebar-link"><span
                                                class="hide-menu">All Events
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/events/noticeboard']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Noticed board
                                        </span></a>
                                </li>

                            </ul>

                        </li>
                    <?php endif;?>

<!--                    library-->
                    <?php if (\Yii::$app->user->can('non_student') && \app\components\ToWords::isSchoolUser()) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="book" class="feather-icon"></i><span
                                        class="hide-menu">Library Management</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/book-category/create']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Add Category
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/book-category/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu">View Categories
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/book/create']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Add book
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/book/index']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Books List
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/schoolcore/book-copy/issued-books']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Issued Books
                                        </span></a>
                                </li>

                            </ul>

                        </li>
                    <?php endif;?>

<!--                    report center-->
                    <?php if (\Yii::$app->user->can('r_report')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="bar-chart" class="feather-icon"></i><span
                                        class="hide-menu">Report Center</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"><a href="<?= Url::to(['/reports']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Channel Graphs
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/default/clas']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Class Graphs
                                        </span></a>
                                </li>


                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/students/stu-info']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Students Report
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/transactions/trans']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Transaction Report
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/transactions/channel']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Channel Summary
                                        </span></a>
                                </li>

                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/transactions/fees-performance']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Detailed Fees Report
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/transactions/balance-adjustment-report']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Balance Adjustment Report
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/reports/students/attendance']) ?>" class="sidebar-link"><span
                                                class="hide-menu">Attendance Summary
                                        </span></a>
                                </li>
                            </ul>

                        </li>
                    <?php endif;?>

                    <?php if (\Yii::$app->user->can('r_report')) : ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                     aria-expanded="false"><i data-feather="bar-chart" class="feather-icon"></i><span
                                        class="hide-menu">Online Reg Center</span></a>

                            <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                                                             aria-expanded="false"><i data-feather="bar-chart" class="feather-icon"></i><span
                                                class="hide-menu">Student Reg</span></a>

                                    <ul aria-expanded="false" class="collapse  first-level base-level-line">
                                        <li class="sidebar-item"><a href="<?= Url::to(['online_reg/std-online-reg/sch-application-requirements']) ?>" class="sidebar-link"><span
                                                        class="hide-menu">School Requirements
                                        </span></a>
                                        </li>

                                        <li class="sidebar-item"><a href="<?= Url::to(['/online_reg/std-self-reg']); ?>" class="sidebar-link"><span
                                                        class="hide-menu">Student Application
                                        </span></a>
                                        </li>



                                    </ul>

                                </li>


                                <li class="sidebar-item"><a href="<?= Url::to(['/online_reg/sch-self-reg/create']) ?>" class="sidebar-link"><span
                                                class="hide-menu">School Reg
                                        </span></a>
                                </li>
                                <li class="sidebar-item"><a href="<?= Url::to(['/online_reg/std-self-reg']); ?>" class="sidebar-link"><span
                                                class="hide-menu">Student Reg
                                        </span></a>
                                </li>



                            </ul>

                        </li>
                    <?php endif;?>
                    <li class="list-divider"></li>
                    <li class="nav-small-cap" style="color:white"><span class="hide-menu">Extra</span></li>

                    <li class="sidebar-item"> <a class="sidebar-link sidebar-link" href="<?=Url::to(['/site/logout']);?>" data-method="post"
                                                 aria-expanded="false"><i data-feather="log-out" class="feather-icon"></i><span
                                    class="hide-menu">Logout</span></a></li>
                </ul>
            </nav>
            <!-- End Sidebar navigation -->
        </div>
        <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- *************************************************************** -->
            <!-- Start First Cards -->
            <!-- *************************************************************** -->

            <!-- *************************************************************** -->
            <!-- End First Cards -->
            <!-- *************************************************************** -->
            <!-- *************************************************************** -->
            <!-- Start Sales Charts Section -->
            <!-- *************************************************************** -->
            <div class="row">
                <div class="container-fluid" id="view_content">



                    <?= Alert::widget() ?>

                    <div class="content" id="view_content">
                        <?= $content ?>
                    </div>
                </div>



                <!-- *************************************************************** -->
                <!-- End Top Leader Table -->
                <!-- *************************************************************** -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

</body>

<footer class="footer">
    <div class="container">
        <p class="text-center">&copy; Service Cops <?= date('Y') ?></p>

    </div>
</footer>

<?php $this->endBody() ?>
<!-- Modal -->

</body>
</html>
<?php $this->endPage() ?>



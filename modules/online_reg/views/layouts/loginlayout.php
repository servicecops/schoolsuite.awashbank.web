<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\LoginAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <?php foreach (Yii::$app->session->getAllFlashes() as $type => $messages):
            Yii::trace($messages); ?>
            <?php
            if (is_array($messages) || is_object($messages)) {
                ?>
                <?php foreach ($messages as $message): ?>
                    <div class="alert alert-<?= $type ?>" role="alert"><?= $message ?></div>
                <?php endforeach ?>
                <?php
            }

            ?>


        <?php endforeach ?>
        <?= $content ?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

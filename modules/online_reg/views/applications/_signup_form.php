<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'Signup';
?>

<div class="container card" style="background: #f1f3ff">

    <?php $form = ActiveForm::begin(['id' => 'form-signup','action' => 'signup']); ?>
    <div class="card-body" >
        <div class="row">

            <div class="col-sm-6" id="user_level">
                <?= $form->field($model, 'user_level', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                    ['applicant_user' => 'Applicant'],
                    ['id' => 'user_level_id1', 'prompt' => 'Type of User']
                )->label('Type Of User') ?>
            </div>
            <div class="col">
                <?= $form->field($model, 'firstname',['labelOptions'=>['style'=>'color:#041f4a']])->textInput(['autofocus' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <?= $form->field($model, 'lastname',['labelOptions'=>['style'=>'color:#041f4a']])->textInput()->label('Last Name') ?>

            </div>
            <?php if($model->isNewRecord) : ?>
                <div class="col">
                    <?= $form->field($model, 'username',['labelOptions'=>['style'=>'color:#041f4a']])->textInput(['autofocus' => true]) ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <div class="col">
                <?= $form->field($model, 'mobile_phone',['labelOptions'=>['style'=>'color:#041f4a']])->textInput()->label('Mobile Number') ?>

            </div>
            <div class="col">
                <?= $form->field($model, 'email', ['labelOptions'=>['style'=>'color:#041f4a']]) ?>
            </div>
        </div>


        <div class="row">

            <?php if($model->isNewRecord): ?>

                <div class="col-sm-6">
                    <?= $form->field($model, 'password',['labelOptions'=>['style'=>'color:#041f4a']])->widget(PasswordInput::classname(), []);?>

                </div>
            <?php endif; ?>




        </div>
        <div class="row">
        <div class="col-sm-6">
            <p>(Note:Password strength must atleast be 24% )</p>

        </div>
        </div>

        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'create' : 'Update', ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton('Reset Form', ['class' => 'btn btn-light']) ?>
                </div>        </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$url = Url::to(['/user/level-options']);
$user_level = $model->user_level;
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#user").addClass('active');
    $("#user_create").addClass('active');
    $('#school_div').hide();
    $('#bank_div').hide();
    $.get('$url', function( data ) {
        $('select#user_level_id').html(data);
        $('select#user_level_id').val('$user_level');
        
        if($('#user_level_id').val()){
            var profile =$('select#user_level_id option[value="' + $('#user_level_id').val() + '"]').attr("data-profile");
            //
            // console.log($('#user_level_id').val());
            // console.log(profile);
            if(profile == 'school_guy'){
                $('#').show();
            }
            $('#school_div').show();
        }
        if($('#user_level_id').val() == 'bank_user'){
            $('#bank_div').show();
        }
    });
    
    $('#user_level_id').change(function(){
        var profile =$('select#user_level_id option[value="' + $(this).val() + '"]').attr("data-profile");
        if(profile == 'school_guy'){
            $('#bank_div').hide();
            $('#school_div').show();
        }
        else if($(this).val() == 'bank_user'){
             $('#school_div').hide();
            $('#bank_div').show();
        }
        else {
            $('#school_id').prop('selectedIndex', 0);
            $('#school_div').hide();
        }
    });
});
JS;
$this->registerJs($script);
?>


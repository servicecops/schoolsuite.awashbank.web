<?php

use app\components\Helpers;

?>



<div class="row" style="margin-top: 15px">

            <div class="col-md-6 "><b>Registration Fee (Non Refundable)</b></div>
            <div class="col-md-6 "><?= $model->reg_fees ?></div>


</div>

<br>
<div class="row">
    <table class="table" >
        <thead>
        <tr><th colspan="4"><span class="lead">Other Requirements</span></th></tr>

        </thead>
        <tbody>
        <?php
        if($contacts = $model->retrieveRequirements) :
            foreach($contacts as $k=>$v): ?>
                <tr>
                    <td style="color:white"><b><?= $v['requirement'] ?></b></td>
                    <td style="color:white"><b><?= $v['required'] ?></b></td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr><td colspan="4">No school requirements provided</td></tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

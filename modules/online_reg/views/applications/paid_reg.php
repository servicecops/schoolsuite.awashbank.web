<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Information';
?>

<div style="color:#F7F7F7">Student Information</div>
<div class="letter">



    <div class="row" style="margin-top:20px">

        <div class="col-md-8 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $title ?></span></div>

    </div>

<div class="row mt-3">


    <ul class="menu-list pull-right">

        <li>
            <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model' => get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target' => '_blank']) ?>
        </li>
    </ul>

</div>

    <div class="row">


                <table class="table table-striped">
                    <thead>
                    <tr>

                        <th class='clink'>First Name</th>
                        <th class='clink'>Last Name</th>
                        <th class='clink'>Middle Name</th>
                        <th class='clink'>Gender</th>
                        <th class='clink'>PlE Index</th>
                        <th class='clink'>PLE Results</th>
                        <th class='clink'>Former School</th>
                        <th class='clink'>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td class="clink"><?= ($v['first_name']) ? $v['first_name'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['middle_name']) ? $v['middle_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['ple_index_number']) ? $v['ple_index_number'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['ple_aggregates']) ? $v['ple_aggregates'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['former_school_name']) ? $v['former_school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td>
                                    <?= Html::a('<i class="fa  fa-eye"></i>', ['applications/view-application', 'id' => $v['id']], ['class'=>'aclink']) ?>
                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No student found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>

            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel'  => 'Last'
            ]); ?>
        

    </div>


</div>
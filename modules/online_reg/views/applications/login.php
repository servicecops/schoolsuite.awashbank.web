<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */
/* @var $stdmodel app\modules\schoolcore\models\CoreStudent */


use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

use app\models\LoginForm;
use yii\web\View;

$this->title = 'Check Application Progress';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="container login-box">
    <div class="row" >
        <div class="col" style="background-color: #fff" >

                <div class="site-login" style="padding-top: 10px">
                    <a class="d-flex align-items-center justify-content-center" href="">
                        <div class="sidebar-brand-icon">
                            <?= Html::img('@web/web/img/PNG1.png', ['alt'=>'logo', 'class'=>'img-responsive']);?>

                        </div>


                    </a>


                    <div class="col-md-12">
                        <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?>&nbsp;&nbsp;&nbsp;</h3>

                    </div>
                    <div class="col-md-12">
                        <div id="flash_message">
                            <?php  if (\Yii::$app->session->hasFlash('access')) : ?>
                                <?= \Yii::$app->session->getFlash('access'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="card mx-auto border-0" style="max-width: 400px;">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options'=>['style'=>'width:300px;margin:0px auto;',"autocomplete"=>"off"]
                    ]); ?>


                    <div class="card-body">
                        <form class="form">

                            <div class="form-group text-dark">
                                <?= $form->field($model, 'payment_ref_code')->textInput(['autofocus' => true]) ->label("Application Code")?>
                            </div>


                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-11">
                                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                </div>
                            </div>




                        </form>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div></div>

    </div>
</div>


<?php
$url = Url::to(['site/reset-passwd']);
$script = <<< JS
$("document").ready(function(){ 

    $('#forgotPassword-link').on('click', function(){
        var username = $('#loginform-username').val();

        $('#login-form').yiiActiveForm('validateAttribute', 'loginform-username');
        $('#login-form').yiiActiveForm('validateAttribute', 'loginform-captcha');
        
        setTimeout(function(){
            if(!$( "#loginform-username-id" ).hasClass( "has-error" )){
                if(!$( "#loginform-captcha-id" ).hasClass( "has-error" )){
                    console.log('no errors')
                    window.location.href = "$url?sr="+btoa(username);
                }
            }
        }, 500);

    })

  });
JS;
$this->registerJs($script);
?>

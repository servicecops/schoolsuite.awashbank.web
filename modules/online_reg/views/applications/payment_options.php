<?php

use app\modules\online_reg\models\SchOnlineApplicationRequirements;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\OnlineRegistration */
/* @var $form yii\bootstrap\ActiveForm */

?>
<?php

$applicationDetails = SchOnlineApplicationRequirements::findOne(['school_id' => $studentDetails->school_id ]);

?>



<div class="api-page">
    <div class=" card bg-white" style="width:100%">
        <div class=" card-body book-form">



                <div class="col-md-12">
                    <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> CHOOSE PAYMENT OPTION&nbsp;&nbsp;&nbsp;</h3>

                </div>


            </div>

            <div class="row" style="min-height: 100px !important;"></div>
            <div class="row" style="min-height: 100px !important;">
                <div class="col-sm-12 text-center text-capitalize" >

                <p>In order to complete registration you need to first pay an Application Fee of Birr <?php echo number_format($applicationDetails->reg_fees)?>

                </p>
                </div>

                <div class="col-sm-12 text-center">
                    <?php  if (\Yii::$app->session->hasFlash('error')) : ?>
                        <p style="color:red">  <?= \Yii::$app->session->getFlash('error'); ?></p>
                    <?php endif; ?>
                    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
                        <div class="notify notify-success">
                            <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
                            <div class="notify-content">
                                <?= \Yii::$app->session->getFlash('successAlert'); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
                        <div class="notify notify-danger">
                            <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
                            <div class="notify-content">
                                <?= \Yii::$app->session->getFlash('viewError'); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?= Html::a('<i class="fa fa-clock-o"></i> PAY NOW', ['applications/pay-immediately','studentDetails'=>$studentDetails->id], ['class' => 'btn_api']) ?>
                    <BR>
                    <br/>
                    <?= Html::a('<i class="fa fa-history"></i> PAY LATER', ['applications/pay-later','studentDetails'=>$studentDetails->id], ['class' => 'btn_api']) ?>

                </div>


            </div>
            <div class="row" style="min-height: 100px !important;"></div>


        </div>
    </div>
</div>

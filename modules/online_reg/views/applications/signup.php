<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BackOfficeUser */

$this->title = 'Create an Account';

?>
<div class="">

    <div class="">

        <div class="row">
            <div class="col-md-12">
                <div class="pic-container">
                    <div class="site-login" style="padding-top: 10px">
                        <a class="d-flex align-items-center justify-content-center" href="">
                            <div class="sidebar-brand-icon">
                                <?= Html::img('@web/web/img/PNG1.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>

                            </div>

                        </a>
                    </div>
                </div>
            </div>

        </div>

        <h5 class="text-center" style="color: #041f4a;padding: 20px"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h5>

        <?= $this->render('_signup_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>

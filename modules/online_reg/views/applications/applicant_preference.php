<?php


use app\modules\localgov\models\County;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\Book */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Welcome to SchoolSuite Student Online Application Platform';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="api-page">
    <div class=" card bg-white" style="width:100%">
        <div class=" card-body book-form">


            <div class="col-md-12">
                <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?>&nbsp;&nbsp;&nbsp;</h3>

            </div>


        </div>

        <div class="row" style="min-height: 100px !important;"></div>
        <div class="row" style="min-height: 100px !important;">
            <div class="col-sm-5" >


            </div>

            <div class="col-sm-6">
                <?php  if (\Yii::$app->session->hasFlash('error')) : ?>
                    <p style="color:red">  <?= \Yii::$app->session->getFlash('error'); ?></p>
                <?php endif; ?>

            </div>

            <div class="col-sm-12" >

                <?php $form = ActiveForm::begin([
                    'action' => ['schools-list'],
                    'method' => 'post',
                    'options'=>['class'=>'formprocess'],
                ]); ?>

                <div class="row">

                    <div class="col-md-8">

                        <?= $form->field($model, 'level_of_interest', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                            ['PRIMARY' => 'Primary Level',
                                'OLEVEL' => 'O Level',
                                'ALEVEL' => 'A Level',
                                'TERTIARY' => 'Tertiary/University',
                                'OTHER' => 'Others',

                            ],
                            ['id' => 'selected_class1', 'prompt' => 'Select level']
                        )->label('Select Level Of Interest') ?>

                    </div>


                    <div class="col-md-2"><?= Html::submitButton('submit', ['class' => 'btn btn-primary']) ?></div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="row" style="min-height: 100px !important;"></div>


    </div>
</div>
</div>


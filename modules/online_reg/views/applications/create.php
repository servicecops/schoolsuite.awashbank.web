<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Core Student';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="letters">

    <div class="row">

        <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('successAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('viewError'); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess'],'action' => 'check-slots']); ?>


    <div class="row">


        <div class="col-sm-6 " style=" color:#505050">
            <p>Select Class you want to join</p>
            <?php
            $data = CoreSchoolClass::find()->where(['school_id' => $id,'active'=>true])->all();

            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Class'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false ) ?>

        </div>
    </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-xm-6">
                    <?= Html::submitButton($model->isNewRecord ? 'Select' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                </div>
                <div class="col-xm-6">
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                </div>
            </div>
        </div>
    </div>

      <?php ActiveForm::end(); ?>

    </div>



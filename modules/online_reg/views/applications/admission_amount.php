<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Core Student';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="letter">

    <div class="row">

        <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('successAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('viewError'); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess'],
        'action' => ['applications/admission-code','studentDetails'=>$studentDetails] ]); ?>


    <div class="row">

        <?= $form->field($model, 'admission_amount', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Amount']])->textInput()->label('Enter Admission/scool fess amount') ?>

    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Select' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>





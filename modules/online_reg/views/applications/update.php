<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Edit student: ' . $model->getFullname();
$this->params['breadcrumbs'][] = ['label' => 'Core User', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-user-update">

    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Update Student Information</h3></div>
    </div>

    <div class="col-md-12">
        <div id="flash_message">
            <?php  if (\Yii::$app->session->hasFlash('actionFailed')) : ?>
                <?= \Yii::$app->session->getFlash('actionFailed'); ?>
            <?php endif; ?>
        </div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

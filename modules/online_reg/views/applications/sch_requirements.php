
<?php

use app\modules\schoolcore\models\CoreBankDetails;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html; ?>

<div class="letter">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'reg_fees')->textInput(['maxlength' => true,'placeholder' => ''])->label('Registration Fees') ?>
        </div>


    </div>

    <hr/>

    <div class="row">

        <div class="checkbox">
            <h3><i class="fa fa-check-square-o"></i>Select Classes with avaialble slots</h3>
            <?php
            foreach ($classes as $k => $v) :
                $checked = in_array($v['id'], $classes) || ($v['class_code'] == 'class_code') ? true : false;
                ?>
                <div class="checkbox checkbox-inline checkbox-info">
                    <input type="checkbox" class="checkbox" name="classes[]" value="<?= $v['id'] ?>"
                           id="<?= $v['class_code'] ?>" <?= $checked ? 'checked' : ''; ?> >
                    <label for="<?= $v['class_code'] ?>">&nbsp;<?= $v['class_code'] ?></label>
                </div>
            <?php endforeach; ?>
        </div>


    </div>

    <div class="row">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;School Requirements </h3></div>
        <div class="col-sm-12">
            <?= $this->render('_requirements', ['model' => $model,'id'=>1 ]); ?>
        </div>


        <br>


    </div>


    <hr>
    <div class="row">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Attach School Documents</h3>
            <br/>
            Please attach all files needed i.e. students list(doc, docx, pdf, xls, or xlsx) and school
            badge(png, jpg, jpeg, pdf)
        </div>


        <div class="col-sm-12">

            <?= FileInput::widget([
                'model' => $model,
                'attribute' => 'uploads[]',
                'name' => 'uploads[]',
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'previewFileType' => 'any',
                    'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'docx', 'doc', 'xls', 'xlsx', 'pdf','mp3','mp4'],
                    'showCancel' => false,
                    'showUpload' => false,
                    'maxFileCount' => 5,
                    'fileActionSettings' => [
                        'showZoom' => false,
                    ]
                ]
            ]); ?>
        </div>

    </div>


    <div class="row">
        <br>
        <hr class="l_header" style="margin-top:15px;">

    </div>


    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

</div>

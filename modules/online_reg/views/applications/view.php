<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

//use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = $model->fullname;


//$studentCampus = null;
//if($model->campus_id) {
//  $studentCampus = SchoolCampuses::findOne(['school_id'=>$model->school_id, 'id'=>$model->campus_id]);
//}
?>


<div class="letters">
    <div class="container bg-white">


        <?php if (\Yii::$app->session->hasFlash('stuAlert')) : ?>
            <div class="notify notify-success">
                <a href="javascript:" class='close-notify' data-flash='stuAlert'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('stuAlert'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
            <div class="notify notify-danger">
                <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
                <div class="notify-content">
                    <?= \Yii::$app->session->getFlash('viewError'); ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="container">
            <div class="row">
                <div class="col-sm">

                </div>

                <div class="col-sm">

                    <div class="float-right">
                        <?php if (\Yii::$app->user->can('rw_student') && $model->application_status != 'ACCEPTED') : ?>
                            <span class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown"
                                  style="float:right">Action &nbsp;<i
                                        class="fa  fa-caret-square-o-down "></i></span>
                            <ul class="dropdown-menu dropdown-menu-right">
                                 <?php if (\Yii::$app->user->can('rw_student') && $model->application_status != 'SHORTLISTED') : ?>
                                <li class="clink"><a
                                            href="<?= Url::to(['/online_reg/applications/shortlist-applicant', 'id' => $model->id]) ?>">Shortlist/Call
                                        for Interview</a></li>
                                 <?php endif; ?>

                                <li class="clink">

                                <?= Html::a('<span > Accept</span>', ['/online_reg/applications/admission-code', 'studentDetails' => $model->id], [ 'data-method' => 'post', 'data-confirm' => "Accepting student generates an admission payment reference which schould be shared with this student"]) ?>


                                <li><?= Html::a('Reject Application', ['/online_reg/applications/reject-application', 'id' => $model->id], ['style' => 'color:#CD0C0C;', 'data-confirm' => 'Are you sure you want to reject this application?', 'data-method' => 'post']) ?></li>

                            </ul>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 text-center" style="margin-top:-20px;">
                <div class="row">

                    <div class="float-right">


                    </div>
                </div>
            </div>
            <hr>

        </div>

        <!--forms-->
        <div class="row">
            <div class="col bg-gradient-light" style="color: #000">
                <?php
                $data = $model;

                ?>


                <div class="row">

                    <div class="col">

                    </div>
                    <div class="col-6">


                    </div>
                    <div class="col">

                    </div>
                </div>
                <p>
                    <span style="font-size:11px; color:#444"><i><?= $model->studentClass->institution0->school_name . ', ' . $model->studentClass->institution0->location . ', TEL : ' . $model->studentClass->institution0->phone_contact_1; ?></i></span>
                </p>

                <div class="row">
                    <div class="col">
                        <b>Date of Birth</b>
                    </div>
                    <div class="col">
                        <?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>Class /Course</b>
                    </div>
                    <div class="col">

                            <?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?><br>

                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>School</b>
                    </div>
                    <div class="col">
                        <?= ($model->school_id) ? $model->schoolName->school_name : ' --' ?><br>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>Gender</b>
                    </div>
                    <div class="col">
                        <?= ($model->gender == 'M') ? 'Male' : 'Female' ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>Email</b>
                    </div>
                    <div class="col">
                        <?= ($model->student_email) ? $model->student_email : ' --' ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>Nationality</b>
                    </div>
                    <div class="col">
                        <?= (isset($model->nationality)) ? $model->nationality : ' --' ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>Day/Boarding</b>
                    </div>
                    <div class="col">
                        <?= (isset($model->day_boarding)) ? $model->day_boarding : ' --' ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <b>Contact Phone</b>
                    </div>
                    <div class="col">
                        <?= ($model->student_phone) ? $model->student_phone : ' --' ?>
                    </div>
                </div>
            </div>
            <div class="col container">

                <div class="row">
                    <div class="col">

                    </div>
                    <div class="col-8">
                        <h4 class="double-underline"> Application Status</h4>
                        <div class="" style="border: 1px dashed #ccc;padding:25px;">
                            <div>
                                <tt>Paid Registration Fees
                                    :<b> <?= ($model->has_paid_reg_fees) ? 'Yes' : 'No' ?></b>
                                </tt><br>
                                <tt>Application Status
                                    :<b>
                                        <?php
                                        if ($model->application_status == 'ACCEPTED') {
                                            ?>
                                            <span style="color:lawngreen"> ACCEPTED</span>

                                        <?php } else if ($model->application_status == 'SHORTLISTED') { ?>

                                            <span style="color:palevioletred"> SHORTLISTED/INVITED FOR INTERVIEW</span>

                                        <?php } else if ($model->application_status == 'REJECTED') { ?>

                                            <span style="color:red">REJECTED </span>

                                        <?php } else { ?>

                                            <span style="color:red"> PENDING</span>

                                        <?php }
                                        ?>


                                    </b></tt>
                                <br>

                            </div>
                        </div>
                    </div>
                    <div class="col">

                    </div>
                </div>

                <!--        exempted fees-->
                <div class="mt-5 bg-gradient-light">
                    <div class="mt-5">
                        <h3 style="color: #000"> Guardian Info</h3>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Name</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_name) ? $model->guardian_name : "--" ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Relationship</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Phone Contact</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col container">
                            <b>Email</b>
                        </div>
                        <div class="col">
                            <?= ($model->guardian_email) ? $model->guardian_email : "--" ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <!--nav-->


<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>




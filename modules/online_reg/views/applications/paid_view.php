<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = $model->first_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-information-view hd-title" data-title="">

    <div class="letters" style="background: #fff">
        <div class="col-md-12 " style="padding:0 18px;">


            <span class="col-md-12 col-sm-12 col-xs-12 sch-title"> <?= Html::encode($this->title) ?></span>


        </div>
        <hr class="l_header">
        <?= Html::a('<i class="fa fa-edit"></i> ShortList', ['applications/shortlist-applicant', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary aclink']) ?>
        <?= Html::a('<i class="fa fa-remove"></i> Reject', ['reject', 'id' => $model->id], ['class' => 'btn btn-sm btn-danger', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to reject this application"]) ?>
        <p></p>
        <hr class="l_header">

        <div class="col-md-12 " >
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Application Status</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->application_status) ? $model->application_status : ' --' ?></div>
        </div>
        <div class="col-md-12 " >
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >First Name</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->first_name) ? $model->first_name : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Last Name</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->last_name) ? $model->last_name : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Middle Name</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->middle_name) ? $model->middle_name : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Gender</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->gender == 'M') ? 'Male' : 'Female' ?></div>
        </div><div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >PLE Index Number</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= (isset($model->ple_index_number)) ? $model->ple_index_number : ' --' ?></div>
        </div><div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >PLE Results</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"> <?= (isset($model->ple_results)) ? $model->ple_results : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Guardian Email</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->guardian_email) ? $model->guardian_email : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Guardian Phone</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->guardian_phone) ? $model->guardian_phone : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Former School</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= (isset($model->former_school)) ? $model->former_school : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >First Choice of Campus</div>
            <div class="col-md-6" style=" padding:10px"><?= ($model->first_name) ? $model->first_name : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Second Choice of Campus</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->first_name) ? $model->first_name : ' --' ?></div>
        </div>
        <?php if ($model->channel_name){ ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Channel</div>
                <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->channel_name) ? $model->channel_name : ' --' ?></div>
            </div>
        <?php }?>

        <?php if ($model->reg_receipt_number){ ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Receipt</div>
                <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->reg_receipt_number) ? $model->reg_receipt_number: ' --' ?></div>
            </div>
        <?php }?>
        <?php if ($model->admission_reg_receipt_number){ ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Receipt</div>
                <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->admission_reg_receipt_number) ? $model->admission_reg_receipt_number: ' --' ?></div>
            </div>
        <?php }?>
        <?php
        if ($model->online_registration_transactionId) {
            ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Online Registration Transaction ID</div>
                <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->online_registration_transactionId) ? $model->online_registration_transactionId : ' --' ?></div>
            </div>
        <?php }?>
        <?php
        if ($model->online_admission_transactionId) {
            ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Online Admission Transaction ID</div>
                <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->online_admission_transactionId) ? $model->online_admission_transactionId : ' --' ?></div>
            </div>
        <?php }?>

        <?php if($model->storedFiles): ?>
            <?= $this->render('files_list', ['uploads' => $model->storedFiles]); ?>
        <?php endif;?>

        <div style="clear: both"></div>
    </div>
</div>


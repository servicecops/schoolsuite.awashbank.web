<?php


use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

$value = 'abc123'
?>


<div class="formz" style="    background: white;
    padding: 20px;color:#505050">

    <div class="col-md-12">
        <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> APPLICATION FORM&nbsp;</h3>

    </div>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess'],'action' => 'submit-form']); ?>


    <div style="padding: 10px;width:100%">
        <p>Please fill the form below to continue</p>
    </div>
    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">
    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;<strong>Personal
                    Information</strong></h3></div>
    </div>

    <div class="row">

        <input type="hidden" name="Classes[class_id]" value=<?php echo $classId ?>>
        <input type="hidden" name="Classes[school_id]" value=<?php echo $schoolDetails->id ?>>

        <div class="col-sm-4">
            <?= $form->field($model, 'first_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'First Name']])->textInput() ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Last Name']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'middle_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Middle Name']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'date_of_birth')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'date_of_birth',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1980:' . (date('Y')),
                        'autoSize' => true,
                        'dateFormat' => 'yyyy-MM-dd',
                    ],
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Date of Birth'
                    ],])->label('Date of birth *') ?>


        </div>
        <div class="col-md-4 ">
            <?= $form->field($model, 'gender')->dropDownList(['M' => 'Male', 'F' => 'Female'], ['prompt' => 'Gender']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'student_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Student Phone']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'student_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'nin_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'National ID NIN']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'religion', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Religion ']])->textInput() ?>
        </div>
        <div class="col-sm-4">

            <?= $form->field($model, 'study_section', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                ['Day' => 'Day',
                'Boarding' => 'Boarding'],
                ['id' => 'study_section1', 'prompt' => 'Study Section']
            )->label('Study Section') ?>
        </div>
    </div>


    <div class="row">
        <br>
        <div class="col-sm-4">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="SchoolInformation[default_part_payment_behaviour]" value="0"><input
                        type="checkbox" id="schoolinformation-default_part_payment_behaviour"
                        name="SchoolInformation[default_part_payment_behaviour]"
                        value="1" <?= ($model->disability) ? 'checked' : '' ?> >
                <label for="schoolinformation-default_part_payment_behaviour">Disability</label>
            </div>
        </div>

    </div>

    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">

    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;<strong>Address
                    Information</strong></h3></div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\RefRegion::find()->all(), 'id', 'description');
            echo $form->field($model, 'region')
                ->dropDownList(
                    $items,           // Flat array ('id'=>'label')
                    ['prompt' => 'Select region',
                        'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });']    // options
                ); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'district')->dropDownList(['prompt' => 'Select district'
                ]
                , ['id' => 'district',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])
            ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Village']])->textInput() ?>
        </div>
    </div>


    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">

    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Relevant Primary Results </strong></h3></div>

        <div class="col-sm-4">
            <div class="col-sm-3">
                <label> Aggregates</label>
            </div>
            <div class="col-sm-9">
                <?= $form->field($model, 'ple_aggregates', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label(false) ?>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="col-sm-3">
                <label> PLE Index Number</label>
            </div>
            <div class="col-sm-9">
                <?= $form->field($model, 'ple_index_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label(false) ?>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="col-sm-3">
                <label> Former Primary School </label>
            </div>
            <div class="col-sm-9">
                <?= $form->field($model, 'former_primary_school', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label(false) ?>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> English</label>
            </div>
            <div class="col-sm-9">
                <select  name="PLE_Results[English]">
                    <option value="">Select Score</option>

                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Science</label>
            </div>
            <div class="col-sm-9">

                <select  name="PLE_Results[Science]">
                    <option value="">Select Score</option>

                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Mathematics</label>
            </div>
            <div class="col-sm-9">

                <select  name="PLE_Results[Mathematics]">
                    <option value="">Select Score</option>

                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Social Studies</label>
            </div>
            <div class="col-sm-6">
                <select   name="PLE_Results[SST]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>

    </div>
    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">

    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Relevant O-Level Results</strong></h3></div>


        <div class="col-sm-4">
            <div class="col-sm-3">
                <label> UCE Aggregates</label>
            </div>
            <div class="col-sm-9">
                <?= $form->field($model, 'uce_aggregates', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label(false) ?>

            </div>
        </div>

        <div class="col-sm-4">
            <div class="col-sm-3">
                <label> UCE Index Number</label>
            </div>
            <div class="col-sm-9">
                <?= $form->field($model, 'uce_index_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label(false) ?>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-3">
                <label> Former Secondary School </label>
            </div>
            <div class="col-sm-9">
                <?= $form->field($model, 'former_secondary_school', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label(false) ?>
            </div>
        </div>


        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> English</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[English]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Physics</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[Physics]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Chemistry</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[Chemistry]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Biology</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[Biology]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> Geography</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[Geography]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> History</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[History]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-3">
                <label> CRE or IRE</label>
            </div>
            <div class="col-sm-9">
                <select   name="UCE_Results[CREorIRE]">
                    <option value="">Select Score</option>
                    <option value="D1">D1</option>
                    <option value="D2">D2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="P7">P7</option>
                    <option value="P8">P8</option>
                    <option value="F9">F9</option>
                </select>
            </div>
        </div>
    </div>
    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">

    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Guardian
                    Information</strong></h3></div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_relation', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'guardian_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone ']])->textInput() ?>
        </div>



    </div>
    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">


    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Former School
                    Information</strong></h3></div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'former_school_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Former School Name']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'former_class', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Former Class']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'former_school_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Former School Email ']])->textInput() ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'former_school_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Former School Phone']])->textInput() ?>
        </div>

    </div>
    <hr class="l_header" style="margin-top:15px;margin-bottom:15px;">

    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Attach Supporting Documents</strong></h3></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        <?= $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
            'options' => ['accept' => '*/*'],
            'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'png','jpeg'], 'showUpload' => true,],
        ])->label(false);
        ?>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->class_id;

$campusurl = Url::to(['/school-information/campuslists']);

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
    
    }
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;

?>

<div class="letters">

    <div class="api-page" style="color:black">
        <div class=" card bg-white" style="width:100%">
            <div class=" card-body book-form">

                <div class="row">
                    <div class="col-md-12">
                        <div class="pic-container">
                            <div class="site-login" style="padding-top: 10px">
                                <a class="d-flex align-items-center justify-content-center" href="">
                                    <div class="sidebar-brand-icon">
                                        <?= Html::img('@web/web/img/PNG1.png', ['alt' => 'logo', 'class' => 'img-responsive']); ?>

                                    </div>

                                </a>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-md-12">
                    <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?>&nbsp;&nbsp;&nbsp;</h3>

                </div>
                <div class="col-md-12">
                    <div id="flash_message">
                        <?php if (\Yii::$app->session->hasFlash('business')) : ?>
                            <?= \Yii::$app->session->getFlash('business'); ?>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <p><i>Your request has been received, please use Reference number <b
                                        style="color:darkred"><b></b><?php echo $resp->paymentReference ?></b> to pay
                                <b style="color:darkred"><b></b>Birr <?php echo $resp->amount ?></b> for registration
                                fees</p></i></p>
                    </div>
                    <div class="col-sm-12">
                        <p><i>Your application will not be considered until a payment from you has been registered </i></p>
                    </div>



                </div>
                <div class="row">

                    <div class="col border-right bg-gradient-light">
                        <div class="card bg-gradient-light">
                            <div class="card-body">

                                <div class="row">
                                    <div class="col">
                                        <b style="color: #000">Name</b>
                                    </div>
                                    <div class="col" style="color: #21211f">
                                        <?= ($resp->studentName ? $resp->studentName: "--") ?>
                                    </div>


                                </div>

                                <hr class="style14">
                                <div class="row">
                                    <div class="col">
                                        <b style="color: #000">Status</b>
                                    </div>
                                    <div class="col" style="color: #21211f">
                                        <?= ($resp->status ? $resp->status : "-- ") ?>
                                    </div>


                                </div>
                                <hr class="style14">
                                <div class="row">
                                    <div class="col">
                                        <b style="color: #000">Payment Options</b>
                                    </div>
                                </div>


                                <hr class="style14">

                                <div class="row">
                                    <div class="col">
                                        <b style="color: #000">Mobile Money</b>
                                    </div>
                                    <div class="col">
                                        <b style="color: #000">Bank</b>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col">
                                        <b style="color: #000">Airtel</b>
                                        <p>Dial: *185# / *165#<br>
                                            Select: Payments -> School Fees -> School Pay and follow instructions
                                        </p><br>
                                        <hr class="style14">

                                        <b style="color: #000">MTN</b>
                                        <p>Access the Airtel Money menu by Dialing *185#<br>
                                            Select School Fees <br>
                                            Select SchoolPay <br>
                                            Follow instructions <br>
                                        </p><br>
                                        <hr class="style14">

                                        <b style="color: #000">Msente</b>
                                        <p>Dial *185#<br>
                                            Choose (4) . Bills and payments<br>
                                            Choose (5). Pay School fees<br>
                                            Follow instructions <br>
                                        </p><br>
                                    </div>
                                    <div class="col">

                                        <p>Simply present your Payment Ref Code <?php echo $resp->paymentReference ?>
                                            to the bank.</p>
                                    </div>

                                </div>
                            </div>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

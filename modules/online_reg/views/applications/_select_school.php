<?php


use app\modules\localgov\models\County;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\Book */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Search School';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="api-page">
    <div class=" card bg-white" style="width:100%">
        <div class=" card-body book-form">

            <div class="row">


            </div>
            <?php $form = ActiveForm::begin([
                'action' => ['schools-list'],
                'method' => 'get',
                'options' => ['class' => 'formprocess'],
            ]); ?>

            <div class="col-md-12">
                <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?>&nbsp;&nbsp;&nbsp;</h3>

            </div>
            <div class="col-md-12">
                <div id="flash_message">
                    <?php if (\Yii::$app->session->hasFlash('appeal')) : ?>
                        <?= \Yii::$app->session->getFlash('appeal'); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">

            </div>
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'school_name', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'School Name'])->label(false) ?>


                </div>


                <div class="col-sm-4">
                    <div class="btn-center">
                        <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">

                </div>
                <?php ActiveForm::end(); ?>
            </div>


    <?php


    $columns = [

        'school_code',
        'school_name',
        'district_name',

        'phone_contact_1',

        [
            'label' => '',
            'value' => function ($model) {
                return Html::a('Apply', ['requirements', 'id' => $model['id']], ['class' => 'aclink']);
            },
            'format' => 'html',
        ],




    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
        'columns' => $columns,
        'resizableColumns' => true,
//    'floatHeader'=>true,
        'responsive' => true,
        'responsiveWrap' => false,
        'bordered' => false,
        'striped' => true,
    ]); ?>


</div>
    </div>


</div>


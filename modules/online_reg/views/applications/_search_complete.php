<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\field\FieldRange;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

?>


<div class="container bg-white">
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['paid-registration'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row">

            <div class="col-md-2">
                <?= $form->field($model, 'class_of_interest', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                    ['S1' => 'Senior One',
                        'S5' => 'Senior Five'],
                    ['id' => 'selected_class1', 'prompt' => 'Select Class']
                )->label(false) ?>

            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'min_agg')->textInput(['placeHolder'=>'Min Agg'])->label(false) ?>

            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'max_agg')->textInput(['placeHolder'=>'Max Agg'])->label(false) ?>

            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control']])->textInput(['title' => 'Enter Reference No',
                    'placeHolder'=>'Enter Reference No',
                    'data-toggle' => 'tooltip',

                    'data-trigger' => 'hover',

                    'data-placement' => 'bottom'])->label(false) ?>

            </div>
            <div class="col-md-2"><?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

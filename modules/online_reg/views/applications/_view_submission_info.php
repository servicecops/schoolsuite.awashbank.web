<?php

use app\components\Helpers;

?>


<br>

<div class="letter">
    <div class="row">

        <div class="col-md-12">
            <h3 style="text-align: center;
    text-decoration: underline;
    padding: 20px;
    color: #465b97;
    text-transform: uppercase;">&nbsp;<i class="fa fa-th-list"></i> School Requirement List&nbsp;&nbsp;&nbsp;</h3>

        </div>
        <div class="col-md-12" >
            <div class="row">
                <div class="col-6 profile-text"><b>1. Registration Fee (Non Refundable)</b></div>
                <div class="col-6 profile-text"><?= $model->reg_fees ?></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-6 profile-text"><b>2. Classes with Available slots</b></div>
                <div class="col-6 profile-text"><?php
                    foreach ($classes as $k => $v) :
                        $checked = in_array($v['id'], $classes) || ($v['class_code'] == 'class_code') ? true : false;
                        ?>
                        <div class="checkbox checkbox-inline checkbox-info">

                            <label for="<?= $v['class_code'] ?>">&nbsp;<?= $v['class_code'] ?></label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
        <table class="table">
            <thead>
            <tr>
                <th colspan="4"><span class="lead">Other Requirements</span></th>
            </tr>

            </thead>
            <tbody>
            <?php
            if ($contacts = $model->retrieveRequirements) :
                foreach ($contacts as $k => $v): ?>
                    <tr>
                        <td><b><?= $v['requirement'] ?></b></td>
                        <td><b><?= $v['required'] ?></b></td>
                    </tr>
                <?php endforeach;
            else :
                ?>
                <tr>
                    <td colspan="4">No school requirements provided</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
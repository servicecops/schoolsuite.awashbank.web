<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

//use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = $model->fullname;

//
////$schedule = $model->paymentSchedule;
//$agreements = $model->StudentAgreements;
//$agreements['striped'] = false;
//$schedule['striped'] = false;

//$studentCampus = null;
//if($model->campus_id) {
//  $studentCampus = SchoolCampuses::findOne(['school_id'=>$model->school_id, 'id'=>$model->campus_id]);
//}
?>
<div class="container bg-white">




    <div class="row mt-5">
        <div class="col-md-12 text-center" style="margin-top:-20px;">
            <div class="row">


            </div>
        </div>
        <hr>

    </div>

    <!--forms-->
    <div class="row">
        <div class="col bg-gradient-light" style="color: #000">
            <?php
            $data = $model;

            ?>


            <div class="row">

                <div class="col">

                </div>
                <div class="col-6">
                    <?php if ($model->passport_photo): ?>
                        <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->image->image_base64 ?>"
                             height="100" width="100"/>
                    <?php else : ?>
                        <?= Html::img('@web/web/img/user.png', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>

                    <?php endif; ?><br>
                    <b>
                        Name: <?= !$model->archived ? $model->fullname : '<span style="color:red">' . $model->fullname . '</span>'; ?></b><br>
                    <b> Online Registration code: &nbsp; <?= ($model->payment_ref_code) ? $model->payment_ref_code : ' --' ?></b><br>
                    <b>Reg No
                        : </b><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?>
                    <br>
                </div>
                <div class="col">

                </div>
            </div>
            <p>
                <span style="font-size:11px; color:#444"><i><?= $model->studentClass->institution0->school_name.', '.$model->studentClass->institution0->location.', TEL : '.$model->studentClass->institution0->phone_contact_1; ?></i></span>
            </p>

            <div class="row">
                <div class="col">
                    <b>Date of Birth</b>
                </div>
                <div class="col">
                    <?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Class /Course</b>
                </div>
                <div class="col">
                    <?php if (!$model->archived) : ?>
                        <?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?><br>
                    <?php elseif ($model->class_when_archived) : ?>
                        <b>Last Class /Course (before
                            Archiving): </b><?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --' ?>
                        <br>
                    <?php endif; ?>                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>School</b>
                </div>
                <div class="col">
                    <?= ($model->school_id) ? $model->schoolName->school_name : ' --' ?><br>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Gender</b>
                </div>
                <div class="col">
                    <?= ($model->gender == 'M') ? 'Male' : 'Female' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Email</b>
                </div>
                <div class="col">
                    <?= ($model->student_email) ? $model->student_email : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Nationality</b>
                </div>
                <div class="col">
                    <?= (isset($model->nationality)) ? $model->nationality : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Day/Boarding</b>
                </div>
                <div class="col">
                    <?= (isset($model->day_boarding)) ? $model->day_boarding : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Contact Phone</b>
                </div>
                <div class="col">
                    <?= ($model->student_phone) ? $model->student_phone : ' --' ?>
                </div>
            </div>
        </div>
        <div class="col container">

            <div class="row">
                <div class="col">

                </div>
                <div class="col-8">
                    <h4 class="double-underline"> Payment Summary</h4>
                    <div class="" style="border: 1px dashed #ccc;padding:25px;">
                        <div>
                            <tt>Total Active Fees
                                : <?= ($model->payableSum) ? number_format($model->payableSum, 2) : "  --"; ?></tt><br>
                            <?php if ($model->studentAccount->account_balance > 0) : ?>
                                <tt>Student Account Balance
                                    :<b> <?= ($model->studentAccount->account_balance < 0) ? '<span style="color:red">' . number_format(abs($model->studentAccount->account_balance), 2) . '</span>' : '<span style="color:lightseagreen">' . number_format(abs($model->studentAccount->account_balance), 2) . '</span>' ?></b></tt>
                                <br>
                            <?php endif; ?>
                            <tt>Outstanding Balance
                                :<b> <?= ($model->studentAccount->outstanding_balance < 0) ? '<span style="color:red">' . number_format($model->studentAccount->outstanding_balance, 2) . '</span>' : number_format($model->studentAccount->outstanding_balance, 2) ?></b></tt><br>

                        </div>
                    </div>
                </div>
                <div class="col">

                </div>
            </div>

            <!--        exempted fees-->
            <div class="mt-5 bg-gradient-light">
                <div class="mt-5">
                    <h3 style="color: #000"> Guardian Info</h3>
                </div>
                <hr>
                <div class="row">
                    <div class="col container">
                        <b>Name</b>
                    </div>
                    <div class="col">
                        <?= ($model->guardian_name) ? $model->guardian_name : "--" ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col container">
                        <b>Relationship</b>
                    </div>
                    <div class="col">
                        <?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col container">
                        <b>Phone Contact</b>
                    </div>
                    <div class="col">
                        <?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col container">
                        <b>Email</b>
                    </div>
                    <div class="col">
                        <?= ($model->guardian_email) ? $model->guardian_email : "--" ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>



<!--nav-->

<div class= "row mt-3">
    <div class="container">
        <div class="row">
            <div class="col-sm">

            </div>
            <div class="col-sm">
                <div class="float-right">
                    <?php $form = ActiveForm::begin([
                        'action' => ['view', 'id'=>$model->id],
                        'method' => 'get',
                        'options'=>['class'=>'formprocess', 'id'=>'student_payments'],
                    ]); ?>
                    <?= $form->field($searchModel, 'only_received', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'style'=>'border:none; box-shadow:none; color:#3c8dbc;', "onchange"=>"$(form).submit()"] ])->dropDownList(['1'=>'Only Payments', '0'=>'All Transactions'])->label(false) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-sm">

                <div class="float-right">
                    <?php
                    echo \yii\bootstrap4\Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-pdf-trans', 'model' => get_class($searchModel)], [
                        'class'=>'btn btn-sm btn-danger',
                        'target'=>'_blank',
                        'data-toggle'=>'tooltip',
                        'title'=>'Will open the generated PDF file in a new window'
                    ]);
                    echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel-old', 'model' => get_class($searchModel)], [
                        'class'=>'btn btn-sm btn-success',
                        'target'=>'_blank'
                    ]);


                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">

        <ul class="nav nav-tabs bg-gradient-light shadow-lg " id="profileTab">
            <li class="active" id = "fees-tab"><a href="#fees" data-toggle="tab"><i class="fa fa-money-bill-alt"></i> Payments History</a></li>
            <li id = "payable_fees"><a href="#payableFees" data-toggle="tab"><i class="fa fa-money-bill-alt"></i> Application Progres</a></li>


        </ul>

        <div id='content' class="tab-content responsive">

            <div class="tab-pane active" id="fees">
                <?= $this->render('_tab_transactions', ['model'=>$model,'dataProvider' => $dataProvider,
                    'searchModel'=> $searchModel]) ?>
            </div>



        </div>
    </div>
</div>
<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>




<?php

use app\components\Helpers;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Submission */

$this->title = $sch->school_name .'Requirements';
$this->params['breadcrumbs'][] = ['label' => 'Submissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$header_color = '';
?>

<div class="letter">

    <div class="row">


    </div>


    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('successAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='viewError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('viewError'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="submission-view">

        <div class="row" style="border-bottom: 1px solid #c5c5c5; padding-bottom: 10px; margin-top:20px">
            <div class="col-md-10" style="padding-left:10px; "><span class="lead text-uppercase"><?php echo $this->title?> - ONLINE REGISTRATION REQUIREMENTS</span></div>

            <div class="col-md-2" style="float: right">
                <?= Html::a('Continue Application', ['applications/application-form', 'id' =>$sch->id], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php if ($model): ?>
        <?= $this->render('_view_applicants_req', ['model' => $model]) ?>


        <?php if ($model->storedFiles): ?>
            <?= $this->render('_applicants_files_list', ['uploads' => $model->storedFiles]); ?>
        <?php endif; ?>
        <?php endif; ?>

        <?php if (!$model): ?>
        <div class="row" style="padding-bottom: 10px; margin-top:20px">

         <div class="col-md-10" style="padding-left:10px;color:red "><span class="lead text-uppercase">School has no requirements set</span></div>
        </div>
        <?php endif; ?>
    </div>


</div>

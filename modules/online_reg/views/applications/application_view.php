<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = 'Application Details for';
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="" data-title="">

    <div class="letter" style="background: #fff">


        <h5 class="text-center" style="color: #041f4a;padding: 20px"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= Html::encode($this->title) .' '.'for '.$model->className->class_description .' at '.$model->schoolName->school_name  ?></h5>

        <div class="col-md-12 " >
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >First Name</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->first_name) ? $model->first_name : ' --' ?></div>
        </div>

        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Last Name</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->last_name) ? $model->last_name : ' --' ?></div>
        </div>

        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Middle Name</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->middle_name) ? $model->middle_name : ' --' ?></div>
        </div>

        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Gender</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->gender == 'M') ? 'Male' : 'Female' ?></div>
        </div><div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >PLE Index Number</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= (isset($model->ple_index_number)) ? $model->ple_index_number : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >PLE Results</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"> <?= (isset($model->ple_aggregates)) ? $model->ple_aggregates : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Registration Payment Ref</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"> <?= (isset($model->online_registration_ref)) ? $model->online_registration_ref : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >UCE Results</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"> <?= (isset($model->ple_aggregates)) ? $model->ple_aggregates : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >UCE Index</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"> <?= (isset($model->online_registration_ref)) ? $model->online_registration_ref : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Guardian Email</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->guardian_email) ? $model->guardian_email : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Guardian Phone</div>
            <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->guardian_phone) ? $model->guardian_phone : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Former School</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= (isset($model->former_school_name)) ? $model->former_school_name : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Home District</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= (isset($model->district)) ? $model->districtName->district_name : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Religion</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= (isset($model->religion)) ? $model->religion : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Relevant Secondary Results</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= (isset($model->uce_results)) ? $model->uce_results : ' --' ?></div>
        </div>
        <div class="col-md-12 ">
            <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Relevant Primary Results</div>
            <div class="col-md-6" style="background: #fff; padding:10px"><?= (isset($model->ple_results)) ? $model->ple_results : ' --' ?></div>
        </div>

        <?php if ($model->channel_name){ ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Channel</div>
                <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->channel_name) ? $model->channel_name : ' --' ?></div>
            </div>
        <?php }?>

        <?php if ($model->reg_receipt_number){ ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Registration Receipt No</div>
                <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->reg_receipt_number) ? $model->reg_receipt_number: ' --' ?></div>
            </div>
        <?php }?>
        <?php
        if ($model->online_registration_transactionId) {
            ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Online Registration Transaction ID</div>
                <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->online_registration_transactionId) ? $model->online_registration_transactionId : ' --' ?></div>
            </div>
        <?php }?>

        <?php if ($model->admission_reg_receipt_number){ ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Admission Receipt No</div>
                <div class="col-md-6" style="background: #fff; padding:10px"><?= ($model->admission_reg_receipt_number) ? $model->admission_reg_receipt_number: ' --' ?></div>
            </div>
        <?php }?>
        <?php
        if ($model->online_admission_transactionId) {
            ?>
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;" >Online Admission Transaction ID</div>
                <div class="col-md-6" style=" background: #fff;padding:10px"><?= ($model->online_admission_transactionId) ? $model->online_admission_transactionId : ' --' ?></div>
            </div>
        <?php }?>

        <?php if($model->storedFiles): ?>
            <?= $this->render('files_list', ['uploads' => $model->storedFiles]); ?>
        <?php endif;?>

        <div style="clear: both"></div>
    </div>
</div>


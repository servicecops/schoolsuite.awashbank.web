<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-bank-form">
    <div class="row hd-title" data-title="Take Photo">

    <?php $form = ActiveForm::begin(['id'=>'student_image', 'options'=>['enctype'=>'multipart/form-data']]); ?>
    <?= $form->errorSummary($model); ?>
  <div class="col-md-12" style="padding: 0px 20px;">
    <div class="col-xs-12 col-lg-12 no-padding">
      <div class="col-md-4 col-sm-4 col-xs-6">
        <div id="my_camera" class="camera-l" style="width:172px; height:133px;"></div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4">
        <div id="my_result"></div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 no-padding">
      </div>
   </div>

   <div class="col-xs-12 col-lg-12 no-padding">
        <div class="col-sm-6">
        <input id="imagebank-image_base64" type="hidden" name="ImageBank[image_base64]" value=""/>
      </div>
   </div>

    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
      <p>
        <div class="col-md-4 col-xs-6">
            <button class="btn btn-primary" onclick="take_snapshot(); return false;"><i class="fa fa-camera "></i> Photo</button>
        </div>
        <div class="col-md-4 col-xs-6">
            <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
     </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
</div>

<?php 
$this->registerJsFile(Url::to('@web/web/js/webcam.min.js', \yii\web\View::POS_END));
$this->registerJsFile(Url::to('@web/web/js/camera.js', \yii\web\View::POS_END));
?>


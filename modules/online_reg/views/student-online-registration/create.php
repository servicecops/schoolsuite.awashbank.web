<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Core Student';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Add Student Information</h3></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\District;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<?php
$value = 'abc123'
?>
<div class="formz">



    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <div style="padding: 10px;width:100%"></div>

    <div class="row">

        <div class="col-sm-6 ">
            <?php
            $url = Url::to(['/schoolcore/core-school/active-schoollist']);
            $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id' => 'school_search',
                    'class' => 'form-control',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ])->label('School Name'); ?>

        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'class_id', ['inputOptions' => ['class' => 'form-control', 'id' => 'student_class_drop']])->dropDownList(
                ['prompt' => 'Filter class'])->label('Class') ?>            </div>

    </div>
    <div class="row">

        <div class="col-sm-4">
            <?= $form->field($model, 'first_name',['inputOptions' => ['class' => 'form-control', 'placeholder' => 'First Name']])->textInput() ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'last_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Last Name']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'middle_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Middle Name']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'date_of_birth')->widget(
                \kartik\date\DatePicker::className(),  [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
                'pluginOptions' => [
                    'autoclose'=>true
                ]
            ]);?>
        </div>
        <div class="col-md-4 ">
            <?= $form->field($model, 'gender')->dropDownList(['M'=>'Male', 'F'=>'Female'], ['prompt'=>'Gender']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'student_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Student Phone']])->textInput()?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'student_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Contact Email']])->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'nin_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'National ID NIN']])->textInput()?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'school_student_registration_number', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Registration Number']])->textInput()->label('Student Reistration number') ?>
        </div>
    </div>


    <div class="row">
        <br>
        <div class="col-sm-4">
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="SchoolInformation[default_part_payment_behaviour]" value="0"><input
                    type="checkbox" id="schoolinformation-default_part_payment_behaviour"
                    name="SchoolInformation[default_part_payment_behaviour]"
                    value="1" <?= ($model->disability) ? 'checked' : '' ?> >
                <label for="schoolinformation-default_part_payment_behaviour">Disability</label>
            </div>
        </div>

    </div>

    <hr/>
    <div class="row">
        <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;<strong>Address Information</strong></h3></div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?php
            $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\RefRegion::find()->all(), 'id', 'description');
            echo $form->field($model, 'region')
                ->dropDownList(
                    $items,           // Flat array ('id'=>'label')
                    ['prompt'=>'Select region',
                        'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/districts']) . '?id="+$(this).val(), function( data ) {
                            $( "select#district" ).html(data);
                        });'  ]    // options
                );?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'district')->dropDownList(['prompt' => 'Select district'
                ]
                ,['id'=>'district',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/county-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#county" ).html(data);
                        });'])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'county')->dropDownList(['prompt' => 'Select county'
                ]
                ,['id'=>'county',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/sub-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#subcounty" ).html(data);
                        });'])
            ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'sub_county')->dropDownList(['placeholder' => 'Select subcounty'],
                ['id'=>'subcounty',
                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/parish-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#parish" ).html(data);
                        });']) ?>

        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'parish')->dropDownList(['placeholder' => 'Select parish'],['id'=>'parish']) ?>

        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'village', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Village']])->textInput() ?>
        </div>
    </div>



</div>
<hr/>
<div class="row">
    <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Guardian Information</strong></h3></div>
</div>
<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_relation', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput()?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput()?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone']])->textInput() ?>
    </div>

</div>
<div class="row">
    <?=$form->field($model, 'password')->hiddenInput(['value'=>$value])->label(false);?>
</div>
<div class="form-group col-xs-12 col-sm-12 col-lg-12">
    <br>
    <hr class="l_header" style="margin-top:15px;">

</div>

<hr/>
<div class="row">
    <div class="col-sm-12 text-center"><h3><i class="fa fa-check-square-o"></i><strong>&nbsp;Former School Information</strong></h3></div>
</div>

<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'former_school_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'former_class', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput()?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput()?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone']])->textInput() ?>
    </div>

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>
<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->class_id;

$campusurl = Url::to(['/school-information/campuslists']);

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
    
    }
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>

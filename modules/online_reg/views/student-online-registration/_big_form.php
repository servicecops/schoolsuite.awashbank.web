<?php

use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreControlSchoolTypes;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

<div class="row">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Guardian Information</h3></div>
</div>
<div class="row">
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Name']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_relation', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Relation']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Email ']])->textInput()->label('') ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($model, 'guardian_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Guardian Phone']])->textInput()->label('') ?>
    </div>

</div>


<div class="form-group col-xs-12 col-sm-12 col-lg-12">
    <br>
    <hr class="l_header" style="margin-top:15px;">

</div>


<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

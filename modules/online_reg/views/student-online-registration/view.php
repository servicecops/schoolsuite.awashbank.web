<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

//use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = $model->fullname;


//$studentCampus = null;
//if($model->campus_id) {
//  $studentCampus = SchoolCampuses::findOne(['school_id'=>$model->school_id, 'id'=>$model->campus_id]);
//}
?>
<div class="container bg-white">


    <?php if (\Yii::$app->session->hasFlash('stuAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='stuAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('stuAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('viewError'); ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="row mt-5">
        <div class="col-md-12 text-center" style="margin-top:-20px;">
            <div class="row">

                <div class="pull-right">
                    <?php if (\Yii::$app->user->can('schoolsuite_admin'))  : ?>
                        <?= Html::a('<i class="fa fa-envelope"></i> Send email', ['send-email', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-light']) ?>

                    <?php endif; ?>

                    <?php if (!$model->archived) : ?>
                        <?= Html::a('<i class="fa fa-upload"></i>Upload Photo', ['/schoolcore/core-student/profileimage-upload', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-light']) ?>
                    <?php endif; ?>
                    <?php if (\Yii::$app->user->can('rw_student'))  : ?>
                        <?= Html::a('<i class="fa fa-edit"></i>Approve Student', ['/schoolcore/core-student/create', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-light']) ?>
                    <?php endif; ?>
                    <?php if (\Yii::$app->user->can('rw_student'))  : ?>
                        <?= Html::a('<i class="fa fa-edit"></i>Reject Student', ['/schoolcore/core-student/create', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-light']) ?>
                    <?php endif; ?>

                    <?= Html::a('<i class="fa fa-print"></i> Print', ['print', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-light', 'target' => '_blank']) ?>
                    <?php if (Yii::$app->user->can('rw_student') && Yii::$app->user->can('rw_sch')) : ?>
                        <?= Html::a('<i class="fa fa-edit" style="color: #fff"></i> <span style="color: #fff">Edit Details</span>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-primary aclink']) ?>
                    <?php endif; ?>


                </div>
            </div>
        </div>
        <hr>

    </div>

    <!--forms-->
    <div class="row">
        <div class="col bg-gradient-light" style="color: #000">
            <?php
            $data = $model;

            ?>


            <div class="row">

                <div class="col">

                </div>
                <div class="col-6">
                    <?php if ($model->passport_photo): ?>
                        <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->image->image_base64 ?>"
                             height="100" width="100"/>
                    <?php else : ?>
                        <?= Html::img('@web/web/img/user.png', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>

                    <?php endif; ?><br>
                    <b>
                        Name: <?= !$model->archived ? $model->fullname : '<span style="color:red">' . $model->fullname . '</span>'; ?></b><br>
                    <br>

                </div>
                <div class="col">

                </div>
            </div>


            <div class="row">
                <div class="col">
                    <b>Date of Birth</b>
                </div>
                <div class="col">
                    <?= ($model->date_of_birth) ? date('M d, Y', strtotime($model->date_of_birth)) : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Class /Course</b>
                </div>
                <div class="col">
                    <?php if (!$model->archived) : ?>
                        <?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?><br>
                    <?php elseif ($model->class_when_archived) : ?>
                        <b>Last Class /Course (before
                            Archiving): </b><?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --' ?>
                        <br>
                    <?php endif; ?>                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>School</b>
                </div>
                <div class="col">
                    <?= ($model->school_id) ? $model->schoolName->school_name : ' --' ?><br>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Gender</b>
                </div>
                <div class="col">
                    <?= ($model->gender == 'M') ? 'Male' : 'Female' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Email</b>
                </div>
                <div class="col">
                    <?= ($model->student_email) ? $model->student_email : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Nationality</b>
                </div>
                <div class="col">
                    <?= (isset($model->nationality)) ? $model->nationality : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Day/Boarding</b>
                </div>
                <div class="col">
                    <?= (isset($model->day_boarding)) ? $model->day_boarding : ' --' ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col">
                    <b>Contact Phone</b>
                </div>
                <div class="col">
                    <?= ($model->student_phone) ? $model->student_phone : ' --' ?>
                </div>
            </div>



        <!--        exempted fees-->
        <div class="mt-5 bg-gradient-light">
            <div class="mt-5">
                <h3 style="color: #000"> Guardian Info</h3>
            </div>
            <hr>
            <div class="row">
                <div class="col container">
                    <b>Name</b>
                </div>
                <div class="col">
                    <?= ($model->guardian_name) ? $model->guardian_name : "--" ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col container">
                    <b>Relationship</b>
                </div>
                <div class="col">
                    <?= ($model->guardian_relation) ? $model->guardian_relation : "--" ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col container">
                    <b>Phone Contact</b>
                </div>
                <div class="col">
                    <?= ($model->guardian_phone) ? $model->guardian_phone : "--" ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col container">
                    <b>Email</b>
                </div>
                <div class="col">
                    <?= ($model->guardian_email) ? $model->guardian_email : "--" ?>
                </div>
            </div>
        </div>
    </div>

</div>



<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>




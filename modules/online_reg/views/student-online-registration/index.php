<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Information';
?>
<h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title) ?></h1>
<br>


    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row mt-3">
        <div class="pull-right">
            <?php
            echo Html::a('<i class="fa far fa-envelope"></i> Send email', ['email'], [
                'class'=>'btn btn-sm btn-info',
                'data-toggle'=>'tooltip',
                'title'=>'Send an email to all students'
            ]);
            ?>
        </div>


            <div class="pull-right">
                <?php
                echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['core-student/export-pdf', 'model' => get_class($searchModel)], [
                    'class'=>'btn btn-sm btn-danger',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class'=>'btn btn-sm btn-success',
                    'target'=>'_blank'
                ]);
                ?>
            </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class='clink'><?= $sort->link('first_name', ['label' => 'Student Name']) ?></th>
                        <th class='clink'><?= $sort->link('school_student_registration_number', ['label' => 'Reg No.']) ?></th>
                        <?php if (Yii::$app->user->can('rw_sch')) {
                            echo "<th class='clink'>" . $sort->link('school_name') . "</th>";
                        } ?>
                        <th class='clink'><?= $sort->link('class_code') ?></th>
                        <th class='clink'><?= $sort->link('student_phone') ?></th>
                        <th class='clink'>Reset</th>
                        <th class='clink'>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['school_student_registration_number']) ? $v['school_student_registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                                <?php if (Yii::$app->user->can('rw_sch')) : ?>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <?php endif; ?>
                                <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td>
                                    <?= Html::a('<i class="fa  fa-user-lock"></i>', ['core-student/reset', 'id' => $v['id']], ['class'=>'aclink']) ?>
                                </td>
                                <td>
                                    <?= Html::a('<i class="fas fa-edit"></i>', ['core-student/update', 'id' =>  $v['id']]); ?>
                                    <?= Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-student/view', 'id' => $v['id']], ['class'=>'aclink']) ?>

                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No student found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel'  => 'Last'
            ]); ?>

        </div>
    </div>

</div>

<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([
        'action' => ['assign', 'id' => $model->id],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>

<div class="row">

    <div class="col-xs-2 no-padding-right">
        <?= $form->field($searchModel, 'student_class')->dropDownList(
            ArrayHelper::map(\app\modules\schoolcore\models\CoreSchoolClass::find()->where(['school_id' => $model->school_id])->andWhere(['<>', 'class_code', '__ARCHIVE__'])->orderBy('class_code')->all(), 'id', 'class_description'), ['prompt' => 'Filter class', 'class' => 'form-control input-sm']
        )->label(false) ?>
    </div>

    <div class="col-xs-2 no-padding">
        <?= $form->field($searchModel, 'gender')->dropDownList(['M' => 'Male', 'F' => 'Female'], ['prompt' => 'Filter Gender', 'class' => 'form-control input-sm'])->label(false) ?>
    </div>
    <div class="col-xs-2 no-padding">
        <?= $form->field($searchModel, 'day_boarding')->dropDownList(['D' => 'Day', 'B' => 'Boarding'], ['prompt' => 'Day /Boarding', 'class' => 'form-control input-sm'])->label(false) ?>
    </div>

    <div class="col-xs-2 no-padding">
        <?= $form->field($searchModel, 'searchTerm', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Enter Student Name /Payment Code'])->label(false) ?>
    </div>
    <div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
</div>
    <?php ActiveForm::end(); ?>



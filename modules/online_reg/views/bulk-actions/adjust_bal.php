<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<?php if($state){
  echo $state['message'];
  }?>
  <div class="col-xs-12 no-padding" style="background: #01b5d2">
<div class="wizard">
    <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">1</span> Edit Group Information</a>
    <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">2</span> Edit Group Members</a>
    <a  class="col-xs-3 current"><span class="badge badge-inverse">3</span> Adjust Fee</a>
</div>
</div>

<div class="col-xs-12" style="background: white;padding:10px; margin-bottom: 5px;">
<div class="row">
<div class="col-xs-12 no-pading">
    <span style="font-size:20px;"><b><?= "Group Members - ".ucwords($model->group_name)."</b> (".$model->school->school_name.")" ?></span>
    <?php if($members) : 

    $form = ActiveForm::begin([
            'action' => ['adjust-bal', 'id'=>$model->id],
            'method' => 'post',
            'options'=>['class'=>'formprocess'],
        ]); 
    ?>
    <div>
    <div class="col-xs-3 no-padding"> 
    <?= $form->field($model2, 'new_balance', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Enter New Balance'] ])->textInput()
        ->label(false) ?>
    </div>

        <div class="col-xs-3 no-padding">
            <?= $form->field($model2, 'balance_type', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Select Balance Adjustment Type']])->dropDownList(
                [
                    'OUTSTANDING' => 'This is the student outstanding balance (Negative)',
                    'OVERPAYMENT' => 'This is the student postitive account balance',
                ],
                ['id' => 'balance_type_select', 'prompt' => 'Select Balance Adjustment Type']
            )->label(false) ?>
    </div>

        <div class="col-xs-3 no-padding">
    <?= $form->field($model2, 'reason', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Give Reason'] ])->textInput()->label(false) ?>
    </div>

      <?= Html::submitButton('<b>Adjust Balance</b>', ['class' => 'btn btn-primary btn-sm', 'data-confirm'=>'Please confirm to change balance for these students']) ?>
      <?php ActiveForm::end(); ?>
  <?php endif;?>
  <div class="pull-right">
      <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>"><button class="btn btn-default btn-sm">Back to Groups</button></a>
  </div>
  </div>
  </div>
</div>  
    
</div>


<?= $this->render('_action_group_members',
    [
        'members'=>$members,
    ]);
?>

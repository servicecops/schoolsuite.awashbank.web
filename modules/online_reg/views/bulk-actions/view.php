<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = $model->group_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letter">
<div class="student-group-information-view">
<div class="col-md-12 " style="background: #efefef">
    <div class="wizard">
        <a  class="current col-xs-4"><span class="badge">1</span>Group Information</a>
        <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">2</span> Assign Students</a>
        <a  class="col-xs-3 gray"><span class="badge badge-inverse">3</span> Take Action</a>
    </div>
</div>

<div class="row">
<div class="col-md-12">

    <div class="row">
     <div class='col-md-7'> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class='col-md-5' style="padding-top:27px;">
    <span class="pull-right">
            <a  class="aclink btn btn-default btn-sm" href="<?= Url::to(['/schoolcore/core-student-group/index'])?>">Back to Groups</a>
            <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-sm']) ?>
            <?= Html::a('<i class="fa fa-remove"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => 'Are you sure you want to delete this Group?',
                'method' => 'post',
            ],
        ]) ?>
        </span>
    </div>
    </div>
    <hr class="l_header">

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
            'group_name',
            'group_description',
            'date_created',
            'date_modified',
            'active:boolean',
        ],
    ]) ?>

</div>
</div>
</div>
</div>

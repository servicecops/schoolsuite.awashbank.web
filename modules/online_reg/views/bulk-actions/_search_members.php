<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\field\FieldRange;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

?>


<div class="container bg-white">
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['assign','id' => $model->id],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row">


            <div class="col-md-2">
                <?php
                echo $form->field($searchModel, 'the_level', ['labelOptions' => ['style' => 'color:#041f4a']])->dropDownList(
                            ['PRIMARY' => 'Primary Level',
                                'OLEVEL' => 'O Level',
                                'ALEVEL' => 'A Level',
                                'TERTIARY' => 'Tertiary/University',
                                'OTHER' => 'Others',

                            ],
                            ['id' => 'selected_class1', 'prompt' => 'Select level']
                        )->label(false) ?>

            </div>
            <div class="col-md-2">
                <?php
                $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id,'active'=>true])->all();

                echo $form->field($searchModel, 'class_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'class_code'),
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['placeholder' => 'Find Class'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false); ?>

            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'min_agg')->textInput(['placeHolder'=>'Min Agg'])->label(false) ?>

            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'max_agg')->textInput(['placeHolder'=>'Max Agg'])->label(false) ?>

            </div>
            <div class="col-md-2">
                <?= $form->field($searchModel, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control']])->textInput(['title' => 'Enter Reference No',
                    'placeHolder'=>'Enter Reference No',
                    'data-toggle' => 'tooltip',

                    'data-trigger' => 'hover',

                    'data-placement' => 'bottom'])->label(false) ?>

            </div>
            <div class="col-md-2"><?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
<?php


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

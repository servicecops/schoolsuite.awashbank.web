<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student Group Informations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="color:#F7F7F7">Core Student Group</div>

<div class="letters">
    <div class="row" data-title="Student Groups">
        <div class="col-md-12" style="background: white;padding:10px;">
            <span style="font-size: 16px; color:#3c8dbc;">Use a group to temporarily put together students for whom you want to apply a single action in bulk. Think of a group as a "basket" of students.</span><br><br>

            Appliable actions include, <b>Changing a class, Applying a fee, Archiving</b> and <b>Adjusting students
                balances</b>.<br>
            Deleting a group does not delete the students from the system. <br>

        </div>

        <div class="col-md-12">


            <div class="col-md-3 "><h3 class="box-title"><i class="fa fa-th-list"></i> &nbsp;&nbsp;Student
                    Groups</h3></div>

            <?php
            $col_w = 9;
            if (Yii::$app->user->can('schoolsuite_admin')) :
                $col_w = 4 ?>
                <div class="col-md-5 " style="padding-top: 20px !important;">
                    <?= $this->render('_search_group', ['searchModel' => $searchModel]); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-<?= $col_w; ?> " style="padding-top: 20px !important;">
                <div class="pull-right">
                <span class="dropdown" style="padding-bottom: 10px">
                    <span class="dropdown-toggle btn  btn-info btn-sm" data-toggle="dropdown"><i
                                class="fa fa-plus "></i>&nbsp;&nbsp; Add New Group</span>
                    <ul class="dropdown-menu ">
                        <li class="clink"><a href="<?= Url::to(['core-student-group/create']); ?>">Empty group</a></li>
                        <li class="clink"><a
                                    href="<?= Url::to(['core-student-group/class-group']) ?>">From class</a></li>
                    </ul>
                </span>
                    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['export-pdf', 'model' => get_class($searchModel)], ['class' => 'btn btn-danger btn-sm pull-right', 'target' => '_blank']) ?>
                    <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['export-data/export-excel', 'model' => get_class($searchModel)], ['class' => 'btn btn-primary btn-sm pull-right', 'target' => '_blank']) ?>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <div class="">
                <table class="table table-striped">
                    <thead class="bg-colorz table thead">
                    <tr><?php if (Yii::$app->user->can('schoolpay_admin')) {
                            echo "<th>" . $sort->link('school_name') . "</th>";
                        } ?>
                        <th><?= $sort->link('date_created') ?></th>
                        <th><?= $sort->link('group_name') ?></th>
                        <th><?= $sort->link('group_description', ['label' => 'Description']) ?></th>
                        <th><?= $sort->link('active') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr>
                                <td><?= ($v['date_created']) ? date('Y-m-d h:g:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <?php endif; ?>
                                <td class="clink"><a
                                            href="<?= Url::to(['core-student-group/view', 'id' => $v['id']]) ?>"><?= ($v['group_name']) ? $v['group_name'] : '<span class="not-set">(not set) </span>' ?></a>
                                </td>
                                <td><?= ($v['group_description']) ? $v['group_description'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= $v['active'] == true ? "<i class='fa  fa-check'></i>" : "no"; ?></td>
                                <td class="dropdown">&nbsp;
                                    <span class="dropdown-toggle" data-toggle="dropdown"><i
                                                class="fa  fa-caret-square-o-down "></i></span>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/assign', 'id' => $v['id']]); ?>">Assign
                                                Students</a></li>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/assign-fee', 'id' => $v['id']]) ?>">Attach
                                                a fee</a></li>
                                        <?php if (Yii::$app->user->can('schoolpay_admin')) :
                                            //  Yii::$app->user->identity->school->hasModule('SCHOOLPAY_PAYMENT_PLANS')) : ?>
                                            <li class="clink"><a
                                                        href="<?= Url::to(['core-student-group/assign-plan', 'id' => $v['id']]) ?>">Assign
                                                    Payment Plan</a></li>
                                        <?php endif; ?>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/remove-fee', 'id' => $v['id']]) ?>">Remove
                                                fee</a></li>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/archive', 'id' => $v['id']]) ?>">Archive
                                                Students</a></li>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/un-archive', 'id' => $v['id']]) ?>">Un-archive
                                                Students</a></li>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/promote', 'id' => $v['id']]); ?>">Promote
                                                as class</a></li>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/remove-students', 'id' => $v['id']]); ?>">Delete
                                                Students</a></li>
                                        <?php if (Yii::$app->user->can('adjust_student_bal')) : ?>
                                            <li class="clink"><a
                                                        href="<?= Url::to(['core-student-group/adjust-bal', 'id' => $v['id']]); ?>">Adjust
                                                    Balance</a></li>
                                        <?php endif; ?>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/part-payments', 'id' => $v['id']]); ?>">Part
                                                Payments</a></li>
                                        <li class="clink"><a
                                                    href="<?= Url::to(['core-student-group/update', 'id' => $v['id']]); ?>">Edit
                                                Group</a></li>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach;
                    else : ?>
                        <tr>
                            <td colspan="6">No group found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
                <?= LinkPager::widget([
                    'pagination' => $pages['pages'],
                ]); ?>
                <div style="padding-bottom: 180px;"><p>&nbsp;</p></div>
            </div>
        </div>
    </div>
</div>


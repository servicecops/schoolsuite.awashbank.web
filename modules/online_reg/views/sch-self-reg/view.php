<?php

use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'School Information';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="container">

        <div class="row card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="l-address">
                    Address: <?= $model->location ?><br>
                    Email: <?= $model->contact_email_1 ?><br>
                    Tel: <?= $model->phone_contact_1 ?><br>
                    Mobile:<?= $model->phone_contact_2 ?>

                </div>
            </div>
            <div class="col">
                <?php if ($model->school_logo): ?>
                    <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->logo->image_base64 ?>"
                         height="100" width="100"/>
                <?php else : ?>
                    <?= Html::img('@web/img/PNG1.png', ['class' => 'img-fluid', 'alt' => 'profile photo']) ?>

                <?php endif; ?><br>
                <span style="color: #000"> <?= $model->school_name ?></span>

            </div>

            <?php if (\Yii::$app->user->can('rw_sch')) : ?>
            <div class="col">
                <div class="top-buttons pull-right">
                    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                        <?= Html::a('<i class="fa fa-edit" style="color: #fff"></i> <span style="color: #fff">Edit Logo</span>', ['logo', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-primary aclink']) ?>
                    <?php endif; ?>
                    <?php if (Yii::$app->user->can('schoolsuite_admin') && Yii::$app->user->can('rw_sch')) : ?>
                        <?= Html::a('<i class="fa fa-edit" style="color: #fff"></i> <span style="color: #fff">Edit Details</span>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm bg-gradient-primary aclink']) ?>
                    <?php endif; ?>

                    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                        <?= Html::a('<i class="fa fa-trash" style="color: #fff"></i><span style="color: #fff"> Delete School</span>', ['delete', 'id' => $model->school_code], ['class' => 'btn btn-sm bg-gradient-danger', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to delete this school"]) ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif;?>
        </div>
        <hr class="style14">
        <div class="row">
            <div class="col-md-12">
                <div class="letter">



                    <div class="profile-data">

                        <ul class="nav nav-tabs bg-gradient-light shadow-lg " id="profileTab">
                            <li class="active" id="school-tab" style="color: #0a0a0a"><a href="#schoolInfo" data-toggle="tab"><i
                                            class="fa fa-info-circle"></i> About</a>
                            </li>

                            <?php if (Yii::$app->user->can('payments_received_list')) : ?>
                                <li id="classes-tab"><a href="#schClasses" data-toggle="tab"><i class="fa fa-chalkboard-teacher"></i>
                                        Classes</a></li>
                                <li id="trasactions-tab"><a href="#transHistory" data-toggle="tab"><i
                                                class="fa fa-exchange-alt"></i> Transaction History</a></li>
                                <li id="payments-tab"><a href="#paymentsReceived" data-toggle="tab"><i
                                                class="fa fa-money-bill-alt"></i> Payments Received</a></li>
                            <?php endif; ?>

                        </ul>

                        <br>
                        <div id='content' class="tab-content responsive">

                            <div class="tab-pane active" id="schoolInfo">
                                <?= $this->render('_tab_school_info', ['model' => $model, 'accounts' => $accounts, 'sections'=>$sections]) ?>
                            </div>

                            <?php if (Yii::$app->user->can('payments_received_list')) : ?>
                                <div class="tab-pane" id="schClasses">
                                    <?= $this->render('_tab_school_classes', ['model' => $model]) ?>
                                </div>
                                <div class="tab-pane" id="transHistory">
                                    <?= $this->render('_tab_trans_history', ['model' => $model]) ?>
                                </div>

                                <div class="tab-pane" id="paymentsReceived">
                                    <?= $this->render('_tab_payments_received', ['model' => $model]) ?>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>


</div>
    </div>
    </div>
<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#schoolInfo").addClass('active');
    $("#schools").addClass('active');
  });
JS;
$this->registerJs($script);
?>

<?php $this->registerJs("(function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);", yii\web\View::POS_END, 'responsive-tab'); ?>

<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

?>

<div class="col-sm-12">

            <?php $i = 1;
            $answerId = 0;

            ?>
            <div id="submission_Contacts">

                <div style="padding:20px 0 10px 0;" class="row">
                    <div class="col no-padding-left"><span style="margin-right:5px"
                                                           class="text-uppercase">Add Contact</span><span
                                class="add_contact_button" style="font-size: 22px;"><i
                                    class="fas fa-plus-circle text-primary"></i></span></div>
                    <div class="col no-padding-left"><span class="text-uppercase">&nbsp;</span></div>
                    <div class="col-auto no-padding-left"><span style="padding-left: 24px;">&nbsp;</span></div>
                </div>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-xm-6">

                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                    </div>
                    <div class="col-xm-6">
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                    </div>
                </div>
            </div>

</div>
        <?php
        $script = <<< JS
$("document").ready(function () {
      var row = $i;
   console.log("this one");
   
    let thisBut = function (selector) {
        console.log("selectedButonValue is: " + selector.val());
    };
    
   
    let thisAnswerOption = function (selector) {

        let singleAswerContainer = $('#singleAnswer1');
        singleAswerContainer.find('.but[anserId=0]');
        let theAnswerId = selector.attr('answerId');
        let selectedOptionA = selector.val();
        console.log("answerId " + theAnswerId);
        console.log("rowId " + theAnswerId);
        console.log("selectedOptionA " + selectedOptionA);
        $('.but[answerId=' + theAnswerId + ']').val(selectedOptionA);
    };

    let thisSourceSelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

        if (selector.val() === 'singleselect') {

            $('#singleSelect' + rowId).show();
        } else {
            $('#singleSelect' + rowId).hide();
        }

        if (selector.val() === 'radioButton') {

            $('#singleAnswer' + rowId).show();
        } else {
            $('#singleAnswer' + rowId).hide();
        }
        if (selector.val() === 'checkbox') {

            $('#multipleAnswer' + rowId).show();
        } else {
            $('#multipleAnswer' + rowId).hide();
        }


 }
   
 

    
    //main div
    var row_html = function (rw) {
        var this_html = '<div style="padding-bottom:20px;" id="add_contact_row_' + rw + '" class="row"><div class="col-md-3">' +
            '<input  class="form-control" title="Must begin with title (HM/HT/DIR/BUR/PRP/ACC/ADMIN)" required="required" pattern="^(HT|Ht|ht|BUR|bur|Bur|DIR|Dir|dir|HM|Hm|hm|PRP|Prp|prp|ACC|Acc|acc|ADMIN|Admin|admin).*"  type="text" placeholder="eg. HT James Mukasa" name="Contacts[' + rw + '][contact_name]" >' +
            '</div>'+
            '<div class="col-md-3">' +
            '<input  class="form-control" required="required"  title="Enter correct  phone number" pattern="^(0|256|260)(7[0123456789]|39|96|97|95])\d{7}$" type="text" placeholder="eg. 0772454545" name="Contacts[' + rw + '][contact]" >' +
            '</div>' +
             '<div class="col-md-3">' +
            '<input  class="form-control" required="required"  title="Enter correct  phone number" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Enter correct email" type="text" placeholder="eg. james.mukasa@gmail.com" name="Contacts[' + rw + '][email]" >' +
            '</div>' +
            '<div id ="answers_container'+rw+'"></div>'+
            '<div class="col-auto no-padding-left">' +
            '<span class="remove_contact_button" style="font-size:22px;" data-for="add_contact_row_' + rw + '"><i class="fas fa-minus-circle text-danger"></i></span>' +
            '</div>'+
            
            
          '</div>'
        return this_html;

    }
    
    let addQuestion = function(){
        row++;
        var new_row_html = row_html(row);
        $('div#submission_Contacts').append(new_row_html);
        let singleAswerContainer = $('#add_contact_row_' + row);
        singleAswerContainer.find(".source_questions").on("change", function () {
            console.log("messaf");
          //  thisSourceSelected($(this));
          let selector = $(this);
        let rowId = selector.attr('rowId'); 
        
        
        populateAnswers(rowId);
        
             singleAswerContainer.find(".answerOptions").on("change", function () {
                thisAnswerOption($(this));
    
            });
        });
       
        // singleAswerContainer.find(".but").on("change", function () {
        //     thisBut($(this));
        // });
        
    }
    
    let populateAnswers = function(row){
        let selector = $("#question_type_selector"+row);
       // console.log(selector );
        
        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        
         var answer_html ='';  
        if (selector.val() === 'singleselect') {
            answer_html =single_input_question(row);
            
        } else if(selector.val() === 'radioButton') {
            answer_html =single_answer_question(row);
        } else if(selector.val() === 'checkbox') {
           answer_html =multi_answer_question(row);
        }
        console.log($("#answers_container"+row));

        $("#answers_container"+row).html(answer_html);
       
    }
 
    $('.add_contact_button').on('click', function (e) {
       
        e.preventDefault();
        addQuestion();
        
    });


    $(".source_questions").on("change", function () {
        
        //thisSourceSelected($(this));
        let selector = $(this);
        let rowId = selector.attr('rowId');        
        populateAnswers(rowId);
    });
    
     $('#theevent').on("click", function () {
         console.log("init");
        firstBlock((this));
    });

    $(".answerOptions").on("change", function () {
        thisAnswerOption($(this));
    });

    $(".but").on("change", function () {
        thisBut($(this));
    });


    $('body').on('click', '.remove_contact_button', function (e) {
        e.preventDefault();
        $('div#' + $(this).attr('data-for')).remove();
    });
    
   addQuestion();
   
   
  
    
});

JS;
        $this->registerJs($script);
        ?>



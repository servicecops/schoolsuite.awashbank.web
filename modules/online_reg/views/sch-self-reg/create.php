<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Register A School';
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">
    <div class="row">
        <div class="col-md-12">
            <div class="pic-container">
                <?= Html::img('@web/images/localgovt_logo.png', ['alt' => 'logo', 'class' => 'pic img-fluid']); ?>

            </div>
        </div>




    </div>
    <div id="flash_message">
        <?php  if (\Yii::$app->session->hasFlash('uploadFailed')) : ?>
            <?= \Yii::$app->session->getFlash('uploadFailed'); ?>
        <?php endif; ?>
    </div>
    <div class="model_titles">
        <div class="col-sm-12 "><h3><i class="fa fa-check-square-o"></i>&nbsp;Create A Submission</h3></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,  'sch_modules'=>[], 'banks'=>$banks,
        'bank_info_error'=>$bank_info_error, 'accounts'=>$accounts,'bk_data'=>$bk_data,
    ]) ?>

</div>

<?php

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

?>

<div class="row hd-title" data-title="School Account History">

    <?php if (\Yii::$app->session->hasFlash('successAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='successAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('successAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('errorAlert')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('errorAlert'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => ['core-school/accounthistory'],
            'method' => 'get',
            'options' => ['class' => 'formprocess']
        ]); ?>
        <ul class=" row menu-list no-padding" style="list-style-type:none">
            <li class="col-md-2 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></li>
            <li class="col-md-2 col-sm-1 col-xs-1 no-padding"><?= DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_to',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                    'clientOptions' => [
                        'class' => 'form-control',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                ]); ?></li>
            <?php if (Yii::$app->user->can('rw_sch')) {
                $url = Url::to(['core-school/schools']);
                $selectedSchool = empty($searchModel->account_id) ? '' : CoreSchool::findOne(['school_account_id'=>$searchModel->account_id])->school_name;
                echo '<li class="col-md-3 col-sm-3 col-xs-3 no-padding">';
                echo $form->field($searchModel, 'account_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'size' => 'sm',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'placeholder' => 'Search School',
                        'id' => 'student_selected_school_id',
                        'class' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false);
                echo '</li>';

            } ?>

            <li class="col-md-2 col-sm-4 col-xs-4 no-padding"><?= $form->field($searchModel, 'payment_id')->dropDownList(ArrayHelper::map(PaymentChannels::find()->all(), 'id', 'channel_name'), ['prompt' => 'Filter channel', 'class' => 'form-control input-sm'])->label(false) ?></li>
            <li class="col-md-2 col-sm-2 col-xs-2 no-padding"><?= $form->field($searchModel, 'transaction_id', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Trans ID'])->label(false) ?></li>
            <li class="col-md-1 col-sm-2 col-xs-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></li>
        </ul>
        <?php ActiveForm::end(); ?>
    </div>

      <div class="col-md-8">
            <div style="padding: 2px; font-size: 90%">
                <i class="fa fa-hourglass-half icon-pending-color"> Processing</i>&nbsp;
                <i class="fa fa-check-circle icon-success-color"> Completed</i>&nbsp;
            </div>
        </div>

        <div class="col-md-4">

                <ul class="row menu-list no-padding" style="list-style-type:none">
                    <?php
                    echo \yii\bootstrap4\Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['export-pdf', 'model' => get_class($searchModel)], [
                        'class'=>'btn btn-sm btn-danger',
                        'target'=>'_blank',
                        'data-toggle'=>'tooltip',
                        'title'=>'Will open the generated PDF file in a new window'
                    ]);
                    echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-pexcel', 'model' => get_class($searchModel)], [
                        'class'=>'btn btn-sm btn-success',
                        'target'=>'_blank'
                    ]);
                    ?>
                </ul>

        </div>


        <div style="overflow-x:auto;">
            <table class="table table-striped">
                <thead>
                <tr>

                    <th class='clink'><?= $sort->link('date_created', ['label' => 'Date']) ?></th>
                    <th class='clink'>Details</th>
                    <th class='clink'>Code</th>
                    <th class='clink'>Reg No.</th>
                    <th class='clink'>Student</th>
                    <th class='clink'><?= $sort->link('channel_trans_id', ['label' => 'Ch Trans Id']) ?></th>
                    <th class='clink'><?= $sort->link('reciept_number', ['label' => 'Receipt No.']) ?></th>
                    <th class='clink'><?= $sort->link('trans_type', ['label' => 'Type']) ?></th>
                    <th class='clink'><?= $sort->link('channel_code', ['label' => 'Channel']) ?></th>
                    <th class='clink'><?= $sort->link('channel_memo', ['label' => 'Ch. Info']) ?></th>
                    <th class='clink'><?= $sort->link('amount') ?></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($dataProvider) :
                    foreach ($dataProvider as $k => $v) : ?>
                        <tr data-key="0">
                            <?PHP

                            ?>
                            <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['registration_number']) ? $v['registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['reciept_number']) ? $v['reciept_number'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['trans_type']) ? $v['trans_type'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['channel_code']) ? '<img src="' . Url::to(['/import/import/image-link', 'id' => $v['payment_channel_logo']]) . '" width="30px" /> ' . $v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['channel_memo']) ? $v['channel_memo'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['payment_id']) ? Html::a('<span class="pull-right"><i class="glyphicon glyphicon-print"></i>&nbsp;&nbsp;&nbsp;</span>', Url::to(['/r', 'i' => base64_encode($v['payment_id'])]), ['target' => '_blank']) : ''; ?></td>
                            <td data-name="<?= $v['trans_type'] . ' (' . number_format($v['amount'], 2) . ')' ?>"
                                data-toggle=popover
                                data-href="<?= Url::to(['/school-information/history', 'id' => $v['id']]) ?>"><i
                                        class="fa fa-arrow-circle-o-right"></i>&nbsp;&nbsp;
                            </td>
                            <td>
                                <?php if (Yii::$app->user->can('reverse_payment')) : ?>
                                    <a class="modal_link" href="javascript:"
                                       data-href="<?= Url::to(['/school-information/reverse-payment', 'id' => $v['payment_id']]) ?>"
                                       data-title="Reverse Payment - <?= number_format($v['amount'], 2) ?>"><i
                                                class="fa fa-close text-danger"></i>&nbsp;</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach;
                else : ?>
                    <td colspan="14">No Record found</td>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]); ?>

    </div>


<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

<?php

use app\modules\schoolcore\models\CoreBankDetails;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html; ?>

<div class="formz" style="    background: white;
    padding: 20px;color:#505050">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="row">

        <div class="col-sm-6">
            <?= $form->field($model, 'submitted_by')->textInput(['maxlength' => true, 'placeholder' => 'Full Name']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'submitter_number')->textInput(['maxlength' => true, 'placeholder' => 'Phone Number']) ?>
        </div>

        <div class="col-sm-6">
            <?=
            $form->field($model, 'country', [
                'inputOptions' => ['class' => 'form-control']
            ])
                ->dropDownList(['Uganda' => 'Uganda', 'Zambia' => 'Zambia', 'Kenya' => 'Kenya', 'Rwanda' => 'Rwanda'],
                    ['options' => ['Uganda' => ['Selected' => true]]])
            ?>        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'school_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => 'Village/Town, District'])->label('School Address(Village/Town, District)') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'primary_bank')->dropDownList(
                ArrayHelper::map($bk_data, 'id', 'bank_name'),
                ['prompt' => 'Select Bank']
            ) ?>


        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'account_title')->textInput() ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'account_number')->textInput() ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'badge_attached')->checkbox() ?>
        </div>


    </div>

    <hr/>

    <div class="row">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;School Contacts <span
                        style="font-weight: 300; font-size:18px;"> (Titles include: <span style="font-size: 14px;">BUR / HT / DIR / HM / PRP / ACC/ADMIN</span> only)
            </h3></div>
        <div class="col-sm-12">
            <?= $this->render('_contacts', ['model' => $model, 'id' => 1]); ?>
        </div>


        <br>


    </div>


    <hr>
    <div class="row">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Attach School Documents</h3>
            <br/>
            Please attach all files needed i.e. students list(doc, docx, pdf, xls, or xlsx) and school
            badge(png, jpg, jpeg, pdf)
        </div>


        <div class="col-sm-12">

            <?= FileInput::widget([
                'model' => $model,
                'attribute' => 'uploads[]',
                'name' => 'uploads[]',
                'options' => ['multiple' => true],
                'pluginOptions' => [
                    'previewFileType' => 'any',
                    'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'docx', 'doc', 'xls', 'xlsx', 'pdf', 'mp3', 'mp4'],
                    'showCancel' => false,
                    'showUpload' => false,
                    'maxFileCount' => 5,
                    'fileActionSettings' => [
                        'showZoom' => false,
                    ]
                ]
            ]); ?>
        </div>

    </div>


    <div class="form-group col-xs-12 col-sm-12 col-lg-12">
        <br>
        <hr class="l_header" style="margin-top:15px;">

    </div>


    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

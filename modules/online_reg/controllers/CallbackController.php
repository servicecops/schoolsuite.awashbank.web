<?php

namespace app\modules\online_reg\controllers;

use app\components\ToWords;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\User;
use app\models\UserSchoolAssociation;
use app\modules\banks\models\BankDetails;
use app\modules\logs\models\Logs;
use app\modules\online_reg\models\StudentOnlineRegistration;
use app\modules\online_reg\models\StudentOnlineRegistrationSearch;
use app\modules\reports\models\HomeStats;
use Yii;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CallbackController extends Controller
{

    public function beforeAction($action)
    {


//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }

//something code right here if user valid

        return true;

    }


    public function actionPamenC0llback()
    {
       // $this->layout = 'loginLayout';

        //send payment ref and phone number
        Yii::trace("inside");
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
;
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);

       $stdDetails = StudentOnlineRegistration::findOne(['online_registration_ref'=>$incomingRequest['paymentReference']]);
//        if($stdDetails!=$incomingRequest['externalPaymentReference']){
//        }

        $stdDetails->online_registration_payment_status =$incomingRequest['status'];
        $stdDetails->channel_name =$incomingRequest['channelName'];
        $stdDetails->reg_receipt_number =$incomingRequest['receiptNumber'];
        $stdDetails->online_registration_transactionId =$incomingRequest['transactionId'];
        $stdDetails->has_paid_reg_fees =true;
        $stdDetails->save(false);




    }


    public function actionClassStudentsList()
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $transaction = StudentOnlineRegistration::getDb()->beginTransaction();
            $searchModel = new StudentOnlineRegistrationSearch();

            //$pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            try{
                $data = $searchModel->searchPaid(Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $model = new StudentOnlineRegistration();

                if ($model->load(Yii::$app->request->post())) {



                    $dataz = Yii::$app->request->post();
                    $post = $dataz['Rollcall'];
                    $ClassDet = CoreSchoolClass::findOne([$classId]);

                    $attendance = Yii::$app->db->createCommand('select * from student_general_attendance where date_created >= now()::date + interval \'1h\' and class_id =:classId' )
                        ->bindValue(':classId', $classId)
                        ->queryAll();
                    Yii::trace($attendance);

                    if($attendance){
                        \Yii::$app->session->setFlash('appeal', "<div class='alert alert-danger'> General rollcall of Students in this class for the day is complete <br> </div>");

                        $transaction->rollBack();
                        return ($request->isAjax) ? $this->renderAjax('index_general_rollcall', ['classId' => $classId, 'model' => $model,
                            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'sort' => $data['sort']]) :
                            $this->render('index_general_rollcall', ['searchModel' => $searchModel, 'classId' => $classId, 'model' => $model,
                                'dataProvider' => $dataProvider, 'sort' => $data['sort']]);
                    }

                    foreach ($post as $K => $v) {
                        //check if student has already been rollcalled for the day



                        $model = new StudentDailyAttendance();
                        $model->teacher_id = Yii::$app->user->identity->getId();

                        $model->school_id = $ClassDet->school_id;
                        $model->class_id = $classId;
                        if (\app\components\ToWords::isSchoolUser()) {
                            $model->school_id = Yii::$app->user->identity->school_id;
                        }

                        Yii::trace($v);
                        $model->student_id = $v['studentid'];
                        if (isset($v['present'])) {
                            $model->present = true;
                        } else {
                            $model->present = false;

                        }
                        $model->save(false);




                    }


                    $transaction->commit();
                    if ($model->save(false)) {
                        Logs::logEvent("General Rollcalled Student for : " . $classId, null, $model->id);
                        \Yii::$app->session->setFlash('appeal', "<div class='alert alert-danger'>There was an issue sending your request try again later or call +256.414.597599<br> </div>");

                        return $this->redirect(['view-gen-student-attendance']);
                    }
                }




                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

                return ($request->isAjax) ? $this->renderAjax('paid_reg_original', [
                    'searchModel' => $searchModel, 'title'=>$title,'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                    $this->render('paid_reg_original', ['searchModel' => $searchModel,'title'=>$title,
                        'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'ROLLCALL  STUDENT ROLLBACK');
                Logs::logEvent("General Rollcalled Student for : " . $classId, $e->getMessage(), $model->id);

                \Yii::$app->session->setFlash('appeal', "<div class='alert alert-danger'>".$e->getMessage(). "<br> </div>");

            }


        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }
    }


}

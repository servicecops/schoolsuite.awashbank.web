<?php

namespace app\modules\online_reg\controllers;

use app\controllers\BaseController;
use app\models\ImageBank;
use app\modules\banks\models\BankAccountDetails;
use app\modules\banks\models\SelfEnrolledBankAccountDetails;
use app\modules\logs\models\Logs;
use app\modules\online_reg\models\CoreSchoolSelfRegistration;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\SchoolAccountTransactionHistorySearch;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolSearch;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\County;
use app\modules\schoolcore\models\District;
use app\modules\schoolcore\models\Parish;
use app\modules\schoolcore\models\SchoolChannel;
use app\modules\schoolcore\models\SchoolModules;
use app\modules\schoolcore\models\SchoolSections;
use app\modules\schoolcore\models\SubCounty;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\UserException;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CoreSchoolController implements the CRUD actions for CoreSchool model.
 */
class SchSelfRegController extends BaseController
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreSchool models.
     * @return mixed
     */
//    public function actionIndex()
////    {
////        $searchModel = new CoreSchoolSearch();
////        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
////        return $this->render('index', [
////            'searchModel' => $searchModel,
////            'dataProvider' => $dataProvider,
////        ]);
////    }

    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new CoreSchoolSelfRegistration();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }

    /**
     * Displays a single CoreSchool model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_sch', ['sch' => $model->id])) {
            $accounts = $this->getSelfEnrolledSchoolAccounts($id);
            Yii::trace('Number of accounts is ' . count($accounts));
            Yii::trace($accounts);
            $sections = $this->getSections($id);
            $module = SchoolModules::find()->where(['school_id' => $id])->all();
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
                'model' => $model,  'accounts' => $accounts, 'sections' => $sections, 'module' => $module,]) :
                $this->render('view', ['model' => $model,  'accounts' => $accounts, 'sections' => $sections,'module' => $module]);


        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }


    private function getSchoolAccounts($schId)
    {
        $accounts = (new Query())->from('core_bank_account_details ba')->select(['ba.id as bid', 'nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId]);
        return $accounts->all();
    }

    private function getSelfEnrolledSchoolAccounts($schId)
    {
        $accounts = (new Query())->from('self_enrolled_bank_account_details ba')->select(['ba.id as bid', 'nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId]);
        return $accounts->all();
    }

    private function getBanks()
    {
        $banks = (new Query())->from('core_nominated_bank')->select(['id', 'bank_name']);
        return $banks->all();
    }

    private function validateBankInfo($bank_accounts)

    {
        $error = null;

        foreach ($bank_accounts as $k => $v) {

            if (!$v) {
                $error = $k;

                Yii::trace($error);
            }
        }
        return $error;

    }

    /**
     * Finds the CoreSchool model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreSchool the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($id)
//    {
//        if (($model = CoreSchool::findOne($id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException('The requested page does not exist.');
//    }

    private function getSchoolBanks($schId)
    {
        $accounts = (new Query())->from('core_bank_account_details ba')->select(['nb.id', 'nb.id as bank_id', 'nb.bank_name'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId])
            ->distinct();
        return $accounts->all();
    }

    /**
     * Deletes an existing CoreSchool model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = CoreSchool::find()->where(['school_code' => $id])->limit(1)->one();
        if (Yii::$app->user->can('rw_sch', ['sch' => $model->id]) && Yii::$app->user->can('schoolsuite_admin') && Yii::$app->user->can('del_sch')) {

            try {
                Yii::$app->db->createCommand("select drop_school_details(:school_id)", [
                    ':school_id' => $id
                ])->execute();

                Logs::logEvent("Delete school: " . $model->school_name, null, null);
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Delete Student: " . $model->school_name . "(" . $model->school_code . ")", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/schoolcore/core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to delete school.');
        }
    }

    /**
     * @return CoreSchool
     */
    public function newModel()
    {
        $model = new CoreSchool();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    public function createModelFormTitle()
    {
        return 'Create A School';
    }

    private function saveAccounts($posted_banks, $sch_id)
    {
        foreach ($posted_banks as $k => $v) {
            $bankAcc = new CoreBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }
    private function saveSelfEnrolledAccounts($posted_banks, $sch_id)
    {
        foreach ($posted_banks as $k => $v) {
            $bankAcc = new SelfEnrolledBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateStudent()
    {

        $model = new CoreStudent();
        $request = Yii::$app->request;
        Yii::trace($model->load(Yii::$app->request->post()));
        if ($model->load(Yii::$app->request->post())) {

            $post = $model->load(Yii::$app->request->post());
            Yii::trace($post);
            $transaction = CoreStudent::getDb()->beginTransaction();
            try {
                //$newId =$this->saveUser();
                $model->created_by = Yii::$app->user->identity->getId();
                $model->save(false);


                $transaction->commit();
                //return $this->redirect(['view', 'id' => $model->id]);
                $searchModel = $this->newSearchModel();
                $schoolData = $searchModel->viewModel($model->id);
                $modeler = $schoolData['models'];
                $attributes = $schoolData['attributes'];
                return $this->render('@app/views/common/modal_view', [
                    'model' => $modeler,
                    'attributes' => $attributes
                ]);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE  School ROLLBACK');
            }


            //$res = ['model' => $model,  'banks' => $banks, 'bank_info_error' => $bank_info_error];
            //turn ($request->isAjax) ? $this->renderAjax('create') : $this->render('create');

        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
    }

    /**
     * @return CoreSchoolSearch
     */
    public function newSearchModel()
    {
        $searchModel = new CoreSchoolSearch();
        return $searchModel;
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $theSearchModel = $this->newSearchModel();
        $allData = $theSearchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $searchModel = $allData['searchModel'];
        $res = ['dataProvider' => $dataProvider, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionSchoollist($q = null, $id = null, $bank = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('core_school')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand(Yii::$app->db); //Search schools via backup db
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }

    public function actionActiveSchoollist($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('core_school')
                ->where(['active' => true])
                ->andWhere(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }

//filter school

    public function actionSchList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, school_name AS text')
                ->from('core_school')
                ->where(['ilike', 'school_name', $q])
                ->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find($id)->school_name];
        }
        return $out;
    }

//    county

    public function actionDistList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, district_name AS text')
                ->from('district')
                ->where(['ilike', 'district_name', $q])
                ->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => District::find($id)->district_name];
        }
        return $out;
    }


    public function actionDistricts($id)
    {
        $districts = District::find()
            ->where(['region_id' => $id])
            ->all();

        if (!empty($districts)) {
            echo '<option value="">Select district</option>';
            foreach ($districts as $district) {
                echo "<option value='" . $district->id . "'>" . $district->district_name . "</option>";
            }
        } else {
            echo "<option>No District found</option>";
        }

    }

    public function actionCountyLists($id)
    {
        $counties = County::find()
            ->where(['district_id' => $id])
            ->all();

        if (!empty($counties)) {
            echo '<option value="">choose county</option>';
            foreach ($counties as $county) {
                echo "<option value='" . $county->id . "'>" . $county->county_name . "</option>";
            }
        } else {
            echo "<option>No county found</option>";
        }

    }

    public function actionSubLists($id)
    {
        $subcounties = SubCounty::find()
            ->where(['county_id' => $id])
            ->all();

        if (!empty($subcounties)) {
            echo '<option value="">Select sub-County</option>';
            foreach ($subcounties as $subcounty) {
                echo "<option value='" . $subcounty->id . "'>" . $subcounty->sub_county_name . "</option>";
            }
        } else {
            echo "<option>No sub county found</option>";
        }

    }

    public function actionParishLists($id)
    {
        $parishes = Parish::find()
            ->where(['sub_county_id' => $id])
            ->all();

        if (!empty($parishes)) {
            echo '<option value="">Select Parish</option>';
            foreach ($parishes as $parish) {
                echo "<option value='" . $parish->id . "'>" . $parish->parish_name . "</option>";
            }
        } else {
            echo "<option>No Parishes found</option>";
        }

    }

    public function actionLogo($id)
    {
        if (!Yii::$app->user->can('super_admin')) {
            throw new ForbiddenHttpException('No permissions to create or update school logo.');
        }

        $model = $this->findModel($id);
        if (Yii::$app->user->can('super_admin') || Yii::$app->user->can('own_sch', ['sch' => $model->id]) || Yii::$app->user->can('edit_sch_logo')) {
            $request = Yii::$app->request;
            $model->scenario = 'photo';
            $imageBank = null;
            $imageBank = $model->school_logo ? ImageBank::find()->where(['id' => $model->school_logo])->limit(1)->one() : new ImageBank();
            try {
                if ($imageBank->load(Yii::$app->request->post())) {
                    $data = $_POST['ImageBank']['image_base64'];
                    list($type, $data) = explode(';', $data);
                    list(, $data) = explode(',', $data);
                    $imageBank->image_base64 = $data;
                    $imageBank->description = 'School Logo: ID - ' . $model->id;
                    if ($imageBank->save()) {
                        $model->school_logo = $imageBank->id;
                        $model->save(false);
                        Logs::logEvent("School logo changed: " . $model->school_name . "(" . $model->school_code . ")", null, null);
                        $this->redirect(['view', 'id' => $model->id]);
                    }
                }
                $res = ['imageBank' => $imageBank, 'model' => $model];
                return ($request->isAjax) ? $this->renderAjax('_sch_logo_croppie', $res) : $this->render('_sch_logo_croppie', $res);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Failed to upload school photo: " . $model->school_name . "(" . $model->school_code . ")", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/schoolcore/core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to create or update school logo.');
        }

    }


    private function updateSchAccounts($sch_id, $accounts, $posts)
    {
        Yii::trace('Db accounts is ' . print_r($accounts, true));
        Yii::trace('Posted accounts is ' . print_r($posts, true));
        $db_accounts = array_column($accounts, 'bid'); //id is in bid
        $post_accounts = array_column($posts, 'id');
        $diff_del = array_diff($db_accounts, $post_accounts);
        if (!empty($diff_del)) {
            foreach ($diff_del as $v) {
                $schAcc = CoreBankAccountDetails::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                $schAcc->delete();
            }
        }

        foreach ($posts as $k => $v) {
            $bankAcc = isset($v['id']) ? CoreBankAccountDetails::find()->where(['id' => $v['id']])->limit(1)->one() : new CoreBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;
            $bankAcc->save(false);
        }
    }


    public function actionSchoolDetail() {
        if (isset($_POST['expandRowKey'])) {
            $model = CoreSchool::findOne($_POST['expandRowKey']);
            return $this->renderPartial('_expand_row', ['model'=>$model]);
        } else {
            return '<div class="alert alert-danger">No data found</div>';
        }
    }

    public function actionSchools($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('school_account_id as id, school_name AS text')
                ->from('core_school')
                ->where(['ilike', 'school_name', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['school_account_id' => $id])->school_name];
        }
        return $out;
    }



    public function actionAccounthistory()
    {
        Yii::trace("in");
        $request = Yii::$app->request;
        if (Yii::$app->user->can('payments_received_list')) {
            $searchModel = new SchoolAccountTransactionHistorySearch();
            if (\app\components\ToWords::isSchoolUser()) {
                $schId = Yii::$app->user->identity->school->id;
                $model = $this->findModel($schId);
                $data = $searchModel->search($model->school_account_id, Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            } else {
                $data = $searchModel->search('', Yii::$app->request->queryParams);
                $dataProvider = $data['query'];
                $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            }

            return ($request->isAjax) ? $this->renderAjax('account_histories', [
                'dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('account_histories', ['dataProvider' => $dataProvider,
                    'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            echo "Hello " . Yii::$app->user->identity->fullname . ", you have no permission to view the payments history";
        }
    }



    public function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfSchoolTransactionHistory', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'School transaction history',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['School transaction history||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public function actionChannels($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_sch', ['sch' => $model->id])) {
            $request = Yii::$app->request;
            $available = array();
            $assigned = array();
            $channels = PaymentChannels::find()->all();

            foreach ($model->schChannels as $chn) {
                $assigned[$chn->payment_channel] = $chn->paymentChannel->channel_name;
            }

            foreach ($channels as $chn) {
                if (!isset($assigned[$chn->id])) {
                    $available[$chn->id] = $chn->channel_name;
                }
            }

            return ($request->isAjax) ? $this->renderAjax('channels', ['model' => $model,
                'available' => $available, 'assigned' => $assigned]) :
                $this->render('channels', ['model' => $model, 'available' => $available,
                    'assigned' => $assigned]);
        } else {
            throw new ForbiddenHttpException('No permissions to perform this action.');
        }
    }

    public function actionAssign($id, $action)
    {
        $error = [];
        if ($channels = $_POST['selected']) {
            if ($action == 'assign') {
                foreach ($channels as $i => $chn) {
                    try {
                        $item = new SchoolChannel();
                        $item->institution = $id;
                        $item->payment_channel = $chn;
                        $item->save(false);
                    } catch (\Exception $exc) {
                        $error[] = $exc->getMessage();
                    }
                }
            } else if ($action == 'delete') {
                Yii::trace(json_encode($channels), 'asign /delete channels');
                foreach ($channels as $i => $chn) {
                    try {
                        Yii::trace(json_encode($chn), 'delete channels');
                        $item = SchoolChannel::find()->where(['payment_channel' => $chn, 'institution' => $id])->one();
                        $item->delete();
                    } catch (\Exception $exc) {
                        $error[] = $exc->getMessage();
                    }
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$this->actionChannelSearch($id, 'available', ''),
            $this->actionChannelSearch($id, 'assigned', ''),
            $error];
    }

    public function actionChannelSearch($id, $target, $term = '')
    {
        $channels = PaymentChannels::find()->all();
        $model = $this->findModel($id);
        $available = [];
        $assigned = [];

        foreach ($model->schChannels as $chn) {
            $assigned[$chn->payment_channel] = $chn->paymentChannel->channel_name;
        }

        foreach ($channels as $chn) {
            if (!isset($assigned[$chn->id])) {
                $available[$chn->id] = $chn->channel_name;
            }
        }

        $result = [];
        $var = ${$target};
        if (!empty($term)) {
            foreach ($var as $i => $descr) {
                if (stripos($descr, $term) !== false) {
                    $result[$i] = $descr;
                }
            }
        } else {
            $result = $var;
        }

        return Html::renderSelectOptions('', $result);
    }

    public function actionFeeslists($id=0)
    {
        if(!$id) $id = 0;
        $db = Yii::$app->db;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $fees = (new Query())->select(['id', 'description'])
            ->from('institution_fees_due')
            ->where(['school_id' => $id])
            ->andWhere(['recurrent' => false])
            ->orderBy('description ASC')
            ->all($db);
        return ArrayHelper::map($fees, 'id', 'description');
    }



    /**
     * Creates a new SchoolInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $this->layout = 'loginlayout';

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $truckerDb= Yii::$app->db2;
        $request = Yii::$app->request;
        $model = new CoreSchoolSelfRegistration();
//        $model->default_part_payment_behaviour = false;
//        $model->active = true;
//        $model->enable_bank_statement = true;
        //$sp_modules = (new \yii\db\Query())->from('suite_modules')->orderBy('module_name')->all();

        $banks = $this->getBanks();
        $bank_info_error = [];
        $model_error = '';
        $bks = new Query;
        $bks->select('id, bank_name ')
            ->from('bank')
         ;

        $bk_data = $bks->all($truckerDb);
         Yii::trace($bk_data);

        try {
            if ($model->load(Yii::$app->request->post()) ) {

                $incoming = Yii::$app->request->post();
                $schoolDetails =$incoming['CoreSchoolSelfRegistration'];
              //  $schoolContacts =$incoming['Contacts']['sch_contacts'];
                //TODO: Begin transaction here
                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                //  Yii::trace($bank_info_error);


                Yii::trace($truckerDb);



                //Check school with same name and contact phone exists
                $query = new Query;
                $query->select('id, school_name AS text')
                    ->from('submission')
                    ->where(['school_name' => strtoupper(trim($model->school_name)),
                        'account_number' => $model->account_number])
                    ->limit(1);

                $data = $query->all($truckerDb);
                Yii::trace($data);


                if ($data)
                throw new Exception('School ' . $model->school_name . ' already exists');

                //get schoolpay channelcode
                $chn = PaymentChannels::findOne([$schoolDetails['primary_bank']]);
//                $query = new Query;
//                $query->select('id')
//                    ->from('banks');
//
//
//                $data = $query->all($truckerDb);





                $sql2 = "INSERT INTO submission (
                                        school_name,  
                                        address, 
                                        country,  
                                        primary_bank, 
                                        account_title, 
                                        account_number, 
                                        badge_attached,
                                        submitter,
                                        is_self_enrolled
                                        )
                                      VALUES (
                                       :school_name, 
                                        :address, 
                                        :country,  
                                        :primary_bank, 
                                        :account_title, 
                                        :account_number, 
                                        :badge_attached,
                                        :submitter,
                                        true
                                      )";

                //Prepare and populate params
                $query = $truckerDb->createCommand($sql2);
                $query->bindValue(':school_name', strtoupper(trim($schoolDetails['school_name'])));
                $query->bindValue(':address', $schoolDetails['address']);
                $query->bindValue(':country', $schoolDetails['country']);
                $query->bindValue(':primary_bank', $schoolDetails['primary_bank']);
                $query->bindValue(':account_title', strtoupper(trim($schoolDetails['account_title'])));
                $query->bindValue(':account_number', $schoolDetails['account_number']);
                $query->bindValue(':badge_attached', $schoolDetails['badge_attached']);
                $query->bindValue(':submitter', 91);

                //Excecute update now
                $query->execute();
                //Get last inserted schId
                $file_id = $truckerDb->getLastInsertID('submission_id_seq');



                //save submission

                //save contacts
                if(is_array($schoolDetails['contacts']) && $schoolDetails['contacts']){
                    foreach ($schoolDetails['contacts'] as $k=>$v) {
                        $sql1 = "INSERT INTO submission_contacts (
                              contact, 
                              contact_name, 
                              email, 
                              submission_id
                              )
                                VALUES (
                              :contact, 
                              :contact_name, 
                              :email, 
                              :submission_id
                                )";
                        $fileQuery = $truckerDb->createCommand($sql1);
                        $fileQuery->bindValue(':contact', $v['contact']);
                        $fileQuery->bindValue(':contact_name',  $v['contact_name']);
                        $fileQuery->bindValue(':email', $v['email']);
                        $fileQuery->bindValue(':submission_id', $file_id);
                        //Insert file
                        $fileQuery->execute();
                        Yii::trace('Saved contact ');
                    }
                }



                //save uploads

                Yii::trace($model->uploads);

                if (is_array($model->uploads) && $model->uploads) {
                    foreach ($model->uploads as $file) {
                        $fname = $file->baseName . '.' . $file->extension;
                        $tmpfile_contents = file_get_contents( $file->tempName );

                        $sql1 = "INSERT INTO submission_uploads (
                              submission, 
                              file_name, 
                              file_data
                              )
                                VALUES (
                              :submission, 
                              :file_name, 
                              :file_data
                                )";
                        $fileQuery2 = $truckerDb->createCommand($sql1);
                        $fileQuery2->bindValue(':submission', $file_id);
                        $fileQuery2->bindValue(':file_name',  $fname);
                        $fileQuery2->bindValue(':file_data',  base64_encode($tmpfile_contents));
                      //Insert file
                        $fileQuery2->execute();


                        Yii::trace('Saved file ' );
                    }
                }





                Yii::trace($model->id);

                $transaction->commit();
                Yii::$app->session->setFlash('uploadFailed', 'Thank you for submitting your school details , our Support team will contact you as soon as possible.');

                //   }
            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('uploadFailed', 'Failed to submit with error : ' . $e->getMessage(). '. Contact Schoolsuite support for help');
            Logs::logEvent("Error on submitting self enrolled school: " , $e->getMessage(), null);

        }

        $res = ['model' => $model,'bk_data'=>$bk_data, 'accounts' => $this->getSchoolAccounts($model->id), 'model_error' => $model_error,'banks' => $banks, 'bank_info_error' => $bank_info_error];
        Yii::trace($res);

        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }





}


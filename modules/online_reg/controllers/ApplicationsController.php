<?php

namespace app\modules\online_reg\controllers;

use app\components\Helpers;
use app\components\ToWords;
use app\models\ContactForm;
use app\models\ImageBank;
use app\models\User;
use app\modules\feesdue\models\FeesDue;
use app\modules\logs\models\Logs;
use app\modules\online_reg\models\SchOnlineApplicationRequirements;
use app\modules\online_reg\models\StudentOnlineRegistration;
use app\modules\online_reg\models\StudentOnlineRegistrationSearch;
use app\modules\paymentscore\models\OnlineStudentAccountHistorySearch;
use app\modules\paymentscore\models\StudentAccountHistorySearch;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use app\modules\schoolcore\models\CoreSubject;
use app\modules\schoolcore\models\CoreSubjectSearch;
use app\modules\schoolcore\models\Events;
use app\modules\schoolcore\models\EventsSearch;
use app\modules\schoolcore\models\OnlineTest;
use app\modules\schoolcore\models\SchoolModules;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * CoreStudentController implements the CRUD actions for CoreStudent model.
 */
class ApplicationsController extends Controller
{


    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



//    }

    /**
     * Creates a view.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $searchModel = new StudentOnlineRegistration();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('index', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('index', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }

    }

    /**
     * Deletes an existing CoreStudent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = CoreStudent::find()->where(['student_code' => $id])->limit(1)->one();

        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {

            try {
                Logs::logEvent("Delete Student: " . $model->fullname, null, $model->id);
                Yii::$app->db->createCommand("select drop_student_details(:student)", [':student' => $id])
                    ->execute();
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Delete Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('schoolcore/core-student/error');
            }
        } else {
            throw new ForbiddenHttpException('No permissions to delete this students.');
        }
    }
    /**
     *
     * /**
     * Updates an existing CoreStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_student', ['sch' => $model->school_id])) {
            $request = Yii::$app->request;
            $oldModel = clone $model;
            try {
                if ($model->load(Yii::$app->request->post())) {
                    $model->school_student_registration_number = trim(strtoupper($model->school_student_registration_number));
                    $model->first_name = ucwords(strtolower($model->first_name));
                    $model->middle_name = ucwords(strtolower($model->middle_name));
                    $model->last_name = ucwords(strtolower($model->last_name));
                    Yii::trace($model->last_name);
                    if ($model->save(false)) {
                        $edited = $this->diffObjects($oldModel, $model);
                        Logs::logEvent("Edited Student (" . $model->student_code . "): " . $edited, null, $model->id);
                        return $this->redirect(['view', 'id' => $model->id]);
                    }

                }
                return ($request->isAjax) ? $this->renderAjax('update', ['model' => $model]) :
                    $this->render('update', ['model' => $model]);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error on Update Student: " . $model->fullname . "(" . $model->student_code . ")", $error, $model->id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                //return \Yii::$app->runAction('/student/error');
            }

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }

    /**
     * Finds the CoreStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreStudent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentOnlineRegistration::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function diffObjects($oldModel, $newModel)
    {
        $newModel->date_of_birth = Date('Y-m-d', strtotime($oldModel->date_of_birth));
        $oldModel->allow_part_payments = (!$oldModel->allow_part_payments) ? 0 : 1;
        $oldModel->disability = (!$oldModel->disability) ? 0 : 1;
        $oldArray = ArrayHelper::toArray($oldModel, [], false);
        $newArray = ArrayHelper::toArray($newModel, [], false);
        $oldvalues = array_diff_assoc($oldArray, $newArray);
        $newvalues = array_diff_assoc($newArray, $oldArray);
        $edited = '';
        foreach ($oldvalues as $ok => $ov) {
            $edited .= " |<b>" . $ok . "</b> from: " . $ov . " to: " . $newvalues[$ok];
        }
        return $edited;
    }

//    protected function findReqSchModel($id)
//    {
//        if (($model = SchOnlineApplicationRequirements::findOne('school_id'=>$id)) !== null) {
//            return $model;
//        }
//
//        throw new NotFoundHttpException('The requested page does not exist.');
//    }

    public function actionViewStdSub($id)
    {

        $model = $this->findModel($id);
        $request = Yii::$app->request;

        $searchModel = new OnlineStudentAccountHistorySearch();
        $searchModel->only_received = true;
        $dataProvider = $searchModel->search($model->student_account_id, Yii::$app->request->queryParams);
        $res = ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel];
        return ($request->isAjax) ? $this->renderAjax('view_std_sub', $res) : $this->render('view_std_sub', $res);


    }


    public function actionViewProgress($id)
    {
        $model = $this->findModel($id);
        Yii::trace($model->has_requested_reg_prn);
        if($model->has_requested_reg_prn){
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('application_view', [
                'model' => $model  ]) :
                $this->render('application_view', ['model' => $model]);
        }
        else{
            $studentDetails = StudentOnlineRegistration::findOne(['id'=>$id]);
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('payment_options', ['studentDetails' => $studentDetails, ]) :
                $this->render('payment_options', ['studentDetails' => $studentDetails, ]);
        }





    }



    public function newModel()
    {
        // TODO: Implement newModel() method.
        return new CoreStudent();
    }

    public function createModelFormTitle()
    {
        return 'New Student';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return
     */

    public function actionApplicationForm($id)
    {

        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();
        $connection = Yii::$app->db;

        $schoolDetails = (new Query())->from('core_school cs')
            ->select(['cs.id', 'cs.school_name', 'st.code'])
            ->innerJoin('core_control_school_types st', 'st.id=cs.school_type')
            ->where(['cs.id' => $id]);
        $schoolDetails->all();

        return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model, 'id' => $id, 'schDetails' => $schoolDetails]) :
            $this->render('create', ['model' => $model, 'id' => $id, 'schDetails' => $schoolDetails]);


    }




    public function actionError()
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('student_error') : $this->render('student_error');
    }


    public
    function actionStudentProfile()
    {
        $searchModel = new StudentAccountHistorySearch();
        $searchModel->only_received = true;
        $dataProvider = $searchModel->search(Yii::$app->user->id, Yii::$app->request->queryParams);
        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
            'model' => $this->findModel(Yii::$app->user->id)]) :
            $this->render('view', [
                'model' => $this->findModel(Yii::$app->user->id),
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider
            ]);
    }

    public
    function actionChangePassword()
    {

        $request = Yii::$app->request;
        $model = $this->findModel(Yii::$app->user->id);
        $model->scenario = 'change';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $passwd = $data['CoreStudent']['new_pass'];
            $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
            $model->setPassword($passwd);
            if ($model->save(false)) {
                Logs::logEvent("User password reset(" . $model->id . "): " . $model->student_code, null, null);
                Yii::$app->session->setFlash('success', 'Thank you for using schoolsuite. Your password has been successfully reset.');
                return $this->redirect(['/site/index']);
            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('change_pass', ['model' => $model]) :
                $this->render('change_pass', ['model' => $model]);
        }


    }

    public
    function actionReset($id)
    {
        //Only schoolpay admins and users with rw_user can access this area
        if (!Yii::$app->user->can('rw_student') || !Yii::$app->user->can('schoolsuite_admin')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->scenario = 'reset';

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $passwd = $data['CoreStudent']['new_pass'];
            $model->locked = false;
//            $model->password_expiry_date = date('Y-m-d');
            $model->setPassword($passwd);
            try {
                $model->save(false);
                Logs::logEvent("Passwd reset for (" . $model->id . ") By Admin: " . $model->first_name, null, $model->id);
                //Send email to user on reset

                $url = Url::to('/login', true);
                //Send email to user on reset
                $fullname = $model->getFullname();
                $emailSubject = 'Schoolsuite password has been reset | ' . $fullname;
                $emailText = "Hello $fullname\n
            
            Greetings from  Awash ESchool!\n
            Your  Awash ESchool password for the username $model->student_code has been reset. \n
            Password: $passwd \n
            To login, go to $url\n
            If you did not request this reset, please contact your School administration or contact support team at: contactcenter@awashbank.com
            Thank you for choosing  Awash ESchool.";
                ToWords::sendEmail($model->student_email,
                    $emailSubject,
                    $emailText);

                return $this->redirect(['view', 'id' => $model->id]);
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->redirect('core-student/error');
            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('reset_pass', ['model' => $model]) :
                $this->render('reset_pass', ['model' => $model]);
        }

    }

    public
    function actionStudentIndex()
    {

        if (Yii::$app->user->can('non_student')) {
            throw new ForbiddenHttpException('Insufficient privileges to access this area.');
        }

        $events = Events::find()->all();
        foreach ($events as $event) {
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $event->id;
            $Event->title = $event->title;
            $Event->backgroundColor = '#e6838e';
            $Event->start = $event->event_date;
            $events[] = $Event;
        }

        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $studentModel = CoreStudent::find()->where(['id' => Yii::$app->user->id])->one();
        if ($studentModel) {
            $subjects = CoreSubject::find()->where(['class_id' => $studentModel->class_id])->all();
            return $this->render('student_index', [
                'events' => $events,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'subjects' => $subjects,
            ]);

        } else {
            return Yii::$app->runAction('/ste/logout');
        }

    }

    public
    function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],

                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public
    function actionEmail()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('student_email')
                    ->from('core_student');
                $command = $query->createCommand();
                $emails = $command->queryAll(\PDO::FETCH_COLUMN);
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if ($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                } else {
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to students: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->redirect('schoolcore/core-student/error');
            }

        }
        return $this->render('contact', ['model' => $model]);
    }

    public
    function actionSendEmail($id)
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            try {

                $senderAddress = isset(Yii::$app->params['EmailSenderAddress']) ? Yii::$app->params['EmailSenderAddress'] : 'info@schoolsuite.com';
                $senderName = isset(Yii::$app->params['EmailSenderName']) ? Yii::$app->params['EmailSenderName'] : "Schoolsuite";

                $query = new Query;
                $query->select('student_email')
                    ->from('core_student')
                    ->where(['id' => $id])->limit(1)->one();
                $command = $query->createCommand();
                $emails = $command->queryAll(\PDO::FETCH_COLUMN);

//                $email = (new Query())->select(['student_email'])->from('core_student')->where(['id' => $id])->limit(1)->one();
                $emailSubject = $model->subject;
                $email_body = $model->body;
                $model->attachment = UploadedFile::getInstances($model, 'attachment');

                if ($model->attachment) {
                    $message = \Yii::$app->mailer->compose()
                        ->setFrom([$senderAddress => $senderName])
                        ->setTo($emails)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($email_body);

                    foreach ($model->attachment as $file) {
                        $filename = 'uploads/' . $file->baseName . '.' . $file->extension;
                        $file->saveAs($filename);
                        $message->attach($filename);
                    }

                    $message->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                } else {
                    \Yii::$app->mailer->compose()
                        ->setTo($emails)
                        ->setFrom([$senderAddress => $senderName])
                        ->setSubject($emailSubject)
                        ->setTextBody($email_body)
                        ->send();
                    Yii::$app->session->setFlash('success', 'Email successfully sent.');

                }

            } catch (\Exception $e) {
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                $error = 'No email is attached to this student. Please add an email to the student and try again';
                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'SEND EMAIL ROLLBACK');
                Logs::logEvent("Error sending email to student: ", $error, $id);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->redirect('schoolcore/core-student/error');
            }

        }
        return $this->render('contact', ['model' => $model]);
    }


    public
    function actionExportPdfTrans($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportData();
        //print_r($data); exit;
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportTrasaction', ['model' => $data['data'], 'type' => $type,
                ]
            ),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print students transactions',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    /**
     * Creates a new SchoolInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public
    function actionSchApplicationRequirements()
    {

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchOnlineApplicationRequirements();


        try {
            if ($model->load(Yii::$app->request->post())) {
                $post = Yii::$app->request->post();
                $data = $post['SchOnlineApplicationRequirements'];
                $cls =$post['classes'];
                foreach($cls as $k){
                    Yii::trace($k);
                    $theclass = CoreSchoolClass::findOne(['id'=>$k]);
                    $theclass->has_slots= true;
                    $theclass->save(false);
                }



                $model->school_id = Yii::$app->user->identity->school_id;
                $model->reg_fees = $data['reg_fees'];
                $model->created_by = Yii::$app->user->identity->getId();
                $model->save(false);


                if ($model->save(false)) {
                    Logs::logEvent("Created New Requirement: ", null, null);
                   // $file_id = $connection->getLastInsertID('std_online_application_id_seq');

                    $model->uploads = UploadedFile::getInstances($model, 'uploads');

                    //save requirements
                    if (is_array($data['requirements']) && $data['requirements']) {
                        foreach ($data['requirements'] as $k => $v) {
                            $sql2 = "INSERT INTO school_submission_requiremments (
                            requirement, 
                              required, 
                               
                              requirement_id
                              )
                                VALUES (
                              :requirement, 
                              :required, 
                              :requirement_id
                                )";
                            $fileQuery = $connection->createCommand($sql2);
                            $fileQuery->bindValue(':requirement', $v['requirement']);
                            $fileQuery->bindValue(':required', $v['required']);
                            $fileQuery->bindValue(':requirement_id', $model->id);
                            //Insert file
                            $fileQuery->execute();
                            Yii::trace('Saved contact ');
                        }
                    }
                    //save uploads

                    Yii::trace($model->uploads);

                    if (is_array($model->uploads) && $model->uploads) {
                        foreach ($model->uploads as $file) {
                            $fname = $file->baseName . '.' . $file->extension;
                            $tmpfile_contents = file_get_contents($file->tempName);

                            $sql1 = "INSERT INTO sch_requirement_submission_uploads (
                              submission_id, 
                              file_name, 
                              file_data
                              )
                                VALUES (
                              :submission, 
                              :file_name, 
                              :file_data
                                )";
                            $fileQuery2 = $connection->createCommand($sql1);
                            $fileQuery2->bindValue(':submission', $model->id);
                            $fileQuery2->bindValue(':file_name', $fname);
                            $fileQuery2->bindValue(':file_data', base64_encode($tmpfile_contents));
                            //Insert file
                            $fileQuery2->execute();


                            Yii::trace('Saved file ');
                        }
                    }


                }


                $transaction->commit();
                return $this->redirect(['view-sch-requirements', 'id' => $model->id], 200);

                //   }
            }


        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Error on requirements creation: ", $model_error, null);
        }
        $schClass = (new \yii\db\Query())->from('core_school_class')->where(['school_id'=>Yii::$app->user->identity->school_id,'active'=>true])->all();

        Yii::trace($schClass);
        $res = ['model' => $model,'classes'=>$schClass ];


        return ($request->isAjax) ? $this->renderAjax('sch_requirements', $res) : $this->render('sch_requirements', $res);

    }


    public
    function actionViewSchRequirements($id)
    {
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('You have no permissions to view this area');
        }
        $request = Yii::$app->request;
        $submission = $this->findReqModel($id);
        $schoolDetails = CoreSchool::findOne([$id]);
        $schClass = (new \yii\db\Query())->from('core_school_class')->where(['school_id'=>Yii::$app->user->identity->school_id,'active'=>true,'has_slots'=>true])->all();
        $res = ['model' => $submission, 'sch' => $schoolDetails,'classes'=>$schClass];
        return $request->isAjax ? $this->renderAjax('_view_submission_info', $res) : $this->render('_view_submission_info', $res);
    }

    /**
     * Finds the CoreStudent model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return CoreStudent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findReqModel($id)
    {
        if (($model = SchOnlineApplicationRequirements::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public
    function actionOnlineApplication()
    {


        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();
        return $this->render('_view_online_application', ['model' => $model]);

    }
    public function actionSignup()
    {
        $this->layout = 'loginlayout';


        $request = Yii::$app->request;
        $model = new User(['scenario' => 'create']);
        $selectedVal = [];
        $schools = null;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $data = Yii::$app->request->post();
            $passwd = $data['User']['password'];

            $model->username = strtolower($data['User']['username']);
            $model->user_level = 'applicant_user';
            $model->setPassword($passwd);
            $model->save(false);


            $model->setAuthAssignment($model->user_level, strval($model->id));
            Logs::logEvent("New User Created(" . $model->id . "): " . $model->username, null, null);
            //Send user created email
            $fullname = $model->getFullname();
            $emailSubject = 'SchoolSuite Logins | ' . $fullname;

            $url = Url::to('SchpaySuite/site/login', true);

            $emailText = "Hello $fullname\n
            
            Greetings from Schoolsuite!\n
            The following are your credentials for  Awash ESchool\n
            Username: $model->username \n
            Password: $passwd \n
            To login, go to $url\n\n
            Thank you for choosing  Awash ESchool.";
            ToWords::sendEmail($model->email,
                $emailSubject,
                $emailText);
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please login to continue.');

            return $this->redirect(['/site/login']);

        }


        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return $request->isAjax ? $this->renderAjax('signup', $res) : $this->render('signup', $res);
    }

    /**
     * Returns schools that allow online reg
     * @return string
     */
    public
    function actionSchoolsList()
    {


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();

        try {
            if ($model->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();
                $data= $data['StudentOnlineRegistration']['level_of_interest'];
                $request = Yii::$app->request;
                $query1 = new Query;
                $_SESSION['level'] = $data;
                $query1->select('id')
                    ->from('suite_modules')
                    ->Where(['suite_modules.module_code' => 'STUDENT_ONLINE_APPLICATION']);

                $command = $query1->createCommand();
                $mod = $command->queryAll();
                if (!$mod) {
                    throw new Exception("No schools found that allow online registration");
                }
                $dataProvider = $model->searchschs(Yii::$app->request->queryParams, $mod);

                return ($request->isAjax) ? $this->renderAjax('_select_school', [
                    'model' => $model, 'dataProvider' => $dataProvider]) :
                    $this->render('_select_school', ['model' => $model,
                        'dataProvider' => $dataProvider]);
            }

            return ($request->isAjax) ? $this->renderAjax('applicant_preference', [
                'model' => $model, ]) :
                $this->render('applicant_preference', ['model' => $model,
                    ]);
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('error', 'Sorry, '.$model_error);
            $this->redirect(['online-application']);
            Logs::logEvent("Failed to select school: ", $model_error, null);
        }


    }

    public
    function actionActiveSchoollist($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query1 = new Query;

            $query1->select('id')
                ->from('suite_modules')
                ->Where(['suite_modules.module_code' => 'STUDENT_ONLINE_APPLICATION']);

            $command = $query1->createCommand();
            $mod = $command->queryAll();
            $mod = $mod[0];
            Yii::trace($mod);

            $query = new Query;
            $query->select('core_school.id, core_school.school_name AS text')
                ->from('core_school')
                ->innerJoin('school_module_association', 'core_school.id = school_module_association.school_id')
                ->where(['core_school.active' => true])
                ->andWhere(['school_module_association.module_id' => $mod['id']])
                ->andWhere(['ilike', 'core_school.school_name', $q])
                ->limit(7);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreSchool::find()->where(['id' => $id])->school_name];
        }
        return $out;
    }

    /**
     * Returns schools req
     * @return string
     */
    public
    function actionRequirements($id)
    {


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();

        try {

            $schoolDetails = CoreSchool::findOne([$id]);
            $list = SchOnlineApplicationRequirements::find()->where(['school_id' => $id])->limit(1)->one();
            if($list){
                $submission = $this->findReqModel($list->id);
                $res = ['model' => $submission, 'sch' => $schoolDetails];
                return $request->isAjax ? $this->renderAjax('_applicants_requirements', $res) : $this->render('_applicants_requirements', $res);

            }else {
                \Yii::$app->session->setFlash('access', "<div class='alert alert-danger'>No school requirements provided </div>");
                $res = ['model' => null, 'sch' => $schoolDetails];
                return $request->isAjax ? $this->renderAjax('_applicants_requirements', $res) : $this->render('_applicants_requirements', $res);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to select school: ", $model_error, null);
        }


    }

    public function actionEnrolledSubRes($id)
    {

        $model = new StudentOnlineRegistration();

        $details = $this->subDetails($id);


        $data = $details['data'];

        Yii::trace($details);
        Yii::trace($data);

        return $this->render('std_reg_confirmation', ['model' => $model, 'data' => $data]);
    }

    private function subDetails($id)
    {

        $query = new Query();

        $query->select('reg.id, reg.first_name, reg.last_name, reg.payment_ref_code,reg.student_email, cl.class_code,sch.school_name, bk.bank_name, ap.reg_fees')
            ->from('std_online_reg reg ')
            ->innerJoin('core_school sch', 'sch.id =reg.school_id')
            ->innerJoin('core_school_class cl', 'cl.id =reg.class_id')
            ->innerJoin('sch_online_application_requirements ap', 'ap.school_id =sch.id')
            ->innerJoin('core_nominated_bank bk', 'bk.id =sch.bank_name')
            ->Where(['reg.id' => $id]);


        $query->orderBy('reg.id desc');

        $columns = [
            'first_name',
            'last_name',
            'payment_ref_code',
            'class_code',
            'school_name',
            'bank_name',
            'student_email',
            'reg_fees'
        ];


        $commandt = $query->createCommand();
        $data = $commandt->queryAll();
        $pages = new Pagination(['totalCount' => $query->count()]);
        return ['columns' => $columns, 'data' => $data,];

    }

    public function actionApplicantLogin()
    {

        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            if ($model->load(Yii::$app->request->post())) {
                Yii::trace(Yii::$app->request->post());

                $post = Yii::$app->request->post();
                $username = $post['StudentOnlineRegistration']['payment_ref_code'];
                $applicant = StudentOnlineRegistration::findOne(['payment_ref_code' => $username]);
                Yii::trace($applicant);

                if ($applicant) {


                    Logs::logEvent("Student Applicant: " . $applicant->id, null, null);


                    return $this->redirect(['application-progress', 'id' => $applicant->id]);

                }
                \Yii::$app->session->setFlash('access', "<div class='alert alert-danger'>Inavlid Student Application code " . $username . "<br> </div>");

                return ($request->isAjax) ? $this->renderAjax('login', ['model' => $model]) :
                    $this->render('login', ['model' => $model]);

            }

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('access', 'Applicant Login Failed : ' . $e->getMessage() . '. Contact Schoolsuite support for help');
            Logs::logEvent("Applicant Login Failed: ", $e->getMessage(), null);

        }


        return ($request->isAjax) ? $this->renderAjax('login', ['model' => $model]) :
            $this->render('login', ['model' => $model]);


    }
    public function actionApplicationProgress($id)
    {

        $model = new StudentOnlineRegistration();

        $details = $this->subDetails($id);

        $data = $details['data'][0];

        Yii::trace($details);
        Yii::trace($data);

        return $this->render('application_progress', ['model' => $model, 'data' => $data]);
    }
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {
            $searchModel = new StudentAccountHistorySearch();
            $searchModel->only_received = true;
            $dataProvider = $searchModel->search($model->student_account_id, Yii::$app->request->queryParams);
            $res = ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel];
            return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }

    public function actionShortlistApplicant($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Yii::trace($model);
        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {


            $model->application_status ='SHORTLISTED';
            $model->save(false);
            return $this->redirect(['view','id'=>$id]);


        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }

    public function actionRejectApplication($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Yii::trace($model);
        if (Yii::$app->user->can('r_student', ['sch' => $model->school_id])) {


            $model->application_status ='REJECTED';
            $model->save(false);
            return $this->redirect(['view','id'=>$id]);


        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }

    public function actionAdmit($id){


    }




    public function actionImportToSuite($id)
    {
        $request = Yii::$app->request;
        $onlineModel = $this->findModel($id);
        Yii::trace($onlineModel);
        if (Yii::$app->user->can('r_student')) {

            $psw ='abc123';
            $model = new CoreStudent();
            $model->active = true;
            $model->allow_part_payments = false;
            $model->nationality = 'Ugandan';
            $model->created_by = Yii::$app->user->identity->getId();
                $model->school_student_registration_number = trim(strtoupper($onlineModel->school_student_registration_number));
                $model->first_name = ucwords(strtolower($onlineModel->first_name));
                $model->middle_name = ucwords(strtolower($onlineModel->middle_name));
                $model->last_name = ucwords(strtolower($onlineModel->last_name));
                $model->class_id = ucwords(strtolower($onlineModel->class_id));
                $model->student_email = ucwords(strtolower($onlineModel->student_email));
                $model->student_phone = ucwords(strtolower($onlineModel->student_phone));
                $model->gender = ucwords(strtolower($onlineModel->gender));
                $model->date_of_birth = ucwords(strtolower($onlineModel->date_of_birth));
                $model->nationality = ucwords(strtolower($onlineModel->nationality));
                $model->disability = ucwords(strtolower($onlineModel->disability));
                $model->guardian_name = ucwords(strtolower($onlineModel->guardian_name));
                $model->guardian_relation = ucwords(strtolower($onlineModel->guardian_relation));
                $model->guardian_email = ucwords(strtolower($onlineModel->guardian_email));
                $model->guardian_phone = ucwords(strtolower($onlineModel->guardian_phone));
                $model->day_boarding = ucwords(strtolower($onlineModel->day_boarding));
                $model->created_by = ucwords(strtolower($onlineModel->created_by));
                $model->auth_key = ucwords(strtolower($onlineModel->auth_key));
                $model->verification_token = ucwords(strtolower($onlineModel->verification_token));


                $model->setPassword($psw);

                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                if ($model->save(false)) {
                    Logs::logEvent("Created New file: " , null, $model->id);

                    $onlineModel->application_status ='ACCEPTED';
                    $onlineModel->save(false);


                    return $this->redirect(['/schoolcore/core-student/view', 'id' => $model->id]);
                }


        } else {
            throw new ForbiddenHttpException('No permissions to view this student.');
        }
    }

    public function actionCheckSlots(){

        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = new StudentOnlineRegistration();
        try {

            if ($model->load(Yii::$app->request->post())) {
                Yii::trace(Yii::$app->request->post());

                $post = Yii::$app->request->post();
                $clsId = $post['StudentOnlineRegistration']['class_id'];
                $slots = CoreSchoolClass::findOne(['id' => $clsId]);
               $schoolDetails = CoreSchool::findOne([$slots->school_id]);

//                $schoolDetails = (new Query())->from('core_school cs')
//                    ->select(['cs.id', 'cs.school_name', 'st.code'])
//                    ->innerJoin('core_control_school_types st', 'st.id=cs.school_type')
//                    ->where(['cs.id' => $slots->school_id]);
//                $schoolDetails->all();
//                Yii::trace($slots);

                if ($slots->has_slots) {

                    return ($request->isAjax) ? $this->renderAjax('_form', ['model' => $model,'classId'=>$slots->id,'schoolDetails'=>$schoolDetails]) :
                        $this->render('_form', ['model' => $model,'classId'=>$slots->id, 'schoolDetails'=>$schoolDetails]);
                }
                \Yii::$app->session->setFlash('viewError', "<div class='alert alert-danger'>No slots available in " . $slots->class_description . "<br> </div>");

                return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model,'id'=>$slots->school_id,'schDetails'=>$schoolDetails]) :
                    $this->render('create', ['model' => $model,'id'=>$slots->school_id,'schDetails'=>$schoolDetails]);

            }

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('access', 'Applicant Login Failed : ' . $e->getMessage() . '. Contact Schoolsuite support for help');
            Logs::logEvent("Applicant Login Failed: ", $e->getMessage(), null);

        }


    }

    public function actionSubmitForm()
    {

        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            if ($model->load(Yii::$app->request->post())) {

                $data =Yii::$app->request->post();

                Yii::trace($data);
                $level = $_SESSION['level'];
                Yii::trace($level);
                $model->class_id = $data['Classes']['class_id'];
                $model->first_name = ucwords(strtolower($model->first_name));
                $model->middle_name = ucwords(strtolower($model->middle_name));
                $model->last_name = ucwords(strtolower($model->last_name));
                $model->school_id = $data['Classes']['school_id'];
                $model->created_by = Yii::$app->user->identity->id;
                $model->level_of_interest = $level;

                $model->uce_results = $data['UCE_Results'];
                $model->ple_results = $data['PLE_Results'];
                $model->uce_index_number = $data['StudentOnlineRegistration']['uce_index_number'];
                $model->ple_index_number = $data['StudentOnlineRegistration']['ple_index_number'];
                $model->uce_aggregates = $data['StudentOnlineRegistration']['uce_aggregates'];
                $model->ple_aggregates = $data['StudentOnlineRegistration']['ple_aggregates'];

                //$model->setPassword($model->password);
                $model->application_status ='PENDING PAYMENT';
                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                $model->save(false);
                $model->saveUploads();

                $transaction->commit();
                unset($_SESSION['level']);

                $stdDetails =$model;
                Yii::trace($stdDetails);
                $applicationDetails = SchOnlineApplicationRequirements::findOne(['school_id' => $model->school_id ]);

                if (!$applicationDetails || !$applicationDetails->reg_fees) {
                    \Yii::$app->session->setFlash('viewError', "<div class='alert alert-danger'>Successful, your data has been submitted. School does not require Registration fees  <br> </div>");

                    return $this->redirect(['index']);

                }

                Yii::$app->session->setFlash('success', 'Thank you for registration. Please you need to pay registration fee of Birr '.$applicationDetails->reg_fees.'  for your application to be considered.');
                return $this->redirect(['payment-options', 'stdDetails' => $model->id]);



            }

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('uploadFailed', 'Failed to submit with error : ' . $e->getMessage() . '. Contact Schoolsuite support for help');
            Logs::logEvent("Error on submitting online registration enrolled school: ", $e->getMessage(), null);

        }




    }


    public function actionPaymentOptions($stdDetails)
    {
        //  Yii::trace("std Detail".$stdDetails);

        $studentDetails = StudentOnlineRegistration::findOne(['id'=>$stdDetails]);

        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('payment_options', ['studentDetails' => $studentDetails, ]) :
            $this->render('payment_options', ['studentDetails' => $studentDetails, ]);

    }






    public function actionPayImmediately($studentDetails)
    {



        //send payment ref and phone number
        $request = Yii::$app->request;
        $model = new StudentOnlineRegistration();
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            if ($model->load(Yii::$app->request->post())) {

                $request = Yii::$app->request;


                $client = new Client([
                    'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
                ]);

                $data = Yii::$app->request->post();
                $phoneNumber = $data['StudentOnlineRegistration']['guardian_phone'];

                $std = StudentOnlineRegistration::findOne(['id' => $studentDetails]);

                $schDetails = CoreSchool::findOne(['id' => $std->school_id]);

                Yii::trace($schDetails);
                $applicationDetails = SchOnlineApplicationRequirements::findOne(['school_id' => $std->school_id]);

                Yii::trace($schDetails);

                $SchoolCode = $schDetails->schoolpay_schoolcode;
                $ExternalRef = $std->id . $std->class_id.'-Registration';

                $Password = $schDetails->transaction_api_password;
                $hash = MD5($SchoolCode . $ExternalRef . $Password);

                Yii::trace($ExternalRef);

                if (!$applicationDetails || !$applicationDetails->reg_fees) {
                    \Yii::$app->session->setFlash('viewError', "<div class='alert alert-danger'>Error, School has no registration fees set <br> </div>");

                    return $this->redirect(['payment-options', 'stdDetails' => $studentDetails]);;

                }




                    $requestData = ['amount' => $applicationDetails->reg_fees, 'firstName' => $std->first_name, 'lastName' => $std->last_name,
                        'externalReference' => $ExternalRef, 'reason' => 'Registration', 'phoneNumber' => $phoneNumber, 'callBackUrl' => 'https://testsystem.com/receivepaymentstatus/55”'];

                    // $baseUrl = Yii::$app->params['BIBCbsBaseUrl'];
                    $url = 'https://schoolpay.co.ug/uatpaymentapi/AndroidRS/AdhocPayments/Request/' . $SchoolCode . '/' . $hash;
                    $req = json_encode($requestData);
                    Yii::trace('Connecting to  ' . $url . ' to send ' . $req);


                    $response = $client->createRequest()
                        ->setMethod('POST')
                        ->setUrl($url)
                        ->addHeaders(['content-type' => 'application/json'])
                        ->setContent($req)
                        ->setOptions(['sslVerifyPeer' => false,
                            CURLOPT_SSL_VERIFYHOST => false,
                            CURLOPT_SSL_VERIFYPEER => false])
                        ->send();

                    $content = $response->getContent();

                    if (!$response->isOk) {
                        return (object)['responseCode' => 500, 'responseText' => 'Could not connect to Schoolpay service. Please try again later. HTTP Code: ' . $response->getStatusCode()];
                    }

                    //update seeta side and payment received

                    $json_decode = json_decode($content);
                    Yii::trace($json_decode);
                    $paymentResponse = $json_decode;

                    if ($paymentResponse->returnCode == 0) {
                        $std->online_registration_ref = $paymentResponse->paymentReference;
                        $std->online_registration_payment_status = $paymentResponse->status;
                        $std->has_requested_reg_prn =true;

                        $std->save(false);
                        Yii::trace( $std->online_registration_ref);
                    } else {
                        \Yii::$app->session->setFlash('viewError', "<div class='alert alert-danger'>Error " . $paymentResponse->returnMessage . "<br> </div>");

                        return $this->redirect(['payment-options', 'stdDetails' => $studentDetails]);;

                    }

                    $transaction->commit();
                    return ($request->isAjax) ? $this->renderAjax('pay_immediately', [
                        'resp' => $paymentResponse, 'phone' => $std->guardian_phone]) :
                        $this->render('pay_immediately', ['resp' => $paymentResponse, 'phone' => $std->guardian_phone]);

                }

            return ($request->isAjax) ? $this->renderAjax('phone_number', ['model'=>$model, 'studentDetails' => $studentDetails]):
                $this->render('phone_number', ['studentDetails' => $studentDetails,'model'=>$model, ]);


        } catch (\Exception $e) {
                Yii::trace($e);
                $transaction->rollBack();
                $model_error = $e->getMessage();
                Yii::$app->session->setFlash('uploadFailed', 'Failed to submit with error : ' . $e->getMessage() . '. Contact Schoolsuite support for help');
                Logs::logEvent("Error on submitting online registration enrolled school: ", $e->getMessage(), null);

            }

    }

    public function actionPayLater($studentDetails)
    {

        //send payment ref and phone number

        $request = Yii::$app->request;


        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);


        $std= StudentOnlineRegistration::findOne(['id'=>$studentDetails]);

        $schDetails = CoreSchool::findOne(['id'=>$std->school_id]);

        Yii::trace($schDetails);
        $applicationDetails =SchOnlineApplicationRequirements::findOne(['school_id'=>$std->school_id]);




        $SchoolCode= $schDetails->schoolpay_schoolcode;
        $ExternalRef=$std->id.$std->class_id.'-Registration';

        $Password= $schDetails->transaction_api_password;
        $hash = MD5($SchoolCode . $ExternalRef . $Password);

        Yii::trace($ExternalRef);

        $requestData= ['amount'=>$applicationDetails->reg_fees,'firstName'=>$std->first_name,'lastName'=>$std->last_name,
            'externalReference'=>$ExternalRef,'reason'=>'Registration 2021', 'callBackUrl'=>'https://testsystem.com/receivepaymentstatus/55”'];

        // $baseUrl = Yii::$app->params['BIBCbsBaseUrl'];
       $url = 'https://schoolpay.co.ug/uatpaymentapi/AndroidRS/AdhocPayments/Register/'.$SchoolCode.'/'.$hash;
        $req = json_encode($requestData);
        Yii::trace('Connecting to  ' . $url . ' to send ' . $req);


        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($req)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        $content = $response->getContent();

        if (!$response->isOk) {
            return (object) ['responseCode' => 500, 'responseText' => 'Could not connect to Schoolpay service. Please try again later. HTTP Code: '. $response->getStatusCode()];
        }

        //update seeta side and payment received

        $json_decode = json_decode($content);
        Yii::trace($json_decode);
        $paymentResponse =$json_decode;

        if($paymentResponse->returnCode == 0){
            $std->online_registration_ref =$paymentResponse->paymentReference;
            $std->online_registration_payment_status =$paymentResponse->status;
            $std->has_requested_reg_prn =true;

            $std->save(false);


        }else{
            \Yii::$app->session->setFlash('viewError', "<div class='alert alert-danger'>Error " . $paymentResponse->returnMessage . "<br> </div>");

            return $this->redirect(['payment-options', 'stdDetails' => $studentDetails]);;

        }


        return ($request->isAjax) ? $this->renderAjax('pay_later', [
            'resp' => $paymentResponse]) :
            $this->render('pay_later', ['resp' => $paymentResponse]);


    }

    public function actionAdmissionCode($studentDetails)
    {
//send payment ref and phone number

        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $model = new StudentOnlineRegistration();

        try {
            if ($model->load(Yii::$app->request->post())) {
                $client = new Client([
                    'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
                ]);
                $std= StudentOnlineRegistration::findOne(['id'=>$studentDetails]);


                $schDetails = CoreSchool::findOne(['id'=>$std->school_id]);

                Yii::trace($schDetails);
                $applicationDetails =SchOnlineApplicationRequirements::findOne(['school_id'=>$std->school_id]);

                $data = Yii::$app->request->post();
                $admissionAmount = $data['StudentOnlineRegistration']['admission_amount'];


                $SchoolCode= $schDetails->schoolpay_schoolcode;


                $ExternalRef= $std->id.$std->class_id.'-Admission';
                Yii::trace($ExternalRef);
                $Password= $schDetails->transaction_api_password;
                $hash = MD5($SchoolCode .$ExternalRef.$Password);



                $requestData= ['amount'=>$admissionAmount,'firstName'=>$std->first_name,'lastName'=>$std->last_name,
                    'externalReference'=>$ExternalRef,'reason'=>'Admissions', 'callBackUrl'=>'https://testsystem.com/receivepaymentstatus/55”'];

                // $baseUrl = Yii::$app->params['BIBCbsBaseUrl'];
                $url = 'https://schoolpay.co.ug/uatpaymentapi/AndroidRS/AdhocPayments/Register/'.$SchoolCode.'/'.$hash;
                $req = json_encode($requestData);
                Yii::trace('Connecting to  ' . $url . ' to send ' . $req);


                $response = $client->createRequest()
                    ->setMethod('POST')
                    ->setUrl($url)
                    ->addHeaders(['content-type' => 'application/json'])
                    ->setContent($req)
                    ->setOptions(['sslVerifyPeer' => false,
                        CURLOPT_SSL_VERIFYHOST => false,
                        CURLOPT_SSL_VERIFYPEER => false])
                    ->send();

                $content = $response->getContent();

                if (!$response->isOk) {
                    return (object) ['responseCode' => 500, 'responseText' => 'Could not connect to Schoolpay service. Please try again later. HTTP Code: '. $response->getStatusCode()];
                }



                $json_decode = json_decode($content);
                Yii::trace($json_decode);
                $paymentResponse =$json_decode;




                if ($paymentResponse->returnCode == 0) {
                    $std->online_admission_ref =$paymentResponse->paymentReference;
                    $std->online_admission_payment_status =$paymentResponse->status;
                    $std->application_status ='ADMITTED';

                    $std->save(false);
                } else {
                    \Yii::$app->session->setFlash('viewError', "<div class='alert alert-danger'>Error " . $paymentResponse->returnMessage . "<br> </div>");

                    return $this->redirect(['payment-options', 'stdDetails' => $studentDetails]);;

                }

                return ($request->isAjax) ? $this->renderAjax('admission_note', [
                    'resp' => $paymentResponse]) :
                    $this->render('admission_note', ['resp' => $paymentResponse]);

            }
            return ($request->isAjax) ? $this->renderAjax('admission_amount', ['model'=>$model, 'studentDetails' => $studentDetails]):
                $this->render('admission_amount', ['studentDetails' => $studentDetails,'model'=>$model, ]);


        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Yii::$app->session->setFlash('uploadFailed', 'Failed to submit with error : ' . $e->getMessage() . '. Contact Schoolsuite support for help');
            Logs::logEvent("Error requesting admission code: ", $e->getMessage(), null);

        }



    }


    public function actionCheckPayment()
    {

        //send payment ref and phone number


        $request = Yii::$app->request;

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);
        $std= StudentOnlineRegistration::find()->where(['online_registration_payment_status'=>'PENDING'])->all();



        foreach($std as $k){
            Yii::trace($k['id']);



            $schDetails = CoreSchool::findOne(['id'=>$std->school_id]);


            $SchoolCode= $schDetails->schoolpay_schoolcode;

            $ExternalRef= $k['online_registration_ref'];
            $Password= $schDetails->transaction_api_password;
            $hash = MD5($SchoolCode .$ExternalRef .$Password);

            $url = 'https://schoolpay.co.ug/uatpaymentapi/AndroidRS/AdhocPayments/Check/'.$SchoolCode.'/'.$hash.'/'.$k['online_registration_ref'];

            ;

            Yii::trace('Connecting to  ' . $url . ' to send ' );


            $response = $client->createRequest()
                ->setMethod('Get')
                ->setUrl($url)
                ->addHeaders(['content-type' => 'application/json'])

                ->setOptions(['sslVerifyPeer' => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false])
                ->send();

            $content = $response->getContent();

            if (!$response->isOk) {
                return (object) ['responseCode' => 500, 'responseText' => 'Could not connect to Schoolpay service. Please try again later. HTTP Code: '. $response->getStatusCode()];
            }



            $json_decode = json_decode($content);
            Yii::trace($json_decode);
            $paymentResponse =$json_decode;

            if($paymentResponse->status =='PAID') {

                $theStd = StudentOnlineRegistration::findOne(['id' => $k['id']]);

                $theStd->online_registration_payment_status = $paymentResponse->status;
                $theStd->channel_name = $paymentResponse->channelName;
                $theStd->receipt_number = $paymentResponse->receiptNumber;
                $theStd->online_registration_transactionId = $paymentResponse->transactionId;
                $theStd->save(false);
            }

        }


    }






    public function actionPaymentCallback()
    {

        //send payment ref and phone number
        Yii::trace("inside");
        $raw_data = Yii::$app->request->getRawBody();
        $incomingRequest = json_decode($raw_data, true);
        Yii::trace($raw_data);
        Yii::trace($incomingRequest);


        //save payment details



    }
    /**
     * Creates a view.
     * @return mixed
     */
    public function actionPaidRegistration()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of Students who have  paid Registration Fee';

        $data = $searchModel->searchPaid(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg_original', [
            'searchModel' => $searchModel, 'title'=>$title,'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('paid_reg_original', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }


    /**
     * Creates a view.
     * @return mixed
     */
    public function actionNotpaidRegistration()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of Students who have not yet paid Registration Fee';

        $data = $searchModel->searchUnpaid(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg', [
            'searchModel' => $searchModel,'title'=>$title, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('paid_reg', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider,'title'=>$title, 'pages' => $pages, 'sort' => $data['sort']]);


    }


    /**
     * Creates a view.
     * @return mixed
     */
    public function actionPaidAdmission()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of Students who have paid Admission Fee';

        $data = $searchModel->searchPaidAdmission(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider,'title'=>$title, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('paid_reg', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider,'title'=>$title, 'pages' => $pages, 'sort' => $data['sort']]);


    }

    public function actionAllRegistration()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of all Students who applied';

        $data = $searchModel->searchAllAdmission(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg', [ 'title'=>$title,
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('paid_reg', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }

    /**
     * Creates a view.
     * @return mixed
     */
    public function actionNotpaidAdmission()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of Students who have not yet paid Admission Fee';

        $data = $searchModel->searchUnpaidAdmission(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages,'title'=>$title, 'sort' => $data['sort']]) :
            $this->render('paid_reg', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }

    public function actionAdmitted()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of all Admitted Students ';

        $data = $searchModel->searchAdmitted(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg', [ 'title'=>$title,
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('paid_reg', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }

    /**
     * Creates a view.
     * @return mixed
     */
    public function actionRejected()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of Students who have been Rejected';
        $data = $searchModel->searchRejected(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_reg', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages,'title'=>$title, 'sort' => $data['sort']]) :
            $this->render('paid_reg', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }



    /**
     * Displays a single SchoolInformation model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewApplication($id)
    {
        $model = $this->findModel($id);

        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view_student_application', [
            'model' => $model]) :
            $this->render('view_student_application', ['model' => $model, ]);


    }

    public function actionPaidView($id)
    {
        $model = $this->findModel($id);

        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('paid_view', [
            'model' => $model]) :
            $this->render('paid_view', ['model' => $model, ]);


    }

    public function actionReject($id)
    {
        $model = $this->findModel($id);
        $model->application_status ='REJECTED';
        $model->save(false);
        return $this->redirect(['/site/index']);


    }
    /**
     * Creates a view.
     * @return mixed
     */
    public function actionPaymentsReceived()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of Payments Received';
        $data = $searchModel->searchPaymentsReceived(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('paid_ref', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages,'title'=>$title, 'sort' => $data['sort']]) :
            $this->render('paid_ref', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }

    public function actionProgressCheck()
    {
        $request = Yii::$app->request;
        Yii::trace(Yii::$app->user);
        $searchModel = new StudentOnlineRegistrationSearch();
        $title= 'List of My Applications';
        $data = $searchModel->searchProgress(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('progress', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages,'title'=>$title, 'sort' => $data['sort']]) :
            $this->render('progress', ['searchModel' => $searchModel,'title'=>$title,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);


    }



}

<?php
namespace app\modules\online_reg;

class OnlineRegistrationModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\online_reg\controllers';
    public function init() {
        parent::init();
    }
}

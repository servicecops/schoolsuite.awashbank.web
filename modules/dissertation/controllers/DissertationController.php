<?php

namespace app\modules\dissertation\controllers;

use Yii;
use app\controllers\BaseController;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\logs\models\Logs;
use app\modules\dissertation\models\Dissertation;
use app\modules\dissertation\models\DissertationSearch;
use app\modules\dissertation\models\DissertationUploads;
use app\modules\schoolcore\models\CoreStudent;
use yii\web\UploadedFile;




if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * DissertationController that handles the lesson plan/syllabus
 */
class DissertationController extends BaseController
{
     

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Display the dissertation
     */
    public function actionIndex(){
        
        $request = Yii::$app->request;

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }


        $searchModel = new DissertationSearch();
        $searchParams = Yii::$app->request->queryParams;
        $schoolId = Yii::$app->user->identity->school_id;
        $searchParams['school_id'] = $schoolId;
        $searchParams['active'] = true;
        Yii::trace($searchParams);

        $studentDetail = $this->determineIfStudent();
        if($studentDetail && $studentDetail['is_student'] == true){
            $studentId = $studentDetail['student_id'];
            $searchParams['student_id'] =  $studentId;
            $dataProvider = $searchModel->search($searchParams);

        }else{
            //school admin has access to all dissertations
            $dataProvider = $searchModel->search($searchParams);
        }

        return ($request->isAjax) ? $this->renderAjax('index', [
                     'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                            'dataProvider' => $dataProvider]);
    }


    //you must override these methods to use BaseControler.
    /**
     * Creates a new Dissertation model
     * @return Dissertation
     */
    public function newModel()
    {
        $model = new Dissertation();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return DissertationSearch
     */
    public function newSearchModel()
    {
        $searchModel = new DissertationSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create Dissertation';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }


     /**
     * Creates a new Dissertation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {   

//        if (Yii::$app->user->can('non_student')) {
//            throw new ForbiddenHttpException('No permission to create or upload the dissertation / publications. Only students are authorised to do so');
//        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new Dissertation();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();
                //if u own the school
                $schoolId = Yii::$app->user->identity->school_id;
                $model->school_id = $schoolId;

                $studentDetail = $this->determineIfStudent();

                if($studentDetail && $studentDetail['is_student'] == true){
                     $model->student_id = $studentDetail['student_id'];
                }else{
                    throw new ForbiddenHttpException('No student details found');
                   // $res = ['model' => $model];
                   // return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


                }

                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                if( $model->save(false) ){
                    Logs::logEvent("Created Dissertation : " . $model->id, null, null);
                    $model->saveUploads(); //fix the upload issue here                   
                }
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

             \Yii::$app->session->setFlash('viewError', $error);
            Logs::logEvent("Failed create new dissertation : ", $error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }


    /**
     * Displays a single Dissertation  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {  
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') )
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);



        $responseData = [
            'model' => $model
        ];

        return ((Yii::$app->request->isAjax)) ? 
            $this->renderAjax('view',$responseData) 
                :
            $this->render('view', $responseData);


    }


    /**
     * Updates an existing Dissertation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        if (Yii::$app->user->can('non_student')) {
            throw new ForbiddenHttpException('No permission to Update the dissertation/publication. Only students are authorised to do so');
        }

        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post())) {

                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                if( $model->save(false) ){
                    Logs::logEvent("Updated the Dissertation: " . $model->id, null, null);
                    //$model->saveAndUpdateUploads(); //ofcourse first delete old uploads n upload new ones
                    $model->saveUploads();                   
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }


        //$model->date_created = ($model->date_created)? date('Y-m-d', strtotime($model->date_created)) : '' ;

        return $this->render('update', [
            'model' => $model,
        ]);
    }


     /**
     * View/Download Document
     * @param integer $id
     * @return mixed
     */
    public function actionViewFile($id){
        $response = Yii::$app->response;
        $model = DissertationUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'pdf']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        if ($ext == 'pdf') {
            $response->headers->set('Content-type', 'application/pdf');
        } else {
            $response->headers->set('Content-type', $ext);
        }
           
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);

        return $response;
    }

   
     /**
     * Remove the uploaded file / Document
     * @param integer $id
     * @return mixed
     */
    public function actionRemoveFile($id){
        //allow only the student to delete the file
        if (!\Yii::$app->user->can('non_student')) {
            $model = DissertationUploads::find()->where(['id' => $id])->limit(1)->one();
            if ($model) {
                $dissertationId = $model->submission_id;
                $model->delete();
                return $this->redirect(['view', 'id' => $dissertationId ]);
            }else{
               throw new NotFoundHttpException('The uploaded dissertation or publication does not exist');
            }
        } else {
            throw new ForbiddenHttpException('You have no permission to delete the uploaded files. Only students are allowed');
        }
    }


    /**
     * Determine whether logged in  user is student
     */
    private function determineIfStudent(){
        $result = [
           'is_student'=> false,
           'student_id' => 2
        ];
        $username = Yii::$app->user->identity->username;
        if ($username && is_numeric($username)) {
            $studentDetail = CoreStudent::findByUsername($username);
            if ($studentDetail && $studentDetail->id) {
                $result['is_student'] = true;
                $result['student_id'] = $studentDetail->id;
            }
        }

        return $result;
    }


    public function actionDo($action){
        //Yii::trace($action);

        $request = Yii::$app->request;
        $actionModel =new Dissertation();

        $actionModel->load($request->post());
        Yii::trace($actionModel);
        $details = Dissertation::findOne($actionModel['item']);

        if(!$details) throw new Exception('Dissertation details not found, please try again or contact support');
        if($actionModel){
            $id =$actionModel['item'];
            $details->notes = $actionModel['notes'];
            if($action){
                $details->approved = true;
            }else{
                $details->approved = false;
            }
            $details->status ='MARKED';
            $details->approved_by = Yii::$app->user->identity->id;

            $details->save(false);
            $this->runAction(['view',id =>$id]);
        }
    }


    /**
     * Retrieves the student information for particular school.
     *  @return mixed
     */
    public function actionStudentList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'first_name', 'last_name'])
                ->from('core_student')
                ->where(['ilike', 'first_name', $q])
                ->orWhere(['ilike', 'last_name', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['first_name'] ." ". $value['last_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }



}
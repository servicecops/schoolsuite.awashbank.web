<?php
namespace app\modules\dissertation;

class DissertationModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\dissertation\controllers';
    public function init() {
        parent::init();
    }
}
<?php

namespace app\modules\dissertation\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * DissertationSearch represents the model behind the search form of `app\modules\dissertation\models\Dissertation`.
 */
class DissertationSearch extends Dissertation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','summary', 'supervisor', 'submission_date', 'student_id' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $schoolId = $params['school_id']? $params['school_id'] : '';
        $activeStatus = $params['active']? $params['active']: '';

        $query = Dissertation::find()->where([
                                  'school_id'=> $schoolId, 
                                  'active'=>$activeStatus 
                ]);
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        

        if (isset($params['student_id'])) {
            $studentId = $params['student_id'];
            $query->andFilterWhere(['student_id' => $studentId ]);
        } 
        

        $query->andFilterWhere(['ilike', 'title', $this->title])
             ->andFilterWhere(['ilike', 'department', $this->department])
             ->andFilterWhere(['=', 'supervisor', $this->supervisor])
             ->andFilterWhere(['=', 'student_id', $this->student_id])
              ->andFilterWhere(['ilike', 'summary', $this->summary ]);

         //what about start date and end date.
        //  $query->andFilterWhere(['>=', 'start_time', $this->start_time ]);
        //  $query->andFilterWhere(['<=', 'end_time', $this->end_time ]);


        return $dataProvider;
    }

}
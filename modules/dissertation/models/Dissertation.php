<?php

namespace app\modules\dissertation\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $title
 * @property int $student_id
 * @property string|null $degree_program
 * @property string|null $submission_date
 * @property string $department
 * @property string $summary
 * @property int $supervisor
 * @property int $school_id
 * @property int|null $created_by
 */
class Dissertation extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dissertation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'student_id', 'department', 'summary', 'supervisor', 'school_id'], 'required'],
            [['title', 'degree_program', 'department', 'summary','notes'], 'string'],
            [['student_id', 'school_id', 'created_by'], 'default', 'value' => null],
            [['student_id', 'school_id', 'created_by', 'supervisor','approved_by','item'], 'integer'],
            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx', 'maxFiles' => 10],
            [['submission_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'student_id' => 'Student ID',
            'degree_program' => 'Degree Program',
            'submission_date' => 'Submission Date',
            'department' => 'Department',
            'summary' => 'Summary',
            'supervisor' => 'Supervisor',
            'school_id' => 'School ID',
            'created_by' => 'Created By',
        ];
    }


    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('dissertation_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {

        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
                $upload = new DissertationUploads();
                $fname = $file->baseName . '.' . $file->extension;

                $tmpfile_contents = file_get_contents($file->tempName);
                Yii::trace($tmpfile_contents);
                $upload->submission_id = $this->id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);

            }
        }
    }


    public function saveAndUpdateUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            //ofcourse first delete old uploads n then upload the new ones
            //simplest way to effect file uploads


            foreach ($this->uploads as $file) {
                $upload = new DissertationUploads();
                $fname = $file->baseName . '.' . $file->extension;

                $tmpfile_contents = file_get_contents($file->tempName);
                Yii::trace($tmpfile_contents);
                $upload->submission_id = $this->id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);

            }
        }
    }

    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

}

<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use kartik\file\FileInput;


/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class=" row ">
    <p>All fields marked with * are required</p>


    <div class="col-md-12">
        <?= ($model->isNewRecord) ? '<h3><i class="fa fa-plus"></i> Create Dissertation</h3>' : '<h3><i class="fa fa-edit"></i> Edit Dissertation</h3>' ?>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>

    <div class="col-md-12">
        <?php

        $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => 'create']);

        if (!$model->isNewRecord) { //edit mode
            $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => ['update', 'id' => $model->id]]);
        }

        ?>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Title *') ?>
            </div>
            <div class="col-md-6">
                <?php
                $url = Url::to(['/planner/planner/staff-list']);
                $staffInfo = empty($model->supervisor) ? '' : CoreStaff::findOne($model->supervisor);
                $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';


                echo $form->field($model, 'supervisor')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
                    'initValueText' => $selectedStaff, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'style' => 'color:#041f4a',
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Supervisor Name *');
                ?>
            </div>


            <div class="col-md-6">
                <?= $form->field($model, 'department', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Department *') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'degree_program', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Degree Program *') ?>
            </div>


            <div class="col-md-12">
                <?= $form->field($model, 'summary', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea()->label('Summary *') ?>
            </div>


            <br>


            <div class="col-md-12 text-center"><h3><i class="fa fa-check-square-o"></i>&nbsp;Attach Documents</h3></div>

            <div class="col-md-6">
                <?= $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
                    'options' => ['multiple' => true],
                    'pluginOptions' => [
                        'allowedFileExtensions' => ['jpg', 'png', 'jpeg', 'pdf'],
                        'showUpload' => false,
                        'showCancel' => false,
                        'maxFileCount' => 5,
                        'fileActionSettings' => [
                            'showZoom' => false,
                        ]
                    ],
                ])->label(false);
                ?>
            </div>
            <div class="col-md-6">
                <h3>1.To upload multiple files, just select all files and then drag and drop there</h3>
                <h3>2.If you do not want to edit the uploaded files/documents, just leave it blank.. </h3>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card-footer">
                <div class="row">
                    <div class="col-xm-6">
                        <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                    </div>
                    <div class="col-xm-6">
                        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

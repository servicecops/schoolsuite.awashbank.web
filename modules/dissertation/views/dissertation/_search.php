<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

?>



<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>
<div class="row student-search">
    <div class="col-md-3">
        <?= $form->field($model, 'title', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Title of dissertation'])->label(false) ?>

    </div>
    <div class="col-md-3">
        <?php
        $url = Url::to(['/planner/planner/staff-list']);
        $staffInfo = empty($model->supervisor) ? '' : CoreStaff::findOne($model->supervisor);
        $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';


        echo $form->field($model, 'supervisor')->widget(Select2::classname(), [
            'options' => ['multiple' => false, 'placeholder' => 'Supervisor'],
            'initValueText' => $selectedStaff, // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label(false);
        ?>
    </div>
    <div class="col-md-3">

        <?php
        $url = Url::to(['student-list']);
        $studentInfo = empty($model->student_id) ? '' : CoreStudent::findOne($model->student_id);
        $selectedStudent = $studentInfo ? $studentInfo->first_name . ' ' . $studentInfo->last_name : '';


        echo $form->field($model, 'student_id')->widget(Select2::classname(), [
            'options' => ['multiple' => false, 'placeholder' => 'Student...'],
            'initValueText' => $selectedStudent, // set the initial display text
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],

            ],
        ])->label(false);
        ?>

    </div>
    <div class="col-md-3">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>



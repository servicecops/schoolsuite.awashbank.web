<?php
/* @var $this yii\web\View */
use yii\helpers\Html; 
use yii\helpers\Url;
$this->title = 'Channel - Destination bank';
$searchOptions = ['action'=>'/banks/reconciliation/index', 'search'=> 'channel'];
?>
<div class="row hd-title" data-title="Channel - Destination bank">
<div class="col-xs-12">
<div class="col-xs-4 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?php echo $this->title ?></h3></div>
<div class="col-xs-5 no-padding" style="padding-top: 15px !important;"><?php echo $this->render('_searchChannel', ['model' => $searchModel, 'options'=>$searchOptions]); ?></div>
<div class="col-xs-3 pull-right no-padding" style="padding-top: 20px !important;">
    <ul class="menu-list pull-right no-padding">
        <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']) ?>
        </li>
        <li>
        <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
        </li>
    </ul>
</div>
</div>

<?php 
if($hasData) {
$jar = ['head'=>['Destination Bank', 'Trans Count', 'Total'], 'dbn'=>['Bank', 'trans', 'total']];
$_SESSION['br']['jar']= $jar;
echo $this->render('_gridview', [ 'results' => $data, 'model'=>$searchModel, 'jar'=>$jar]); 
} ?>

<?php if(!$hasData) : ?>
    <div class="col-xs-12" style="font-size:18px;color:grey;">
        <p>Please select <b>Start Date</b> and <b>End Date </b> to generate a reconciliation report</p>
    </div>
<?php endif; ?>
</div>
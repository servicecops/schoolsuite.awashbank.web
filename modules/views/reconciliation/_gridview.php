
<div class="col-xs-12">
<table class ='table'>
<thead><tr><th colspan="4"> Reconciliation summary for (<?= $model->start_date." - ".$model->end_date; ?>)</th></tr></thead>
<?php 
$gsum = $gcount = 0;
foreach($results as $res) : 
$sum = $count = 0;
$rs = count($res['data']) + 2;
?>
    <tr>
        <th rowspan= "<?= $rs; ?>"> <?= $res['name'] ?> </th>
        <?php foreach($jar['head'] as $head){
            echo "<th>".$head."</th>";
        } ?>
    </tr>
    <?php foreach($res['data'] as $row) : 
    ?>
    <tr>
        <?php foreach($jar['dbn'] as $k=>$v){
            if($k==0){
                echo "<td>".$row[$v]."</td>";
            } else {
                echo "<td>".number_format($row[$v])."</td>";
            }
        }

        ?>
    </tr>
    <?php 
    $sum = $sum + $row['total'];
    $count = $count + $row['trans'];
    endforeach; ?>
    <tr class="border-total">
    <td>TOTAL</td>  <td><b><?= number_format($count) ?></b></td>  <td><b><?= number_format($sum) ?></b></td>
    </tr>
    <?php 
    $gsum = $gsum + $sum;
    $gcount = $gcount + $count;
    endforeach; ?>
    <tr style="background-color:#E3EEF1 !important;">
    <th colspan="2">GRAND TOTAL</th>
    <th><?= number_format($gcount); ?></th>
    <th><?= number_format($gsum); ?></th>
    </tr>
</table>
</div>
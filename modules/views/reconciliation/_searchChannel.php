<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\PaymentChannels;
use app\modules\banks\models\BankDetails;
use app\models\SchoolInformation;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
?>
<?php $form = ActiveForm::begin([
          'action' => [$options['action']],
          'method' => 'post',
          'options' => [
                'class'=>'formprocess'
             ]
      ]); ?>
    <div class="col-xs-12 no-padding">
    <ul class=" menu-list no-padding">
     <li class="col-xs-2 no-padding"><?= DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'start_date',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'Start Date'],
                    'clientOptions' =>[
                          'class'=>'form-control',
                          'changeMonth'=> true,
                          'changeYear'=> true,
                          'yearRange'=>'1900:'.(date('Y')+1),
                          'autoSize'=>true,
                          ],
                  ]); ?></li>
      <li class="col-xs-2 no-padding"><?= DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'end_date',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'End Date'],
                    'clientOptions' =>[
                          'class'=>'form-control',
                          'changeMonth'=> true,
                          'changeYear'=> true,
                          'yearRange'=>'1900:'.(date('Y')+1),
                          'autoSize'=>true,
                          ],
                  ]); ?></li>
      <?php if($options['search'] == 'channel') : ?>
     <li class="col-xs-4 no-padding"><?= $form->field($model, 'payment_channel')->dropDownList(ArrayHelper::map(PaymentChannels::find()->all(), 'id', 'channel_name'), ['prompt'=>'All Channels', 'class'=>'form-control input-sm'])->label(false) ?></li>
      <?php elseif($options['search'] == 'bank') : 
      if(Yii::$app->user->can('schoolpay_admin')) : ?>
      <li class="col-xs-3 no-padding"><?= $form->field($model, 'bank')->dropDownList(ArrayHelper::map(BankDetails::find()->all(), 'id', 'bank_name'), ['prompt'=>'All Banks', 'class'=>'form-control input-sm'])->label(false) ?></li>
      <?php endif; ?>
      
      <li class="col-xs-3 no-padding">
      <?php 
            $url = Url::to(['/school-information/schoollist']);
            $selectedSchool = empty($model->school) ? '' : SchoolInformation::findOne($model->school)->school_name;
            echo $form->field($model, 'school')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size'=>'sm',
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School', 
                    'class'=>'',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false);

    ?>
        </li>
    <?php endif; ?>
     <li class="col-xs-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></li>
    </ul>
    </div>

    <div class="col-xs-12">
        <?php
        // print_r($model->errors) ;
        if(!empty($model->errors) ){
          foreach($model->errors as $err){
            echo "<span style='color: #D8000C;'>".$err[0]."</span><br>";
          }
        }

        ?>
    </div>
  <?php ActiveForm::end(); ?>

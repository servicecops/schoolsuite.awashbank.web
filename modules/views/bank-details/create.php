<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\BankDetails */

$this->title = 'Add New Bank';
$this->params['breadcrumbs'][] = ['label' => 'Bank Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row hd-title" data-title="Add User">
  <div class="col-md-12">
  <div class="letter">
  <div class="row">
<div class="bank-details-create">
      <div class="col-md-12" style="text-align:center; color:#565656;"><h3><i class="fa fa-plus"></i>&nbsp; &nbsp; <?= Html::encode($this->title) ?></h3></h3>
        <hr class="l_header">
    </div>
    <p>&nbsp;</p>
 </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
</div>


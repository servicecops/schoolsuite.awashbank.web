<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImageBank */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="letter">
<div class="row hd-title" data-title="Upload Bank Logo">
<?php $form = ActiveForm::begin(['id'=>'bank_logo', 'options'=>['enctype'=>'multipart/form-data']]); ?>
<div class="col-xs-12 no-padding">
	<div class="col-md-6 no-padding">
	<div id="upload-demo" style="padding-top:0;padding-left:0;"></div>
	<input type="hidden" id="imagebase64" name="ImageBank[image_base64]">
	</div>
	<div class="col-md-6 text-center" style="padding-top:10px;">
		<div>
		<h3 style="padding:0px;line-height:30px;"><?= $model->bank_name; ?><h3>
		<hr class="l_header">
		<div id="crop_result" style="padding:50px 100px;">
			<?php if($model->bank_logo) : ?>
				<img class="img-thumbnail" src="<?= Url::to(['/import/import/image-link', 'id'=>$model->bank_logo]) ?>" height="210" width="200" />'
			<?php endif; ?>
		</div>
		</div>
	</div>
</div>
<div class="col-xs-12">
	<div class="col-md-6">
		<label class="btn btn-primary btn-file">
			Upload<input type="file" id="upload" style="display:none;" value="Choose a file"/>
		</label>
		<span class="btn btn-info upload-result">Crop</span>
		<?= Html::submitButton('Save Logo', ['class' => 'btn btn-info save_cropped_photo', 'style'=>'display:none;']) ?>
	</div>
</div>
<?php ActiveForm::end(); ?>
</div>
</div>

<script type="text/javascript">
$( document ).ready(function() {
    var $uploadCrop;
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();          
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
            }           
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 220,
            height: 220,
        },
        boundary: {
            width: 400,
            height: 400
        }
    });
    $('.cr-viewport').html("<div class='text-center' style='color:gray;padding:75px 10px;font-size:22px;'>Click Upload Button Below</div>");

    $('#upload').on('change', function () {  $('.cr-viewport').html(''); readFile(this); });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport',
            format:'jpeg'
        }).then(function (resp) {
            $('#imagebase64').val(resp);
            $('#crop_result').html('<img class="img-thumbnail" src="'+resp+'" height="210" width="200" />');
            $('.save_cropped_photo').css('display', 'inline');
            return false;
        });
    });

});
</script>
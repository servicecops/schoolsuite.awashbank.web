<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\BankDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-xs-12 bank-details-form">

    <?php $form = ActiveForm::begin(['options'=>['class'=>'formprocess']]); ?>

    <?= $form->field($model, 'bank_name')->textInput() ?>
    <?= $form->field($model, 'bank_code')->textInput() ?>

    <?= $form->field($model, 'bank_address')->textInput() ?>

    <?= $form->field($model, 'contact_email')->textInput() ?>

    <?= $form->field($model, 'contact_phone')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add Bank' : 'Edit Bank', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

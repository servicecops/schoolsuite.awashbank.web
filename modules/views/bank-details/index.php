<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\banks\models\BankDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bank Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-details-index">

    <div class="col-xs-12">
      <div class="col-xs-8 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?php echo $this->title ?></h3></div>
      <div class="col-xs-4 no-padding pull-right" style="padding-top: 20px !important;"><?= Html::a('Add Bank', ['/banks/bank-details/create'], ['class' => 'btn btn-success pull-right aclink']) ?></div>
    </div>
    
    <div class='col-md-12'>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'bank_name',
            'bank_address',
            'contact_email:email',
            'contact_phone',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

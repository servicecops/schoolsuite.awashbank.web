<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use app\models\SchoolInformation;
use app\models\PaymentChannels;
use yii\jui\DatePicker;
use yii\web\JsExpression;
?>


<?php $form = ActiveForm::begin([
    'action' => [$req],
    'method' => 'get',
    'options'=>['class'=>'formprocess'],
]); ?>

<div class="col-sm-2 no-padding">
    <?= $form->field($model, 'date_from')->widget(DatePicker::className(),
        [
            'model'=>$model,
            'attribute'=>'date_from',
            'clientOptions' =>[
                'changeMonth'=> true,
                'changeYear'=> true,
                'yearRange'=>'1900:'.(date('Y')+1),
                'autoSize'=>true,
            ],
            'options'=>[
                'class'=>'form-control input-sm',
                'placeholder'=>'Date from'
            ],])->label(false) ?>
</div>

<div class="col-sm-2 no-padding-left">
    <?= $form->field($model, 'date_to')->widget(DatePicker::className(),
        [
            'model'=>$model,
            'attribute'=>'date_to',
            'clientOptions' =>[
                'changeMonth'=> true,
                'changeYear'=> true,
                'yearRange'=>'1900:'.(date('Y')+1),
                'autoSize'=>true,
            ],
            'options'=>[
                'class'=>'form-control input-sm',
                'placeholder'=>'Date To'
            ],])->label(false) ?>
</div>

<div class="col-sm-2 no-padding">
    <?= $form->field($model, 'channel')->dropDownList(
        ArrayHelper::map(PaymentChannels::find()->where(['channel_type'=>'Mobile'])->orderBy('channel_code')->all(), 'id', 'channel_name'), [
            'prompt'=>'Filter Channel',
            'id'=>'channel_search',
            'class'=>'form-control input-sm' ]
    )->label(false) ?></div>

<div class="col-xs-2 no-padding">
    <?= $form->field($model, 'school')->textInput(['class'=>'form-control input-sm', 'placeholder'=>'Search School'])->label(false) ?>
</div>



<div class="col-xs-2 no-padding"><?= $form->field($model, 'settled')->dropDownList(['false'=>'Pending', 'true'=>'Settled'], ['prompt'=>'Status', 'class'=>'form-control input-sm'])->label(false) ?></div>


<div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
<?php ActiveForm::end(); ?>

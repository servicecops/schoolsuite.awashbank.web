<div class="col-xs-12">
    <table class="table table-striped table-responsive">
        <thead>
        <tr><th> Date </th>
            <th> Channel</th>
            <th> Trans Id</th>
            <th> Payer Phone</th>
            <th> Center Name</th>
            <th> Center Number</th>
            <th> Center Level</th>
            <th> Receipt No</th>
            <th> Settled</th>
            <th> Date Settled</th>
            <th> Settlement Reference</th>
            <th> Amount</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($query) :
            foreach($query as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['date_created']) ? date("Y-m-d H:i", strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_name']) ? $v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_depositor_phone']) ? $v['channel_depositor_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['center_name']) ? $v['center_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['center_number']) ? $v['center_number'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><?= ($v['center_level']) ? $v['center_level'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['schoolpay_receipt_number']) ? $v['schoolpay_receipt_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['reconciled']) ? 'Settled' : 'Pending' ?></td>
                    <td><?= ($v['bank_settlement_date']) ? date("Y-m-d H:i:s a", strtotime($v['bank_settlement_date'])) : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['bank_settlement_reference']) ? $v['bank_settlement_reference'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['amount']) ? number_format($v['amount']) : '<span class="not-set">(not set) </span>' ?></td>

                </tr>
            <?php endforeach;
        else :?>
            <tr>
                <td colspan="12">No Records found</td>
            </tr>

        <?php endif; ?>
        </tbody>
    </table>

</div>
<div class="col-xs-12">
        <table class="table table-striped table-responsive">
        <thead>
        <tr><th> Date </th>
        <th> Channel</th>
        <th>Channel Trans ID</th>
        <th> Payer Phone</th>
        <th> Payment Code</th>
        <th>Student Name</th>
        <th>School Name</th>
        <th> Reg No</th>
        <th> Class Code</th>
        <th>Receipt No</th>
        <th> Amount</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            if($query) :
            foreach($query as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['date_created']) ? date("d-m-Y H:i:s", strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_name']) ? $v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_depositor_phone']) ? $v['channel_depositor_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['payment_code']) ? $v['payment_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['last_name']) ? $v['first_name'].' '.$v['middle_name'].' '.$v['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_student_registration_number']) ? $v['school_student_registration_number'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['reciept_number']) ? $v['reciept_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['amount']) ? number_format($v['amount']) : '<span class="not-set">(not set) </span>' ?></td>
                    
                </tr>
            <?php endforeach; 
            else :?>
            <td colspan="14">No Records found</td>
        <?php endif; ?>
        </tbody>
        </table>

    </div>
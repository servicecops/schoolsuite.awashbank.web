<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index hd-title" data-title="School Transactions">
<div class="row">
    <div class="col-xs-12">
    <div>
      <div class="col-sm-3 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
      
      <div class="col-sm-6 col-xs-12 no-padding" ><?php echo $this->render('_search_all', ['model' => $searchModel]); ?></div>
      <div class="col-sm-3 col-xs-12 no-padding">
        <ul class="menu-list pull-right no-padding">
            <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn btn-danger btn-sm', 'target'=>'_blank']) ?>
            </li>
            <li><?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
            </li>
        </ul>
    </div>
    </div>
    </div>
    

    <div class="col-xs-12">
        <div class="box-body table table-responsive no-padding">
        <table class="table table-striped">
        <thead>
        <tr><th class='clink'><?= $sort->link('date_created') ?></th>
        <th class='clink'><?= $sort->link('channel_name', ['label'=>'Channel']) ?></th>
        <th class='clink'><?= $sort->link('channel_trans_id', ['label'=>'Ch Trans Id']) ?></th>
        <th class='clink'><?= $sort->link('channel_depositor_phone', ['label'=>'Payer Phone']) ?></th>
        <th class='clink'><?= $sort->link('payment_code') ?></th>
        <th class='clink'><?= $sort->link('last_name', ['label'=>'Student Name']) ?></th>
        <th class='clink'><?= $sort->link('school_name') ?></th>
        <th class='clink'><?= $sort->link('school_student_registration_number', ['label'=>'Reg No']) ?></th>
        <th class='clink'><?= $sort->link('class_code') ?></th>
        <th class='clink'><?= $sort->link('reciept_number') ?></th>
        <th class='clink'><?= $sort->link('amount') ?></th>
        </tr>
        </thead>
        <tbody>
            <?php 
            if($dataProvider) :
            foreach($dataProvider as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['date_created']) ? date("Y-m-d H:i a", strtotime($v['date_created']))  : '<span class="not-set">(not set) </span>' ?></td>
                    
                    <td><?= ($v['channel_name']) ? '<img src="'.Url::to(['/import/import/image-link', 'id'=>$v['payment_channel_logo']]).'" width="25px" /> &nbsp;'.$v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_depositor_phone']) ? $v['channel_depositor_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['payment_code']) ? $v['payment_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['last_name']) ? $v['first_name'].' '.$v['middle_name'].' '.$v['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_student_registration_number']) ? $v['school_student_registration_number'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['reciept_number']) ? $v['reciept_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                    
                </tr>
            <?php endforeach; 
            else :?>
            <td colspan="14">No Records found</td>
        <?php endif; ?>
        </tbody>
        </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]);?>

    </div>
</div>



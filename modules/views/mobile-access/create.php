<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\MobileAccessUsers */

$this->title = 'Add New Mobile User';
$this->params['breadcrumbs'][] = ['label' => 'Mobile Access Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mobile-access-users-create hd-title" data-title="Add New Mobile user">

	<div class="letter">
    <div class="row">
    <div class="col-md-12" style="text-align:center; color:#565656;"><h3><?= Html::encode($this->title) ?></h3>
        <hr class="l_header">
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>

</div>

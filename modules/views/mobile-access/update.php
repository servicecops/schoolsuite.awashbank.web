<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\MobileAccessUsers */

$this->title = 'Edit ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mobile Access Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mobile-access-users-update">
<div class="mobile-access-users-update hd-title" data-title="<?= Html::encode($this->title) ?>">

	<div class="letter">

    <div class="row">
    <div class="col-md-12" style="text-align:center; color:#565656;"><h3><?= Html::encode($this->title) ?></h3>
        <hr class="l_header">
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
</div>

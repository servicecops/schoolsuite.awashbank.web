<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\import\models\StudentPaymentCodeRequests */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="self-enrolled hd-title" data-title="<?= $model->name ?>">
<div class="row">
<div class="col-md-12">
    <div class="letter">
    <div class="row">
     <div class='col-md-7'> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class='col-md-5' style="padding-top:27px;">
    <span class="pull-right">
        <?= Html::a('<i class="fa fa-plus"></i>&nbsp;&nbsp; Add New', ['create'], ['class' => 'aclink btn  btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-remove"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </span>
    </div>
    </div>
    <hr class="l_header">


    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
             
            'phone_number',
            'name',
            'date_created',
            'active:boolean',
            [                   
                'label' => 'Bank',
                'value' => $model->bank->bank_name,
            ],
            'school_id',
            'user_type',
            'device_id',
            'encryption_key',
        ],
    ]) ?>

</div>
</div>
</div>
<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mobile Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index hd-title" data-title="Mobile Access Users">
<div class="row">
    <div class="col-xs-12">
    <div>
      <div class="col-sm-2 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
      
      <div class="col-sm-7 col-xs-12 no-padding" ><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
      <div class="col-sm-3 col-xs-12 no-padding">
        <ul class="menu-list pull-right">
            <li><?= Html::a('<i class="fa fa-plus"></i>&nbsp;&nbsp; Add New', ['create'], ['class' => 'aclink btn  btn-info btn-sm']) ?>
            </li>
            <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']) ?>
            </li>
            <li>
            <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
            </li>
        </ul>
    </div>
    </div>
    </div>
    <div class="col-xs-12">
       <div class="box-body table table-responsive no-padding">
        <table class="table">
        <thead>
        <tr><th class='clink'><?= $sort->link('name') ?></th><th class='clink'><?= $sort->link('bank_name') ?></th><th class='clink'><?= $sort->link('device_id')?></th><th class='clink'><?= $sort->link('phone_number')?></th><th class='clink'><?= $sort->link('user_type') ?><th class='clink'>&nbsp;</th></tr>
        </thead>
        <tbody>
            <?php 
            if($dataProvider) :
            foreach($dataProvider as $k=>$v) : ?>
                <tr >
                    <td><?= ($v['name']) ? $v['name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['bank_name']) ? $v['bank_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['device_id']) ? $v['device_id'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['phone_number']) ? $v['phone_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['user_type']) ? $v['user_type'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><a href="javascript:;" onclick="clink('<?= Url::to(['view', 'id'=>$v['id']]) ?>')" title="View" data-pjax="0"><span class="fa fa-search"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="javascript:;" onclick="clink('<?= Url::to(['update', 'id'=>$v['id']]) ?>')" title="Update" data-pjax="0"><span class="glyphicon glyphicon-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; 
            else :?>
            <td colspan="8">No student found</td>
        <?php endif; ?>
        </tbody>
        </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]);?>

    </div>
</div>


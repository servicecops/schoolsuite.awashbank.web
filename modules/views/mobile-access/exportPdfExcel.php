<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php  
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Mobile User List</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <th>Name</th><th>Bank Name</th><th>Device Id</th><th>Phone Number</th><th>User Type</th></tr>
        </thead>
        <tbody>
            <?php 
            if($query) :
            foreach($query as $k=>$v) : ?>
                <tr >
                    <td><?= ($v['name']) ? $v['name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['bank_name']) ? $v['bank_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['device_id']) ? $v['device_id'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['phone_number']) ? $v['phone_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['user_type']) ? $v['user_type'] : '<span class="not-set">(not set) </span>' ?></td>
                </tr>
            <?php endforeach; 
            else :?>
            <tr><td colspan="8">No student found</td></tr>
        <?php endif; ?>
        </tbody>
        </table>

</div>
<?php

namespace app\modules\gradingcore\controllers;




use app\modules\feesdue\models\FeesDue;
use app\modules\feesdue\models\FeesDueStudentExemption;
use app\modules\feesdue\models\InstitutionFeesDuePenalty;
use app\modules\gradingcore\models\Grading;
use app\modules\gradingcore\models\GradingSearch;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\BaseObject;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

if (!Yii::$app->session->isActive){
    session_start();
}

/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class GradingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FeesDue models.
     * @return mixed
     */

    public function actionIndex()
    {

        if(Yii::$app->user->can('r_grade')){
            $request = Yii::$app->request;
            $searchModel = new GradingSearch();
            $data = $searchModel->searchIndex(Yii::$app->request->queryParams);
            $columns =$data['columns'];
            $dataProvider = $data['query'];
            $pages = ['pages'=>$data['pages'], 'page'=>$data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('index', ['columns'=>$columns,
                'searchModel' => $searchModel,'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ]) :
                $this->render('index', ['columns'=>$columns,
                    'searchModel' => $searchModel,'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }

    }

    /**
     * Displays a single FeesDue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $request = Yii::$app->request;
        $searchModel = new GradingSearch();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $schoolData = $searchModel->viewModel($id);
        $modeler = $schoolData['models'];
        $attributes =$schoolData['attributes'];
        $res =[
            'model' => $modeler,
            'attributes'=>$attributes
        ];

        return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);

    }


    /**
     * Finds the PaymentChannels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentChannels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Grading::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function actionClasslist($q = null, $schoolId)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, class_description AS text')
                ->from('core_school_class')
                ->where(['school_id' => $schoolId])
                ->andWhere(['ilike', 'class_description', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($schoolId > 0) {
            $out['results'] = ['id' => $schoolId, 'text' => CoreSchoolClass::find()->where(['school_id' => $schoolId])->class_description];
        }
        return $out;
    }


    public function actionUpdate($id)
    {
        Yii::trace("insude\n");
        $request = Yii::$app->request;
        //if(Yii::$app->user->can('r_grade')){
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {

            $model->save(false);
            $searchModel= new GradingSearch();

            $schoolData = $searchModel->viewModel($id);
            Yii::trace($schoolData);
            $modeler = $schoolData['models'];
            $attributes =$schoolData['attributes'];


            return $this->render('@app/views/common/modal_view', [
                'model' => $modeler,
                'attributes'=>$attributes
            ]);

            // return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return ($request->isAjax) ? $this->renderAjax('update', ['model' => $model]) :
                $this->render('update', ['model' => $model]);
        }

//        } else {
//            throw new ForbiddenHttpException('No permissions to update or create grades.');
//        }
    }

/**
* Deletes an existing Grade model.
* If deletion is successful, the browser will be redirected to the 'index' page.
* @param integer $id
* @return mixed
*/
    public function actionDelete($id)
    {

        if(Yii::$app->user->can('del_grade')){

            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException('No permissions to delete a channel.');
        }
    }



    public function actionExportExcel($model)
    {
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex'])  ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(20000)->all(Yii::$app->db);
        $type = 'Excel';

        $file = $this->renderPartial($data['exportFile'], [
            'query'=>$query, 'type'=>$type, 'subSessionIndex'=>$subSessionIndex
        ]);
        $fileName = $data['fileName'].'.xls';
        $options = ['mimeType'=>'application/vnd.ms-excel'];

        try{
            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        } catch(\Exception $e){
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to create Excel", $error, null,null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            return \Yii::$app->runAction('/core-student/error');
        }
    }

    public function actionStudentClassGrades()
    {
        $searchModel = new Grading();
        $studentModel = CoreStudent::find()->where(['web_user_id'=>Yii::$app->user->id])->one();

        $allData = $searchModel->searchStudentClassGrades($studentModel->class_id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $title ="My Class Grades";

        $searchForm = $allData['searchForm'];
        $res = ['dataProvider' => $dataProvider, 'title'=>$title,'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }




}

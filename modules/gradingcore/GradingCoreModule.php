<?php
namespace app\modules\gradingcore;

class GradingCoreModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\gradingcore\controllers';
    public function init() {
        parent::init();
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Update Core School: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Core Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-school-update">

    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Update Grades for Class</h3></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grades';
$this->params['breadcrumbs'][] = $this->title;
$studentModel = new CoreStudentSearch();
?>
<div class="formz">

    <div class="">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;School Grades</h3></div>

    </div>


        <div class="col-md-12">
            <div class="pull-right">
                <?php if (\app\components\ToWords::isSchoolUser()) : ?>
                    <a href="<?= Url::to(['/schoolcore/core-school-class/add-grades']); ?>" class="btn-info" >Add
                        Grades</a>

                <?php endif; ?>
                <?php

                echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                    'class'=>'btn btn-sm btn-danger',
                    'target'=>'_blank',
                    'data-toggle'=>'tooltip',
                    'title'=>'Will open the generated PDF file in a new window'
                ]);
                echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                    'class'=>'btn btn-sm btn-success',
                    'target'=>'_blank'
                ]);
                ?>
            </div>
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class='clink'><?= $sort->link('grades', ['label'=>'Grade']) ?></th>
                        <th class='clink'><?= $sort->link('min_mark', ['label' => 'Min']) ?></th>
                        <th class='clink'><?= $sort->link('max_mark', ['label' => 'Max']) ?></th>
                        <?php if (Yii::$app->user->can('rw_sch')) {
                            echo "<th class='clink'>" . $sort->link('school_name') . "</th>";
                        } ?>
                        <th class='clink'><?= $sort->link('class',['label'=>'Class']) ?></th>
                        <th class='clink'><?= $sort->link('comment',['label'=>'Comment']) ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td class="clink"><?= ($v['grades']) ? '<a href="' . Url::to(['grading/view', 'id' => $v['id']]) . '">' . $v['grades'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?=  $v['min_mark']  ?></td>
                                <td><?= ($v['max_mark']) ? $v['max_mark'] : '<span class="not-set">(not set) </span>' ?></td>
                                <?php if (Yii::$app->user->can('rw_sch')) : ?>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <?php endif; ?>
                                <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['comment']) ? $v['comment'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td>
                                    <?= Html::a('<i class="fa  fa-ellipsis-h"></i>', ['grading/view', 'id' => $v['id']], ['class'=>'aclink']) ?>

                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No Grades found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel'  => 'Last'
            ]); ?>

        </div>
    <div style="clear: both"></div>
    </div>


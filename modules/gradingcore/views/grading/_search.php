<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="core-school-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>



    <?= $form->field($model, 'school_name')->textInput(['placeholder' => 'Search School name'])->label(false) ?>

    <?php // echo $form->field($model, 'school_type') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'phone_contact_1') ?>

    <?php // echo $form->field($model, 'phone_contact_2') ?>

    <?php // echo $form->field($model, 'contact_email_1') ?>

    <?php // echo $form->field($model, 'contact_email_2') ?>

    <?php // echo $form->field($model, 'school_code') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

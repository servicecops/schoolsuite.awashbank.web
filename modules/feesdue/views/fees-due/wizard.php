<?php
if (!isset($labels)) $labels = [
    1 => ['title'=>'Add New Fee', 'gray'>true],
    2 => ['title'=>'Assign Classes /Exceptions', 'gray'>true],
    3 => ['title'=>'Submit For Approval', 'gray'>true],
    4 => ['title'=>'Approve Fee', 'gray'>true],
];

if (!isset($labels[1])) $labels[1] = ['title'=>'Add New Fee', 'gray'>true];
if (!isset($labels[2])) $labels[2] = ['title'=>'Assign Classes /Exceptions', 'gray'>true];
if (!isset($labels[3])) $labels[3] = ['title'=>'Submit For Approval', 'gray'>true];
if (!isset($labels[4])) $labels[4] = ['title'=>'Approve Fee', 'gray'>true];

if(!isset($activeIndex)) $activeIndex = 1;

?>
<div class="wizard">
    <?php for($i=1; $i<count($labels); $i++) : ?>
        <a class="<?= $activeIndex==$i ? 'current' : '' ?> col-xs-3"><span class="badge"><?= $i ?></span> <?= $labels[$i]['title'] ?></a>
    <?php endfor; ?>
</div>
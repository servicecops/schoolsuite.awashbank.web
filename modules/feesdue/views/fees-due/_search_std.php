<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Information';
?>

<div style="color:#F7F7F7">Students List</div>
<div class="row">



    <div class="col-md-12" style="margin-top:20px">

        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-md-8"><?php echo $this->render('_search_students', ['model' => $searchModel, 'id'=>$id]); ?></div>
    </div>
    <div class=" col-md-12">
        <?php $form = ActiveForm::begin([
            'action' => ['assign-stds', 'id' => $id],
            'method' => 'post',
            'options' => ['class' => 'formprocess'],
        ]); ?>
        <div class="row">
            <div class="pull-left">
                <?= Html::submitButton('Exempt', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
        </div
    </div>


    <div class="col-md-12">


                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th style="padding:0 15px;"><span class="checkbox checkbox-info checkbox-circle"
                                                          style="margin:6px;">
                                <input type="checkbox" class="checkall"
                                                                                     onclick="checkall()"
                                                                                     id="check_all_id1"><label
                                        for="check_all_id1">Select All</label></span></th>
                        <th class='clink'><?= $sort->link('student_code') ?></th>
                        <th class='clink'><?= $sort->link('first_name', ['label' => 'Student Name']) ?></th>

                        <th class='clink'><?= $sort->link('class_code') ?></th>
                        <th class='clink'>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td style="padding:0 15px;">
                                <span class="checkbox checkbox-info checkbox-circle"
                                      style="margin:6px;">
                                    <input type="checkbox"
                                           class="checkbox"
                                           name="selected_stu[]"
                                           id="<?= $v['id']; ?>"
                                           value="<?= $v['id'] ?>">
                                    <label class='col-xs-12 no-padding' for="<?= $v['id']; ?>"></label>
                                </span>
                                </td>
                                <td class="clink"><?= ($v['student_code']) ?  $v['student_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>

                                <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>


                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No student found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>



    </div>
    <?php ActiveForm::end(); ?>
<div style="clear: both"></div>

</div>
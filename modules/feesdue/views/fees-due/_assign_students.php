<?php

use app\models\InstitutionFeesDuePenalty;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $penaltyModel InstitutionFeesDuePenalty */
/* @var $model app\models\FeesDue */

$this->title = 'Assign Students' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="letters">


    <div class="row">
        <div class="fees-due-view hd-title" data-title="<?= $feeDetails->description ?>">
            <div class="col-md-12 " style="background: #efefef;">
                <div class="wizard">
                    <?php if ($feeDetails->created_by == Yii::$app->user->identity->id) : ?>
                        <a href="<?= Url::to(['update', 'id' => $feeDetails->id]) ?>" class="aclink col-md-3"><span
                                    class="badge">1</span> Edit Fee Information</a>
                    <?php else: ?>
                        <a class="col-md-3 gray"><span class="badge">1</span> Edit Fee Information</a>
                    <?php endif ?>
                    <a class="current col-md-3"><span class="badge">2</span> Assign Classes /Exceptions</a>
                    <a href="<?= Url::to(['details', 'id' => $feeDetails->id]) ?>" class="aclink col-md-3"><span
                                class="badge badge-inverse">3</span> Submit For Approval</a>
                    <a class="col-md-2 gray"><span class="badge">4</span> Approve Fee</a>
                </div>
            </div>

            <div class="col-md-12" style="background: white;padding:10px">
                <b style="color:#D82625; font-size: 20px;">Important: </b> <br>
                The students you select here will bw exempted from this fee <br>


                <p><span style=" font-size: 16px;"><i class="fa fa-exclamation-circle"></i> <b>GROUP FEE:</b> If you are planning to apply this fee to a group, <a
                                href="<?= Url::to(['details', 'id' => $feeDetails->id]) ?>" class="aclink"><b>SKIP THIS PAGE</b></a> and go straight to <b>Submit for Approval </b></span></p>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="col-md-8 no-padding">
            <h3><i class="fa fa-exchange"></i> &nbsp;<?= Html::encode($this->title) ?></h3>
        </div>

        <div class="col-md-4 no-padding">
            <div class="pull-right" style="padding-top:20px;">
                <?= Html::a('Back', ['index'], ['class' => 'btn btn-default btn-sm aclink']) ?>

            </div>
        </div>

    </div>


    

    <div class=" row">

        <div class="col-md-12"><?php echo $this->render('_search_students', ['model' => $searchModel, 'id' => $id]); ?></div>


    </div>

    <div class=" row">
        <?php $form = ActiveForm::begin([
            'action' => ['assign-stds', 'id' => $id],
            'method' => 'post',
            'options' => ['class' => 'formprocess'],
        ]); ?>


       
        
        <div class="row">
            <div class="pull-left">
                <?= Html::submitButton('Exempt', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
        </div
        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">
        <div class="box-body table table-responsive ">
            <table class="table">
                <thead>
                <tr style="background: white;padding:10px">
                    <th style="padding:0 15px;"><span class="checkbox checkbox-info checkbox-circle"
                                                      style="margin:6px;"><input type="checkbox"
                                                                                 class="checkall"
                                                                                 onclick="checkall()"
                                                                                 id="check_all_id1"><label
                                    for="check_all_id1">Select All</label></span></th>
                    <th>Payment code</th>
                    <th>Student Name</th>
                    <th>Class</th>
                    <th>Gender</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($allClsStds) :
                    $srNo = 1;
                    foreach ($allClsStds as $k => $v) : ?>
                        <?php
                        $studentName = $v['first_name'] ? $v['first_name'] : "";
                        $studentName .= $v['middle_name'] ? " " . $v['middle_name'] : "";
                        $studentName .= $v['last_name'] ? " " . $v['last_name'] : "";
                        ?>

                        <tr>
                            <td style="padding:0 15px;">
                                <span class="checkbox checkbox-info checkbox-circle"
                                      style="margin:6px;">
                                    <input type="checkbox"
                                           class="checkbox"
                                           name="selected_stu[]"
                                           id="<?= $v['id']; ?>"
                                           value="<?= $v['id'] ?>">
                                    <label class='col-xs-12 no-padding' for="<?= $v['id']; ?>"></label>
                                </span>
                            </td>
                            <td><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= $studentName ?></td>
                            <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set"> -- </span>' ?></td>
                            <td><?= ($v['gender']) ? $v['gender'] : '<span class="not-set"> -- </span>' ?></td>
                        </tr>
                        <?php
                        $srNo++;
                    endforeach;
                else :
                    ?>
                    <td colspan="8">No Records found for the search Criteria</td>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

    </div>
</div>



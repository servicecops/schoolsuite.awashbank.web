<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="fees_due_index">
    <?php  
	if($type == 'Excel') {
		echo "<table><tr> <th colspan='7'><h3> List of Courses</h3> </th> </tr> </table>";
	}
    ?>
    <table class="table table-striped table-responsive">
        <thead>
        <tr><?php if(Yii::$app->user->can('schoolpay_admin')){echo "<th>School Name</th>"; } ?> <th>Description</th><th>Due Amount</th><th>Effective Date</th><th>End Date</th><th>Approval Status</th></tr>
        </thead>
        <tbody>
            <?php 
            foreach($query as $k=>$v) : ?>
                <tr>
                    <?php if(Yii::$app->user->can('schoolpay_admin')) : ?>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <?php endif; ?>
                    <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['due_amount']) ? $v['due_amount'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['effective_date']) ? $v['effective_date'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['end_date']) ? $v['end_date'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= $v['approval_status']==true ?  "Approved" : "pending"; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>
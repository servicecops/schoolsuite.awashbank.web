<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */
/* @var $model app\models\FeesDue */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="letters">

    <div class="row">
        <div class="fees-due-view hd-title" data-title="<?= $model->description ?>">
            <div class="col-md-12 " style="background: #efefef;">
                <div class="wizard">
                    <?php if ($model->created_by == Yii::$app->user->identity->id) : ?>
                        <a href="<?= Url::to(['update', 'id' => $model->id]) ?>" class="aclink col-md-3"><span
                                    class="badge">1</span> Edit Fee Information</a>
                    <?php else: ?>
                        <a class="col-md-3 gray"><span class="badge">1</span> Edit Fee Information</a>
                    <?php endif ?>
                    <a class="current col-md-3"><span class="badge">2</span> Assign Classes /Exceptions</a>
                    <a href="<?= Url::to(['details', 'id' => $model->id]) ?>" class="aclink col-md-3"><span
                                class="badge badge-inverse">3</span> Submit For Approval</a>
                    <a class="col-md-2 gray"><span class="badge">4</span> Approve Fee</a>
                </div>
            </div>

            <div class="col-md-12" style="background: white;padding:10px">
                <b style="color:#D82625; font-size: 20px;">Important: </b> <br>
                This fee will be applied to all classes that you put in <b>Assigned Classes</b>,<br>
                For students in assigned classes that are <b>exempted</b> from this fee. Use the <b> Class students
                    filter</b>
                to search and put them in the <b>Excemptions box</b>.<br><br>

                <p><span style=" font-size: 16px;"><i class="fa fa-exclamation-circle"></i> <b>GROUP FEE:</b> If you are planning to apply this fee to a group, <a
                                href="<?= Url::to(['details', 'id' => $model->id]) ?>"
                                class="aclink"><b>SKIP THIS PAGE</b></a> and go straight to <b>Submit for Approval </b></span>
                </p>
            </div>
        </div>

    </div>
    <div class="row">

        <div class="col-md-8 no-padding">
            <h3><i class="fa fa-exchange"></i> &nbsp;<?= Html::encode($this->title) ?></h3>
        </div>

        <div class="col-md-4 no-padding">
            <div class="pull-right" style="padding-top:20px;">
                <?= Html::a('Back', ['index'], ['class' => 'btn btn-default btn-sm aclink']) ?>
                <?php if ($model->created_by == Yii::$app->user->identity->id) : ?>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

    <div class="row ">
        <div class="col-md-3 profile-text"
             style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('school_id') ?></div>
        <div class="col-md-9 profile-text"><?= ($model->school->school_name) ? Html::a($model->school->school_name, ['/schoolcore/core-school/view', 'id' => $model->school->id]) : "--" ?></div>
    </div>

    <div>
        <div style="width:50%;float:left">
            <div class="row ">
                <div class="col-md-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('due_amount') ?></div>
                <div class="col-md-6 profile-text"><?= $model->due_amount ?></div>
            </div>
        </div>
        <div style="width:50%;float:left">
            <div class="row ">
                <div class="col-sm-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('description') ?></div>
                <div class="col-sm-6 profile-text"><?= $model->description ?></div>
            </div>
        </div>
        <div style="width:50%;float:left">

            <div class=" row ">
                <div class=" col-sm-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('effective_date') ?></div>
                <div class="col-sm-6 profile-text"><?= $model->effective_date ?></div>
            </div>
            <div class=" row ">
                <div class=" col-sm-6 profile-text "
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('end_date') ?></div>
                <div class=" col-sm-6 profile-text "><?= $model->end_date ?></div>
            </div>
        </div>
        <div style="width:50%;float:left">
            <div class="row ">
                <div class="col-lg-6 col-md-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('created_by') ?></div>
                <div class="col-lg-6 col-md-6 profile-text"><?= $model->createdBy->username ?></div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 profile-text"
                     style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('priority') ?></div>
                <div class="col-lg-6 col-md-6 profile-text"><?= $model->priority ?></div>
            </div>

        </div>
        <div style="width:50%;float:left">

            <div class="row ">
                <div class="col-lg-6 col-md-6 rofile-text" style="background: #e9f4fb !important;">Fee Type</div>
                <div class="col-lg-6 col-md-6 profile-text"><?= $model->recurrent ? 'Recurrent' : 'One-Time' ?></div>
            </div>
            <?php if ($model->recurrent) : ?>
                <div class="row ">
                    <div class="col-lg-6 col-md-6 rofile-text"
                         style="background: #e9f4fb !important;"><?= $model->recurrent ? 'Apply Every' : '' ?></div>
                    <div class="col-lg-6 col-md-6 profile-text"><?= $model->recurrent ? $model->apply_frequency . ' ' . ($model->apply_frequency_unit == 'MONTHLY' ? 'Months' : 'Weeks') : '' ?></div>
                </div>
            <?php endif; ?>
        </div>
        <div style="width:50%;float:left">

            <?php if ($model->recurrent) : ?>
                <div class="row ">
                    <div class="col-lg-6 col-md-6 rofile-text" style="background: #e9f4fb !important;">Last Apply Date
                    </div>
                    <div class="col-lg-6 col-md-6 profile-text"><?= $model->last_apply_date ? $model->last_apply_date : '-' ?></div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 rofile-text" style="background: #e9f4fb !important;">Next Apply Date
                    </div>
                    <div class="col-lg-6 col-md-6 profile-text"><?= $model->next_apply_date ? $model->next_apply_date : '' ?></div>
                </div>

            <?php endif; ?>
        </div>
        <div style="width:50%;float:left">

            <div class="row ">
                <div class="col-lg-6 col-md-6 rofile-text" style="background: #e9f4fb !important;">Credit Hour Fee
                </div>
                <div class="col-lg-6 col-md-6 profile-text"><?= $model->credit_hour ? 'Yes' : 'No' ?></div>
            </div>
        </div>
        <div style="clear: both"></div>
    </div>


    <?php if ($penaltyModel) : ?>
        <?= $this->render('_penalty_view', ['penaltyModel' => $penaltyModel, 'model' => $model]); ?>
    <?php endif; ?>


    <p><br></p>
    <?php $form = ActiveForm::begin([
        'action' => ['view', 'id' => $model->id],
        'method' => 'post',
        'options' => ['class' => 'formprocess','id' => 'assign_fee_student_form_modal'],
    ]); ?>


    <h3>Assign Classes</h3><br/>
    <div class="col-md-12">
    <div class="col-md-6">
        <?php
        $url = Url::to(['fees-due/classlist', 'schoolId' => $schoolId]);
        $selectedClass = empty($feeClass->class_id) ? '' : CoreSchoolClass::findOne($model->class_id)->class_description;
        echo $form->field($feeClass, 'class_id')->widget(Select2::classname(), [
            'initValueText' => $selectedClass, // set the initial display text
//                    'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'placeholder' => 'Select Class',
               'id' => 'feediv',
                'multiple' => true,
                'class' => 'form-control',
                'style' => ['width' => '250px']
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
            ],
        ])->label('Choose Classes'); ?>
    </div>
    <div class="col-md-6" id='credithrsdiv'>
        <?= $form->field($feeClass, 'credit_hours', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Credit Hours', 'id' => 'credithrs'
        ],

        ])->textInput()->label('Credit Hours*') ?>
    </div>

            <div class="col-md-6  alert alert-danger invalid-accounts-error" style="display: none">
                Please enter credit hours to continue
            </div>


</div>



<?php if ($model->enable_reminders) { ?>
    <h3>SMS Reminders</h3>
    <div class="col-md-12">

        <?= $this->render('create_message', ['model' => $model, 'searchModel' => $searchModel, 'id' => $model->id, 'form' => $form, 'user_sch' => $schoolId,]) ?>
    </div>
<?php } ?>

<hr style="border-top: 2px solid #f6fbfc;"/>


<div class="col-md-6">

    <?= $this->render('assign_student_concession') ?>
</div>


<div class="form-group col-md-12 ">
    <div class="col-md-6 col-md-offset-3">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-block btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>


</div>
<?php
$url = Url::to(['/feesdue/fees-due/credithr-fee']);
$id =$model->id;
$script = <<< JS
    $(document).ready(function(){
          
        var isCrdit=false;
     
          $('#credithrsdiv').hide();
        $('form#assign_fee_student_form_modal').yiiActiveForm('validate');
 
             $('#feediv').change(function(){
                 var feeId =$(this).val();
                 
             $.get('$url?id='+$id, function( data ) {
    
                if(data == true ){
                    isCrdit = true;
                      $('#credithrsdiv').show();
                } else{
                      $('#credithrsdiv').hide();
                }
                
                });
        });
             
             
        let schoolForm = $('#assign_fee_student_form_modal');
        schoolForm.on('beforeSubmit', function(e) {
            

            //This is a chance to do extra validation
            let ok = true;
            schoolForm.find('.form-control').each(function() {
                let accountControl = $(this);
                if(!accountControl.val() && isCrdit == true ) {
                    accountControl.addClass('is-invalid')
                    $('.invalid-accounts-error').show()
                    ok = false;
                    // return false;
                }else {
                    accountControl.removeClass('is-invalid')
                     $('.invalid-accounts-error').hide()
                }
            })
            return ok;

        });     
   
   });
JS;
$this->registerJs($script);
?>

<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="row">

    <div class="col-sm-12"><h3><i class="fa fa-adjust"></i>&nbsp;Concessions / Discounts</h3></div>

    <div class="col-sm-12">
        <?php $i = 1;

        ?>
        <div id="submission_Contacts">

            <div  class="row">
                <div class="col-sm-12"><span style="margin-right:5px"
                                             class="text-uppercase">Add Concession</span><span
                            class="add_contact_button" style="font-size: 22px;"><i
                                class="fa fa-plus text-primary"></i></span></div>
                <div class="col no-padding-left"><span class="text-uppercase">&nbsp;</span></div>
                <div class="col-auto no-padding-left"><span style="padding-left: 24px;">&nbsp;</span></div>
            </div>

        </div>

    </div>

</div>
<?php
$script = <<< JS
$("document").ready(function () {
      var row = $i;
   console.log("this one");
   
    let thisBut = function (selector) {
        console.log("selectedButonValue is: " + selector.val());
    };
    
   
    let thisAnswerOption = function (selector) {

        let singleAswerContainer = $('#singleAnswer1');
        singleAswerContainer.find('.but[anserId=0]');
        let theAnswerId = selector.attr('answerId');
        let selectedOptionA = selector.val();
        console.log("answerId " + theAnswerId);
        console.log("rowId " + theAnswerId);
        console.log("selectedOptionA " + selectedOptionA);
        $('.but[answerId=' + theAnswerId + ']').val(selectedOptionA);
    };

    let thisSourceSelected = function (selector) {
        // alert('inside function');

        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        // console.log("RowId"+rowId);
        // console.log("selectedOption"+selectedOption);

        if (selector.val() === 'singleselect') {

            $('#singleSelect' + rowId).show();
        } else {
            $('#singleSelect' + rowId).hide();
        }

        if (selector.val() === 'radioButton') {

            $('#singleAnswer' + rowId).show();
        } else {
            $('#singleAnswer' + rowId).hide();
        }
        if (selector.val() === 'checkbox') {

            $('#multipleAnswer' + rowId).show();
        } else {
            $('#multipleAnswer' + rowId).hide();
        }


    }

   
    


    
    //main div
     var row_html = function (rw) {
        var this_html = '<div style="padding-bottom:20px;" id="add_contact_row_' + rw + '" class="row">' +
         '' +
            '<div class="col-sm-12">'+
                '<div class="col-md-3">'+
                   '<label for="otherField8">Enter Pecentange</label>' +
                                    '<input name="FeeClass[concessions][' + rw + '][percentage]" type="number"  min="0" max="99" answerId="3"' +
                                           'class="form-controls w-100 answerOptions"  id="otherField2">' +
                '</div>'+

                '<div class="col-md-9">'+
                   '<label for="otherField8">Paste Payment Codes</label>' +
                   '<textarea rows="5" cols="70" name="FeeClass[concessions][' + rw + '][payment_codes]" answerId="3"' +
                                           'class="form-controls w-100 answerOptions"  id="otherField2"></textarea>' +
                '</div>'+
            '</div>'
         
            '<div class="col-auto no-padding-left">' +
            '<span class="remove_contact_button" style="font-size:22px;" data-for="add_contact_row_' + rw + '"><i class="fas fa-minus-circle text-danger"></i></span>' +
            '</div>'+
            
            
          '</div>'
        return this_html;

    }
    
    let addQuestion = function(){
        row++;
        var new_row_html = row_html(row);
        $('div#submission_Contacts').append(new_row_html);
        let singleAswerContainer = $('#add_contact_row_' + row);
        singleAswerContainer.find(".source_questions").on("change", function () {
            console.log("messaf");
          //  thisSourceSelected($(this));
          let selector = $(this);
        let rowId = selector.attr('rowId'); 
        
        
        populateAnswers(rowId);
        
             singleAswerContainer.find(".answerOptions").on("change", function () {
                thisAnswerOption($(this));
    
            });
        });
       
        // singleAswerContainer.find(".but").on("change", function () {
        //     thisBut($(this));
        // });
        
    }
    
    let populateAnswers = function(row){
        let selector = $("#question_type_selector"+row);
       // console.log(selector );
        
        let rowId = selector.attr('rowId');
        let selectedOption = selector.val();
        
         var answer_html ='';  
        if (selector.val() === 'singleselect') {
            answer_html =single_input_question(row);
            
        } else if(selector.val() === 'radioButton') {
            answer_html =single_answer_question(row);
        } else if(selector.val() === 'checkbox') {
           answer_html =multi_answer_question(row);
        }
        console.log($("#answers_container"+row));

        $("#answers_container"+row).html(answer_html);
       
    }
 
    $('.add_contact_button').on('click', function (e) {
       
        e.preventDefault();
        addQuestion();
        
    });


    $(".source_questions").on("change", function () {
        
        //thisSourceSelected($(this));
        let selector = $(this);
        let rowId = selector.attr('rowId');        
        populateAnswers(rowId);
    });
    
     $('#theevent').on("click", function () {
         console.log("init");
        firstBlock((this));
    });

    $(".answerOptions").on("change", function () {
        thisAnswerOption($(this));
    });

    $(".but").on("change", function () {
        thisBut($(this));
    });


    $('body').on('click', '.remove_contact_button', function (e) {
        e.preventDefault();
        $('div#' + $(this).attr('data-for')).remove();
    });
    
   addQuestion();
   
   
  
    
});

JS;
$this->registerJs($script);
?>



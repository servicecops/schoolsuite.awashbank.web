<?php

use app\models\SchoolConcessions;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = "Concession Assignment";
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <hr style="border-top: 2px solid #f6fbfc;"/>

    <div class="col-sm-12"><h3 >Assigned Concessions</h3></div>
    <div class="col-sm-12">
    <?php


  ?>

    <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Select percentage to search students with that concession.

    </div>

    <div class="col-md-4">

        <?php
        $items = ArrayHelper::map(SchoolConcessions::find()->where(['fee_id'=>$id])->all(), 'id', 'percentage');
        echo $form->field($model, 'concessions_id')
            ->dropDownList(
                $items,           // Flat array ('id'=>'label')
                ['prompt' => 'Select Percentage',
                ]    // options
            ); ?>
    </div>




    <div class="col-sm-5">

        <div class="row">
            <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Remove Action </b> will detach selected student from that percentage
            </div>
            <div class="col-md-12">
                <div class="box-body table table-responsive no-padding">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class='clink'>payment_code</th>
                            <th class='clink'>Student Name</th>

                            <th class='clink'>class_code</th>
                            <th class='clink'>Remove</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($dataProvider) :
                            foreach ($dataProvider as $k => $v) : ?>
                                <tr data-key="0">
                                    <td class="clink"><?= ($v['payment_code']) ? '<a href="' . Url::to(['/student/view', 'id' => $v['id']]) . '">' . $v['payment_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>

                                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td>
                                        <?= Html::a('<i class="fa fa-close"></i>', ['remove', 'id' => $v['id'], 'concession'=>$v['concession'],'feeid'=>$id], ['class' => 'aclink']) ?>
                                    </td>

                                </tr>
                            <?php endforeach;
                        else :?>
                            <tr>
                                <td colspan="8">No student found</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>
</div>




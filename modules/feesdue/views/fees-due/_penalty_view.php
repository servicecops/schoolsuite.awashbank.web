<?php
/**
 * @var \app\models\InstitutionFeesDuePenalty $penaltyModel
 * @var \app\models\FeesDue $model
 */

use unclead\multipleinput\MultipleInput;
use yii\bootstrap4\ActiveForm;

?>
<div class="col-md-12 col-md-12 col-sm-12">
    <hr>
    <div class="col-lg-6 col-sm-6 col-md-12 no-padding ">
        <div class="col-lg-6 col-md-6 profile-label">Penalty Type</div>
        <div class="col-lg-6 col-md-6 profile-text"><?= $penaltyModel->penalty_type ? ucwords(str_replace('_', ' ', strtolower($penaltyModel->penalty_type))) : '-' ?></div>
    </div>

    <?php if ($penaltyModel->penalty_type == 'PERCENTAGE') : ?>
        <div class="col-lg-6 col-sm-6 col-md-12 no-padding ">
            <div class="col-lg-6 col-md-6 profile-label">Penalty Percentage</div>
            <div class="col-lg-6 col-md-6 profile-text"><?= $penaltyModel->penalty_amount ? $penaltyModel->penalty_amount : '' ?>
                %
            </div>
        </div>

    <?php elseif($penaltyModel->penalty_type == 'FIXED') : ?>
        <div class="col-lg-6 col-sm-6 col-md-12 no-padding ">
            <div class="col-lg-6 col-md-6 profile-label">Penalty Amount</div>
            <div class="col-lg-6 col-md-6 profile-text"><?= $penaltyModel->penalty_amount ? $penaltyModel->penalty_amount : '' ?></div>
        </div>
    <?php else : ?>
        <div class="col-lg-6 col-sm-6 col-md-12 no-paddin g ">
            <div class="col-lg-6 col-md-6 profile-label">Penalty Amount

                <?php $form = ActiveForm::begin([
                ]);
                ?>
                <?= $form->field($penaltyModel, 'penalty_variable_amount')->widget(MultipleInput::className(), [
                        'removeButtonOptions'=>['class'=>'hidden-component'],
                        'addButtonOptions'=>['class'=>'hidden-component']
                ])->label(false);
                ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($penaltyModel->penalty_frequency == 'RECURRENT') : ?>
        <?php
        $penaltyUnits = 'Day';
        if ($penaltyModel->penalty_apply_frequency_unit == 'M') $penaltyUnits = 'Month';
        if ($penaltyModel->penalty_apply_frequency_unit == 'W') $penaltyUnits = 'Week';

        //Work on plural
        if ($penaltyModel->penalty_apply_frequency_term > 1) $penaltyUnits .= 's';
            ?>
            <div class="col-lg-6 col-sm-6 col-md-12 no-padding ">
            <div class="col-lg-6 col-md-6 profile-label">Apply Penalty</div>
            <div class="col-lg-6 col-md-6 profile-text">Every <?= $penaltyModel->penalty_apply_frequency_term . " $penaltyUnits" ?></div>
        </div>
    <?php else : ?>
        <div class="col-lg-6 col-sm-6 col-md-12 no-padding ">
            <div class="col-lg-6 col-md-6 profile-label">Apply Penalty</div>
            <div class="col-lg-6 col-md-6 profile-text">ONLY ONCE</div>
        </div>
    <?php endif; ?>

    <?php
    $penaltyUnits = 'Days';
    if ($model->arrears_after_units == 'M') $penaltyUnits = 'Months';
    if ($model->arrears_after_units == 'W') $penaltyUnits = 'Weeks';
    ?>


</div>
<div class="col-md-12 col-md-12 col-sm-12">
    <div class="col-lg-6 col-sm-6 col-md-12 no-padding ">
        <div class="col-lg-6 col-md-6 profile-label">Penalty Starts</div>
        <div class="col-lg-6 col-md-6 profile-text"><?= $model->arrears_after_count . " $penaltyUnits after effective date" ?></div>
    </div>
</div>

<?php

$script = <<< JS



$("document").ready(function(){ 
    $('.list-cell__penalty_variable_amount').find('input').prop('disabled', true)
    });
   


JS;
$this->registerJs($script);
?>

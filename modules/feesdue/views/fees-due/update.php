<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\FeesDue */

$this->title = 'Update ' . $model->description . '';
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="letter">


        <div class="col-md-12" style="background: #efefef">
            <div class="wizard">
                <a class="col-md-3 current"><span class="badge">1</span> Edit Fee Information</a>
                <a href="<?= Url::to(['view', 'id' => $model->id]) ?>" class="col-md-3"><span class="badge">2</span>
                    Assign Classes /Exceptions</a>
                <a class="col-md-3 gray"><span class="badge badge-inverse">3</span> Submit For Approve</a>
                <a class="col-md-2 gray"><span class="badge">4</span> Approve Fee</a>
            </div>
        </div>
        <div class="col-md-12"><h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3>
        </div>

        <div class="col-md-12">


            <?= $this->render('_form', [
                'model' => $model,
                'penaltyModel' => $penaltyModel,
            ]) ?>

        </div>
        <div style="clear: both"></div>
    </div>
</div>

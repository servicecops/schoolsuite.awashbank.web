<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;


?>

<div style="color:#F7F7F7">Student Information</div>
<?php $form = ActiveForm::begin([
    'action' => ['search-std','id'=>$id],
    'method' => 'get',
    'options' => ['class' => 'formprocess'],
]); ?>


<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-2">Enter Student Name or Student Code</div>
    <div class="col-md-6">
        <?= $form->field($model, 'modelSearch', ['inputOptions' => ['class' => 'form-control'],])->textInput(['title' => 'Enter student name or payment code',

            'data-toggle' => 'tooltip',

            'data-trigger' => 'hover',

            'data-placement' => 'bottom'])->label(false) ?>

    </div>
    <div class="col-md-2">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>

</div>
<?php ActiveForm::end(); ?>



<?php


$script = <<< JS
$('[data-toggle="tooltip"]').tooltip({

    placement: "right",

    trigger: "hover"

});

JS;
$this->registerJs($script);
?>

<div class="row hd-title" data-title="<?= $model->description ?>">

<div class="col-md-8 ">
    <h3>Fee Members</h3>
</div>
<div id="exempt" class="col-md-12 table-responsive " style="padding:0 0 0 15px;">
    <?php

    use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

    echo "<table id='exempted' class ='table-bordered table table-striped'>";
    echo "<tr>";
    echo "<th class='text-center'> Sr.No </th>";

    echo "<th class='text-center'> Payment code </th>";
    echo "<th class='text-center'> Class Code </th>";
    echo "<th class='text-center'> Student Name </th>";
    echo "<th class='text-center'> Number of Sent Reminders </th>";
    echo "<th class='text-center'> Next Reminder Date </th>";
    echo "<th class='text-center'> Reminders Closed </th>";
    echo "</tr>";
    if (!empty($smsReminders)) {
        $i = 1;
        if (isset($_GET['page'])) {
            $i = (($_GET['page'] - 1) * 20) + 1;
        }
        foreach ($smsReminders as $k => $v) {
            Yii::trace($v);
            echo "<tr>";
            echo "<td class='text-center'>" . $i . "</td>";
            echo "<td class='text-center'>" . $v['payment_code'] . "</td>";
            echo "<td class='text-center'>" . $v['class_code'] . "</td>";
            echo "<td class='text-center'>" . $v['student_name'] . "</td>";
            echo "<td class='text-center'>" . $v['number_sent_reminders'] . "</td>";
            echo "<td class='text-center'>" . $v['next_reminder_date'] . "</td>";
            if($v['reminders_closed']){
                echo "<td class='text-center'>" ."Yes". "</td>";
            } else{
                echo "<td class='text-center'>" ."No"."</td>";
            }
            echo "</tr>";
            $i++;
        }
    } else {
        echo "<tr>";
        echo "<td class='text-center'></td>";
        echo "<td class='text-center'>Fee has not been assigned to any student </td>";
        echo "<td class='text-center'></td>";
        echo "<td class='text-center'></td>";
        echo "</tr>";

    }
    echo "</table>";
    ?>
    <div><?= LinkPager::widget(['pagination' => $st_pages]); ?></div>
</div>
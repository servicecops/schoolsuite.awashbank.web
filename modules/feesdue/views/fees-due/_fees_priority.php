<?php


use app\models\Classes;
use app\models\FeesDue;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\sortinput\SortableInput;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $priorityModem DynamicModel */
/* @var $feesList FeesDue[] */
/* @var $model app\models\FeesDue */

$list = [];
foreach ($feesList as $fee) {
    $list[$fee['id']] = ['content' => $fee['description'] . '   Effective:' . $fee['effective_date'] . ' to ' . $fee['end_date'] . ' - Birr' . number_format($fee['due_amount'], 2)];
}

$this->title = 'Fees Priority';
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="color:#fff">Fees Priority</p>

<div class="letter">
    <div class="col-md-12"><h3><i class="fa fa-arrow-down"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3></div>


        <div class="col-md-3 no-padding">

            <?php $form = ActiveForm::begin([
                'action' => ['fees-priority'],
                'method' => 'get',
                'options' => ['class' => 'formprocess']
            ]); ?>

            <div class="col-md-12 no-padding">
                <div class="col-md-10 no-padding">
                    <?= $form->field($searchModel, 'student_class')->dropDownList(
                        ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'class_description'), ['prompt' => 'Filter class', 'class' => 'form-control input-sm']
                    )->label(false) ?>
                </div>
                <div class="col-md-2 no-padding">
                    <?= Html::submitButton("<i class='fa fa-search'></i>", ['class' => 'btn btn-primary btn-sm']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>


        <div class="col-md-9  ">
            <?php if (isset($error)): ?>
                <div class="col-md-12"><p class="alert alert-danger"><i class="fa fa-info"></i> <?= $error ?></p>
                </div><?php endif; ?>
            <?php if (isset($message)): ?>
                <div class="col-md-12"><p class="alert alert-info"><i class="fa fa-check-circle"></i><?= $message ?></p>
                </div> <?php endif; ?>
            <div class="col-md-12"><b>Instructions: </b> To alter the fees priority, drag and drop them to alter
                their order and save.
            </div>
            <?php $form = ActiveForm::begin([
                'action' => 'fees-priority',
                'method' => 'post'
            ]); ?>


            <div class="col-md-12 col-lg-12 no-padding">

                <div class="col-sm-12">
                    <?= $form->field($priorityModel, 'priority_order')
                        ->widget(SortableInput::className(),
                            [
                                'items' => $list,
                                'hideInput' => true,

                            ]
                        )->label(false) ?>
                </div>


            </div>


            <div class="form-group col-md-12 col-sm-6 col-lg-4 no-padding">
                <div class="col-md-6">
                    <?= Html::submitButton("Save",
                        ['class' => 'btn btn-block btn-primary']) ?>
                </div>

            </div>

            <?php ActiveForm::end(); ?>

        </div>
        <div style="clear: both"></div>



</div>


<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FeesDue */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */
Yii::trace($model->recurrent);
//if(!isset($model->recurrent)) $model->recurrent = true;
$disableControls = false;
if(!$model->isNewRecord && $model->approval_status) {
    $disableControls = true;
}
if($model->approval_status)
    $disableControls = false; //Approved fees can be edited but via a workflow
//Yii::trace($model->has_penalty);
?>
<?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>

<div class="row">


    <?php
    if ($this->context->action->id == 'update')
        $action = ['update', 'id' => $_REQUEST['id']];
    else
        $action = ['create'];
    ?>

    <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Please make sure you enter the
        correct EFFECTIVE and END date. You will only be able to apply a fee in the period between it's effective and
        end date.
    </div>
    <?php $form = ActiveForm::begin([
        'action' => $action,

    ]); ?>

    <div class="col-md-12 col-lg-12 no-padding">

        <?php if (\Yii::$app->user->can('schoolpay_admin')) : ?>
            <div class="col-sm-6">
                <?php
                $url = Url::to(['../schoolcore/core-school/active-schoollist']);
                $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                    'initValueText' => $selectedSchool, // set the initial display text
                    'class' => 'form-control',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'disabled' => !$model->isNewRecord,
                    'options' => [
                        'placeholder' => 'Filter School',
                        'id' => 'fee_selected_school_id',

                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label(false); ?>
            </div>
        <?php endif; ?>



        <div class="col-sm-6">
            <?= $form->field($model, 'recurrent', ['inputOptions' => ['class' => 'form-control',
                'placeholder' => 'Description', 'disabled'=>$disableControls,
                ]])->dropDownList(
                ['1' => 'Recurrent', '0' => 'One-Time'],
                ['id' => 'fee_type_select', 'prompt' => 'Fee Type']
            )->label(false) ?>
        </div>


    </div>

    <div class="col-md-12 col-lg-12 no-padding">

        <div class="col-sm-6">
            <?= $form->field($model, 'due_amount', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Fee Amount', 'step' => '0.1', 'disabled'=>$disableControls]])->textInput()->label('') ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Description']])->textInput()->label('') ?>
        </div>

    </div>


    <div class="col-md-12 col-lg-12 no-padding">

        <div class="col-sm-6">
            <?= $form->field($model, 'effective_date')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'emp_joining_date',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('effective_date') ? ' is-invalid' : ''),
                        'prompt' => 'Effective Date',
                        'disabled' => $model->approval_status,

                    ],
                    'dateFormat' => 'yyyy-MM-dd'
                ])->label('Effective Date *') ?>

        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date')->widget(DatePicker::className(),
                [
                    'model' => $model,
                    'attribute' => 'emp_joining_date',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control' . ($model->hasErrors('end_date') ? ' is-invalid' : ''),
                        'prompt' => 'Effective Date',
                    ],
                    'dateFormat' => 'yyyy-MM-dd'
                ])->label('End Date *') ?>
        </div>


        <div class="col-md-12 col-lg-12 no-padding" id="recurrent-settings-container">

            <div class="col-sm-12" style="color: #0b58a2">
                <i class="fa fa-info-circle"></i>
                <b>Recurrent fees will be re-applied periodically according to the
                    frequency you set below</b>
            </div>

            <div class="col-sm-6">
                <?= $form->field($model, 'apply_frequency', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Apply Every']])->dropDownList(
                    [1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5', 6 => '6',
                        7 => '7', 8 => '8', 9 => '9', 10 => '10', 11 => '11', 12 => '12',
                        13=>'13', 14=>'14', 15=>'15', 16=>'16', 17=>'17', 18=>'18',
                        19=>'19', 20=>'20', 21=>'21', 22=>'22', 23=>'23', 24=>'24', 25=>'25',
                        26=>'26', 27=>'27', 28=>'28', 29=>'29', 30=>'30', 31=>'31'],
                    ['id' => 'apply_frequency_unit_select', 'prompt' => 'Term Duration']
                )->label('Apply Every *') ?>
            </div>

            <div class="col-sm-6">
                <?= $form->field($model, 'apply_frequency_unit', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Apply Every']])->dropDownList(
                    ['MONTHLY' => 'Months', 'WEEKLY' => 'Weeks', 'DAILY' => 'Days'],
                    ['id' => 'apply_frequency_select', 'prompt' => 'Apply Every']
                )->label('Frequency *') ?>
            </div>

            <?php if(!$model->isNewRecord): ?>
            <div class="col-sm-4">
                <?= $form->field($model, 'next_apply_date')->widget(DatePicker::className(),
                    [
                        'model' => $model,
                        'attribute' => 'next_apply_date',
                        'dateFormat' => 'yyyy-MM-dd',
                        'clientOptions' => [
                            'changeMonth' => true,
                            'changeYear' => true,
                            'yearRange' => '1900:' . (date('Y') + 1),
                            'autoSize' => true,
                        ],
                        'options' => [
                            'class' => 'form-control',
                            'prompt' => 'Effective Date',
                        ],
                    ])->label('Next Apply Date') ?>
            </div>
            <?php endif; ?>
        </div>

        <?php $penalyCheckBoxDisplay = $disableControls ? 'none' : 'auto'; ?>
        <div class="col-sm-6" style="display:<?= $penalyCheckBoxDisplay ?>">
            <?= $form->field($model, 'has_penalty', ['inputOptions' => ['class' => 'form-control has_penalty_checkbox', 'placeholder' => 'Apply Every']])
                ->checkbox(['id' => 'has_penalty_select', ],
                false
            )->label('Has Penalty') ?>
        </div>
        <?php if (\Yii::$app->user->can('automatic_alerting')) : ?>
        <?php $smsCheckBoxDisplay = $disableControls ? 'none' : 'auto'; ?>
        <div class="col-sm-6" style="display:<?= $penalyCheckBoxDisplay ?>">
            <?= $form->field($model, 'enable_reminders', ['inputOptions' => ['class' => 'form-control enable_reminders_checkbox', 'placeholder' => 'Apply Every']])
                ->checkbox(['id' => 'enable_reminders_select', ],
                    false
                )->label(false) ?>
        </div>
        <?php endif; ?>
        <div class="col-sm-6" style="display:<?= $penalyCheckBoxDisplay ?>">
            <?= $form->field($model, 'credit_hour', ['inputOptions' => ['class' => 'form-control credit_hour', 'placeholder' => '']])
                ->checkbox(['id' => 'credit_hour', ],
                    false
                )->label('Is Credit Hour') ?>
        </div>
        <div class="col-md-12 col-lg-12 no-padding" id="arrears-settings-container">
            <hr>
            <div class="col-sm-12" style="color: #0b58a2">
                <i class="fa fa-info-circle"></i>
                <b>If you wish to set penalties, the fee will be considered to be in arrears and unpaid after the set
                    period below</b>
            </div>

            <div class="col-md-3 col-sm-12">
                <?php
                $selectOptions = [];
                for ($i = 1; $i <= 31; $i++) {
                    $selectOptions[$i] = (string)$i;

                }
                ?>
                <?= $form->field($model, 'arrears_after_count', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Fee is considered unpaid after']])->dropDownList(
                    $selectOptions,
                    ['id' => 'arrears_after_count_select', 'prompt' => 'Consider As Unpaid After']
                )->label('Consider As Unpaid After') ?>
            </div>

            <div class="col-md-3 col-sm-12">
                <?= $form->field($model, 'arrears_after_units', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Select Units']])->dropDownList(
                    ['M' => 'Months', 'W' => 'Weeks', 'D' => 'Days'],
                    ['id' => 'arrears_after_units_select', 'prompt' => 'Select Units']
                )->label('*') ?>
            </div>


            <div class="col-md-3 col-sm-12">
                <?= $form->field($penaltyModel, 'penalty_type', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Fee is considered unpaid after', 'disabled'=>$disableControls]])->dropDownList(
                    [
                        'FIXED' => 'Fixed Constant Amount',
                        'PERCENTAGE' => 'Fixed Percentage of Outstanding Balance',
                        'VARYING_AMOUNT' => 'Varying Penalty Amount',
                        'VARYING_PERCENTAGE' => 'Varying Percentage of Outstanding Balance',
                    ],
                    ['id' => 'penalty_type_select', 'prompt' => 'Penalty Type']
                )->label('Penalty Type *') ?>
            </div>

            <div class="col-md-3 col-sm-12" id="_fixed_amount_container">
                <?= $form->field($penaltyModel, 'penalty_amount', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Penalty Amount or Percentage', 'step' => '0.1', 'disabled'=>$disableControls]])->textInput()->label('Penalty Amount or Percentage *') ?>
            </div>

            <div class="col-md-3 col-sm-12 alert-primary" id="_variable_amount_container">
                <?= $form->field($penaltyModel, 'variable_amount')->widget(MultipleInput::className(), [
                    'max' => 10,
                    'showGeneralError' =>'true'
                ])->label('Varying amount or percentage');
                ?>

            </div>

            <div class="col-md-3 col-sm-12">
                <?= $form->field($penaltyModel, 'penalty_frequency', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Select Units', 'disabled'=>$disableControls]])->dropDownList(
                    ['ONETIME' => 'One Time - Apply only Once', 'RECURRENT' => 'Recurrent - Apply Recurrently'],
                    ['id' => 'penalty_frequency_select', 'prompt' => 'Select Frequency']
                )->label('Penalty Frequency *') ?>

            </div>




            <div class="col-md-3 col-sm-12 penalty_recurrent_settings_container">
                <?php
                $selectOptions = [];
                for ($i = 1; $i <= 31; $i++) {
                    $selectOptions[$i] = (string)$i;

                }
                ?>
                <?= $form->field($penaltyModel, 'penalty_apply_frequency_term', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Fee is considered unpaid after', 'disabled'=>$disableControls]])->dropDownList(
                    $selectOptions,
                    ['id' => 'penalty_apply_frequency_term_select', 'prompt' => 'Select Recurrency']
                )->label('Re-apply penalty every *') ?>
            </div>

            <div class="col-md-3 col-sm-12 penalty_recurrent_settings_container">
                <?= $form->field($penaltyModel, 'penalty_apply_frequency_unit', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Select Units', 'disabled'=>$disableControls]])->dropDownList(
                    ['M' => 'Months', 'W' => 'Weeks', 'D' => 'Days'],
                    ['id' => 'penalty_apply_frequency_unit_select', 'prompt' => 'Select Units']
                )->label('*') ?>
            </div>

            <div class="col-md-12 col-lg-12 no-padding">
                <hr>
                <div class="col-md-3 col-sm-12">
                    <?= $form->field($penaltyModel, 'penalty_expiry_date')->widget(DatePicker::className(),
                        [
                            'model' => $penaltyModel,
                            'attribute' => 'penalty_expiry_date',
                            'dateFormat' => 'yyyy-MM-dd',
                            'clientOptions' => [
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'prompt' => 'Effective Date',
                            ],
                        ])->label('Penalty should not be applied beyond this date') ?>
                </div>

                <div class="col-md-3 col-sm-12">
                    <?= $form->field($penaltyModel, 'maximum_penalty_amount', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Penalty should not exceed', 'step' => '0.1', 'disabled'=>$disableControls]])->textInput()->label('Penalty should not exceed') ?>
                </div>

                <div class="col-md-3 col-sm-12">
                    <?= $form->field($penaltyModel, 'maximum_penalty_application_count', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Penalty application count cannot exceed these number of times', 'step' => '1', 'disabled'=>$disableControls]])->textInput()->label('Stop applying after these number of times') ?>
                </div>
            </div>
        </div>


    </div>

    <div class="form-group col-md-12 col-sm-6 col-lg-4 no-padding">
        <div class="col-md-6">
            <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update',
                ['class' => 'btn btn-block btn-primary', $model->child_of_recurrent]) ?>
        </div>
        <div class="col-md-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

$script = <<< JS



$("document").ready(function(){ 
    function evaluateRecurrentVisibility() {
        if($('#fee_type_select').val() === '1') {
           $('#recurrent-settings-container').show()
        }else {
           $('#recurrent-settings-container').hide() 
        }
}

    function evaluatePenaltyRecurrentVisibility() {
        if($('#penalty_frequency_select').val() === 'RECURRENT') {
           $('.penalty_recurrent_settings_container').show()
        }else {
           $('.penalty_recurrent_settings_container').hide() 
        }
}

function evaluatePenaltyAmountVisibility() {
        var _penaltyTypeSelect = $('#penalty_type_select');
        var _FixedAmountContainer = $('#_fixed_amount_container');
        var _VaryingAmountContainer = $('#_variable_amount_container');
        
        if(_penaltyTypeSelect.val() === 'FIXED' || _penaltyTypeSelect.val() === 'PERCENTAGE') {
           
           _FixedAmountContainer.show()
        }else {
          _FixedAmountContainer.hide() 
        }
        
        
        if(_penaltyTypeSelect.val() === 'VARYING_AMOUNT' || _penaltyTypeSelect.val() === 'VARYING_PERCENTAGE') {
           _VaryingAmountContainer.show()
        }else {
          _VaryingAmountContainer.hide() 
        }
}

function evaluatePenaltyControls() {
        if($('#has_penalty_select').is(":checked")) {
           $('#arrears-settings-container').show()
        }else {
           $('#arrears-settings-container').hide() 
        }
}

evaluateRecurrentVisibility();
evaluatePenaltyRecurrentVisibility();
evaluatePenaltyControls();
evaluatePenaltyAmountVisibility();

    //On changing fee type, hide or show recurrent controls
   var _body = $('body');
   
   _body.on('change', '#fee_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   _body.on('change', '#penalty_frequency_select', function(){
       evaluatePenaltyRecurrentVisibility();
   });   
   
   _body.on('change', '#has_penalty_select', function(){
       evaluatePenaltyControls();
   });
   
      _body.on('change', '#penalty_type_select', function(){
       evaluatePenaltyAmountVisibility();
   });
   
    });
   


JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = "Permissions Assignment";
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="perms-view hd-title" data-title="Permissions Assignment">

 <section class="content-header">
<div class="row">
  <div class="col-md-12">
    <h2 class="page-header">    
        <i class="fa fa-institution"></i> <?= Html::encode($this->title) ?>

        <div class="pull-right">
        <?= Html::a('Delete', ['delete', 'id' => $model->name], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </div> 
        <div class="pull-right">
            &nbsp;
        </div>

         <div class="pull-right">
       <?= Html::a('Update', ['update', 'id' => $model->name], ['class' => 'btn btn-primary']) ?>
        </div>

    </h2>
  </div><!-- /.col -->
</div>
</section>

<div class="class-associate row">
        <div class="col-md-12 col-lg-12">
            <div class="col-lg-5 col-sm-5 col-md-12">
                <b>Permissions Available</b>:
                <div class="form-group has-feedback">
                    <input name="search_av" type="text" class="form-control perm-search" placeholder="Search..." data-target="available"/>
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
                <?php
                echo Html::listBox('selected', '', $available, [
                    'id' => 'available',
                    'multiple' => true,
                    'size' => 20,
                    'class'=>'form-control',
                    'style' => 'width:100%;height:350px;padding:5px']);
                ?>
            </div>

             <div class="col-lg-2 col-sm-2 col-md-12 text-center" style="padding-top:100px;">
                <br><br>
                <?php
                echo Html::a('>>', '#', ['class' => 'btn btn-block btn-primary', 'title' => 'Assign', 'data-action' => 'assign']) . '<br><br>';
                echo Html::a('<<', '#', ['class' => 'btn btn-block btn-danger', 'title' => 'Delete', 'data-action' => 'delete']) . '<br>';
                ?>
            <br><br>
            </div>

            <div class="col-lg-5 col-sm-5 col-md-12">
                <b>Assigned</b>:
                <div class="form-group has-feedback">
                    <input name="search_asgn" type="text" class="form-control perm-search" placeholder="Search..." data-target="assigned"/>
                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
                <?php
                echo Html::listBox('selected', '', $assigned, [
                    'id' => 'assigned',
                    'multiple' => true,
                    'size' => 20,
                    'class'=>'form-control',
                    'style' => 'width:100%;height:350px;padding:5px']);
                ?>
            </div>

        </div>
        </div>
        <?php $this->render('_script',['name'=>$model->name]); ?>
</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */
/* @var $model app\models\FeesDue */


$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$allowEdit = (($model->created_by == Yii::$app->user->identity->id) && ($model->approval_status == false)) || $admin;

$allowEdit = true; //Allow update by all, as requested by BiB


?>

<div class="row ">

    <?php if (Yii::$app->session->hasFlash('actionFailed')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='errorAlert'>&times;</a>
            <div class="notify-content">
                <?= Yii::$app->session->getFlash('actionFailed'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-12">
        <div class="col-md-12 no-padding"
             style="background: <?= ($model->ready_for_approval == true) ? '#01b5d2 ' : '#efefef' ?>;">
            <div class="wizard">
                <?php if ($allowEdit): ?>
                    <a href="<?= Url::to(['update', 'id' => $model->id]) ?>" class="aclink col-xs-3"><span
                                class="badge">1</span> Edit Fee Information</a>
                <?php else: ?>
                    <a class="col-xs-3 gray"><span class="badge">1</span> Edit Fee Information</a>
                <?php endif ?>
                <?php if ($allowEdit) : ?>
                    <a href="<?= Url::to(['view', 'id' => $model->id]) ?>" class="aclink col-xs-3"><span
                                class="badge">2</span> Assign Classes /Exceptions</a>
                <?php else : ?>
                    <a class="col-xs-3 gray"><span class="badge">2</span> Assign Classes /Exceptions</a>
                <?php endif; ?>

                <a class="<?= ($model->ready_for_approval == true) ? 'gray ' : 'current ' ?> col-xs-3"><span
                            class="badge badge-inverse">3</span> Submit For Approval</a>

                <a class="<?= ($model->ready_for_approval == true) ? 'current ' : 'gray ' ?> col-xs-2"><span
                            class="badge">4</span> Approve Fee</a>
            </div>
        </div>

        <div class="col-md-12 " style="background: white;padding:10px">
            <b style="color:#D82625; font-size: 20px;">Important: </b> <br>
            <?php if ($model->ready_for_approval == false && ($model->created_by == Yii::$app->user->identity->id || $admin)) : ?>
                <span style="color:#01b5d2; font-size: 16px;">After you have submited for approval, <b>ANOTHER SCHOOL USER</b> needs to verify and approve this fee </span>
                <br>
                Click the <b>Submit for approval</b> button below &nbsp;&nbsp;
                <b>Note: </b>Fee application is done by two users to avoid mistakes (It's a Team Job)<br><br>

                This fee will be applied to all students in the <b>selected
                    classes</b> below except the exempted ones

            <?php elseif (($model->approval_status == false) && ($model->ready_for_approval == true) && ($model->created_by != \Yii::$app->user->identity->id || $admin)) : ?>
                <span style="color:#01b5d2; font-size: 16px;">Please <b>VERIFY</b> and <b>APPROVE THIS FEE</b> </span>
                <br>
                Click on the <b>Approve</b> button below &nbsp;&nbsp;
                <b>Note: </b>Fee application is done by two users to avoid mistakes (It's a Team Job)<br> <br>
                Otherwise if you have any queries with this fee contact the creator(
                <b><?= ucwords($model->createdBy->fullname) ?></b>) to fix them before approving.

            <?php elseif (($model->approval_status == false) && ($model->ready_for_approval == true) && ($model->created_by == \Yii::$app->user->identity->id || $admin)) : ?>
                <span style="color:#01b5d2; font-size: 16px;">This fee has been <b>SUBMITTED FOR APPROVAL</b> but not yet approved. </span>
                <br><br>
                Please contact <b>ANOTHER SCHOOL USER</b> to verify and approve this fee.<br>
                <b>Note: </b>Fee application is done by two users to avoid mistakes (It's a Team Job)

            <?php elseif (($model->ready_for_approval == false) && ($model->created_by != \Yii::$app->user->identity->id || $admin)) : ?>
                <span style="color:#01b5d2; font-size: 16px;">This fee has <b>NOT BEEN SUBMITTED</b> for approval by the creater. </span>
                <br><br>
                Contact
                <b><?= ucwords($model->createdBy->fullname) ?></b> to submit this fee for approval so that you can APPROVE it.
                <br>
                <b>Note: </b>Fee application is done by two users to avoid mistakes (It's a Team Job)
            <?php elseif ($model->approval_status == true) : ?>
                <span style="color:#01b5d2; font-size: 16px;">This fee has been <b>APPROVED</b> </span> <br>
            <?php endif; ?>

        </div>

    </div>

    <div class="col-md-12">
        <div class="col-md-8 no-padding">
            <h3><i class="fa fa-exchange"></i> &nbsp;<?= Html::encode($this->title) ?></h3>
        </div>

        <div class="col-md-4 no-padding">
            <div class="pull-right" style="padding-top:20px;">
                <?= Html::a('Back', ['index'], ['class' => 'btn btn-default btn-sm aclink']) ?>
                <?php if (($model->created_by == Yii::$app->user->identity->id) && ($model->approval_status == false)): ?>

                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>

                    <?php if (empty($class_fee)) : ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-label"><?= $model->getAttributeLabel('school_id') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->school->school_name) ? Html::a($model->school->school_name, ['/schoolcore/core-school/view', 'id' => $model->school->id]) : "--" ?></div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('due_amount') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->due_amount ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->description ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('effective_date') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->effective_date ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('end_date') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->end_date ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('created_by') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->createdBy) ? ucwords($model->createdBy->fullname) . ' (' . $model->createdBy->username . ')' : '--'; ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('priority') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->priority ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label">Fee Type</div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->recurrent ? 'Recurrent' : 'One-Time' ?></div>
        </div>

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->recurrent ? 'Apply Every' : '' ?></div>
            <?php
            $applyEvery = $model->apply_frequency_unit == 'MONTHLY' ? 'Months' : ($model->apply_frequency_unit == 'WEEKLY' ? 'Weeks' : 'Days');
            ?>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->recurrent ? $model->apply_frequency . ' ' . $applyEvery : '' ?></div>
        </div>
    </div>

    <?php if ($model->recurrent) : ?>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                <div class="col-lg-6 col-xs-6 profile-label">Last Apply Date</div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->last_apply_date ? $model->last_apply_date : '-' ?></div>
            </div>

            <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                <div class="col-lg-6 col-xs-6 profile-label">Next Apply Date</div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->next_apply_date ? $model->next_apply_date : '' ?></div>
            </div>
        </div>
    <?php endif; ?>


        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                <div class="col-lg-6 col-xs-6 profile-label">Credit Hour Fee</div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->credit_hour ? 'Yes' : 'No' ?></div>
            </div>

           
        </div>



    <?php if ($penaltyModel) : ?>
        <?= $this->render('_penalty_view', ['penaltyModel' => $penaltyModel, 'model' => $model]); ?>
    <?php endif; ?>


</div>
<p><br></p>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12  no-padding">
            <div class="col-md-3 table-responsive no-padding">
                <?php

                echo "<table class ='table-bordered table'>";
                echo "<tr style='background-color:#F9F9F9'>";
                echo "<th class='text-center'> Selected Classes </th>";
                echo "<th class='text-center'> Remove </th>";
                echo "</tr>";
                if (!empty($class_fee)) {
                    foreach ($class_fee as $k => $v) {
                        $cid = $v['cid'];
                        $url = Html::a('<i class="fa fa-remove"></i>', ['fees-due/remove-assigned-class', 'cid' => $cid, 'id' => $id]);
                        echo "<tr onclick=\"
                $.urlParam = function(name){
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    if (results==null){
                       return null;
                    }
                    else{
                       return results[1] || 0;
                    }
                }
                var urlval = $.urlParam('id');
                $('.tr_active').removeClass('tr_active');
                $(this).addClass('tr_active');
                $.get( '../fees-due/lists?clid=" . $v['cid'] . "&fid='+urlval, function( data ) {
                $( 'div#exempt' ).html(data);
                $(this).removeAttr('style');
            });\"
        >";
                        echo "<td class='text-center'>" . $v['cdes'] . "</td>";
                        echo "<td class='text-center'> " . $url . "</td>";

                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td class='text-center'>No Class Assigned this fee</td>";
                    echo "</tr>";

                }
                echo "</table>";
                ?>

                <div><?= LinkPager::widget(['pagination' => $cl_pages]); ?></div>
            </div>

            <?php Pjax::begin(); ?>
            <div id="exempt" class="col-md-9 table-responsive " style="padding:0 0 0 15px;">
                <?php
                echo "<table id='exempted' class ='table-bordered table table-striped'>";
                echo "<tr>";
                echo "<th class='text-center'> Sr.No </th>";
                echo "<th class='text-center'> Payment code </th>";
                echo "<th class='text-center'> Class </th>";
                echo "<th class='text-center'> Exempted Students </th>";
                echo "<th class='text-center'> Remove</th>";
                echo "</tr>";
                if (!empty($student_exemp)) {
                    $i = 1;
                    if (isset($_GET['page'])) {
                        $i = (($_GET['page'] - 1) * 20) + 1;
                    }
                    foreach ($student_exemp as $k => $v) {
                        $stdId = $v['sn'];
                        $link = Html::a('<i class="fa fa-remove"></i>', ['fees-due/remove-std-exmpt', 'stdId' => $stdId, 'id' => $id]);
                        echo "<tr>";
                        echo "<td class='text-center'>" . $i . "</td>";
                        echo "<td class='text-center'>" . $v['sn'] . "</td>";
                        echo "<td class='text-center'>" . $v['cl'] . "</td>";
                        echo "<td class='text-center'>" . $v['fn'] . " " . $v['mn'] . " " . $v['ln'] . "</td>";
                        echo "<td class='text-center'> " . $link . "</td>";
                        echo "</tr>";
                        $i++;
                    }
                } else {
                    echo "<tr>";
                    echo "<td class='text-center'></td>";
                    echo "<td class='text-center'>No exempted students for this fee</td>";
                    echo "<td class='text-center'></td>";
                    echo "<td class='text-center'></td>";
                    echo "</tr>";

                }
                echo "</table>";
                ?>
                <div><?= LinkPager::widget(['pagination' => $st_pages]); ?></div>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>


    <div class="col-md-8 ">
        <h3>Consessions</h3>
    </div>
    <div id="exempt" class="col-md-12 table-responsive " style="padding:0 0 0 15px;">
        <?php
        echo "<table id='exempted' class ='table-bordered table table-striped'>";
        echo "<tr>";
        echo "<th class='text-center'> Sr.No </th>";

        echo "<th class='text-center'> Concession </th>";
        echo "<th class='text-center'> Student code </th>";
        echo "<th class='text-center'> Class Code </th>";
        echo "<th class='text-center'> Student Name </th>";
        echo "</tr>";
        if (!empty($concessions)) {
            $i = 1;
            if (isset($_GET['page'])) {
                $i = (($_GET['page'] - 1) * 20) + 1;
            }
            foreach ($concessions as $k => $v) {
                Yii::trace($v);
                echo "<tr>";
                echo "<td class='text-center'>" . $i . "</td>";
                echo "<td class='text-center'>" . $v['percentage'] . "</td>";
                echo "<td class='text-center'>" . $v['student_code'] . "</td>";
                echo "<td class='text-center'>" . $v['class_code'] . "</td>";
                echo "<td class='text-center'>" . $v['first_name'] . " " . $v['last_name'] . "</td>";
                echo "</tr>";
                $i++;
            }
        } else {
            echo "<tr>";
            echo "<td class='text-center'></td>";
            echo "<td class='text-center'>No  students consession for this fee</td>";
            echo "<td class='text-center'></td>";
            echo "<td class='text-center'></td>";
            echo "</tr>";

        }
        echo "</table>";
        ?>
        <div><?= LinkPager::widget(['pagination' => $st_pages]); ?></div>
    </div>


    <?php if (\Yii::$app->user->can('automatic_alerting')) : ?>


        <?php if ($model->enable_reminders) : ?>

            <div class="col-md-8 ">
                <h3>Sms Reminders</h3>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                    <div class="col-lg-6 col-xs-6 profile-label">SMS enabled</div>
                    <div class="col-lg-6 col-xs-6 profile-text"><?= $model->enable_reminders ? 'Yes' : 'No' ?></div>
                </div>

                <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                    <div class="col-lg-6 col-xs-6 profile-label">Days for Msg to be sent after effective date</div>
                    <div class="col-lg-6 col-xs-6 profile-text"><?= $model->send_msg_after_effective_date_in_days ? $model->send_msg_after_effective_date_in_days : '-' ?></div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                    <div class="col-lg-6 col-xs-6 profile-label">Reminder Frequency</div>
                    <div class="col-lg-6 col-xs-6 profile-text"><?= $model->reminder_frequency_in_days ? $model->reminder_frequency_in_days : '-' ?></div>
                </div>

                <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                    <div class="col-lg-6 col-xs-6 profile-label">SMS Stop Sending after</div>
                    <div class="col-lg-6 col-xs-6 profile-text"><?= $model->stop_sending_reminders ? $model->stop_sending_reminders : '-' ?></div>
                </div>
            </div>


        <?php endif; ?>
    <?php endif; ?>

    <div class='col-md-12'>
        <div class='pull-right'>

            <?php //if(!$admin) : ?>
            <?php if ($model->ready_for_approval == false && ($model->created_by == Yii::$app->user->identity->id || $admin)) : ?>
                <?= Html::a('Submit For Approval', ['ready-toapprove', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-block',
                    'data' => [
                        'confirm' => 'Are you sure you want to Submit  this fee for approval?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php elseif (($model->approval_status == false) && ($model->ready_for_approval == true) && ($model->created_by != \Yii::$app->user->identity->id || $admin) && !\Yii::$app->user->can('is_school_operator')) : ?>
                <?= Html::a('Approve', ['approve', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-block',
                    'data' => [
                        'confirm' => 'Are you sure you want to Approve  this fee ?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
            <?php //endif; ?>
        </div>
    </div>
    <div style="clear: both"></div>

<?php
$script = <<< JS
$("document").ready(function(){ 
   $.urlParam = function(name){
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    if (results==null){
                       return null;
                    }
                    else{
                       return results[1] || 0;
                    }
                }
                var urlval = $.urlParam('id');
    $('#approvalReady').click(function(){
      $.post( '../fees-due/ready-toapprove?id='+urlval, function() {
                  $( '#approvalReady' ).prop("disabled",true);
    });
  });
});
JS;
$this->registerJs($script);
?>

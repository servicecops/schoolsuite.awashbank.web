<?php
use yii\helpers\Url;
?>
<script type="text/javascript">
<?php $this->beginBlock('JS_END') ?>
    yii.process = (function ($) {
        var _onSearch = false;
        var pub = {
            roleSearch: function () {
                if (!_onSearch) {
                    _onSearch = true;
                    var $th = $(this);
                    setTimeout(function () {
                        _onSearch = false;
                        var data = {
                            target:$th.data('target'),
                            term: $th.val(),
                        };
                        var target = '#' + $th.data('target');

                        $.get((target =='#available' || target=='#assigned') ? '<?= Url::toRoute(['role-search']) ?>' : '<?= Url::toRoute(['stu-search']) ?>', data,
                            function (html) {
                                $(target).html(html);
                            });
                    }, 500);
                }
            },
            action: function () {
                var action = $(this).data('action');
                if(action == 'assign' || action == 'remove'){
                    var params = $((action == 'assign' ? '#available' : '#assigned') + ', .role-search').serialize();
                    var urlAssign = '<?= Url::toRoute(['assign', 'action'=>'assign']) ?>';
                    var urlRemove = '<?= Url::toRoute(['assign', 'action'=>'remove']) ?>';
                    $.post(action=='assign' ? urlAssign : urlRemove,
                        params, function (r) {
                            $('#available').html(r[0]);
                            $('#assigned').html(r[1]);
                        });
                }
                else if(action == 'stuassign' || action == 'sturemove'){
                    var params = $((action == 'stuassign' ? '#stu_available' : '#stu_assigned') + ', .stu-search').serialize();
                    var urlAssign = '<?= Url::toRoute(['stuassign', 'action'=>'stuassign']) ?>';
                    var urlRemove = '<?= Url::toRoute(['stuassign', 'action'=>'sturemove']) ?>';
                    $.post(action=='stuassign' ? urlAssign : urlRemove,
                        params, function (r) {
                            $('#stu_available').html(r[0]);
                            $('#stu_assigned').html(r[1]);
                        });

                }
                
                return false;
            }
        }

        return pub;
    })(window.jQuery);
<?php $this->endBlock(); ?>

<?php $this->beginBlock('JS_READY') ?>
    $('.role-search').keydown(yii.process.roleSearch);
    $('a[data-action]').click(yii.process.action);
<?php $this->endBlock(); ?>
</script>
<?php
yii\web\YiiAsset::register($this);
$this->registerJs($this->blocks['JS_END'], yii\web\View::POS_END);
$this->registerJs($this->blocks['JS_READY'], yii\web\View::POS_READY);
?>

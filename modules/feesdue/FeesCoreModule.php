<?php
namespace app\modules\feesdue;

class FeesCoreModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\feesdue\controllers';
    public function init() {
        parent::init();
    }
}

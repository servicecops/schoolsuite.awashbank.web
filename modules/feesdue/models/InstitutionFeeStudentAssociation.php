<?php

namespace app\modules\feesdue\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;

/**
 * This is the model class for table "institution_fee_student_association".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $fee_id
 * @property integer $student_id
 * @property boolean $applied
 * @property string $created_by
 * @property string $date_applied
 * @property string $date_modified
 *
 * @property InstitutionFeesDue $fee
 * @property StudentInformation $student
 * @property StudentAccountTransactionHistory[] $studentAccountTransactionHistories
 * @property boolean $has_penalty
 * @property boolean $penalties_closed
 * @property integer $penalty_id
 * @property string $fee_outstanding_balance
 * @property string $last_penalty_apply_date
 * @property string $next_penalty_apply_date
 * @property string $penalties_outstanding_balance
 * @property string $penalties_accrued
 * @property integer $penalty_application_count
 *
 */
class InstitutionFeeStudentAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_fee_student_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_applied', 'date_modified', 'fee_outstanding_balance' ,'penalties_accrued', 'penalty_application_count'], 'safe'],
            [['fee_id', 'student_id'], 'required'],
            [['fee_id', 'student_id'], 'integer'],
            [['applied', 'has_penalty', 'penalties_closed'], 'boolean'],
            [['created_by'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'fee_id' => 'Fee ID',
            'student_id' => 'Student ID',
            'applied' => 'Applied',
            'created_by' => 'Created By',
            'date_applied' => 'Date Applied',
            'date_modified' => 'Date Modified',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeDue()
    {
        return $this->hasOne(FeesDue::className(), ['id' => 'fee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAccountTransactionHistories()
    {
        return $this->hasMany(StudentAccountTransactionHistory::className(), ['fee_association_id' => 'id']);
    }
}

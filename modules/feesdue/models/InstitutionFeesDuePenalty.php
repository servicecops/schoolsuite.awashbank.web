<?php

namespace app\modules\feesdue\models;

use Yii;
use yii\db\ActiveRecord;
use yii\validators\EmailValidator;

/**
 * This is the model class for table "institution_fees_due_penalty".
 *
 * @property integer $id
 * @property string $date_created
 * @property string $penalty_type
 * @property string $penalty_frequency
 * @property string $penalty_apply_frequency_unit
 * @property integer $penalty_apply_frequency_term
 * @property string $penalty_amount
 * @property integer $school_id
 * @property string $penalty_expiry_date
 * @property string $maximum_penalty_amount
 * @property integer $maximum_penalty_application_count
 */
class InstitutionFeesDuePenalty extends ActiveRecord
{


    public $variable_amount;
    public function init()
    {
        $this->maximum_penalty_amount = '1000.00';
        $this->maximum_penalty_application_count = 0;
        $this->penalty_expiry_date = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_fees_due_penalty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'penalty_expiry_date'], 'safe'],
            [['school_id','penalty_expiry_date','maximum_penalty_application_count','maximum_penalty_amount'], 'required'],
            [['penalty_apply_frequency_term', 'school_id', 'maximum_penalty_application_count'], 'integer'],
            [['penalty_amount', 'maximum_penalty_amount'], 'number'],
            [['penalty_type', 'penalty_frequency'], 'string', 'max' => 255],
            [['penalty_apply_frequency_unit'], 'string', 'max' => 1],
            [['variable_amount'], 'validateVariableAmounts', 'skipOnEmpty' => true, 'skipOnError' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'penalty_type' => 'Penalty Type',
            'penalty_frequency' => 'Penalty Frequency',
            'penalty_apply_frequency_unit' => 'Penalty Apply Frequency Unit',
            'penalty_apply_frequency_term' => 'Penalty Apply Frequency Term',
            'penalty_amount' => 'Penalty Amount',
            'school_id' => 'School ID',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Institution Fees Due Penalty',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'penalty_type' => ['label'=>'Penalty Type'],
                    'penalty_frequency' => ['label'=>'Penalty Frequency'],
                    'penalty_apply_frequency_unit' => ['label'=>'Penalty Apply Frequency Unit'],
                    'penalty_apply_frequency_term' => ['type'=>'number', 'label'=>'Penalty Apply Frequency Term'],
                    'penalty_amount' => ['type'=>'number', 'label'=>'Penalty Amount'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'date_created'],
            ['attribute'=>'penalty_type'],
            ['attribute'=>'penalty_frequency'],
            ['attribute'=>'penalty_apply_frequency_unit'],
            ['attribute'=>'penalty_apply_frequency_term'],
            ['attribute'=>'penalty_amount'],
            ['attribute'=>'school_id'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Institution Fees Due Penalty" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

    public function validateVariableAmounts($attribute, $params)
    {
        if($this->penalty_type != 'VARYING_PERCENTAGE' && $this->penalty_type != 'VARYING_AMOUNT') {
            return;
        }
        $items = $this->$attribute;

        if (!is_array($items)) {
            $items = [];
        }

        foreach ($items as $index => $item) {
            $error = null;
            $key = $attribute . '[' . $index . ']';
            if(!is_numeric($item)) {
                $this->addError($key, 'Penalty amount should be numeric');
                continue;
            }

            if($items[$index]<=0) {
                $this->addError($key, 'Penalty amount should greater than zero');
                continue;
            }

            //Percentage penalties cannot exceed 100
            if($this->penalty_type == 'VARYING_PERCENTAGE' && $items[$index]>100) {
                $this->addError($key, 'Penalty percentage cannot exceed 100');
                continue;
            }

        }
    }

}

<?php


namespace app\modules\feesdue\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * FeesDueSearch represents the model behind the search form about `app\models\FeesDue`.
 */
class FeesDueSearch extends FeesDue
{

    public $modelSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'due_amount', 'priority', 'created_by'], 'integer'],
            [['date_created', 'effective_date', 'end_date', 'school_id', 'description','modelSearch'], 'safe'],
            [['delete_flag'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['fd.id', 'school_name', 'description', 'due_amount', 'effective_date', 'end_date', 'approval_status', 'fd.created_by', 'ready_for_approval', 'recurrent', 'next_apply_date'])
            ->from('institution_fees_due fd')
            ->innerJoin('core_school sch', 'sch.id=fd.school_id')
            ->andFilterWhere(['fd.school_id' => $this->school_id])
            ->andWhere(['>=', 'fd.end_date', Date('Y-m-d')]);
        if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['fd.school_id' => Yii::$app->user->identity->school_id]);
        }
        $query->andFilterWhere(['ilike', 'description', $this->description]);
        //Children of recurrent fees are internal so should not be displayed on web
        $query->andWhere(['fd.child_of_recurrent' => false]);

        if(Yii::$app->user->can('view_by_branch')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'school_name', 'description', 'due_amount', 'effective_date', 'end_date', 'approve_status'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('fd.date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

    public function searchStudents($id,$params)
    {
        $this->load($params);

        Yii::trace("stds here1");


        $assignedClasses = (new Query())
            ->select(['class_id'])
            ->from('institution_fee_class_association')
            ->where(['fee_id' => $id])
            ->all();
        Yii::trace($assignedClasses);

        $allClsStds = (new Query())
            ->select(['cm.id', 'cm.student_code', 'cm.first_name','cm.middle_name','cm.last_name', 'cm.gender','csc.class_code',  "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name"])
            ->from('core_student cm')
            ->innerJoin('core_school_class csc','csc.id =cm.class_id' )

            ->andWhere(['in', 'class_id', $assignedClasses]);

        Yii::trace($this->modelSearch);
        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            if (preg_match("#^[0-9]{10}$#", $this->modelSearch)) {
                $allClsStds->andWhere(['student_code' => $this->modelSearch]);
                $allClsStds->orWhere(['schoolpay_paymentcode' => $this->modelSearch]);

            } else if (strpos($this->modelSearch, '-') !== false || strpos($this->modelSearch, '/') !== false) {
                //Registration numbers usually have a dash - or slass /
                $allClsStds->andWhere(['school_student_registration_number' => trim(strtoupper($this->modelSearch))]);
            } else {
                $allClsStds = $this->nameLogic($allClsStds, $this->modelSearch);
            }
        }
        Yii::trace($allClsStds);


        $pages = new Pagination(['totalCount' => $allClsStds->count()]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $allClsStds->orderBy($sort->orders) : $allClsStds->orderBy('cm.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $allClsStds;
        $allClsStds->offset($pages->offset)->limit($pages->limit);
        return ['query' => $allClsStds->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }


    private function nameLogic($query, $seachName)
    {
        $search_words = preg_split("/[\s,]+/", $seachName);
        $search_words = array_filter($search_words);
        $words = array();
        if (count($search_words) > 1) {
            foreach ($search_words as $word) {
                $words[] = $word;
            }
            if (isset($words[0])) {
                $name = str_replace(' ', '', $words[0]);
                $query->andFilterWhere(['or',
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }
            if (isset($words[1])) {
                $name = str_replace(' ', '', $words[1]);
                $query->andFilterWhere(['or',
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }
            if (isset($words[2])) {
                $name = str_replace(' ', '', $words[2]);
                $query->andFilterWhere(['or',
                    ['ilike', 'first_name', $name],
                    ['ilike', 'middle_name', $name],
                    ['ilike', 'last_name', $name]
                ]);
            }

        } else {
            $name = str_replace(' ', '', $seachName);
            $query->andFilterWhere(['or',
                ['ilike', 'first_name', $name],
                ['ilike', 'middle_name', $name],
                ['ilike', 'last_name', $name],

            ]);
        }
        return $query;

    }


    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'FeesDue',
            'title' => 'Fees Datasheet',
            'exportFile' => '/fees-due/exportPdfExcel',
        ];

        return $data;
    }
}

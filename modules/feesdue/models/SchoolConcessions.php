<?php

namespace app\modules\feesdue\models;

use Yii;

/**
 * This is the model class for table "school_types".
 *
 * @property integer $id
 * @property integer $fee_id
 * @property integer $student_id
 * @property integer $concessions
 * @property integer $school_id
 *
 * @property SchoolInformation[] $schoolInformations
 */
class SchoolConcessions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fee_concessions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['fee_id','percentage','school_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_type_name' => 'School Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolInformations()
    {
        return $this->hasMany(SchoolInformation::className(), ['school_type' => 'id']);
    }
}

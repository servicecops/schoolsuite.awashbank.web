<?php

namespace app\modules\feesdue\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "institution_fees_due".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $school_id
 * @property integer $due_amount
 * @property string $effective_date
 * @property string $end_date
 * @property string $description
 * @property boolean $delete_flag
 * @property string $created_by
 *
 * @property InstitutionFeeClassAssociation[] $institutionFeeClassAssociations
 * @property SchoolInformation $school
 * @property boolean $recurrent
 * @property boolean $child_of_recurrent
 * @property string $next_apply_date
 * @property string $last_apply_date
 * @property integer $apply_frequency
 * @property string $apply_frequency_unit
 * @property integer $parent_fee_id
 */
class InstitutionFeesDue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_fees_due';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'effective_date', 'end_date', 'child_of_recurrent', 'recurrent', 'apply_frequency', 'apply_frequency_unit'], 'safe'],
            [['school_id', 'due_amount', 'end_date', 'description', 'recurrent'], 'required'],
            [['school_id', 'due_amount', 'apply_frequency'], 'integer'],
            [['description', 'created_by', 'apply_frequency_unit'], 'string'],
            [['delete_flag', 'child_of_recurrent', 'recurrent'], 'boolean'],
            [['apply_frequency'], 'validateRecurrentFieldss', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'school_id' => 'School ID',
            'due_amount' => 'Due Amount',
            'effective_date' => 'Effective Date',
            'end_date' => 'End Date',
            'description' => 'Description',
            'delete_flag' => 'Delete Flag',
            'created_by' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionFeeClassAssociations()
    {
        return $this->hasMany(InstitutionFeeClassAssociation::className(), ['fee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(SchoolInformation::className(), ['id' => 'school_id']);
    }


}

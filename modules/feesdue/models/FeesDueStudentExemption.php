<?php

namespace app\modules\feesdue\models;

use Yii;

/**
 * This is the model class for table "fees_due_student_exemption".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $fee_id
 * @property string $student_number
 */
class FeesDueStudentExemption extends \yii\db\ActiveRecord
{
    public $selected_student;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fees_due_student_exemption';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['fee_id'], 'required'],
            [['fee_id'], 'integer'],
            [['student_number'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'fee_id' => 'Fee ID',
            'student_number' => 'Student Number',
        ];
    }
}

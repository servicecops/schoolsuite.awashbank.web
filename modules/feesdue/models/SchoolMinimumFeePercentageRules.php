<?php

namespace app\modules\feesdue\models;

use Yii;

/**
 * This is the model class for table "school_minimum_fee_percentage_rules".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $school_id
 * @property integer $minimum_percentage
 * @property boolean $active
 */
class SchoolMinimumFeePercentageRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_minimum_fee_percentage_rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['school_id', 'minimum_percentage'], 'required'],
            [['school_id', 'minimum_percentage'], 'integer'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'school_id' => 'School ID',
            'minimum_percentage' => 'Minimum Percentage',
            'active' => 'Active',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New School Minimum Fee Percentage Rules',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                    'minimum_percentage' => ['type'=>'number', 'label'=>'Minimum Percentage'],
                    'active' => ['type'=>'number', 'label'=>'Active'],
                ]
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'date_created'],
            ['attribute'=>'school_id'],
            ['attribute'=>'minimum_percentage'],
            ['attribute'=>'active'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "School Minimum Fee Percentage Rules" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

<?php

namespace app\modules\feesdue\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class FeeClassSearch extends FeeClass
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fee_id', 'class_id'], 'integer'],
            [['date_created', 'created_by'], 'safe'],
            [['applied'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FeeClass::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'fee_id' => $this->fee_id,
            'class_id' => $this->class_id,
            'applied' => $this->applied,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}

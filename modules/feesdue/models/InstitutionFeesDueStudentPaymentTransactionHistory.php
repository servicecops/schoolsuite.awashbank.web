<?php

namespace app\modules\feesdue\models;

use Yii;

/**
 * This is the model class for table "institution_fees_due_student_payment_transaction_history".
 * @property integer $id
 * @property string $date_created
 * @property integer $payment_id
 * @property integer $fee_id
 * @property integer $fee_student_association_id
 * @property integer $student_id
 * @property string $amount
 * @property string $balance_before
 * @property string $balance_after
 * @property string $description
 * @property string $penalty_amount
 * @property string $penalty_balance_before
 * @property string $penalty_balance_after
 */
class InstitutionFeesDueStudentPaymentTransactionHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_fees_due_student_payment_transaction_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['payment_id', 'fee_id', 'fee_student_association_id', 'student_id', 'amount', 'balance_before', 'balance_after'], 'required'],
            [['payment_id', 'fee_id', 'fee_student_association_id', 'student_id'], 'integer'],
            [['amount', 'balance_before', 'balance_after'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'payment_id' => 'Payment ID',
            'fee_id' => 'Fee ID',
            'fee_student_association_id' => 'Fee Student Association ID',
            'student_id' => 'Student ID',
            'amount' => 'Amount',
            'balance_before' => 'Balance Before',
            'balance_after' => 'Balance After',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Institution Fees Due Student Payment Transaction History',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'payment_id' => ['type'=>'number', 'label'=>'Payment Id'],
                    'fee_id' => ['type'=>'number', 'label'=>'Fee Id'],
                    'fee_student_association_id' => ['type'=>'number', 'label'=>'Fee Student Association Id'],
                    'student_id' => ['type'=>'number', 'label'=>'Student Id'],
                    'amount' => ['type'=>'number', 'label'=>'Amount'],
                    'balance_before' => ['type'=>'number', 'label'=>'Balance Before'],
                    'balance_after' => ['type'=>'number', 'label'=>'Balance After'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'date_created'],
            ['attribute'=>'payment_id'],
            ['attribute'=>'fee_id'],
            ['attribute'=>'fee_student_association_id'],
            ['attribute'=>'student_id'],
            ['attribute'=>'amount'],
            ['attribute'=>'balance_before'],
            ['attribute'=>'balance_after'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Institution Fees Due Student Payment Transaction History" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

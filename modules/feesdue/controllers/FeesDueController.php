<?php

namespace app\modules\feesdue\controllers;


use app\modules\feesdue\models\FeeClass;
use app\modules\feesdue\models\FeesDue;
use app\modules\feesdue\models\FeesDueSearch;
use app\modules\feesdue\models\FeesDueStudentExemption;
use app\modules\feesdue\models\InstitutionFeesDuePenalty;
use app\modules\feesdue\models\SchoolConcessions;
use app\modules\feesdue\models\SchoolMinimumFeePercentageRules;
use app\modules\feesdue\models\StudentConcessions;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\workflow\controllers\WorkflowController;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\base\UserException;
use yii\data\Pagination;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

//use app\models\FeesDue;
//use app\models\FeeClass;
//use app\models\Classes;
//use app\models\FeesDueSearch;
//use app\modules\logs\models\Logs;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class FeesDueController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FeesDue models.
     * @return mixed
     */

    public function actionIndex()
    {
        if (Yii::$app->user->can('r_fd')) {
            $request = Yii::$app->request;
            $searchModel = new FeesDueSearch();
            $data = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            $res = ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']];
            return ($request->isAjax) ? $this->renderAjax('index', $res) :
                $this->render('index', $res);
        } else {
            throw new ForbiddenHttpException('No permissions to view FEES.');
        }


    }

    /**
     * Displays a single FeesDue model.
     * @param integer $id
     * @return mixed
     */

//    public function actionView($id)
//    {
//        $model = $this->findModel($id);
//        $user_sch = $model->school_id;
//
//        Yii::trace($user_sch);
//        if (Yii::$app->user->can('rw_fd') && ($model->school_id == $user_sch || Yii::$app->user->can('schoolpay_admin'))) {
//            $request = Yii::$app->request;
//            $feeClass = new FeeClass();
//            $feeModel = new FeeClass();
//            $exemptedModel = new FeesDueStudentExemption();
//            $exemptions = new FeesDueStudentExemption();
//            $cls = (new Query())
//                ->select(['class_id'])
//                ->from('institution_fee_class_association')
//                ->where(['fee_id' => $id]);
//
//            $stuExmp = (new Query())
//                ->select(['student_number'])
//                ->from('fees_due_student_exemption')
//                ->where(['fee_id' => $id]);
//            //$stuExmp = array_filter(array_values($stuExmp));
//
//            $av_classes = CoreSchoolClass::find()->where(['school_id' => $user_sch]);
//            if ($feeClass->load(Yii::$app->request->post())) {
//
//                Yii::trace($av_classes);
//                // print_r($av_classes);
//                $data = Yii::$app->request->post();
//                Yii::trace($data);
//                Yii::trace($data['FeeClass']['class_id']);
//                if(!$data['FeeClass']['class_id']){
//                    throw new UserException("No classes submitted, Please click on skip page to continue");
//                }
////        if ($feeClass->load($data)) {
//                $feeClassModel = new FeeClass();
//                $feeClassModel->class_id = $data['FeeClass']['class_id'];
//
//
////            $feeClas =array();
//                $feeClas = $feeClassModel->class_id;
////            print_r($feeClas);
////
//
//                //       $clses = Json::decode($data['FeeClass']['class_id']);
//                $clses = array_unique($feeClas);
//                if (!empty($clses)) {
//                    $del_class = [];
//                    if ($cls->count() > 0) {
//                        $fm = $clses;
//                        $sv_cl = array_column($cls->all(), 'class_id');
//                        $clses = array_diff($clses, $sv_cl);
//                        $del_class = array_diff($sv_cl, $fm);
//                    }
//                    if (!empty(array_filter($clses))) {
//                        foreach ($clses as $cls) {
//                            $f_class = new FeeClass();
//                            $f_class->class_id = $cls;
//                            $f_class->fee_id = $id;
//                            $f_class->created_by = Yii::$app->user->identity->username;
//                            $f_class->save();
//                        }
//                    }
//                    if (!empty(array_filter($del_class))) {
//                        foreach ($del_class as $cls) {
//                            $fclass = FeeClass::find()->where(['class_id' => $cls])->limit(1)->one();
//                            $fclass->delete();
//                        }
//                    }
//                    $res = ['id' => $id];
//                    //   $feeClassModel->save(false);
//                    return $this->redirect(['assign-stds', 'id' => $model->id]);
//                }
//            }
//
//            //Load penalty too
//            $penaltyModel = $model->has_penalty ? $this->getFeesDuePenalty($model) : null;
//            $request = Yii::$app->request;
//            $classes = Yii::$app->db->createCommand('SELECT * FROM core_school_class WHERE school_id =:id ')
//                ->bindValue(':id', $user_sch)
//                ->queryAll();
//
//            $available = array();
//            $assigned = array();
//            //$classes = CoreSchoolClass::find()->where(['type'=>2])->all();
//
//
//            foreach ($classes as $class) {
//                if (!isset($assigned[$class['id']])) {
//                    $available[$class['id']] = $class['class_description'];
//                }
//            }
//            $stdExemptionModel = new FeesDueStudentExemption();
//
//            $res = ['model' => $model, 'feeModel' => $feeModel, 'exemptedModel' => $exemptedModel, 'stdExemptionModel' => $stdExemptionModel
//                , 'feeClass' => $feeClass, 'exemptions' => $exemptions, 'schoolId' => $user_sch,
//                'av_classes' => $av_classes, 'cls' => $cls->all(), 'penaltyModel' => $penaltyModel, 'available' => $available, 'assigned' => $assigned, 'classes' => $classes];
//            return ($request->isAjax) ? $this->renderAjax('view', $res) :
//                $this->render('view', $res);
//
//        } else {
//            throw new ForbiddenHttpException('No permissions to view FEES.');
//        }
//
//    }

    public function actionCredithrFee($id){
        $fee = FeesDue::findOne(['id'=>$id]);

        Yii::trace("credit hr ".$fee->credit_hour);
        if($fee->credit_hour){
            return true;
        }else{
            return false;
        }


    }


    public function actionView($id)
    {
        $model = $this->findModel($id);
        $user_sch = $model->school_id;

        Yii::trace($user_sch);
        if (Yii::$app->user->can('rw_fd') && ($model->school_id == $user_sch || Yii::$app->user->can('schoolpay_admin'))) {
            $request = Yii::$app->request;
            $feeClass = new FeeClass();
            $feeModel = new FeeClass();
            $searchModel = new FeesDue();

            $exemptedModel = new FeesDueStudentExemption();
            $exemptions = new FeesDueStudentExemption();
            $cls = (new Query())
                ->select(['class_id'])
                ->from('institution_fee_class_association')
                ->where(['fee_id' => $id]);

            $stuExmp = (new Query())
                ->select(['student_number'])
                ->from('fees_due_student_exemption')
                ->where(['fee_id' => $id]);
            //$stuExmp = array_filter(array_values($stuExmp));

            $av_classes = CoreSchoolClass::find()->where(['school_id' => $user_sch]);
            if ($feeClass->load(Yii::$app->request->post())) {

                Yii::trace($av_classes);
                // print_r($av_classes);
                $data = Yii::$app->request->post();
                Yii::trace($data);
                //Yii::trace($data['FeeClass']['class_id']);


                $data = Yii::$app->request->post();


                if (isset($data['FeeClass']['concessions']) && $data['FeeClass']['concessions']) {
                    $percentages = $data['FeeClass']['concessions'];
                }

                if ($percentages) {
                    if (is_array($percentages) && $percentages) {

                        foreach ($percentages as $k => $v) {
                            $codes = preg_split("/[\s,]+/", $v['payment_codes']);


//                        $codes = preg_split("/\r\n|\n|\r/", $v['student_codes']);
//                        Yii::trace($codes);
                            if ($codes) {
                                foreach ($codes as $code) {
                                    Yii::trace($code);

                                    if (preg_match('/^\d{10}$/', trim($code))) {

                                        $std = CoreStudent::findOne(['student_code' => $code,'school_id'=>$model->school_id]);

                                        if($std) {

                                            $feeDetails = FeesDue::findOne([$id]);
                                            //check if student has existing concession on this fee

                                            $existingStdConcession = StudentConcessions::findOne(['fee_id' => $id, 'school_id' => $user_sch, 'student_id' => $std->id]);

                                            if ($existingStdConcession) {
                                                \Yii::$app->session->setFlash('fee', "<div class='alert alert-danger'>Concession has not been applied, Student $code has an existing discount for this fee<br> </div>");
                                                return $this->redirect(['view', 'id' => $id]);

                                            }
                                            $existingConcession = SchoolConcessions::findOne(['fee_id' => $id, 'school_id' => $user_sch, 'percentage' => $v['percentage']]);
                                            $concessions = new StudentConcessions();


                                            if ($existingConcession) {

                                                //create new school percentage

                                                $concessions->fee_id = $id;
                                                $concessions->student_id = $std->id;
                                                $concessions->concessions_id = $existingConcession->id;
                                                $concessions->school_id = $user_sch;
                                                $concessions->save();
                                            } else {

                                                $newConcession = new SchoolConcessions();

                                                $newConcession->fee_id = $id;
                                                $newConcession->percentage = $v['percentage'];
                                                $newConcession->school_id = $user_sch;
                                                $newConcession->save();

                                                //save student concession
                                                $concessions->fee_id = $id;
                                                $concessions->student_id = $std->id;
                                                $concessions->concessions_id = $newConcession->id;
                                                $concessions->school_id = $user_sch;
                                                $concessions->save();


                                            }
                                        }


                                    }
                                }
                            }
                        }
                    }
                }
                if (isset($data['FeesDue']) && $data['FeesDue']) {
                    $smsReminders = $data['FeesDue'];

                }
                if (isset($smsReminders)) {

                    //update fees due table with sms parameters

                    if (isset($smsReminders['send_msg_after_effective_date_in_days']) && $smsReminders['send_msg_after_effective_date_in_days']) {
                        $send_after = $smsReminders['send_msg_after_effective_date_in_days'];
                    } else {
                        $send_after = $model->send_msg_after_effective_date_in_days;
                    }

                    if (isset($smsReminders['reminder_frequency_in_days']) && $smsReminders['reminder_frequency_in_days']) {
                        $reminder_frequency = $smsReminders['reminder_frequency_in_days'];
                    } else {
                        $reminder_frequency = $model->reminder_frequency_in_days;
                    }
                    if (isset($smsReminders['stop_sending_reminders']) && $smsReminders['stop_sending_reminders']) {
                        $stop_after = $smsReminders['stop_sending_reminders'];
                    } else {
                        $stop_after = $model->stop_sending_reminders;
                    }
                    if (isset($smsReminders['message_template']) && $smsReminders['message_template']) {
                        $msg_template = $smsReminders['message_template'];
                    } else {

                        $msg_template = $model->message_template;
                    }


                    $model->send_msg_after_effective_date_in_days = $send_after;
                    $model->reminder_frequency_in_days = $reminder_frequency;
                    $model->stop_sending_reminders = $stop_after;
                    $model->message_template = $msg_template;
                    $model->sent_reminders = 0;

                    $model->save();


                }

                if (isset($data['FeeClass']['class_id'])) {
//                    throw new UserException("No classes submitted, Please click on skip page to continue");
//
//        if ($feeClass->load($data)) {
                    $feeClassModel = new FeeClass();
                    $feeClassModel->class_id = $data['FeeClass']['class_id'];
                    $credithrs = $data['FeeClass']['credit_hours'];

                    if(!$credithrs){
                        $credithrs =1;
                    }

//            $feeClas =array();
                    $feeClas = $feeClassModel->class_id;
                    Yii::trace($feeClas);
//
//
                    if (!empty($feeClas)) {
                        //       $clses = Json::decode($data['FeeClass']['class_id']);
                        $clses = array_unique($feeClas);
                        if (!empty($clses)) {
                            $del_class = [];
                            if ($cls->count() > 0) {
                                $fm = $clses;
                                $sv_cl = array_column($cls->all(), 'class_id');
                                $clses = array_diff($clses, $sv_cl);
                                $del_class = array_diff($sv_cl, $fm);
                            }
                            if (!empty(array_filter($clses))) {
                                foreach ($clses as $cls) {
                                    $f_class = new FeeClass();
                                    $f_class->class_id = $cls;
                                    $f_class->fee_id = $id;
                                    $f_class->credit_hours = $credithrs;
                                    $f_class->created_by = Yii::$app->user->identity->username;

                                    $f_class->save();
                                }
                            }
                            if (!empty(array_filter($del_class))) {
                                foreach ($del_class as $cls) {
                                    $fclass = FeeClass::find()->where(['class_id' => $cls])->limit(1)->one();
                                    $fclass->delete();
                                }
                            }
                            $res = ['id' => $id];
                            //   $feeClassModel->save(false);
                            return $this->redirect(['assign-stds', 'id' => $model->id]);
                        }
                    }
                }


               // $transaction->commit();

                return $this->redirect(['details', 'id'=>$id]);
//
                //return Yii::$app->runAction('/fees-due/details', ['id' => $id]);
            }

            //Load penalty too
            $penaltyModel = $model->has_penalty ? $this->getFeesDuePenalty($model) : null;
            $request = Yii::$app->request;
            $classes = Yii::$app->db->createCommand('SELECT * FROM core_school_class WHERE school_id =:id ')
                ->bindValue(':id', $user_sch)
                ->queryAll();

            $available = array();
            $assigned = array();
            //$classes = CoreSchoolClass::find()->where(['type'=>2])->all();


            foreach ($classes as $class) {
                if (!isset($assigned[$class['id']])) {
                    $available[$class['id']] = $class['class_description'];
                }
            }
            $stdExemptionModel = new FeesDueStudentExemption();


            $res = ['model' => $model, 'feeModel' => $feeModel, 'exemptedModel' => $exemptedModel, 'stdExemptionModel' => $stdExemptionModel
                , 'feeClass' => $feeClass, 'exemptions' => $exemptions, 'schoolId' => $user_sch,'searchModel'=>$searchModel,
                'av_classes' => $av_classes, 'cls' => $cls->all(), 'penaltyModel' => $penaltyModel, 'available' => $available, 'assigned' => $assigned, 'classes' => $classes];
            return ($request->isAjax) ? $this->renderAjax('view', $res) :
                $this->render('view', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to view FEES.');
        }

    }




    //get class names

    /**
     * Finds the FeesDue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FeesDue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = null;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $model = FeesDue::findOne(['id' => $id, 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $model = FeesDue::findOne($id);
        }
        if ($model !== null) {
            Yii::trace($model->recurrent);
            if (!$model->recurrent) {
                $model->recurrent = '0'; //Dirty fix for false values returned as blank
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getFeesDuePenalty(FeesDue $model)
    {
        if ($model->isNewRecord) return new InstitutionFeesDuePenalty();

        $penaltyModel = InstitutionFeesDuePenalty::findOne(['id' => $model->penalty_id, 'school_id' => $model->school_id]);
        if (!$penaltyModel) return new InstitutionFeesDuePenalty();

        $penaltyModel->variable_amount = $penaltyModel->penalty_variable_amount;


        return $penaltyModel;
    }

    public function actionClasslist($q = null, $schoolId)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, class_description AS text')
                ->from('core_school_class')
                ->where(['school_id' => $schoolId])
                ->andWhere(['ilike', 'class_description', $q])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($schoolId > 0) {
            $out['results'] = ['id' => $schoolId, 'text' => CoreSchoolClass::find()->where(['school_id' => $schoolId])->class_description];
        }
        return $out;
    }

    public function actionDetails($id)
    {
        $model = $this->findModel($id);
        $user_sch = Yii::$app->user->identity->school_id;
        $read = $admin = false;
        {
            if (isset($user_sch) && $model->school_id == $user_sch) {
                $read = true;
            } else if (Yii::$app->user->can('schoolpay_admin')) {
                $read = true;
                $admin = true;
            }
        }
        if (Yii::$app->user->can('r_fd') && $read) {
            $request = Yii::$app->request;
            $fee_class = $this->findFeeClasses($id);
            $stu_exmp = $this->findExmpStu($id);

            $st_pages = new Pagination(['totalCount' => $stu_exmp->count()]);
            $cl_pages = new Pagination(['totalCount' => $fee_class->count()]);
            $fee_class->offset($cl_pages->offset);
            $fee_class->limit($cl_pages->limit);
            $stu_exmp->offset($st_pages->offset);
            $stu_exmp->limit($st_pages->limit);


            $student_exemp = $stu_exmp->all();
            $class_fee = $fee_class->all();
            Yii::trace($student_exemp);
            Yii::trace($class_fee);

            //Load penalty too
            $penaltyModel = $model->has_penalty ? $this->getFeesDuePenalty($model) : null;


            //load concessions


            $concessions = Yii::$app->db->createCommand('

select si.id, si.student_code, si.first_name, si.last_name, cls.class_code,fc.id as concession,fc.percentage,
            concat_ws(\' \', first_name, middle_name ,middle_name ) as student_name,
            si.school_student_registration_number 
from core_student  si 
inner JOIN core_school_class cls on cls.id = si.class_id

inner join core_school sch on sch.id = cls.school_id
inner join student_fee_concessions sc on si.id=sc.student_id
inner join fee_concessions fc on fc.id=sc.concessions_id

where fc.fee_id =:feeId')
                ->bindValue(':feeId', $id)
                ->queryAll();

            $stdExemptionModel = new FeesDueStudentExemption();
            $feeId = $id;
            $res = ['model' => $model, 'feeId' => $feeId, 'stdExemptionModel' => $stdExemptionModel,
                'student_exemp' => $student_exemp, 'class_fee' => $class_fee, 'cl_pages' => $cl_pages, 'concessions'=>$concessions,
                'st_pages' => $st_pages, 'admin' => $admin, 'penaltyModel' => $penaltyModel, 'id'=>$id];

            return ($request->isAjax) ? $this->renderAjax('fee_applied', $res) :
                $this->render('fee_applied', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to view FEES.');
        }

    }


    protected function findFeeClasses($id)
    {
        $clses = (new Query())
            ->select(['fc.class_id AS cid', 'cl.class_description AS cdes'])
            ->from('institution_fee_class_association  fc')
            ->join('join', 'core_school_class cl', 'cl.id = fc.class_id')
            ->where(['fee_id' => $id]);
        return $clses;

    }

    protected function findExmpStu($id)
    {
        $stu = (new Query())
            ->select(['exmp.student_number AS sn', 'sinfo.first_name AS fn', 'sinfo.middle_name AS mn', 'sinfo.last_name AS ln', 'sc.class_code AS cl'])
            ->from('fees_due_student_exemption  exmp')
            ->join('join', 'core_student sinfo', 'sinfo.student_code = exmp.student_number')
            ->join('join', 'core_school_class sc', 'sc.id=sinfo.class_id')
            ->where(['fee_id' => $id]);
        return $stu;

    }

    /**
     * @return mixed
     * @var $penaltyModel InstitutionFeesDuePenalty
     * Creates a new FeesDue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
//    public function actionCreate()
//    {
//        if (Yii::$app->user->can('rw_fd')) {
//        $request = Yii::$app->request;
//
//        $model = new FeesDue();
//        $penaltyModel = $this->getFeesDuePenalty($model);
//
//        if ($model->load(Yii::$app->request->post())) {
//            Yii::trace($request->post());
//            try {
//                if ($model->save(false)) {
//
//                    //If with penalty, save with penalty too
//                    if ($model->has_penalty)
//                        $this->saveFeePenalty($model, $penaltyModel);
//
//                    // Logs::logEvent("Fee Created: " . $model->description, null, null);
//                    return $this->redirect(['view', 'id' => $model->id]);
//                }
//            } catch (\Exception $e) {
//                $res = ['model' => $model, 'penaltyModel' => $penaltyModel];
//                //$transaction->rollBack();
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                Yii::trace('Error: ' . $error, 'CREATE  FEES DUE ROLLBACK');
//
//                return ($request->isAjax) ? $this->renderAjax('create', $res) :
//
//                    $this->render('create', $res);
//            }
//
//        }
//        $res = ['model' => $model, 'penaltyModel' => $penaltyModel];
//        return ($request->isAjax) ? $this->renderAjax('create', $res) :
//            $this->render('create', $res);
//
//        } else {
//            throw new ForbiddenHttpException('No permissions to create a FEE.');
//        }
//
//
//    }

    public function actionCreate()
    {
        if (Yii::$app->user->can('rw_fd')) {
            $request = Yii::$app->request;

            $model = new FeesDue();

            $penaltyModel = $this->getFeesDuePenalty($model);

            if ($model->load(Yii::$app->request->post())) {
                try {

                    if (\app\components\ToWords::isSchoolUser()) {
                        $model->school_id = Yii::$app->user->identity->school_id;
                    }
                    $model->created_by = Yii::$app->user->identity->getId();
                    if ($model->save(true)) {

                        //If with penalty, save with penalty too
                        if ($model->has_penalty)

                            $this->saveFeePenalty($model, $penaltyModel);

                        Logs::logEvent("Fee Created: " . $model->description, null, null);
                        return $this->redirect(['view', 'id' => $model->id]);
                    }else {
                        \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . 'Error saving');
                    }
                } catch (\Exception $e) {
                    Yii::trace($e);
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
                    $res = ['model' => $model, 'penaltyModel' => $penaltyModel];
                    return ($request->isAjax) ? $this->renderAjax('create', $res) :
                        $this->render('create', $res);
                }

            }
            $res = ['model' => $model, 'penaltyModel' => $penaltyModel];
            return ($request->isAjax) ? $this->renderAjax('create', $res) :
                $this->render('create', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create a FEE.');
        }

    }


    private function saveFeePenalty(FeesDue                   $model,
                                    InstitutionFeesDuePenalty $penaltyModel)
    {
        //Validate the penalty
        $penaltyModel->load(Yii::$app->request->post());
        $penaltyModel->school_id = $model->school_id;

        //Yii::trace("Penalty is ".$penaltyModel);

        //For varying penalties, set amount to zero
        if($penaltyModel->penalty_type == 'VARYING_AMOUNT' || $penaltyModel->penalty_type == 'VARYING_PERCENTAGE') {
            $penaltyModel->penalty_amount = 0;
            Yii::trace($penaltyModel->variable_amount);
            //Only consider values of variable amount
            $penaltyAmounts = array_values($penaltyModel->variable_amount);
            Yii::trace($penaltyAmounts);
            $penaltyModel->penalty_variable_amount = new Expression("cast('".json_encode($penaltyAmounts)."' as jsonb)");
        }else {
            if (empty($penaltyModel->penalty_amount)) {
                $penaltyModel->addError('penalty_amount', 'The penalty amount is required');
                throw new Exception("Validation Error: The penalty amount is required");
            }
            //Clear variable penalties
            $penaltyModel->penalty_variable_amount = new Expression("cast('[]' as jsonb)");;
        }

        if (!$penaltyModel->validate()) {
            $error = '';
            foreach ($penaltyModel->getErrorSummary(false) as $item) {
                 $error .= '|' . $item;
            }
            throw new Exception("Validation Error: " . $error);
        }
        //Manually validate required fields
        if (empty($penaltyModel->penalty_type)) {
            $penaltyModel->addError('penalty_type', 'The penalty type is required');
            throw new Exception("Validation Error: The penalty type is required");
        }



        if (empty($penaltyModel->penalty_frequency)) {
            $penaltyModel->addError('penalty_frequency', 'The penalty frequency is required');
            throw new Exception("Validation Error: ");
        }

        if ($penaltyModel->penalty_frequency == 'RECURRENT') {
            if (empty($penaltyModel->penalty_apply_frequency_term)) {
                $penaltyModel->addError('penalty_apply_frequency_term', 'The penalty frequency term is required');
                throw new Exception("Validation Error: The penalty frequency term is required");
            }

            if (empty($penaltyModel->penalty_apply_frequency_unit)) {
                $penaltyModel->addError('penalty_apply_frequency_unit', 'The penalty frequency is required');
                throw new Exception("Validation Error: ");
            }
        } else {
            $penaltyModel->penalty_apply_frequency_term = null;
            $penaltyModel->penalty_apply_frequency_unit = null;
        }


        $penaltyModel->save(false);
        //Set the penalty id
        $model->penalty_id = $penaltyModel->id;
        $model->save();
    }

    /**
     * Updates an existing FeesDue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        if (!Yii::$app->user->can('rw_fd'))
//            throw new ForbiddenHttpException('No permissions to create a FEE.');
//
//
//        $request = Yii::$app->request;
//        $model = $this->findModel($id);
//        $penaltyModel = $this->getFeesDuePenalty($model);
//
//        $res = ['model' => $model, 'penaltyModel' => $penaltyModel];
//
//        if (!Yii::$app->request->post()) {
//            return ($request->isAjax) ? $this->renderAjax('update', $res) :
//                $this->render('update', $res);
//        }
//        //Load
//        $model->load(Yii::$app->request->post());
//
//
//        if (!$model->validate()) {
//
//            return ($request->isAjax) ? $this->renderAjax('update', $res) :
//                $this->render('update', $res);
//        }
//
//        //Save in transaction
//        $connection = Yii::$app->db;
//        $transaction = $connection->beginTransaction();
//        try {
//            $model->save();
//            //If with penalty, save with penalty too
//            if ($model->has_penalty)
//                $this->saveFeePenalty($model, $penaltyModel);
//
//            $transaction->commit();
//            //  Logs::logEvent("updated Fees: " . $model->description, null, null);
//            return $this->redirect(['details', 'id' => $model->id]);
//        } catch (\Exception $e) {
//            $transaction->rollBack();
//            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//            Yii::trace('Error: ' . $error, 'CREATE  FEES DUE ROLLBACK');
//            $res = ['model' => $model, 'penaltyModel' => $penaltyModel, 'error' => $e->getMessage()];
//            return ($request->isAjax) ? $this->renderAjax('update', $res) :
//                $this->render('update', $res);
//        }
//
//
//    }

    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rw_fd'))
            throw new ForbiddenHttpException('No permissions to update a FEE.');


        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $penaltyModel = $this->getFeesDuePenalty($model);

//        Yii::trace($penaltyModel);
        $res = ['model' => $model, 'penaltyModel' => $penaltyModel];

        if (!Yii::$app->request->post()) {
            return ($request->isAjax) ? $this->renderAjax('update', $res) :
                $this->render('update', $res);
        }
        //Load
        $model->load(Yii::$app->request->post());
        if (!$model->validate()) {
            return ($request->isAjax) ? $this->renderAjax('update', $res) :
                $this->render('update', $res);
        }

        if ($model->approval_status) {
            return $this->requestEditingApproval($model, $penaltyModel);
        }

        //Save in transaction
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $model->save();
            //If with penalty, save with penalty too
            Yii::trace("here penalty");
            if ($model->has_penalty)
                $this->saveFeePenalty($model, $penaltyModel);

            $transaction->commit();
            Logs::logEvent("updated Fees: " . $model->description, null, null);
            return $this->redirect(['details', 'id' => $model->id]);
        } catch (\Exception $e) {
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
            $transaction->rollBack();
            $res = ['model' => $model, 'penaltyModel' => $penaltyModel, 'error' => $e->getMessage()];
            return ($request->isAjax) ? $this->renderAjax('update', $res) :
                $this->render('update', $res);
        }
    }


    private function requestEditingApproval($model, $penaltyModel)
    {
        $existingFee = $this->findModel($model->id);
        //Now build a params request for the changed fields
        $editableFeeFields = array('due_amount', 'end_date', 'next_apply_date', 'description', 'has_penalty', 'arrears_after_units', 'arrears_after_count', 'apply_frequency', 'apply_frequency_unit', 'enable_reminders');

        $request_params = array();
        foreach ($editableFeeFields as $field) {

            if ($existingFee->$field != $model->$field) {
                Yii::trace($model->$field);
                $request_params[$field] = $model->$field;
                Yii::trace($request_params);
            }
        }

        if ($model->has_penalty) {
            $penaltyModel->load(Yii::$app->request->post());

            $penaltyModel->school_id = $model->school_id;
            $this->validatePenaltyModel($penaltyModel);

            $existingPenaltyModel = $this->getFeesDuePenalty($existingFee);

            $editablePenaltyFields = array('penalty_type', 'penalty_frequency', 'penalty_apply_frequency_unit', 'penalty_apply_frequency_term','penalty_variable_amount',
                'penalty_amount', 'penalty_expiry_date', 'maximum_penalty_amount', 'maximum_penalty_application_count', 'school_id');

            $penaltyChangedFields = array();
            foreach ($editablePenaltyFields as $field) {
                if ($existingPenaltyModel->$field != $penaltyModel->$field || !$existingPenaltyModel->$field) {
                    $penaltyChangedFields[$field] = $penaltyModel->$field;
                }
            }
            $penaltyAmounts = array_values($penaltyModel->variable_amount);

             $penaltyChangedFields['penalty_variable_amount'] =$penaltyAmounts ;


            $request_params['penalty'] = (object)$penaltyChangedFields;
        }

        //TODO: Work on penalty too
        $branch = null;
        if(Yii::$app->user->can('view_by_branch')){
            $branch = Yii::$app->user->identity->branch_id;
        }

        $workflowRequest = [
            'approval_required_permission' => json_encode(array('sch_admin','branch_chekers',  'super_admin', 'sys_admin')),
            'record_id' => $model->id,
            'request_params' => json_encode((object)$request_params),
            'request_notes' => 'Edit fee details',
            'school_id' => $model->school_id,
            'workflow_record_type' => 'EDIT_FEE',
            'branch_id'=>$branch
        ];

        try {
            $insert_id = WorkflowController::requestWorkflowApproval($workflowRequest);
            \Yii::$app->session->setFlash('stuAlert', "<i class='fa fa-check-circle-o'></i>&nbsp; Your record has been submitted for approval by a member of either (superadmin, sysadmin or school admin groups)");
            return \Yii::$app->runAction('/workflow/workflow/view', ['id' => $insert_id]);
        } catch (\Exception $e) {
            Yii::trace($e->getMessage());
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $e->getMessage());
            return \Yii::$app->runAction('/feesdue/fees-due/view', ['id' => $model->id]);
        }

    }



    public function actionFindstudents($params, $cls)
    {
        $clses = explode(',', $cls);
        $clses = array_filter($clses);
        if (!empty($clses)) {
            $stu = (new Query())
                ->select(['student_code AS pc', 'first_name AS fn', 'middle_name AS mn', 'last_name AS ln', 'sc.class_code AS cl'])
                ->from('core_school si')
                ->join('join', 'core_scholl_class sc', 'sc.id=si.student_class')
                ->where(['student_class' => $clses, 'active' => true]);
            if (is_numeric($params)) {
                $stu->andWhere(['like', 'CAST(student_code AS VARCHAR)', $params]);
            } else {
                $search_words = explode(' ', $params);
                $search_words = array_filter($search_words);
                $words = array();
                if (count($search_words) > 1) {
                    foreach ($search_words as $word) {
                        $words[] = $word;
                    }
                    if (isset($words[0])) {
                        $name = str_replace(' ', '', $words[0]);
                        $stu->andFilterWhere(['or',
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name]
                        ]);
                    }
                    if (isset($words[1])) {
                        $name = str_replace(' ', '', $words[1]);
                        $stu->andFilterWhere(['or',
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name]
                        ]);
                    }
                    if (isset($words[2])) {
                        $name = str_replace(' ', '', $words[3]);
                        $stu->andFilterWhere(['or',
                            ['ilike', 'first_name', $name],
                            ['ilike', 'middle_name', $name],
                            ['ilike', 'last_name', $name]
                        ]);
                    }

                } else {
                    $name = str_replace(' ', '', $params);
                    $stu->andFilterWhere(['or',
                        ['ilike', 'first_name', $name],
                        ['ilike', 'middle_name', $name],
                        ['ilike', 'last_name', $name],

                    ]);
                }
            }
            $stu->limit(10);
            $count = $stu->count();
            $students = $stu->all();
            if ($count > 0) {
                foreach ($students as $v) {
                    echo "<option value='" . $v['pc'] . "'>" . $v['pc'] . "  - " . $v['cl'] . " :  " . $v['fn'] . " " . $v['mn'] . " " . $v['ln'] . "</option>";
                }
            } else {
                echo "<option> No student found </option>";
            }

        } else {
            echo "<option> No Class Selected </option>";
        }

    }

    public function actionSelectedClasses($id)
    {
        $clses = $this->findFeeClasses($id);
        $count = $clses->count();
        $classes = $clses->all();
        if ($count > 0) {
            foreach ($classes as $cl) {
                echo "<option value='" . $cl['cid'] . "'>" . $cl['cdes'] . "</option>";
            }
        }

    }

    public function actionSelectedStudents($id)
    {
        $stu = $this->findExmpStu($id);
        $count = $stu->count();
        $students = $stu->all();
        if ($count > 0) {
            foreach ($students as $cStu) {
                echo "<option value='" . $cStu['sn'] . "'>" . $cStu['sn'] . "  - " . $cStu['cl'] . " :&nbsp;" . $cStu['fn'] . " " . $cStu['mn'] . " " . $cStu['ln'] . "</option>";
            }
        }

    }

    public function actionReadyToapprove($id)
    {
        $model = $this->findModel($id);
        $model->ready_for_approval = true;
        $model->save(false);
        Logs::logEvent("Ready to approve: " . $model->description, null, null);
        // return $this->redirect(['details', 'id'=>$id]);
        return $this->redirect(['details', 'id'=>$id]);

        //return Yii::$app->runAction('details', ['id' => $id]);
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $model->approval_status = true;
        $model->save(false);
        Logs::logEvent("Approved: " . $model->description, null, null);
        // return $this->redirect(['details', 'id'=>$id]);

        $cls = (new Query())
            ->select(['class_id'])
            ->from('institution_fee_class_association')
            ->where(['fee_id' => $id]);

        if ($cls->count() > 0) {
            foreach ($cls as $class) {
                $stds = (new Query())
                    ->select(['id'])
                    ->from('core_student')
                    ->where(['class_id' => $id]);
                foreach ($stds as $std) {

                }
            }
        }
        return $this->redirect(['details', 'id'=>$id]);

       // return Yii::$app->runAction('details', ['id' => $id]);
    }

    public function actionListToapprove()
    {
        $pending = FeesDue::find()->where(['ready_for_approval' => true, 'approval_status' => false])->all();

    }

    /**
     * Deletes an existing FeesDue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        Logs::logEvent("updated Fees: " . $model->description, null, null);
        $model->delete();

        // return $this->redirect(['index']);
        return Yii::$app->runAction('/fees-due/index');
    }

    public function actionRemoveAssignedClass($cid,$id)
    {
//        $model = FeeClass::findOne(['fee_id' => $id, 'class_id' => $cid]);
//        //check if class has students
//
//        Logs::logEvent("Removing class '.$cid.'  from fee: " . $id, null, null);
//        $model->delete();
        $feeDetails = FeesDue::findOne($id);
        $currentDate =new Expression('NOW()');
        $associatedFee = FeeClass::findOne(['fee_id'=>$id]);


        if($feeDetails->recurrent){
            //get recurrent feesIds
            $getRecurrentFees = FeesDue::find()->where(['parent_fee_id'=>$id])

                ->andWhere("end_date>=".$currentDate) //compound condition

                ->all();


            Yii::trace($getRecurrentFees);
            foreach ($getRecurrentFees as $feeCurrent){
                $withStudents = (new Query())
                    ->select(['ifsa.id'])
                    ->from('institution_fee_student_association ifsa')
                    ->innerJoin('core_student si', 'si.id=ifsa.student_id')
                    ->innerJoin('institution_fee_class_association ca', 'ca.class_id=si.class_id')
                    ->where(['ifsa.fee_id' => $feeCurrent->id])
                    ->andWhere(['ca.class_id'=> $cid])
                    ->all();

                Yii::trace($withStudents);

                if(!$withStudents){
                    try{
                        $connection = Yii::$app->db;
                        $transaction = $connection->beginTransaction();
                        Yii::$app->db->createCommand("update institution_fee_class_association set applied =false where fee_id = :id and class_id =:class_id")
                            ->bindValue(':id', $id)
                            ->bindValue(':class_id', $cid)
                            ->execute();


                        Yii::$app->db->createCommand("delete from institution_fee_class_association where fee_id = :id and class_id =:class_id")
                            ->bindValue(':id', $id)
                            ->bindValue(':class_id', $cid)
                            ->execute();
                        $transaction->commit();
                    }catch (\Exception $e){
                        $transaction->commit();
                        Yii::trace("Class removal error: ".$e->getMessage());
                    }


                }else{
                    $error = 'FEE CLASS ASSOCIATION CAN NOT BE UNDONE BECAUSE FEE ASSOCIATION HAS BEEN APPLIED TO ALL STUDENTS, PLEASE FIRST REMOVE FEE FROM ALL STUDENTS';
                    \Yii::$app->session->setFlash('actionFailed', $error);
                    // throw new Exception("FEE CLASS ASSOCIATION CAN NOT BE UNDONE BECAUSE FEE ASSOCIATION HAS BEEN APPLIED TO ALL STUDENTS, PLEASE FIRST REMOVE FEE FROM ALL STUDENTS");

                }


            }

        }else{
            $withStudents = (new Query())
                ->select(['ifsa.id'])
                ->from('institution_fee_student_association ifsa')
                ->innerJoin('core_student si', 'si.id=ifsa.student_id')
                ->innerJoin('institution_fee_class_association ca', 'ca.class_id=si.class_id')
                ->where(['ifsa.fee_id' => $id])
                ->andWhere(['ca.class_id'=> $cid])
                ->all();
            Yii::trace($withStudents);

            if(!$withStudents){
                try{
                    $connection = Yii::$app->db;
                    $transaction = $connection->beginTransaction();
                    Yii::$app->db->createCommand("update institution_fee_class_association set applied =false where fee_id = :id and class_id =:class_id")
                        ->bindValue(':id', $id)
                        ->bindValue(':class_id', $cid)
                        ->execute();


                    Yii::$app->db->createCommand("delete from institution_fee_class_association where fee_id = :id and class_id =:class_id")
                        ->bindValue(':id', $id)
                        ->bindValue(':class_id', $cid)
                        ->execute();
                    $transaction->commit();
                }catch (\Exception $e){
                    $transaction->commit();
                    Yii::trace("Class removal error: ".$e->getMessage());
                }


            }else{
                $error = 'FEE CLASS ASSOCIATION CAN NOT BE UNDONE BECAUSE FEE ASSOCIATION HAS BEEN APPLIED TO ALL STUDENTS, PLEASE FIRST REMOVE FEE FROM ALL STUDENTS';
                \Yii::$app->session->setFlash('actionFailed', $error);
               // throw new Exception("FEE CLASS ASSOCIATION CAN NOT BE UNDONE BECAUSE FEE ASSOCIATION HAS BEEN APPLIED TO ALL STUDENTS, PLEASE FIRST REMOVE FEE FROM ALL STUDENTS");

            }

        }




        // return $this->redirect(['index']);
        return $this->redirect(['details', 'id'=>$id]);
    }






    public function actionRemoveStdExmpt($stdId,$id)
    {
        $model = FeesDueStudentExemption::findOne(['fee_id' => $id, 'student_number' => $stdId]);
        Logs::logEvent("Removing exempted student '.$stdId.'  from fee: " . $id, null, null);
        $model->delete();

         return $this->redirect(['details', 'id'=>$id]);
        //return Yii::$app->runAction('details', ['id'=>$id]);
    }





    public function actionLists($fid, $clid)
    {
        $stu = $this->findExmpStu($fid)->andWhere(['sinfo.class_id' => $clid]);
        $pages = new Pagination(['totalCount' => $stu->count()]);
        $stu->offset($pages->offset);
        $stu->limit($pages->limit);
        if ($stu->count() > 0) {
            echo "<table id='exempted' class ='table-bordered table table-striped'>";
            echo "<tr>";
            echo "<th class='text-center'> Sr.No </th>";
            echo "<th class='text-center'> Student code </th>";
            echo "<th class='text-center'> Class </th>";
            echo "<th class='text-center'> Exempted Students </th>";
            echo "</tr>";
            $i = 1;
            if (isset($_GET['page'])) {
                $i = (($_GET['page'] - 1) * 20) + 1;
            }
            foreach ($stu->all() as $k => $v) {
                echo "<tr>";
                echo "<td class='text-center'>" . $i . "</td>";
                echo "<td class='text-center'>" . $v['sn'] . "</td>";
                echo "<td class='text-center'>" . $v['cl'] . "</td>";
                echo "<td class='text-center'>" . $v['fn'] . " " . $v['mn'] . " " . $v['ln'] . "</td>";
                echo "</tr>";
                $i++;
            }
            echo "</table>";
            echo "<div>" . LinkPager::widget(['pagination' => $pages]) . "</div>";
        }
    }

    public function actionSearchFees($id)
    {
        $query = FeesDue::find()
            ->andWhere(['school_id' => $id, 'approval_status' => true])
            ->andWhere(['<=', 'effective_date', date("Y-m-d")])
            ->andWhere(['>=', 'end_date', date("Y-m-d")]);

        if ($query->count() > 0) {
            $fees_list = $query->orderBy('date_created DESC')->all();
            echo "<option value=''> Filter by fee </option>";
            foreach ($fees_list as $fee) {
                echo "<option value='" . $fee->id . "'>" . $fee->description . " - " . $fee->due_amount . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }

    public function actionFeesPriority()
    {
        if (!\app\components\ToWords::isSchoolUser())
            throw new ForbiddenHttpException("This area is not accessible to non school users");


        $request = Yii::$app->request;
        $priorityModel = new DynamicModel(['priority_order']);
        $priorityModel->addRule(['priority_order'], 'required');

        $searchModel = new DynamicModel(['student_class']);
        $searchModel->addRule(['student_class'], 'safe');

        $schoolId = Yii::$app->user->identity->school_id;
        // $schoolId = 1;

        $error = null;
        $message = null;

        if ($request->post() && $priorityModel->load($request->post()) && $priorityModel->validate()) {
            //Save the fee priorities
            //Fees will be returned as comma separated, in the set order
            $feesOrder = explode(',', $priorityModel->priority_order);
            $feeCount = count($feesOrder);

            //Save fees priority in trans
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {

                $sql = "UPDATE institution_fees_due set priority = :priority  where id=:id and school_id=:school_id ";
                //Since default priority is 1, set priorities as negatives
                $priorityIndex = $feeCount * -1;
                foreach ($feesOrder as $feeId) {
                    $connection->createCommand($sql)
                        ->bindValue(':priority', $priorityIndex)
                        ->bindValue(':id', $feeId)
                        ->bindValue(':school_id', $schoolId)
                        ->execute();

                    $priorityIndex++; //Increment priority
                }
                $transaction->commit();
                $message = 'Fees priority has been saved succesfully';
                //  Logs::logEvent("Updated Fees Priority: ", null, null);
//                return $this->redirect(['fees-due/fees-priority']);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = $e->getMessage();
            }
        }

        $today = @date('Y-m-d');
        //Find them valid fees for this school

        $query = new Query();
        $query->select(['fdue.*'])
            ->from('institution_fees_due fdue')
            ->leftJoin('institution_fee_class_association cl', 'cl.fee_id=fdue.id')
            ->andWhere(['<=', 'effective_date', $today])
            ->andWhere(['>=', 'end_date', $today])
            ->andFilterWhere(['school_id' => $schoolId = Yii::$app->user->identity->school_id]);

        //Check if search model is set
        if ($searchModel->load($request->get())) {

            Yii::trace("here");
            $query->andFilterWhere(['cl.class_id' => $searchModel->student_class]);
        }

        $query->addOrderBy(['priority' => SORT_ASC, 'id' => SORT_ASC]);

        $feesDue = $query->all();

        $res = ['priorityModel' => $priorityModel, 'feesList' => $feesDue, 'message' => $message, 'error' => $error, 'searchModel' => $searchModel];
        return ($request->isAjax) ? $this->renderAjax('_fees_priority', $res) :
            $this->render('_fees_priority', $res);
    }

    public function actionTestService()
    {

        $connection = Yii::$app->db;
        try {

            //Get pending fee associations

            $feeAssociationsList = Yii::$app->db->createCommand('SELECT fees.id as feeId, classes.id as classId,classes.class_description, fees.child_of_recurrent, fees.due_amount           
                    , fees.description
                    from institution_fees_due fees 
                    inner join institution_fee_class_association assoc on fees.id = assoc.fee_id 
                    inner Join core_school_class classes on classes.id=assoc.class_id
                    WHERE assoc.fee_id = fees.id AND assoc.applied = false and fees.approval_status=true AND fees.effective_date<=:currentTimeStamp AND assoc.class_id=classes.id AND fees.recurrent=false')
                ->bindValue('currentTimeStamp', new Expression('NOW()'))
                ->queryAll();


            if (!$feeAssociationsList) {
                Yii::trace("No due fee associations to process");
                return;
            }


            foreach ($feeAssociationsList as $object) {
                Yii::trace($object);


                //Get all students in class

                $studentsList = Yii::$app->db->createCommand('SELECT * FROM core_student WHERE class_id=:sclass ')
                    ->bindValue(':sclass', $object['classid'])
                    ->queryAll();

                //Determine parent


                if ($object['child_of_recurrent']) {
                    $parentRecurrentFeeDue = $object['feeid'];
                }

                //Fetch them
                $amountLong = $object['due_amount'];


                Yii::trace("Going to process fee due " . $object['description'] . " " . $amountLong);
                $processedRecords = 0;

                foreach ($studentsList as $student) {
                    $transaction = $connection->beginTransaction();

                    Yii::trace($student);
                    //Check exemption for this student and fee
                    //If fee is child of recurrent, check exemption from parent fee
                    //Criteria feeExemption = session.createCriteria(Entity_FeesDueStudentExemption.class);
                    $feeIdToConsiderForExemption = $object['feeid'];
                    if ($object['child_of_recurrent']) {
                        $feeIdToConsiderForExemption = $parentRecurrentFeeDue;
                    }

                    $feeExemption = Yii::$app->db->createCommand('SELECT * FROM fees_due_student_exemption WHERE student_number=:studentNumber and fee_id =:feeId  ')
                        ->bindValue(':studentNumber', $student['student_code'])
                        ->bindValue(':feeId', $feeIdToConsiderForExemption)
                        ->queryOne();

                    //Check exemption now

                    if ($feeExemption) {
                        Yii::trace("Student " . $student['first_name'] . " " . $student['last_name'] . " is exempted from fee " . $object['feeid']);
                        continue;
                    }


                    $connection->createCommand("select apply_fee_to_student(
                        :student_id, 
                        :fee, 
                        :userid, 
                        :username, 
                        :userip)", [
                        ':student_id' => $student['id'],
                        ':fee' => $object['feeid'],
                        ':userid' => 0,
                        ':username' => 'SYSTEM',
                        ':userip' => '',
                    ])->execute();
                    $transaction->commit();

                    Logs::logEvent("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['id'] . " ", null, $student['id']);


                    Yii::trace("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['id']);
                    //Yii::trace("Return:  - Fee applied ok for  student " . $student['last_name'] . " " . $student['first_name']);
                }

                //Update status of fee association
                $sql2 = "UPDATE institution_fee_class_association SET applied = true,date_applied=:date WHERE fee_id = :feeId";
                $connection->createCommand($sql2)
                    ->bindValue(':date', new Expression('NOW()'))
                    ->bindValue(':feeId', $object['feeid'])
                    ->execute();

                Yii::trace("Fee association applied to class " . $object['description']);
            }
            Yii::trace("Fee association applied to all classes ");

        } catch (\Exception $e) {
            $transaction->rollback();

            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Error on assigning a fee: " . $student['last_name'] . ' ' . $student['first_name'] . "(" . $student['student_code'] . ")", $error, $student['id']);

            Yii::trace($error);
            return $error;
            //Yii::trace($error)
        }

    }


    public function actionPerms($id)
    {
        $model = new CoreSchoolClass();
        if (Yii::$app->user->can('rw_role')) {
            $request = Yii::$app->request;
            $classes = CoreSchoolClass::find()->where(['school_id' => $id])->limit(1)->one();

            $available = array();
            $assigned = array();
            //$classes = CoreSchoolClass::find()->where(['type'=>2])->all();

//        foreach($model->authItemChildren as $perms){
//            $assigned[$perms->child] = $perms->child0->description;
//        }

            foreach ($classes as $perm) {
                if (!isset($assigned[$perm->name])) {
                    $available[$perm->id] = $perm->class_description;
                }
            }

            return ($request->isAjax) ? $this->renderAjax('view', ['model' => $model,
                'available' => $available, 'assigned' => $assigned]) :
                $this->render('view', ['model' => $model, 'available' => $available,
                    'assigned' => $assigned]);

        } else {
            throw new ForbiddenHttpException('No permissions to update or create roles.');
        }
    }

    public function actionAssign($name, $action)
    {

        $post = Yii::$app->request->post();
        $perms = $post['selected'];
        $error = [];
        if ($action == 'assign') {
            foreach ($perms as $k => $v) {
                try {
                    $item = new FeeClass();
                    $item->parent = $name;
                    $item->child = $v;
                    $item->save(false);
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }
            }
        } else {
            foreach ($perms as $k => $v) {
                try {
                    $item = FeeClass::find()->where(['parent' => $name, 'child' => $v])->one();
                    $item->delete();
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }

            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$this->actionPermSearch($name, 'available', ''),
            $this->actionPermSearch($name, 'assigned', ''),
            $error];
    }

    public function actionPermSearch($name, $target, $term = '')
    {
        $permissions = AuthItem::find()->where(['type' => 2])->all();
        $model = AuthItem::find()->where(['name' => $name])->limit(1)->one();
        $available = [];
        $assigned = [];

        foreach ($model->authItemChildren as $perms) {
            $assigned[$perms->child] = $perms->child0->description;
        }

        foreach ($permissions as $perm) {
            if (!isset($assigned[$perm->name])) {
                $available[$perm->name] = $perm->description;
            }
        }

        $result = [];
        $var = ${$target};
        if (!empty($term)) {
            foreach ($var as $i => $descr) {
                if (stripos($descr, $term) !== false) {
                    $result[$i] = $descr;
                }
            }
        } else {
            $result = $var;
        }

        return Html::renderSelectOptions('', $result);
    }

    public function actionGetStudents($q = null, $id)

    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        $sv_cl = null;


        $query2 = (new Query())
            ->select(['core_school_class.id', 'core_school_class.class_description'])
            ->from('core_school_class')
            ->innerJoin('institution_fee_class_association', 'core_school_class.id = institution_fee_class_association.class_id')
            ->where(['institution_fee_class_association.fee_id' => $id]);


        //$class_id=array();
        $class_id = array_column($query2->all(), 'id');
//            foreach($cls as $class){
//                $class_id =$class['id'];
//
//            }

        Yii::trace($class_id);
        $query = new Query;
        if (!is_null($q)) {

            $query->select(["CONCAT(first_name, ' ', last_name) AS text", 'id'])
                ->from('core_student')
                ->where(['in', 'class_id', $class_id])
                ->andWhere(['ilike', 'CONCAT(first_name, \' \', last_name)', $q])
                ->limit(7);
            $command = $query->createCommand();
            $datas = $command->queryAll();
            $out['results'] = array_values($datas);
            Yii::trace($datas);
        } elseif ($class_id > 0) {
            $query->select(["CONCAT(first_name, ' ', last_name) AS text", 'id'])
                ->from('core_student')
                ->where(['in', 'class_id', $class_id])
                ->limit(7);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            Yii::trace($data);
            //$out['results'] = ['id' => $class_id, 'text' => CoreStudent::find()->where(['class_id' => $class_id])->first_name];
        }
        Yii::trace($out);

        return $out;

    }

    public function actionSearchStd($id)
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;
            $searchModel = new FeesDueSearch();
            Yii::trace(Yii::$app->request->queryParams);
            $data = $searchModel->searchStudents($id, Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('_search_std', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'id' => $id, 'sort' => $data['sort']]) :
                $this->render('_search_std', ['searchModel' => $searchModel, 'id' => $id,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }

    }


    public function actionAssignStds($id)
    {
        $searchModel = new FeesDueSearch();

        $model = new FeesDueStudentExemption();
        $modelSearch = new FeesDueSearch();

        $request = Yii::$app->request;

        $exemptedModel = new FeesDueStudentExemption();

        // $av_classes = CoreStudent::find()->where(['student_code' => $user_sch]);
        if (Yii::$app->request->post()) {

            Yii::trace("posting inside");
            // print_r($av_classes);
            $data = Yii::$app->request->post();
            Yii::trace($data);
            $exemptedModel->student_number = $data['selected_stu'];
//            $feeClas =array();
            $std = $exemptedModel->student_number;


            if (!empty($std)) {
                $stds = array_unique($std);

                $del_class = [];

                if (!empty(array_filter($stds))) {
                    foreach ($stds as $std) {
                        Yii::trace($stds);
                        $query = new Query();
                        $query->select('student_code')
                            ->from('core_student')
                            ->where(['id' => $std]);

                        $command = $query->createCommand();
                        $datas = $command->queryOne();

                        Yii::trace($datas);
                        $existing = FeesDueStudentExemption::findOne(['fee_id' => $id, 'student_number' => $datas['student_code']]);
                        if (!$existing) {


                            $f_class = new FeesDueStudentExemption();
                            $f_class->student_number = $datas['student_code'];
                            $f_class->fee_id = $id;
                            $f_class->save();
                        }
                    }
                }
                if (!empty(array_filter($del_class))) {
                    foreach ($del_class as $thestds) {
                        $fclass = FeesDueStudentExemption::find()->where(['student_number' => $thestds])->limit(1)->one();
                        $fclass->deledetailste();
                    }
                }
                $res = ['id' => $id];
                //   $feeClassModel->save(false);
                return $this->redirect(['details', 'id' => $id]);
            }
            return $this->redirect(['details', 'id' => $id]);
        }

        $results = $searchModel->searchStudents($id, Yii::$app->request->queryParams);
        $data = $results['query'];
        $stdExemptionModel = new FeesDueStudentExemption();
        $feeDetails = FeesDue::findOne($id);
        $res = ['model' => $model, 'exemptedModel' => $exemptedModel, 'feeDetails'=>$feeDetails,'stdExemptionModel' => $stdExemptionModel
            , 'id' => $id, 'allClsStds' => $data, 'modelSearch' => $modelSearch, 'searchModel' => $searchModel
        ];
        return ($request->isAjax) ? $this->renderAjax('view', $res) :
            $this->render('_assign_students', $res);

//        } else {
//            throw new ForbiddenHttpException('No permssions to view FEES.');
//        }

    }

    public function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print Fees Due',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Fees Due||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }


    public function actionFeesPercentageLocks($schoolId = 0)
    {
        if (!Yii::$app->user->can('rw_fd'))
            throw new ForbiddenHttpException('No permissions to create a FEE.');

        if (\app\components\ToWords::isSchoolUser()) {
            $schoolId = Yii::$app->user->identity->school_id;
        }

        $error = null;
        $message = null;

        $request = Yii::$app->request;


        $model = 0;
        if ($schoolId > 0) {
            $model = SchoolMinimumFeePercentageRules::findOne(['school_id' => $schoolId]);
        }

        if (!$model) {
            $model = new SchoolMinimumFeePercentageRules();
            $model->minimum_percentage = 0;
            $model->school_id = $schoolId;
        }

        $res = ['model' => $model, 'error' => $error, 'message' => $message];

        if (!$request->post()) {
            return ($request->isAjax) ? $this->renderAjax('percentage_locks', $res) :
                $this->render('percentage_locks', $res);
        }


        if (!$model->load($request->post()) || !$model->validate()) {
            $error = 'Validation error';
            return ($request->isAjax) ? $this->renderAjax('percentage_locks', $res) :
                $this->render('percentage_locks', $res);
        }


        if (!$model->save()) {
            $error = 'Failed to save details';
        } else {
            $message = 'Percentage rule has been set successfully';
        }

        $res = ['model' => $model, 'error' => $error, 'message' => $message];
        return ($request->isAjax) ? $this->renderAjax('percentage_locks', $res) :
            $this->render('percentage_locks', $res);
    }


    public static function staticFindFeeClasses($id)
    {
        $clses = (new Query())
            ->select(['fc.class_id AS cid', 'cl.class_description AS cdes'])
            ->from('institution_fee_class_association  fc')
            ->join('join', 'core_school_class cl', 'cl.id = fc.class_id')
            ->where(['fee_id' => $id]);
        return $clses;

    }

    public static function staticFindExmpStu($id)
    {
        $stu = (new Query())
            ->select(['exmp.student_number AS sn', 'sinfo.first_name AS fn', 'sinfo.middle_name AS mn', 'sinfo.last_name AS ln', 'sc.class_code AS cl'])
            ->from('fees_due_student_exemption  exmp')
            ->join('join', 'core_student sinfo', 'sinfo.student_code = exmp.student_number')
            ->join('join', 'core_school_class sc', 'sc.id=sinfo.class_id')
            ->where(['fee_id' => $id]);
        return $stu;

    }
    public function validatePenaltyModel($penaltyModel)
    {
        if (!$penaltyModel->validate()) {
            Yii::trace($penaltyModel->errors);
            throw new Exception("Validation Error: ");
        }
        //Manually validate required fields
        if (empty($penaltyModel->penalty_type)) {
            $penaltyModel->addError('penalty_type', 'The penalty type is required');
            throw new Exception("Validation Error: The penalty type is required");
        }

        if (empty($penaltyModel->penalty_amount)) {
            $penaltyModel->addError('penalty_amount', 'The penalty amount is required');
            throw new Exception("Validation Error: The penalty amount is required");
        }

        if (empty($penaltyModel->penalty_frequency)) {
            $penaltyModel->addError('penalty_frequency', 'The penalty frequency is required');
            throw new Exception("Validation Error: ");
        }

        if ($penaltyModel->penalty_frequency == 'RECURRENT') {
            if (empty($penaltyModel->penalty_apply_frequency_term)) {
                $penaltyModel->addError('penalty_apply_frequency_term', 'The penalty frequency term is required');
                throw new Exception("Validation Error: The penalty frequency term is required");
            }

            if (empty($penaltyModel->penalty_apply_frequency_unit)) {
                $penaltyModel->addError('penalty_apply_frequency_unit', 'The penalty frequency is required');
                throw new Exception("Validation Error: ");
            }
        } else {
            $penaltyModel->penalty_apply_frequency_term = null;
            $penaltyModel->penalty_apply_frequency_unit = null;
        }
    }

    public static function staticFeesDuePenalty(FeesDue $model)
    {
        if ($model->isNewRecord) return new InstitutionFeesDuePenalty();

        $penaltyModel = InstitutionFeesDuePenalty::findOne(['id' => $model->penalty_id, 'school_id' => $model->school_id]);
        if (!$penaltyModel) return new InstitutionFeesDuePenalty();
        return $penaltyModel;
    }


}

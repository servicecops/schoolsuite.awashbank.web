<?php

namespace app\modules\feesdue\controllers;


use app\components\AwashStatementHelper;

use app\modules\messages\models\MessageOutbox;
use app\modules\messages\models\MsgSchoolCredits;
use app\modules\messages\models\MsgSchoolCreditsHistory;
use app\modules\messages\models\MsgSchoolMessages;
use app\modules\messages\models\MsgSchoolOutbox;
use app\modules\schoolcore\models\AwashBranches;
use app\modules\schoolcore\models\CoreSchool;

use yii\httpclient\Client;
use Yii;

use yii\db\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

//use yii\widgets\LinkPager;

/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class FeesServiceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionTest()
    {
        $this->layout = 'loginlayout';
        $status = "Fee association applied to all classes ";
        return json_encode([
            'returncode' => 909,
            'returnmessage' => $status,
        ]);

    }

    /**
     * Will process pending fee associations and pass debits to the respective students
     * process onetime fees
     */
    public function actionProcessPendingFeeClassAssociations()
    {

        $this->layout = 'loginlayout';

        $request = Yii::$app->request;

        $connection = Yii::$app->db;
        try {
            $transaction = $connection->beginTransaction();
            //Get pending fee associations

            $limit = 500;
            if(isset(Yii::$app->params['ProcessPendingFeeClassAssociationsBatchSize'])) {
                $limit = Yii::$app->params['ProcessPendingFeeClassAssociationsBatchSize'];
            }

            $feeAssociationsList = Yii::$app->db->createCommand('SELECT fees.id as feeId, classes.id as classId,classes.class_description, assoc.credit_hours,fees.child_of_recurrent, fees.due_amount           
                    , fees.description, fees.parent_fee_id
                    from institution_fees_due fees 
                    inner join institution_fee_class_association assoc on fees.id = assoc.fee_id 
                    inner Join core_school_class classes on classes.id=assoc.class_id
                    WHERE  assoc.applied = false and fees.approval_status=true AND fees.effective_date<=:currentTimeStamp AND assoc.class_id=classes.id AND fees.recurrent=false limit '. $limit)
                ->bindValue('currentTimeStamp', new Expression('NOW()'))
                ->queryAll();


            if (!$feeAssociationsList) {
                Yii::trace("No due fee associations to process");
                return;
            }


            foreach ($feeAssociationsList as $object) {
                Yii::trace($object);


                //Get all students in class

                $studentsList = Yii::$app->db->createCommand('SELECT id, student_code, first_name, middle_name, last_name FROM core_student WHERE class_id=:sclass ')
                    ->bindValue(':sclass', $object['classid'])
                    ->queryAll();

                //Determine parent


                if ($object['child_of_recurrent']) {
                    $parentRecurrentFeeDue = $object['parent_fee_id'];
                }

                //Fetch them
                $amountLong = $object['due_amount'];


                Yii::trace("Going to process fee due " . $object['description'] . " " . $amountLong);
                $processedRecords = 0;

                foreach ($studentsList as $student) {

                    Yii::trace($student);
                    //Check exemption for this student and fee
                    //If fee is child of recurrent, check exemption from parent fee
                    //Criteria feeExemption = session.createCriteria(Entity_FeesDueStudentExemption.class);
                    $feeIdToConsiderForExemption = $object['feeid'];
                    if ($object['child_of_recurrent']) {
                        $feeIdToConsiderForExemption = $parentRecurrentFeeDue;
                    }

                    $feeExemption = Yii::$app->db->createCommand('SELECT id FROM fees_due_student_exemption WHERE student_number=:studentNumber and fee_id =:feeId  ')
                        ->bindValue(':studentNumber', $student['student_code'])
                        ->bindValue(':feeId', $feeIdToConsiderForExemption)
                        ->queryOne();

                    //Check exemption now

                    if ($feeExemption) {
                        Yii::trace("Student " . $student['first_name'] . " " . $student['last_name'] . " is exempted from fee " . $object['feeid']);
                        continue;
                    }

                    $jsonObj= [
                        'studentId'=> $student['id'],
                        'feeId'=> $object['feeid'],
                        'webUserId'=>0,
                        'webUserName'=>'SYSTEM',
                        'creditHours'=>$object['credit_hours'],
                        'ipAdress'=>''
                    ];

                    $jsonText = json_encode($jsonObj);

                    $connection->createCommand("select apply_fee_to_student(:jsonText)", [
                        ':jsonText' =>$jsonText

                    ])->execute();

//                    $connection->createCommand("select apply_fee_to_student(
//                        :student_id,
//                        :fee,
//                        :userid,
//                        :username,
//                        :userip)", [
//                        ':student_id' => $student['id'],
//                        ':fee' => $object['feeid'],
//                        ':userid' => 0,
//                        ':username' => 'SYSTEM',
//                        ':userip' => '',
//                    ])->execute();

                    $this->echoLog("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['id'] . " ");


                    Yii::trace("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['id']);
                    //Yii::trace("Return:  - Fee applied ok for  student " . $student['last_name'] . " " . $student['first_name']);
                }

                //Update status of fee association
                $sql2 = "UPDATE institution_fee_class_association SET applied = true,date_applied=:date WHERE fee_id = :feeId";
                $connection->createCommand($sql2)
                    ->bindValue(':date', new Expression('NOW()'))
                    ->bindValue(':feeId', $object['feeid'])
                    ->execute();

                Yii::trace("Fee association applied to class " . $object['description']);
            }
            Yii::trace("Fee association applied to all classes ");
            $transaction->commit();


        } catch (\Exception $e) {
            $transaction->rollback();

            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
           // $this->echoLog("Error on assigning a fee: " . $student['last_name'] . ' ' . $student['first_name'] . "(" . $student['student_code'] . ")");

            Yii::trace($error);
            return $error;
            //Yii::trace($error)
        }
    }

    /**
     * @throws \yii\db\Exception
     * Will process due penalties
     */
    public function actionProcessPenalties()
    {
        $this->layout = 'loginlayout';
        $transaction = Yii::$app->db->beginTransaction();
        try {

            $penalties = Yii::$app->db->createCommand('SELECT assoc.id as associd           
                    from institution_fee_student_association assoc 
                    inner join core_student std on std.id = assoc.student_id 
                    inner Join institution_fees_due fee on fee.id=assoc.fee_id
                    inner Join institution_fees_due_penalty penalty on penalty.id=assoc.penalty_id
                    WHERE assoc.penalties_closed = false AND assoc.next_penalty_apply_date <= current_date and fee_outstanding_balance < 0 AND assoc.has_penalty =true limit 10000')

                //->bindValue('currentTimeStamp', new Expression('NOW()'))
                ->queryAll();

            if (!$penalties) {
                $this->echoLog("No Penalties to process");
                die("");
            }

            foreach ($penalties as $value) {
                Yii::trace($value['associd']);

                $result = \Yii::$app->db->createCommand("select apply_student_penalty(:associd)")
                    ->bindValue(':associd', $value['associd'])
                    ->execute();

            }

            $transaction->commit();
            $this->echoLog("Penalties applied successfully");
        } catch (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing penalties " . $error);
            $transaction->rollback();
            return $error;
        }
    }

    /**
     * Will process pending fee associations and pass debits to the respentive students
     * This will only apply fees that are not recurrent
     */
    public function actionProcessRecurringFees()
    {
        $this->layout = 'loginlayout';

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            //Get pending recurring associations

            $feesDueList = Yii::$app->db->createCommand('SELECT * FROM institution_fees_due WHERE approval_status=true AND recurrent=true AND effective_date<=:currentTimeStamp AND end_date>=:currentTimeStamp AND next_apply_date<=:currentTimeStamp ')
                ->bindValue(':currentTimeStamp', new Expression('NOW()'))
                ->bindValue(':status', 1)
                ->queryAll();

            Yii::trace($feesDueList);
            if (!$feesDueList) {
                Yii::trace("No due recurring fee associations to process");
                return;
            }

            foreach ($feesDueList as $feesDue) {

                //Determine periods


                $result = \Yii::$app->db->createCommand("SELECT * FROM fee_create_period_child_fee(:fee_id) AS result")
                    ->bindValue(':fee_id', $feesDue['id'])
                    ->execute();


            }

            $transaction->commit();
            $this->echoLog("Recurring fees processed successfully");

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
           $this->echoLog("Error on processing recurring fees ", $error, null);

            return $error;
        }
    }

    public function actionProcessBookedStudentFees()
    {
        $this->layout = 'loginlayout';

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {


            $bookedFeesList = Yii::$app->db->createCommand('SELECT * FROM institution_fee_student_association WHERE applied=:applied AND due_date=:dueDate')
                ->bindValue(':applied', false)
                ->bindValue(':dueDate', new Expression('NOW()'))
                ->queryAll();

            if (!$bookedFeesList)
                return;


            foreach ($bookedFeesList as $studentAssociation) {

                $jsonObj= [
                    'studentId'=> $studentAssociation['student_id'],
                    'feeId'=> $studentAssociation['fee_id'],
                    'webUserId'=>0,
                    'webUserName'=>'SYSTEM',
                    'creditHours'=>1,
                    'ipAdress'=>' '

                ];
                $jsonText = json_encode($jsonObj);

                $sqlQuery = Yii::$app->db->createCommand("SELECT * FROM apply_fee_to_student(:jsonText) AS result")
                    ->bindValue(':jsonText', $jsonText)
                    ->queryOne();


                Yii::trace($sqlQuery);
                $aBoolean = $sqlQuery;

                Yii::trace("Return: " . " " . " - Booked Fee applied ok for  student " . $studentAssociation['student_id']);
            }

//            Yii::trace($bookedFeesList);
//            foreach ($bookedFeesList as $studentAssociation) {
//                $sqlQuery = Yii::$app->db->createCommand("SELECT * FROM apply_fee_to_student(:student_id, :fee_id, 0, 'SYSTEM', '') AS result")
//                    ->bindValue(':student_id', $studentAssociation['student_id'])
//                    ->bindValue(':fee_id', $studentAssociation['fee_id'])
//                    ->queryOne();
//
//
//                Yii::trace($sqlQuery);
//                $aBoolean = $sqlQuery;
//
//                Yii::trace("Return: " . " " . " - Booked Fee applied ok for  student " . $studentAssociation['student_id']);
//            }

            $transaction->commit();
        } catch
        (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing booked std fees " . $error);

            Yii::trace($error);
            $transaction->rollBack();
        }
    }


    //process fee sms


    public function actionProcessSmsFee()
    {
        $this->layout = 'loginlayout';

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {


            $sqlQuery = Yii::$app->db->createCommand("SELECT * FROM process_fee_reminders() as Result")
                ->queryOne();


            Yii::trace($sqlQuery);
            $aBoolean = $sqlQuery;

            $transaction->commit();
        } catch
        (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing booked std fees " . $error);

            Yii::trace($error);
            $transaction->rollBack();
        }
    }

    public function actionStatementTest()
    {
        $statement = AwashStatementHelper::getStatement('01320233419401', '2022-01-01', '2022-04-01');

        return json_encode($statement);
    }

    public function actionProcessSuiteMessages()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {


            $messages = Yii::$app->db->createCommand("select msg.*, sch.sms_sender_id from msg_school_messages msg
                                                    inner join core_school sch on sch.id = msg.school_id
                                                    where schedule_date <= :currentTimeStamp and processed =false")
                ->bindValue('currentTimeStamp', new Expression('NOW()'))
                ->queryAll();

            if (!$messages) {
                $this->echoLog("No messages to process");
                return;
            }
            foreach ($messages as $msg) {
                $this->processScheduledMessages($msg);
            }


            $transaction->commit();
        } catch
        (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing suite messages " . $error);

            Yii::trace($error);
            $transaction->rollBack();
        }
    }


    private function processScheduledMessages($msg)
    {
        Yii::trace($msg);
        //get school Details
        $schoolDetails = CoreSchool::findOne([$msg['school_id']]);
        if (!$schoolDetails) {
            $this->echoLog("School not found while processing message ");
            throw new Exception('School not found while processing message');
        }

        //The school should have a gl, else create it

        $msgCredits = MsgSchoolCredits::findOne(['school_id' => $schoolDetails->id]);
        if (!$msgCredits) {
            $msgCredits = new MsgSchoolCredits();
            $msgCredits->school_id = $schoolDetails->id;
            $msgCredits->credit_bal = 0;
            $msgCredits->description = $schoolDetails->school_name . ' - Message A/C';
            $msgCredits->save(false);
            $schoolCredits = $msgCredits->credit_bal;
        } else {
            $schoolCredits = $msgCredits->credit_bal;

        }


        if (strcasecmp('STATIC_RECIPIENTS', $msg['message_type']) == 0) {

            $this->processStaticRecipientsMessage($msg, $schoolDetails, $msgCredits);
        }
        if (strcasecmp('STUDENT_GROUP', $msg['message_type']) == 0 || strcasecmp('BY_CLASSES', $msg['message_type']) == 0) {
            $this->processGroupRecipientsMessage($msg, $schoolDetails, $msgCredits);
        }

        Yii::trace('staticd ' . strcasecmp('STATIC_RECIPIENTS', $msg['message_type']));
    }

    private function processStaticRecipientsMessage($msg, CoreSchool $schoolDetails, MsgSchoolCredits $schoolCredits)
    {
        $this->echoLog("Starting processing for message (STATIC) ");


        $msgDetails = MsgSchoolMessages::findOne(['id' => $msg['id']]);

        if (!$msgDetails->recipients) {
            $this->echoLog("No static recipients");

            $msgDetails->processed = true;
            $msgDetails->date_processed = new Expression('NOW()');
            $msgDetails->save(false);
            return;
        }


        //get phone numbers
        $phoneNumbers = explode(',', $msg['recipients']);
        $recipientList = [];
        foreach ($phoneNumbers as $phoneNumber) {
            $params = trim($phoneNumber);
            $valid = $this->validePhone($params);
            if ($valid) {
                array_push($recipientList, $params);
            } else {
                $this->echoLog("Invalid or Duplicate Phone Number ");
            }
        }

        if (count($recipientList) == 0) {

            $this->echoLog("All specified recipients are invalid");

            $msgDetails->processed = true;
            $msgDetails->date_processed = new Expression('NOW()');
            $msgDetails->save(false);
            return;
        }

        //Now we are sending
        $this->sendStaticSchoolMessageToRecipients($msg, $schoolDetails, $schoolCredits, $recipientList);

    }


    private function validePhone($phoneNumber)
    {
        if (!preg_match('/^(0|251)(9|1)\d{8}$/i', $phoneNumber)) {

            return false;
        } else {
            return true;
        }

    }

    private function sendStaticSchoolMessageToRecipients($msg, CoreSchool $schoolDetails, $schoolCredits, $recipientList)
    {

        $msg_text = $msg['message_template'];
        $msgId = $msg['id'];
        $messageText = $this->applySchoolMessageTemplate($msg_text, $schoolDetails, null);

        $messagesSentSoFar = 0;
        $creditsBefore = $schoolCredits->credit_bal;
        foreach ($recipientList as $reciepient) {
            if ($schoolCredits->credit_bal <= 0) {
                $this->echoLog(" Could not send to " . $reciepient . '- School has run out of credits');
                break;
            }

            $currentMessage = $messageText;
            $currentReceipient = $reciepient;
            $queued = $this->queueSchoolMessage($currentMessage, $currentReceipient, $schoolDetails, $msgId);
            if ($queued) {
                $schoolCredits->credit_bal -= 1;
            }

            $this->echoLog("Message queued for " . $reciepient);
            $messagesSentSoFar += 1;
        }
        if ($messagesSentSoFar > 0) {
            $this->echoLog($messagesSentSoFar . " Message sent ");
            $MsgSchoolCreditsHistory = new MsgSchoolCreditsHistory();
            $MsgSchoolCreditsHistory->credits = -1 * $messagesSentSoFar;
            $MsgSchoolCreditsHistory->date_created = new Expression('NOW()');
            $MsgSchoolCreditsHistory->credits_after = $schoolCredits->credit_bal;
            $MsgSchoolCreditsHistory->credits_before = $creditsBefore;
            $MsgSchoolCreditsHistory->message_credits_id = $schoolCredits->id;
            $MsgSchoolCreditsHistory->description = $messagesSentSoFar . ' messages sent for message ' . $msg['title'];
            $MsgSchoolCreditsHistory->save(false);
        }


        //update mshlog msg count
        $sql2 = "UPDATE msg_school_messages SET processed = true,  date_processed=:date_processed ,
                               number_of_processed_messages = :number_of_processed_messages WHERE  id= :id";
        Yii::$app->db->createCommand($sql2)
            ->bindValue(':date_processed', new Expression('NOW()'))
            ->bindValue(':number_of_processed_messages', $messagesSentSoFar)
            ->bindValue(':id', $msg['id'])
            ->execute();
    }


    private function applySchoolMessageTemplate($msg_text, $schoolDetails, $stdList)
    {
        Yii::trace($stdList);

        $msg_text = str_replace('{SCHOOL_NAME}', isset($schoolDetails->school_name) ? $schoolDetails->school_name : '', $msg_text);
        $msg_text = str_replace('{GUARDIAN_NAME}', isset($stdList['guardian_name']) ? $stdList['guardian_name'] : '', $msg_text);
        $msg_text = str_replace('{STUDENT_PAYMENT_CODE}', isset($stdList['student_code']) ? $stdList['student_code'] : '', $msg_text);
        $msg_text = str_replace('{STUDENT_CLASS}', isset($stdList['class_code']) ? $stdList['class_code'] : '', $msg_text);
        $msg_text = str_replace('{OUTSTANDING_BALANCE}', isset($stdList['outstanding_balance']) ? $stdList['outstanding_balance'] : '', $msg_text);
        $msg_text = str_replace('{STUDENT_NAME}', isset($stdList['full_name']) ? $stdList['full_name'] : '', $msg_text);
        return $msg_text;


    }

    private function queueSchoolMessage($currentMessage, $currentReceipient, CoreSchool $schoolDetails, $msgId)
    {
        if (isset($currentReceipient)) {
            $queueMsg = new MsgSchoolOutbox();
            $queueMsg->sender_id = $schoolDetails->sms_sender_id;
            $queueMsg->msg_text = $currentMessage;
            $queueMsg->msg_id = $msgId;
            $queueMsg->sent = false;
            $queueMsg->recipient = $currentReceipient;
            $queueMsg->school_id = $schoolDetails->id;
            $queueMsg->date_created = new Expression('NOW()');
            if ($queueMsg->save(false)) {
                return true;
            } else {
                return false;
            }

        }


    }

    private function processGroupRecipientsMessage($msg, CoreSchool $schoolDetails, MsgSchoolCredits $schoolCredits)
    {
        echo("Starting processing for message  " . $msg['message_type'] . "\n");


        $msgDetails = MsgSchoolMessages::findOne(['id' => $msg['id']]);

        if (!$msgDetails->recipient_group) {
            echo("No group recipients\n");

            $msgDetails->processed = true;
            $msgDetails->date_processed = new Expression('NOW()');
            $msgDetails->save(false);
            return;
        }


        Yii::trace($msgDetails->recipient_group);
        $recipientGroupArray = explode(',', $msgDetails->recipient_group);

        if (strcasecmp('ALL', $recipientGroupArray[0]) == 0) {
            $this->echoLog("No valid group recipients");
        }

        if (!$recipientGroupArray) {
            $this->echoLog("No valid group recipients");

            $msgDetails->processed = true;
            $msgDetails->date_processed = new Expression('NOW()');
            $msgDetails->save(false);
            return;
        }


        // $recipientGroupArray = array_map('intval', json_decode($recipientGroupArray, true));


        //  $recipientGroupArray = array_map('intval', $recipientGroupArray);
        Yii::trace("uii");
        $gpIds = implode(",", $recipientGroupArray);
        Yii::trace($gpIds);

        $gpLists = Yii::$app->db->createCommand("select  concat(cs.first_name,' ',cs.last_name) as full_name,cs.guardian_phone,cs.guardian2_phone,cs.guardian2_name, cs.student_phone ,sag.outstanding_balance, cs.guardian_name, cs.student_code 
from student_group_information sgi  inner join student_group_student sgs  on sgs.group_id  = sgi.id 
inner join core_student cs on cs.id =sgs.student_id  inner join student_account_gl sag on sag.id = cs.student_account_id  where sgi.school_id =:schoolId and sgi.id in (:gpIds)")
            ->bindValue('schoolId', $msgDetails->school_id)
            ->bindValue('gpIds', (int)$gpIds)
            ->queryAll();

        if (strcasecmp('BY_CLASSES', $msg['message_type']) == 0) {
            $gpLists = Yii::$app->db->createCommand("select concat(cs.first_name,' ',cs.last_name) as full_name,cs.guardian2_phone,cs.guardian2_name, cs.guardian_phone, cs.student_phone,csc.class_code, sag.outstanding_balance, cs.guardian_name, cs.student_code
from core_student cs   
    inner join core_school_class csc   on cs.class_id   = csc.id
inner join student_account_gl sag on sag.id = cs .student_account_id where csc.school_id =:schoolId and csc.id in (:gpIds)")
                ->bindValue('schoolId', $msgDetails->school_id)
                ->bindValue('gpIds', (int)$gpIds)
                ->queryAll();


        }
        //  Yii::trace($gpLists);
        //Now we are sending
        $this->sendGroupSchoolMessageToRecipients($msg, $schoolDetails, $schoolCredits, $gpLists);


    }

    private function sendGroupSchoolMessageToRecipients($msg, CoreSchool $schoolDetails, MsgSchoolCredits $schoolCredits, $gpLists)
    {
        Yii::trace($gpLists);
        $msgId = $msg['id'];
        $msg_text = $msg['message_template'];
        $messagesSentSoFar = 0;
        $creditsBefore = $schoolCredits->credit_bal;
        foreach ($gpLists as $stdList) {

            Yii::trace($schoolCredits->credit_bal);
            if (strcasecmp('GUARDIAN', $msg['send_to']) == 0) {
                $phoneNumber = trim($stdList['guardian_phone']);
            } else {
                $phoneNumber = trim($stdList['student_phone']);
            }


            $valid = $this->validePhone($phoneNumber);
            if (!$valid) {
                $this->echoLog("Invalid Phone number'.$phoneNumber.', Could not send for  " . $stdList['student_code']);
                continue;
            }

            if ($schoolCredits->credit_bal <= 0) {
                $this->echoLog(" Could not send to " . $phoneNumber . '- School has run out of credits');
                break;
            }


            $currentReceipient = $phoneNumber;

            $messageText = $this->applySchoolMessageTemplate($msg_text, $schoolDetails, $stdList);
            $currentMessage = $messageText;
            $queued = $this->queueSchoolMessage($currentMessage, $currentReceipient, $schoolDetails, $msgId);
            if ($queued) {
                $schoolCredits->credit_bal -= 1;
            }

            $this->echoLog("Message queued for " . $phoneNumber);
            $messagesSentSoFar += 1;

            //If sending to guardian, replace recipient and send to second guardian too
            if (strcasecmp('GUARDIAN', $msg['send_to']) == 0) {

                $phoneNumber = $stdList['guardian2_phone'];
                $valid = $this->validePhone($phoneNumber);
                if (!$valid) {
                    $this->echoLog("Invalid Phone number'.$phoneNumber.', Could not send for  " . $stdList['student_code']);
                    continue;
                }

                if ($schoolCredits->credit_bal <= 0) {
                    $this->echoLog(" Could not send to " . $phoneNumber . '- School has run out of credits');
                    break;
                }

                $currentReceipient = $phoneNumber;

                $messageText = $this->applySchoolMessageTemplate($msg_text, $schoolDetails, $stdList);
                $currentMessage = $messageText;
                $queued = $this->queueSchoolMessage($currentMessage, $currentReceipient, $schoolDetails, $msgId);
                if ($queued) {
                    $schoolCredits->credit_bal -= 1;
                }

                $this->echoLog("Message queued for " . $phoneNumber);
                $messagesSentSoFar += 1;

                Yii::trace("number of messages sent after :" . $messagesSentSoFar);

            }

        }
        if ($messagesSentSoFar > 0) {
            $this->echoLog($messagesSentSoFar . " Message sent ");
            $MsgSchoolCreditsHistory = new MsgSchoolCreditsHistory();
            $MsgSchoolCreditsHistory->credits = -1 * $messagesSentSoFar;
            $MsgSchoolCreditsHistory->date_created = new Expression('NOW()');
            $MsgSchoolCreditsHistory->credits_after = $schoolCredits->credit_bal;
            $MsgSchoolCreditsHistory->credits_before = $creditsBefore;
            $MsgSchoolCreditsHistory->message_credits_id = $schoolCredits->id;
            $MsgSchoolCreditsHistory->description = $messagesSentSoFar . ' messages sent for message' . $msg['title'];
            $MsgSchoolCreditsHistory->save(false);
        }


        //update mshlog msg count
        $msgOriginal = MsgSchoolMessages::findOne(['id' => $msg['id']]);

        $msgOriginal->processed = true;
        $msgOriginal->number_of_processed_messages = $messagesSentSoFar;
        $msgOriginal->date_processed = new Expression('NOW()');
        $msgOriginal->save(false);

        $schoolCredits->save(false);

    }

    private function echoLog($message)
    {
        echo(date('Y-m-d H:i:s') . $message . "\n");
    }

    public function actionDispatchMessages()
    {
        $pending = MessageOutbox::findAll(['message_status' => 'PENDING', 'email_message' => false]);
        if (count($pending) == 0) {
            echo(date('Y-m-d H:i:s') . ":=> No messages to send\n");
        }
        foreach ($pending as $msg) {
            try {
                //Result is of type acceptreport
//                <acceptreport>
//			<statuscode>0</statuscode>
//			<statusmessage>Message accepted for delivery</statusmessage>
//			<messageid>cc8f2d49-f7b9-4df9-b5fe-306322932d43</messageid>
//			<recipient>0913013755</recipient>
//			<messagetype>SMS:TEXT</messagetype>
//			<messagedata>TestMessage</messagedata>
//		</acceptreport>

                $result = $this->dispatchMessage($msg->recipient_number, $msg->message_text);
                if ($result->statuscode == 0) {
                    $msg->message_status = 'SENT';
                    $msg->time_sent = date('Y-m-d H:i:s');
                    $msg->save(false);
                    echo(date('Y-m-d H:i:s') . ':=> Sent message to ' . $msg->recipient_number . ' => ' . $msg->message_text . "\n");
                }

            } catch (\Exception $e) {
                Yii::trace($e);
                echo(date('Y-m-d H:i:s') . ':=> Failure sending message to ' . $msg->recipient_number . ' => ' . $e->getMessage() . "\n");
            }
        }
    }

    public function actionDispatchSchoolMessages()
    {
        $pending = MsgSchoolOutbox::findAll(['sent' => false]);
        if (count($pending) == 0) {
            echo(date('Y-m-d H:i:s') . ":=> No messages to send\n");
        }
        foreach ($pending as $msg) {
            try {
                //Result is of type acceptreport
//                <acceptreport>
//			<statuscode>0</statuscode>
//			<statusmessage>Message accepted for delivery</statusmessage>
//			<messageid>cc8f2d49-f7b9-4df9-b5fe-306322932d43</messageid>
//			<recipient>0913013755</recipient>
//			<messagetype>SMS:TEXT</messagetype>
//			<messagedata>TestMessage</messagedata>
//		</acceptreport>

                $result = $this->dispatchMessage($msg->recipient, $msg->msg_text, $msg->sender_id);
                if ($result->statuscode == 0) {
                    $msg->sent = true;
                    $msg->save(false);
                    echo(date('Y-m-d H:i:s') . ':=> Sent message to ' . $msg->recipient . ' => ' . $msg->msg_text . "\n");
                }

            } catch (\Exception $e) {
                Yii::trace($e);
                echo(date('Y-m-d H:i:s') . ':=> Failure sending message to ' . $msg->recipient . ' => ' . $e->getMessage() . "\n");
            }
        }
    }


    private function dispatchMessage($to, $message, $sender_id = null)
    {
        $smsUser = Yii::$app->params['awashSmsApiUsername'];
        $smsPassword = Yii::$app->params['awashSmsApiPassword'];
        $smsUrl = Yii::getAlias('@awashSmsUrl');
        if (!$smsUrl) $smsUrl = 'http://10.36.30.27:9509/api';


        if (!$sender_id) {
            $sender_id = 'Awash-Eschool';

            if (!$sender_id) {
                $sender_id = 'AB-Eschool';
            }

            $smsUrl = "$smsUrl?command=SendMessage&Username=" . urlencode($smsUser) .
                "&Password=" . urlencode($smsPassword) . "&Recipient=" . urlencode($to) . "&MessageType=SMS:TEXT&MessageData=" .
                urlencode($message) . '&originator=' . urlencode($sender_id) . '&charset=' . urlencode('UTF-8');

            Yii::trace($smsUrl);
            $client = new Client([
                'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
            ]);

            $response = $client->createRequest()
                ->setMethod('GET')
                ->setUrl($smsUrl)
                ->setOptions(['sslVerifyPeer' => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false])
                ->send();


            $content = $response->getContent();
            Yii::trace($content);
            if (!$response->isOk) {
                $error = 'Could not connect to Awash sms service. Please try again later';
                throw new ForbiddenHttpException($error);
            }

            $soapResponse = simplexml_load_string($content);

            $reponseBody = $soapResponse->children()
                ->data->children()[0];

            return $reponseBody;
        }
    }


    public function actionSetPasswords()
    {

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $usersList = Yii::$app->db->createCommand("select * from awash_temp_users where text_password is null limit 100")
          //  ->Limit(100)
            ->queryAll();
        try {
            foreach ($usersList as $k => $v) {

//generate unique password
                $length = 5;

                $chars = array_merge(range(0, 5), range('a', 'z'), range('A', 'Z'));

                shuffle($chars);

                $password = implode(array_slice($chars, 0, $length));
                $auth_key =Yii::$app->security->generateRandomString();;

//hash password
                $hashed = Yii::$app->security->generatePasswordHash($password);

//Update table
                $sql2 = "UPDATE awash_temp_users SET text_password = :password,firstname=:firstname, lastname=:lastname, hash_password=:hashpsd, updated_username=:updated_username,auth_key=:auth_key WHERE id = :theId";
                $connection->createCommand($sql2)
                    ->bindValue(':password', $password)
                    ->bindValue(':hashpsd', $hashed)
                    ->bindValue(':updated_username', strtolower($v['username']))
                    ->bindValue(':lastname', ucwords($v['lastname']))
                    ->bindValue(':firstname', ucwords($v['firstname']))
                    ->bindValue(':auth_key', $auth_key)
                    ->bindValue(':theId', $v['id'])
                    ->execute();
            }
            $transaction->commit();


        } catch
        (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing  " . $error);

            Yii::trace($error);
            $transaction->rollBack();
        }


    }

    public function actionUpdateSchreg()
    {

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $usersList = Yii::$app->db->createCommand("select * from core_school")
            //  ->Limit(100)
            ->queryAll();
        try {
            foreach ($usersList as $k => $v) {

                $branchDetails = AwashBranches::findOne(['id'=>$v['branch_id'] ]);
                Yii::trace($branchDetails);

//Update table
                $sql2 = "UPDATE core_school SET branch_region = :branchReg WHERE id = :theId";
                $connection->createCommand($sql2)
                    ->bindValue(':branchReg', $branchDetails['branch_region'])
                    ->bindValue(':theId', $v['id'])
                    ->execute();
            }
            $transaction->commit();


        } catch
        (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing  " . $error);

            Yii::trace($error);
            $transaction->rollBack();
        }


    }

    public function actionDeleteBranch()
    {

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $brList = Yii::$app->db->createCommand("select * from awash_branches where branch_region is null")
            //  ->Limit(100)
            ->queryAll();
        try {
            foreach ($brList as $k => $v) {

                $branchDetails = CoreSchool::findOne(['branch_id'=>$v['id']]);
                Yii::trace($branchDetails);

//Update table
                if($branchDetails){

                    $this->echoLog("Error on deleting  branch: " .$branchDetails['school_name']. ' is attached to branch : '.$v['branch_name'] );?>

                    <br/>
                    <?php

                }else{
                    $sql2 = "delete from awash_branches where id = :theId";
                    $connection->createCommand($sql2)
                        ->bindValue(':theId',$v['id'])
                        ->execute();
                }


            }
            $transaction->commit();


        } catch
        (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on processing  " . $error);

            Yii::trace($error);
            $transaction->rollBack();
        }


    }

    /**
     * Will process pending fee associations and pass debits to the respective students
     * process onetime fees
     */
    public function actionProcessPendingFeeGroupAssociations()
    {

        $this->layout = 'loginlayout';

        $request = Yii::$app->request;

        $connection = Yii::$app->db;
        try {
            $transaction = $connection->beginTransaction();
            //Get pending fee associations

            $gpAssociationsList = Yii::$app->db->createCommand('select ifga .id as gpFeeId, ifd.id as feeId, sgi.id as stdgpid ,ifd.child_of_recurrent, ifd.due_amount           
                    , ifd.description, ifd.parent_fee_id, ifga.credit_hours
from institution_fees_due ifd 
    inner join institution_fee_group_association ifga on ifga.fee_id = ifd.id
inner join student_group_information sgi on sgi.id = ifga.group_id where ifga.applied = false
and ifd.approval_status=true AND ifd.effective_date<=:currentTimeStamp  AND ifd.recurrent=false;')
                ->bindValue('currentTimeStamp', new Expression('NOW()'))
                ->queryAll();

            if (!$gpAssociationsList) {
                Yii::trace("No due group associations to process");
                return;
            }


            foreach ($gpAssociationsList as $object) {
                Yii::trace($object);


                //Get all students in class

                $studentsList = Yii::$app->db->createCommand('SELECT * FROM core_student cs inner join student_group_student sgs on sgs.student_id=cs.id
                WHERE sgs.group_id=:gpId')
                    ->bindValue(':gpId', $object['stdgpid'])
                    ->queryAll();

                //Determine parent


                if ($object['child_of_recurrent']) {
                    $parentRecurrentFeeDue = $object['parent_fee_id'];
                }

                //Fetch them
                $amountLong = $object['due_amount'];


                Yii::trace("Going to process fee due " . $object['description'] . " " . $amountLong);
                $processedRecords = 0;

                foreach ($studentsList as $student) {


                    //Check exemption for this student and fee
                    //If fee is child of recurrent, check exemption from parent fee
                    //Criteria feeExemption = session.createCriteria(Entity_FeesDueStudentExemption.class);
                    $feeIdToConsiderForExemption = $object['feeid'];
                    if ($object['child_of_recurrent']) {
                        $feeIdToConsiderForExemption = $parentRecurrentFeeDue;
                    }

                    $feeExemption = Yii::$app->db->createCommand('SELECT * FROM fees_due_student_exemption WHERE student_number=:studentNumber and fee_id =:feeId  ')
                        ->bindValue(':studentNumber', $student['student_code'])
                        ->bindValue(':feeId', $feeIdToConsiderForExemption)
                        ->queryOne();

                    //Check exemption now

                    if ($feeExemption) {
                        Yii::trace("Student " . $student['first_name'] . " " . $student['last_name'] . " is exempted from fee " . $object['feeid']);
                        continue;
                    }
                    Yii::trace($object['feeid']);
                    Yii::trace($student);

                    $jsonObj= [
                        'studentId'=> $student['student_id'],
                        'feeId'=> $object['feeid'],
                        'webUserId'=>0,
                        'webUserName'=>'SYSTEM',
                        'creditHours'=>$object['credit_hours'],
                        'ipAdress'=>''
                    ];
                    $jsonText = json_encode($jsonObj);
                    Yii::trace($jsonText);


                    $connection->createCommand("select apply_fee_to_student(:jsonText)", [
                        ':jsonText' =>$jsonText

                    ])->execute();

                    $this->echoLog("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['student_id'] . " ");


                    Yii::trace("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['student_id']);
                    //Yii::trace("Return:  - Fee applied ok for  student " . $student['last_name'] . " " . $student['first_name']);
                }

                //Update status of fee association
                $sql2 = "UPDATE institution_fee_group_association SET applied = true,date_applied=:date WHERE fee_id = :feeId";
                $connection->createCommand($sql2)
                    ->bindValue(':date', new Expression('NOW()'))
                    ->bindValue(':feeId', $object['feeid'])
                    ->execute();

                Yii::trace("Fee association applied to class " . $object['description']);
            }
            Yii::trace("Fee association applied to all classes ");
            $transaction->commit();


        } catch (\Exception $e) {
            $transaction->rollback();

            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            $this->echoLog("Error on assigning a fee: ");

            Yii::trace($error);
            return $error;
            //Yii::trace($error)
        }
    }
   

}

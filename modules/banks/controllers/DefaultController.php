<?php

namespace app\modules\banks\controllers;

class DefaultController extends \app\controllers\DefaultController
{

    protected function classModels($id)
    {
        $classes = [
            'user-access'=>[
                'search'=>'app\modules\banks\models\BankSettlementAccessUsersSearch',
                'model'=>'app\modules\banks\models\BankSettlementAccessUsers',
                'rights'=>[
                    'index'=>'super_admin', 'view'=>'super_admin','create'=>'super_admin',
                    'update'=>'super_admin', 'delete'=>'super_admin',
                ]
            ]
        ];
        return $classes[$id];
    }
}

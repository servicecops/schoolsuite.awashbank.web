<?php

namespace app\modules\banks\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\banks\models\Reconciliation;
use app\modules\banks\models\BankDetailsSearch;

if (!Yii::$app->session->isActive){
          session_start();  
      }

/**
 * BankDetailsController implements the CRUD actions for BankDetails model.
 */
class ReportsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BankDetails models.
     * @return mixed
     */
    public function actionMobileTrans()
    {
    if(Yii::$app->user->can('bank_reports')){
            $request = Yii::$app->request;
            $searchModel = new BankDetailsSearch();
            $data = $searchModel->searchMobileTrans(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages'=>$data['pages'], 'page'=>$data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('mobile_transactions', [
                'searchModel' => $searchModel,'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ]) : 
                $this->render('mobile_transactions', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ]);
        } else {
        throw new ForbiddenHttpException('No permissions to view Bank transactions.');
        }
    }

    public function actionUnebMno()
    {
        if(!Yii::$app->user->can('uneb_mno')){
            throw new ForbiddenHttpException('No permissions to view this area.');
        }

        $request = Yii::$app->request;
        $searchModel = new BankDetailsSearch();
        $data = $searchModel->searchUnebMno(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];

        $res = ['searchModel' => $searchModel,'dataProvider' => $dataProvider, 'pages'=>$data['pages'], 'sort'=>$data['sort'] ];
        return $request->isAjax ? $this->renderAjax('uneb_trans', $res) : $this->render('uneb_trans', $res);
    }

    public function actionAllTrans()
    {
        if(!Yii::$app->user->can('bank_reports')) {
            throw new ForbiddenHttpException('No permissions to view Bank transactions.');
        }

        $request = Yii::$app->request;
        $searchModel = new BankDetailsSearch();
        $data = $searchModel->searchAll(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages'=>$data['pages'], 'page'=>$data['cpage']];

        $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages'=>$pages, 'sort'=>$data['sort'] ];

        return ($request->isAjax) ? $this->renderAjax('all_transactions', $res) : $this->render('all_transactions', $res);
    }


   

}
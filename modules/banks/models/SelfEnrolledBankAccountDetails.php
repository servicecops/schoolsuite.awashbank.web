<?php

namespace app\modules\banks\models;

use Yii;

/**
 * This is the model class for table "bank_account_details".
 *
 * @property integer $id
 * @property integer $bank_id
 * @property string $account_title
 * @property string $account_number
 * @property string $account_type
 * @property string $date_created
 * @property string $date_modified
 * @property integer $school_id
 *
 * @property NominatedBankDetails $bank
 */
class SelfEnrolledBankAccountDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'self_enrolled_bank_account_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_id', 'account_number'], 'required'],
            [['bank_id', 'school_id'], 'integer'],
            [['account_title', 'account_number'], 'string'],
            [['date_created', 'date_modified'], 'safe'],
            [['account_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_id' => 'Bank ID',
            'account_title' => 'Account Title',
            'account_number' => 'Account Number',
            'account_type' => 'Account Type',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
            'school_id' => 'School ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(NominatedBankDetails::className(), ['id' => 'bank_id']);
    }
}

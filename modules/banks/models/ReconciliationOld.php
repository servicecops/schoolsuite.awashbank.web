<?php

namespace app\modules\banks\models;

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\AwashBankRegion;
use app\modules\schoolcore\models\AwashBranches;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\RefRegion\RefRegion;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use function GuzzleHttp\Promise\all;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * BankDetailsSearch represents the model behind the search form about `app\modules\banks\models\BankDetails`.
 */
class ReconciliationOld extends Model
{
    public $start_date;
    public $end_date;
    public $payment_channel;
    public $bank;
    public $school;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date'], 'required'],
            [['start_date', 'end_date', 'payment_channel', 'bank', 'school'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function bankDestination()
    {
        $_SESSION['br']['reindex'] = ['id' => 'id', 'col' => 'channel'];
        $_SESSION['ch']['other'] = 'none';

        return $this->reindexArray($this->bankQuery(), 'id', 'channel', 'branch', 'region', 'schCode','bankReg');
    }

    public function chnSch()
    {
        $_SESSION['br']['reindex'] = ['id' => 'id', 'col' => 'channel'];
        $_SESSION['ch']['other'] = 'other';

        return $this->reindexArray($this->schoolQuery(), 'id', 'channel', 'branch', 'region', 'schCode','bankReg');
    }

    public function schChannels()
    {
        $_SESSION['br']['reindex'] = ['id' => 'id', 'col' => 'sch'];
        $_SESSION['ch']['other'] = 'none';

        return $this->reindexArray($this->channelQuerys(), 'id', 'sch', 'branch', 'region', 'schCode','bankReg');
    }

    private function bankQuery()
    {
        $query = new Query();
        $end_date = date('Y-m-d', strtotime($this->end_date . ' +1 day'));
        $query->select(['chn.id AS id', 'chn.channel_name AS channel', 'bd.bank_name AS Bank', 'sch.school_code AS schCode', 'br.branch_name AS branch', 'rg.description AS region','abr.bank_region_name As bankReg','count(bd.bank_name) AS trans', 'SUM(pr.amount) AS total'])
            ->from('payments_received pr')
            ->innerJoin('core_school sch', 'pr.school_id = sch.id')
            ->innerJoin('payment_channels chn', 'pr.payment_channel = chn.id')
            ->innerJoin('awash_branches br', 'sch.branch_id = br.id')
            ->innerJoin('ref_region rg', 'sch.region = rg.id')
            ->innerJoin('awash_bank_region abr', 'sch.branch_region = abr.id')
            ->innerJoin('core_nominated_bank bd', 'sch.bank_name = bd.id')
            ->where(['pr.reversed' => false, 'pr.reversal' => false])
            ->andWhere(['between', 'pr.date_created', $this->start_date, $end_date]);
        if ($this->payment_channel) {
            $query->andWhere(['chn.id' => $this->payment_channel]);
        }

        if(Yii::$app->user->can('view_by_region')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy(['chn.id', 'chn.channel_name', 'bd.bank_name', 'br.branch_name', 'sch.school_code', 'rg.description','abr.bank_region_name'])
            ->orderBy('chn.channel_name', 'chn.id');
        $_SESSION['chn_data'] = $query;
        return $query->all(Yii::$app->db2);
    }

    private function schoolQuerys()
    {
        $query = new Query();
        $end_date = date('Y-m-d', strtotime($this->end_date . ' +1 day'));
        $query->select(['chn.id AS id', 'chn.channel_name AS channel', 'sch.school_name AS sch', 'sch.school_code AS schCode','abr.bank_region_name As bankReg', 'br.branch_name AS branch', 'rg.description AS region', 'count(sch.school_name) AS trans', 'SUM(pr.amount) AS total'])
            ->from('payments_received pr')
            ->innerJoin('core_school sch', 'pr.school_id = sch.id')
            ->innerJoin('payment_channels chn', 'pr.payment_channel = chn.id')
            ->innerJoin('core_nominated_bank bd', 'sch.bank_name = bd.id')
            ->innerJoin('awash_branches br', 'sch.branch_id = br.id')
            ->innerJoin('ref_region rg', 'sch.region = rg.id')
            ->innerJoin('awash_bank_region abr', 'sch.branch_region = abr.id')
            ->leftJoin('auto_settlement_requests ast', 'ast.payment_id = pr.id')
            ->where(['pr.reversed' => false, 'pr.reversal' => false])
            ->andWhere(['between', 'pr.date_created', $this->start_date, $end_date]);
        if (Yii::$app->user->identity->user_level == 'bank_user') {
            //For each bank, return transactions for their internal channels
            //Plus transactions settled with them
            //Get the bank for its code
            $bank = BankDetails::findOne(Yii::$app->user->identity->bank_id);
            //Select the bank config
            $bankConfig = Yii::$app->params['bankTransactionsConfig'][$bank->bank_code];
            if (empty($bankConfig)) throw new ForbiddenHttpException('Bank config for ' . $bank->bank_code . ' is not defined.');

            $bankInterChannelCodes = $bankConfig['internalChannelCodes']; //An array of channel codes

            $internalPaymentChannels = PaymentChannels::find()->where(['in', 'channel_code', $bankInterChannelCodes])->all();
            $internalPaymentChannelIds = ArrayHelper::getColumn($internalPaymentChannels, 'id');

            $query->andFilterWhere(['or',
                ['in', 'pr.payment_channel', $internalPaymentChannelIds] //From internal codes
               // ['ast.settlement_bank_id' => $bank->id]
            ]); //settled at bank
        }
        if ($this->payment_channel) {
            $query->andWhere(['chn.id' => $this->payment_channel]);
        }
        $query->groupBy(['chn.id', 'chn.channel_name', 'sch.school_name', 'br.branch_name', 'sch.school_code', 'rg.description'])
            ->orderBy('chn.channel_name', 'chn.id');
        $_SESSION['chn_data'] = $query;
        return $query->all(Yii::$app->db2);
    }

    private function channelQuerys()
    {
        $query = new Query();
        $end_date = date('Y-m-d', strtotime($this->end_date . ' +1 day'));
        $query->select(['sch.id AS id', 'sch.school_name AS sch', 'sch.school_code AS schCode', 'chn.channel_name AS chn', 'count(pr.id) AS trans', 'SUM(pr.amount) AS total',])
            ->from('payments_received pr')
            ->innerJoin('payment_channels chn', 'pr.payment_channel=chn.id')
            ->innerJoin('core_school sch', 'pr.school_id = sch.id')
            ->innerJoin('core_nominated_bank bd', 'sch.bank_name = bd.id')
            ->Where(['pr.reversed' => false, 'pr.reversal' => false])
            ->andWhere(['between', 'pr.date_created', $this->start_date, $end_date]);
        if (Yii::$app->user->identity->user_level == "bank_user") {
            //For each bank, return transactions for their internal channels
            //Plus transactions settled with them
            //Get the bank for its code
            $bank = BankDetails::findOne(Yii::$app->user->identity->bank_id);
            //Select the bank config
            $bankConfig = Yii::$app->params['bankTransactionsConfig'][$bank->bank_code];
            if (empty($bankConfig)) throw new ForbiddenHttpException('Bank config for ' . $bank->bank_code . ' is not defined.');

            $bankInterChannelCodes = $bankConfig['internalChannelCodes']; //An array of channel codes

            $internalPaymentChannels = PaymentChannels::find()->where(['in', 'channel_code', $bankInterChannelCodes])->all();
            $internalPaymentChannelIds = ArrayHelper::getColumn($internalPaymentChannels, 'id');

            $query->andFilterWhere(['or',
                ['in', 'pr.payment_channel', $internalPaymentChannelIds] //From internal codes
              //  ['ast.settlement_bank_id' => $bank->id]
            ]); //settled at bank
        } else if ($this->bank) {
            $query->andWhere(['bd.id' => $this->bank]);
        }

        if ($this->school) {
            $query->andWhere(['sch.id' => $this->school]);
        }

        $query->groupBy(['sch.id', 'sch.school_name', 'chn.channel_name', 'sch.school_code'])
            ->orderBy('sch.school_name', 'sch.id');
        $_SESSION['chn_data'] = $query;
        return $query->all(Yii::$app->db2);
    }


    public function reindexArray($array, $index, $name, $branch, $region, $schCode,$bankReg)
    {

     //   Yii::trace($name);
        $result = [];
        foreach ($array as $arr) {
           Yii::trace($arr);


            $id = $arr[$index];
            $nm = $arr[$name];
//            $br = $arr[$branch];
//            $rg = $arr[$region];
            $schcode = $arr[$schCode];
            Yii::trace($id);
           // $bankReg = $arr[$bankReg];
//Yii::trace($nm);
            $sch =CoreSchool::findOne([$id]);
            $stds =CoreStudent::find()->where(['school_id' => $sch['id'],'archived'=>false])->count();
          Yii::trace($stds);
            $Reg =AwashBankRegion::findOne(['id'=>$sch['branch_region']])->bank_region_name;
            $bkReg =RefRegion::findOne(['id'=>$sch['region']])->description;
            $brch =AwashBranches::findOne(['id'=>$sch['branch_id']])->branch_name;
            unset($arr[$name]);
            unset($arr[$branch]);
            unset($arr[$region]);
            unset($arr[$schCode]);
            unset($arr[$bankReg]);
            $result[$id]['name'] = $nm;
            $result[$id]['branch'] =$brch;
            $result[$id]['region'] = $bkReg;
            $result[$id]['bankReg'] = $Reg;
            $result[$id]['schCode'] = $schcode;
            $result[$id]['stds'] = $stds;
            $result[$id]['data'][] = $arr;
        }

       // Yii::trace($result);
        return $result;
    }

    public static function getExportQuery()
    {

        $file = '@app/modules/banks/views/reconciliation/exportPdfExcel';


        if ($_SESSION['ch']['other'] == 'other') {
            $file = '@app/modules/banks/views/reconciliation/exportPdfExcel';

        }

        if(!isset($_SESSION['chn_data'])){
            $dta =null;
        }else{
            $dta= $_SESSION['chn_data'];
        }

        $data = [

            'data' =>$dta,
            'fileName' => 'transaction_reconciliations',
            'title' => 'Channel-Bank Destination',
            'exportFile' => $file,
        ];

        return $data;
    }
}

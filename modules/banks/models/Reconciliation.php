<?php

namespace app\modules\banks\models;

use app\models\PaymentChannels;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

if (!Yii::$app->session->isActive){
          session_start();  
      }

/**
 * BankDetailsSearch represents the model behind the search form about `app\modules\banks\models\BankDetails`.
 */
class Reconciliation extends Model
{
    public $start_date;
    public $end_date; 
    public $payment_channel;
    public $bank;
    public $school;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date'], 'required'],
            [['start_date', 'end_date', 'payment_channel', 'bank', 'school'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
   
    public function bankDestination()
    {
        $_SESSION['br']['reindex']= ['id'=>'id', 'col'=>'channel'];
        return $this->reindexArray($this->bankQuery(), 'id', 'channel');
    }

    public function chnSch()
    {
        $_SESSION['br']['reindex']= ['id'=>'id', 'col'=>'channel'];
        return $this->reindexArray($this->schoolQuery(), 'id', 'channel');
    }

    public function schChannel()
    {
        $_SESSION['br']['reindex']= ['id'=>'id', 'col'=>'sch'];
        return $this->reindexArray($this->channelQuery(), 'id', 'sch');
    }

    private function bankQuery()
    {
        $query = new Query();
        $end_date = date('Y-m-d', strtotime($this->end_date . ' +1 day'));
        $query ->select(['chn.id AS id', 'chn.channel_name AS channel', 'bd.bank_name AS Bank', 'count(bd.bank_name) AS trans', 'SUM(pr.amount) AS total' ])
                ->from('payments_received pr')
                ->innerJoin('core_school sch','pr.school_id = sch.id')
                ->innerJoin('payment_channels chn', 'pr.payment_channel = chn.id')
                ->innerJoin('core_nominated_bank bd','sch.bank_name = bd.id')
                ->where(['pr.reversed'=>false, 'pr.reversal'=>false])
                ->andWhere(['between', 'pr.date_created', $this->start_date, $end_date]);
        if($this->payment_channel){
          $query->andWhere(['chn.id'=>$this->payment_channel]);
        }
          $query->groupBy(['chn.id', 'chn.channel_name', 'bd.bank_name'])
                ->orderBy('chn.channel_name', 'chn.id');
        $_SESSION['chn_data'] = $query;
    return $query->all(Yii::$app->db2);
    }

    private function schoolQuery()
    {
        $query = new Query();
        $end_date = date('Y-m-d', strtotime($this->end_date . ' +1 day'));
        $query ->select(['chn.id AS id', 'chn.channel_name AS channel', 'sch.school_name AS sch', 'count(sch.school_name) AS trans', 'SUM(pr.amount) AS total' ])
                    ->from('payments_received pr')
                    ->innerJoin('core_school sch','pr.school_id = sch.id')
                    ->innerJoin('payment_channels chn', 'pr.payment_channel = chn.id')
                    ->innerJoin('core_nominated_bank bd','sch.bank_name = bd.id')
                    ->leftJoin('auto_settlement_requests ast','ast.payment_id = pr.id')
                    ->where(['pr.reversed'=>false, 'pr.reversal'=>false])
                    ->andWhere(['between', 'pr.date_created', $this->start_date, $end_date]);
        if(Yii::$app->user->identity->user_level == 'bank_user'){
            //For each bank, return transactions for their internal channels
            //Plus transactions settled with them
            //Get the bank for its code
            $bank = BankDetails::findOne(Yii::$app->user->identity->bank_id);
            //Select the bank config
            $bankConfig = Yii::$app->params['bankTransactionsConfig'][$bank->bank_code];
            if (empty($bankConfig)) throw new ForbiddenHttpException('Bank config for ' . $bank->bank_code . ' is not defined.');

            $bankInterChannelCodes = $bankConfig['internalChannelCodes']; //An array of channel codes

            $internalPaymentChannels = PaymentChannels::find()->where(['in', 'channel_code', $bankInterChannelCodes])->all();
            $internalPaymentChannelIds = ArrayHelper::getColumn($internalPaymentChannels, 'id');

            $query->andFilterWhere(['or',
                ['in', 'pr.payment_channel', $internalPaymentChannelIds], //From internal codes
                ['ast.settlement_bank_id'=>$bank->id]]); //settled at bank
        }
        if($this->payment_channel){
          $query->andWhere(['chn.id'=>$this->payment_channel]);
        }
        $query->groupBy(['chn.id', 'chn.channel_name', 'sch.school_name'])
                ->orderBy('chn.channel_name', 'chn.id');
        $_SESSION['chn_data'] = $query;

    return $query->all(Yii::$app->db2);
    }

    private function channelQuery()
    {
        $query = new Query();
        $end_date = date('Y-m-d', strtotime($this->end_date . ' +1 day'));
        $query ->select(['sch.id AS id', 'sch.school_name AS sch', 'chn.channel_name AS chn', 'count(chn.channel_name) AS trans', 'SUM(pr.amount) AS total' ])
                    ->from('payments_received pr')
                    ->innerJoin('payment_channels chn', 'pr.payment_channel=chn.id' )
                    ->innerJoin('core_school sch','pr.school_id = sch.id')
                    ->innerJoin('core_nominated_bank bd','sch.bank_name = bd.id')
                    ->leftJoin('auto_settlement_requests ast','ast.payment_id = pr.id')
                    ->Where(['pr.reversed'=>false, 'pr.reversal'=>false])
                    ->andWhere(['between', 'pr.date_created', $this->start_date, $end_date]);
        if(Yii::$app->user->identity->user_level == "bank_user"){
            //For each bank, return transactions for their internal channels
            //Plus transactions settled with them
            //Get the bank for its code
            $bank = BankDetails::findOne(Yii::$app->user->identity->bank_id);
            //Select the bank config
            $bankConfig = Yii::$app->params['bankTransactionsConfig'][$bank->bank_code];
            if (empty($bankConfig)) throw new ForbiddenHttpException('Bank config for ' . $bank->bank_code . ' is not defined.');

            $bankInterChannelCodes = $bankConfig['internalChannelCodes']; //An array of channel codes

            $internalPaymentChannels = PaymentChannels::find()->where(['in', 'channel_code', $bankInterChannelCodes])->all();
            $internalPaymentChannelIds = ArrayHelper::getColumn($internalPaymentChannels, 'id');

            $query->andFilterWhere(['or',
                ['in', 'pr.payment_channel', $internalPaymentChannelIds], //From internal codes
                ['ast.settlement_bank_id'=>$bank->id]]); //settled at bank
        }
        else if($this->bank){
            $query->andWhere(['bd.id'=>$this->bank]);
        }

        if($this->school){
            $query->andWhere(['sch.id'=>$this->school]);   
        }

        $query->groupBy(['sch.id', 'sch.school_name', 'chn.channel_name'])
                ->orderBy('sch.school_name', 'sch.id');
        $_SESSION['chn_data'] = $query;
        return $query->all(Yii::$app->db2);
    }

  

    public function reindexArray($array, $index, $name)
    {
        $result = [];
        foreach($array as $arr){
            $id= $arr[$index]; $nm=$arr[$name];
            unset($arr[$name]);
            $result[$id]['name'] = $nm;
            $result[$id]['data'][] = $arr;
        }
        Yii::trace($result);
        return $result;
    }

    public static function getExportQuery() 
    {

        $file = '@app/modules/banks/views/reconciliation/exportPdfExcel';


//        if ($_SESSION['ch']['other'] == 'other') {
//            $file = '@app/modules/banks/views/reconciliation/exportPdfExcel';
//
//        }

        if(!isset($_SESSION['chn_data'])){
            $dta =[];
        }else{
            $dta= $_SESSION['chn_data'];
        }

        $data = [

            'data' =>$dta,
            'fileName' => 'transaction_reconciliations',
            'title' => 'Channel-Bank Destination',
            'exportFile' => $file,
        ];
//    $data = [
//            'data'=>$_SESSION['chn_data'],
//            'fileName'=>'transaction_reconciliations',
//            'title'=>'Channel-Bank Destination',
//            'exportFile'=>'@app/modules/banks/views/reconciliation/exportPdfExcel_other',
//        ];

        return $data;
    }
}

<?php
Yii::trace($results);
?>
<div class="col-md-12">
    <table class ='table'>
        <thead><tr><th colspan="12"> Reconciliation summary for (<?= $model->start_date." - ".$model->end_date; ?>)</th></tr>

        <tr>
            <th > Bank Region</th>
            <th > Country Region </th>
            <th > Branch </th>

            <th >School Name </th>
            <th> Number of Students </th>

        </tr>
        </thead>
        <?php
        $gsum = $gcount = 0;
        foreach($results as $res) :
            $sum = $count = 0;
            $rs = count($res['data']) + 2;
            ?>

            <tr>
                <th rowspan= "<?= $rs; ?>"> <?= $res['bankReg'] ?> </th>
                <th rowspan= "<?= $rs; ?>"> <?= $res['region'] ?> </th>
                <th rowspan= "<?= $rs; ?>"> <?= $res['branch'] ?> </th>

                <th rowspan= "<?= $rs; ?>"> <?= $res['name'] ?> </th>
                <th rowspan= "<?= $rs; ?>"> <?= $res['stds'] ?> </th>
                <?php foreach($jar['head'] as $head){
                    echo "<th>".$head."</th>";
                } ?>
            </tr>
            <?php foreach($res['data'] as $row) :
            ?>
            <tr>
                <?php foreach($jar['dbn'] as $k=>$v){
                    if($k==0){
                        echo "<td>".$row[$v]."</td>";
                    } else {
                        echo "<td>".number_format($row[$v])."</td>";
                    }
                }

                ?>
            </tr>
            <?php
            $sum = $sum + $row['total'];
            $count = $count + $row['trans'];
        endforeach; ?>
            <tr class="border-total">
                <td>TOTAL</td>  <td><b><?= number_format($count) ?></b></td>  <td><b><?= number_format($sum) ?></b></td>
            </tr>
            <?php
            $gsum = $gsum + $sum;
            $gcount = $gcount + $count;
        endforeach; ?>
        <tr style="background-color:#E3EEF1 !important;">
            <th colspan="6">GRAND TOTAL</th>
            <th><?= number_format($gcount); ?></th>
            <th><?= number_format($gsum); ?></th>
        </tr>
    </table>
</div>

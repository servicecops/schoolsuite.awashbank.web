<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\BankDetails */

$this->title = $model->bank_name;
$this->params['breadcrumbs'][] = ['label' => 'Bank Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bank-view hd-title" data-title="<?= $model->bank_name ?>">
<div class="row">
<div class="col-md-12">
    <div class="letters no-padding">
      <div class="row" style="padding-top:100px;">
                <?php if($model->bank_logo): ?>
               <img class="sch-icon" src="<?= Url::to(['/import/import/image-link', 'id'=>$model->bank_logo]) ?>" height="100" width="100" />
                <?php endif; ?><br>
           <span class="col-md-12 col-sm-12 col-xs-12 sch-title"> <?= Html::encode($this->title) ?></span>

           <div class="col-xs-12">
            <div class="top-buttons pull-right">
                <?= Html::a('<i class="fa fa-edit"></i> Edit Logo', ['logo', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-edit"></i> Edit Details', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
            </div>
    </div>
    <div class="letters">
    <hr class="l_header">
    
    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
            //'id',
            'bank_name',
            'bank_address',
            'contact_email:email',
            'contact_phone',
            'date_created',
        ],
    ]) ?>
    </div>
    </div>
</div>
</div>




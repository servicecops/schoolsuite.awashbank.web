<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\banks\models\BankDetails;

?>

<div class="mobile_access">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>

    <div class="col-xs-3 no-padding">
        <?= $form->field($model, 'bank_id')->dropDownList(ArrayHelper::map(BankDetails::find()->all(), 'id', 'bank_name'), ['prompt'=>'All Banks', 'class'=>'form-control input-sm'])->label(false) ?>
            </div>

    <div class="col-xs-3 no-padding"><?= $form->field($model, 'device_id', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Device Id'])->label(false) ?></div>
    <div class="col-xs-3 no-padding"><?= $form->field($model, 'name', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Enter User Name'])->label(false) ?></div>
    <div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
    <?php ActiveForm::end(); ?>

</div>

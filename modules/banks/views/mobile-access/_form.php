<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\banks\models\BankDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\banks\models\MobileAccessUsers */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin([
    'id'=>'auto_enrollment_form',
  ]); ?>
 
   <div class="col-xs-12 col-lg-12 no-padding" >
        <div class="col-sm-6">
        <?= $form->field($model, 'name', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Enter Name'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
        <?= $form->field($model, 'phone_number', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Phone Number'] ])->textInput()->label('') ?>
        </div>
   </div>


    <div class="col-xs-12 no-padding">
        <div class="col-sm-6">
        <?= $form->field($model, 'bank_id')->dropDownList(ArrayHelper::map(BankDetails::find()->all(), 'id', 'bank_name'), ['prompt'=>'All Banks', 'class'=>'form-control input-sm'])->label(' ') ?>
        </div>
        <div class="col-sm-6">
        <?= $form->field($model, 'device_id', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Device Id'] ])->textInput()->label('') ?>
        </div>
   </div>

   <div class="col-xs-12 no-padding">
       <div class="col-sm-6 col-xs-12" >
            <div class="checkbox checkbox-inline checkbox-info">
                <input type="hidden" name="Student[active]" value="0">
                <input type="checkbox" id="student-active" class="checkbox-inline" name="Student[active]" value="1"  <?= $model->active ? "checked" : ""; ?> > 
                <label for="student-active">Active</label> 
            </div> 
        </div>
    </div>

    
    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding" style="margin-top:20px;">
        <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => 'btn btn-block btn-info']) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
     </div>

    <?php ActiveForm::end(); ?>
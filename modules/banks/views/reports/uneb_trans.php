<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Uneb Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index hd-title" data-title="Uneb Transactions">
    <div class="row">
        <div class="col-xs-12">
            <div>
                <div class="col-sm-3 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>

                <div class="col-sm-7 col-xs-12 no-padding" ><?php echo $this->render('_search_uneb_trans', ['model' => $searchModel, 'req'=>'uneb-mno']); ?></div>
                <div class="col-sm-2 col-xs-12 no-padding">
                    <ul class="menu-list pull-right no-padding">
                        <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn btn-danger btn-sm', 'target'=>'_blank']) ?>
                        </li>
                        <li><?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="col-xs-12">
            <div style="padding: 2px; font-size: 90%">
            <span class="pull-right>">
            <i class="fa fa-hourglass-half icon-pending-color"> Pending</i>&nbsp;
            <i class="fa fa-check-circle icon-success-color"> Settled</i>&nbsp;
        </span>
            </div>
            <div class="box-body table table-responsive no-padding">
                <table class="table ">
                    <thead>
                    <tr>
                        <th></th>
                        <th class='clink'><?= $sort->link('date_created') ?></th>
                        <th class='clink'><?= $sort->link('channel_name', ['label'=>'Channel']) ?></th>
                        <th class='clink'><?= $sort->link('channel_trans_id', ['label'=>'Trans Id']) ?></th>
                        <th class='clink'><?= $sort->link('channel_depositor_phone', ['label'=>'Payer Phone']) ?></th>
                        <th class='clink'><?= $sort->link('center_name', ['label'=>'Center Name']) ?></th>
                        <th class='clink'><?= $sort->link('center_number', ['label'=>'Center Number']) ?></th>
                        <th class='clink'><?= $sort->link('center_level') ?></th>
                        <th class='clink'><?= $sort->link('schoolpay_receipt_number', ['label'=>'Receipt No.']) ?></th>
                        <th class='clink'><?= $sort->link('reconciled', ['label'=>'Status']) ?></th>
                        <th class='clink'><?= $sort->link('bank_settlement_date', ['label'=>'Date settled']) ?></th>
                        <th class='clink'><?= $sort->link('bank_settlement_reference', ['label'=>'Bank. Ref']) ?></th>
                        <th class='clink'><?= $sort->link('amount') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($dataProvider) :
                        foreach($dataProvider as $k=>$v) : ?>
                            <tr data-key="0">
                                <td><?= ($v['reconciled']) ? '<i class="fa fa-check-circle icon-success-color"></i>' : '<i class="fa fa-hourglass-half icon-pending-color"></i>'  ?></td>

                                <td><?= ($v['date_created']) ? date("Y-m-d H:i:s", strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['channel_name']) ? '<img src="'.Url::to(['/import/import/image-link', 'id'=>$v['payment_channel_logo']]).'" width="25px" /> &nbsp;'.$v['channel_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['channel_trans_id']) ? $v['channel_trans_id'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['channel_depositor_phone']) ? $v['channel_depositor_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['center_name']) ? $v['center_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['center_number']) ? $v['center_number'] : '<span class="not-set">(not set) </span>'?></td>
                                <td><?= ($v['center_level']) ? $v['center_level'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['schoolpay_receipt_number']) ? $v['schoolpay_receipt_number'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['reconciled']) ? 'Settled' : 'Pending' ?></td>
                                <td><?= ($v['bank_settlement_date']) ? date("Y-m-d H:i:s a", strtotime($v['bank_settlement_date'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['bank_settlement_reference']) ? $v['bank_settlement_reference'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>

                            </tr>
                        <?php endforeach;
                    else :?>
                        <td colspan="14">No Records found</td>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages,
            ]);?>

        </div>
    </div>



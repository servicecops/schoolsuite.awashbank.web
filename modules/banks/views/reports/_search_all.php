<?php

use app\modules\paymentscore\models\PaymentChannels;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
?>


    <?php $form = ActiveForm::begin([
        'action' => ['all-trans'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>

    <div class="col-sm-3 no-padding">
    <?= $form->field($model, 'channel')->dropDownList(
            ArrayHelper::map(PaymentChannels::find()->orderBy('channel_code')->all(), 'id', 'channel_name'), [
            'prompt'=>'Filter Channel', 
            'id'=>'channel_search',
            'class'=>'form-control input-sm' ]
            )->label(false) ?></div>

    <div class="col-sm-3 no-padding">
    <?=  $form->field($model, 'school')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(\app\modules\schoolcore\models\CoreSchool::find()->where(['bank_name'=>Yii::$app->user->identity->bank_id])->all(), 'id', 'school_name'),
                'language' => 'en',
                'size'=>'sm',
                'options' => [
                    'placeholder' => 'Filter School', 
                    'class'=>'form-control input-sm',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'class'=>'form-control input-sm',
                ],
            ])->label(false); ?>
    </div>

    <div class="col-sm-2 no-padding">
    <?= $form->field($model, 'date_from')->widget(DatePicker::className(),
            [
    'model'=>$model,
    'attribute'=>'date_from',
    'clientOptions' =>[
    'changeMonth'=> true,
    'changeYear'=> true,
    'yearRange'=>'1900:'.(date('Y')+1),
    'autoSize'=>true,
    ],
    'options'=>[
        'class'=>'form-control input-sm',
        'placeholder'=>'Date from'
     ],])->label(false) ?>
    </div>

    <div class="col-sm-2 no-padding">
    <?= $form->field($model, 'date_to')->widget(DatePicker::className(),
            [
    'clientOptions' =>[
    'changeMonth'=> true,
    'changeYear'=> true,
    'yearRange'=>'1900:'.(date('Y')+1),
    'autoSize'=>true,
    ],
    'options'=>[
        'class'=>'form-control input-sm',
        'placeholder'=>'Date To'
     ],])->label(false) ?>
        </div>
    
 	<div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
    <?php ActiveForm::end(); ?>

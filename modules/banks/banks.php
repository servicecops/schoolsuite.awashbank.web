<?php

namespace app\modules\banks;

class Banks extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\banks\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

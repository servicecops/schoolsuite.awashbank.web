<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Self Enrollment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index hd-title" data-title="Approve Self Enrolled Students">
<div class="row">
    <div class="col-xs-12">
    <div>
      <div class="col-sm-2 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> Self Enrolled</span></div>
      
      <div class="col-sm-7 col-xs-12 no-padding" ><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
      <div class="col-sm-3 col-xs-12 no-padding">
        <ul class="menu-list pull-right">
            <li><?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']) ?>
            </li>
            <li>
            <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>
            </li>
        </ul>
    </div>
    </div>
    </div>

    <div class="col-xs-12">
       <div class="box-body table table-responsive no-padding">
        <table class="table">
        <thead>
        <tr><th class='clink'><?= $sort->link('date_created') ?></th><th class='clink'><?= $sort->link('first_name') ?></th><th class='clink'><?= $sort->link('last_name') ?></th><th class='clink'><?= $sort->link('school_name')?></th><th class='clink'><?= $sort->link('class_code') ?></th><th class='clink'><?= $sort->link('student_phone') ?></th><th class='clink'><?= $sort->link('gurdian_name') ?></th><th class='clink'><?= $sort->link('gurdian_phone') ?></th><th class='clink'>&nbsp;</th></tr>
        </thead>
        <tbody>
            <?php 
            if($dataProvider) :
            foreach($dataProvider as $k=>$v) : ?>
                <tr <?= ($v['approved']) ? '' :"style='background-color:#f9dbdb;'" ?> >
                    <td><?= ($v['date_created']) ? date('Y-m-d h:i:s', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['first_name']) ? $v['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['last_name']) ? $v['last_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['student_phone']) ? $v['student_phone'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['gurdian_name']) ? $v['gurdian_name'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><?= ($v['gurdian_phone']) ? $v['gurdian_phone'] : '<span class="not-set">(not set) </span>'?></td>
                    <td><a href="javascript:;" onclick="clink('<?= Url::to(['/import/import/view', 'id'=>$v['id']]) ?>')" title="View" data-pjax="0"><span class="fa fa-search"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="javascript:;" onclick="clink('<?= Url::to(['/import/import/update', 'id'=>$v['id']]) ?>')" title="Update" data-pjax="0"><span class="glyphicon glyphicon-edit"></span></a>
                    </td>
                </tr>
            <?php endforeach; 
            else :?>
            <td colspan="8">No student found</td>
        <?php endif; ?>
        </tbody>
        </table>
        </div>
        <?= LinkPager::widget([
            'pagination' => $pages['pages'],
        ]);?>

    </div>
</div>
<?php

use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\SchoolInformation;
use app\models\Classes;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options'=>['class'=>'formprocess'],
    ]); ?>

    <div class="col-xs-3 no-padding">
        <?php 
            $url = Url::to(['/schoolcore/core-school/schoollist']);
            $selectedSchool = empty($model->schsearch) ? '' : CoreSchool::findOne($model->schsearch)->school_name;
            echo $form->field($model, 'schsearch')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size'=>'sm',
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School', 
                    'id'=>'self_enrolled_school_select',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>
            </div>
    <div class="col-xs-3 no-padding"><?= $form->field($model, 'student_class', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->dropDownList(
        ['prompt'=>'Filter class'])->label(false) ?></div>

    <div class="col-xs-2 no-padding"><?= $form->field($model, 'approved', ['inputOptions'=>[ 'class'=>'form-control input-sm']] )->dropDownList(['1'=>'Approved', '0'=>'Pending'], ['prompt'=>'Approval Status'])->label(false) ?></div>

    <div class="col-xs-3 no-padding"><?= $form->field($model, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->textInput(['placeHolder'=>'Enter Student Name'])->label(false) ?></div>
    <div class="col-xs-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
    <?php ActiveForm::end(); ?>

</div>

<?php 
$url = Url::to(['/classes/lists']);
$cls = $model->student_class;
$script = <<< JS
$("document").ready(function(){ 
    var sch = $("#self_enrolled_school_select").val();
    if(sch){
        $.get('$url', {id : sch}, function( data ) {
            $('select#studentpaymentcoderequestssearch-student_class').html(data);
            $('select#studentpaymentcoderequestssearch-student_class').val('$cls');
        });
    }

    $('body').on('change', '#self_enrolled_school_select', function(){

        $.get('$url', {id : $(this).val()}, function( data ) {
            $('select#studentpaymentcoderequestssearch-student_class').html(data);
        });
    });

  });
JS;
$this->registerJs($script);
?>

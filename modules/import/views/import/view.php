<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\import\models\StudentPaymentCodeRequests */

$this->title = $model->fullname;
$this->params['breadcrumbs'][] = ['label' => 'Student Payment Code Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="self-enrolled hd-title" data-title="<?= $model->fullname ?>">
<div class="row">
<div class="col-md-12">
    <div class="letter">
    <?php  if (\Yii::$app->session->hasFlash('failedApproval')) : ?>
        <div class="alert alert-danger">
            Student with <b> <?= $model->school_student_registration_number ?></b> Reg No already exists at <b><?= $model->schoolId->school_name ?></b>
            <br>
        </div>
    <?php endif; ?>
    <div class="row">
     <div class='col-md-7'> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class='col-md-5' style="padding-top:27px;">
    <span class="pull-right">
        <?php if(!$model->approved) : ?>
         <?= Html::a('<i class="fa fa-check"></i> Approve', ['approve', 'id' => $model->id], ['class' => 'btn btn-sm btn-info', 'data-confirm'=>'Are you sure you want to approve this student']) ?>
        <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
        <?php else: ?>
            <?= Html::a('View student', ['/student/view', 'id' => $model->approved_student_id], ['class' => 'btn btn-primary btn-sm']) ?>
        <?php endif; ?>
        <?= Html::a('<i class="fa fa-remove"></i> Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </span>
    </div>
    </div>
    <hr class="l_header">

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
             [                   
                'label' => 'School',
                'value' => $model->schoolId->school_name,
            ],
            [                   
                'label' => 'Student name',
                'value' => $model->fullname,
            ],
            [                   
                'label' => 'Class /Course',
                'value' => $model->student_class ? $model->studentClass->class_description : '--',
            ],
            [                   
                'label' => 'Reg No',
                'value' => $model->school_student_registration_number ? $model->school_student_registration_number : '--',
            ],
            [                   
                'label' => 'Gender',
                'value' => $model->gender=='M' ? 'Male' : 'Female',
            ],
            'date_of_birth',
            'date_created',
            'active:boolean',
            'student_email:email',
            'student_phone',
            'gurdian_name',
            'guardian_relation',
            'gurdian_email:email',
            'gurdian_phone',
            'date_modified',
            'allow_part_payments:boolean',
            'day_boarding',
            'registration_mode',
            'requested_by',
            'device_id',
            'device_name',
            'cell_info',
            'network_mode',
            'status',
            'approved:boolean',
        ],
    ]) ?>

</div>
</div>
</div>

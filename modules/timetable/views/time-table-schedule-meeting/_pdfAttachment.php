<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */


$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreSchoolClass;

?>


    <div style="width:100%;background-color: #e6ecff;padding:20px">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h3><?php echo $schoolDetail['school_name'] ?></h3>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Location: <?php echo $schoolDetail['region_name'] ?><br>
                District: <?php echo $schoolDetail['district_name'] ?><br>
                Address: <?php echo $schoolDetail['village'] ?><br>
            </div>
            <div class="col-md-4">
                    <div style="margin-left: 25px">
                        <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>
                    </div>
            </div>
            <div class="col-md-4">
                Tel:<?php echo $schoolDetail['phone_contact_1'] ?><br>
                MOBILE: <?php echo $schoolDetail['phone_contact_2'] ?><br>
                EMAIL:<?php echo $schoolDetail['contact_email_1'] ?>

            </div>
        </div>

       
        <div style="width:150px"></div>
        <br></br>
        <div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b> Title </b></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? Html::encode($model->title) : "--" ?></div>
        </div>
    </div>
</div><br>

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b>Meeting Day and Time</b></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->datetime) ? Html::encode($model->datetime) : "--" ?></div>
        </div>
    </div>
</div><br>


<div class="row">
    <div class="col-md-6">
        <div class="row ">

           <?php 
               $staffInfo = empty($model->chairperson) ? '' : CoreStaff::findOne($model->chairperson);
               $selectedChairperson = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '--';

           ?>
           <div class="col-lg-6 col-xs-3 profile-label"><b>Chairperson </b> </div>
           <div class="col-lg-6 col-xs-9 profile-text"><?=  $selectedChairperson ?></div>
        </div>
    </div><br>

    <div class="col-md-6">
        <div class="row ">
           <?php 
               $staffInfo = empty($model->secretary) ? '' : CoreStaff::findOne($model->secretary);
               $selectedSecretary = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '--';

           ?>
           <div class="col-lg-6 col-xs-3 profile-label"> Secretary </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?=  $selectedSecretary ?></div>
        </div>
    </div>
</div> <br>

<div class="row">
    <div class="col-md-8">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b>Agenda</b> </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->agenda) ? HtmlPurifier::process($model->agenda) : "--"  ?></div>
        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-8">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><b> Discussion Notes </b></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->discussion_notes) ? HtmlPurifier::process($model->discussion_notes) : "--"  ?></div>
        </div>
    </div>
</div>
<br>

<div class = "row">
        <div class = "col-md-8">
             <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-label"><b>Follow Up Actions</b></div>
                <div class="col-lg-6 col-xs-9 profile-text"> --- </div>
            </div>
        </div>
</div><br>


    <div class = "row">
        <div class = "col-md-8">
            <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-label"><b>Attended Meeting</b></div>
                <div class="col-lg-6 col-xs-9 profile-text">
                    <ul class="list-group">
                     <?php
                       if(!empty($attendeeList)){
                            foreach($attendeeList as $attendee){
                                echo  '<li class="list-group-item">' . $attendee . '</li>';
                            }
                        }
                     ?>
                    </ul>
                </div>
            </div>
         </div>
    </div> <br>
    
    <div class = "row">
        <div class = "col-md-8">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-label"><b>Absentee List</b></div>
                <div class="col-lg-6 col-xs-9 profile-text">
                    <ul class="list-group">
                        <?php
                            if(!empty($absenteeList)){
                              foreach($absenteeList as $absentee){
                                echo  '<li class="list-group-item">' . $absentee . '</li>';
                              }
                            }
                       ?>
                    </ul> 
                </div>
            </div> 
        </div>
    </div><br>
    <div class = "row">
        <div class="col-sm-6">
            <div class="row ">
              <div class="col-lg-6 col-xs-3 profile-label"><b>Next Meeting Date</b></div>
              <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->next_meeting_date) ? Html::encode($model->datetime) : "--" ?></div>
            </div>
        </div>
    </div>

        <div style="clear:both"></div>

        <div class="row">
            <br><br>
            <h5>Powered By</h5>
        </div>
        <div class="row">
            <?= Html::img('@web/web/img/no_bg_logo.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '65px']); ?>
        </div>
        <div class="row"
             style="height:50px; margin-top:70px;background-image: linear-gradient(180deg,#2B3990 10%,#0068AD 100%);">
            <div style="display: table;color:white;margin: auto;"><i>All rights reserved</i></div>
        </div>
    </div>
    <br>


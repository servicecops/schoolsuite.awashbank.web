<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\helpers\HtmlPurifier;
use kartik\select2\Select2;
use app\models\User;
use app\modules\planner\models\CoreStaff;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="core-school-index">
    <div class = "row letter">
        <div class = "col-md-6">
           <h3><b>Meeting Schedule</b> - <?= Html::encode($model->title) ?></h3>
        </div>
    </div>

<br></br>

<div class="row letter">
    <div class="col-sm-8">
        <div class="col profile-text"><h3><b><?= ($model->title) ? Html::encode($model->title) : "--" ?></b></h3></div>
        <div id="flash_message">
            <?php  if (\Yii::$app->session->hasFlash('sendMailError')) : ?>
               <h3 style = "color:#ff0000;"><?= \Yii::$app->session->getFlash('sendMailError'); ?></h3>
            <?php endif; ?>
            <?php  if (\Yii::$app->session->hasFlash('sendMailSuccess')) : ?>
               <h3 style = "color:#00FF00;"><?= \Yii::$app->session->getFlash('sendMailSuccess'); ?></h3>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-4">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-edit">Edit</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-envelope">Send Email</i>', ['share-schedule-meeting-minutes', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash">Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to close this meeting?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>
</div>

<br><br>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('title') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? Html::encode($model->title) : "--" ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Meeting Day and Time</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->datetime) ? Html::encode($model->datetime) : "--" ?></div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="row ">

           <?php 
               $staffInfo = empty($model->chairperson) ? '' : CoreStaff::findOne($model->chairperson);
               $selectedChairperson = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '--';

           ?>
           <div class="col-lg-6 col-xs-3 profile-label"> Chairperson </div>
           <div class="col-lg-6 col-xs-9 profile-text"><?=  $selectedChairperson ?></div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="row ">
           <?php 
               $staffInfo = empty($model->secretary) ? '' : CoreStaff::findOne($model->secretary);
               $selectedSecretary = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '--';

           ?>
           <div class="col-lg-6 col-xs-3 profile-label"> Secretary </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?=  $selectedSecretary ?></div>
        </div>
    </div>
</div> <br>

<div class="row">
    <div class="col-md-8">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Agenda </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->agenda) ? HtmlPurifier::process($model->agenda) : "--"  ?></div>
        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-md-8">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('discussion_notes') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->discussion_notes) ? HtmlPurifier::process($model->discussion_notes) : "--"  ?></div>
        </div>
    </div>
</div>
<br>

<div class = "row">
        <div class = "col-md-8">
             <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-label">Follow Up Actions</div>
                <div class="col-lg-6 col-xs-9 profile-text"> --- </div>
            </div>
        </div>
</div><br>

<?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    <div class = "row">
        <div class = "col-md-8">
            
            <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-label">Attended Meeting</div>
                <div class="col-lg-6 col-xs-9 profile-text">
                    <?php if ($model->isNewRecord): ?>
                       
                       <?php else: ?>
                           <?php
           
                               echo $form->field($model, 'attendee_list')->widget(Select2::classname(), [
                                   'options' => ['multiple' => true, 'placeholder' => 'Select those who attended'],
                                   'data' => $guestEmailAddresses
                               ])->label(''); 
           
                        ?>
                                   
                    <?php endif; ?>
                </div>
            </div>
         </div>
    </div> <br>
    
    <div class = "row">
        <div class = "col-md-8">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-label">Absentee List</div>
                <div class="col-lg-6 col-xs-9 profile-text">
                        
                    <?php if ($model->isNewRecord): ?>
                       
                    <?php else: ?>
                        <?php
                            echo $form->field($model, 'absentee_list')->widget(Select2::classname(), [
                                   'options' => ['multiple' => true, 'placeholder' => 'Select those who did not attend or absent'],
                                   'data' => $guestEmailAddresses
                            ])->label(''); 
                        ?>
                                              
                    <?php endif; ?>  
                </div>
            </div> 
        </div>
    </div> <br>

    <div class = "row">
        <div class = "col-md-8">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-label">Signatories of Meeting</div>
                <div class="col-lg-6 col-xs-9 profile-text">
                        
                    <?php if ($model->isNewRecord): ?>
                       
                    <?php else: ?>
                        <?php
                            echo $form->field($model, 'signatories')->widget(Select2::classname(), [
                                   'options' => ['multiple' => true, 'placeholder' => 'Select those who did not attend or absent'],
                                   'data' => $signatories
                            ])->label(''); 
                        ?>
                                              
                    <?php endif; ?>  
                </div>
            </div> 
        </div>
    </div><br>

    <div class="row">
        <div class="col-md-8">
           <div class = "row">
                <div class="col-lg-6 col-xs-3 profile-label">Next meeting Date</div>
                <div class="col-lg-6 col-xs-9 profile-text">
                    <?= ($model->next_meeting_date) ? Html::encode($model->next_meeting_date) : "--" ?>
                </div>  
            </div> 
        </div>
    </div>
<?php ActiveForm::end(); ?>
</div>

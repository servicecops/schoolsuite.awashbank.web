<?php
use app\modules\planner\models\CoreStaff;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */

$staffUrl = Url::to(['/planner/planner/staff-list']);

?>
<div class="container card" style="width:90%;">
    <div class = "row">
          <div class = "col-xs-12">
              <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create Schedule Meeting Minutes</h3>': '<h3><i class="fa fa-edit"></i> Edit Meeting Minutes</h3>' ?>
             <hr class="l_header"></hr>
          </div> 
    </div>
    <br></br>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'datetime', ['labelOptions' => ['style' => 'color:#041f4a', 'label'=> 'Starts']])->Input('datetime-local') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'meeting_link', ['labelOptions' => ['style' => 'color:#041f4a', 'label'=> 'Meeting Link ( Skype or Zoom or Google Meet)']])->textInput() ?>
        </div>
    </div>
    
   
    <div class="row">
        <div class="col-sm-6">
            <?php
                $staffInfo = empty($model->chairperson) ? '' : CoreStaff::findOne($model->chairperson);
                $selectedChairperson = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

                echo $form->field($model, 'chairperson')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
                    'initValueText' => $selectedChairperson, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $staffUrl,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Chairperson'); 

                
            ?>
        </div>
        <div class="col-sm-6">
            <?php

                $staffInfo = empty($model->secretary) ? '' : CoreStaff::findOne($model->secretary);
                $selectedSecretary = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

                echo $form->field($model, 'secretary')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
                    'initValueText' => $selectedSecretary, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $staffUrl,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Secretary'); 
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <?= $form->field($model, 'agenda')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                'preset' => 'basic'
                //'preset' => 'standard'
             ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <?php if ($model->isNewRecord): ?>
                       
            <?php else: ?>
                <?= $form->field($model, 'discussion_notes')->widget(CKEditor::className(), [
                           'options' => ['rows' => 4],
                           //'preset' => 'basic'
                           'preset' => 'standard'
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class = "row">
        <div class = "col-sm-8">
             <h6>Follow up Actions</h6>
        </div>
    </div><br>

    <div class = "row">
        <div class = "col-sm-12">

            <?php if ($model->isNewRecord): ?>
                       
            <?php else: ?>
                <?php

                    echo $form->field($model, 'attendee_list')->widget(Select2::classname(), [
                        'options' => ['multiple' => true, 'placeholder' => 'Select those who attended'],
                        //'initValueText' => ['8','10','12'], // set the initial display text
                        'data' => $guestEmailAddresses
                    ])->label('Attendee List'); 

                ?>
                        
            <?php endif; ?>
         </div>
         <div class = "col-sm-12">
            <?php if ($model->isNewRecord): ?>
                       
            <?php else: ?>
                  <?php
                    $selectedAbsentees = $model->absentee_list;
                    echo $form->field($model, 'absentee_list')->widget(Select2::classname(), [
                        'options' => ['multiple' => true, 'placeholder' => 'Select those who did not attend or absent'],
                        //'initValueText' => [], // set the initial display text
                        'initValueText' => $selectedAbsentees,
                        'data' => $guestEmailAddresses
                     ])->label('Absentee List'); 
                  ?>
                                   
            <?php endif; ?>   
         </div>
    </div>

    <div class = "row">
        <div class = "col-sm-12">
            <?php if ($model->isNewRecord): ?>
                       
            <?php else: ?>
                  <?php
                    $selectedSignatories = $model->signatories;
                    echo $form->field($model, 'signatories')->widget(Select2::classname(), [
                        'options' => ['multiple' => true, 'placeholder' => 'Select the digital signatures'],
                        'initValueText' => $selectedSignatories,
                        'data' => $signatories
                     ])->label('Signatories of meeting minutes'); 
                  ?>
                                   
            <?php endif; ?>   
         </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php if ($model->isNewRecord): ?>
                       
            <?php else: ?>
                <?= $form->field($model, 'next_meeting_date', ['labelOptions' => ['style' => 'color:#041f4a', 'label'=> 'Next Meeting Date']])->Input('datetime-local') ?>                                 
            <?php endif; ?>
        </div>
    </div>
    
    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

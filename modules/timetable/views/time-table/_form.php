<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;


$calendarCategory = [
    'examination'=> 'Examination','lesson'=> 'Teaching',
    'student-group' => 'Student Group', 
    'other' => 'Other'
];

$schoolId = Yii::$app->user->identity->school_id;

$classList = ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $schoolId ])->all(), 'id', 'class_code');

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="container card" style="width:90%;">
    <div class = "row">
          <div class = "col-xs-12">
              <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create TimeTable</h3>': '<h3><i class="fa fa-edit"></i> Edit Timetable</h3>' ?>
             <hr class="l_header"></hr>
          </div> 
    </div>
    <br></br>
    <p>All fields marked with * are required</p>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ->label('Title *')?>
        </div>
        <div class="col">
            <?= $form->field($model, 'description', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea()->label('Description *') ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'start_date', ['labelOptions' => ['style' => 'color:#041f4a', 'label'=> 'Begins']])->Input('date')->label('Start Date *') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'end_date', ['labelOptions' => ['style' => 'color:#041f4a', 'label'=> 'Ends']])->Input('date')->label('End Date *') ?>
        </div>
    </div>
    
   
    <div class="row">
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'calendar_category')->widget(Select2::classname(), [
                    'data' => $calendarCategory,
                    'options' => ['placeholder' => 'Select timetable category'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Timetable Category *');
            ?>
        </div>
        <div class="col-sm-6">
            <?php
                
                $url = Url::to(['/planner/planner/terms-list']);
                $selectedTerm = empty($model->term_id) ? '' : CoreTerm::findOne($model->term_id)->term_name;

                echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                    'options' => ['multiple'=>false, 'placeholder' => 'Search Terms ...'],
                    'initValueText' => $selectedTerm, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        'language' => [
                             'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Enter the Term name'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php
                $avgScheduleDurationList = [
                    '+1 hour' => '1 hour', '+45 minutes' => '45 Minutes', 
                    '+40 minutes' => '40 Minutes','+30 minutes' => '30 minutes'
                ];
                echo $form->field($model, 'average_schedule_duration')->widget(Select2::classname(), [
                    'data' => $avgScheduleDurationList,
                    'options' => ['placeholder' => 'Select average duration for single lesson '],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Average/Single schedule duration');
            ?>
        </div>
        <div class="col-sm-6">
            <?php
                

                echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                    'data' => $classList,
                    'options' => ['placeholder' => 'Select the class '],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Selected Class');
            ?>
        </div>
    </div>

    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

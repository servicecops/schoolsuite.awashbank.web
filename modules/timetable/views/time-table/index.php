<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\TimetableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Time Tables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">



    <?php

    $columns = [
        [
            'label' => 'Title',
            'value'=>function($model){
                return Html::a( $model['title'], [
                    'view', 'id' => $model['id'], 'category' => $model['calendar_category']
                ]);
                
            },
            'format'=>'raw',
        ],
        [
            'label' => 'Description',
            'value' => 'description'
        ],
        [
            'label' => 'Term',
            'value' => function($model){
                return Html::a( $model['term_id'] ? CoreTerm::findOne($model['term_id'])->term_name : "--" , ['/schoolcore/core-term/view', 'id' => $model['term_id']]);
            },
            'format' => 'raw'
        ],
        'start_date',
        'end_date',
        [
            'label' => 'Category',
            'value' => 'calendar_category'
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a( $model['title'], [
                    'view', 'id' => $model['id'], 'category' => $model['calendar_category']
                ]);
            },
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-edit"></i>', ['update', 'id' => $model['id']]);
            },
        ]
        ////
    ];
    ?>


<div style="margin-top:20px"></div>
    <div class="letter">

        <div class="row" style="margin-top:20px">

            <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        </div>
        <div class="row">

            <div class="col-md-3 col-xs-12 no-padding"></div>
            <div class="col-md-6 col-xs-12 no-padding"><?php //echo $this->render('_search', ['model' => $searchModel]); ?></div>
            <div class="col-md-3 col-xs-12 no-padding">
                <?php if (Yii::$app->user->can('rw_planner')) : ?>
                    <?= Html::a('New TimeTable', ['create'], ['class' => 'btn btn-sm btn-primary  float-right']) ?>
                <?php endif; ?>
                <div class="float-right">
                    <?php
                         echo ExportMenu::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => $columns,
                            'target' => '_blank',
                            'exportConfig' => [
                                ExportMenu::FORMAT_TEXT => false,
                                ExportMenu::FORMAT_HTML => false,
                                ExportMenu::FORMAT_EXCEL => false,
    
                            ],
                            'dropdownOptions' => [
                                'label' => 'Export Plans',
                                'class' => 'btn btn-outline-secondary'
                            ]
                        ])
                    ?>
                </div>
            </div>
        </div>
    </div>
    

    <div class="mt-3">

        <div class="float-right">

        </div>

        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
            'columns' => $columns,
            'resizableColumns'=>true,
            'responsive'=>true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>
</div>

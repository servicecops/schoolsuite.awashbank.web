<?php
 use yii\helpers\Html;
 use yii\helpers\Url;
 use kartik\select2\Select2;

 use app\models\User;
?>

<!-- <div class = "letter"> -->
<div class="row" id = "adjust-color-container" >
    <div class = "col-md-4">
        <h5>Assign Colors To Classes</h5>
        <form id = "assign-class-colors">
            <div class="form-group">
                <?php
                    echo Select2::widget([
                            'name' => 'selected_class',
                            'data' => $classList,
                            'options' => [
                                'placeholder' => 'Select the class or student group  ...',
                                'id' => 'selected_class_item'
                            ],
                    ]);

                ?>
            </div>
            <div class="form-group">
                <label for="selected_class_color">Choose the color</label>
                <input type="color" class="form-control" id="selected_class_color" aria-describedby="colorHelp" value = "#0000ff">
           </div>
           <button type="submit" class="add-item btn btn-primary">Assign Color</button>
        </form>
    </div>
    <div class="col-md-8">
         <h5>Selected Classes</h5>
         <input type = "hidden" id = "selected-category" value = "<?= $category ?>" class = "form-control">
         <div class = "color-picker-form">
               <?php
                    echo '<ul class="list-group list-group-horizontal">';
                    foreach ($colorData as $colorItem) {
                       echo '<li class="list-group-item color-list">';
                       echo '<button type="button" class="color-picker-button" style = "background-color:' .$colorItem['color'] . '">' . $colorItem['selected_class'] . '</button>';
                       echo '</li>';
                    }
                    echo '</ul>';

               ?>
         </div>
    </div>
<!-- </div> -->

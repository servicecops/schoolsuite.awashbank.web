<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\assets\FullCalendarAsset;
use app\modules\timetable\models\TimetableScheduleMeeting;
use app\modules\planner\models\CoreStaff;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\SchoolPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Timetable';
$this->params['breadcrumbs'][] = $this->title;

$availableMailingList =  ArrayHelper::map($mailingList, 'id', 'title');
$timetableCategory = Yii::$app->request->get('category');

$staffData = CoreStaff::find()->all();
$teacherList = ArrayHelper::map($staffData, 'id', function($user){
    return $user['first_name'] . ' ' . $user['last_name'];
});

?>
<div class="core-school-index">
    <div class = "row letter">
        <div class = "col-md-6">
           <h3><b>TimeTable Schedule</b> - <?= $model->title ?></h3>
        </div>
    </div>

<br></br>
    
    <div class = "letter">
        <div class = "row">
            <div class = "col-md-4">
            <?php if (! $model->class_id): ?>
               <form id = "selected-class-form"> 
                    <div class="form-group">
                        <label for="select-calendar-class">Filter classes or student groups</label>
                        <?php
                            echo Select2::widget([
                                    'name' => 'select-calendar-class',
                                    'data' => $classList,
                                    'options' => [
                                        'placeholder' => 'Select the timetable for specific  class or student group  ...',
                                        'id' => 'select-calendar-class'
                                    ],
                                ]);

                        ?>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit Class</button>
                </form>
                <?php else: ?>
                <?php endif; ?>
            </div>
            <div class = "col-md-6">
                <div class="float-right">
                   <button type="button" class="add-item btn btn-primary" id = "create-email-list"><i class="fa fa-envelope">&nbsp;Mailing List</i></button>
                   <button type="button" class="add-item btn btn-primary" id = "new-email-list"><i class="fa fa-envelope">&nbsp;New List</i></button>
                   <button type="button" class="add-item btn btn-primary" id = "show-color-picker"><i class="fa fa-comment">&nbsp;Assign Colors To Classes</i></button>
                   <button type="button" class="add-item btn btn-primary" id = "hide-color-picker"><i class="fa fa-comment">&nbsp;Close Color Picker</i></button>
                </div>
            </div>

            <div class = "col-md-12">
                <?= 
                   $this->render('_colorPicker', [
                      'category' => $model->calendar_category,
                      'colorData' => $colorData,
                      'classList' => $classList
                    ])
                ?>
            </div>

            <div class = "col-md-12">
                <?= 
                   $this->render('_attendeeList', [
                      'category' => $model->calendar_category,
                      //'classList' => $classList
                      'mailingList' => $mailingList,
                      'guestEmailAddresses' => $guestEmailAddresses
                    ])
                ?>
            </div>
        </div>
    </div>
    <div class = "letter" id = "calendar-container">
        <div class = "row">
            <div class = "col-md-12">
                <div class="text-center" id = "loading-timetable">
                    <div class="spinner-border text-primary" role="status"  style="width: 3rem; height: 3rem;">
                      <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <div id="calendar" style="height: 800px;">
                </div>

                <!-- Add Modal -->
                <div class="modal fade" id="schedule-add">
                    <div class="modal-dialog">
                        <div class="modal-content">
                          <!-- Modal Header -->
                            <div class="modal-header">
                                <h4 class="modal-title">Add Your Schedule</h4>
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                           <!-- Modal body -->
                            <div class="modal-body">
                                <form id = "add-schedule-event">
                                    <div class="form-group">
                                       <label>Title:</label>
                                       <input type="text" id = "event_title" name ="event" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                       <label>Start Date:</label>
                                       <input type="datetime-local" class="form-control" id="start_date" name="start_date" required>
                                    </div>
                                    <div class="form-group">
                                       <label>End Date:</label>
                                       <input type="datetime-local" class="form-control" id="end_date" name="end_date" required>
                                    </div>
                                    <div class="form-group">
                                       <label for="selected_class">Select class</label>
                                        <?php
                                           echo Select2::widget([
                                                'name' => 'selected_class',
                                                'value' => $model->class_id,
                                                'data' => $classList,
                                                'options' => [
                                                  'placeholder' => 'Select the class or student group  ...',
                                                  'id' => 'selected_class'
                                                ],
                                            ]);

                                        ?>
                                    </div>
                                    <div class="form-group">
                                       <label for="select_teacher">Assign Teachers</label>
                                        <?php
                                            $url = Url::to(['/planner/planner/staff-list']);

                                            echo Select2::widget([
                                                'name' => 'selected_teacher',
                                                'options' => [
                                                  'placeholder' => 'Search by firstname',
                                                  'id' => 'selected_teacher'
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true,
                                                    'minimumInputLength' => 3,
                                                    'language' => [
                                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                                    ],
                                                    'ajax' => [
                                                        'url' => $url,
                                                        'dataType' => 'json',
                                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                                    ],
                            
                                                ],
                                            ]);

                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="event_description">Description</label>
                                        <textarea class="form-control" id="event_description" rows="2"></textarea>
                                    </div>
                                    <input type = "hidden" id = "event-school-id" value = "<?= $model->school_id ?>"  class = "form-control">
                                    <input type = "hidden" id = "event-schedule-duration" value = "<?= $model->average_schedule_duration ?>"  class = "form-control">
                                    <label title="Recurring event">
                                        <input type="checkbox" id = "show-recurring-event" value="show-recurring-event" unchecked>Enable Recurring event
                                    </label>
                                   
                                    <?php

                                        $startRecurr =  '<div class="form-group">' .
                                                            '<label>Recurring/Repeat Starts:</label>' . 
                                                            '<input type="date" class="form-control" id="recurring_start_date" name="recurring_start_date">'.
                                                        '</div>';
                                        $endRecurr =    '<div class="form-group">' . 
                                                            '<label>Recurring/Repeat Ends:</label>' . 
                                                            '<input type="date" class="form-control" id="recurring_end_date" name="recurring_end_date">' . 
                                                        '</div>';
                                            
                                            echo '<div class = "recurring-event-container">';
                                            echo $startRecurr;
                                            echo $endRecurr;

                                            echo '<div class="form-group">';
                                            echo '<label for="recurring_frequency">Repeats</label>';
                                            echo Select2::widget([
                                                   'name' => 'recurring_frequency',
                                                   'data' => $recurringFrequency,
                                                   'options' => [
                                                       'placeholder' => 'when this event/schedule repeats...',
                                                       'id' => 'recurring_frequency'
                                                    ],
                                            ]);

                                            echo '</div>';
                                            echo '</div>';
                                            echo '<br></br>';
                                        
                                    ?>
                                    <button type="submit" class="btn btn-primary">Add Schedule</button>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div> <!-- end of the modal content -->
                    </div>
                </div>

                <!-- Edit Modal -->
                <div class="modal fade" id="schedule-edit">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <!-- Modal Header -->
                            <div class="modal-header">
                                 <h4 class="modal-title">Edit Your Schedule</h4>
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <!-- Modal body -->
                            <div class="modal-body">
                                <form id = "edit-schedule-event">
                                    <div class="form-group">
                                        <label>Schedule Name:</label>
                                        <input type="text"  id = "edit-event-title" class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label>Start Date:</label>
                                       <input type="datetime-local" class="form-control" id="edit-start-date" name="edit-start-date" required>
                                    </div>
                                    <div class="form-group">
                                       <label>End Date:</label>
                                       <input type="datetime-local" class="form-control" id="edit-end-date" name="edit-end-date" required>
                                    </div>
                                    <div class="form-group">
                                       <label for="edit-select-teachers">Assign Teachers</label>
                                       <?php
                                            echo Select2::widget([
                                                'name' => 'edit-select-teachers',
                                                'options' => [
                                                  'placeholder' => 'Search by firstname',
                                                  'id' => 'edit-select-teachers'
                                                ],
                                                'data' => $teacherList
                                           ]);
                                        ?>

                                    </div>
                                    <div class="form-group">
                                        <label for="edit-event-description">Description</label>
                                        <textarea class="form-control" id="edit-event-description" rows="2"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-event-location">Location/ Lecture Room</label>
                                        <input class="form-control" id="edit-event-location">
                                    </div>
                            
                                    <div class = "determine-meeting-link"></div>
                                    <button type="button" class="btn btn-primary btn-sm" id = "add-event-attendees">
                                        <i class="fa fa-users" aria-hidden="true">Attendees</i>
                                    </button><br>
                                    <div class = "form-group event-meeting">
                                        
                                        <label for="selected-mailing-list">Invite Attendees</label>
                                       
                                         <?php
                                            echo Select2::widget([
                                                'name' => 'selected-mailing-list',
                                                'value' => '',
                                                'options' => [
                                                  'placeholder' => 'Select the mailing list to send invite to its members  ...',
                                                  'id' => 'selected-mailing-list'
                                                ],
                                                'data' => $availableMailingList
                                            ]);

                                        ?>
                                    </div>
                                    <input type = "hidden" id = "edit-event-id" value = "" name = "edit-event-id" class = "form-control">
                                    <div class = "time-buttons" style = "padding:10px;">
                                        <button type="submit" class="btn btn-primary">Update Schedule</button>
                                        <button type="button" class="btn btn-danger" id = "delete-schedule-event">Delete Schedule</button>
                                    </div>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                               <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

</div>

<?php
//register the script in footer to only act after loading the elements
  FullCalendarAsset::register($this);
?>
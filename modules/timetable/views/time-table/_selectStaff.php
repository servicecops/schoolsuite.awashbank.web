<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use kartik\select2\Select2;

?>

<?php
    $url = Url::to(['/planner/planner/staff-list']);
    
    if ($action === "add-staff") {
         
        echo Select2::widget([
            'name' => 'selected_teacher',
            'options' => [
                    'placeholder' => 'Search by firstname',
                    'id' => 'selected_teacher'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'language' => [
                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                ],
                'ajax' => [
                    'url' => $url,
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                            
            ],
        ]);
    
    } else if ($action == "edit-staff") {
        # code...
    }else{

    }
    

?>
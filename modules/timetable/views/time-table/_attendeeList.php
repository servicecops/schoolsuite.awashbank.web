<?php
 use yii\helpers\Html;
 use yii\helpers\Url;
 use yii\web\JsExpression;
 use kartik\select2\Select2;
 use app\models\User;
 
?>


<div class="row" id = "email-list-container">
    <div class = "col-md-4">
        <div class="form-group">
            <label for="email-list-title">Email Address</label>
            <input type="text" class="form-control" id="new-guest-email" placeholder = "hey@gmail.com,hello@yahoo.com" aria-describedby="emailList"><br>
            <h6 class = "recent-added-emails"></h6>
            <button type="button" id = "add-guest-email" class="add-item btn btn-primary"><i class="fa fa-user">&nbsp;Add Email Address</i></button>
        </div>
        <br>
         
        <div class = "edit-mailing-list">
        <h5>Modify Email List</h5>
        <form id = "event-attendee-list">
            <div class="form-group">
                <label for="email-list-title">Title</label>
                <input type="text" class="form-control" id="email-list-title" aria-describedby="colorHelp">
            </div>
            <div class="form-group">
                <?php
                    
                    $url = Url::to(['/timetable/mailing-list/fetch-guest-email-addresses']);
                    
                    echo Select2::widget([
                        'name' => 'selected-event-attendees',
                        'value' => [ ],
                        'data' => $guestEmailAddresses,
                        'options' => [
                            'placeholder' => 'Select the guest email addresses ...',
                            'id' => 'selected-event-attendees',
                            'multiple' => true
                        ],
                    ]);


                ?>
            </div>
           <button type="submit" class="add-item btn btn-primary">Save Email List</button>
        </form>
        </div>
        
       <div class = "new-mailing-list">
            <h5> New Mailing List </h5>
            <form id = "new-mailing-list"> 
                <div class="form-group">
                  <label for="new-mailing-list-title">Title</label>
                  <input type="text" class="form-control" id="new-mailing-list-title" aria-describedby="colorHelp">
                </div>
                <div class="form-group">
                   <label for="new-mailing-list-description">Description</label>
                   <textarea class="form-control" id="new-mailing-list-description"  rows = "2" aria-describedby="colorHelp"></textarea>
                </div>
                <button type="submit" class="add-item btn btn-primary">Add Email List</button>   
            </form>
        </div>

    </div>

    <div class = "col-md-8">
        <h5>Mailing List </h5>
        <ul class="list-group mailing-list-box">
              <?php
                   foreach($mailingList as $listItem){
                       echo '<li class="list-group-item mailing-list">';
                       echo '<span>' . $listItem['title'] . '</span>&nbsp;&nbsp;&nbsp;';
                       echo '<a href = "javascript:void(0);"><i class="fa fa-edit" data-mail-id="' . $listItem['id'] .  '" data-mail-title="' . $listItem['title'] .  '">Edit</i></a>';
                       echo '</li>';
                   }
 
              ?>
        </ul>
    </div>
</div>
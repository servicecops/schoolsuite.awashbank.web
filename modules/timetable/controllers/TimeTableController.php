<?php

namespace app\modules\timetable\controllers;

use Yii;
//use yii\web\Controller;
use app\controllers\BaseController;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\timetable\models\Timetable;
use app\modules\timetable\models\TimetableSearch;
use app\modules\timetable\models\TimetableSchedule;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\schoolcore\models\CoreStudent;
//use app\modules\timetable\models\TimeTableColors;
use app\modules\timetable\models\TimetableScheduleRecurring;




if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * TimeTableController that handles different kinds of 
 * timetables ranging from exam, test and classes
 */
class TimeTableController extends BaseController
{
     

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Creates a new TimeTable model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write to the School Timetable');
        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new Timetable();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();
                //if u own the school
                $schoolId = Yii::$app->user->identity->school_id;

                $model->school_id = $schoolId;

                if( $model->save(false) ){
                     Logs::logEvent("Created New Timetable : " . $model->id, null, null);                    
                }
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed create new school time table : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }

    /**
     * Display the default school timetable list
     */

    public function actionIndex()
    {
        // if ( \Yii::$app->user->can('non_student') || !Yii::$app->user->can('r_timetable'))
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');

        $request = Yii::$app->request;
        $dataProvider;
        $searchModel = new TimetableSearch();
        $searchParams = Yii::$app->request->queryParams;
        $schoolId = Yii::$app->user->identity->school_id;
        $searchParams['school_id'] = $schoolId;
        $searchParams['active'] = true;

        $studentDetail = $this->determineIfStudent();
        if($studentDetail && $studentDetail['is_student'] == true){
            $classId = $studentDetail['class_id'];
            $searchParams['class_id'] =  $classId;
            $searchParams['student_id'] = $studentDetail['student_id'];
            $dataProvider = $searchModel->search($searchParams);

        }else{
            //school admin has access to all timetables for that school
            $dataProvider = $searchModel->search($searchParams);
        }

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);
       
    }

    /**
     * Updates an existing Timetable model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write to the School Timetable');
        }

        $model = $this->findModel($id);
        try {
            if ($model->load(Yii::$app->request->post())) {

                //$model->date_modified = date('Y-m-d H:i:s');

                if( $model->save(false) ){
                    Logs::logEvent("Updated the School Timetable: " . $model->id, null, null);                    
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }


        $model->start_date = ($model->start_date)? date('Y-m-d', strtotime($model->start_date)) : '' ;
        $model->end_date =  ($model->end_date)? date('Y-m-d', strtotime($model->end_date)) : '';

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Displays a single Timetable  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {  
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') )
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);

        $recurringFrequency = [
            'weekly' => 'Every week', 'Mon-Fri' => 'Daily(Mon- Fri)', 'daily' => 'Every Day'
        ];

        $classList = array();
        $classData = array();
        $selectedCategory = $model->calendar_category;
        $schoolId  = Yii::$app->user->identity->school_id;

        // } else  //avoid above default values because schools have different codes for classes
        if ($selectedCategory === 'student-group') {

            $classData = CoreStudentGroupInformation::find()->where(['school_id' => $schoolId])->asArray()->all();
            $classList = ArrayHelper::map($classData, 'id', 'group_name');
        }
         else { 

            $classData = CoreSchoolClass::find()->where(['school_id' => $schoolId ])->asArray()->all();
            $classList = ArrayHelper::map($classData, 'id', 'class_code'); 
       
        }

        $colorData = $this->fetchTimetableColorsForSchool($schoolId,$classData);

        $mailingList = $this->fetchMailingListForSchool($schoolId);
        $guestEmailAddresses = TimetableSchedule::fetchGuestEmailAddresses($schoolId);
        $guestEmailAddresses = ArrayHelper::map($guestEmailAddresses, 'id', 'attendee_email');

        $responseData = [
            'model' => $model,
            'classList' => $classList,
            'colorData' => $colorData,
            'mailingList' => $mailingList,
            'guestEmailAddresses' => $guestEmailAddresses,
            'recurringFrequency' => $recurringFrequency
        ];

        return ((Yii::$app->request->isAjax)) ? 
            $this->renderAjax('view',$responseData) 
                :
            $this->render('view', $responseData);
    }

    //you must override these methods to use BaseControler.
    /**
     * Creates a new Timetable model
     * @return Timetable
     */
    public function newModel()
    {
        $model = new Timetable();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return TimetableSearch
     */
    public function newSearchModel()
    {
        $searchModel = new TimetableSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create School Timetable';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    /**
     * Retrieves the students group for particular school.
     *  @return mixed
     */
    public function actionStudentGroupList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'first_name', 'last_name'])
                ->from('core_staff')
                ->where(['ilike', 'first_name', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['first_name'] ." ". $value['last_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }

    //the same applies for the classes list .. contact Gorretti about 

     //timetable schedules..
     /**
     * Creates a new TimetableSchdule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreateSchedule()
    {
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Timetable');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //date_default_timezone_set('Africa/Kampala');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new TimetableSchedule();
        $recurringModel = new TimetableScheduleRecurring();
        $outputResponse = [];
        try {
            $startDate = $request->post('starts');
            $endDate =  $request->post('ends');

            $model->title = $request->post('title');
            $model->starts = $startDate;
            $model->ends = $endDate;
            $schoolId = $request->post('school_id');
            $selectedClass =  $request->post('selected_class');
            $timetableCategory = $request->post('timetable_category');
            $recurring_start_date = $request->post('recurring_start_date');
            $recurring_end_date = $request->post('recurring_end_date');
            $recurring_frequency = $request->post('recurring_frequency');
            $averageScheduleDuration = $request->post('average_schedule_duration');
            $selectedTeacher = $request->post('selected_teacher');

            $model->category = $request->post('category');
            $model->description = $request->post('description');
            $model->selected_class = $selectedClass;
            $model->selected_teacher = $selectedTeacher;
            $model->timetable_id = $request->post('timetable_id');
            $model->created_by = Yii::$app->user->identity->getId();
            //what about getting the color for selected cl'ass 
            $selectedColor = $this->fetchColorForClass($schoolId,$selectedClass);
            $model->color = $selectedColor['finalColorId'];

            if (!empty($recurring_frequency) && !empty($recurring_start_date) && !empty($recurring_end_date)) {
                 $model->recurring = true;
            }

            //check if the time schedule duration is being respected.
            if (! $this->respectAverageScheduleDuration($startDate,$endDate,$averageScheduleDuration)) {
                $outputResponse = ['success'=>false, 'message' => 'Your schedule duration cannot be less than average schedule duration ' . $averageScheduleDuration];
                return $outputResponse;
            }

            //check if the schedule is not colliding for particular teacher.
            $recurring = $model->recurring ? true : false;
            if (TimetableSchedule::checkForCollidingSchedule($startDate,$endDate,$selectedTeacher,$recurring)) {
                $outputResponse = ['success'=>false, 'message' => 'Your lesson schedule is colliding with another lesson for particular teacher'];
                return $outputResponse; 
            }


            if ($model->validate()) {
                if ( $model->save(false)) {
                    //save the recurring events for the lessons.
                    if (!empty($recurring_start_date) && !empty($recurring_end_date)) {
                        $recurringModel->schedule_id = $model->id; 
                        $recurringModel->start_recurring = $recurring_start_date;
                        $recurringModel->end_recurring = $recurring_end_date;
                        //$recurringModel->recurring_day_of_week = "1"; //repeats on Monday
                        $recurringModel->recurring_day_of_week = $recurring_frequency;
                        $recurringModel->save(false);
                    }

                    Logs::logEvent("Created timetable schedule: " . $model->id, null, null);
                }

                $transaction->commit();

            }

            $outputResponse = ['success'=>true, 'message' => array(
                'id' => $model->id,
                'title' => $model->title,
                'timetable_id'=> $model->timetable_id,
                'color' => $selectedColor['finalColor'],
                //'created_by' => User::findOne($model->created_by)->username,
                //'date_created'=> Yii::$app->formatter->asDateTime(new \DateTime())
                'recurring_event' => $this->determineDayOfRecurring($model->recurring,$model->id,$model->starts)
            )];
            
            return  $outputResponse;  

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to create new timetable schedule : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to add the timetable schedule'];
            return $outputResponse;
        }

    }


    private function convertJSToPHPDate($dateInMs){
          //$dateInMs = $dateInMs/1000;
          $seconds = ceil($dateInMs/1000);
          $dateStr = date("Y-m-d H:i:s", $seconds);
          return $dateStr;
    }

    private function convertToISODateFormat($dateStr){
        $datetime = new \DateTime($dateStr);
        $datetime->format(DATE_ATOM);
        return $datetime;
    }


    /**
     * Updates  a TimetableSchedule model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionUpdateSchedule()
    {
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Timetable');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        //date_default_timezone_set('Africa/Kampala');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;

        $outputResponse = ['success' => false, 'message' => 'Failed to update schedule'];

         try {
                
                $scheduleId = $request->post('schedule_id');
                $model = $this->findTimeTableScheduleModel($scheduleId);

                $model->title = HtmlPurifier::process($request->post('title'));
                $model->starts = $request->post('starts');
                $model->ends = $request->post('ends');

                $model->date_modified = date('Y-m-d H:i:s');
                $model->mailing_list_id = $request->post('mailing_list_id');
                //$model->meeting_link_id =  $request->post('meeting_link');
                $location = $request->post('location');
                if (!empty($location)) {
                    $model->location = $location;
                }

                if (!empty($model->id) && !empty($model->title) && !empty($model->starts) && !empty($model->ends) ) {
                    $startDate = $model->starts;
                    $endDate = $model->ends;
                    $selectedTeacher = $model->selected_teacher;
                    // if (! $this->respectAverageScheduleDuration($startDate,$endDate,$averageScheduleDuration)) {
                    //     $outputResponse = ['success'=>false, 'message' => 'Your schedule duration cannot be less than average schedule duration ' . $averageScheduleDuration];
                    //     return $outputResponse;
                    // }
        
                    //check if the schedule is not colliding for particular teacher.
                    $recurring = $model->recurring ? true : false;
                    // if (TimetableSchedule::checkForCollidingSchedule($startDate,$endDate,$selectedTeacher,$recurring)) {
                    //     $outputResponse = ['success'=>false, 'message' => 'Your lesson schedule is colliding with another lesson for particular teacher'];
                    //     return $outputResponse; 
                    // } //u do not this  teacher conflict constraint for update

                    if ( $model->save()) {
                        Logs::logEvent("Updated timetable schedule: " . $model->id, null, null);
                    }

                    $transaction->commit();

                    $outputResponse = ['success'=>true, 'message' => array(
                        'id' => $model->id,
                        'title' => $model->title,
                        'timetable_id'=> $model->timetable_id,
                        'created_by' => User::findOne($model->created_by)->username,
                        'meeting_link' => $model->meeting_link_id,
                        'mailing_list_id'=> $model->mailing_list_id
                        //'date_created'=> Yii::$app->formatter->asDateTime(new \DateTime())
                    )];

                } else {
                    
                    $outputResponse = ['success'=> false, 'message'=> 'Failed to validate your schedule input'];
                }
                
            return  $outputResponse;  

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to update the timetable schedule : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to update the timetable schedule'];
            return $outputResponse;
        }

    }


    /**
     * Deletes an existing TimetableSchedule model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteSchedule($id)
    {   
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Timetable');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        try {

            //$this->findCommentModel($id)->delete();
            //only the owner is allowed to delete the comment.
            $userId = Yii::$app->user->identity->getId();

            $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when deleting schedule'];
            
            //just archive comments so that only the PM can view it.
            $model = $this->findTimeTableScheduleModel($id);
            if ($model->created_by == $userId) {
                $model->active = false;
                
                if ($model->save(false)) {
                   Logs::logEvent("Archived the timetable schedule: " . $model->id, null, null);
                   $outputResponse = ['success'=>true, 'message' => 'Deleted the timetable schedule succesful']; 
                }
            } else {
                  
                $outputResponse = ['success'=>false, 'message' => 'Failed to delete the  timetable schedule because you are not author']; 
            }
        
            
            return $outputResponse;

        } catch (\Exception $e) {
             
            $model_error = $e->getMessage();

            Logs::logEvent("Failed to delete  the schedule  : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to delete the schedule'];

            return $outputResponse;
        }
       
    }


    /**
     * Finds the TimetableSchedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimetableSchedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTimeTableScheduleModel($id)
    {  
        if (($model = TimetableSchedule::findOne($id)) !== null) {
             return $model;
        } else {
            throw new NotFoundHttpException('The requested timetable schedule does not exist.');
        }
    }

    /**
     * Retrieve timetable schedules for the timetable
     */

    public function actionLoadTimeTableSchedules($timetableId)
    {   
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') ){
        //     throw new ForbiddenHttpException('No permissions to view timetable schedules.');

        // }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $outResponse = [
            'success' => false,
            'message' => 'Failed to retrieve the timetable schedules data for the timetable'
        ];

        $request = Yii::$app->request;
        
        $timetableId = $request->get('timetableId');
        $selectedClass = $request->get('selectedClass');
        $timetableSchedules = array();

        //show specific lessons for particular logged in student
        $username = Yii::$app->user->identity->username;
        if ($username && is_numeric($username)) {
            $studentDetail = CoreStudent::findByUsername($username);
            if ($studentDetail && $studentDetail->class_id) {
                $selectedClass =  $studentDetail->class_id;
            }
        }


        if ($selectedClass == 'all-classes') {

            $timetableSchedules = TimetableSchedule::find()
                                        ->where(['timetable_id' => $timetableId, 'active'=> true ])
                                        ->orderBy('id')
                                        ->asArray()
                                        ->all();
        } else {
             
            $timetableSchedules = TimetableSchedule::find()
                                        ->where([
                                            'timetable_id' => $timetableId, 
                                            'active'=> true,
                                            'selected_class'=> $selectedClass

                                        ])
                                        ->orderBy('id')
                                        ->asArray()
                                        ->all();

        }
        
        $resultData = array();

        foreach ($timetableSchedules as $schedule) {
            $selectedColor = $this->fetchColorForTimetableSchedule($schedule['color']);
            array_push($resultData, [
                'id' => $schedule['id'],
                'title' => $schedule['title'],
                'category' => $schedule['category'],
                'start' => $schedule['starts'],
                'end' => $schedule['ends'],
                'color' => $selectedColor,
                'description' => $schedule['description'],
                'selected_teacher' => $schedule['selected_teacher'],
                'meeting_link' => $schedule['meeting_link_id'],
                'mailing_list_id' => $schedule['mailing_list_id'],
                'recurring_event' => $this->determineDayOfRecurring($schedule['recurring'],$schedule['id'],$schedule['starts']),
                'location' => $schedule['location']
            ]);
        }

        $outResponse = [
            'success' => true,
            'schedules' => json_encode($resultData)
        ];
        return $outResponse;
    }

    /**
     * Fetch the  saved colors for the specific school_id
     * @param integer $schoolId
     * @return mixed
     */
    private function fetchTimetableColorsForSchool($schoolId, $classList)
    {

        $finalColors = array();
        $connection = Yii::$app->db;

        //check if the project has any saved color codes. 
        $sqlQuery = "SELECT * FROM timetable_colors WHERE school_id=:school_id";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':school_id', $schoolId);
        $colorData = $sqlQuery->queryAll(); 

        if (count($colorData)> 0) {
            foreach ($colorData as $colorItem) {
                $item = $this->filterClassColors($classList, $colorItem['selected_class']);
                //$item = $this->getArrayFiltered('id',$colorItem['selected_class'], $classList);
                //Yii::trace($item); //colors + selected classes
                if (!empty($item)) {
                    array_push($finalColors, [
                        'id' => $colorItem['id'],
                        'selected_class' => $item,
                        'color' => $colorItem['color']
                    ]);
                }
            }
        }

        return $finalColors;

    }

    /**
     * Filter the color arrays for particular selected class.
     * 
     */
    private function filterClassColors($classList = [], $searchItem){
        $selected_class = '';
        foreach ($classList as $class) {
            if ($class['id'] == $searchItem) {
                if (isset($class['group_name'])) {
                   $selected_class = $class['group_name'];
                }

                if (isset($class['class_code'])) {
                    $selected_class = $class['class_code'];
                }
            }
        }

        return $selected_class;

    }


    /**
     * Fetch the  saved colors for the specific class
     * @param integer $schoolId
     * @param integer $
     * @return mixed
     */
    private function fetchColorForClass($schoolId, $selectedClass)
    {
        $finalColor = "";
        $finalColorId = "";
        $connection = Yii::$app->db;

        //check if the project has any saved color codes. 
        $sqlQuery = "SELECT * FROM timetable_colors WHERE school_id=:school_id AND selected_class=:selected_class";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':school_id', $schoolId);
        $sqlQuery->bindValue(':selected_class', $selectedClass);
        $colorData = $sqlQuery->queryOne(); 

        if (!empty($colorData)) {
            $finalColorId = $colorData['id']; //for easier update when changing colors for c
            $finalColor = $colorData['color'];

        }else{
             $randomColor = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
             $finalColor = $randomColor; 
        }

        //return $finalColor;
        $result = array(
            "finalColorId" => $finalColorId,
            "finalColor" =>$finalColor
        );
        return $result;

    }

    /**
     * Fetch the  saved colors for the timetable schedule
     * @param integer $colorId
     * @return mixed
    */
    private function fetchColorForTimetableSchedule($colorId){
         
        $finalColor = "";
        $connection = Yii::$app->db;

        //check if the project has any saved color codes. 
        $sqlQuery = "SELECT * FROM timetable_colors WHERE id=:id";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':id', $colorId);
        $colorData = $sqlQuery->queryOne(); 

        if (!empty($colorData)) {
            $finalColor = $colorData['color'];

        }else{
             $randomColor = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
             $finalColor = $randomColor; 
        }

        return $finalColor;
    }

    /**
     * Adjust the colors of the activity progress
     * @return mixed
     */
    public function actionAdjustTimetableColors()
    {   
        if (!Yii::$app->user->can('rw_timetable') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Timetable');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $userId = Yii::$app->user->identity->getId();

        $request = Yii::$app->request;

        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when updating teh colors'];
        

        $selectedClass = $request->post('selected_class');
        $selectedColor = $request->post('selected_color');
        $timetableId = $request->post('timetable_id');
        
        $timetableDetail = $this->findModel($timetableId);

        if (empty($timetableDetail->school_id)) {
            $outputResponse = ['success'=>false, 'message' => 'Failed to adjust the colors for the timetable'];
            return $outputResponse;
        }
        
        $schoolId = $timetableDetail->school_id;
      
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try{
                     
            $sql1 = "SELECT * FROM timetable_colors WHERE school_id=:school_id AND selected_class=:selected_class";
            $sql1 = $connection->createCommand($sql1);
            $sql1->bindValue(':school_id', $schoolId);
            $sql1->bindValue(':selected_class', $selectedClass);
            $rows = $sql1->queryAll(); 
                    
            if (count($rows)> 0 ) {
                // UPDATE (table name, column values, condition)
               //These methods will properly quote table and column names and bind parameter values.
                Yii::$app->db->createCommand()->update('timetable_colors', 
                        [
                            'color' => $selectedColor
                        ],
                        ['selected_class' => $selectedClass, 'school_id'=> $schoolId]
                    )->execute();

            } else {
    
                $connection->createCommand()->insert('timetable_colors', [
                            'selected_class' => $selectedClass,
                            'school_id' => $schoolId,
                            'color' => $selectedColor,
                            'created_by' => $userId
                    ])->execute();
            }

            $transaction->commit();
            Logs::logEvent("Adjusted the color of the classes for timetable: " . $schoolId, null, null);
            $outputResponse = ['success'=>true, 'message' => 'Adjusted the colors of the timetable successful']; 

        }catch(\Exception $e){

            $transaction->rollBack();
            $model_error = $e->getMessage();

            Logs::logEvent("Failed to adjust the colors of timetable  : ", $model_error, null);
            $outputResponse = ['success'=>false, 'message' => 'Failed to adjust teh activity colors'];

        }

          
        return $outputResponse;
       
    }

    /**
     * Filtering array of objects
     * 
     */
    private function getArrayFiltered($aFilterKey, $aFilterValue, $array) {
        $filtered_array = array();
        foreach ($array as $value) {
            if (isset($value->$aFilterKey)) {
                if ($value->$aFilterKey == $aFilterValue) {
                    $filtered_array[] = $value;
                }
            }
        }
    
        return $filtered_array;
    }

    /**
     * Fetch mailing list for particular school
     * @param integer $schoolId
     * @return mixed
     */
    private function fetchMailingListForSchool($schoolId){
        $query = new Query;
        $mailingList = array();

        $query->select(['id', 'title', 'description', 'school_id', 'email_list'])
                 ->from('timetable_mailing_list')
                 ->where(['school_id' => $schoolId ]);
                 
        $command = $query->createCommand();
        $data = $command->queryAll();
 
        $queryResult = array_values($data);


        return $queryResult;
    }

    // /**
    //  * Retrieves the guest Email Addreses for creating email list
    //  *  @return mixed
    //  */
    // private function fetchGuestEmailAddresses($schoolId)
    // {
    //         $query = new Query;

    //         $query->select(['id', 'attendee_email', 'timetable_id', 'school_id'])
    //               ->from('timetable_guest_emails')
    //               ->where(['school_id' => $schoolId]);

    //         $command = $query->createCommand();
    //         $data = $command->queryAll();

    //         $staffData = ArrayHelper::map($data, 'id', 'attendee_email');

    //     return $staffData;
        
    // }

    /**
     * Determine the day of recurring event
     * @param boolean $recurring
     * @param integer $scheduleId
     * @return mixed
     */
    private function determineDayOfRecurring($recurring,$scheduleId,$scheduleTime){
        $repeatDetail = array();
        if ($recurring == true) {
            $recurringDetail = TimetableScheduleRecurring::find()->where(['schedule_id' => $scheduleId])->asArray()->all();
            if (!empty($recurringDetail)) {
               $recurringDetail = $recurringDetail[0];
               $recurringDay = date('w', strtotime($scheduleTime));
                $repeatEventTime = array();
                $dayOfWeek = $recurringDetail['recurring_day_of_week'];
                if ($dayOfWeek == 'weekly') {
                    $repeatEventTime = array($recurringDay);
                } else if ($dayOfWeek == 'Mon-Fri') {
                    $repeatEventTime = array(1,2,3,4,5);
                } else if ($dayOfWeek == 'daily') {
                    $repeatEventTime = array();
                }
               
               $repeatDetail = array(
                   'id' => $recurringDetail['id'],
                   'schedule_id' => $recurringDetail['schedule_id'],
                   'start_recurring' => $recurringDetail['start_recurring'],
                   'end_recurring' => $recurringDetail['end_recurring'],
                   'recurring_day_of_week' => $repeatEventTime
               );
            }
        }

        return $repeatDetail;

    }

    /**
     * calculate the time difference in hours
     * @param string $startdate
     * @param string $enddate
     * @return float
     */
    private function differenceInHours($startdate,$enddate){
        $difference = abs($startdate - $enddate)/3600;
        return round($difference,2); //round to 2 dps
    }

    /**
     * check if the lesson schedule respects average duration
     * @param string $startdate
     * @param string $enddate
     * @param string $avgDuration
     * @return bool
     */
    private function respectAverageScheduleDuration($startdate,$enddate,$avgDuration){
        $startdate = strtotime($startdate);
        $enddate = strtotime($enddate); 
        $timeDifference = $this->differenceInHours($startdate,$enddate);
        $lessonDivider = 1; // 1/1 = 1
        if ($avgDuration == '+1 hour') {
            $lessonDivider = 1; // 1/1 = 1
        } else if ($avgDuration == '+40 minutes') {
            $lessonDivider = 0.666667; // 40 mins = 0.6667 hours
        } else if ($avgDuration == '+30 minutes') {
            $lessonDivider = 0.5; // 30 minutes = 0.5 hours
        }else if ($avgDuration == '+45 minutes') {
            $lessonDivider = 0.75; // 45 minutes = 0.75 hours
        }

        return $timeDifference && ($timeDifference >= $lessonDivider);
    }


    /**
     * Determine whether logged in  user is student
     */
    private function determineIfStudent(){
        $result = [
           'is_student'=> false,
           'student_id' => 2,
           'class_id' => 1
        ];

        $username = Yii::$app->user->identity->username;
        if ($username && is_numeric($username)) {
            $studentDetail = CoreStudent::findByUsername($username);
            if ($studentDetail && $studentDetail->id) {
                $result['is_student'] = true;
                $result['student_id'] = $studentDetail->id;
                $result['class_id'] = $studentDetail->class_id;
            }
        }

        return $result;
    }


}
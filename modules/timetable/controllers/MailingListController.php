<?php

namespace app\modules\timetable\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\modules\timetable\models\Timetable;
use app\modules\timetable\models\TimetableSchedule;
use app\modules\logs\models\Logs;



/**
 * MailingListController that handles different kinds of 
 * events related to the mailing list
 */
class MailingListController extends Controller
{
     

        /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Add the guest email addresses to school for timetable members
     * @return mixed
     */
    public function actionAddGuestEmailAddresses()
    {   
        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Timetable');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $userId = Yii::$app->user->identity->getId();

        $request = Yii::$app->request;

        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when adding the email addresses to timetable'];
        
        $selectedColor = $request->post('selected_color');
        $timetableId = $request->post('timetable_id');
        $emailList  = $request->post('emailList');
        $schoolId = $request->post('school_id');

        if (empty($emailList) || empty($timetableId) || empty($schoolId)) {
            $outputResponse = ['success'=>false, 'message' => 'Your email addresses or schoolId or timetableId is empty'];
        }

        //clean the email list.provided
        $splitEmailList = preg_split('/[\ \n\,]+/', $emailList);
        $cleanedEmailList = array();
        foreach($splitEmailList as $email){
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                array_push($cleanedEmailList, $email);
            }
        }
        
        $insertedEmailAddress = array();
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try{
              
            foreach ($cleanedEmailList as $guestAttendee) {

                $sql1 = "SELECT * FROM timetable_guest_emails WHERE school_id=:school_id AND attendee_email=:attendee_email";
                $sql1 = $connection->createCommand($sql1);
                $sql1->bindValue(':school_id', $schoolId);
                $sql1->bindValue(':attendee_email', $guestAttendee);
                $rows = $sql1->queryAll();

                if (count($rows)> 0 ) {
                    // UPDATE (table name, column values, condition)
                   //These methods will properly quote table and column names and bind parameter values.
                     Yii::trace("Email address is already registered");
                } else {
        
                    $connection->createCommand()->insert('timetable_guest_emails', [
                                'attendee_email' => $guestAttendee,
                                'school_id' => $schoolId,
                                'timetable_id' => $timetableId,
                                'created_by' => $userId
                    ])->execute();
                    
                    array_push($insertedEmailAddress,$guestAttendee);
                }
                
            }
            
            $transaction->commit();
            Logs::logEvent("Added the guest email addresses to timetable: " . $schoolId, null, null);
            $outputResponse = [
                'success'=>true, 
                'message' => 'Added the guest email addresses to  timetable successful',
                'insertedEmailList' => $insertedEmailAddress
            ]; 

        }catch(\Exception $e){

            $transaction->rollBack();
            $model_error = $e->getMessage();

            Logs::logEvent("Failed to add the guest email addresses to schools  : ", $model_error, null);
            $outputResponse = ['success'=>false, 'message' => 'Failed to add the guest email addresses'];

        }

          
        return $outputResponse;
       
    }

    /**
     * Retrieves the guest Email Addreses for creating email list
     *  @return mixed
     */
    public function actionFetchGuestEmailAddresses($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'attendee_email', 'timetable_id', 'school_id'])
                ->from('timetable_guest_emails')
                ->where(['ilike', 'attendee_email', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    //'text' => $value['first_name'] ." ". $value['last_name']
                    'text' => $value['attendee_email']
                ));
            }
            //$staffData = ArrayHelper::map($data, 'id', 'attendee_email');

            $out['results'] = $staffData;

        }
        return $out;
    }


    /**
     * Add the mailing list to the school
     * @return mixed
     */
    public function actionAddMailingList()
    {   
        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Timetable');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $userId = Yii::$app->user->identity->getId();

        $request = Yii::$app->request;

        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when adding mailing list'];
        
        $title = $request->post('title');
        $description = $request->post('description');
        $timetableId = $request->post('timetable_id');
        //$emailList  = $request->post('emailList');
        $schoolId = $request->post('school_id');
        
        if (empty($title)) {
            $outputResponse = ['success'=>false, 'message' => 'Your title for mailing list is empty']; 
        }

        if (empty($schoolId || empty($timetableId))) {
            $outputResponse = ['success'=>false, 'message' => 'Your schoolId or timetableId  is empty and it is required'];
        }

        
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try{
                

            $sql1 = "SELECT * FROM timetable_mailing_list WHERE school_id=:school_id AND title=:title";
            $sql1 = $connection->createCommand($sql1);
            $sql1->bindValue(':school_id', $schoolId);
            $sql1->bindValue(':title', $title);
            $rows = $sql1->queryAll();

            if (count($rows)> 0 ) {

                $outputResponse = [
                    'success'=> false, 
                    'message' => 'That mailing list is already registered in the system'
                ];

            } else {
        
                $connection->createCommand()->insert('timetable_mailing_list', [
                                'title' => $title,
                                'description' => $description,
                                'school_id' => $schoolId,
                                'timetable_id' => $timetableId,
                                'created_by' => $userId
                ])->execute();

                $mailingListId = $connection->getLastInsertID();
                     
                $transaction->commit();
                Logs::logEvent("Added the mailing list: " . $title, null, null);
                $outputResponse = ['success' => true, 'message' => $mailingListId ];
                    
            }
                
            
        }catch(\Exception $e){

            $transaction->rollBack();
            $model_error = $e->getMessage();

            Logs::logEvent("Failed to add the mailing list  to schools  : ", $model_error, null);
            $outputResponse = ['success'=>false, 'message' => 'Failed to add the mailing list to the school'];

        }

          
        return $outputResponse;
       
    }

    /**
     * Add/remove  the email addresses from particular mailing list
     * @return mixed
     */
    public function actionModifyMailingList()
    {   
        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $userId = Yii::$app->user->identity->getId();

        $request = Yii::$app->request;

        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when updating teh colors'];
        
        
        $emailList = $request->post('emailList');
        $timetableId = $request->post('timetable_id');
        $mailingListId = $request->post('mailingListId');
        $title = $request->post('title');
        $schoolId =  $request->post('school_id');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try{

               //These methods will properly quote table and column names and bind parameter values.
            Yii::$app->db->createCommand()->update('timetable_mailing_list', 
                        [
                            'title' =>  $title,
                            'email_list' =>  $emailList
                        ],
                        ['id' => $mailingListId, 'school_id'=> $schoolId]
                    )->execute();
            
            $transaction->commit();
            Logs::logEvent("Modify the mailing  list for school: " . $schoolId, null, null);
            $outputResponse = ['success'=>true, 'message' => 'Adjusted the mailing list for school successful']; 

        }catch(\Exception $e){

            $transaction->rollBack();
            $model_error = $e->getMessage();

            Logs::logEvent("Failed to adjust the mailing list for schools  : ", $model_error, null);
            $outputResponse = ['success'=>false, 'message' => 'Failed to modify the mailing list'];

        }

          
        return $outputResponse;
       
    }

    /**
     * Fetch email addresses for particular mailingList
     * @return mixed
     */
    public function actionEmailAddressesForSelectedMailingList(){

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
        $request = Yii::$app->request;

        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when fetching the emails for mailing list'];
         
        $mailingListId = $request->get('id');

        // $query = new Query;
        // $mailingList = array();

        // $query->select(['id', 'title', 'description', 'school_id', 'email_list'])
        //          ->from('timetable_mailing_list')
        //          ->where(['id' => $mailingListId ]);
        //          //u need inner join for guest_email_id
                 
        // $command = $query->createCommand();
        // $data = $command->queryAll();
 
        // $queryResult = array_values($data);
        $queryResult = $this->fetchEmailIdsForMailingList($mailingListId);
        
        $outputResponse = [
            'success' => true,
            'message' => json_encode($queryResult)
        ];

        return $outputResponse;
    }

    /**
     * Send Email Reminders to the event attendee
     */
    public function  actionSendEventReminders(){

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    
        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when sending the email alerts'];

        //fetch all the mailing list id  in the timetable schedules.
        $timetables = TimetableSchedule::find()->where(['active'=>true ])
                                                ->orderBy('id')
                                                ->asArray()
                                                ->all();
        if (count($timetables) > 0) {
            $sentEmailCounts = 0;
            foreach ($timetables as $timetable){
                if (!empty($timetable['mailing_list_id'])) {
                      //then also check for days.. or 2 hours reminder 
                    $mailingListId = $timetable['mailing_list_id'];
                    $mailingListRow = $this->fetchEmailIdsForMailingList($mailingListId);

                    if (count($mailingListRow) > 0) {
                        if (!empty($mailingListRow[0]['email_list']) ) {
                            $emailList = json_decode($mailingListRow[0]['email_list']);
                            //what about checking if we can go ahead to send email alert
                            $sendEventNotification = $this->determineScheduleDueTime($timetable['starts']);
                            if ($sendEventNotification['alertMember'] == true) {
                                $emailAlertMessage = $sendEventNotification['alertText'];
                                foreach($emailList as $emailId){
                                    $emailDetail = $this->fetchEmailAddress($emailId);
                                    if (!empty($emailDetail)) {
                                       Yii::trace($emailDetail['attendee_email']);
                                       $this->sendEmailEventReminder($emailDetail['attendee_email'], $emailAlertMessage);
                                       $sentEmailCounts+=1;
                                    }
                                }
                            }else{
                                Yii::trace("Not yet ready to send email schedule");
                            }
                        }
                    }

                }   
            }

            $outputResponse = [
                "success" => true,
                "message" => "The email  due alerts for scheduled events  have been sent out successfully",
                "sentEmailCounts" => $sentEmailCounts
            ];
        }

        return $outputResponse;
    }

    /**
     * Fetch the emailIds for the selected mailing list
     * @param integer $mailingListId
     * @return mixed
     */
    private function fetchEmailIdsForMailingList($mailingListId){
      
        $query = new Query;
        $mailingList = array();

        $query->select(['id', 'title', 'description', 'school_id', 'email_list'])
                 ->from('timetable_mailing_list')
                 ->where(['id' => $mailingListId ]);
                 //u need inner join for guest_email_id
                 
        $command = $query->createCommand();
        $data = $command->queryAll();
 
        $queryResult = array_values($data);
        
        return $queryResult;
    }

    /**
     * Determine the whether timetable schedule event is close
     */
    private function determineScheduleDueTime($scheduleDate){
        date_default_timezone_set("Africa/Kampala");
        $alertMember = false;
        $alertText = "Your have a future event happening at " . $scheduleDate . "in  5 days time";
        $twoHoursInFuture = new \DateTime("+2 hours");
        $fiveHoursInFuture = new \DateTime("+5 hours");
        $tomorrowDate = new \DateTime("tomorrow");
        $twoDaysInFuture = new \DateTime("+2 days");
        $fiveDaysInFuture = new \DateTime("+5 days");

        $currentDate = new \DateTime($scheduleDate);

        $alertText = "You have been invited to attend the event at " . $scheduleDate ;
        
        if ($currentDate < $twoHoursInFuture) {
            $alertMember = true;
            //$alertText = "You have been invited to attend the event at " . $scheduleDate . "in 2 hours time";
            $alertText = $alertText . "scheduled to happen in 2 hours time";
        }else{
              if ($currentDate <  $fiveHoursInFuture) {
                  $alertMember = true;
                  //$alertText = "You have been invited to attend the event at " . $scheduleDate . "in 5 hours time";
                  $alertText = $alertText . "scheduled to happen in 5 hours time";
              }else{
                    if ($currentDate < $tomorrowDate) {
                        $alertMember = true;
                       $alertText = $alertText . "scheduled to happen tomorrow";
                    }else{
                        if ($currentDate < $twoDaysInFuture) {
                            $alertMember = true;
                            $alertText = $alertText . "scheduled to happen in 2 days";
                        }else{
                            //stop at 5 days in future.
                            if ($currentDate < $fiveDaysInFuture) {
                                $alertMember = true;
                                $alertText = $alertText . "scheduled to happen in 5 days";
                            }
                        }
                    }
              }
        }

        $result = array(
            "alertMember" => $alertMember,
            "alertText"=> $alertText
        );


    }

    /***
     * Fetch a single email addresse for the selected id
     */
    private function fetchEmailAddress($emailId){

        $connection = Yii::$app->db; 
        $sqlQuery = "SELECT * FROM timetable_guest_emails WHERE id=:id";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':id', $emailId);
        $colorData = $sqlQuery->queryOne();
        return $colorData;
    }

     /**
     * Send the email address to the members
     * @param string $emailAddress
     * @return mixed
     */
    private function sendEmailEventReminder($emailAddress,$message)
    {
        return \Yii::$app->mailer->compose()
                ->setTo($emailAddress)
                ->setFrom('support@schoolsuite.ug')
                ->setSubject('School Event Reminder')
                ->setHtmlBody($message)
                ->send(); 
    }




}





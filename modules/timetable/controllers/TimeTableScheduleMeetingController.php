<?php

namespace app\modules\timetable\controllers;

use Yii;
//use yii\web\Controller;
use app\controllers\BaseController;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use kartik\mpdf\Pdf;


use app\modules\timetable\models\TimetableScheduleMeeting;
//use app\modules\timetable\models\Timetable;
use app\modules\timetable\models\TimetableSchedule;
use app\modules\studentreport\models\SignatureSealMedia;
use app\modules\planner\models\CoreStaff;
use app\modules\logs\models\Logs;




/**
 * MeetingScheduleController that handles different kinds of 
 * events related to the meeting schedule
 */
class TimeTableScheduleMeetingController extends BaseController
{
     

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


     //you must override these methods to use BaseControler.
    /**
     * Creates a new Timetable model
     * @return Timetable
     */
    public function newModel()
    {
        $model = new TimetableScheduleMeeting();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return TimetableSearch
     */
    public function newSearchModel()
    {
        $searchModel = new TimetableScheduleMeeting();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create Schedule Meeting';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }


    /**
     * Display the meeting schedule
     */
    public function actionIndex(){
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view schools plans.'); 
            
        $request = Yii::$app->request;

        return ($request->isAjax) ? $this->renderAjax('index', [
                'model' => 'hello schedule... '
            ]) :
            $this->render('index', [
                'model' => 'hello goldsoft'
            ]);
        
    }


    /**
     * Creates a new TimeTableScheduleMeeting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write to the School Timetable');
        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new TimetableScheduleMeeting();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();
                //if u own the school
                $schoolId = Yii::$app->user->identity->school_id;

                //$model->school_id = $schoolId;
                $timetableScheduleId = $request->get('id');
                if (empty($timetableScheduleId)) {
                    throw new ForbiddenHttpException('You cannot create meeting schedule without the timetableScheduleId');
                }

                //$model->timetable_id = $timetableId;
                $model->timetable_schedule_id = $timetableScheduleId;
                Yii::trace("timetable schedule id");
                Yii::trace($timetableScheduleId);
               

                if( $model->save(false) ){
                     Logs::logEvent("Created New Timetable Schedule Meeting : " . $model->id, null, null);                    
                }

                $timetableScheduleModel = $this->findTimetableScheduleModel($timetableScheduleId);
                $timetableScheduleModel->meeting_link_id = $model->id;
                $timetableScheduleModel->save(false);
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed  to create scheduled meeting : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }


    /**
     * Displays a single TimetableScheduleMeeting  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {  
        if (!Yii::$app->user->can('r_timetable'))
            throw new ForbiddenHttpException('No permissions to view schools timetable');
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $reformattedModel = $this->reformatScheduleMeetingModel($id);

        $responseData = [
            'model' => $reformattedModel['model'],
            'guestEmailAddresses' => $reformattedModel['guestEmailAddresses'],
            'signatories' => $reformattedModel['signatories']
        ];

        return ((Yii::$app->request->isAjax)) ? 
            $this->renderAjax('view',$responseData) 
                :
            $this->render('view', $responseData);
    }


    /**
     * Updates an existing TimetableScheduleMeeting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write to the School Timetable');
        }

        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post())) {
 
                $postedFormData = Yii::$app->request->post();
                $attendeeList = $postedFormData['TimetableScheduleMeeting']['attendee_list'];
                $absenteeList = $postedFormData['TimetableScheduleMeeting']['absentee_list'];
                $signatories  = $postedFormData['TimetableScheduleMeeting']['signatories'];
                
                Yii::trace($postedFormData);

                $model->attendee_list = json_encode($attendeeList);
                $model->absentee_list = json_encode($absenteeList);
                $model->signatories = json_encode($signatories);

                $model->date_modified = date('Y-m-d H:i:s');

                if( $model->save(false) ){
                    Logs::logEvent("Updated the Schedule Meeting: " . $model->id, null, null);                    
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }

        $reformattedModel = $this->reformatScheduleMeetingModel($id);
        
        return $this->render('update', [
            'model' => $reformattedModel['model'],
            'guestEmailAddresses' => $reformattedModel['guestEmailAddresses'],
            'signatories' => $reformattedModel['signatories']
        ]);
    }


     /**
     * Finds the TimetableSchedule model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimetableSchedule the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTimetableScheduleModel($id)
    {   

        if (($model = TimetableSchedule::findOne($id)) !== null) {
             return $model;
        } else {
            throw new NotFoundHttpException('The requested timetable schedule does not exist.');
        }
    }


    /**
     * Sends schedule meeting minutes  to user
     *
     * @return bool whether the email was sent
    */
    public function actionShareScheduleMeetingMinutes($id){
        
      
        $reformattedModel = $this->reformatScheduleMeetingModel($id);
        $emailAddressesList = $this->collectEmailAddressesForMeetingMinutes($id);

        $reformattedModel['staffList'] = $emailAddressesList['staffList'];
        $reformattedModel['attendeeList'] = $emailAddressesList['attendeeList'];
        $reformattedModel['absenteeList'] = $emailAddressesList['absenteeList'];

        //prepare the pdf attachment for sending.
        $schoolDetail = array(
            'school_name' => 'Kihaka Primary School',
            'region_name' => 'Central',
            'district_name' => 'Kampala',
            'village' => 'Central Division',
            'phone_contact_1' => '075434556',
            'phone_contact_2' => '07823444565',
            'contact_email_1' => 'hello@schoolsuite.com'
        );

        $reformattedModel['schoolDetail'] = $schoolDetail;
        $pdf = new Pdf();
        $mpdf = $pdf->api; //fetches mpdf api

        $mpdf->WriteHTML($this->renderPartial('_pdfAttachment', $reformattedModel )); //pdf is a name of view file responsible for this pdf document
        $pdfPath = $mpdf->Output('', 'S'); 



        //staffList.
        if (count($emailAddressesList['staffList']) > 0 ) {
            foreach($emailAddressesList['staffList'] as $staffEmail){
                $sendStaff = $this->sendEmailMessage($reformattedModel,$staffEmail, $pdfPath);
            }
        }

        //attendeeList.
        if (count($emailAddressesList['attendeeList']) > 0 ) {
            foreach($emailAddressesList['attendeeList'] as $attendeeEmail){
                $sendStaff = $this->sendEmailMessage($reformattedModel,$attendeeEmail, $pdfPath);
            }
        }
        

         //absenteeList.
         if (count($emailAddressesList['absenteeList']) > 0 ) {
            foreach($emailAddressesList['absenteeList'] as $absenteeEmail){
                $sendStaff = $this->sendEmailMessage($reformattedModel,$absenteeEmail, $pdfPath);
            }
        }

        
        $errorMessage = 'Failed to share the schedule meeting minutes via email.';
        $successMessage = 'Meeting minutes have been shared via email successful';
        // \Yii::$app->session->setFlash('sendMailError', $errorMessage);
        \Yii::$app->session->setFlash('sendMailSuccess', $successMessage);
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Reformat the schedule meeting model for view template
     * @param integer $id
     * @return mixed
     */
    private function reformatScheduleMeetingModel($id){
        
        $model = $this->findModel($id);

        $schoolId  = Yii::$app->user->identity->school_id;

        $guestEmailAddresses = TimetableSchedule::fetchGuestEmailAddresses($schoolId);
        $guestEmailAddresses = ArrayHelper::map($guestEmailAddresses, 'id', 'attendee_email');
        $signatureData = SignatureSealMedia::find()->where(['school_id' => $schoolId])->asArray()->all();
        
        $signatories = ArrayHelper::map($signatureData, 'id', 'individual_name');
         
        //default values for attendee and absentee list
        $model->attendee_list = json_decode($model->attendee_list);
        $model->absentee_list = json_decode($model->absentee_list);
        $model->datetime = date('Y-m-d\TH:i', strtotime($model->datetime));
        $model->signatories = json_decode($model->signatories);
        $model->next_meeting_date = ($model->next_meeting_date)? date('Y-m-d\TH:i', strtotime($model->next_meeting_date)) : '' ;

        $responseData = array(
            'model' => $model,
            'guestEmailAddresses' => $guestEmailAddresses,
            'signatories' => $signatories
        );
        return $responseData;
    }

    /**
     * Pick the email addresses for sharing the  schedule meeting minutes
     * @param integer $id
     * @return mixed
     */
    private function collectEmailAddressesForMeetingMinutes($id){
        $emailAddresesList = [];
        $selectedAttendeeList = [];
        $selectedAbsenteeList = [];

        $model = $this->findModel($id);

        $schoolId  = Yii::$app->user->identity->school_id;

        $guestEmailAddresses = TimetableSchedule::fetchGuestEmailAddresses($schoolId);
        $guestEmailAddresses = array_values($guestEmailAddresses);

        $chairpersonEmail = empty($model->chairperson) ? '' : CoreStaff::findOne($model->chairperson)->email_address;
        if (!empty($chairpersonEmail)) {
            array_push($emailAddresesList, $chairpersonEmail);
        }

        $secretaryEmail = empty($model->secretary) ? '' : CoreStaff::findOne($model->secretary)->email_address;
        if (!empty($secretaryEmail)) {
            array_push($emailAddresesList, $secretaryEmail);
        }

        //attendeelist.
        $attendeeList = json_decode($model->attendee_list);
        if (count($attendeeList) > 0) {
            foreach ($attendeeList as $attendee) {
                //find email addreses for attendee in this $guestEmailAddresses 
                $foundElement = array_filter($guestEmailAddresses, function($item) use ($attendee){ //$attendee closure
                    return $item['id'] == $attendee;
                });
                if ($foundElement) {
                    $attendeeDetail  = array_values($foundElement);
                    if (isset($attendeeDetail[0]['attendee_email'])) {
                        # code...
                        array_push($selectedAttendeeList, $attendeeDetail[0]['attendee_email']);
                    }
                }
            }
            
        }
        //absenteeList.
        $absenteeList = json_decode($model->absentee_list);
        if (count($absenteeList) > 0 ) {
            foreach($absenteeList as $absentee){
                //find email addreses for absentee in this $guestEmailAddresses
                $foundElement = array_filter($guestEmailAddresses, function($item) use ($absentee){ //$attendee closure
                    return $item['id'] == $absentee;
                });
                if ($foundElement) {
                    $absenteeDetail  = array_values($foundElement);
                    if (isset($attendeeDetail[0]['attendee_email'])) {
                        # code...
                        array_push($selectedAbsenteeList, $absenteeDetail[0]['attendee_email']);
                    }
                } 
            }
        }


        return array(
            "staffList" => $emailAddresesList,
            "attendeeList" => $selectedAttendeeList,
            "absenteeList" => $selectedAbsenteeList
        );
    }

    /**
     * Send the email message to the attendees of the minutes
     * @param mixed $reformattedModel 
     * @param string $email
     * @return boolean
     */
    private function sendEmailMessage($reformattedModel, $email, $pdfPath){
        $meetingModel = $reformattedModel['model'];
        $meetingTitle = $meetingModel->title;
        return  Yii::$app->mailer
                        ->compose()
                        ->attachContent($pdfPath, ['fileName' => 'Meeting minutes for '. $meetingTitle . '.pdf',   'contentType' => 'application/pdf'])
                            ->setFrom([Yii::$app->params['supportEmail'] => 'Schoolsuite'])
                            ->setTo($email)
                            ->setSubject('Meeting Minutes for ' . $meetingTitle)
                            ->send();
    }
    

}
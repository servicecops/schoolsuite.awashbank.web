<?php

namespace app\modules\timetable\models;

use Yii;

/**
 * This is the model class for table "timetable".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property int $school_id It must belong to particular school
 * @property string $calendar_category
 * @property int|null $term_id
 * @property string|null $average_schedule_duration
 * @property string|null $schedule_break
 * @property int|null $class_id
 * @property int $created_by
 * @property string $created_at
 */
class Timetable extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timetable';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'start_date', 'end_date','school_id', 'calendar_category', 'created_by'], 'required'],
            [['title', 'description','calendar_category', 'average_schedule_duration', 'schedule_break'], 'string'],
            [['start_date', 'end_date', 'created_at'], 'safe'],
            [['school_id', 'created_by'], 'default', 'value' => null],
            [['school_id', 'created_by', 'term_id', 'class_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'calendar_view' => 'Calendar View',
            'school_id' => 'School ID',
            'calendar_category' => 'Calendar Category',
            'term_id' => 'Term',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }
}

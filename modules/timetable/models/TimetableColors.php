<?php

namespace app\modules\timetable\models;

use Yii;

/**
 * This is the model class for table "timetable_colors".
 *
 * @property int $id
 * @property string $selected_class
 * @property string $color
 * @property int $school_id
 * @property int $created_by
 * @property string|null $date_created
 * @property string|null $date_modified
 */
class TimetableColors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timetable_colors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['selected_class', 'color', 'school_id', 'created_by'], 'required'],
            [['selected_class', 'color'], 'string'],
            [['school_id', 'created_by'], 'default', 'value' => null],
            [['school_id', 'created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'selected_class' => 'Selected Class',
            'color' => 'Color',
            'school_id' => 'School ID',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
}

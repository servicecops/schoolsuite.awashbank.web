<?php

namespace app\modules\timetable\models;

use Yii;

/**
 * This is the model class for table "timetable_schedule_recurring".
 *
 * @property int $id
 * @property int $schedule_id
 * @property string $start_recurring
 * @property string $end_recurring
 * @property string|null $recurring_day_of_week
 * @property string $date_created
 */
class TimetableScheduleRecurring extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timetable_schedule_recurring';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['schedule_id', 'start_recurring', 'end_recurring'], 'required'],
            [['schedule_id', 'recurring_day_of_week'], 'default', 'value' => null],
            [['schedule_id'], 'integer'],
            [['start_recurring', 'end_recurring', 'date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_id' => 'Schedule ID',
            'start_recurring' => 'Start Recurring',
            'end_recurring' => 'End Recurring',
            'recurring_day_of_week' => 'Recurring Day Of Week',
            'date_created' => 'Date Created',
        ];
    }
}

<?php

namespace app\modules\timetable\models;

use Yii;
use app\models\User;
use app\modules\planner\models\CoreStaff;

/**
 * This is the model class for table "timetable_schedule_meeting".
 *
 * @property int $id
 * @property string $title
 * @property string|null $datetime
 * @property int $chairperson
 * @property int|null $secretary
 * @property string $agenda
 * @property string|null $discussion_notes
 * @property int|null $action_points
 * @property string|null $attendee_list
 * @property string|null $absentee_list
 * @property string|null $signatories
 * @property int $timetable_schedule_id
 * @property int $meeting_link
 * @property string|null $next_meeting_date
 * @property int|null $created_by
 * @property string|null $date_created
 * @property string|null $date_modified
 */
class TimetableScheduleMeeting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timetable_schedule_meeting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'chairperson', 'agenda', 'timetable_id'], 'required'],
            [['title', 'agenda', 'discussion_notes', 'meeting_link'], 'string'],
            [['datetime', 'date_created', 'date_modified', 'next_meeting_date'], 'safe'],
            //[['chairperson', 'secretary', 'action_points', 'created_by', 'timetable_id'], 'default', 'value' => null],
            [['chairperson', 'secretary', 'action_points', 'created_by', 'timetable_schedule_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'datetime' => 'Datetime',
            'chairperson' => 'Chairperson',
            'secretary' => 'Secretary',
            'agenda' => 'Agenda',
            'discussion_notes' => 'Discussion Notes',
            'action_points' => 'Action Points',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
            'timetable_id' => 'Timetable ID',
        ];
    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [


            'title',
            'datetime',

            [
                'label' => 'Chaired  By',
                'value' => function ($model) {
                    $staff = CoreStaff::findOne($model->secretary);
                    return $staff ? $staff->first_name . ' ' . $staff->last_name : "";
                }
            ],
            [
                'label' => 'Secretary',
                'value' => function ($model) {
                    $staff = CoreStaff::findOne($model->secretary);
                    return $staff ? $staff->first_name . ' ' . $staff->last_name : "";
                }
            ],
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    $user = User::findOne($model->created_by);
                    $user = $user ? $user->username : "";
                    return $user;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = TimetableScheduleMeeting::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

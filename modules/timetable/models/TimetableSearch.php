<?php

namespace app\modules\timetable\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * TimetableSearch represents the model behind the search form of `app\modules\timetable\models\Timetable`.
 */
class TimetableSearch extends Timetable
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','start_date', 'end_date', 'calendar_category'], 'safe'],
            //[['subject_id', 'id', 'created_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $schoolId = $params['school_id']? $params['school_id'] : '';
        $activeStatus = $params['active']? $params['active']:true;
        $query = Timetable::find()
                       ->where(['school_id'=> $schoolId , 'active'=> $activeStatus ]);
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (isset($params['class_id']) && isset($params['student_id'])) { //
            $classId = $params['class_id'];
            $query->andFilterWhere(['=', 'class_id', $classId ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'created_at' => $this->created_at,
            'id' => $this->id,
            'calendar_category' => $this->calendar_category,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->title]);


        return $dataProvider;
    }

}
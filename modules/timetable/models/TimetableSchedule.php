<?php

namespace app\modules\timetable\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\data\Pagination;

/**
 * This is the model class for table "timetable_schedule".
 *
 * @property int $id
 * @property string $title
 * @property string $starts
 * @property string $ends
 * @property string|null $category
 * @property int $timetable_id
 * @property bool $active
 * @property int|null $color
 * @property string|null $description
 * @property int|null $selected_class
 * @property int|null $selected_teacher
 * @property string|null $location
 * @property int $created_by
 * @property string $date_created
 * @property int|null $mailing_list_id
 * @property string|null $meeting_link_id
 * @property boolean|null $recurring 
 * @property string|null $date_modified
 */
class TimetableSchedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timetable_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'starts', 'ends', 'timetable_id', 'created_by'], 'required'],
            [['title', 'category','description'], 'string'],
            [['starts', 'ends', 'date_created', 'date_modified'], 'safe'],
            [['timetable_id', 'created_by'], 'default', 'value' => null],
            [['timetable_id', 'created_by','selected_teacher', 'mailing_list_id', 'color', 'meeting_link_id', 'selected_class'], 'integer'],
            [['active', 'recurring'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'starts' => 'Starts',
            'ends' => 'Ends',
            'category' => 'Category',
            'timetable_id' => 'Timetable ID',
            'is_read_only' => 'Is Read Only',
            'color' => 'Color',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }


    /**
     * Retrieves the guest Email Addreses for creating email list
     *  @return mixed
     */
    public static function fetchGuestEmailAddresses($schoolId)
    {
            $query = new Query;

            $query->select(['id', 'attendee_email', 'timetable_id', 'school_id'])
                  ->from('timetable_guest_emails')
                  ->where(['school_id' => $schoolId]);

            $command = $query->createCommand();
            $staffData = $command->queryAll();

            // // $staffData = ArrayHelper::map($data, 'id', 'attendee_email');
            // $staffData = $data;

        return $staffData;
        
    }


    /**
     * Generate the timeschedule  data for the timetable report
     * @param array $params
     * @return mixed
    */
    public  static function generateTimeScheduleReportData($params){

        $query = new Query;

        if (!empty($params['teacher_id'])) {
            $query->select([
                'ts.id','ts.title','ts.timetable_id','ts.starts', 'ts.ends','ts.selected_class','ts.location',
                'ts.selected_teacher','cl.class_code','cl.school_id', 'tim.term_id', 'tim.calendar_category',
                'tim.average_schedule_duration'
            ])
            ->from('timetable_schedule ts')
            ->innerJoin('core_school_class cl', 'cl.id=ts.selected_class')
            ->innerJoin('timetable tim', 'tim.id=ts.timetable_id')
            ->andWhere(['ts.selected_class' => $params['class_id']])
            ->andFilterWhere(['cl.school_id' => $params['school_id'] ])
            ->andFilterWhere(['tim.term_id' => $params['term_id']])
            ->andFilterWhere(['tim.calendar_category' => $params['timetable_type'] ])
            ->andFilterWhere([ 'ts.selected_teacher' => $params['teacher_id'] ]);

        } else {
            
            $query->select([
                'ts.id','ts.title','ts.timetable_id','ts.starts', 'ts.ends','ts.selected_class','ts.location',
                'ts.selected_teacher','cl.class_code','cl.school_id', 'tim.term_id', 'tim.calendar_category',
                'tim.average_schedule_duration'
            ])
            ->from('timetable_schedule ts')
            ->innerJoin('core_school_class cl', 'cl.id=ts.selected_class')
            ->innerJoin('timetable tim', 'tim.id=ts.timetable_id')
            ->andWhere(['ts.selected_class' => $params['class_id']])
            ->andFilterWhere(['cl.school_id' => $params['school_id'] ])
            ->andFilterWhere(['tim.term_id' => $params['term_id']])
            ->andFilterWhere(['tim.calendar_category' => $params['timetable_type'] ]);
           
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('ts.id DESC');

        $timetable_data = $query->all(Yii::$app->db);

        return $timetable_data;   
    }

    /**
     * Sort the months by name..
     * @param array $monthList
     * @return mixed
     */
    public function getSortedMonth($monthList = [])
    {
	    // take empty array to store sorted month name	
	     $sortedNameList = array();
	
	    // take empty array to store month name in numbers
	     $monthNumberList = array();
	
	    // Iterate the loop till end of month list array and inside this, converted month name in number.
	    $maxLength = count($monthList);
	    for($i = 0; $i < $maxLength; $i++){
		  // convert month name to number and store it into $monthNumberList array.
		    //if (isset($i)) {
              $monthnumber = date('m',strtotime($monthList[$i]));
              array_push($monthNumberList,$monthnumber);	
            //}
	    }
	
	    // sorted, month number in accending order..e.g. 01,02,03....12
	    sort($monthNumberList);
	
	    // Iterate the loop till end of month and fetch data one by and converted into month name
	    for($j = 0; $j < $maxLength; $j++){	
		   // fetch month number
		   $monthNum  = $monthNumberList[$j];
	
		   // converted month number to month name 
		   $dateObj   = \DateTime::createFromFormat('!m', $monthNum);
		   $monthName = $dateObj->format('F'); 
		
		   // store month name in sortedNameList array.
		   array_push($sortedNameList,$monthName);			
	    }
	
	    return $sortedNameList;

    }

    /**
     * Generate timetable schedule data for the teacher collision data
     * @param array $params
     * @return mixed
     */
    public static function generateTimeScheduleDataForTeacherCollision($params){

        $query = new Query;
        if (!empty($params['class_id'])) {
            $query->select([
                'ts.id','ts.title','ts.timetable_id','ts.starts', 'ts.ends','ts.selected_class',
                'ts.selected_teacher','cl.class_code','cl.school_id', 'tim.term_id', 'tim.calendar_category',
                'ts.location', 'tim.average_schedule_duration'
            ])
            ->from('timetable_schedule ts')
            ->innerJoin('core_school_class cl', 'cl.id=ts.selected_class')
            ->innerJoin('timetable tim', 'tim.id=ts.timetable_id')
            ->andWhere(['ts.selected_class' => $params['class_id'] ])
            ->andFilterWhere(['cl.school_id' => $params['school_id'] ])
            ->andFilterWhere(['tim.term_id' => $params['term_id'] ])
            ->andFilterWhere(['tim.calendar_category' => $params['timetable_type'] ])
            ->andFilterWhere([ 'ts.selected_teacher' => $params['teacher_id'] ]);
        }else{

            $query->select([
                'ts.id','ts.title','ts.timetable_id','ts.starts', 'ts.ends','ts.selected_class',
                'ts.selected_teacher','cl.class_code','cl.school_id', 'tim.term_id', 'tim.calendar_category',
                'ts.location','tim.average_schedule_duration'
            ])
            ->from('timetable_schedule ts')
            ->innerJoin('core_school_class cl', 'cl.id=ts.selected_class')
            ->innerJoin('timetable tim', 'tim.id=ts.timetable_id')
            ->andFilterWhere(['cl.school_id' => $params['school_id'] ])
            ->andFilterWhere(['tim.term_id' => $params['term_id'] ])
            ->andFilterWhere(['tim.calendar_category' => $params['timetable_type'] ])
            ->andFilterWhere([ 'ts.selected_teacher' => $params['teacher_id'] ]);
            
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('ts.id DESC');

        $timetable_data = $query->all(Yii::$app->db);

        return $timetable_data;
       
    }

    /**
     * Check for colliding timeschedules for particular teacher
     * @param string $startDate
     * @param string $endDate
     * @param integer $teacherId
     * @return mixed
     */
    public static  function checkForCollidingSchedule($startDate,$endDate,$teacherId,$recurring){
        $isColliding = false;

        $query = new Query();
        $query->select([
           's.id', 's.title', 's.starts', 's.ends','s.selected_teacher','s.recurring', 's.selected_class'
        ])
        ->from('timetable_schedule s')
        ->andWhere(['s.selected_teacher' => $teacherId ]);
        //Add on the classId too..  but later..
        //->andFilterWhere(['s.selected_class' => $class_id ]);
        $command = $query->createCommand();
        $scheduleList = $command->queryAll();

        if (count($scheduleList) > 0) {
            //filter schedules for the particular day.
            $selectedInputDay = date("D", strtotime($startDate));

            $foundLessonList = array_filter($scheduleList, function($item) use ($selectedInputDay){ //$dayDate closure
                $selectedDay = date("D", strtotime($item['starts']));
                return $selectedDay == $selectedInputDay;
            });

            if ($foundLessonList) {

                foreach ($foundLessonList as $lesson) {
                    
                    if ($recurring == true) {
                        $lessonStart = date("H:i", strtotime($lesson['starts']));
                        $lessonEnd = date("H:i", strtotime($lesson['ends']));
                        //Add on today date to simplify comparison.
                        $lessonStart =  '2021-12-15 ' . $lessonStart;
                        $lessonEnd = '2021-12-15 ' . $lessonEnd;
                        //input date for new schedule
                        $startDateTime = date("H:i", strtotime($startDate));
                        $endDateTime = date("H:i", strtotime($endDate));

                        $startDateTime = '2021-12-15 ' . $startDateTime;
                        $endDateTime = '2021-12-15 ' . $endDateTime;
                        
                        if ( (new \DateTime($startDateTime) >= new \DateTime($lessonStart) ) && ( new \DateTime($endDateTime) <= new \DateTime($lessonEnd)) ) {
                            $isColliding = true;
                            Yii::trace($lesson);
                        }

                    }else{
                        //input date for the new schedule
                        $startDateTime  = new \DateTime($startDate);
                        $endDateTime  = new \DateTime($endDate);
                        //lesson from the db.
                        $lessonStartTime  = new \DateTime($lesson['starts']);
                        $lessonEndTime  = new \DateTime($lesson['ends']);
                        
                        if ($startDateTime >= $lessonStartTime && $endDateTime <= $lessonEndTime) {
                            $isColliding = true;
                            Yii::trace($lesson);
                        }

                    }

                }
            }
        }

        return $isColliding;    
    }


}

<?php
namespace app\modules\timetable;

class TimeTableModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\timetable\controllers';
    public function init() {
        parent::init();
    }
}
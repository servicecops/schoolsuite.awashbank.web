<?php
namespace app\modules\planner;

class PlannerModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\planner\controllers';
    public function init() {
        parent::init();
    }
}

<?php

namespace app\modules\planner\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\controllers\BaseController;
use app\modules\logs\models\Logs;
use app\modules\planner\models\SchoolPlanActivity;
use app\modules\planner\models\SchoolPlan;
use app\modules\planner\models\SchoolPlanSubActivity;
use app\modules\planner\models\SchoolPlanActivityStaffAssociation;

use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use app\modules\planner\models\SchoolPlanActivitySearch;
use app\modules\planner\models\SchoolPlanSubActivitySearch;
use app\modules\planner\models\SchoolPlanChanges;
use yii\db\Query;
use yii\web\JsExpression;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * PlannerController implements the CRUD actions for SchoolPlan model.
 */
class PlannerActivityController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Deletes an existing SchoolPlanActivity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }

        $model = $this->findModel($id);
        $userId = Yii::$app->user->identity->getId();


        if ($model->created_by == $userId) {
            $subActivities  = SchoolPlanSubActivity::find()
                                        ->where(['plan_activity_id' => $id])
                                        ->asArray()
                                        ->all();

            $budgetList = SchoolPlanBudget::find()
                                          ->where(['activity_id' => $id])
                                          ->asArray()
                                          ->all();
            
            if (count($subActivities) > 0 || count($budgetList) > 0 ) {
                throw new ForbiddenHttpException('You cannot delete the activity if it is still attached to subActivities or budgets');  
            }
            
            $model->active = false;
            
            if ($model->save(false)) {
               Logs::logEvent("Deleted the school plan activity: " . $model->id, null, null);
               return $this->redirect(['index']);
            }
        
        } else {
            throw new ForbiddenHttpException('No permission to delete the subactivity');
        }
        
    }

    /**
     * Creates a new SchoolPlanActivity model
     * @return SchoolPlanActivity
     */
    public function newModel()
    {
        $model = new SchoolPlanActivity();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return SchoolPlanActivitySearch
     */
    public function newSearchModel()
    {
        $searchModel = new SchoolPlanActivitySearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create School Plan Activity';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionLists($id = 0)
    {
        if(!$id) $id = 0;
        $countSchoolPlans = SchoolPlanActivity::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'plan_name', '__ARCHIVE__'])
            ->count();

        $schoolPlans = SchoolPlanActivity::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'plan_name', '__ARCHIVE__'])
            ->orderBy('plan_name')
            ->all();

        Yii::trace($schoolPlans);

        if ($countSchoolPlans > 0) {
            echo "<option value=''> Select School Plan </option>";
            foreach ($schoolPlans as $myPlan) {
                echo "<option value='" . $myPlan->id . "'>" . $myPlan->plan_name . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }


    /**
     * Retrieve the activities and subactivities for the Gantt chart
     */

    public function actionGanttChartData($planId)
    {   
        // if (!Yii::$app->user->can('r_planner')){
        //     throw new ForbiddenHttpException('No permissions to view schools plans.');

        // }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $outResponse = [
            'success' => false,
            'message' => 'Failed to retrieve the activity data for the school plan'
        ];


        $request = Yii::$app->request;
        
        $planId = $request->get('planId');

        $activityRows = SchoolPlanActivity::find()
                                ->where(['plan_id' => $planId, 'active'=>true ])
                                ->orderBy('id')
                                ->asArray()
                                ->all();
        
        $gnattRows = array();
        $subRows = array();
        $resultData = array();

        $colors = $this->fetchActivityColorForPlanner($planId);

        foreach ($activityRows as $activity  ) {

            $activityId = $activity['id'];
            $activityName = $activity['title'];

            $activityTime = $this->convertPHPToJSDateTime($activity['expected_start_date'],$activity['expected_end_date']); 
            $activityProgress = ($activity['progress']/100);

            $activityColor = $this->assignActivityProgressColors($activityProgress, $colors);

            array_push($gnattRows, [
                'id' => $activityId,
                'name' => $activityName,
                'start' =>  $activityTime['startDate'],
                'end' => $activityTime['endDate'],
                'completed' => $activityProgress,
                'color'=> $activityColor
            ]);


            //what about the sub activities.
            $subActivities = SchoolPlanSubActivity::find()
                                ->where(['plan_activity_id' => $activityId , 'active'=>true ])
                                ->orderBy('id')
                                ->asArray()
                                ->all();

            if (count($subActivities) > 0) {
                foreach ($subActivities as $value) {
                    
                    $subActivityTime = $this->convertPHPToJSDateTime($value['expected_start_date'],$value['expected_end_date']); 
                    array_push($subRows, [
                        //id must be unique for subtasks thus use title instead
                        'id'=> $value['title'], 
                        'name' => $value['title'],
                        'parent' => $activityName,
                        'start' => $subActivityTime['startDate'],
                        'end' => $subActivityTime['endDate']
                    ]);
                }
            }
        }

        //$resultData = array_merge($gnattRows, $subRows);
        $resultData = array(
            'activities' => $gnattRows,
            'subActivities' => $subRows
        );

        $outResponse = [
            'success' => true, 
            'message' => json_encode($resultData)
        ];

       return $outResponse;

    }

    /**
     * Displays a single SchoolPlanActivity model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) 
    {   
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }


        $model = $this->findModel($id);
        if (Yii::$app->user->can('r_planner', ['sch' => $model->id])) { 

            $searchModel = new SchoolPlanSubActivitySearch();
            $searchQueryParams = array(
                "activity_id" => $id
            );
            
            $dataProvider = $searchModel->searchNewIndex($searchQueryParams);

            //what about the staff details for school actvities.
            $staffList = self::fetchStaffInformation($id);
            
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
                'model' => $model, 'staffList' => $staffList ]) 
                         :
                $this->render('view', 
                    [
                        'model' => $model, 
                        'subActivities' => $dataProvider, 
                        'staffList'=> $staffList 
                ]);


        } else {
            throw new ForbiddenHttpException('No permissions to view school Activity plans');
        }
    }


    /**
     * Creates a new SchoolPlanActivity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_planner')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolPlanActivity();
        $activityStaffModel = new SchoolPlanActivityStaffAssociation();
        $planId = $request->get('id');

        
        if ($model->load(Yii::$app->request->post())) {

            $respectTimeRange = $this->checkIfActivityRespectsPlanTimeRange($model,$planId);

            if ($respectTimeRange == false) {
                $errorMessage = 'The activity cannot start before or  end after the school plan time period.';
                \Yii::$app->session->setFlash('failedActivity', $errorMessage);

                $res = ['model' => $model, 'activityStaffModel'=>$activityStaffModel];
                return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
                
            }

            try {

                $data = Yii::$app->request->post();

                $assocData = $data['SchoolPlanActivityStaffAssociation']['staff_id'];
                $userId = Yii::$app->user->identity->getId();
                $model->created_by =  $userId;
                $model->plan_id = $planId;


                if( $model->save(false) ){
                    
                    Logs::logEvent("Created New School Plan Activity: " . $model->id, null, null); 
                     
                    //checking if association tbl has post data
                    if ($activityStaffModel->load(Yii::$app->request->post())){
                          //remove duplicate classisd
                        $assocData = array_unique($assocData);  
                        if($assocData){
                            foreach($assocData as $staffId){
                                $newStaffModel = new SchoolPlanActivityStaffAssociation();

                                $newStaffModel->activity_id = $model->id;
                                $newStaffModel->created_by = $userId;
                                $newStaffModel->staff_id = $staffId;
                                $newStaffModel->save(false);
                            }
                        }
                       
                      
                    }
                }
                 
                $transaction->commit();
                //$res =['activityStaffModel'=>$activityStaffModel, ];
                return $this->redirect(['view','id' => $model->id]);

            } catch (\Exception $e) {
               Yii::trace($e);
               $transaction->rollBack();
               $model_error = $e->getMessage();
               Logs::logEvent("Failed to new school plan : ", $model_error, null);
            }

        }

        $res = ['model' => $model, 'activityStaffModel'=>$activityStaffModel];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
    
    }

    /**
     * Updates an existing SchoolPlanActivity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('rw_planner')) {
            $req = Yii::$app->request;

            if (empty(Yii::$app->user->identity) ) {
             
                return \Yii::$app->runAction('/site/login');
            }
            
            $model = $this->findModel($id);

            $model->date_modified = date('Y-m-d H:i:s');

            $session = Yii::$app->session;

            $previousActivityModel = $session->get('beforeEditActivity');
            if (!empty($previousActivityModel)) {
                $model->version = (float)($previousActivityModel->version) + 0.1;
            }

            //render the edit form if not saving data
            $model->expected_start_date = date('Y-m-d', strtotime($model->expected_start_date));
            $model->expected_end_date = date('Y-m-d', strtotime($model->expected_end_date));

            $model->actual_start_date = ($model->actual_start_date)? date('Y-m-d', strtotime($model->actual_start_date)) : '' ;
            $model->actual_end_date =  ($model->actual_end_date)? date('Y-m-d', strtotime($model->actual_end_date)) : '';
            
             //what about the staff details for specific activityId
            $staffList = self::fetchStaffInformation($id);
            $staffData = array();

            //renderactivity_id= array();
            foreach ($staffList as $value) {
                $text = $value['first_name'] ." ". $value['last_name'];
                array_push($staffData, $text);
            }

            $activityStaffModel = new SchoolPlanActivityStaffAssociation();

            if ( $model->load(Yii::$app->request->post()) ) {

                //track the edit changes.
                $postData = Yii::$app->request->post();
                $loggedInUser = Yii::$app->user->identity->getId();
                $schoolActivity = $postData['SchoolPlanActivity'];
                //$previousActivityModel = $session->get('beforeEditActivity');

                //check if the activity timeline is outside the schedule of the plan.
                $planId = $model->plan_id;
                $respectTimeRange = $this->checkIfActivityRespectsPlanTimeRange($model,$planId);

                if ($respectTimeRange == false) {
                    $errorMessage = 'The activity cannot start before or  end after the  expected or actual school plan time period.';
                    \Yii::$app->session->setFlash('failedActivity', $errorMessage);

                     return ($req->isAjax) ? $this->renderAjax('update', [
                        'model' => $model,
                        'staffList' => $staffData,
                        'activityStaffModel' => $activityStaffModel
                    ]) : $this->render('update', [
                        'model' => $model,
                        'staffList' => $staffData,
                        'activityStaffModel' => $activityStaffModel
                    ]);
                
                }else{
                     
                    $model->save(false);
                }


                if (!empty($previousActivityModel)) {
                    $category = 'activity';
                    SchoolPlanChanges::saveSchoolPlanAdjustments($previousActivityModel,$model,$category);
                    //SchoolPlanAdjustments::saveSchoolPlanAdjustments($previousActivityModel,$model,$category);
                    $session->remove('beforeEditActivity');
                }
                
                return $this->redirect(['view' , 'id' => $model->id]);

            } else {

                //keep the activity model before editing for tracking activity versions
                $session->set('beforeEditActivity', $model);
                
                return ($req->isAjax) ? $this->renderAjax('update', [
                    'model' => $model,
                    'staffList' => $staffData,
                    'activityStaffModel' => $activityStaffModel
                ]) : $this->render('update', [
                    'model' => $model,
                    'staffList' => $staffData,
                    'activityStaffModel' => $activityStaffModel
                ]);
            }
        } else {
            throw new ForbiddenHttpException('No permissions to edit school planner information');
        }


    }

    /**
     *  Retrieve the staff information for the activity_plans
     *  
     */
    public static function fetchStaffInformation($activityId)
    {
       
        $query = (new Query())->select(['ass.id','ass.activity_id', 'ass.staff_id', 'si.first_name', 'si.last_name', 'si.email_address'])
                  ->from('school_plan_activity_staff_association ass')
                  ->innerJoin('core_staff si', 'si.id=ass.staff_id')
                  ->where(['ass.activity_id'=>$activityId])
                  ->all();
        return $query ? $query : null;
    }


    /**
     *  Convert the PHP DateTime  into Javascript DateTime
     */
    private function convertPHPToJSDateTime($expected_start_date,$expected_end_date)
    {
            $startDate = new \DateTime($expected_start_date);
            $formatStartDate = $startDate->format('Y-m-d');
            $endDate = new \DateTime($expected_end_date);
            $formatEndDate = $endDate->format('Y-m-d');

            $dtime = \DateTime::createFromFormat("Y-m-d", $formatStartDate);
            $timestamp = $dtime->getTimestamp();

            $dtime2 = \DateTime::createFromFormat("Y-m-d", $formatEndDate);
            $timestamp2 = $dtime2->getTimestamp();

    
            $activityStartDate = $timestamp*1000;
            $activityEndDate = $timestamp2*1000;

            return array(
                'startDate' => $activityStartDate,
                'endDate' => $activityEndDate
            );
    }


     /**
     * Display the default school plans list
     */

    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view schools plans.');

        $request = Yii::$app->request;

        $planId = $request->get('id');

        $planDetail = SchoolPlan::findOne($planId);
        $planTitle = $planDetail->plan_name ? $planDetail->plan_name : "School Plan Activity";

        return ($request->isAjax) ? $this->renderAjax('index', ['planTitle' => $planTitle]) :$this->render('index', ['planTitle' => $planTitle]);
    }

    /**
     * Generate random colors for progress task
     * 
     */
    private function assignActivityProgressColors($activityProgress,$colors)
    {   

        $taskColor = '#ff0000';
        //$colors = ['red', 'blue', 'green', 'yellow'];
        //$colors = ['#ff0000','#2f7ed8','#006400','#ffff00'];
        $activityProgress = ($activityProgress*100);
        
        if ($activityProgress >= 0 && $activityProgress <= 25) {

            $taskColor = $colors[0]; //poor

        } else if ($activityProgress > 25  && $activityProgress <= 50 ) {

          $taskColor = $colors[1]; //average

        } else if ($activityProgress > 50 && $activityProgress <= 75) {

             $taskColor = $colors[2]; //Good

        }else if ($activityProgress > 75 && $activityProgress <= 100) {
            
            $taskColor = $colors[3]; //Excellent

        }

        return $taskColor;
    }

    /**
     * Fetch the  saved colors for the specific school_plan
     * @param integer $planId
     * @return mixed
     */
    private function fetchActivityColorForPlanner($planId)
    {
        $colors = array();
        //$colors = ['red', 'blue', 'green', 'yellow'];
        $defaultColors = ['#ff0000','#2f7ed8','#006400','#ffff00'];
        $connection = Yii::$app->db;
        
        //check if the project has any saved color codes. 
        $sqlQuery = "SELECT * FROM school_plan_colors WHERE school_plan_id=:school_plan_id";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':school_plan_id', $planId);
        $colorData = $sqlQuery->queryAll(); 

        if (count($colorData)> 0) {
            $savedColors = $colorData[0];
            if (!empty($savedColors)) {
                $colors = array(
                    $savedColors['poor_progress'], $savedColors['average_progress'],
                    $savedColors['good_progress'],$savedColors['excellent_progress'],
                );
            }
        }

        $finalColors =  !empty($colors) ? $colors : $defaultColors;

        return $finalColors;

    }

    /**
     * Check if the activity does not exceed the timeline of school plans
     * 
     */
    private function checkIfActivityRespectsPlanTimeRange($activityModel,$planId)
    {
        $respectTimeRange = true;
        $planDetail = SchoolPlan::findOne($planId);
        
        $savedStartDate = $this->removeTimeFromDate($planDetail->expected_start_date);
        $savedEndDate = $this->removeTimeFromDate($planDetail->expected_end_date);
        
        if (!empty($planDetail)) {
            if (strtotime($activityModel->expected_start_date) < strtotime($savedStartDate)) {
               $respectTimeRange = false;
            } else {
                if (strtotime($activityModel->expected_end_date) > strtotime($savedEndDate)) {
                    $respectTimeRange = false;
                }else{
                    //what about the actual_start_date n actual_end_date.
                    if  ( !empty($activityModel->actual_start_date) || !empty($activityModel->actual_end_date) ) {
                            if ( strtotime($activityModel->actual_start_date) < strtotime($savedStartDate)) {
                                $respectTimeRange = false;
                            }

                            if (strtotime($activityModel->actual_end_date) > strtotime($savedEndDate)) {
                                $respectTimeRange = false;
                            }
                    }
                }
                
            }

           
          
        }

        return $respectTimeRange;
    }

    /**
     * Remove the time component from the date objects
     */
    private function removeTimeFromDate($dateStr)
    {
        $startDate = new \DateTime($dateStr);
        $formatDate = $startDate->format('Y-m-d');
        
        //return new \DateTime($formatDate);
        return $formatDate;
    }


}
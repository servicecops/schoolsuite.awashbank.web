<?php

namespace app\modules\planner\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\controllers\BaseController;
use app\modules\logs\models\Logs;
use app\modules\planner\models\SchoolPlan;
use app\modules\planner\models\SchoolPlanActivity;
use app\modules\planner\models\SchoolPlanSubActivity;
use app\modules\planner\models\SchoolPlanSubActivityStaffAssociation;
use app\modules\planner\models\SchoolPlanSubActivitySearch;
use app\modules\planner\models\SchoolPlanComment;
use app\modules\planner\models\SchoolPlanAttachment;
use app\modules\planner\models\SchoolPlanAttachmentSearch;
use app\modules\planner\models\SchoolPlanBudget;
use app\modules\planner\models\SchoolPlanChanges;

use app\models\User;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\db\Query;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;

if (!Yii::$app->session->isActive) {
    session_start();
}

date_default_timezone_set('Africa/Kampala');

/**
 * PlannerSubActivityController implements the CRUD actions for SchoolPlanSDubActivity model.
 */
class PlannerSubActivityController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

     /**
     * Deletes an existing SchoolPlanSubActivity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
       
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {   
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);
        $userId = Yii::$app->user->identity->getId();


        if ($model->created_by == $userId) {
            
            //only delete subaactivity if it is not attached to comment or attachment or budgets
            $commentList = SchoolPlanComment::find()
                                        ->where(['subject_id' => $id])
                                        ->asArray()
                                        ->all();
            $attachmentList = SchoolPlanAttachment::find()
                                        ->where(['subject_id' => $id])
                                        ->asArray()
                                        ->all();

            $budgetList = SchoolPlanBudget::find()
                                        ->where(['activity_id' => $id])
                                        ->asArray()
                                        ->all();
            
            if (count($commentList) > 0 || count($attachmentList) > 0 ||  count($budgetList) > 0) {
                throw new ForbiddenHttpException('You cannot delete the subactivity if it is still attached to comments or  attachments or school budgets');  
            }
            
            $model->active = false;
            
            if ($model->save(false)) {
               Logs::logEvent("Deleted the school plan sub_activity: " . $model->id, null, null);
               //$outputResponse = ['success'=>true, 'message' => 'Deleted the school plan attachment succesful']; 
               //return $this->redirect(['index']);
               return Yii::$app->runAction('/planner/planner-activity/view', ['id' => $model->plan_activity_id]);
            }
        
        } else {
            throw new ForbiddenHttpException('No permission to delete the subactivity');
        }
        

    }

    /**
     * Creates a new SchoolPlanSubActivity model
     * @return SchoolPlanSubActivity
     */
    public function newModel()
    {
        $model = new SchoolPlanSubActivity();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return SchoolPlanSubActivitySearch
     */
    public function newSearchModel()
    {
        $searchModel = new SchoolPlanSubActivitySearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create School Plan SubActivity';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionLists($id = 0)
    {
        if(!$id) $id = 0;
        $countSubActivityPlans = SchoolPlanSubActivity::find()
            ->where(['plan_activityid' => $id])
            ->andWhere(['<>', 'title', '__ARCHIVE__'])
            ->count();

        $schoolSubActivities = SchoolPlanSubActivity::find()
            ->where(['plan_activity_id' => $id])
            ->andWhere(['<>', 'title', '__ARCHIVE__'])
            ->orderBy('title')
            ->all();

        Yii::trace($schoolSubActivities);

        if ($countSubActivityPlans > 0) {
            echo "<option value=''> Select School SubActivity Plan </option>";
            foreach ($schoolSubActivities as $myPlan) {
                echo "<option value='" . $myPlan->id . "'>" . $myPlan->title . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }


    /**
     * Displays a single SchoolPlanSubActivity model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) 
    {   
        if (empty(Yii::$app->user->identity) ) { 
            return \Yii::$app->runAction('/site/login');
        }

        $model = $this->findModel($id);
        $attachmentModel = new SchoolPlanAttachment();

        if (Yii::$app->user->can('r_planner', ['sch' => $model->id])) {

            $staffList = self::fetchStaffInformation($id);
            $school_plan_comments = SchoolPlanComment::find()
                                        ->where(['subject_id' => $id])
                                        ->where(['active' => true ])
                                        ->orderBy('date_created DESC')
                                        ->asArray()
                                        ->all();
            $searchAttachmentModel = new SchoolPlanAttachmentSearch();
            
            $queryParams = Yii::$app->request->queryParams;
            $queryParams['active'] = true;

            $attachmentDataProvider = $searchAttachmentModel->search($queryParams);

             $task_reminders = [
                '30 minutes'=>'30 minutes', '1 hour'=>'1 hour', '2 hours'=>'2 hours', '1 day'=>'1 day', 
                '3 days'=>'3 days', '1 week'=>'1 week', '2 weeks'=>'2 weeks', '3 weeks'=>'3 weeks',
                '1 month'=>'1 month', '2 months'=>'2 months'
            ];

            $model->task_due_date = ($model->task_due_date)? date('F j, Y, g:i a', strtotime($model->task_due_date)) : '' ;
            
            $dueDateText = ($model->reminder_alert_duration) ?  $this->determineTaskDueDuration($model->task_due_date, $model->reminder_alert_duration): '' ;

            $responseData = [
                'model'=>$model, 
                'staffList' => $staffList, 
                'commentList' => $school_plan_comments,
                'attachmentList' => $attachmentDataProvider,
                'attachmentModel' => $attachmentModel,
                'taskReminders' => $task_reminders,
                'dueDateText'=> ($dueDateText ==  '')? $dueDateText : $dueDateText['message']
            ];

            return ((Yii::$app->request->isAjax)) ? 
                $this->renderAjax('view', $responseData ) 
                      :
                $this->render('view', $responseData);

        } else {
            throw new ForbiddenHttpException('No permissions to view school plan sub activities');
        }
    }


    /**
     * Creates a new SchoolPlanSubActivity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_planner')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) { 
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolPlanSubActivity();
        $subActivityStaffModel = new SchoolPlanSubActivityStaffAssociation;

        $plan_activity_id = $request->get('id');
        $activity_version = $request->get('version');
        
        if ($model->load(Yii::$app->request->post())) {

            $respectTimeRange = $this->checkIfSubActivityRespectsActivityTimeRange($model,$plan_activity_id);

            if ($respectTimeRange == false) {
                 $errorMessage = 'The subActivity cannot start before or  end after the expected or actual school activity time period.';
                \Yii::$app->session->setFlash('failedSubActivity', $errorMessage);

                $res = ['model' => $model, 'activityStaffModel'=>$subActivityStaffModel];
                return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
            }

            try {
                
                $userId = Yii::$app->user->identity->getId();
                $model->created_by =  $userId;
                //$model->plan_activity_id = $request->get('id');
                $model->plan_activity_id = $plan_activity_id;
                if (!empty($activity_version)) {
                    $model->version = (float)$activity_version + 0.01; //1.01
                }
                
                if( $model->save(false) ){

                    Logs::logEvent("Created New School Plan SubActivity: " . $model->id, null, null);


                    //checking if association tbl has post data
                    if ($subActivityStaffModel->load(Yii::$app->request->post())){
                        
                        $data = Yii::$app->request->post();

                        $assocData = $data['SchoolPlanSubActivityStaffAssociation']['staff_id'];
                        
                          //remove duplicate classisd
                        $assocData = array_unique($assocData);  
                        if($assocData){
                            foreach($assocData as $staffId){

                                $newStaffModel = new SchoolPlanSubActivityStaffAssociation();

                                $newStaffModel->sub_activity_id = $model->id;
                                $newStaffModel->created_by = $userId;
                                $newStaffModel->staff_id = $staffId;
                                $newStaffModel->save(false);
                            }
                        }
                       
                      
                    }

                }
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            } catch (\Exception $e) {

                Yii::trace($e);
                $transaction->rollBack();
                $model_error = $e->getMessage();
                Logs::logEvent("Failed to create new school sub-activity : ", $model_error, null);
           }
        }

        $res = ['model' => $model, 'activityStaffModel'=>$subActivityStaffModel];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

     /**
     * Updates an existing SchoolPlanSubActivity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('rw_planner')) {
            $req = Yii::$app->request;

            if (empty(Yii::$app->user->identity) ) { 
                return \Yii::$app->runAction('/site/login');
            }

            $model = $this->findModel($id);

            $model->date_modified = date('Y-m-d H:i:s');

            $session = Yii::$app->session;

            $previousSubActivityModel = $session->get('beforeEditSubActivity');
            if (!empty($previousSubActivityModel)) {
                $model->version = (float)($previousSubActivityModel->version) + 0.01;
            }

            //render the edit form if not saving data
            $model->expected_start_date = date('Y-m-d', strtotime($model->expected_start_date));
            $model->expected_end_date = date('Y-m-d', strtotime($model->expected_end_date));

            $model->actual_start_date = ($model->actual_start_date)? date('Y-m-d', strtotime($model->actual_start_date)) : '' ;
            $model->actual_end_date =  ($model->actual_end_date)? date('Y-m-d', strtotime($model->actual_end_date)) : '';

            //what about the staff details for specific activityId
            $staffList = self::fetchStaffInformation($id);
            $staffData = array();
            foreach ($staffList as $value) {
                $text = $value['first_name'] ." ". $value['last_name'];
                array_push($staffData, $text);
            }

            $activityStaffModel = new SchoolPlanSubActivityStaffAssociation();

            if ( $model->load(Yii::$app->request->post()) ) {

                //track the edit changes.
                $postData = Yii::$app->request->post();
                $loggedInUser = Yii::$app->user->identity->getId();

                $schoolActivity = $postData['SchoolPlanSubActivity'];
                
                $plan_activity_id = $model->plan_activity_id;

                $respectTimeRange = $this->checkIfSubActivityRespectsActivityTimeRange($model,$plan_activity_id);

                if ($respectTimeRange == false) {
                    $errorMessage = 'The subActivity cannot start before or  end after the expected or actual school activity time period.';
                    \Yii::$app->session->setFlash('failedSubActivity', $errorMessage);
                    
                    $formErrorResponse = [
                        'model' => $model,
                        'staffList' => $staffData,
                        'activityStaffModel' => $activityStaffModel
                    ];

                    return ($req->isAjax) ? $this->renderAjax('update',$formErrorResponse)
                                       : $this->render('update', $formResponse);
                }else{

                    $model->save(false);

                }

                //$previousSubActivityModel = $session->get('beforeEditSubActivity');
                if (!empty($previousSubActivityModel)) {
                    $category = 'sub-activity';
                    SchoolPlanChanges::saveSchoolPlanAdjustments($previousSubActivityModel,$model,$category);
                    $session->remove('beforeEditSubActivity');
                }

                //SchoolPlanChanges::saveProjectChanges($projectChanges);

                return $this->redirect(['view' , 'id' => $model->id]);
            } else {

                 //keep the subActivity model before editing for tracking the changes
                 $session->set('beforeEditSubActivity', $model);
                
                return ($req->isAjax) ? $this->renderAjax('update', [
                    'model' => $model,
                    'staffList' => $staffData,
                    'activityStaffModel' => $activityStaffModel
                ]) : $this->render('update', [
                    'model' => $model,
                    'staffList' => $staffData,
                    'activityStaffModel' => $activityStaffModel
                ]);
            }
        } else {
            throw new ForbiddenHttpException('No permissions to edit school planner information');
        }


    }


    /**
     *  Retrieve the staff information for the sub_activity_plans
     *  
     */
    public static function fetchStaffInformation($subActivityId)
    {
       
        $query = (new Query())->select(['ass.id','ass.sub_activity_id', 'ass.staff_id', 'si.first_name', 'si.last_name', 'si.email_address'])
                  ->from('school_plan_subactivity_staff_association ass')
                  ->innerJoin('core_staff si', 'si.id=ass.staff_id')
                  ->where(['ass.sub_activity_id'=>$subActivityId])
                  ->all();
        return $query ? $query : null;
    }
    
    /**
     * Creates a new SchoolPlanComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreateComment()
    {
        if (!Yii::$app->user->can('rw_planner')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        date_default_timezone_set('Africa/Kampala');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolPlanComment();
        $outputResponse = [];
         try {

                $model->notes = $request->post('comment_text');
                $model->subject_type = $request->post('subject_type');
                $model->subject_id = $request->post('sub_activity_id');
                $model->created_by = Yii::$app->user->identity->getId();

                if ($model->validate()) {
                    if ( $model->save(false)) {
                        Logs::logEvent("Created subactivity  Comment: " . $model->id, null, null);
                    }

                    $transaction->commit();

                }
            
                // formatting the initials of the author.
                $commentUser = User::findOne($model->created_by);
                               
                $comment_author =  $commentUser ? $commentUser->username: 'Anonymous';
                $author_initials =  $commentUser ? substr($commentUser->lastname, 0, 1) . substr($commentUser->firstname, 0, 1) : 'AA';
                $author_initials = strtoupper($author_initials);

                $outputResponse = ['success'=>true, 'message' => array(
                   'id' => $model->id,
                   'notes' => $model->notes,
                   'subject_id'=> $model->subject_id,
                   'subject_type' => $model->subject_type,
                   'created_by' => $comment_author,
                   'author_initials' => $author_initials,
                   'date_created'=> Yii::$app->formatter->asDateTime(new \DateTime())
                )];
            
            return  $outputResponse;  

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to create new comment : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to add the comment'];
            return $outputResponse;
        }

    }


    /**
     * Updates  a SchoolPlanComment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionUpdateComment()
    {
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        date_default_timezone_set('Africa/Kampala');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        //$model = new SchoolPlanComment();
        $outputResponse = ['success' => false, 'message' => 'Failed to update comment'];

         try {
                
                $commentId = $request->post('comment_id');
                $model = $this->findCommentModel($commentId);

                $model->notes = HtmlPurifier::process($request->post('comment_text'));
                $model->created_by = Yii::$app->user->identity->getId();
                $model->date_modified = date('Y-m-d H:i:s');

                if (!empty($model->id) && !empty($model->notes)) {


                    // allow only owner of comment to update it. or PM.
                       
                    if ( $model->save()) {
                        Logs::logEvent("Updated subactivity  Comment: " . $model->id, null, null);
                    }

                    $transaction->commit();

                    $outputResponse = ['success'=>true, 'message' => array(
                        'id' => $model->id,
                        'notes' => $model->notes,
                        'subject_id'=> $model->subject_id,
                        'subject_type' => $model->subject_type,
                        'created_by' => User::findOne($model->created_by)->username,
                        //'date_created'=> Yii::$app->formatter->asDateTime(new \DateTime())
                    )];

                } else {
                    
                    $outputResponse = ['success'=> false, 'message'=> 'Failed to validate your coment input'];
                }
                
            return  $outputResponse;  

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to create update the school comment : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to update the comment'];
            return $outputResponse;
        }

    }

    /**
     * Finds the SchoolPlanComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolPlanComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCommentModel($id)
    {
        if (($model = SchoolPlanComment::findOne($id)) !== null) {
             return $model;
        } else {
            throw new NotFoundHttpException('The requested school plan comment does not exist.');
        }
    }
    
    
    /**
     * Deletes an existing SchoolPlanComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteComment($id)
    {   
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        try {

            //$this->findCommentModel($id)->delete();
            //only the owner is allowed to delete the comment.
            $userId = Yii::$app->user->identity->getId();

            $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when deleting comment'];
            
            //just archive comments so that only the PM can view it.
            $model = $this->findCommentModel($id);
            if ($model->created_by == $userId) {
                $model->active = false;
                
                if ($model->save(false)) {
                   Logs::logEvent("Archived the school plan comment: " . $model->id, null, null);
                   $outputResponse = ['success'=>true, 'message' => 'Deleted the school comment succesful']; 
                }
            } else {
                  
                $outputResponse = ['success'=>false, 'message' => 'Failed to delete the comment because you are not author']; 
            }
        
            
            return $outputResponse;

        } catch (\Exception $e) {
             
            $model_error = $e->getMessage();

            Logs::logEvent("Failed to delete  the school comment  : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to delete the school comment'];

            return $outputResponse;
        }
       
    }
    

    /**
     * Updates  a SchoolPlanComment model with due date 
     *  @return mixed
     */
    public function actionSetTaskDeadline()
    {
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        date_default_timezone_set('Africa/Kampala');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $outputResponse = ['success' => false, 'message' => 'Failed to set the due date for task'];

         try {
                
                $postData = Yii::$app->request->post();
        
                $subActivityId = $postData['SchoolPlanSubActivity']['id'];
                $model = $this->findModel($subActivityId);
                Yii::trace($postData);

                $model->task_due_date =  $postData['SchoolPlanSubActivity']['task_due_date'];
                $model->reminder_alert_duration = $postData['SchoolPlanSubActivity']['reminder_alert_duration'];
                
                if (!empty($model->task_due_date) && !empty($model->reminder_alert_duration)) {
                     
                   
                    // allow only the PM or owner to set the due date for now
                    $userId = Yii::$app->user->identity->getId();
                    $session = Yii::$app->session;
                    $isProjectManager = $session->get('project-manager');


                    if ( $isProjectManager === true) {
                        Yii::trace($model->task_due_date);  
                        if ( $model->save()) {
                            Logs::logEvent("Updated subactivity  Due Date: " . $model->id, null, null);
                        }
    
                        $transaction->commit();
    
                        $outputResponse = ['success'=>true, 'message' => array(
                            'id' => $model->id,
                            'title' => $model->title,
                            'task_due_date'=> $model->task_due_date,
                            'reminder_alert_duration' => $model->reminder_alert_duration,
                            'created_by' => User::findOne($model->created_by)->username
                        )];
    
                    } else {
                         
                        $outputResponse = ['success'=> false, 'message'=> 'You are not authorised to set the due date, only the project manager is allowed'];
                    }
        
                } else {
                    
                    $outputResponse = ['success'=> false, 'message'=> 'Failed to validate your due date input'];
                }
                
            return  $outputResponse;  

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to set the due date : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to set the due date'];
            return $outputResponse;
        }

    }
    
    /***
     * Determine whether the task is overdue. 
     */
    private function determineTaskDueDuration($dueDate,$reminder_duration){
        
        $result = '<p class = "due-text-info">You still have some time until ' . $dueDate . "</p>";
        $dueDateStatus = 'still-pending';
        $dtNow = new \DateTime();
        $dtToCompare = new \DateTime($dueDate);

        if ($dtNow > $dtToCompare) {
            $result = '<p class = "due-text-danger">Your task is overdue, It is beyond the due date ' . $dueDate . "</p>";
            $dueDateStatus = 'task-overdue';
        } else {

            $added_duration = "+". $reminder_duration;
            $dtNow = date_modify($dtNow, $added_duration);

            if ($dtNow > $dtToCompare) {
                //If less than 24 hours .. show the difference using hours.
                //if less than week , show the days instead.
                //if less than month, show the weeks instead
                $diff = $dtNow->diff($dtToCompare); 
                $diff = $diff->h + ($diff->days * 24); 
                // Yii::trace("date difference"); //hours
                // Yii::trace($diff); 
                $result = '<p class = "due-text-warning">Your task is almost due...' .$dueDate . ' and you are left with '.$diff . ' hours</p>' ;
                $dueDateStatus = 'task-almost-due';
            }
            
        }

        $response = array(
            'status' => $dueDateStatus,
            'message' => $result
        );

        return $response;
        
    }

    /***
     * Send the automated email alerts for the tasks that are overdue
     * 
     */
    public function actionSendTaskDueEmailAlerts()
    {   
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $subActivities = SchoolPlanSubActivity::find()
                         ->where(['active'=>true ])
                         ->orderBy('id')
                         ->asArray()
                         ->all();
        $alertMessages = [];
        $emailAlertCount = 0;

        if (count($subActivities) > 0 ) {

            foreach($subActivities as $subActivity){
                if ( !empty($subActivity['reminder_alert_duration']) && !empty($subActivity['task_due_date'])) {
                    $alertDueText = $this->determineTaskDueDuration($subActivity['task_due_date'], $subActivity['reminder_alert_duration']);
                    if ($alertDueText['status'] == 'task-overdue' || $alertDueText['status'] == 'task-almost-due') {
                        $staffList = self::fetchStaffInformation($subActivity['id']);

                        if (count($staffList) > 0 ) {
                            foreach ($staffList as $staff) {
                                $this->sendEmailMessage($staff['email_address'],$alertDueText['message']);
                               
                            }
                        }
                        $projectManager = $this->determineProjectManagerEmailAddress($subActivity['plan_activity_id']);

                        if ( !empty($projectManager) ) {     
                            if ( ! in_array($projectManager, array_column($staffList, 'email_address'))) { 
                                //avoid re-sending email alert to member if he is project manager
                                $this->sendEmailMessage($projectManager, $alertDueText['message']);
                            }
                        }

                    $emailAlertCount += 1;
                    } else {
                        Yii::trace("no need to receive the email alert");
                    }
                }
            }
        }

        $actionResponse = [
            'success'=> true, 
            'message' => 'Successful sent out ' . $emailAlertCount .  ' email alerts to the members'
        ];
        
        return $actionResponse;
    }



    /**
     * Retrieve the project manager email address for the school_plan
     * @param integer $managed_by
     * @return mixed
     */
    private function determineProjectManagerEmailAddress($ActivityId)
    {     
        $emailAddress = '';
        $activity = SchoolPlanActivity::findOne($ActivityId);
        //only send alerts if receive_due_alerts is enabled
        $plan = SchoolPlan::findOne(['id'=> $activity->plan_id, 'receive_due_alerts'=>true]);

        if (!empty($plan)) {
            $user = User::findOne(['school_user_id' => $plan->managed_by]);
            $emailAddress = $user ? $user->email : '';
        }
        return $emailAddress;

    }

    /**
     * Send the email address to the members
     * @param string $emailAddress
     * @return mixed
     */
    private function sendEmailMessage($emailAddress,$message)
    {
           \Yii::$app->mailer->compose()
                //->setTo('goldsoft25@gmail.com')
                ->setTo($emailAddress)
                ->setFrom('support@schoolsuite.ug')
                ->setSubject('Your task is almost due')
                //->setTextBody("You successfully reset your password \n Username: " . $user->username . "\n Password: " . $password)
                ->setHtmlBody($message)
                ->send(); 
    }

    /**
     * Check if the subActivity does not exceed the timeline of activity
     * 
     */
    private function checkIfSubActivityRespectsActivityTimeRange($subActivityModel,$activityId)
    {
        $respectTimeRange = true;
        $activityDetail = SchoolPlanActivity::findOne($activityId);
        

        if (!empty($activityDetail)) {
            if (strtotime($subActivityModel->expected_start_date) < strtotime($activityDetail->expected_start_date)) {
               $respectTimeRange = false;
            } else {
                if (strtotime($subActivityModel->expected_end_date) > strtotime($activityDetail->expected_end_date )) {
                    $respectTimeRange = false;
                }else{

                     //what about the actual_start_date n actual_end_date.
                    if  ( !empty($subActivityModel->actual_start_date) || !empty($subActivityModel->actual_end_date) ) {
                        if ( strtotime($subActivityModel->actual_start_date) < strtotime($activityDetail->expected_start_date)) {
                            $respectTimeRange = false;
                        }

                        if (strtotime($subActivityModel->actual_end_date) > strtotime($activityDetail->expected_end_date )) {
                            $respectTimeRange = false;
                        }
                    }
                }
            }
          
        }

        return $respectTimeRange;
    }

    

}
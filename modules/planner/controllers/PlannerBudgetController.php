<?php

namespace app\modules\planner\controllers;

use app\models\ImageBank;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Yii;
use app\modules\planner\models\SchoolPlanAttachment;
use app\modules\planner\models\SchoolPlanAttachmentSearch;
use app\modules\planner\models\SchoolPlanSubActivity;
use app\modules\planner\models\SchoolPlanActivity;
use app\modules\planner\models\SchoolPlanBudget;
use app\modules\planner\models\SchoolPlanBudgetSearch;
use app\modules\planner\models\SchoolPlan;
use app\modules\logs\models\Logs;


use yii\base\Exception;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;


if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * PlannerBudgetController implements the CRUD actions for SchoolPlanBudget model.
 */
class PlannerBudgetController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SchoolPlanBudget models.
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view school budgets');
        
        $activityId = $request->get('activityId');
        $activityType = $request->get('activityType');
        $searchModel = new SchoolPlanBudgetSearch();
        $searchParams = array(
            'activity_id'=>$activityId
        );

        $dataProvider = $searchModel->searchNewIndex($searchParams);
        
        $schoolActivityModel;
        if ($activityType == 'activity') {
           $schoolActivityModel = SchoolPlanActivity::findOne($activityId);
        }

        if ($activityType === 'subActivity') {
            $schoolActivityModel = SchoolPlanSubActivity::findOne($activityId);
        }
        
        

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'activityModel' => $schoolActivityModel,
            'activityType' => $activityType
        ]) : $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'activityModel' => $schoolActivityModel,
            'activityType' => $activityType
        ]);
    }

    /**
     * Displays a single SchoolPlanBudget model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
         
        $model = $this->findModel($id);

        //format the budget approval date.
        $model->approval_date = ($model->approval_date)? date('Y-m-d', strtotime($model->approval_date)) : '' ;

        //load the attachments for the budgets.. 
        $attachmentModel = new SchoolPlanAttachment();

        //what about the school plan attachments.
        $searchAttachmentModel = new SchoolPlanAttachmentSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['active'] = true;

        $attachmentDataProvider = $searchAttachmentModel->search($queryParams);
                
        
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $model,
            'attachmentList' =>$attachmentDataProvider,
            'attachmentModel' => $attachmentModel,
        ]) : $this->render('view', [
            'model' => $model,
            'attachmentList' =>$attachmentDataProvider,
            'attachmentModel' => $attachmentModel,
        ]);
    }


    /**
     * Creates a new School Plan Budget model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_planner')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolPlanBudget();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();

                $model->activity_id = $request->get('id');

                if( $model->save(false) ){
                     Logs::logEvent("Created New School Plan Budget: " . $model->id, null, null);                    
                }
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to create new school plan budget : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

    /**
     * Finds the SchoolPlanBudget model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolPlanBudget the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolPlanBudget::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     *  Upload the documents to the school budget
     * Upload the documents to the budget for accountability purposes
     * @return mixed
     * @throws \yii\base\InvalidArgumentException|\yii\db\Exception
     */
    public function actionUploadDocuments() 
    {
        $model = new SchoolPlanBudget();
        $request = Yii::$app->request;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
        $model->activity_id = $request->post('activity_id');

        $transaction = SchoolPlanBudget::getDb()->beginTransaction();
        try {
                
            $image = UploadedFile::getInstance($model, 'image');
            Yii::trace($image);
            if (!is_null($image)) {
                $model->file_attachment = $image->name;
                $model->attach_name = $image->name;
                $tmp_img = explode('.', $image->name);
                $ext = end($tmp_img); //new image extension

                $model->file_attachment = Yii::$app->security->generateRandomString() . ".{$ext}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/planner/';
                $path = Yii::$app->params['uploadPath'] . $model->file_attachment;
                $image->saveAs($path);

                $model->created_by = Yii::$app->user->identity->getId();
                $model->save(false);
                $transaction->commit();
                $outputResponse = ['success'=>true, 'message' => 'Uploaded the document successful'];

            } else {

                Yii::trace("No attachment  attached onto the form");
                $outputResponse = ['success' => false, 'message'=> 'It is not valid attachment being uploaded'];
            }
                    
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE planner attachment callback');

                $outputResponse = ['success'=>false, 'message' => 'Failed to upload the file'];
            }

        return $outputResponse;

    }


    /**
     * Updates an existing SchoolPlanBudget model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        try {
            if ($model->load(Yii::$app->request->post())) {

                $model->date_modified = date('Y-m-d H:i:s');

                if( $model->save(false) ){
                    Logs::logEvent("Updated the School Plan Budget: " . $model->id, null, null);                    
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SchoolPlanBudget model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {    

        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);

        $userId = Yii::$app->user->identity->getId();

        if ($model->created_by == $userId) {

            $model->active = false;
            
            if ($model->save(false)) {
            
               Logs::logEvent("Archived the school budget: " . $model->id, null, null);
               
               $activityData = SchoolPlanSubActivity::findOne($model->activity_id);

               $activityType = !empty($activityData)? 'subActivity' : 'activity'; 

                return $this->redirect(['/planner/planner-budget/index','activityId' => $model->activity_id, 'activityType' => $activityType ]);
            }
            
        
        } else {
              
           throw new ForbiddenHttpException('No permission to delete the school plan budget'); 
        }

    }

    /**
     * Show the budget summary for project.
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionBudgetSummary()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view school budgets');
        
        
        $planId = $request->get('planId');

        $resultData = array();
        
        $schoolPlan = SchoolPlan::findOne($planId);
        $planTitle = $schoolPlan ? $schoolPlan->plan_name: '';
        $activityRows = SchoolPlanActivity::find()
                                ->where(['plan_id' => $planId])
                                ->orderBy('id')
                                ->asArray()
                                ->all();
        if (count($activityRows) > 0) {
            # code...
            $taskArray = array();
            foreach ($activityRows as $activity) {
                //fetch the sub tasks for each activity
                $groupedSubTasks  = array();
                $activityId = $activity['id'];
                $activityTitle = $activity['title'];
                $subActivities = SchoolPlanSubActivity::find()
                                ->where(['plan_activity_id' => $activityId])
                                ->orderBy('id')
                                ->asArray()
                                ->all();

                //what about the budgets items for activity.
                $activityBudgets = array();
                $taskBudgets = SchoolPlanBudget::find()
                                ->where(['activity_id' => $activityId ])
                                ->orderBy('id')
                                ->asArray()
                                ->all();

                if (count($taskBudgets) > 0 ) {
                    $activityBudgets = $taskBudgets;
                }

                     

                if (count($subActivities) > 0) {
                    //fetch the budgets for each subactivity.
                    foreach ($subActivities as $subTask) {
                        $subTaskId = $subTask['id'];
                        $subTaskBudgets = SchoolPlanBudget::find()
                                          ->where(['activity_id' => $subTaskId ])
                                          ->orderBy('id')
                                          ->asArray()
                                          ->all();
                        $subTaskBudgetCost = $this->calculateTotalSubActivityBudget($subTaskBudgets);
                        array_push($groupedSubTasks, [
                            'subActivityId' => $subTaskId,
                            'subActivityTitle' => $subTask['title'],
                            'budgets' => $subTaskBudgets,
                            'budgetCost' => $subTaskBudgetCost 
                        ]);

                    }
                }

                array_push($taskArray, [
                    'activityId' => $activityId,
                    'activityTitle' => $activityTitle,
                    'activityBudgets' => $activityBudgets,
                    'subTasks' => $groupedSubTasks
                ]);
            }
            
            $resultData = $taskArray;
        }
        
        
        return ($request->isAjax) ? $this->renderAjax('_summary', [
            'budgetList' => $resultData,
            'planTitle' => $planTitle
        ]) : $this->render('_summary', [
            'budgetList' => $resultData,
            'planTitle' => $planTitle
        ]);
    }

    /***
     * Calculate the total budget cost for subactivity
     * 
     */
    private function calculateTotalSubActivityBudget($budgets)
    {
        $totalBudgetCost = 0;
        if (count($budgets)> 0 ) {
            foreach ($budgets as $budget) {
                $budgetCost = ((float)$budget['unit_cost'] * (float)$budget['item_quantity']);
                $totalBudgetCost+=$budgetCost;
                //$totalBudgetCost = $totalBudgetCost + $budgetCost;
            }
        }

        return $totalBudgetCost;
    }


    /**
     * Updates  a SchoolPlanBudget  model with approval date
     *  @return mixed
     */
    public function actionApprove()
    {
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        date_default_timezone_set('Africa/Kampala');

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $outputResponse = ['success' => false, 'message' => 'Failed to approve the school plan budget'];

         try {
                
                $postData = Yii::$app->request->post();
                $budgetItemId = $postData['SchoolPlanBudget']['id'];
                $model = $this->findModel($budgetItemId);

                $model->approval_date =  $postData['SchoolPlanBudget']['approval_date'];
                
                if (!empty($model->approval_date) ) {
                     
                   
                    // allow only owner of budget to approive it or the PM.
            
                    if ( $model->save()) {
                        Logs::logEvent("Updated school plan budget approval_date: " . $model->id, null, null);
                    }

                    $transaction->commit();
                    $outputResponse = ['success'=> true, 'message' => 'Successfully approved the school plan budget'];

                } else {
                    
                    $outputResponse = ['success'=> false, 'message'=> 'Failed to validate approval date input'];
                }
                
            return  $outputResponse;  

        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to approve the school plan budget: ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to approve the school budget'];
            return $outputResponse;
        }

    }


     /**
     * Creates a new School Plan Budget model by uploading the Excel document
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionUploadExcel()
    {
        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new DynamicModel(['importFile', 'description', 'activity_id']);
        $model->addRule(['importFile'], 'file', ['extensions' => 'xls, xlsx'])
              ->addRule(['description', 'activity_id'], 'required')
              ->addRule(['activity_id'], 'integer');
        
        $inputFile = $hash = $file_name = null;
        if (isset($_FILES['file'])) {
            $inputFile = $_FILES['file']['tmp_name'];
            $hash = md5_file($inputFile);
            $file_name = $_FILES['file']['name'];
        } else if (isset($_FILES['DynamicModel'])) {
            $inputFile = $_FILES['DynamicModel']['tmp_name']['importFile'];
            $hash = md5($inputFile);
            $file_name = $_FILES['DynamicModel']['name']['importFile'];
        }
        

        if ($model->load(Yii::$app->request->post())) { 
              
            if ($inputFile && $model->description && $model->activity_id) {

                try {

                    //extract the data from the excel documents.
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);


                    $sheet = $objPHPExcel->getSheet(0);
                    $highestRow = $sheet->getHighestRow();
                    $highestColumn = $sheet->getHighestcolumn();

                    $upload_by = Yii::$app->user->identity->getId();

 
                    $sql1 = "INSERT INTO budget_upload_file_details (
                               file_name, 
                               hash, 
                               uploaded_by, 
                               description, 
                               activity_id
                            )
                              VALUES (
                               :file_name, 
                               :hash, 
                               :uploaded_by, 
                               :description, 
                               :activity_id
                            )";

                    $activityId = (int) $model->activity_id;

                    $fileQuery = $connection->createCommand($sql1);
                    $fileQuery->bindValue(':file_name', $file_name);
                    $fileQuery->bindValue(':hash', $hash);
                    $fileQuery->bindValue(':uploaded_by', $upload_by);
                    $fileQuery->bindValue(':description', $model->description);
                    $fileQuery->bindValue(':activity_id', $activityId);
                    //Insert file
                    $fileQuery->execute();
                    $file_id = $connection->getLastInsertID('budget_upload_file_details_id_seq');
                    $regNos = [];
                    $numberOfRecords = 1;

                    for ($row = 1; $row <= $highestRow; $row++) {
                        if ($row == 1) {
                            continue;
                        }
                        $v = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        //Collection information from excel row
                        $excelTitle = !isset($v[0][0]) ? null : trim(ucwords(strtolower($v[0][0])));
                        $excelDescription = !isset($v[0][1]) ? null : trim(ucwords(strtolower($v[0][1])));
                        $excelUnitCost = !isset($v[0][2]) ? null : $v[0][2];
                        $excelItemQuantity = !isset($v[0][3]) ? null : $v[0][3];
                        $excelComments = !isset($v[0][4]) ? null : strtoupper(trim($v[0][4]));

                        //Check that both title and description are present
                        if (empty($excelTitle) || empty($excelDescription)) {
                            throw new Exception("Both title and description are required. Check record $numberOfRecords");
                        }

                        if (empty($excelUnitCost) && ! is_numeric($excelUnitCost)) {
                            throw new Exception("unit_cost is required and it must be a number. Check record $numberOfRecords");
                        }

                        if (empty($excelItemQuantity) && ! is_numeric($excelItemQuantity)) {
                            throw new Exception("item_quantity is required and it must be a number. Check record $numberOfRecords");
                        }


                        if ($excelTitle && $excelDescription && $excelUnitCost && $excelItemQuantity && $excelComments ) {
                             $budgetModel = new SchoolPlanBudget();
                             $budgetModel->title = $excelTitle;
                             $budgetModel->description = $excelDescription;
                             $budgetModel->unit_cost = $excelUnitCost;
                             $budgetModel->item_quantity = $excelItemQuantity;
                             $budgetModel->comments = $excelComments;
                             $budgetModel->activity_id = $activityId;
                             $budgetModel->created_by  = $upload_by;

                            $budgetModel->save(false);
                            
                            //Increment no of records
                            $numberOfRecords++;
                        } else {
                            $transaction->rollBack();
                            $title =$excelTitle ? " title: " .$excelTitle : " <b>Title : (not set)</b>";
                            $description = $excelDescription ? " Description: " . $excelDescription : " <b>Description: (not set)</b>";
                            $itemQty = $excelItemQuantity ? " item Quantity: " . $$excelItemQuantity : " <b>Item Quantity: (not set)</b>";
                            $comment = $excelComments ? " comment: " . $excelComments : " <b>comment: (not set)</b>";
                            $unitCost = $excelUnitCost ? " Unit Cost: " . $excelUnitCost : " <b>Unit Cost: (not set)</b>";
                            Logs::logEvent("Failed to upload budget: ", "<b>Fix Row - " . $row . "</b> " . $title . $description . $itemQty . $comment . $unitCost, null);
                            return "<span style='font-size:15px;color:#C50101;'><p><h4>Fix Row - " . $row . "</h4> " . $title . $description . $itemQty . $comment . $unitCost . " </p></span>";
                        }
                    }

                    //$imported = (new Query())->select(['imported', 'uploaded_by'])->from('budget_upload_file_details')->where(['id' => $file_id])->limit(1)->one();
                    $transaction->commit();
                    Logs::logEvent("School budgets  imported for subActivity: " . $activityId, null, null);

                    //Send email
                    // $subject = "New School plan budget  File Uploaded $file_name";
                    // $emailMessage = "New file uploaded for $schoolRecord->school_name \n
                    // Uploaded by: " . Yii::$app->user->identity->username . "\n
                    // File Hash: $hash \n
                    // Number of records: $numberOfRecords";

                     $message = "Bulk import for budget excel was successful";
                    \Yii::$app->session->setFlash('uploadSuccess', $message);

                    $subActivityModel = $this->findSubActivityModel($activityId);

                    return  $this->redirect(['/planner/planner-sub-activity/view', 'id'=>$subActivityModel->id]);
                

                } catch (\Exception $e) {
                    Yii::trace($e);
                    $transaction->rollBack();
                    $model_error = $e->getMessage();
                    Logs::logEvent("Failed to upload school budgets: ", $model_error, null);
                    return "<div class='alert alert-danger' style='    background-color: #dd4b39 !important;'>Excel file File upload failed<br><br>
                    <p>" . $model_error . "</p> </div>";

                }

            } else{

                return "<div class='alert alert-danger'>File or Description or Activity can't be blank</div>";

            }

        }else{

            return ($request->isAjax) ? $this->renderAjax('upload_file', ['model' => $model]) :
                $this->render('upload_file', ['model' => $model]);
        }


    }


    /**
     * Retrieves the subactivities information for particular schoolplan
     *  @return mixed
     */
    public function actionSubActivityList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        
        if (!is_null($q)) {

            $query = new Query();
            $planId = Yii::$app->request->get('planId');
            $planId = !empty($planId)? $planId : '';

            $query->select(['si.id','si.title','si.description','si.plan_activity_id', 'si.active','activity.plan_id'])
                ->from('school_plan_subactivity si')
                ->innerJoin('school_plan_activity activity', 'activity.id=si.plan_activity_id') //activity
                ->where(['ilike', 'si.title', $q])
                ->andWhere([ 'activity.plan_id' => $planId,'si.active' => true]);
                
                //->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['title']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }


    /**
     * Finds the SchoolPlanSubActivity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolPlanSubActivity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findSubActivityModel($id)
    {
        if (($model = SchoolPlanSubActivity::findOne($id)) !== null) {
             return $model;
        } else {
            throw new NotFoundHttpException('The requested school plan subactivity does not exist.');
        }
    }


}




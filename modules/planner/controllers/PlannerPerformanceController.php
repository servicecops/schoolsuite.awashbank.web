<?php

namespace app\modules\planner\controllers;


use app\modules\planner\models\SchoolPlan;
use app\modules\planner\models\SchoolPlanActivity;
use app\modules\planner\models\SchoolPlanSubActivity;
use app\modules\planner\models\CoreStaff;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;


if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 *  PlannerPerformanceController deals with performance of the school plan 
 * activities and subactivities
 */
class PlannerPerformanceController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {  
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view schools plans performance.');
    
    
        $plans = array();
        $completeTaskProgress = array();
        $incompleteTaskProgress = array();
        $projectManagers = array();
        $schoolId = Yii::$app->user->identity->school_id;

        //return per term according to selected term
        //$schoolPlanList = SchoolPlan::find()->asArray()->orderBy('id')->all();
        $schoolPlanList = SchoolPlan::find()->where(['school_id' => $schoolId ])->asArray()->orderBy('id')->all();
        foreach ($schoolPlanList as $plan) {
            
            $detailedInfo = false;
            $progressData = $this->determineActivityProgress($plan, $detailedInfo);
            
            array_push($plans, $plan['plan_name']); //X-axis categories
            //series
            array_push($completeTaskProgress, $progressData['averageProgress']);
            array_push($incompleteTaskProgress, $progressData['incompleteProgress']);

            //pie chart data.
            array_push($projectManagers, [
                'name' => $progressData['projectManager'],
                'y' => $progressData['averageProgress']
            ]);
        }
        //series data --> Y-axis 
        $planTaskData = [
            [ 'name' =>'Incomplete', 'data' => $incompleteTaskProgress ],
            [ 'name' => 'Complete', 'data' => $completeTaskProgress ]
        ];

        $projectManagerPerformance = $this->removeDuplicateProjectManager($projectManagers);
        $rankList = $this->rankProjectManagers($projectManagerPerformance);
        Yii::trace("rankList");
        Yii::trace($rankList);
        return $this->render('index', [
            'plans' => $plans,
            'planTaskData' => $planTaskData,
            'projectManagers' => $projectManagerPerformance,
            'rankList' => $rankList
        ]);
    }

    /**
     * Sort the project managers according to the best performance
     */
    private  function rankProjectManagers($projectManagers)
    {   
       $keys = array_column($projectManagers, 'y');
       array_multisort($keys, SORT_DESC, $projectManagers);
       $bestPerformers = $projectManagers;
       $totalSum = array_sum($keys);
       $rankList = [];
       foreach ($projectManagers as $item ) {
           $rating = '';
           $avgPerformance = (($item['y']/$totalSum)*100);
           $avgPerformance = round($avgPerformance,2);
           if ($avgPerformance >  0 && $avgPerformance <= 30) {
               $rating = 'Poor';
           } else if ($avgPerformance > 30 && $avgPerformance <= 60) {
               $rating = 'Good';
           } else if ($avgPerformance > 60 && $avgPerformance <= 90) {
               $rating = 'Very Good';
           }else if ($avgPerformance > 90 && $avgPerformance <=100) {
              $rating = 'Excellent';
           }

           array_push($rankList, [
               'name' => $item['name'],
               'averagePerformance' => $avgPerformance,
               'rating' => $rating
           ]);
           
           
       }
       return $rankList;
    }

    


    /**
     * Determine the task progress for the activities 
     */
    private function determineActivityProgress($schoolPlan,$detailedInfo)
    {
        $resultData = []; //series data
        $averageProgress = 0;
        $incompleteProgress = 0;
        $planId = $schoolPlan['id'];
        $activityRows = SchoolPlanActivity::find()
                                ->where(['plan_id' => $planId])
                                ->orderBy('id')
                                ->asArray()
                                ->all();
        $activityData = array();

        if (count($activityRows) > 0) {
            $totalProgress = 0;
            $totalIncomplete = 0;
            $activityCount = 0;
            $incompleteProgress = 0;

            foreach ($activityRows as $activity) {
                $totalProgress+=(float)$activity['progress'];
                $totalIncomplete+=(100 - (float)$activity['progress']);
                $activityCount+=1;
                //what about the sub_activities.
                //$subActivitiesRows = $this->determineSubActivityProgress($activity['id']);
                $subActivitiesRows = $detailedInfo ? $this->determineSubActivityProgress($activity['id']) : [];
                
                array_push($activityData, [
                    'activityId' => $activity['id'],
                    'activityName' => $activity['title'],
                    'activityProgress' => (float)$activity['progress'],
                    'activityIncomplete' => 100 - ((float)$activity['progress']),
                    'subActivities' => $subActivitiesRows
                ]);

            }

            $averageProgress = ($totalProgress/$activityCount);
            //$incompleteProgress = (100 - $averageProgress); //let users respect 100 %
            $incompleteProgress = ($totalIncomplete/$activityCount);
        }

        $staffInfo = empty($schoolPlan['managed_by']) ? '' : CoreStaff::findOne($schoolPlan['managed_by']);
        $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';    

        $resultData = [
            'schoolPlan' => $schoolPlan['plan_name'],
            'averageProgress' => $averageProgress,
            'incompleteProgress' => $incompleteProgress,
            'projectManager' => $selectedStaff,
            'activityData' => $activityData
        ];

        return $resultData;
    }

    /**
     * Determines the detailed progress of sub activities for particular activity
     * @param string $id
     * @return mixed
     */
    private function determineSubActivityProgress($activityId)
    {   
        $resultData = array();
        $subActivities = SchoolPlanSubActivity::find()
                              ->where(['plan_activity_id' => $activityId])
                              ->orderBy('id')
                              ->asArray()
                              ->all();

        if (count($subActivities) > 0 ) {
            $resultData = $subActivities;
        }

        return $resultData;
    }

    /**
     * Displays the performance metrics for single project
     */

     public function actionView()
     {   
        $planName = Yii::$app->request->get('plan-name');
        return $this->render('view', [
            'planName' => $planName
        ]);
    }


    /**
     * Fetch the performance metrics for single school plan.
     */
    public function actionViewSinglePerformance()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $outputResponse = [
            'success' => false, 
            'message' => 'Failed to retrieve performance metrics for school plan'
        ];

        
        $planName = Yii::$app->request->get('plan-name');

        $plan = SchoolPlan::find()->where(['like', 'plan_name', $planName . '%', false])->asArray()->one();

        if (!empty($plan)) {
              
            $detailedInfo = true;
            $progressData = $this->determineActivityProgress($plan, $detailedInfo);

            $projectDetail = array(
                'schoolPlan' => $progressData['schoolPlan'],
                'averageProgress' => $progressData['averageProgress'],
                'incompleteProgress' => $progressData['incompleteProgress'],
                'projectManager' => $progressData['projectManager'],
                'activityCount' => count($progressData['activityData'])
            );
            //fromat data for the Highchart drilldown graph.
            $reformattedActivities = array();
            $drillDownChartData = array();
            foreach ($progressData['activityData'] as $activity) {
                $activityName = $activity['activityName'];
                $drilldownValue =  !empty($activity['subActivities'])? $activityName : null;
                array_push($reformattedActivities,[
                    'name' => $activityName,
                    'y' => (int) $activity['activityProgress'],
                    'drilldown' => $drilldownValue
                ]);
                // what about the drilldown chart.
                if (!empty($activity['subActivities'])) {
                    $subActivities = array();
                    foreach ($activity['subActivities'] as $subActivity) {
                        array_push($subActivities, [
                            $subActivity['title'], 
                            (int)$subActivity['progress'] 
                        ]);
                    }
                    array_push($drillDownChartData, [
                        'name' => $activityName,
                        'id' => $activityName,
                        'data' => $subActivities
                    ]);
                }
                
            }

            $responseData = array(
                'projectDetail' => $projectDetail,
                'activities' => $reformattedActivities,
                'drillDown' => $drillDownChartData
            );

            $outputResponse = [
                'success' => true, 
                'message' => json_encode($responseData) //format php arrays into JSON for JS
            ];
        }

        return $outputResponse;

    }

    /**
     * Remove the duplicate project managers from array
     */
    private function removeDuplicateProjectManager($projectManagers)
    {
        $finalManagers = [];
        $members = array_unique(array_column($projectManagers, 'name'));

        if (count($members) > 0 ) {
            for ($i = 0; $i < count($members); $i++) {

                if (isset($members[$i])) {
                    
                    $member = $members[$i];
                    
                    $totalProgress = $this->calculateTotalProgress($projectManagers, $member);
                
                    array_push(
                        $finalManagers, [
                           'name' => $member,
                           'y' => $totalProgress
                        ]
                    );
                
                }
            }
        }    
        return $finalManagers;
    }

    private function calculateTotalProgress($projectManagers,$member)
    {
            $matches = array();
            $totalProgress = 0;
            foreach($projectManagers as $a){
                if($a['name'] == $member)
                    $matches[]=$a;
            }
            foreach($matches as $value){
               $totalProgress+=$value['y'];  
            }
            return $totalProgress;
    }


}
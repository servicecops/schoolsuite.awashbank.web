<?php

namespace app\modules\planner\controllers;

use app\models\ImageBank;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Yii;
use app\modules\planner\models\SchoolPlanAttachment;
use app\modules\planner\models\SchoolPlanAttachmentSearch;
use app\modules\planner\models\SchoolPlanSubActivity;
use app\modules\logs\models\Logs;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;


if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * PlannerAttachmentController implements the CRUD actions for SchoolPlanAttachment model.
 */
class PlannerAttachmentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SchoolPlanAttachment models.
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionIndex() // put in the actionView for sub_activity
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view planner attachments.');

        $searchModel = new SchoolPlanAttachmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) : $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SchoolPlanAttachment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }


        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) : $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the SchoolPlanAttachment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolPlanAttachment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolPlanAttachment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new SchoolPlanAttachment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidArgumentException|\yii\db\Exception
     */
     /**
     * Creates a new SchoolPlanAttachment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidArgumentException|\yii\db\Exception
     */
    public function actionCreate() 
    {
        $model = new SchoolPlanAttachment();
        $request = Yii::$app->request;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            
        $model->subject_id = $request->post('subject_id');

        $transaction = SchoolPlanAttachment::getDb()->beginTransaction();
        try {
                
            $image = UploadedFile::getInstance($model, 'image');
            //Yii::trace($image);
            if (!is_null($image)) {
                $model->file_attachment = $image->name;
                $model->attach_name = $image->name;
                $tmp_img = explode('.', $image->name);
                $ext = end($tmp_img); //new image extension

                $model->file_attachment = Yii::$app->security->generateRandomString() . ".{$ext}";
                $fp = fopen($image->tempName, 'r');
                //$fp = fopen($model->file_attachment, 'r');
                $content = fread($fp, filesize($image->tempName));
                //$content = fread($fp, filesize($model->file_attachment));
				fclose($fp);

                
                $model->file_attachment =  base64_encode($content);
                
                $model->created_by = Yii::$app->user->identity->getId();
                $model->save(false);
                $transaction->commit();
                $outputResponse = ['success'=>true, 'message' => 'Uploaded the document successful'];

            } else {

                Yii::trace("No attachment  attached onto the form");
                $outputResponse = ['success' => false, 'message'=> 'It is not valid attachment being uploaded'];
            }
                    
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'CREATE planner attachment callback');

                $outputResponse = ['success'=>false, 'message' => 'Failed to upload the file'];
            }

        return $outputResponse;

    }


    /**
     * Updates an existing SchoolPlanAttachment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        try {
            if ($model->load(Yii::$app->request->post())) {
                $image = UploadedFile::getInstance($model, 'image');
                if (!is_null($image)) {
                    $model->file_attachment = $image->name;
                    Yii::trace($image->name);
                    $tmp_img = explode('.', $image->name);
                    $ext = end($tmp_img); //new image extension

                    $model->file_attachment = Yii::$app->security->generateRandomString() . ".{$ext}";
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/planner/';
                    $path = Yii::$app->params['uploadPath'] . $model->file_attachment;
                    $image->saveAs($path);
                }
                $model->modified = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['index']);
            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SchoolPlanAttachment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {   

        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }

        $model = $this->findModel($id);

        $userId = Yii::$app->user->identity->getId();

        if ($model->created_by == $userId) {

            $model->active = false;
            
            if ($model->save(false)) {
               Logs::logEvent("Archived the attachment: " . $model->id, null, null);
               $outputResponse = ['success'=>true, 'message' => 'Deleted the school plan attachment succesful']; 
            }
        
        } else {
              
            $outputResponse = ['success'=>false, 'message' => 'Failed to delete the attachment because you are not author']; 
        }
    
        
        $subActivityModel = SchoolPlanSubActivity::findOne($model->subject_id);
        
        return Yii::$app->runAction('/planner/planner-sub-activity/view', ['id' => $subActivityModel->id]);

    }
}

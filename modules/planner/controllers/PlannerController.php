<?php

namespace app\modules\planner\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\controllers\BaseController;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\SchoolPlan;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use app\modules\planner\models\SchoolPlanSearch;
use app\modules\planner\models\SchoolPlanActivity;
use app\modules\planner\models\SchoolPlanChanges;
use app\modules\planner\models\SchoolPlanChangeSearch;
use app\modules\planner\models\SchoolPlanAttachmentSearch;
use app\modules\planner\models\SchoolPlanComment;
use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\planner\models\SchoolPlanManagers;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use yii\base\DynamicModel;

if (!Yii::$app->session->isActive) {
    session_start();
}



/**
 * PlannerController implements the CRUD actions for SchoolPlan model.
 */
class PlannerController extends BaseController
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }




    /**
     * Deletes an existing SchoolPlan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        if (!Yii::$app->user->can('rw_planner') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $model = $this->findModel($id);
        $userId = Yii::$app->user->identity->getId();

        if ($model->created_by == $userId) {

            //only delete school plan  if it is not attached to activity
            $activityList = SchoolPlanActivity::find()
                                        ->where(['plan_id' => $id])
                                        ->asArray()
                                        ->all();

            if (count($activityList) > 0 ) {
                throw new ForbiddenHttpException('You cannot delete the school plan  if it is still attached to activities');
            }


            if ($model->delete()) {
               Logs::logEvent("Deleted the school plan : " . $model->id, null, null);
               return $this->redirect(['index']);
            }

        } else {
            throw new ForbiddenHttpException('No permission to delete the school plan');
        }


        return $this->redirect(['index']);
    }

    /**
     * Creates a new SchoolPlan model
     * @return SchoolPlan
     */
    public function newModel()
    {
        $model = new SchoolPlan();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return SchoolPlanSearch
     */
    public function newSearchModel()
    {
        $searchModel = new SchoolPlanSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create School Plan';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionLists($id = 0)
    {
        if(!$id) $id = 0;
        $countSchoolPlans = SchoolPlan::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'plan_name', '__ARCHIVE__'])
            ->count();

        $schoolPlans = SchoolPlan::find()
            ->where(['school_id' => $id])
            ->andWhere(['<>', 'plan_name', '__ARCHIVE__'])
            ->orderBy('plan_name')
            ->all();

        Yii::trace($schoolPlans);

        if ($countSchoolPlans > 0) {
            echo "<option value=''> Select School Plan </option>";
            foreach ($schoolPlans as $myPlan) {
                echo "<option value='" . $myPlan->id . "'>" . $myPlan->plan_name . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }


    /**
     * Display the default school plans list
     */

    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view schools plans.');

        $request = Yii::$app->request;
        $searchModel = new SchoolPlanSearch();
        $schoolId = Yii::$app->user->identity->school_id;

        $searchParams = Yii::$app->request->queryParams;
        $searchParams['school_id'] = $schoolId;
        //$dataProvider = $searchModel->searchNewIndex(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchNewIndex($searchParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }

    /**
     * Displays a single SchoolPlan model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) //needs refactor too
    {

        if (empty(Yii::$app->user->identity) ) {

            return \Yii::$app->runAction('/site/login');
        }

        $model = $this->findModel($id);
        //project manager status.
        $this->determineProjectManagerStatus($model->managed_by,$model->assisted_by );
        if (Yii::$app->user->can('r_planner', ['sch' => $model->id])) {
            return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
                'model' => $model  ]) :
                $this->render('view', ['model' => $model]);


        } else {
            throw new ForbiddenHttpException('No permissions to view school plans');
        }
    }


    /**
     * Creates a new School Plan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_planner')) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolPlan();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();
                $model->school_id = Yii::$app->user->identity->school_id;

                if( $model->save(false) ){
                     Logs::logEvent("Created New School Plan: " . $model->id, null, null);
                }

                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to new school plan : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

    public function actionTermsList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, term_name AS text')
                ->from('core_term')
                ->where(['ilike', 'term_name', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id]);
                //->limit(5);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => CoreTerm::find($id)->term_name];
        }
        return $out;
    }


    /**
     * Display the School PLan activities
     */

    public function actionViewSchoolPlanActivity()
    {
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view schools plans activities');

        $request = Yii::$app->request;
        $searchModel = new SchoolPlanSearch();
        $dataProvider = $searchModel->searchNewIndex(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('planner/planner-activity/index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);
    }

    /**
     * Retrieves the staff information for particular school.
     *  @return mixed
     */
    public function actionStaffList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if (!is_null($q)) {
            $query = new Query;

            $query->select(['id', 'first_name', 'last_name'])
                ->from('core_staff')
                ->where(['ilike', 'first_name', $q])
                ->orWhere(['ilike', 'last_name', $q])
                ->andWhere(['school_id'=>Yii::$app->user->identity->school_id])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();

            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['first_name'] ." ". $value['last_name']
                ));
            }

            $out['results'] = $staffData;

        }
        return $out;
    }


    /**
     * Updates an existing SchoolPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('rw_planner')) {


            if (empty(Yii::$app->user->identity) ) {

                return \Yii::$app->runAction('/site/login');
            }

            $model = $this->findModel($id);

            $req = Yii::$app->request;

            $model->date_modified = date('Y-m-d H:i:s');

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                 //track the edit changes.
                $postData = Yii::$app->request->post();
                $loggedInUser = Yii::$app->user->identity->getId();
                $schoolPlan = $postData['SchoolPlan'];
                $projectChanges = array(
                    'title' => $schoolPlan['plan_name'],
                    'description' => $schoolPlan['description'],
                    'expected_start_date' => $schoolPlan['expected_start_date'],
                    'expected_end_date' => $schoolPlan['expected_end_date'],
                    'actual_start_date' => $schoolPlan['actual_start_date'],
                    'actual_end_date' => $schoolPlan['actual_end_date'],
                    //'term_id' => $postData['term_id'],
                    'category' => 'school_plan',
                    'updated_by' => $loggedInUser,
                    'subject_id' => $id,
                    'planner_id' => $id
                );

                SchoolPlanChanges::saveProjectChanges($projectChanges);

                return Yii::$app->runAction('/planner/planner/view', ['id' => $model->id]);

            } else {

                $model->expected_start_date = date('Y-m-d', strtotime($model->expected_start_date));

                $model->expected_end_date = date('Y-m-d', strtotime($model->expected_end_date));

                $model->actual_start_date = ($model->actual_start_date)? date('Y-m-d', strtotime($model->actual_start_date)) : '' ;

                $model->actual_end_date =  ($model->actual_end_date)? date('Y-m-d', strtotime($model->actual_end_date)) : '';

                return ($req->isAjax) ? $this->renderAjax('update', [
                    'model' => $model,
                ]) : $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new ForbiddenHttpException('No permissions to edit school planner information');
        }


    }

    /**
     * Determine the project manager for the school_plan
     * @param integer $managed_by
     * @param integer $assisted_by
     * @return mixed
     */
    private function determineProjectManagerStatus($managed_by,$assisted_by)
    {
        $session = Yii::$app->session;
        $loggedInUser = Yii::$app->user->identity->getId();
        $user = User::findOne($loggedInUser);

        if (!empty($user)) {
            // managed_by for school_plan represents the project manager.
            if ($user->school_user_id === $managed_by) {
                 $session->set('project-manager', true);
            } else {
                //check if u r second PM
                if ($user->school_user_id == $assisted_by) {
                    $session->set('project-manager', true);
                }else{
                    $session->set('project-manager', false);
                }
            }

        }
    }


    /**
     * Show the settings  for project.
     * @return mixed
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSetting()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->user->can('r_planner'))
            throw new ForbiddenHttpException('No permissions to view school planner settings');

        $session = Yii::$app->session;
        $isProjectManager = $session->get('project-manager');
        if (!$isProjectManager) {
           throw new ForbiddenHttpException('Only the project Managers are authorised to view planner settings');
        }

        $planId = $request->get('planId');
         //if u own the school
        $schoolId = Yii::$app->user->identity->school_id;

        $resultData = array();

        $schoolPlan = SchoolPlan::findOne($planId);
        $planTitle = $schoolPlan ? $schoolPlan->plan_name: '';
        $emailAlertChecked = $schoolPlan? $schoolPlan->receive_due_alerts:'';

        $searchParams = array(
            'planner_id' => $planId,
            'startDate' => '',
            'endDate' => ''
        );

        $queryParams = Yii::$app->request->queryParams;

        $searchModel = new SchoolPlanChanges();
        $dataProvider = $searchModel->searchProjectAdjustments($queryParams);

        $searchAttachmentModel = new SchoolPlanAttachmentSearch();
        $attachmentList = $searchAttachmentModel->searchDeletedAttachments($queryParams);
        $commentList = SchoolPlanComment::fetchDeletedComments($queryParams);

        $colorData = $this->fetchActivityColorForPlanner($planId);

        $staffData = CoreStaff::find()->where(['school_id' => $schoolId ])->asArray()->all();
        $staffList = ArrayHelper::map($staffData, 'id', function($array,$default){
            return $array['first_name'] . ' '. $array['last_name'];
        });


        $responseData = [
            'planTitle' => $planTitle,
            'plannerChanges' => $dataProvider,
            'searchModel' => $searchModel,
            'emailAlertChecked' => $emailAlertChecked,
            'attachmentList' => $attachmentList,
            'commentList' => $commentList,
            'colorData' => $colorData,
            'staffList' => $staffList
        ];

        return ($request->isAjax) ?
             $this->renderAjax('_setting',$responseData )
                :
            $this->render('_setting', $responseData);

    }

    /**
     * Enable/Disable the reminder due alerts that are supposed to be received
     * by project manager
     */
    public function actionModifyReceivingAlerts()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $response = [
            'success'=>false,
            'message' => 'Failed to modify the ability to receive the due date email alerts'
        ];

        $postData = Yii::$app->request;

        $model = $this->findModel($postData->post('planId'));

        if ($postData->post('receive_alerts') == 'enabled') {
            $model->receive_due_alerts = true;
        } else {
            $model->receive_due_alerts = false;
        }

        if ( $model->save(false)) {
            Logs::logEvent("Updated  the ability to receive due date alerts : " . $model->id, null, null);
            $response = [
                'success'=>true,
                'message' => 'Successfully updated  the ability to receive the due date email alerts'
            ];

        }

        return $response;

    }


    /**
     * Deletes an existing SchoolPlanComment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDeleteComment($id)
    {
        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try {

            //$this->findCommentModel($id)->delete();
            //only the owner is allowed to delete the comment.
            $userId = Yii::$app->user->identity->getId();

            $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when deleting comment'];

            //only the PM has permission to delete  comment forever
            $model = $this->findCommentModel($id);
            $session = Yii::$app->session;
            $isProjectManager = $session->get('project-manager');
            if ($isProjectManager == true) {
                $model->delete();
                   Logs::logEvent("Deleted  the school plan comment forever: " . $model->id, null, null);
                   $outputResponse = ['success'=>true, 'message' => 'Deleted the school comment succesful'];
            } else {

                $outputResponse = ['success'=>false, 'message' => 'Failed to delete the comment because you are not authroised'];
            }


            return $outputResponse;

        } catch (\Exception $e) {

            $model_error = $e->getMessage();

            Logs::logEvent("Failed to delete  the school comment  : ", $model_error, null);

            $outputResponse = ['success'=>false, 'message' => 'Failed to delete the school comment'];

            return $outputResponse;
        }

    }

    /**
     * Finds the SchoolPlanComment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SchoolPlanComment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findCommentModel($id)
    {
        if (($model = SchoolPlanComment::findOne($id)) !== null) {
             return $model;
        } else {
            throw new NotFoundHttpException('The requested school plan comment does not exist.');
        }
    }


     /**
     * Adjust the colors of the activity progress
     * @return mixed
     */
    public function actionAdjustActivityColors()
    {
        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        $userId = Yii::$app->user->identity->getId();

        $request = Yii::$app->request;

        $planId = $request->get('planId');
            //color changes
        $poor_progress = $request->post('poor_progress');
        $average_progress = $request->post('average_progress');
        $good_progress = $request->post('good_progress');
        $excellent_progress = $request->post('excellent_progress');


        $outputResponse = ['success'=>false, 'message' => 'Something wrong happened when updating teh colors'];

            //only the PM has permission to adjust the activity colors

        $session = Yii::$app->session;
        $isProjectManager = $session->get('project-manager');
        if ($isProjectManager == true) {

            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();

            try{

                    $sql1 = "SELECT * FROM school_plan_colors WHERE school_plan_id=:school_plan_id";
                    //$rows = $db->createCommand($sql1)->queryAll();
                    $sql1 = $connection->createCommand($sql1);
                    $sql1->bindValue(':school_plan_id', $planId);
                    $rows = $sql1->queryAll();

                    if (count($rows)> 0 ) {

                        $sql2 = "UPDATE school_plan_colors SET poor_progress=:poor_progress,average_progress=:average_progress,good_progress=:good_progress,excellent_progress=:excellent_progress WHERE school_plan_id=:school_plan_id";
                        $sql2 = $connection->createCommand($sql2);
                        $sql2->bindValue(':poor_progress', $poor_progress);
                        $sql2->bindValue(':average_progress', $average_progress);
                        $sql2->bindValue(':good_progress', $good_progress);
                        $sql2->bindValue(':excellent_progress', $excellent_progress);
                        $sql2->bindValue(':school_plan_id', $planId);
                        $sql2->execute();

                    } else {

                        $sql3 = "INSERT INTO school_plan_colors (
                                     school_plan_id,poor_progress,average_progress,good_progress,
                                     excellent_progress,date_created,created_by
                                    )
                                    VALUES (
                                         :school_plan_id,:poor_progress,:average_progress,:good_progress,
                                         :excellent_progress,NOW(), :created_by
                                    )";

                                    $sqlQuery = $connection->createCommand($sql3);
                                    $sqlQuery->bindValue(':school_plan_id', $planId);
                                    $sqlQuery->bindValue(':poor_progress', $poor_progress);
                                    $sqlQuery->bindValue(':average_progress', $average_progress);
                                    $sqlQuery->bindValue(':good_progress', $good_progress);
                                    $sqlQuery->bindValue(':excellent_progress', $excellent_progress);
                                    $sqlQuery->bindValue(':created_by', $userId);
                                   //insert the color for the activity progress
                                   $sqlQuery->execute();
                    }

                    $transaction->commit();
                    Logs::logEvent("Adjusted the color of the activity progress for planner: " . $planId, null, null);
                    $outputResponse = ['success'=>true, 'message' => 'Adjusted the colors of activity progress successful'];

                }catch(\Exception $e){

                    $transaction->rollBack();
                    $model_error = $e->getMessage();

                    Logs::logEvent("Failed to adjust the activity colors  : ", $model_error, null);
                    $outputResponse = ['success'=>false, 'message' => 'Failed to adjust teh activity colors'];

                }

            } else {

                $outputResponse = ['success'=>false, 'message' => 'Failed to adjust the activity colors because you are not authorised as project Manager'];

            }


            return $outputResponse;

    }

    /**
     * Transfer this method to the data model maybe for the school_plan to be resused
     * Fetch the  saved colors for the specific school_plan
     * @param integer $planId
     * @return mixed
     */
    private function fetchActivityColorForPlanner($planId)
    {
        $colors = array();
        //$colors = ['red', 'blue', 'green', 'yellow'];
        $defaultColors = ['#ff0000','#2f7ed8','#006400','#ffff00'];
        $connection = Yii::$app->db;

        //check if the project has any saved color codes.
        $sqlQuery = "SELECT * FROM school_plan_colors WHERE school_plan_id=:school_plan_id";
        $sqlQuery = $connection->createCommand($sqlQuery);
        $sqlQuery->bindValue(':school_plan_id', $planId);
        $colorData = $sqlQuery->queryAll();

        if (count($colorData)> 0) {
            $savedColors = $colorData[0];
            Yii::trace("what are saved colors");
            Yii::trace($savedColors);
            if (!empty($savedColors)) {
                $colors = array(
                    $savedColors['poor_progress'], $savedColors['average_progress'],
                    $savedColors['good_progress'],$savedColors['excellent_progress'],
                );
            }
        }

        $finalColors =  !empty($colors) ? $colors : $defaultColors;

        return $finalColors;

    }

     /**
     * Adjust the project managers for the school plan
     * Work on this if multi PM is really required
     * Comment it out later if not needed
     * @return mixed
     */
    public function actionModifyProjectManagers()
    {
        if (!Yii::$app->user->can('rw_planner') ) {
            throw new ForbiddenHttpException('No permission to Write or Update to the School Planner');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        $userId = Yii::$app->user->identity->getId();

        $request = Yii::$app->request;

        $planId = $request->get('planId');

        $projectList = $request->post('projectList');
        $splitProjectList = preg_split('/[\ \n\,]+/', $projectList);

        $plannerManagers = SchoolPlanManagers::find()->where(['plan_id' => $planId])->asArray()->all;
        //how do we remove de-selected PM
        // foreach ($splitProjectList as $staffId) {

        // }
        //work on this if David persists for this function..
        // just add the project_manager to school_plan
        if (count($plannerManagers) > 0 ) {
            foreach ($plannerManagers as $manager) {

            }
        }else{

            foreach($splitProjectList as $staffId){

            }
        }

        $outputResponse = [
            "success" => false,
            "message" => 'Failed to adjust the project managers for school plan'
        ];

        return $outputResponse;

    }


    public function actionMenu(){
       // return $this->render('menu');

        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('menu') :
            $this->render('menu');
    }


}

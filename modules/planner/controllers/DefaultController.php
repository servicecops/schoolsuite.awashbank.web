<?php

namespace app\modules\planner\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `schoolcore` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        //return $this->render('index');

        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('index') :
            $this->render('index');
    }
}

<?php

use yii\helpers\Html;
use miloschuman\highcharts\HighchartsAsset;
use yii\web\JsExpression;
use yii\web\View;


$this->title = 'Planner Performance';
$this->params['breadcrumbs'][] = $this->title;

HighchartsAsset::register($this)->withScripts(['modules/exporting', 'modules/drilldown']);
?>
<div class="core-school-index">
    <div class = "row letter">
        <div class = "col-md-6">
           <h3>School Planner Perfomance - <?= Html::encode($planName) ?></h3>
        </div>
    </div>

    <br></br>

    <div class = "row">
        <div class = "col-md-12">
            <div class="card-group">
                <div class="card border-right">
                    <div class="card-body">
                        <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                        <div class="d-inline-flex align-items-center">
                            <h3 class="text-dark mb-1 font-weight-medium">Project Manager</h3>
                        </div>
                            <h4 class="text-muted font-weight-normal mb-0 w-100 text-truncate" id = "project-manager">Goldsoft</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h3 class="text-dark mb-1 w-100 text-truncate font-weight-medium" id = "project-activity-count">5</h3>
                            <h4 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Activities</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card border-right">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <div class="d-inline-flex align-items-center">
                                <h3 class="text-dark mb-1 font-weight-medium" id = "tasks-completed">55%</h3>
                            </div>
                            <h4 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Tasks Completed</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row card">
                <div class="card-body">
                    <div class="d-flex d-lg-flex d-md-block align-items-center">
                        <div>
                            <h3 class="text-dark mb-1 font-weight-medium" id = "tasks-incomplete">45%</h3>
                            <h4 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Tasks Incomplete</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
      </div>
    </div> <!-- end of the row -->
   
   <br></br>
    <div class = "row letter">
        <div class = "col-md-12">
            <div class="main-content card">
                <div class="card-body">
                    <div id = "chart-container">
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

 
 $script = <<< JS
    
    $("document").ready(function($){
        
        //fetch the performance metrics.
        var hostUrl = window.location.pathname;
            actionUrl = hostUrl.replace(/view/gi, "view-single-performance"),
            queryParams = new URLSearchParams(window.location.search);
            actionUrl = actionUrl + '?plan-name=' + queryParams.get('plan-name');

        $.get(actionUrl, function(data, status ){

            if (data.success === true) {
                var chartData = JSON.parse(data.message);
                createPerformanceChart(chartData)
                constructProjectStats(chartData.projectDetail)
            } else {
                alert('Failed to load the performance metrics') 
            } 
        })

    
        // Create the chart
        function createPerformanceChart(chartData){

            Highcharts.chart('chart-container', {
                chart: {
                      type: 'column'
                },
                title: {
                     text: 'Performance  Metrics for Activities'
                },
                subtitle: {
                    text: 'Click the activity columns to view progress of sub-activities'
                },
                xAxis: {
                      type: 'category'
                },
                yAxis: {
                    title: {
                        text: '% of progress performance'
                    }
                },
                legend: {
                  enabled: false
                },
                plotOptions: {
                    series: {
                       borderWidth: 0,
                       dataLabels: {
                           enabled: true,
                           format: '{point.y:.1f}%'
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [
                    {
                        name: "Activities",
                        colorByPoint: true,
                        data: chartData.activities
                    }
                ],
                drilldown: {
                   series: chartData.drillDown
                }
            });
        }
        
        //project statitics overview
        function constructProjectStats(project){
            
            $('h4#project-manager').text(project.projectManager)
            $('h3#project-activity-count').text(project.activityCount)
            $('h3#tasks-completed').text(project.averageProgress + '%')
            $('h3#tasks-incomplete').text(project.incompleteProgress + '%')
        }
    

    });

JS;
  $this->registerJS($script);
?>

<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\SchoolPlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Planner Performance';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-index">
    <div class = "row letter">
        <div class = "col-md-6">
           <h3>School Planner Performance</h3>
        </div>
    </div>

    <br></br>

    <div class = "row letter">
        <div class = "col-md-12">
             
               <?= Highcharts::widget([
                        'scripts' => [
                            'modules/exporting',
                            // 'themes/gray'
                        ],
                        'options' => [
                            'exporting'=>[
                                'enabled'=>true
                            ],
                            'credits'=>[
                                'enabled'=>false
                            ],
                            'chart'=> [
                                'type' => 'column',
                                'backgroundColor'=>'#fff'
                            ],
                            //'colors'=>['#046DC9','#FE2626', '#FFE600', '#48CFF4', '#AB0E96', '#E8BB1A'],
                            'colors' => ['#046DC9','#008000'],
                            'title' => ['text' => '<b>Average School Plans Performance</b>'],
                            'xAxis' => [
                                'categories' => $plans,
                                'crosshair' => true
                            ],
                            'yAxis' => [
                                'title' => ['text' => 'Progress % '],
                                'min' => 0   
                            ],
                            'plotOptions'=> [
                                'column' => [
                                    'stacking' => 'percent'
                                ],
                                'series' => [
                                    //'allowPointSelect' => true,
                                    'point' =>[
                                        'events'=> [
                                            'click'=> new JSExpression(
                                                 "showSubTask"
                                            )
                                        ]
                                    ]
                                ]
                            ],
                            'tooltip' => [
                                'pointFormat' => '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                                'shared' => true
                            ],
                            'series' => $planTaskData

                        ]
                ]); 
            ?>
        </div>
    </div>

    <br>

    <div class = "row letter">
        <div class = "col-md-8">
               <!--piecharts-->
            <?php
               echo Highcharts::widget([
                'scripts' => [
                    'highcharts-3d',
                    //'themes/grid-light',
                ],
                'options' => [
                    'exporting'=>[
                        'enabled'=>false
                    ],
                    'legend'=>[
                        'align'=>'left',
                        'verticalAlign'=>'bottom',
                        'layout'=>'horizontal',
                    ],
                    'credits'=>[
                        'enabled'=>false
                    ],
                    //'colors'=>["#43C6DB", '#F7A35C', "#F17CB0", '#2B908F', '#808080', '#7999d5', '#ffff36', '#F45B5B'],
                    'colors'=>["#2B908F","#43C6DB","#F17CB0"], 
                    'chart'=> [
                        'backgroundColor'=>'#fff',
                        'type'=>'pie',
                        'options3d'=>[
                            'enabled'=>true,

                            'alpha'=>45,
                            'beta'=>7
                        ]
                    ],
                    'title'=>[
                        'text'=>'<b>Project Manager Performance</b>',
                        'margin'=>0,
                    ],
                    'plotOptions'=>[
                        'pie'=>[
                            'allowPointSelect'=> true,
                            'depth'=>20,
                            'innerSize'=>40,
                            'cursor' =>'pointer',
                            'dataLabels'=>[
                                'enabled'=>true,
                                'format' => '<b>{point.name}</b>: {point.percentage:.1f} %'
                            ],
                            'showInLegend'=>true,
                        ],

                    ],
                    'tooltip' => [
   
                        'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
                    ],
                    'series'=> [
                        [
                            'name'=>'Tasks completed',
                            'colorByPoint'=> true,
                            'data' => $projectManagers,
                        ],
                    ]
                ],
            ]);
            ?>
        </div>

        <div class = "col-md-4" id = "rankList">
            <h5><b>Best Performing Project Managers </b></h5>
            <?php
               echo '<ol>';
                foreach ($rankList as $rank) {
                   echo '<li>';
                   echo '<p><b>'.$rank['name'] . ' -  ' . $rank['averagePerformance'] . '% </b></p>';
                   echo '<p> Rating - ' . $rank['rating'] . '</p>';
                   echo '</li>';
                }
                echo '</ol>';
            ?>
        </div>
    </div>

    </div>

</div>


<?php

 
 $script = <<< JS

    function showSubTask(evt){

        console.log('hello subtask', evt.point.category)

         let planName = evt.point.category,
             hostUrl = window.location.pathname;

        console.log('host url', hostUrl)
        if(planName !== ""){
            let planUrl = hostUrl.replace(/index/gi, "view?plan-name="+ planName)
            window.location.href = planUrl
            //console.log('task url', planUrl)
        }

    }

JS;
  $this->registerJS($script);
?>
<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use app\modules\planner\models\SchoolPlanActivity;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $staffList */
/* @var $commentList */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="classes-view hd-title"
     data-title="<?=  $model->title  ?>">
</div>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class = "col"><h5><b>SubActivity</b> - <?= ($model->title) ? $model->title : "--" ?></h5></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
           <?= Html::a('<i class="fa fa-eye">Budgeting</i>', [
                  '/planner/planner-budget/index', 
                  'activityId' => $model->id,
                  'activityType' => 'subActivity'
                ],
                [
                  'class' => 'btn btn-sm btn-primary'
                ]) 
            ?>
            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this sub Activity?',
                    'method' => 'post',
                ],
            ]) 
            ?>
            <?= Html::a('<i class="fa fa-arrow-left" >Activity</i>', ['/planner/planner-activity/view', 'id'=> $model->plan_activity_id], ['class' => 'btn btn-sm btn-primary']) ?>
        </p>
    </div>

</div>

</div> <!--end of card container -->
<br>

<div class = "letter">

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('title') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? $model->title : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">SubActivity Starts</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->expected_start_date) ? $model->expected_start_date : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">SubActivity Ends</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->expected_end_date) ? $model->expected_end_date : "--" ?></div>
        </div>
    </div>
</div>
<div class = "row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->description) ? $model->description : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Work Completion (%)</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->progress) ? $model->progress : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> School Plan Activity</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->plan_activity_id) ? SchoolPlanActivity::findOne($model->plan_activity_id)->title : "--" ?></div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Managed By </div>
            <div class="col-lg-6 col-xs-9 profile-text">
                <?php
                   if (!empty($staffList)) {
                    foreach ($staffList as $staff) {
                        echo '<li>';
                        echo $staff['first_name'] . " " . $staff['last_name']; 
                        echo '<br>';
                        echo '</li>';
                      }
                    }
                ?>
            </div>
        </div>
    </div>

</div>

</div>
<br></br>

<div class = "letter">
<div class = "row comments-header">
    <div class="float-left">
        <button type="button" class="add-item btn btn-primary" id = "show-comments"><i class="fa fa-comment">&nbsp;Show comments</i></button>
        <button type="button" class="add-item btn btn-primary" id = "show-attachments"><i class="fa fa-paperclip">&nbsp;Show Attachments</i></button>
        <button type="button" class="add-item btn btn-primary" id = "show-due-date"><i class="fa fa-calendar">&nbsp;Set Deadline</i></button>
    </div>
</div>


<div class = "due-date-container">
    <div class = "row">
        <div class = "float-left">
            <h4>Set Due Date </h4>
        </div>
    </div>
    <?php 
        $url = Url::to(['/planner/planner-sub-activity/set-task-deadline']);
        $form = ActiveForm::begin([
                'options' => ['class' => 'due-date-form'],
                'action' => ['/planner/planner-sub-activity/set-task-deadline']
        ]); 
    ?>
        <div class="row">
            <div class="col-md-4">
               <div class = <?= "task-deadline-box"?>>
                   <?= $dueDateText ?>
                </div>
                <div class = "due-date-alert">
                   <p>The due date has been successful set</p>
                </div>
                <?php

                   echo $form->field($model, 'task_due_date')->widget(DateTimePicker::classname(), [
	                   'options' => ['placeholder' => 'Enter the due time ...'],
	                   'pluginOptions' => [
		                   'autoclose' => true,
                           'todayBtn' => true
	                    ]
                    ]);
                ?>
                <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false); ?>
            </div>
        </div>
        <div class = "row">
            <div class="col-md-4">
                    <?php
                        
                        $selectedDuration = empty($model->reminder_alert_duration)? $model->reminder_alert_duration : '';
                        echo $form->field($model, 'reminder_alert_duration')->widget(Select2::classname(), [
                            'data' => $taskReminders,
                            'initValueText' => $selectedDuration, //l set the initial display text
                            'options' => ['placeholder' => 'Set due date reminder ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                    ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xm-4">
                <?= Html::submitButton( 'Save', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
        </div>
       
    <?php ActiveForm::end(); ?>
 </div>

<?= $this->render('_comments', [
      'model' => $model,
      'commentList' => $commentList
]) ?>

<?= $this->render('/planner-attachment/index', [
      'model' => $attachmentModel,
      'attachmentList' => $attachmentList
]) ?>

</div>

<?php
$script = <<< JS
$("document").ready(function($){
    
    // show only the comments container
    $('#show-comments').click(function(){
        $('.attachment-container').hide();
        $('.due-date-container').hide();
        $('.comment-container').show();
    })
    
    //show the attachment container
    $('#show-attachments').click(function(){
        $('.comment-container').hide();
        $('.due-date-container').hide();
        $('.attachment-container').show();
    })

     //show the due-date container
     $('#show-due-date').click(function(){
        $('.comment-container').hide();
        $('.attachment-container').hide();
        $('.due-date-container').show();
    })
    
    $(".due-date-form").submit(function(event) {

        event.preventDefault(); // stopping submitting
        var formData = $(this).serializeArray(),
            url = $(this).attr('action');
           
        $.post(url, formData, function(response){
            if (response.success === true) {
                var dueDateText = "You still have some time left to accomplish the task"
                $('.due-date-alert').show().fadeOut(2000)
                $('.task-deadline-box').find("p").text(dueDateText).addClass("due-text-info");
            } else {

                var alertDueDate = $('.due-date-alert').show().find("p");
                    //alertDueDate.text("Failed to set the due date").css('color', 'red').fadeOut(2000);
                    alertDueDate.text(response.message).css('color', 'red').fadeOut(3000);
                
            }
        });
        
    });
    
})

JS;
$this->registerJs($script);
?>


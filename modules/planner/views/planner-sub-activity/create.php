<?php

use app\modules\planner\planner\SchoolPlanSubActivity;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\planner\planner\SchoolPlanSubActivity*/
  
//$model->id = id;
?>
<div class="core-school-class-create">


    <?= $this->render('_form', [
        'model' => $model, 'activityStaffModel'=>$activityStaffModel
    ]) ?>

</div>

<?php
   use app\models\User;
   use dosamigos\ckeditor\CKEditor;
   use yii\helpers\Html;

?>
<!-- the comments section -->
<div class = "comment-container">
    <div class = "row">
        <div class = "float-left">
            <h4>Comments</h4>
        </div>
    </div>
    <div class = "row">
        <div class = "col-xs-6">

            <form role="form" id = "add-comment">
                <div class="form-group">
                     <?php CKEditor::begin([ 
                         'name'=> 'comment_text', 
                         'preset' => 'basic',
                         'options' => ['rows' => 3],
                         'id'=> 'add-comment-ckeditor'
                     ]);?>
                      <?php CKEditor::end();?>
                </div>
                <div class="form-group">
                   <button type="submit" class="add-item btn btn-success"><i class="fa fa-plus">Add Comment</i></button>
                </div>
            </form>
        </div>
    </div>
    <br></br>
    <div class = "row">
        <div class = "col-xs-12">
            <div class="actionBox">
                <ul class="commentList">
                    <?php   
                        if (!empty($commentList)) {
                            foreach ($commentList as $comment) {
                                $posted_on = $comment['date_created'] ? Yii::$app->formatter->asDateTime($comment['date_created']) : '';
                                $commentUser = User::findOne($comment['created_by']);
                               
                                $comment_author =  $commentUser ? $commentUser->username: 'Anonymous';
                                $author_initials =  $commentUser ? substr($commentUser->lastname, 0, 1) . substr($commentUser->firstname, 0, 1) : 'AA';
                                $author_initials = strtoupper($author_initials);
                                echo '<li>';
                                echo '<div class="commenterImage"><div class = "comment-initials-box"><span>' . $author_initials . '</span></div></div>';
                                echo '<div class="commentText">';
                                echo '<div  class="notes">'. $comment['notes'] .'</div><span class="date sub-text">';
                                echo 'Posted by <a href = "javascript:void(0);">' .$comment_author .'</a> On ' .$posted_on .'</span>';
                                echo '<div class = "commentAction">';
                                echo '<a href = "javascript:void(0);"><span><i class="fa fa-edit edit-comment" data-comment-id="' .$comment['id'].'" style="font-size:15px;color:#87CEFA;">Edit</i> </span></a>&nbsp;&nbsp;';
                                echo '<a href = "javascript:void(0);"><span><i class="fa fa-trash delete-comment" data-comment-id="' .$comment['id'].'"  style="font-size:15px;color:#87CEFA;">Delete</i></span></a>';
                                echo '</div>';
                                echo '</div>';
                                echo '</li>';
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div> <!--end of comments div -->

<?php
 $script = <<< JS
 $("document").ready(function($){ 
   
   //submitting comment.
    $('#add-comment').submit(function(evt){
        evt.preventDefault();

        var comment_text = $('#add-comment-ckeditor').val(),
            hostUrl = window.location.pathname,
            postUrl = hostUrl.replace(/view/gi, "create-comment"),
            //postUrl = '<?= Url::to(['/planner/planner-subactivity/create-comment']); ?>',
           queryParams = new URLSearchParams(window.location.search);
        
        var formData = {
            comment_text: comment_text,
            subject_type: 'planner_subactivity',
            sub_activity_id: queryParams.get('id')
        }

        if (formData.comment_text === "") {
            alert('comment text cannot be empty')
            return;
        }
        var commentText = $('#add-comment-ckeditor').val();

        $.post(postUrl, formData, function(response){
            if (response.success === true) {
                
                var comment = response.message
                var listRow = '<li>' + 
                               '<div class="commenterImage">' + 
                                  '<div class = "comment-initials-box"><span>' + comment.author_initials + '</span></div></div>' +
                               '<div class="commentText">' + 
                               '<div class="notes">' + comment.notes + '</div><span class="date sub-text">' + 
                               'Posted by <a href = "javascript:void(0);">' + comment.created_by + 
                               '</a> On ' + comment.date_created + '</span>' +
                               '<div class = "commentAction">' +
                               '<a href = "javascript:void(0);"><span><i class="fa fa-edit edit-comment" data-comment-id="' + comment.id + '" style="font-size:15px;color:#87CEFA;">Edit</i> </span></a>&nbsp;&nbsp;' +
                               '<a href = "javascript:void(0);"><span><i class="fa fa-trash delete-comment" data-comment-id="' + comment.id + '" style="font-size:15px;color:#87CEFA;">Delete</i></span></a>' +
                               '<div></li>';

                $('.commentList').prepend(listRow);
                //$('#comment_text').val('');
                $(".cke_wysiwyg_frame").contents().find("body").html("");

            } else {
                console.log('error ', response)
                alert('Failed to add the comment')
            }
        })

    })
    
 
    //editing the comment
    $(document).on('click', '.edit-comment', function(e){

        e.preventDefault()

        var commentTextElem = $(this).parent().parent().parent().parent().find('.notes'),
            commentText = commentTextElem.text(),
            commentId = $(this).data('comment-id')

        var commentFormBox = '<form class="edit-form">' +
                              '<textarea class="form-control edit-input" type="text"  rows="2" cols="20"></textarea><br>' +
                              '<button type="submit" class="add-item btn btn-success">save</button>' +
                              '</form>';

        var deleteButton = $(this).parent().parent().parent().find('.delete-comment')
        deleteButton.hide();

        var editBox = $(this);
        $(this).replaceWith(commentFormBox)
        $('.edit-input').val(commentText)

        $('.edit-form').submit(function(event){
           event.preventDefault();
           var editedValue = $('.edit-input').val()

            var formData = {
                comment_text: editedValue,
                comment_id: commentId
            }

            var hostUrl = window.location.pathname,
                actionUrl = hostUrl.replace(/view/gi, "update-comment")
           
            $.ajax({
                url: actionUrl,
                type: 'PUT',
                data: formData,
                success: function(response){
                    if (response.success === true) {
                        var commentResponse = response.message;
                        commentTextElem.text(commentResponse.notes);
                        $('.edit-form').hide()
                        $('.edit-form').replaceWith(editBox);
                        deleteButton.show();

                    }else{
                          
                        alert('Failed to edit the comment')
                    }
                }
            })
        })
    });

    //deleting the comment.
    $(document).on('click', '.delete-comment', function(e){
        e.preventDefault()
        var confirmAction = window.confirm('Are you sure that you want to delete the comment')

        if (confirmAction === true) {
            var listElement = $(this).parents("li").last();
            commentId = $(this).data('comment-id')      
            deleteCommentAction(commentId,listElement)
        }
    
    });

    function deleteCommentAction(commentId, listElement){
        
        var hostUrl = window.location.pathname,
            actionUrl = hostUrl.replace(/view/gi, "delete-comment"),

            deleteUrl = actionUrl + '?id='+ commentId;
            
            $.ajax({
                url: deleteUrl,
                type: 'DELETE',
                success: function(response){
                    console.log('delete response', response)
                    if (response.success === true) {
                        //delete the parent li from the comment box
                        listElement.remove()

                    }else{
                          
                        //alert('Failed to delete the comment')
                        alert(response.message)
                    }
                }
            })
        
    }
   
    //Generic confirmation
    function confirm(heading, question, cancelButtonTxt, okButtonTxt, callback) {
        console.log('hello confirm')
        var confirmModal = 
            $('<div class="modal hide fade">' +    
               '<div class="modal-header">' +
                    '<a class="close" data-dismiss="modal" >&times;</a>' +
                    '<h3>' + heading +'</h3>' +
                '</div>' +

                '<div class="modal-body">' +
                    '<p>' + question + '</p>' +
                '</div>' +

                '<div class="modal-footer">' +
                    '<a href="#" class="btn" data-dismiss="modal">' +  cancelButtonTxt + '</a>' +
                    '<a href="#" id="okButton" class="btn btn-primary">' +  okButtonTxt + 
                    '</a>' +
                '</div>' +
            '</div>');

            confirmModal.find('#okButton').click(function(event) {
                callback();
                confirmModal.modal('hide');
            });

            confirmModal.modal('show');     
        };
 });
JS;
$this->registerJs($script);
?>

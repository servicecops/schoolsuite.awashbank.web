<?php


use app\modules\planner\models\SchoolPlanSubActivity;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
?>


<div >
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row mt-12">


            <div class="col">
                <?= $form->field($model, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control'],])->textInput(['title' => 'Enter Plan name',

                    'data-toggle' => 'tooltip',

                    'data-trigger' => 'hover',

                    'data-placement' => 'bottom'])->label(false) ?>

            </div>
            <div>

                <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

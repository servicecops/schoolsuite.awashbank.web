<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use kartik\select2\Select2;
use kartik\slider\Slider;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="letter">
    <div class = "row">
        <div class = "col-xs-12">
            <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create Sub Activity</h3>': '<h3><i class="fa fa-edit"></i> Edit  Sub Activity</h3>' ?>
            <hr class="l_header"></hr>
        </div> 
    </div>
</div>
<br></br>
    

<div class = "letter">

    <div id="flash_message">
        <?php  if (\Yii::$app->session->hasFlash('failedSubActivity')) : ?>
            <h3 style = "color:#ff0000;"><?= \Yii::$app->session->getFlash('failedSubActivity'); ?></h3>
        <?php endif; ?>
    </div>
    
    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ?>
        </div>
    </div>
    <div class = "row">
        <div class="col-md-8">
            <?= $form->field($model, 'description', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'expected_start_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'expected_end_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?=  (!$model->isNewRecord)? $form->field($model, 'actual_start_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') : '' ?>
        </div>
        <div class="col">
            <?=  (!$model->isNewRecord)?$form->field($model, 'actual_end_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') : '' ?>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <?php
                $url = Url::to(['/planner/planner/staff-list']);
                
                if ($model->isNewRecord) {
                    $staffInfo = empty($activityStaffModel->staff_id) ? '' : CoreStaff::findOne($model->managed_by);
                    $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';    
                } else {
                     
                    $selectedStaff = $staffList;
                    Yii::trace($selectedStaff);
                }
                
                echo $form->field($activityStaffModel, 'staff_id')->widget(Select2::classname(), [
                    'options' => ['multiple' => true, 'placeholder' => 'Search by firstname'],
                    'initValueText' => $selectedStaff, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Managed by'); 
            ?>
        </div>
        <div class = "col-sm-6">
            <label for="progress_bar">Completion <span id = "activity_progress">0</span> %</label>
            <?php 
                echo $form->field($model, 'progress')->widget(Slider::className(),[
                    'pluginOptions'=>[
                        'min'=>0,
                        'max'=>100,
                        'step'=>10
                    ],
                    'pluginEvents'=> [
                        'slide'=> new JSExpression(
                            'function(val){
                               document.getElementById("activity_progress").textContent = val.value
                               document.getElementById("activity_progress").style.color = "green"
                               document.getElementById("activity_progress").style.fontSize = "x-large"
                            }'
                        )
                    ]
                ])->label('progress');
            ?>
        </div>
    </div>

    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

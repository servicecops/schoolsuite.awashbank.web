<?php

 use yii\helpers\Html;
 use kartik\grid\GridView;
 use yii\widgets\DetailView;
 use kartik\select2\Select2;
 use yii\helpers\Url;
 use app\models\User;
?>

<div class = "letter">
<div class="row">
    <div class="col-md-12">
        <div class = "row">
              <h4>Planner Setting - <?=  $planTitle  ?></h4>
        </div>
    </div>
</div>

<div class = "row">
    <div class="col-md-12">
        <div class = "row">
            <?php 
                if ($emailAlertChecked) {
                    echo '<label class="btn btn-primary" title="Send Due Date Reminders to Project Manager">';
                    echo '<input type="checkbox"  id = "due-date-reminders" value="due-date-reminders" checked>Due Date Reminders</label> &nbsp; &nbsp;';
                } else {
                    echo '<label class="btn btn-primary" title="Send Due Date Reminders to Project Manager">';
                    echo '<input type="checkbox"  id = "due-date-reminders" value="due-date-reminders" unchecked>Due Date Reminders</label> &nbsp; &nbsp;';
                }
                
             ?>
            <label class="btn btn-primary" title="Track Amended Changes">
                <input type="checkbox" id = "project-versions" value="show-amended-changes" unchecked>Show Amended Changes
            </label> &nbsp; &nbsp;
            <label class="btn btn-primary" title="Show deleted comments and attachments">
                <input type="checkbox" id = "show-trash-can" value="show-trash-can" unchecked>Show Deleted Items
            </label> &nbsp; &nbsp;
            <label class="btn btn-primary" title="Adjust color changes for activity and sub-activities">
                <input type="checkbox"  id = "color-code" value="adjust-color-changes" unchecked>Adjust color changes
            </label>
            <!-- <label class="btn btn-primary" title="Add or remove project Managers">
                <input type="checkbox"  id = "project-managers" value="project-managers" unchecked>Attach Project Managers
            </label> -->
        </div>
    </div>
</div>

</div>


<br></br>
<div class = "letter">
 
<div class = "row" id = "planner-changes" style = "overflow:auto;overflow-y:hidden;">
        <?= 
          $this->render('_plannerChangeList2', [
            'dataProvider' => $plannerChanges,
          ]) 
        ?>  
</div>
<div class = "row" id = "planner-trash-can">
    <div class = "col-xs-12">
        <h4>Deleted Comments and Attachments</h4>
        <div class="float-left">
           <button type="button" class="add-item btn btn-primary" id = "show-comments"><i class="fa fa-comment">&nbsp;Show comments</i></button>
           <button type="button" class="add-item btn btn-primary" id = "show-attachments"><i class="fa fa-paperclip">&nbsp;Show Attachments</i></button>
        </div>
    </div>
    
    <div class = "col-xs-12" id = "deleted-comment-container">
            <div class="actionBox">
                <ul class="commentList">
                    <?php   
                        if (!empty($commentList)) {
                            foreach ($commentList as $comment) {
                                $posted_on = $comment['date_created'] ? Yii::$app->formatter->asDateTime($comment['date_created']) : '';
                                $commentUser = User::findOne($comment['created_by']);
                               
                                $comment_author =  $commentUser ? $commentUser->username: 'Anonymous';
                                $author_initials =  $commentUser ? substr($commentUser->lastname, 0, 1) . substr($commentUser->firstname, 0, 1) : 'AA';
                                $author_initials = strtoupper($author_initials);

                                echo '<li>';
                                echo '<div class="commenterImage"><div class = "comment-initials-box"><span>' . $author_initials . '</span></div></div>';
                                echo '<div class="commentText">';
                                echo '<div  class="notes">'. $comment['notes'] .'</div><span class="date sub-text">';
                                echo 'Posted by <a href = "javascript:void(0);">' .$comment_author .'</a> On ' .$posted_on .'</span>';
                                echo '<div class = "commentAction">';
                                echo '<a href = "javascript:void(0);"><span><i class="fa fa-edit edit-comment" data-comment-id="' .$comment['id'].'" style="font-size:15px;color:#87CEFA;">Edit</i> </span></a>&nbsp;&nbsp;';
                                echo '<a href = "javascript:void(0);"><span><i class="fa fa-trash delete-comment" data-comment-id="' .$comment['id'].'"  style="font-size:15px;color:#87CEFA;">Delete</i></span></a>';
                                echo '</div>';
                                echo '</div>';
                                echo '</li>';
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
        
        <div class = "col-xs-12" id = "deleted-attachment-container">
           <?= $this->render('/planner-attachment/_list', [
              'attachmentList' => $attachmentList
            ]) ?>
    </div>
</div>

<div class = "row" id = "adjust-color-container">
<form class ="adjust-color-form" style = "width:50%">
    <div class="form-group">
        <label for="poor_progress">Poor Progress (0-25%)</label>
        <input type="color" class="form-control" id="poor_progress" aria-describedby="colorHelp" value = "<?= $colorData[0] ?>">
        <small id="colorHelp" class="form-text text-muted">Please choose color that signifies danger like red to show poor performance so far </small>
    </div>
    <div class="form-group">
       <label for="average_progress">Average Progress (25-50%)</label>
       <input type="color" class="form-control" id="average_progress" aria-describedby="colorHelp" value = "<?= $colorData[1] ?>">
       <small id="colorHelp" class="form-text text-muted">Please choose color that signifies average performance like blue </small>
   </div>
   <div class="form-group">
       <label for="good_progress">Good Progress (50-75%)</label>
       <input type="color" class="form-control" id="good_progress" aria-describedby="colorHelp" value = "<?= $colorData[2] ?>">
       <small id="colorHelp" class="form-text text-muted">Please choose color that signifies good performance like green </small>
   </div>
   <div class="form-group">
       <label for="excellent_progress">Excellent Progress (75-100%)</label>
       <input type="color" class="form-control" id="excellent_progress" aria-describedby="colorHelp" value = "<?= $colorData[3] ?>">
       <small id="colorHelp" class="form-text text-muted">Please choose color that signifies excellent performance like blue </small>
   </div>
  
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

<!-- Attach project Managers -->
    <div class = "row" id = "project-manager-container">
        <div class = "col-xs-8">
            <h5>Attach Project Managers </h5>
            <?php
               echo Select2::widget([
                    'name' => 'selected-project-manager-list',
                    'value' => [17,18],
                    'data' => $staffList,
                    'options' => [
                            'placeholder' => 'Select the class or student group  ...',
                            'id' => 'selected-project-manager-list',
                            'multiple' => true
                    ],
               ]);

            ?>
            <br>
            <button type = "button"  id = "modify-project-manager" class = "btn btn-primary">Submit</button>
        </div>
    </div>
</div>


<?php
$script = <<< JS
$("document").ready(function($){

    var hostUrl = window.location.pathname,
        queryParams = new URLSearchParams(window.location.search);
  
    $('#color-code').change(function(){
        if($(this).is(':checked')){
            $('#planner-changes').hide();
            $('#planner-trash-can').hide();
            $('#adjust-color-container').show();
        }else{
            $('#adjust-color-container').hide();
        }
    });

    $('#show-trash-can').change(function(){
        if($(this).is(':checked')){
            $('#planner-changes').hide();
            $('#adjust-color-container').hide();
            $('#planner-trash-can').show();
        }else{
            $('#planner-trash-can').hide();
        }
    });
    
    $('#due-date-reminders').change(function(){
        var receive_alerts = '';
        if($(this).is(':checked')){
            receive_alerts = 'enabled'
        }else{
            alert('The project Manager will no longer receive due date email alerts')
            receive_alerts = 'disabled'
        }

        //var hostUrl = window.location.pathname,
        var  postUrl = hostUrl.replace(/setting/gi, "modify-receiving-alerts")
            //queryParams = new URLSearchParams(window.location.search);
        
         var formData = {
            receive_alerts: receive_alerts,
            planId: queryParams.get('planId')
         }

         $.post(postUrl, formData, function(response){
              if (response.success === true) {
                  alert('You have successfully updated the ability to receive the due date alerts')
              } else {
                  alert('You have failed to update the ability to receive the due date alerts')
              }
         })
    });

    $('#project-versions').change(function(){
        if($(this).is(':checked')){
            $('#planner-trash-can').hide();
            $('#adjust-color-container').hide();
            $('#planner-changes').show();
        }else{
            $('#planner-changes').hide();
        } 
    });

    $('#project-managers').change(function(){
          
        if($(this).is(':checked')){
            $('#planner-trash-can').hide();
            $('#adjust-color-container').hide();
            $('#planner-changes').hide();
            $('#project-manager-container').show()
        }else{
            $('#project-manager-container').hide();
        } 
    })

    //about school comments.
    // show only the comments container
    $('#show-comments').click(function(){
        $('#deleted-attachment-container').hide();
        $('#deleted-comment-container').show();
    })
    
    //show the attachment container
    $('#show-attachments').click(function(){
        $('#deleted-comment-container').hide();
        $('#deleted-attachment-container').show();
    })

    //first make it work before optimise.
    //deleting the comment.
    $(document).on('click', '.delete-comment', function(e){
        e.preventDefault()
        var confirmAction = window.confirm('Are you sure that you want to delete the comment forever')

        if (confirmAction === true) {
            var listElement = $(this).parents("li").last();
            commentId = $(this).data('comment-id')      
            deleteCommentAction(commentId,listElement)
        }
    
    });

    function deleteCommentAction(commentId, listElement){
        
        //var  hostUrl = window.location.pathname,
        var  actionUrl = hostUrl.replace(/setting/gi, "delete-comment"),
            deleteUrl = actionUrl + '?id='+ commentId;

            $.ajax({
                url: deleteUrl,
                type: 'DELETE',
                success: function(response){

                    if (response.success === true) {
                        //delete the parent li from the comment box
                        listElement.remove()

                    }else{
                          
                        alert(response.message)
                    }
                }
            })
        
    }

    $(document).on('click', '.edit-comment', function(e){ 
        e.preventDefault()
        alert('Deleted comments cannot be edited  by project Manager')
    });

    //submit the color changes for progress performance.
    $('.adjust-color-form').submit(function(evt){

        evt.preventDefault()
        var poor_progress = $('#poor_progress').val(),
            average_progress = $('#average_progress').val(),
            good_progress = $('#good_progress').val(),
            excellent_progress = $('#excellent_progress').val()

        var confirmAction = window.confirm('Are you sure that you want to update the color of the progress performance')
            
        if (confirmAction) {
            var formData = {
                poor_progress:poor_progress,
                average_progress:average_progress,
                good_progress: good_progress,
                excellent_progress: excellent_progress
            }

            //var  hostUrl = window.location.pathname,
            var  actionUrl = hostUrl.replace(/setting/gi, "adjust-activity-colors"),
                 //queryParams = new URLSearchParams(window.location.search);
                 updateUrl = actionUrl + '?planId=' + queryParams.get('planId');

            $.post(updateUrl, formData, function(response){
                 console.log(response)
                if (response.success) {
                     alert(response.message) 
                } else {
                     alert(response.message)
                }
            })

            

        }   
        
    });

    //modify the project manager list.
    $('#modify-project-manager').click(function(evt){
        evt.preventDefault();
        var projectList = $('#selected-project-manager').val();
        if (projectList) {
            //var  hostUrl = window.location.pathname,
            var  actionUrl = hostUrl.replace(/setting/gi, "modify-project-managers"),
                 //queryParams = new URLSearchParams(window.location.search);
                 updateUrl = actionUrl + '?planId=' + queryParams.get('planId');
            var formData = {
                planId: queryParams.get('planId'),
                projectList: projectList
            }
            $.post(updateUrl, formData, function(response){
                 console.log(response)
                if (response.success) {
                     alert(response.message) 
                } else {
                     alert(response.message)
                }
            })
        }else{
            alert("You have not selected any staff to be project manager")
        }
    })


});


JS;
$this->registerJs($script);
?>
<?php


use app\modules\planner\models\SchoolPlan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use kartik\date\DatePicker;
?>


<div >
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['setting'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row">
            <div class = "col">
                   <?php
                      echo '<label class="control-label">Select date range</label>';
                      echo DatePicker::widget([
                          'model' => $model,
                          'attribute' => 'from_date',
                          'attribute2' => 'to_date',
                          'options' => ['placeholder' => 'Start date'],
                          'options2' => ['placeholder' => 'End date'],
                          'type' => DatePicker::TYPE_RANGE,
                          'pluginOptions' => [
                              'format' => 'yyyy-mm-dd',
                              'autoclose' => true,
                          ]
                      ]);

                   ?>
            </div>
            <div>

            <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

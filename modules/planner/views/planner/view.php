<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->plan_name;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$staffInfo = empty($model->managed_by) ? '' : CoreStaff::findOne($model->managed_by);
$projectManager = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

$assistedBy = empty($model->assisted_by) ? '' : CoreStaff::findOne($model->assisted_by);
$projectManagerAssistant = $assistedBy ? $assistedBy->first_name . ' ' . $assistedBy->last_name : '';

?>
<div class="classes-view hd-title"
     data-title="<?=  $model->plan_name  ?>">
</div>

<div class="row letter">
    <div class="col-md-6">
        <div class="row ">

            <div class="col-lg-6 col-xs-9 profile-text"><h3><b><?= ($model->plan_name) ? $model->plan_name : "--" ?></b></h3></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-plus">Add Activity</i>', ['/planner/planner-activity/create', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-eye">View Activities</i>', ['/planner/planner-activity/index', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this plan?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

</div>

<br><br>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('plan_name') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->plan_name) ? $model->plan_name : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Plan Starts</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->expected_start_date) ? $model->expected_start_date : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Plan Ends</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->expected_end_date) ? $model->expected_end_date : "--" ?></div>
        </div>
    </div>
</div>
<div class = "row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->description) ? $model->description : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('date_created') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->date_created) ? $model->date_created : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> Term </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $model->term_id ? CoreTerm::findOne($model->term_id)->term_name : "--" ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Project Manager </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $projectManager ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Assisted By </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $projectManagerAssistant ?></div>
        </div>
    </div>
</div>
<br></br>

<div class = "row budget-header">
    <div class="float-left">
        <?= Html::a('<i class="fa fa-cog">Planner Setting</i>', ['/planner/planner/setting', 'planId'=> $model->id], ['class' => 'btn btn-primary']) ?> &nbsp;&nbsp;
        <?= Html::a('Budget Summary', ['/planner/planner-budget/budget-summary', 'planId'=> $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-upload">Import Budgets</i>', ['/planner/planner-budget/upload-excel', 'planId'=> $model->id], ['class' => 'btn btn-primary']) ?>
        
    </div>
</div>

</div>

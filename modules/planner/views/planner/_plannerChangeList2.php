<?php

 use yii\helpers\Html;
 use kartik\grid\GridView;
 use yii\widgets\DetailView;
 use yii\helpers\Url;
 use app\models\User;
?>


<?php

    $columns = [
        [
            'label' => 'Updated On',
            'value'=> function ($model){
                $formattedDate = date('F j, Y, g:i a', strtotime($model['date_created']));
                return $formattedDate;
            },
           'format' => 'html',
           'headerOptions' => ['style' => 'width:20%;height:40%']
        ],
        [
           'label' => 'Activity Before',
           'value' => function($model,$rowNumber){

                if ($model['category'] == 'activity') {
                    $row = json_decode($model['previous_version'], true);
                    $data = json_decode($row,true); //to avoid string array
                    
                    $countNo = !empty($data['version'])? $data['version']:"-";
                    //$countNo = array_key_exists('version', $data)? $data['version']:"-";
                    $detailView = $this->render('_plannerChanges', [
                        'data' => $data,
                        'rowNumber' => $countNo
                    ]);

                    return $detailView;

               } else {
                   return '--';
               }   
           },
           'headerOptions' => ['style' => 'width:40%;height:40%'],
           'format' => 'html'
        ],
        [
            'label' => 'Activity After',
            'value' => function($model,$rowNumber){

                if ($model['category'] == 'activity') {
                    $row = json_decode($model['current_version'], true);
                    $data = json_decode($row,true); //to avoid string array
                    $countNo = !empty($data['version'])? $data['version']:"-";
                    //$countNo = array_key_exists('version', $data)? $data['version']:"-";

                    $detailView = $this->render('_plannerChanges', [
                        'data' => $data,
                        'rowNumber' => $countNo
                    ]);

                    return $detailView;
                } else {
                    return '--';
                }   
            },
            'headerOptions' => ['style' => 'width:40%;height:40%'],
            'format' => 'html'
        ],
        [
            'label' => 'SubActivity Before',
            'value' => function($model,$rowNumber){
                if ($model['category'] == 'sub-activity') {
                    $row = json_decode($model['previous_version'],true);
                    $data = json_decode($row,true); //to avoid string array
                    $countNo = !empty($data['version'])? $data['version']:"-";

                    $detailView = $this->render('_plannerChanges', [
                        'data' => $data,
                        'rowNumber' => $countNo
                    ]);

                    return $detailView;  
                } else {
                    return '--';
                }   
            },
            'headerOptions' => ['style' => 'width:40%;height:40%'],
            'format' => 'html'
        ],
        [
            'label' => 'SubActivity After',
            'value' => function($model, $rowNumber){
                if ($model['category'] == 'sub-activity') {
                    $row = json_decode($model['current_version'],true);
                    $data = json_decode($row,true); //to avoid string array
                    $countNo = !empty($data['version'])? $data['version']:"-";
                    //$countNo = array_key_exists('version', $data)? $data['version']:"-";
                    $detailView = $this->render('_plannerChanges', [
                        'data' => $data,
                        'rowNumber' => $countNo
                    ]);

                    
                } else {
                    return '--';
                }
                
            },
            'format' => 'html',
            'headerOptions' => ['style' => 'width:40%;height:40%'],
        ],
        [
            'label' => 'Updated By',
            'value'=> function ($model){
                $user = User::findOne($model['updated_by'])->username;
                return '<p>' .$user . '</p>';
            },
           'format' => 'html',
           'headerOptions' => ['style' => 'width:30%;height:40%']
        ]
     ];

?>

<div class="mt-3">

    <div class="float-right">

    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        //'resizableColumns' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'bordered' => false,
        'striped' => true,
]); ?> 
</div>

<?php

use app\components\Helpers;
use yii\helpers\Url; ?>

<?php if (Helpers::is('r_timetable') || !Helpers::is('non_student') ) :?>

    <div class="card my-1 bg-light">
        <h3 class="card-header display-4 d-flex justify-content-between">School planner center</h3>
        <div class="row card-body justify-content-center">
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/timetable/time-table/index']); ?>">
                <div class="display-4 d-flex justify-content-between  text-center"> <span>Timetables</span> <i class="fa fa-calendar-times-o" aria-hidden="true"></i></div>
            </a>

            <?php if (\app\components\ToWords::isSchoolUser() || Helpers::is('r_planner') ) :?>
                <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/lessonplan/lesson-plan/index']); ?>">
                    <div class="display-4 d-flex justify-content-between  text-center"> <span>Lesson plans</span> <i class="fa fa-calendar" aria-hidden="true"></i></div>
                </a>
            <?php endif; ?>

            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/dissertation/dissertation/index']); ?>">
                <div class="display-4 d-flex justify-content-between  text-center"> <span>Dissertation</span> <i class="fa fa-paperclip" aria-hidden="true"></i></div>
            </a>
        </div>

    </div>
<?php endif; ?>

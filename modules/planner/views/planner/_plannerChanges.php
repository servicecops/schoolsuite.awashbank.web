<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $data */
/* @var $rowNumber */

?>

<?php

    echo  DetailView::widget([
            'model' => $data,
            'options' => ['class' => 'table  detail-view'],
            'attributes' => [
                [
                   'label' =>  'Title (' . $rowNumber . ')',
                    'value' => $data['title']
                ],
                [
                   'label' => 'Description',
                   'value' => substr($data['description'],0,100)  //limit to 100 words
                ],
                [
                   'label' => 'Expected start Date', 
                    'value'=> date('d/m/y', strtotime($data['expected_start_date']))
                ],
                [
                   'label' => 'Expected End Date', 
                   'value'=> date('d/m/y', strtotime($data['expected_end_date']))
                ]       
        ],
    ]);

?>
    
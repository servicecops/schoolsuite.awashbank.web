<?php

use app\modules\schoolcore\models\CoreTerm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm*/

?>
<div class="core-school-class-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

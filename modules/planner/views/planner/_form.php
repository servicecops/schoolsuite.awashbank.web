<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="container card" style="width:90%;">
    <div class = "row">
          <div class = "col-xs-12">
              <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create School Plan</h3>': '<h3><i class="fa fa-edit"></i> Edit School Plan</h3>' ?>
             <hr class="l_header"></hr>
          </div> 
    </div>
    <br></br>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'plan_name', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'description', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'expected_start_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'expected_end_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') ?>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <?=  (!$model->isNewRecord)? $form->field($model, 'actual_start_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') : '' ?>
        </div>
        <div class="col">
            <?=  (!$model->isNewRecord)?$form->field($model, 'actual_end_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') : '' ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php
            $url = Url::to(['terms-list']);
            $termDesc = empty($model->term_id) ? '' : CoreTerm::findOne($model->term_id)->term_name;
            $selectedTerm = $termDesc;
            echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                'options' => ['multiple'=>false, 'placeholder' => 'Search Terms ...'],
                'initValueText' => $selectedTerm, // set the initial display text
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label('Enter the Term name'); ?>

        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <?php
                 $url = Url::to(['staff-list']);
                 $staffInfo = empty($model->managed_by) ? '' : CoreStaff::findOne($model->managed_by);
                 $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';
                 

                echo $form->field($model, 'managed_by')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
                    'initValueText' => $selectedStaff, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Enter the Staff name - project manager by default'); 
                ?>
            </div>

            <div class="col-sm-6">
            <?php
                 $url = Url::to(['staff-list']);
                 $staffInfo = empty($model->assisted_by) ? '' : CoreStaff::findOne($model->assisted_by);
                 $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';
                 

                echo $form->field($model, 'assisted_by')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
                    'initValueText' => $selectedStaff, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Enter the second project Manager'); 
                ?>
            </div>
    </div>

    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSignatures */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="formz">

    <div style="padding: 10px;width:100%"></div>

    <div class="row">
        <div class="col-sm-8">
            <?php

                $subjectId = Yii::$app->request->get('id');
                $model->subject_id = $subjectId;

                echo FileInput::widget([
                    'model' => $model,
                    'name' => 'attachment',
                    'attribute' => 'image',
                    'pluginOptions' => [
                        'uploadUrl' => Url::to(['/planner/planner-attachment/create']),
                        'uploadExtraData' => [
                            'subject_id' => $subjectId
                        ],
                        'maxFileCount' => 10,
                        'allowedFileExtensions'=>['jpg','gif','png','jpeg','pdf', 'xls','xlsx', 'docx', 'doc'],
                        'maxFileSize' => (1024*10),//10 MB
                        //what about styling.
                        'browseClass' => 'btn btn-success btn-sm',
                        'browseIcon' => '<i class="glyphicon glyphicon-folder-open"></i>',
                        'uploadClass' => 'btn  btn-sm btn-info',
                        'uploadIcon' => '<i class="glyphicon glyphicon-upload"></i>',
                        'removeClass' => 'btn  btn-sm btn-danger',
                        'removeIcon' => '<i class="glyphicon glyphicon-trash"></i>'
                    ]
                ]);
               
            ?>
        </div>

    </div>


    <hr/>


</div>
<hr/>
</div>

<?= $this->render('_list', [
        'attachmentList'=>$attachmentList
    ])
?>
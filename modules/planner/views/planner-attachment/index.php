<?php

  use yii\helpers\Html;
  use kartik\grid\GridView;
  use yii\helpers\Url;

  /* @var $this yii\web\View */
  /* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
  /* @var $dataProvider yii\data\ActiveDataProvider */

?>

<!-- the attachments section -->
<div class = "attachment-container">
    <div class = "row">
        <div class = "float-left">
            <h4>attachments</h4>
        </div>
    </div>
    <div class = "row">
        <div class = "col-xs-8">
            <?= $this->render('_form', [
                'model' => $model, 
                'attachmentList'=>$attachmentList
            ])
            ?>
        </div>
    </div>
    <br></br>

 
  

</div> <!--end of attachments div -->
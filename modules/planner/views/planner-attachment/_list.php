
<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?php

$columns = [
     [ 
         'label' => 'Name',
         'value' => function ($model) {
            return Html::a($model['attach_name'], ['planner-attachment/view', 'id' => $model['id']], ['class' => 'aclink']);
         },
         'format' => 'html',
         'headerOptions' => ['style' => 'width:30%;height:40%'],
     ],
     [
         'label' => 'Attachment',
         'format' => 'raw',
         'value' => function ($model) {
            if ($model['file_attachment'] != '') {
                $ImageTypes = array('gif', 'png', 'jpg', 'jpeg');
                $attachFileExtension = pathinfo($model['attach_name'], PATHINFO_EXTENSION);
                
                if (in_array($attachFileExtension, $ImageTypes)) {
                    // Format the image SRC:  data:{mime};base64,{data};
                    $imageSrc = 'data: '. $attachFileExtension .';base64,'.$model['file_attachment'];
                    return '<img src="'. $imageSrc .'" width="100px" height="auto">';
                }else{
                    return '<img src="' . Yii::$app->homeUrl . '/web/img/documentv2.png" width="100px" height="auto">';
                }   
            } else {
               return 'No Attachment';
            }
            
         },
         'headerOptions' => ['style' => 'width:30%;height:40%'],
     ],
     [
         'label' => 'Uploaded By',
         'value'=> function ($model){
             $user = User::findOne($model['created_by'])->username;
             return '<p>' .$user . '</p>';
         },
        'format' => 'html',
        'headerOptions' => ['style' => 'width:30%;height:40%']
     ],
     'date_created',
     [

         'label' => '',
         'format' => 'raw',
         'value' => function ($model) {
             return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['planner-attachment/view', 'id' => $model['id']]);
         }
     ]
 ];
?>

<div class = "row">
    <div class = "col-xs-12">
        <?= GridView::widget([
            'dataProvider' => $attachmentList,
            'columns' => $columns,
            'resizableColumns' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?> 
        </div>
</div>
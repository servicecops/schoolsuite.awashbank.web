<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\modules\planner\models\SchoolPlanSubActivity;

/* @var $this yii\web\View */
/* @var $model app\modules\planner\models\SchoolPlanAttachment */

$this->title = $model->attach_name ? $model->attach_name : '---';
$this->params['breadcrumbs'][] = ['label' => 'Planner Attachment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class = "letter">
    <div class="row">
        <div class="col-md-6">
            <div class="row ">
                <div><h5><b>Attachment</b> - <?= $this->title ?></h5></div>
            </div>
        </div>
        <div class = "col-md-6">
                <p>
                <?= Html::a('<i class="fa fa-trash" >Delete Attachment</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                       'confirm' => 'Are you sure you want to delete this attachment?',
                       'method' => 'post',
                    ],
                ]) 
                ?>
                </p>
        </div>

    </div>

</div> <!--end of card container -->
<br></br>

<div class = "letter">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'attach_name',
            [
               'label' => 'Sub Activity',
               'value' => SchoolPlanSubActivity::findOne($model->subject_id)->title
            ],
            'date_created',
            [
                'label' => 'Uploaded By',
                'value' =>  User::findOne($model->created_by)->username
            ],
            'date_modified',
        ],
    ]) 
    ?>

    <?php
        if ($model->file_attachment!='') {
            $ImageTypes = array('gif', 'png', 'jpg', 'jpeg');
            $attachFileExtension = pathinfo($model['attach_name'], PATHINFO_EXTENSION);
            if (in_array($attachFileExtension, $ImageTypes)) {
                // Format the image SRC:  data:{mime};base64,{data};
                $imageSrc = 'data: '. $attachFileExtension .';base64,'.$model['file_attachment']; //how iam going to get content type
                echo '<img src="'. $imageSrc .'" width="450px" height="auto">';
            }else{
               echo '<br /><p><img src="' . Yii::$app->homeUrl . '/web/img/documentv2.png" width="150px" height="auto">';
            }
        }
    ?>
</div>

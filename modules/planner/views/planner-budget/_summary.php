<div class="row">
    <div class="col-md-12">
        <div class = "row letter">
              <h4>Budget Overview - <?=  $planTitle  ?></h4>
        </div>
        <br>
        <div class="row">
            <table class="table table-striped table-bordered table-responsive" id = "budget-summary">
                <thead>
                    <tr class = "budget-column">
                      <th scope="col"><b>BudgetItem No:</b> </th>
                      <th scope="col"></th>
                      <th scope="col"></th>
                      <th scope="col"><b>Quantity</b></th>
                      <th scope="col"><b>Unit Cost</b></th>
                      <th scope="col"><b>Total Cost</b></th>
                      <th scope="col"><b>Accountability</b></th>
                    </tr>
                    <tr>
                       <th scope="col"></th>
                       <th scope="col">ACTIVITY</th>
                       <th scope="col">SUB-ACTIVITY</th>
                       <th scope="col"></th>
                       <th scope="col"></th>
                       <th scope="col"></th>
                       <th scope="col"></th>

                    </tr>
                </thead>
               <tbody>
                    <?php
                        $counter = 0;
                        $finalBudgetTotal = 0;
                        $activityBudgetTotal = 0;

                        foreach ($budgetList as $item) {

                            $counter+=1;
                            $budgetTotal = 0;
                            $tableRow = '<tr>'.
                                           '<th scope = "row"># ' . $counter . '</th>' .
                                           '<td><b><strong>'. $item['activityTitle'] .'</strong></b></td>' .
                                           '<td></td><td></td><td></td><td></td><td></td>' .
                                        '</tr>';

                            echo $tableRow;

                            //what about the activityBudgets
                            foreach($item['activityBudgets'] as $activityBudgetItem){
                                    $activityBudgetRow =  '<tr>' .
                                                                '<td></td><td>' .$activityBudgetItem['title'] .'</td>' .
                                                                'td></td><td></td><td></td><td></td><td></td><td></td>' .
                                                           '</tr>';

                                    $activityBudgetTotal+=($activityBudgetItem['item_quantity'] * (float)$activityBudgetItem['unit_cost']);

                                    echo $activityBudgetRow;
                            }

                            $subTaskCounter = 0;
                            foreach($item['subTasks'] as $subTask){

                                $subTaskCounterRow  = $counter . '.' . ($subTaskCounter+=1);

                                $subTaskRow = '<tr>' .
                                                '<th scope="row"></th>' .
                                                   '<td></td>' .
                                                   '<td><b>' . $subTaskCounterRow . '  ' .$subTask['subActivityTitle'] . '</b></td>' .
                                                   '<td></td>' .
                                                   '<td></td>' .
                                                   '<td></td>' .
                                                   '<td></td>' .
                                                '</tr>';

                                echo $subTaskRow;

                                //what about the budget items row. under the subTask
                                $budgetItemCounter = 0;
                                $subBudgetAmount = 0;
                                foreach($subTask['budgets'] as $budgetItem ){
                                    $lastSubRowCounter = $counter . '.' . $subTaskCounter . '.' . ($budgetItemCounter+=1);
                                    $lastRow = '<tr>' .
                                                   '<td></td><td></td>' .
                                                   '<td>' .$lastSubRowCounter . ' ' . $budgetItem['title'] . '</td>' .
                                                   '<td>' . $budgetItem['item_quantity'] . '</td>'.
                                                   '<td>' . number_format($budgetItem['unit_cost'], 2)  . '</td>' .
                                                   '<td>' . number_format(($budgetItem['item_quantity'] * $budgetItem['unit_cost']), 2) . '</td>' .
                                                   '<td></td>' .
                                                '</tr>';

                                    $subBudgetAmount+=($budgetItem['item_quantity'] * $budgetItem['unit_cost']);

                                    echo $lastRow;
                                }

                                $budgetTotal+=$subBudgetAmount;
                                //$finalBudgetTotal+=$budgetTotal;

                                $subTotalRow =  '<tr>' .
                                                   '<td></td><td></td><td><b>Sub Total</b></td>' .
                                                   '<td colspan = "2"></td><td><b>' . number_format($subBudgetAmount)  . '</b></td>' .
                                                '</tr>';
                                echo $subTotalRow;

                            }

                            $finalBudgetTotal+= (float)$budgetTotal;
                            $totalBudgetRow = '<tr>' .
                                                '<td></td><td></td>' .
                                                '<td><b>TOTAL</b></td>' .
                                                '<td colspan = "2"></td>' .
                                                '<td><b>' . $budgetTotal .  '</b></td>' .
                                                '</tr>';
                            echo $totalBudgetRow;

                         }

                        $finalBudgetTotal = number_format(($finalBudgetTotal + $activityBudgetTotal), 2);

                        $finalTotalRow =   '<tr class = "budget-summary-total">' .
                                              '<td colspan = "3"></td>' .
                                              '<td colspan ="2"><b>TOTAL</b></td>' .
                                              '<td><b>' . $finalBudgetTotal .  '</b></td>' .
                                            '</tr>';
                        echo $finalTotalRow;
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>

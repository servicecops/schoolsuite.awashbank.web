<?php

use app\models\User;
//use app\modules\planner\models\CoreStaff;
use kartik\select2\Select2;
use kartik\slider\Slider;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="letter">
    
    <div class = "row">
        <div class = "col-xs-12">
             <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create School Budget</h3>': '<h3><i class="fa fa-edit"></i> Edit School Budget</h3>' ?>
            <hr class="l_header"></hr>
        </div> 
    </div>
</div>
<br></br>

<div class = "letter">
    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ?>
        </div>
    </div>

    <div class = "row">
        <div class="col-md-12">
            <?= $form->field($model, 'description', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'unit_cost', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'item_quantity', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class = "row">
        <div class="col-md-12">
            <?= $form->field($model, 'comments', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
        </div>
    </div>
    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

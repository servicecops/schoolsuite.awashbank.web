<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use kartik\grid\GridView;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $subActivities  */

$this->title = $activityModel['title'];
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="classes-view hd-title"
     data-title="<?=  $this->title  ?>">
</div>

<div class ="letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col"><h3><b> <?= ($activityType == 'subActivity')? 'SubActivity' : 'Activity'  ?></b> - <?= ($this->title) ? $this->title : "--" ?></h3></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-plus">Add Budget</i>', ['/planner/planner-budget/create', 'id' => $activityModel['id'] ], ['class' => 'btn btn-sm btn-primary']) ?>
            <?php
                if ($activityType == 'activity') {
                    echo Html::a('<i class="fa fa-arrow-left" >Activity</i>', ['/planner/planner-activity/view', 'id' => $activityModel['id']], ['class' => 'btn btn-sm btn-primary']);
                }

                if ($activityType == 'subActivity') {
                    echo  Html::a('<i class="fa fa-arrow-left" >Sub Activity</i>', ['/planner/planner-sub-activity/view', 'id' => $activityModel['id']], ['class' => 'btn btn-sm btn-primary']);
                }
            ?>
        </p>
    </div>

</div>

</div> <!--end of card container -->
<br></br>

<?php

    $columns = [
        [
            'label' => 'Title',
            'value'=>function($model){
                return Html::a($model['title'], ['/planner/planner-budget/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'description',
        'item_quantity',
        'unit_cost',
        [
            'label' => 'Total',
            'value'=>function($model){
                return (($model->unit_cost*$model->item_quantity));
            },
            //'format'=>'html',
        ],
        'comments',
        [
            'label' => 'Created By',
            'value'=>function($model){
                 $user = $model->created_by ? User::findOne($model->created_by)->username : '';
                 return $user;
            },
            //'format'=>'html',   
        ],
        [
            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['/planner/planner-budget/view', 'id' => $model['id']]);
            },
        ]
    ];
    ?>

<div class = "letter">
    <div class = "row">
        <h4>School Budgets</h4>
    </div>

    <div class = "row">
        <div class = "col-xs-12">
            <?= GridView::widget([
               'dataProvider'=> $dataProvider,
               'columns' => $columns,
               'resizableColumns'=>true,
               'responsive'=>true,
               'responsiveWrap' => false,
               'bordered' => false,
               'striped' => true,
             ]);
             ?>
        </div>
    </div>

</div>

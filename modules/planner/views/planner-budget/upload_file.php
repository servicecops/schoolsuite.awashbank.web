<?php

use app\modules\schoolcore\models\CoreSchool;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class = "letter">
<div class="row hd-title" data-title="Upload Excel">
<div class="col-xs-12 col-lg-12 no-padding">
    <div id="flash_message">
        <?php  if (\Yii::$app->session->hasFlash('uploadFailed')) : ?>
            <h3><?= \Yii::$app->session->getFlash('uploadFailed'); ?></h3>
        <?php endif; ?>
        <?php  if (\Yii::$app->session->hasFlash('uploadSuccess')) : ?>
            <h3> <?= \Yii::$app->session->getFlash('uploadSuccess'); ?> </h3>
        <?php endif; ?>
    </div>
    
    <h4>Import School Budgets</h4>
    <table class="table" style="background-color:#fff; margin-bottom:3px;">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <tr>

            <th>    
                <?= $form->field($model, 'importFile', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'File']])->fileInput()->label(false) ?>
            </th>
            <th>            
                <?= $form->field($model, 'description', ['inputOptions' => ['class' => 'form-control input-sm inputRequired', 'placeholder' => 'Description']])->textInput()->label(false) ?>
            </th>
            <th>
                <?php
                       //replace with the subactivities for particular school plan
                        $planId = \Yii::$app->request->get('planId');
                        $url = Url::to(['sub-activity-list']);

                        $url = $url . '?planId=' . $planId;
                        
                         echo $form->field($model, 'activity_id')->widget(Select2::classname(), [
                            'options' => ['multiple' => false, 'placeholder' => 'Search subActivity'],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
        
                            ],
                        ])->label(false);

                    ?>
                    </div>

            </th>

            <th>
                <button class="btn btn-block btn-primary btn-sm">Upload</button>
            </th>
        </tr>
        <?php ActiveForm::end(); ?>
    </table>
</div>
</div>

    <div class="row">
        <div class="col-md-12">
            <div id="loading"></div>
            <div id="errorMessage"></div>
            <div id="response">
                <div class="col-xs-12 no-padding">
                    <p style="font-size:15px;color:grey;"><b>Note:</b> Only excel data sheets that follow the <b>template format provided by schoolsuite</b> will be allowed.
                    <b>**And follow column Order !important**</b> <a href="<?= \Yii::getAlias('@web/uploads/schsuite_budget_import_template.xlsx'); ?>" target="_blank">download template</a></p>
                    <table class="table table-striped" style="font-size:12px;">
                        <thead>
                            <tr>
                              <th>title</th><th>description</th><th>unit_cost</th><th>item_quantity</th><th>comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               <td>Clearing the school pitch</td>
                               <td>We need to get iit ready for coming sports Day</td>
                               <td>150000</td>
                               <td>3</td>
                               <td>The parents committee should cover this budget</td>
                            </tr>
                            <tr>
                               <td>Required</td><td>Required</td><td>Required</td>Required</td><td>Required</td><td>Optional</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use kartik\grid\GridView;
use app\modules\planner\models\SchoolPlanSubActivity;
use app\modules\planner\models\SchoolPlanActivity;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $subActivities  */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class = "letter">
<div class="classes-view hd-title"
     data-title="<?=  $model->title  ?>">
</div>

<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div><h3><b>School Budget </b> - <?= ($model->title) ? $model->title : "--" ?></h3></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-edit">Edit Budget</i>', ['update', 'id' => $model->id ], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete Budget</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this budget?',
                    'method' => 'post',
                ],
            ]) 
            ?>
        </p>
    </div>

</div>
</div> <!-- card container for header -->
<br></br>


<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('title') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? $model->title : "--" ?></div>
        </div>
    </div>
</div>

<div class = "row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->description) ? $model->description : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> Unit Cost </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->unit_cost) ? $model->unit_cost : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Item Quantity </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->item_quantity) ? $model->item_quantity  : "--" ?></div>
        </div>
    </div>
</div>

<div class = "row">
   <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Activity</div>
            <div class="col-lg-6 col-xs-9 profile-text">
                 <?php
                    //check if it is activity or subActivity
                    if (!empty($model->activity_id)) {
                        $subActivity = SchoolPlanSubActivity::findOne($model->activity_id);
                        if (!empty($subActivity)) {
                             echo $subActivity->title;
                        } else {
                            $activity = SchoolPlanActivity::findOne($model->activity_id);
                            $activity = !empty($activity) ? $activity->title : '---';
                            echo $activity;
                        }
                         
                    } else {
                       echo '---';
                    }
                   

                 ?>

            </div>
        </div>
    </div>
</div>

<div class = "row">
   <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Approved On </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->approval_date) ? date('F j, Y, g:i a', strtotime($model->approval_date))  : "Still Pending..." ?></div>
        </div>
    </div>
</div>

</div>


<?php

    $columns = [
        [
            'label' => 'Title',
            'value'=>function($model){
                return Html::a($model['title'], ['/planner/planner-sub-activity/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'expected_start_date',
        'actual_start_date',
        'expected_end_date',
        'actual_end_date',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['/planner/planner-sub-activity/view', 'id' => $model['id']]);
            },
        ]
    ];
    ?>

<br></br>

<div class = "letter">
<div class = "row attachment-header">
    <div class="float-left">
        <button type="button" class="add-item btn btn-success" id = "show-attachments">
             <i class="fa fa-paperclip">&nbsp;Upload Documents</i>
        </button>
        <button type = "button" class="add-item btn btn-success" id = "approval-budget">
           <?php
                if ($model->approval_date) {
                   echo '<i class="fa fa-edit">&nbsp; Change Approval Date</i>';
                } else {
                   echo '<i class="fa fa-check">&nbsp; Approve Budget</i>';
                }
               
           ?>
        </button>
    </div>
</div>

<div class = "approval-date-container">
    <div class = "row">
        <div class = "float-left approval-date-alert">
            <p>You have successful approved the budget</p>
        </div>
    </div>
    
    <?php 
            //$url = Url::to(['/planner/planner-sub-activity/set-task-deadline']);
        $form = ActiveForm::begin([
            'options' => ['class' => 'approval-date-form'],
            'action' => ['/planner/planner-budget/approve']
        ]); 
    ?>
        <div class = "row">
            <div class="col-xs-4">
                 <?= $form->field($model, 'approval_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') ?>
                 <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xm-4">
                <?= Html::submitButton( 'Save', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>

<?= $this->render('/planner-attachment/index', [
    'model' => $attachmentModel,
    'attachmentList' => $attachmentList
]) ?>

</div>

</div>

<?php
$script = <<< JS
$("document").ready(function($){
    
    $('#show-attachments').click(function(){
        $('.attachment-container').show();
    })


    $('#approval-budget').click(function(){
        $('.attachment-container').hide();
        $('.approval-date-container').show();
    })

    $('.approval-date-form').submit(function(event) {

        event.preventDefault(); // stopping submitting
        var formData = $(this).serializeArray(),
            url = $(this).attr('action');
   
        $.post(url, formData, function(response){
            if (response.success === true) {
               var dueDateText = "You have successful approved the school budget"
                $('.approval-date-alert').show().fadeOut(3000)
            } else {

             var alertDueDate = $('.approval-date-alert').show().find("p");
                 alertDueDate.text("Failed to approve the budget").css('color', 'red').fadeOut(3000);
                
            }
        })

    });


});
JS;
$this->registerJs($script);
?>




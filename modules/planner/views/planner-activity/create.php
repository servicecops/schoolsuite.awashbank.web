<?php

use app\modules\planner\planner\SchoolPlanActivity;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\planner\planner\SchoolPlanActivity*/
  
//$model->id = id;
?>
<div class="core-school-class-create">


    <?= $this->render('_form', [
        'model' => $model,'activityStaffModel'=>$activityStaffModel
    ]) ?>

</div>

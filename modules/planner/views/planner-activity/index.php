<?php


use yii\bootstrap4\Html;
use yii\helpers\Url;
//use miloschuman\highcharts\GanttChart;
use miloschuman\highcharts\HighchartsAsset;
use yii\web\JsExpression;
use yii\web\View;


$this->title = 'School Plan  -  ' .$planTitle ;
HighchartsAsset::register($this)->withScripts([ 'modules/gantt','modules/exporting']);
?>

<div class = "letter">
    <h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title) ?></h1>  
</div>
<br></br>

<div class = "letter">
    <div class="text-center" id = "loading-gantt-chart">
        <div class="spinner-border text-primary" role="status"  style="width: 3rem; height: 3rem;">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <div id = "chart-container">
        
    </div>
</div>

<?php

 
 $script = <<< JS

   $('document').ready(function($){


        //fetch the performance metrics.
        var hostUrl = window.location.pathname;
            actionUrl = hostUrl.replace(/index/gi, "gantt-chart-data"),
            queryParams = new URLSearchParams(window.location.search);
            actionUrl = actionUrl + '?planId=' + queryParams.get('id');


        $.get(actionUrl, function(data,status){

            if (data.success === true) {
                var response = JSON.parse(data.message);
                var activities = response.activities,
                    subActivities = response.subActivities;
                    //chartData = [].concat(activities,subActivities)
                // chartData = [...activities, ...subActivities]
                // chartData.forEach(item=>{
                //     delete item.seriesIndex
                // })
                $('#loading-gantt-chart').hide();
                drawGnattChart(activities)
            } else {
                alert('Failed to load the performance metrics') 
            } 
        })
        

        function drawGnattChart(chartData){
            
            var navigatorOptions = {
                    enabled: true,
                    liveRedraw: true,
                    series: {
                        type: 'gantt',
                        pointPlacement: 0.5,
                        pointPadding: 0.25
                    },
                    yAxis:{
                        min: 0,
                        max: 3,
                        reversed: true,
                        categories: []
                    }
            }


                // THE CHART
                Highcharts.ganttChart('chart-container', {
                        title: {
                           text: 'School  Plan Activities'
                        },
                        // xAxis: {
                        //   min: today.getTime() - (2 * day),
                        //   max: today.getTime() + (32 * day)
                        // },
                        plotOptions: {
                            series: {
                               animation: false, // Do not animate dependency connector
                                allowPointSelect: true,
                                point: {
                                        events: {
                                            click: showSubTask,
                                        }
                                }

                            }
                        },
                        yAxis: {
                           uniqueNames: true
                        },
                        navigator: navigatorOptions,
                        scrollbar: {
                             enabled: true
                        },
                        rangeSelector: {
                            enabled: true,
                            selected: 0
                        },
                        series: [{
                            name: 'Project 1',
                            data: chartData
                        }]
                });
            }


            function showSubTask(evt){
                
                let  taskDetail = evt.point.options,
                     hostUrl = window.location.pathname;
        
                if (taskDetail.id) {
                    let taskUrl = hostUrl.replace(/index/gi, "view?id="+ taskDetail.id)
                    window.location.href = taskUrl
                }
            }


    });


JS;
 $this->registerJs($script);
 ?>



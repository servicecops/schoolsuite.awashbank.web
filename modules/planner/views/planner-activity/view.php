<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use kartik\grid\GridView;
use app\modules\planner\models\SchoolPlan;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $subActivities  */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="classes-view hd-title"
     data-title="<?=  $model->title  ?>">
</div>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col"><h3><b>Activity</b>- <?= ($model->title) ? $model->title : "--" ?></h3></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-plus">Add SubActivity</i>', [
                  '/planner/planner-sub-activity/create', 
                  'id' => $model->id,
                  'version' => $model->version
                ], 
                ['class' => 'btn btn-sm btn-primary'])
             ?>
            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this activity?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('<i class="fa fa-eye">Budgeting</i>', [
                '/planner/planner-budget/index', 
                'activityId' => $model->id,
                'activityType' => 'activity'
                ],
                [
                  'class' => 'btn btn-sm btn-primary'
                ]) 
            ?>
        </p>
    </div>

</div>

</div> <!-- end of card container -->

<br>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('title') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? $model->title : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Activity Starts</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->expected_start_date) ? $model->expected_start_date : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Activity Ends</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->expected_end_date) ? $model->expected_end_date : "--" ?></div>
        </div>
    </div>
</div>
<div class = "row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->description) ? $model->description : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> Work Completion (%)</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->progress) ? $model->progress : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Plan Name</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->plan_id) ? SchoolPlan::findOne($model->plan_id)->plan_name : "--" ?></div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Managed By </div>
            <div class="col-lg-6 col-xs-9 profile-text">
                <?php
                   if (!empty($staffList)) {
                    foreach ($staffList as $staff) {
                        echo '<li>';
                        echo $staff['first_name'] . " " . $staff['last_name']; 
                        echo '<br>';
                        echo '</li>';
                      }
                    }
                ?>
            </div>
        </div>
    </div>

</div>

</div>
<br></br>

<?php

    $columns = [
        [
            'label' => 'Title',
            'value'=>function($model){
                return Html::a($model['title'], ['/planner/planner-sub-activity/view', 'id' => $model['id']], ['class'=>'aclink']);
            },
            'format'=>'html',
        ],
        'expected_start_date',
        'actual_start_date',
        'expected_end_date',
        'actual_end_date',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['/planner/planner-sub-activity/view', 'id' => $model['id']]);
            },
        ]
    ];
    ?>

<div class = "letter">
    <div class = "row">
        <h4>Sub Activities</h4>
    </div>

    <div class = "row">
        <div class = "col-xs-12">
            <?= GridView::widget([
               'dataProvider'=> $subActivities,
                'columns' => $columns,
                'resizableColumns'=>true,
                'responsive'=>true,
                'responsiveWrap' => false,
                'bordered' => false,
                'striped' => true,
             ]); 
           ?>
    </div>
</div>

</div>

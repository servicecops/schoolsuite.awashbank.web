<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm*/

$this->title = 'Edit School Activity: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'School Activity', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="core-school-class-update">

<?= $this->render('_form', [
    'model' => $model,
    'staffList' => $staffList,
    'activityStaffModel' => $activityStaffModel
]) ?>

</div>

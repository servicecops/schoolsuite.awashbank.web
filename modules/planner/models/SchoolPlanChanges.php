<?php

namespace app\modules\planner\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\modules\logs\models\Logs;


/**
 * This is the model class for table "school_plan_changes".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property string|null $actual_start_date
 * @property string|null $actual_end_date
 * @property string $expected_start_date
 * @property string $expected_end_date
 * @property string $category
 * @property int $updated_by
 * @property string|null $date_created
 * @property string|null $subject_id
 */
class SchoolPlanChanges extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_changes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'expected_start_date', 'expected_end_date', 'category', 'updated_by'], 'required'],
            [['title', 'description', 'category', 'subject_id'], 'string'],
            [['actual_start_date', 'actual_end_date', 'expected_start_date', 'expected_end_date', 'date_created'], 'safe'],
            [['updated_by'], 'default', 'value' => null],
            [['updated_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'actual_start_date' => 'Actual Start Date',
            'actual_end_date' => 'Actual End Date',
            'expected_start_date' => 'Expected Start Date',
            'expected_end_date' => 'Expected End Date',
            'category' => 'Category',
            'updated_by' => 'Updated By',
            'date_created' => 'Date Created',
            'subject_id' => 'Subject ID',
        ];
    }

    /**
     *  Save the project changes that have been made.
     */
    public static function saveProjectChanges($planDetail)
    {
        try {
            
            $planChange = new SchoolPlanChanges();
            $planChange->title = $planDetail['title'];
            $planChange->description = $planDetail['description'];
            $planChange->actual_start_date = $planDetail['actual_start_date'];
            $planChange->actual_end_date = $planDetail['actual_end_date'];
            $planChange->expected_start_date = $planDetail['expected_start_date'];
            $planChange->expected_end_date = $planDetail['expected_end_date'];
            $planChange->category = $planDetail['category'];
            $planChange->updated_by = $planDetail['updated_by'];
            $planChange->subject_id = $planDetail['subject_id'];
            $planChange->planner_id = $planDetail['planner_id'];

            $planChange->save();

        }catch(\Exception $e){
            Yii::error($e);
            Yii::trace($e);
        }
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $plannerId = $params['planner_id']? $params['planner_id'] : '';
        $query = SchoolPlanChanges::find()
                       ->where(['planner_id'=> $plannerId])
                       ->orderBy('date_created DESC');

        // add conditions that should always apply here
        if ($params['startDate'] && $params['endDate']) {
            Yii::trace("filter records by date");
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'planner_id' => $this->planner_id,
            'id' => $this->id,
            'date_modified' => $this->date_modified,
            'updated_by' => $this->updated_by
        ]);

        return $dataProvider;
    }

    /**
     * Save the project changes for the log adjustments.
     * tracking the before and after changes to the school plan
     * Refactor the whole data model for this
     */
    public static function saveSchoolPlanAdjustments($previousModel,$currentModel,$category)
    {
         
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {

            $planId = '12';
            if ($category == 'sub-activity') {
                $ActivityDetail = SchoolPlanActivity::findOne($currentModel->plan_activity_id);
                $planId = $ActivityDetail->plan_id;
            }
            
            $connection->createCommand()->insert('school_plan_adjustments', [
                //'planner_id' => $currentModel->plan_id,
                'planner_id' => ($category == 'activity')? $currentModel->plan_id : $planId,
                'subject_id' => $currentModel->id,
                'category' => $category,
                'previous_version' => self::cleanDataModel($previousModel,$category),
                'current_version' => self::cleanDataModel($currentModel,$category),
                'updated_by' => $currentModel->created_by,
            ])->execute();

            $transaction->commit();
            
        } catch (\Exception $e) {
             $transaction->rollBack();
             $model_error = $e->getMessage();

            Logs::logEvent("Failed to insert the school plan adjustments : ", $model_error, null);            
        }
    }


    /**
     * Extract the planner changes from the data model
     */
    private static function cleanDataModel($activityModel,$category)
    {
        $result = array(
           'title' => $activityModel->title,
           'description' => $activityModel->description,
           'expected_start_date' => $activityModel->expected_start_date,
           'expected_end_date' => $activityModel->expected_end_date,
           'actual_start_date' => empty($activityModel->actual_start_date)? null : $activityModel->actual_start_date,
           'actual_end_date' => empty($activityModel->actual_end_date)? null :$activityModel->actual_end_date ,
           'category' => $category,
           'version' => $activityModel->version,
           'updated_by' => $activityModel->created_by
        // 'subject_id' => $id,
        // 'planner_id' => $model->plan_id 
         );
        
         $result = json_encode($result);

       return $result;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchProjectAdjustments($params)
    {
        
        $plannerId = $params['planId']? $params['planId'] : '';
        
        $query = (new Query())->select([
                      'id','planner_id','subject_id','category','previous_version', 
                       'current_version','updated_by', 'date_created'
                    ])
                    ->from('school_plan_adjustments')
                    ->where(['planner_id' => $plannerId ])
                    ->orderBy(['date_created' => SORT_DESC]);

        // add conditions that should always apply here
        // if ($params['startDate'] && $params['endDate']) {
        //     Yii::trace("filter records by date");
        // }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'planner_id' => $this->planner_id,
            'id' => $this->id,
            'date_modified' => $this->date_modified,
            'updated_by' => $this->updated_by
        ]);

        return $dataProvider;
    }

}

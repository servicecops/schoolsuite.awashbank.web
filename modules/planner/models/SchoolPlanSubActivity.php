<?php

namespace app\modules\planner\models;

use app\models\User;
use app\modules\planner\models\SchoolPlanActivity;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * This is the model class for table "school_plan_subactivity".
 *
 * @property int $id
 * @property int $plan_activity_id
 * @property string|null $description
 * @property string $title
 * @property string $expected_start_date
 * @property string $expected_end_date
 * @property int|null $progress
 * @property int $created_by
 * @property int $managed_by
 * @property string|null $actual_start_date
 * @property string|null $actual_end_date
 * @property string $date_modified
 * @property string $date_created
 * @property string|null $task_due_date
 * @property string|null $reminder_alert_duration
 * @property boolean $active
 * @property float|null $version
 */
class SchoolPlanSubActivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_subactivity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','title','expected_start_date', 'expected_end_date', 'created_by'], 'required'],
            [['title','description', 'reminder_alert_duration'], 'string'],      
            [['expected_start_date', 'expected_end_date', 'managed_by', 'actual_start_date', 'actual_end_date', 'date_created'], 'safe'],
            [['progress', 'created_by', 'reminder_alert_duration'], 'default', 'value' => null],
            [['id','progress','plan_activity_id','created_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_activity_id' => 'Plan Activity ID',
            'description' => 'Description',
            'title' => 'Title',
            'expected_start_date' => 'Expected Start Date',
            'expected_end_date' => 'Expected End Date',
            'progress' => 'Progress',
            'created_by' => 'Created By',
            'managed_by' => 'managed_by',
            'actual_start_date' => 'Actual Start Date',
            'actual_end_date' => 'Actual End Date',
        ];
    }


    public function getUsername()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

     /**
     * Gets query for [[SchoolPlan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolPlanActivity()
    {
        return $this->hasOne(SchoolPlanActivity::className(), ['id' => 'plan_activity_id']);
    }

    public function viewModel($params)
    {
        $attributes = [
            
            'title',
            'description',
            'expected_start_date',
            'expected_end_date',
            // [
            //     'label' => 'Managed By',
            //     'value' => function ($model) {
            //         return $model->userName->firstname.' '.$model->userName->lastname;
            //     }
            // ],
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = SchoolPlanSubActivity::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

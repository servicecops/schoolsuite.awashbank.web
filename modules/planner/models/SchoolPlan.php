<?php

namespace app\modules\planner\models;


use app\models\User;
use app\modules\schoolcore\models\CoreTerm;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use Yii;


/**
 * This is the model class for table "school_plan".
 *
 * @property int $id
 * @property string $plan_name
 * @property string $description
 * @property string $expected_start_date
 * @property string $expected_end_date
 * @property string $created_by
 * @property int $managed_by
 * @property string $date_modified
 * @property string $date_created
 * @property int $term_id
 * @property string|null $actual_start_date
 * @property string|null $actual_end_date
 * @property boolean $receive_due_alerts
 * @property int|null $assisted_by
 * @property CoreTerm $term
 */
class SchoolPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','plan_name','description', 'expected_start_date', 'expected_end_date', 'created_by', 'managed_by', 'term_id'], 'required'],
            [['id', 'managed_by', 'assisted_by','created_by'], 'integer'],
            [['plan_name','description'], 'string'],
            [['expected_start_date', 'expected_end_date', 'actual_start_date', 'actual_end_date', 'date_created', 'actual_start_date', 'actual_end_date'], 'safe'],
            [['term_id'], 'exist', 'skipOnError' => true, 'targetClass' => CoreTerm::className(), 'targetAttribute' => ['term_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_name' => 'Plan Name',
            'description' => 'Description',
            'date_created' => 'Date Created',
            'expected_start_date' => 'Expected Start Date',
            'expected_end_date' => 'Expected End Date',
            'created_by' => 'Created By',
            'managed_by' => 'Managed By',
            'term_id' => 'Term ID',
            'actual_start_date' => 'Actual Start Date',
            'actual_end_date' => 'Actual End Date',
            'date_modified'=>'Date Modified',
            'assisted_by' => 'Additional Project Manager'
        ];
    }

    public function formAttributeConfig() {
        $config = [

            ['attributeName'=>'plan_name', 'controlType'=>'text'],
            ['attributeName'=>'description', 'controlType'=>'textarea'],
            ['attributeName'=>'expected_start_date', 'controlType'=>'date_picker'],
            ['attributeName'=>'expected_end_date', 'controlType'=>'date_picker'],
            ['attributeName'=>'managed_by', 'controlType'=>'text'],
            ['attributeName'=>'term_id', 'controlType'=>'term_id_only', 'placeholder'=>'Choose term name', 'prompt'=>'Choose term name'],

        ];

        return $config;
    }


    public function updateAttributeConfig() {
        $config = [

            ['attributeName'=>'plan_name', 'controlType'=>'text'],
            ['attributeName'=>'description', 'controlType'=>'text'],
            ['attributeName'=>'expected_start_date', 'controlType'=>'date_picker'],
            ['attributeName'=>'expected_end_date', 'controlType'=>'date_picker'],
            ['attributeName'=>'managed_by', 'controlType'=>'text'],
            ['attributeName'=>'term_id', 'controlType'=>'term_id_only', 'placeholder'=>'Choose term name', 'prompt'=>'Choose term name'],

        ];

        return $config;
    }

    public function getUsername()
    {
        return $this-> hasOne(User::className(), ['id' => 'created_by']);
    }



    /**
     * Gets query for [[Term]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTerm()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [


            'plan_name',
            'description',
            'expected_start_date',
            'expected_end_date',
            [
                'label' => 'Managed By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                }
            ],
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = SchoolPlan::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }
}

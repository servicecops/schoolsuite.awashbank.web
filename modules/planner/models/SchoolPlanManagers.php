<?php

namespace app\modules\planner\models;

use Yii;

/**
 * This is the model class for table "school_plan_managers".
 *
 * @property int $id
 * @property int $plan_id
 * @property int $staff_id
 * @property string|null $date_created
 * @property string|null $date_modified
 * @property int $created_by
 */
class SchoolPlanManagers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_managers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plan_id', 'staff_id', 'created_by'], 'required'],
            [['plan_id', 'staff_id', 'created_by'], 'default', 'value' => null],
            [['plan_id', 'staff_id', 'created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan_id' => 'Plan ID',
            'staff_id' => 'Staff ID',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
            'created_by' => 'Created By',
        ];
    }
}

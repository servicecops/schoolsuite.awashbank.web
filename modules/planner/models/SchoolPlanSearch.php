<?php

namespace app\modules\planner\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 * SchoolPlanSearch represents the model behind the search form of `app\modules\planner\models\SchoolPlan`.
 */
class SchoolPlanSearch extends SchoolPlan
{
    public $test,$answer,$schsearch, $modelSearch;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'term_id',], 'integer'],
            [['date_created', 'plan_name','expected_start_date', 'expected_end_date', 'actual_start_date', 'actual_end_date'], 'safe'],
            [['modelSearch'], 'trim']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $query = SchoolPlan::find();
        $searchForm = 'plan_name';
        // add conditions that should always apply here
        $columns = [
            'plan_name',
            'description',
            'expected_start_date',
            'expected_end_date',
            [
                'label' => 'Plan Name',
                'value' => function ($model) {
                    return $query->plan_name; // i think 

                },
            ],
            'date_created',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['core-term/view/', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['ilike', 'plan_name', $this->modelSearch])
            ->andFilterWhere(['ilike', 'description', $this->modelSearch]);


        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params) //needs refactoring to support school plan
    {
        $searchForm = 'plan_name';
        $placeholder = 'search term ';
        $query = (new Query())
            ->select(['ct.id', 'ct.plan_name', 'ct.description', 'ct.expected_end_date', 'ct.expected_start_date','ct.date_created', 'ct.date_modified',])
            ->from('school_plan ct')
            ->innerJoin('core_term csc', 'ct.term_id= csc.id');
        $query->orderBy('ct.id desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'plan_name',

            [
                'label' => 'Plan Name',
                'value'=>function($model){
                    return Html::a($model['description'], ['/planner/planner/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'expected_start_date',
            'expected_end_date',
            //'school_name',
            'term_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['planner/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function searchNewIndex($params)
    {   
       
        $schoolId = isset($params['school_id']) ? $params['school_id'] : '';
        $query = SchoolPlan::find()->where(['school_id' => $schoolId ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'pagination' => [
            //     'pagesize' => 5 // in case you want a default pagesize
            // ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
            //'term_id' => $this->term_id,
        ]);

        $query->andFilterWhere(['ilike', 'plan_name', $this->modelSearch]);
               //->andFilterWhere(['ilike', 'description', $this->modelSearch]);

        return $dataProvider;
    }

}

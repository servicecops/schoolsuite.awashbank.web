<?php

namespace app\modules\planner\models;

use app\models\User;
use app\modules\planner\models\SchoolPlan;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * This is the model class for table "school_plan_activity".
 *
 * @property int $id
 * @property string|null $description
 * @property string $title
 * @property string|null $expected_start_date
 * @property string|null $expected_end_date
 * @property int|null $progress
 * @property int $created_by
 * @property int $managed_by
 * @property int $plan_id
 * @property string|null $actual_start_date
 * @property string|null $actual_end_date
 * @property string $date_modified
 * @property string $date_created
 * @property string|null $task_due_date
 * @property string|null $reminder_due_date
 * @property boolean $active
 * @property float|null $version
 */
class SchoolPlanActivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','title', 'description', 'expected_start_date','expected_end_date','plan_id', 'created_by'], 'required'],
            [['title','description'], 'string'],
            [['expected_start_date', 'expected_end_date', 'actual_start_date', 'actual_end_date', 'date_created'], 'safe'],
            [['progress', 'created_by'], 'default', 'value' => null],
            [['id','progress', 'created_by', 'managed_by', 'plan_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['plan_id'], 'exist', 'skipOnError' => true, 'targetClass' => SchoolPlan::className(), 'targetAttribute' => ['plan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'title' => 'Title',
            'expected_start_date' => 'Expected Start Date',
            'expected_end_date' => 'Expected End Date',
            'progress' => 'Progress',
            'created_by' => 'Created By',
            'managed_by' => 'Managed By',
            'plan_id' => 'Plan ID',
            'actual_start_date' => 'Actual Start Date',
            'actual_end_date' => 'Actual End Date',
        ];
    }

    public function getUsername()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

     /**
     * Gets query for [[SchoolPlan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolPlan()
    {
        return $this->hasOne(SchoolPlan::className(), ['id' => 'plan_id']);
    }

    public function viewModel($params)
    {
        $attributes = [
            
            'title',
            'description',
            'expected_start_date',
            'expected_end_date',
            [
                'label' => 'Managed By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                }
            ],
            [
                'label' => 'Created By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = SchoolPlanActivity::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

}

<?php

namespace app\modules\planner\models;


use app\models\User;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\db\Query;
use Yii;

/**
 * This is the model class for table "core_staff".
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string|null $other_names
 * @property string $date_created
 * @property string|null $date_updated
 * @property int|null $created_by
 * @property string|null $gender
 * @property string|null $phone_contact_1
 * @property string|null $phone_contact_2
 * @property int $school_id
 * @property string|null $email_address
 * @property bool $active
 * @property bool|null $archived
 * @property string|null $passport_photo
 * @property string|null $date_archived
 * @property int|null $staff_type_id
 * @property string|null $user_level
 * @property string|null $date_of_birth
 * @property string|null $nin_no
 */
class CoreStaff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'id', 'last_name', 'first_name', 'school_id'], 'required'],
            [['date_created', 'date_updated', 'date_archived', 'date_of_birth'], 'safe'],
            [['created_by', 'school_id', 'staff_type_id'], 'default', 'value' => null],
            [['id','created_by', 'school_id', 'staff_type_id'], 'integer'],
            [['active', 'archived'], 'boolean'],
            [['passport_photo', 'nin_no'], 'string'],
            [['last_name', 'first_name', 'other_names', 'gender', 'phone_contact_1', 'phone_contact_2', 'email_address'], 'string', 'max' => 255],
            [['user_level'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'last_name' => 'Last Name',
            'first_name' => 'First Name',
            'other_names' => 'Other Names',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'gender' => 'Gender',
            'phone_contact_1' => 'Phone Contact 1',
            'phone_contact_2' => 'Phone Contact 2',
            'school_id' => 'School ID',
            'email_address' => 'Email Address',
            'active' => 'Active',
            'archived' => 'Archived',
            'passport_photo' => 'Passport Photo',
            'date_archived' => 'Date Archived',
            'staff_type_id' => 'Staff Type ID',
            'user_level' => 'User Level',
            'date_of_birth' => 'Date Of Birth',
            'nin_no' => 'Nin No',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    /**
     * Combine firstname and lastName
     */
    public function getFullNames(){
        return $this->last_name.' '.$this->first_name;
    }
}

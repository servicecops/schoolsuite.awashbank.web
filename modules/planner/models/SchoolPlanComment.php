<?php

namespace app\modules\planner\models;

use app\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use Yii;


/**
 * This is the model class for table "school_plan_comments".
 *
 * @property int $id
 * @property string $notes
 * @property int $created_by
 * @property int $subject_id
 * @property string $subject_type
 * @property string|null $date_created
 * @property string|null $date_modified
 * @property boolean $active
 */
class SchoolPlanComment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_comments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['notes', 'created_by', 'subject_id', 'subject_type'], 'required'],
            [['notes', 'subject_type'], 'string'],
            [['created_by', 'subject_id'], 'default', 'value' => null],
            [['id','created_by', 'subject_id'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notes' => 'Notes',
            'created_by' => 'created_by',
            'subject_id' => 'Subject ID',
            'subject_type' => 'Subject Type',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }

    public function getUsername()
    {
        return $this-> hasOne(User::className(), ['id' => 'created_by']);
    }
    
    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'notes',
            'subject_type',
            [
                'label' => 'created_by',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                }
            ],
            'date_created',
            'date_modified',
        ];

        if (($models = SchoolPlanComment::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    /**
     * @param $params
     * @return array
     * Return the deleted comments for particular school plan via setting
     */
    public function fetchDeletedComments($params)
    {
        $planId = $params['planId'];

        $query = (new Query())->select(['comment.id','comment.notes','comment.subject_id','comment.created_by', 'comment.date_created','comment.active','si.title','activity.plan_id'])
                  ->from('school_plan_comments comment')
                  ->innerJoin('school_plan_subactivity si', 'si.id=comment.subject_id')//subactivity 
                  ->innerJoin('school_plan_activity activity', 'activity.id=si.plan_activity_id') //activity
                  ->where(['activity.plan_id'=>$planId, 'comment.active' => false])
                  ->all(); //school plan

        //return $query ? $query : null;
        return $query ? $query : [];
    }
}

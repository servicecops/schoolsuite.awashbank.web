<?php

namespace app\modules\planner\models;

use Yii;

/**
 * This is the model class for table "school_plan_subactivity_staff_association".
 *
 * @property int $id
 * @property int $staff_id
 * @property int $created_by
 * @property string $date_created
 * @property int|null $date_modified
 * @property int $sub_activity_id
 */
class SchoolPlanSubActivityStaffAssociation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_subactivity_staff_association';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['staff_id', 'created_by', 'date_created', 'sub_activity_id'], 'required'],
            [['staff_id', 'created_by', 'date_modified', 'sub_activity_id'], 'default', 'value' => null],
            [['staff_id', 'created_by', 'date_modified', 'sub_activity_id'], 'integer'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
            'sub_activity_id' => 'Sub Activity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'staff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolPlanActivity()
    {
        return $this->hasOne(SchoolPlanActivity::className(), ['id' => 'sub_activity_id']);
    }

}

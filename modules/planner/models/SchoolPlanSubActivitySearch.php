<?php

namespace app\modules\planner\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;

/**
 * SchoolPlanSubActivitySearch represents the model behind the search form of `app\modules\planner\models\SchoolPlanSubActivity`.
 */
class SchoolPlanSubActivitySearch extends SchoolPlanSubActivity
{
    public $test,$answer,$schsearch, $modelSearch;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'plan_activity_id',], 'integer'],
            [['date_created', 'title','expected_start_date', 'expected_end_date', 'actual_start_date', 'actual_end_date'], 'safe'],
            [['modelSearch'], 'trim']

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $query = SchoolPlanSubActivity::find();
        $searchForm = 'title';
        // add conditions that should always apply here
        $columns = [
            'title',
            'description',
            'expected_start_date',
            'expected_end_date',
            [
                'label' => 'Title',
                'value' => function ($model) {
                    return $query->title; // i think

                },
            ],
            'date_created',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['plan-activity/view/', 'id' => $model['id']]);
                }
            ],
            ////
        ];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            //'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->modelSearch])
            ->andFilterWhere(['ilike', 'description', $this->modelSearch]);


        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    /*
    * Return particular fields for dataprovider
    */
    public function searchIndex($params) //needs refactoring to support school plan
    {
        $searchForm = 'title';
        $placeholder = 'search sub activities ';
        $query = (new Query())
            ->select(['ct.id', 'ct.title', 'ct.description', 'ct.expected_end_date', 'ct.expected_start_date','ct.date_created', 'ct.date_modified',])
            ->from('school_subactivity ct')
            ->innerJoin('school_plan csc', 'ct.plan_activity_id= csc.id');
        $query->orderBy('ct.id desc');

        if (\app\components\ToWords::isSchoolUser()) {

                //$query->andWhere(['ct.plan_activity_id' => Yii::$app->user->identity->plan_activity_id]); //needs refactor

        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {

                $query->andWhere(['csc.id' => $this->schsearch]); //needs refactor
           // }
        }
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'title',
            [
                'label' => 'Plan Name',
                'value'=>function($model){
                    return Html::a($model['description'], ['/planner/planner/view', 'id' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'expected_start_date',
            'expected_end_date',
            //'term_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['planner/view', 'id' => $model['id']]);
                },
            ],
            ////
        ];
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function searchNewIndex($params)
    {
        $query = SchoolPlanSubActivity::find()->where([
             'plan_activity_id'=>$params['activity_id'],
             'active' => true
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'pagination' => [
            //     'pagesize' => 5 // in case you want a default pagesize
            // ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            //'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
            'plan_activity_id' => $this->plan_activity_id,
        ]);

        $query->andFilterWhere(['ilike', 'title', $this->modelSearch])
               ->andFilterWhere(['ilike', 'description', $this->modelSearch]);

        return $dataProvider;
    }

}

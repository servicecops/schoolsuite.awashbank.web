<?php

namespace app\modules\planner\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * SchoolPlanChangeSearch represents the model behind the search form of `app\modules\planner\models\SchoolPlanChanges`.
 */
class SchoolPlanChangeSearch extends SchoolPlanChanges
{
   
    // This attribute will hold the values to filter our database data
    //public $date_created_range;
    //public $date_created_range, $from_date, $to_date;
    public $from_date, $to_date;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // return [
        //     [['id', 'created_by', 'term_id',], 'integer'],
        //     [['date_created', 'plan_name','expected_start_date', 'expected_end_date', 'actual_start_date', 'actual_end_date'], 'safe'],
        //     [['modelSearch'], 'trim']

        // ];
        return ArrayHelper::merge(
            [
				//[['created_at_range'], 'safe'] // add a rule to collect the values
                [['from_date', 'to_date'], 'safe']
			],
			parent::rules()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params)
    {
        $query = SchoolPlanChanges::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);
        Yii::trace("date params");
        Yii::trace($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'date_created' => $this->date_created,
        //     //'date_modified' => $this->date_modified,
        //     //'created_by' => $this->created_by,
        //     //'term_id' => $this->term_id,
        // ]);

        // $query->andFilterWhere(['ilike', 'plan_name', $this->modelSearch]);
        //        //->andFilterWhere(['ilike', 'description', $this->modelSearch]);

        // do we have values? if so, add a filter to our query
		// if(!empty($this->created_at_range) && strpos($this->created_at_range, '-') !== false) {
		// 	list($start_date, $end_date) = explode(' - ', $this->created_at_range);
		// 	$query->andFilterWhere(['between', 'user.created_at', strtotime($start_date), strtotime($end_date)]);
		// }
        $query->andFilterWhere(['between', 'date_created', strtotime($this->from_date), strtotime($this->to_date)]);		

        return $dataProvider;
    }



}
<?php

namespace app\modules\planner\models;

use Yii;

/**
 * This is the model class for table "school_plan_attachment".
 *
 * @property int $id
 * @property string $attach_name
 * @property int $subject_id
 * @property string|null $date_created
 * @property string|null $date_modified
 * @property string $file_attachment
 * @property int|null $created_by
 * @property boolean $active
 */
class SchoolPlanAttachment extends \yii\db\ActiveRecord
{   
    /**
     * @var UploadedFile
     */
    public $image;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject_id', 'file_attachment'], 'required'],
            [['subject_id', 'created_by'], 'default', 'value' => null],
            [['subject_id', 'created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
            [['file_attachment'], 'string'],
            [['attach_name'], 'string', 'max' => 255],
            //image validation
            [['image'], 'file', 'extensions' => 'jpg,jpeg,png','pdf','xls', 'xlsx','csv','docx','doc'],
            [['image'], 'file', 'maxSize' => '100000']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attach_name' => 'Attachment Name',
            'subject_id' => 'Subject ID',
            'date_created' => 'Uploaded On',
            'date_modified' => 'Date Modified',
            'file_attachment' => 'File Attachment',
            'created_by' => 'Created By',
        ];
    }
}

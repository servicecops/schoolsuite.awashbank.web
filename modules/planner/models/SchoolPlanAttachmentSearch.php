<?php

namespace app\modules\planner\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * SchoolPlanAttachmentSearch represents the model behind the search form of `app\modules\planner\models\SchoolPlanAttachment`.
 */
class SchoolPlanAttachmentSearch extends SchoolPlanAttachment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attach_name','file_attachment', 'date_created', 'date_modified'], 'safe'],
            [['subject_id', 'id', 'created_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $subjectId = $params['id']? $params['id'] : '';
        $activeStatus = $params['active']? $params['active']:true;
        $query = SchoolPlanAttachment::find()
                       ->where(['subject_id'=> $subjectId , 'active'=> $activeStatus ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'subject_id' => $this->subject_id,
            'id' => $this->id,
            'date_modified' => $this->date_modified,
            'created_by' => $this->created_by,
            'active' => $this->active //'model' => $attachmentModel,
        ]);

        $query->andFilterWhere(['ilike', 'attach_name', $this->attach_name])
            ->andFilterWhere(['ilike', 'file_attachment', $this->file_attachment]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied for deleted attachments
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDeletedAttachments($params)
    {
        
        $planId = $params['planId'];

        $query = (new Query())->select(['attach.id','attach.attach_name','attach.subject_id','attach.file_attachment','attach.created_by', 'attach.active','si.title','activity.plan_id'])
                  ->from('school_plan_attachment attach')
                  ->innerJoin('school_plan_subactivity si', 'si.id=attach.subject_id')//subactivity 
                  //->innerJoin('school_plan_activity activity', 'activity.id=attach.subject_id')
                  ->innerJoin('school_plan_activity activity', 'activity.id=si.plan_activity_id') //activity
                  ->where(['activity.plan_id'=>$planId, 'attach.active' => false]); //school plan


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'date_created' => $this->date_created,
        //     'created_by' => $this->created_by
        // ]);

        // $query->andFilterWhere(['ilike', 'attach_name', $this->attach_name])
        //     ->andFilterWhere(['ilike', 'file_attachment', $this->file_attachment]);

        return $dataProvider;

    }
}

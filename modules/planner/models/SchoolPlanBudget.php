<?php

namespace app\modules\planner\models;

use Yii;

/**
 * This is the model class for table "school_plan_budget".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property float $unit_cost
 * @property int $item_quantity
 * @property string|null $comments
 * @property int $activity_id
 * @property string $date_modified
 * @property string $date_created
 * @property boolean $active
 * @property string $approval_date
 */
class SchoolPlanBudget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_budget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'unit_cost', 'item_quantity', 'activity_id'], 'required'],
            [['title', 'description', 'comments'], 'string'],
            [['unit_cost'], 'number'],
            [['item_quantity', 'activity_id'], 'default', 'value' => null],
            [['id','item_quantity', 'activity_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'unit_cost' => 'Unit Cost',
            'item_quantity' => 'Item Quantity',
            'comments' => 'Comments',
            'activity_id' => 'Activity ID',
        ];
    }
}

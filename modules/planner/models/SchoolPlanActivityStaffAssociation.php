<?php

namespace app\modules\planner\models;

use Yii;

/**
 * This is the model class for table "school_plan_activity_staff_association".
 *
 * @property int $id
 * @property int $activity_id
 * @property int $staff_id
 * @property int $created_by
 * @property string $date_created
 * @property string|null $date_modified
 */
class SchoolPlanActivityStaffAssociation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_plan_activity_staff_association';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'id','activity_id', 'staff_id', 'created_by', 'date_created'], 'required'],
            [['activity_id', 'staff_id', 'created_by'], 'default', 'value' => null],
            [['id','activity_id', 'staff_id', 'created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activity_id' => 'Activity ID',
            'staff_id' => 'Staff ID',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(CoreStaff::className(), ['id' => 'staff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolPlanActivity()
    {
        return $this->hasOne(SchoolPlanActivity::className(), ['id' => 'activity_id']);
    }


}

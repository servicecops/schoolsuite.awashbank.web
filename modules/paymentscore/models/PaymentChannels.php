<?php

namespace app\modules\paymentscore\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "payment_channels".
 *
 * @property integer $id
 * @property string $channel_code
 * @property string $password
 * @property string $channel_name
 * @property string $channel_email
 * @property string $channel_address
 * @property boolean $active
 * @property string $cert_data
 * @property integer $incorrect_auth_count
 * @property string $date_created
 * @property string $date_modified
 *
 * @property InstitutionPaymentChannelAssociation[] $institutionPaymentChannelAssociations
 * @property PaymentsReceived[] $paymentsReceiveds
 */
class PaymentChannels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_channels';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_modified',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_code', 'channel_name'], 'required', 'except' => 'photo'],
            [['channel_code', 'password', 'channel_name', 'channel_email', 'channel_address', 'cert_data', 'channel_type', 'payment_instructions'], 'string'],
            [['active'], 'boolean'],
            [['incorrect_auth_count', 'payment_channel_logo', 'payment_channel_processing_strategy'], 'integer'],
            [['log_for_auto_settlement'], 'boolean'],
            [['date_created', 'date_modified', 'log_for_auto_settlement'], 'safe'],
            [['channel_code'], 'unique']
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['photo'] = ['payment_channel_logo'];
        $scenarios['update'] = ['active', 'log_for_auto_settlement', 'payment_channel_logo', 'channel_code', 'channel_name', 'channel_address', 'cert_data', 'channel_type', 'payment_instructions', 'payment_type', 'payment_channel_processing_strategy'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_code' => 'Channel Code',
            'password' => 'Password',
            'channel_name' => 'Channel Name',
            'channel_email' => 'Channel Email',
            'channel_address' => 'Channel Address',
            'active' => 'Active',
            'cert_data' => 'Cert Data',
            'incorrect_auth_count' => 'Incorrect Auth Count',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
            'payment_instructions' => 'Payment Instructions',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionPaymentChannelAssociations()
    {
        return $this->hasMany(InstitutionPaymentChannelAssociation::className(), ['payment_channel' => 'id']);
    }

    public function getLogo()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'payment_channel_logo']);
    }

    public function getDescLogo()
    {
        return '<img src="' . Url::to(['/import/import/image-link', 'id' => $this->payment_channel_logo]) . '" width="30px" /> ' . $this->channel_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsReceiveds()
    {
        return $this->hasMany(PaymentsReceived::className(), ['payment_channel' => 'id']);
    }

    public static function getMobileChannels()
    {
        return self::find()->where(['channel_type' => 'Mobile'])->all();
    }

    public static function getNonMobileChannels()
    {
        return self::find()->where(['<>', 'channel_type', 'Mobile'])->all();
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->payment_channel_processing_strategy = PcProcessingStrategy::findOne(['strategy_code'=>'MAIN_ACCOUNT'])->id;
        $this->log_for_auto_settlement = false;
        return true;
    }
}

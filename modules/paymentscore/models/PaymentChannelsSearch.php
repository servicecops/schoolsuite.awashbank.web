<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

if (!Yii::$app->session->isActive){
          session_start();  
      }

/**
 * PaymentChannelsSearch represents the model behind the search form about `app\models\PaymentChannels`.
 */
class PaymentChannelsSearch extends PaymentChannels
{
    public $modelSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'incorrect_auth_count'], 'integer'],
            [['channel_code', 'password', 'channel_name', 'channel_email', 'channel_address', 'modelSearch', 'cert_data', 'date_created', 'date_modified'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentChannels::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'incorrect_auth_count' => $this->incorrect_auth_count,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified,
        ]);

        $query->orFilterWhere(['ilike', 'channel_code', $this->modelSearch])
            ->orFilterWhere(['ilike', 'password', $this->modelSearch])
            ->orFilterWhere(['ilike', 'channel_name', $this->modelSearch])
            ->orFilterWhere(['ilike', 'channel_email', $this->modelSearch])
            ->orFilterWhere(['ilike', 'channel_address', $this->modelSearch]);


        unset($_SESSION['exportData']);
        $_SESSION['exportData'] = $dataProvider;

        return $dataProvider;
    }

    public static function getExportData() 
    {
        $data = [
                'data'=>$_SESSION['exportData'],
                'fileName'=>'payment_channels', 
                'title'=>'List of payment channels',
                'exportFile'=>'/payment-channels/exportPdfExcel',
            ];
        return $data;
    }

}

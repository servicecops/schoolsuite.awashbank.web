<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "thirdparty_school_providers".
 *
 * @property integer $id
 * @property string $provider_id
 * @property string $date_created
 * @property boolean $active
 * @property string $provider_name
 */
class ThirdpartySchoolProvider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thirdparty_school_providers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_id', 'provider_name'], 'required'],
            [['provider_id'], 'string'],
            [['date_created'], 'safe'],
            [['active'], 'boolean'],
            [['provider_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'provider_id' => 'Provider ID',
            'date_created' => 'Date Created',
            'active' => 'Active',
            'provider_name' => 'Provider Name',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Thirdparty School Provider',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'provider_id' => ['label'=>'Provider Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'active' => ['type'=>'number', 'label'=>'Active'],
                    'provider_name' => ['label'=>'Provider Name'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'provider_id'],
            ['attribute'=>'date_created'],
            ['attribute'=>'active'],
            ['attribute'=>'provider_name'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Thirdparty School Provider" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }


}

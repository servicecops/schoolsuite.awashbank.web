<?php

namespace app\modules\paymentscore\models;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;
/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class AutoSettlementSearch extends Model
{
    public $start_date, $end_date, $payment_channel, $school_name, $settled, $settlement_bank_id, $school_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date', 'payment_channel', 'school_name', 'settled', 'settlement_bank_id', 'school_id'], 'safe']
        ];
    }

    public function search($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time()+86400);
        $end_date = $this->end_date ? date('Y-m-d', strtotime($this->end_date . ' +1 day')) : $today;
        $query = (new Query())->select(['asr.id', 'asr.date_created AS date', 'chn.channel_name AS chn', 'nb.bank_name', 'chn.payment_channel_logo', 'settled', 'student_name', 'student_code', 'school_name','settlement_reference', 'asr.amount', 'nb.bank_logo as destination_bank_logo', 'asr.school_code as school_code'])
                    ->from('auto_settlement_requests asr')
                    ->innerJoin('payment_channels chn', 'chn.id=asr.source_payment_channel')
                    ->innerJoin('payments_received pr', 'pr.id=asr.payment_id')
                    ->innerJoin('core_nominated_bank nb', 'nb.id=asr.settlement_bank_id')
                    ->filterWhere(['between', 'asr.date_created', $this->start_date, $end_date])
                    ->andFilterWhere(['ilike', 'asr.school_name', $this->school_name])
                    ->andFilterWhere(['asr.source_payment_channel'=>$this->payment_channel])
                    ->andFilterWhere(['asr.settlement_bank_id'=>$this->settlement_bank_id])
                    ->andFilterWhere(['pr.school_id'=>$this->school_id])
                    ->andFilterWhere(['asr.settled'=>$this->settled]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'channel_name', 'bank_name', 'student_name', 'student_code', 'school_name', 'settlement_reference', 'amount', 'school_code'
                ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('asr.date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public static function findSettlement($id)
    {
        $query = (new Query())->select(['asr.id', 'asr.date_created AS date', 'chn.channel_name AS chn', 'nb.bank_name', 'student_name', 'student_code', 'school_name', 'asr.amount', 'school_code', 'schoolpay_reciept_number AS recpt_no', 'settled', 'settlement_reference', 'source_payment_channel_code', 'last_fetch_date', 'last_process_status', 'payer_phone', 'payment_id'])
                    ->from('auto_settlement_requests asr')
                    ->innerJoin('payment_channels chn', 'chn.id=asr.source_payment_channel')
                    ->innerJoin('nominated_bank_details nb', 'nb.id=asr.settlement_bank_id')
                    ->where(['asr.id'=>$id])
                    ->limit(1);

        return $query->one(Yii::$app->db2);
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData'],
            'labels'=>['date_created', 'channel_name', 'bank_name', 'student_name', 'student_code', 'school_name', 'settlement_reference', 'amount'],
            'fileName'=>'Mobile_Money_Settlement',
            'title'=>'Mobile Money Settlement',
            'exportFile'=>'@app/views/payments-received/exportMMPdfExcel',
        ];

        return $data;
    }

}

<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "payment_channel_account_transaction_history".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $amount
 * @property string $posting_user
 * @property string $description
 * @property integer $payment_id
 * @property string $trans_type
 * @property integer $account_id
 * @property boolean $reversal_flag
 * @property boolean $reversed
 * @property string $date_reversed
 * @property integer $balance_after
 * @property integer $balance_before
 * @property string $channel_transaction_processed_date
 *
 * @property PaymentChannelAccountGl $account
 * @property PaymentsReceived $payment
 */
class ChannelTransactions extends \yii\db\ActiveRecord
{
    public $school;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_channel_account_transaction_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_reversed', 'channel_transaction_processed_date'], 'safe'],
            [['amount', 'account_id'], 'required'],
            [['amount', 'payment_id', 'account_id', 'balance_after', 'balance_before'], 'integer'],
            [['posting_user', 'description', 'trans_type'], 'string'],
            [['reversal_flag', 'reversed'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'amount' => 'Amount',
            'posting_user' => 'Posting User',
            'description' => 'Description',
            'payment_id' => 'School',
            'trans_type' => 'Trans Type',
            'account_id' => 'Account ID',
            'reversal_flag' => 'Reversal Flag',
            'reversed' => 'Reversed',
            'date_reversed' => 'Date Reversed',
            'balance_after' => 'Bal After',
            'balance_before' => 'Bal Before',
            'channel_transaction_processed_date' => 'Channel Transaction Processed Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(PaymentChannelAccountGl::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentsReceived::className(), ['id' => 'payment_id']);
    }
}

<?php

namespace app\modules\paymentscore\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use app\models\Student;

/**
 * This is the model class for table "student_account_transaction_history".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $amount
 * @property string $outstanding_balance_after;
 * @property string $posting_user
 * @property string $description
 * @property integer $fee_association_id
 * @property integer $payment_id
 * @property string $trans_type
 * @property integer $account_id
 * @property boolean $reversal_flag
 * @property boolean $reversed
 * @property string $date_reversed
 *
 * @property InstitutionFeeClassAssociation $feeAssociation
 * @property PaymentsReceived $payment
 * @property StudentAccountGl $account
 */
class StudentAccountHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_account_transaction_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_reversed'], 'safe'],
            [['amount', 'account_id'], 'required'],
            [['amount', 'fee_association_id', 'payment_id', 'account_id'], 'integer'],
            [['posting_user', 'description', 'trans_type'], 'string'],
            [['reversal_flag', 'reversed'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date',
            'amount' => 'Amount',
            'posting_user' => 'Posting User',
            'description' => 'Description',
            'fee_association_id' => 'Paid For',
            'payment_id' => 'Channel',
            'trans_type' => 'Trans Type',
            'account_id' => 'Account',
            'balance_after' => 'Balance',
            'reversal_flag' => 'Reversal Flag',
            'reversed' => 'Reversed',
            'date_reversed' => 'Date Reversed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeeAssociation()
    {
        return $this->hasOne(InstitutionFeeStudentAssociation::className(), ['id' => 'fee_association_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentsReceived::className(), ['id' => 'payment_id']);
    }

    public function getPaymentChannel()
    {
       return $this->payment->paymentChannel->channel_code;
    }

    public function getStudent()
    {
       return $this->hasOne(CoreStudent::className(), ['student_account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(StudentAccountGl::className(), ['id' => 'account_id']);
    }
}

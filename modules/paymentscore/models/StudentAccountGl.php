<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "student_account_gl".
 *
 * @property integer $id
 * @property string $account_title
 * @property integer $account_balance
 *
 * @property StudentAccountTransactionHistory[] $studentAccountTransactionHistories
 * @property StudentInformationt $studentInformation
 */
class StudentAccountGl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_account_gl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_title'], 'string'],
            [['account_balance'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_title' => 'Account Title',
            'account_balance' => 'Account Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountHistories()
    {
       return  $this->hasMany(StudentAccountHistory::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentInformation()
    {
        return $this->hasOne(StudentInformation::className(), ['student_account_id' => 'id']);
    }
}

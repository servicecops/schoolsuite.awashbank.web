<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "auto_settlement_requests".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $source_payment_channel
 * @property string $source_payment_channel_code
 * @property string $student_code
 * @property string $student_name
 * @property string $school_name
 * @property string $source_channel_trans_id
 * @property string $schoolpay_reciept_number
 * @property integer $payment_id
 * @property integer $amount
 * @property string $payer_phone
 * @property string $last_fetch_date
 * @property string $last_process_status
 * @property string $date_last_settled
 * @property string $settlement_reference
 * @property string $school_code
 * @property boolean $settled
 * @property integer $settlement_bank_id
 */
class AutoSettlementRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auto_settlement_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'last_fetch_date', 'date_last_settled'], 'safe'],
            [['source_payment_channel', 'student_code', 'school_code', 'settlement_bank_id'], 'required'],
            [['source_payment_channel', 'payment_id', 'amount', 'settlement_bank_id'], 'integer'],
            [['source_channel_trans_id', 'schoolpay_reciept_number'], 'string'],
            [['settled'], 'boolean'],
            [['source_payment_channel_code', 'student_code', 'student_name', 'school_name', 'payer_phone', 'last_process_status', 'settlement_reference', 'school_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'source_payment_channel' => 'Source Payment Channel',
            'source_payment_channel_code' => 'Source Payment Channel Code',
            'student_code' => 'Student Code',
            'student_name' => 'Student Name',
            'school_name' => 'School Name',
            'source_channel_trans_id' => 'Source Channel Trans ID',
            'schoolpay_reciept_number' => 'Schoolpay Reciept Number',
            'payment_id' => 'Payment ID',
            'amount' => 'Amount',
            'payer_phone' => 'Payer Phone',
            'last_fetch_date' => 'Last Fetch Date',
            'last_process_status' => 'Last Process Status',
            'date_last_settled' => 'Date Last Settled',
            'settlement_reference' => 'Settlement Reference',
            'school_code' => 'School Code',
            'settled' => 'Settled',
            'settlement_bank_id' => 'Settlement Bank ID',
        ];
    }
}

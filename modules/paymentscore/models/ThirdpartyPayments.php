<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "thirdparty_provider_payment_notification_queue".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property integer $provider_id
 * @property boolean $sent_to_provider
 * @property string $provider_notification_reference
 */
class ThirdpartyPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thirdparty_provider_payment_notification_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'provider_id', 'sent_to_provider'], 'required'],
            [['payment_id', 'provider_id'], 'integer'],
            [['sent_to_provider'], 'boolean'],
            [['provider_notification_reference'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Payment ID',
            'provider_id' => 'Provider ID',
            'sent_to_provider' => 'Sent To Provider',
            'provider_notification_reference' => 'Provider Notification Reference',
        ];
    }
}

<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "bulk_payments_records".
 *
 * @property integer $id
 * @property string $file_name
 * @property string $hash
 * @property integer $school_id
 * @property integer $uploaded_by
 * @property string $date_uploaded
 * @property string $description
 * @property string $secrete_code
 * @property boolean $processed
 * @property boolean $approved
 */
class BulkPaymentRecords extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bulk_payments_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'hash', 'school_id', 'description'], 'required'],
            [['file_name', 'hash', 'description'], 'string'],
            [['school_id', 'uploaded_by'], 'integer'],
            [['date_uploaded'], 'safe'],
            [['processed', 'approved'], 'boolean'],
            [['secrete_code'], 'string', 'max' => 20],
            [['hash'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'hash' => 'Hash',
            'school_id' => 'School ID',
            'uploaded_by' => 'Uploaded By',
            'date_uploaded' => 'Date Uploaded',
            'description' => 'Description',
            'secrete_code' => 'Secrete Code',
            'processed' => 'Processed',
            'approved' => 'Approved',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Bulk Payment Records',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'file_name' => ['label'=>'File Name'],
                    'hash' => ['label'=>'Hash'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                    'uploaded_by' => ['type'=>'number', 'label'=>'Uploaded By'],
                    'date_uploaded' => ['type'=>'date', 'label'=>'Date Uploaded'],
                    'description' => ['label'=>'Description'],
                    'secrete_code' => ['label'=>'Secrete Code'],
                    'processed' => ['type'=>'number', 'label'=>'Processed'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'file_name'],
            ['attribute'=>'hash'],
            ['attribute'=>'school_id'],
            ['attribute'=>'uploaded_by'],
            ['attribute'=>'date_uploaded'],
            ['attribute'=>'description'],
            ['attribute'=>'secrete_code'],
            ['attribute'=>'processed'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Bulk Payment Records" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "payment_channel_payment_processing_strategies".
 *
 * @property integer $id
 * @property string $strategy_code
 * @property string $strategy_description
 *
 * @property PaymentChannels[] $paymentChannels
 */
class PcProcessingStrategy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_channel_payment_processing_strategies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['strategy_code', 'strategy_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'strategy_code' => 'Strategy Code',
            'strategy_description' => 'Strategy Description',
        ];
    }

    public function getCodeDesc(){
        return $this->strategy_code." - ".$this->strategy_description;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentChannels()
    {
        return $this->hasMany(PaymentChannels::className(), ['payment_channel_processing_strategy' => 'id']);
    }
}

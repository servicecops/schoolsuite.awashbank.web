<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ThirdpartyPayments;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;

/**
 * ThirdpartyPaymentsSearch represents the model behind the search form about `app\models\ThirdpartyPayments`.
 */
class ThirdpartyPaymentsSearch extends ThirdpartyPayments
{
    public $school, $date_from, $date_to, $channel;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_id', 'provider_id'], 'integer'],
            [['sent_to_provider'], 'boolean'],
            [['provider_notification_reference','channel', 'sent_to_provider' ,'school', 'date_from', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($params){
        $this->load($params);
        $to_date = $this->date_to ? date('Y-m-d') : $this->date_to;
        $end_date = $this->date_from ? date('Y-m-d', strtotime($to_date."+1 days")) : null;
        $query = (new Query())->select(['pr.date_created',  'pr.channel_memo', 'pc.channel_name', 'pc.payment_channel_logo',
            'pr.amount', 'pr.reciept_number', 'qu.payment_id', 'si.first_name','si.middle_name', 'si.last_name', 'prv.provider_name', 'sch.school_name', 'qu.sent_to_provider'])
            ->from('thirdparty_provider_payment_notification_queue qu')
            ->innerJoin('payments_received pr', 'pr.id=qu.payment_id')
            ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel')
            ->innerJoin('thirdparty_school_providers prv', 'prv.id=qu.provider_id')
            ->innerJoin('school_information sch', 'sch.id=pr.school_id')
            ->innerJoin('core_student si', 'si.id=pr.student_id')
            ->andWhere(['pr.reversed'=>false, 'pr.reversal'=>false])
            ->andFilterWhere(['pr.school_id'=>$this->school])
            ->andFilterWhere(['pr.payment_channel'=>$this->channel])
            ->andFilterWhere(['prv.id'=>$this->provider_id])
            ->andFilterWhere(['between', 'pr.date_created', $this->date_from, $end_date])
            ->andFilterWhere(['qu.sent_to_provider'=>$this->sent_to_provider]);


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'channel_memo', 'channel_name','provider_name', 'school_name', 'sent_to_provider', 'amount'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pr.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(Yii::$app->db2), 'pages'=>$pages, 'sort'=>$sort];

    }
}




<?php

namespace app\modules\paymentscore\models;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "payment_channels".
 *
 * @property integer $id
 * @property string $channel_code
 * @property string $password
 * @property string $channel_name
 * @property string $channel_email
 * @property string $channel_address
 * @property boolean $payment_channel
 * @property string $channel_request_id
 * @property integer $incorrect_auth_count
 * @property string $date_created
 * @property string $date_modified
 *
 * @property PaymentsReceived[] $paymentsReceiveds
 */
class PaymentRequestLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_requests_log';
    }

//    public function behaviors()
//    {
//        return [
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'date_created',
//                'value' => new Expression('NOW()'),
//            ],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_request_id', 'payment_channel'], 'required', ],

            [['date_created',  'signature','raw_data','request_response','response_time'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'password' => 'Password',
            'channel_name' => 'Channel Name',
            'channel_email' => 'Channel Email',
            'channel_address' => 'Channel Address',
            'active' => 'Active',
            'cert_data' => 'Cert Data',
            'incorrect_auth_count' => 'Incorrect Auth Count',
            'date_created' => 'Date Created',
            'payment_instructions' => 'Payment Instructions',
        ];
    }

}

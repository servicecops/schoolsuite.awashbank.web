<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "image_bank".
 *
 * @property integer $id
 * @property string $description
 * @property string $image_base64
 *
 * @property StudentInformation[] $studentInformations
 */
class ImageBank extends \yii\db\ActiveRecord
{
    public $crop_info, $upload_image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['image_base64', 'required', 'message'=>'Image is required'],
            [['description', 'image_base64'], 'string'],
            // [['upload_image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            ['crop_info', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'image_base64' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentInformations()
    {
        return $this->hasMany(StudentInformation::className(), ['student_image' => 'id']);
    }
}

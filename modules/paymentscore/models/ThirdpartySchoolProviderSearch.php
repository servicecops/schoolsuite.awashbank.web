<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

if (!Yii::$app->session->isActive){
          session_start();  
      }

/**
 * ThirdpartySchoolProviderSearch represents the model behind the search form about `app\models\SchoolInformation`.
 */
class ThirdpartySchoolProviderSearch extends ThirdpartySchoolProvider
{
    public $modelSearch, $schsearch, $external_school_code;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['provider_id', 'provider_name', 'date_created', 'active', 'enable_inquiry', 'enable_notification', 'schsearch', 'external_school_code'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['*'])
            ->from('thirdparty_school_providers p')
            ->andFilterWhere(['ilike', 'provider_name', $this->provider_name]);

            $pages = new Pagination(['totalCount' => $query->count()]);
            $sort = new Sort([
                'attributes' => ['provider_id', 'provider_name'],
            ]);
            ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('p.id');
            unset($_SESSION['findData']);
            $_SESSION['findData'] = $query;
            $query->offset($pages->offset)->limit($pages->limit);
            return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public static function getExportQuery() 
    {
    $data = [
            'data'=>$_SESSION['findData'],
            'labels'=>['school_code', 'school_name', 'physical_address', 'school_type_name'],
            'fileName'=>'Providers',
            'title'=>'Providers',
            'exportFile'=>'/school-information/exportPdfExcel',
        ];

        return $data;
    }
}

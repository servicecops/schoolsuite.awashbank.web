<?php

namespace app\modules\paymentscore\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use Yii;

/**
 * This is the model class for table "payments_received".
 *
 * @property integer $id
 * @property string $reciept_number
 * @property string $date_created
 * @property integer $student_id
 * @property integer $school_id
 * @property integer $payment_channel
 * @property string $channel_trans_id
 * @property string $channel_memo
 * @property integer $amount
 * @property string $channel_process_date
 * @property string $channel_payment_type
 *
 * @property PaymentChannels $paymentChannel
 * @property CoreSchool $school
 * @property CoreStudent $student
 * @property SchoolAccountTransactionHistory[] $schoolAccountTransactionHistories
 * @property StudentAccountTransactionHistory[] $studentAccountTransactionHistories
 */
class PaymentsReceived extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payments_received';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'channel_process_date'], 'safe'],
            [['student_id', 'school_id', 'payment_channel', 'channel_trans_id'], 'required'],
            [['student_id', 'school_id', 'payment_channel', 'amount','schpay_payment_code'], 'integer'],
            [['channel_trans_id', 'channel_memo', 'channel_payment_type'], 'string'],
            [['payment_channel', 'channel_trans_id'], 'unique', 'targetAttribute' => ['payment_channel', 'channel_trans_id'], 'message' => 'The combination of Payment Channel and Channel Trans ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reciept_number' => 'Receipt Number',
            'date_created' => 'Date Created',
            'student_id' => 'Student',
            'school_id' => 'School',
            'payment_channel' => 'Payment Channel',
            'channel_trans_id' => 'Channel Trans ID',
            'channel_memo' => 'Channel Memo',
            'amount' => 'Amount',
            'channel_process_date' => 'Channel Process Date',
            'channel_payment_type' => 'Channel Payment Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentChannel()
    {
        return $this->hasOne(PaymentChannels::className(), ['id' => 'payment_channel']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(CoreStudent::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolAccountTransactionHistories()
    {
        return $this->hasMany(SchoolAccountTransactionHistory::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAccountTransactionHistories()
    {
        return $this->hasMany(StudentAccountTransactionHistory::className(), ['payment_id' => 'id']);
    }
}

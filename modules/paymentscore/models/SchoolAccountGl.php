<?php

namespace app\modules\paymentscore\models;

use app\modules\schoolcore\models\CoreSchool;
use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "school_account_gl".
 *
 * @property integer $id
 * @property string $account_title
 * @property integer $account_balance
 *
 * @property CoreSchool[] $schoolInformations
 */
class SchoolAccountGl extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_account_gl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_title'], 'string'],
            [['account_balance'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_title' => 'Account Title',
            'account_balance' => 'Account Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolInformations()
    {
        return $this->hasMany(CoreSchool::className(), ['school_account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountHistories()
    {
        // return $this->hasMany(SchoolAccountTransactionHistory::className(), ['account_id' => 'id']);
        $query = SchoolAccountTransactionHistory::find()->where(['account_id' => $this->id]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $query->offset($pages->offset)->limit($pages->limit)->orderBy('date_created DESC');
        return ['query'=>$query->all(), 'pages'=>$pages];
    }
}

<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "thirdparty_provider_school_association".
 *
 * @property integer $id
 * @property integer $school_id
 * @property integer $provider_id
 * @property string $date_created
 * @property string $external_school_code
 */
class ThirdpartyProviderSchoolAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thirdparty_provider_school_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'provider_id'], 'required'],
            [['school_id', 'provider_id'], 'integer'],
            [['date_created', 'id', 'school_id', 'provider_id'], 'safe'],
            [['external_school_code'], 'string', 'max' => 255],
            [['school_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'provider_id' => 'Provider ID',
            'date_created' => 'Date Created',
            'external_school_code' => 'External School Code',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Thirdparty Provider School Association',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'school_id' => ['type'=>'number', 'label'=>'School Id'],
                    'provider_id' => ['type'=>'number', 'label'=>'Provider Id'],
                    'date_created' => ['type'=>'date', 'label'=>'Date Created'],
                    'external_school_code' => ['label'=>'External School Code'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'school_id'],
            ['attribute'=>'provider_id'],
            ['attribute'=>'date_created'],
            ['attribute'=>'external_school_code'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Thirdparty Provider School Association" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

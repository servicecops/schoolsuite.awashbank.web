<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;
use yii\data\ActiveDataProvider;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * SchoolAccountTransactionHistorySearch represents the model behind the search form about `app\models\SchoolAccountTransactionHistory`.
 */
class SchoolAccountTransactionHistorySearch extends SchoolAccountTransactionHistory
{
    /**
     * @inheritdoc
     */
    public $date_from, $date_to, $transaction_id, $branch_id, $branch_region, $student_campus, $campus_id;

    public static function findHistory($id)
    {
        $query = (new Query())->select(['th.id', 'th.date_created', 'th.amount', 'description', 'pc.channel_name', 'pr.channel_memo', 'th.trans_type', 'sgl.account_title', 'pr.channel_depositor_phone', 'pr.channel_depositor_name', 'pr.channel_depositor_branch', 'pr.reciept_number'])
            ->from('school_account_transaction_history th')
            ->leftJoin('school_account_gl sgl', 'sgl.id=th.account_id')
            ->leftJoin('payments_received pr', 'pr.id=th.payment_id')
            ->leftJoin('payment_channels pc', 'pc.id=pr.payment_channel')
            ->where(['th.id' => $id])
            ->limit(1);
        return $query->one(Yii::$app->db);
    }

    public static function getExportQuery()
    {


        $data = [
            'data' => $_SESSION['findData']['query'],
            'labels' => $_SESSION['findData']['cols'],
            'title' => $_SESSION['findData']['title'],
            'fileName' => $_SESSION['findData']['file_name'],
            'exportFile' => $_SESSION['findData']['exportFile'],
        ];
        return $data;
    }

    public function rules()
    {
        return [
            [['id', 'amount', 'account_id', 'balance_after',], 'integer'],
            [['date_created', 'posting_user', 'description', 'trans_type', 'campus_id','payment_id','student_campus', 'date_reversed', 'date_from', 'date_to', 'account_id', 'transaction_id', 'branch_id', 'branch_region'], 'safe'],
            [['reversal_flag', 'reversed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /*    public static function getExportQuery()
        {
        $data = [
                'data'=>$_SESSION['findData'],
                'labels'=> ['date_created', 'description', 'student_name', 'student_code','registration_number', 'trans_type', 'channel_trans_id', 'reciept_number', 'channel_code', 'channel_memo', 'amount'],
                'fileName'=>'transaction_histories',
                'title'=>'School Transaction History',
                'exportFile'=>'/school-account-transaction-history/exportPdfExcel',
            ];

        return $data;
        }*/

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search($account, $params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolsuite_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -60 days'));
        }

        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();
        $query->select(['th.id', 'th.date_created', 'th.description', 'th.trans_type',
            'pr.reciept_number', 'pr.channel_trans_id', 'th.payment_id', 'th.reversed', 'pc.channel_code',
            'pc.channel_name', 'pr.channel_memo', 'th.amount', 'th.balance_after', 'pc.payment_channel_logo',
            'sinfo.student_code', 'sinfo.school_student_registration_number as registration_number',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name"])
            ->from('school_account_transaction_history th')
            ->innerJoin('payments_received pr', 'pr.id=th.payment_id')
            ->innerJoin('core_student sinfo', 'sinfo.id=pr.student_id')
            ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel');
        if (\app\components\ToWords::isSchoolUser()) {
            $query = $query->where(['th.account_id' => $account]);
        }
        if (Yii::$app->user->can('schoolsuite_admin')) {
            $query = $query->andFilterWhere(['th.account_id' => $this->account_id]);
        }
        $query->andFilterWhere(['pr.payment_channel' => $this->payment_id]);
        $query->andFilterWhere(['pr.channel_trans_id' => $this->transaction_id]);
        //Disable reversed and reversal transactions from the search
        $query->andFilterWhere(['pr.reversal' => false]);
        $query->andFilterWhere(['pr.reversed' => false]);
        if ($this->date_from) {
            $query->andWhere(['between', 'th.date_created', $this->date_from, $date_to]);

        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'description', 'trans_type', 'reciept_number', 'channel_trans_id', 'channel_code', 'channel_memo', 'amount', 'balance_after'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('th.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page]; //Use slave db connection in reporting mode
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchPayment($account, $params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        $query = new Query();
        $query->select(['th.id', 'th.date_created', 'th.description', 'th.trans_type', 'cl.class_code', 'cl.class_description',
            'pr.reciept_number', 'pr.channel_trans_id', 'th.payment_id', 'th.reversed', 'pc.channel_code',
            'pc.channel_name', 'pr.channel_memo', 'th.amount', 'th.balance_after', 'pc.payment_channel_logo',
            'sinfo.student_code as payment_code', 'sinfo.school_student_registration_number as registration_number',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            '(case when asr.id is null then true else asr.settled end) as processed'])
            ->from('school_account_transaction_history th')
            ->innerJoin('payments_received pr', 'pr.id=th.payment_id')
            ->innerJoin('core_student sinfo', 'sinfo.id=pr.student_id')
            ->innerJoin('core_school_class cl', 'cl.id=sinfo.class_id')
            ->leftJoin('auto_settlement_requests asr', 'pr.id=asr.payment_id') //LEft join mm payments
            ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel');
        if (\app\components\ToWords::isSchoolUser()) {
            $query = $query->where(['th.account_id' => $account]);
        }
        if (Yii::$app->user->can('schoolpay_admin')) {
            $query = $query->andFilterWhere(['th.account_id' => $this->account_id]);
        }
        $query->andFilterWhere(['pr.payment_channel' => $this->payment_id]);
        $query->andFilterWhere(['pr.channel_trans_id' => $this->transaction_id]);
        //Disable reversed and reversal transactions from the search
        $query->andFilterWhere(['pr.reversal' => false]);
        $query->andFilterWhere(['pr.reversed' => false]);
        if ($this->date_from) {
            $query->andWhere("pr.date_created::date>='$this->date_from'");
            if ($date_to) $query->andWhere("pr.date_created::date<='$date_to'");
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'description', 'trans_type', 'reciept_number', 'channel_trans_id', 'channel_code', 'channel_memo', 'amount', 'balance_after'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('th.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $this->setSession('Payments history', $query, $this->getCols(), 'Payments_histories');
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page]; //Use slave db connection in reporting mode
    }


    public function searchSupplementaryPayment($account, $params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        $query = new Query();
        $query->select(['pr.id', 'pr.date_created',  'cl.class_code', 'cl.class_description','ifd.description',
            'pr.reciept_number', 'pr.channel_trans_id', 'pr.id',  'pc.channel_code',
            'pc.channel_name', 'pr.channel_memo', 'pr.amount',  'pc.payment_channel_logo',
            'sinfo.student_code as payment_code', 'sinfo.school_student_registration_number as registration_number',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
            '(case when asr.id is null then true else asr.settled end) as processed'])
            ->from('supplementary_fee_payments_received pr')
            ->innerJoin('core_student sinfo', 'sinfo.id=pr.student_id')
            ->innerJoin('institution_supplementary_fees ifd', 'ifd.id=pr.fee_id')
            ->innerJoin('core_school_class cl', 'cl.id=sinfo.class_id')
            ->leftJoin('auto_settlement_requests asr', 'pr.id=asr.payment_id') //LEft join mm payments
            ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel');
        if (\app\components\ToWords::isSchoolUser()) {
            $query = $query->where(['pr.school_id' => $account]);
        }
        if (Yii::$app->user->can('schoolpay_admin')) {
            $query = $query->andFilterWhere(['pr.school_id' => $this->account_id]);
        }
        $query->andFilterWhere(['pr.payment_channel' => $this->payment_id]);
        $query->andFilterWhere(['pr.channel_trans_id' => $this->transaction_id]);
        //Disable reversed and reversal transactions from the search
        $query->andFilterWhere(['pr.reversal' => false]);
        $query->andFilterWhere(['pr.reversed' => false]);
        if ($this->date_from) {
            $query->andWhere("pr.date_created::date>='$this->date_from'");
            if ($date_to) $query->andWhere("pr.date_created::date<='$date_to'");
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'description', 'trans_type', 'reciept_number', 'channel_trans_id', 'channel_code', 'channel_memo', 'amount', 'balance_after'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pr.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $this->setSession('Payments history', $query, $this->getCols(), 'Payments_histories');
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page]; //Use slave db connection in reporting mode
    }

    protected function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;


        $_SESSION['findData']['exportFile'] = '@app/views/common/exportPdfExcel';

        if ($_SESSION['PENALTY_MODE'] == 'penalty') {
            $_SESSION['findData']['exportFile'] = '@app/modules/schoolcore/views/school-information/exportPenaltyPdfExcel';

        }
        if ($_SESSION['PENALTY_MODE'] == 'history_sup') {
            $_SESSION['findData']['exportFile'] = '@app/modules/schoolcore/views/school-information/exportSupplementaryPdfExcel';

        }
    }

    public function getCols()
    {
        return [
            'date_created' => ['type' => 'date', 'label' => 'Date Created'],
            'description' => ['label' => 'Description'],
            'payment_code' => ['label' => 'Payment Code'],
            'registration_number' => ['label' => 'Registration Number'],
            'student_name' => ['label' => 'Student Name'],
            'trans_type' => ['label' => 'Trans Type'],
            'channel_code' => ['label' => 'Channel code'],
            'channel_memo' => ['label' => 'Channel memo'],
            'amount' => ['type' => 'number', 'label' => 'Amount'],
        ];
    }

    public function searchPenaltyPayment($account, $params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }

//        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_to = $this->date_to ? $this->date_to : $today;
        $query = new Query();
        $query->select(['ifdspth.id', 'ifdspth.date_created', 'ifd.description', 'cl.class_code', 'cl.class_description',
            'ifdspth.penalty_amount',
            'sinfo.student_code as payment_code', 'sinfo.school_student_registration_number as registration_number',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name",
        ])
            ->from('institution_fees_due_student_payment_transaction_history ifdspth')
            ->innerJoin('core_student sinfo', 'sinfo.id=ifdspth.student_id')
            ->innerJoin('core_school_class cl', 'cl.id=sinfo.class_id')
            ->leftJoin('institution_fees_due ifd', 'ifd.id=ifdspth.fee_id'); //LEft join mm payments
        if (\app\components\ToWords::isSchoolUser()) {
            $query = $query->where(['ifd.school_id' => Yii::$app->user->identity->school_id]);
        }
        if (Yii::$app->user->can('schoolpay_admin')) {
            $query = $query->andFilterWhere(['ifd.school_id' => $this->account_id]);
        }
        $query->andFilterWhere(['>', 'ifdspth.penalty_amount', 0]);

        if($this->student_campus){
            $query->andFilterWhere(['sinfo.campus_id' => $this->student_campus]);
        }

        if ($this->date_from) {
            $query->andWhere("ifdspth.date_created::date>='$this->date_from'");
            if ($date_to) $query->andWhere("ifdspth.date_created::date<='$date_to'");
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'date_created', 'penalty_amount', 'description',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ifdspth.date_created DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        unset($_SESSION['findData']);
        // $_SESSION['findData'] = $query;
        $this->setSession('Penalty history', $query, $this->getCols(), 'Penalty_histories');
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page]; //Use slave db connection in reporting mode
    }

    public function schoolPerfomance($account, $params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        $from_date = empty($this->date_from) ? date('Y-m-d', strtotime("-30 days")) : date('Y-m-d', strtotime($this->date_from . ' -30 days'));

        Yii::trace($params);


        $date_to = $this->date_to ? $this->date_to : $today;

        $trQuery = new Query();
        $trQuery->select(['abr.bank_region_name', 'ab.branch_name', 'rr.description', 'count(cs.branch_id) AS schs'])
            ->from('core_school cs')
            ->innerJoin('awash_branches ab', 'ab.id=cs.branch_id')
            ->innerJoin('ref_region rr', 'rr.id=cs.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=cs.branch_region');
        //  ->filterWhere(['between','cs.date_created', $this->date_from, $date_to]);
        //   ->filterWhere(['between', 'asr.date_created', $this->start_date, $end_date])

        // ->Where(['cs.end_date' => $date_to]);

        Yii::trace($this->date_from);
        Yii::trace($date_to);
        if (isset($from_date)) {

            $trQuery->andFilterWhere(['between', 'cs.date_created', $from_date, $date_to]);

        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $trQuery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $trQuery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $trQuery->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $trQuery->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }

        $trQuery->groupBy(['abr.bank_region_name', 'ab.branch_name', 'rr.description']);

        $pages = new Pagination(['totalCount' => $trQuery->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'abr.bank_region_name', 'ab.branch_name', 'rr.description',
            ],
        ]);
        ($sort->orders) ? $trQuery->orderBy($sort->orders) : $trQuery->orderBy('count(cs.branch_id) DESC');
        $trQuery->offset($pages->offset)->limit($pages->limit);
        unset($_SESSION['findData']);
        // $_SESSION['findData'] = $query;
        $this->setSession('Branch history', $trQuery, $this->getColz(), 'school_histories');
        return ['activity' => $trQuery->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page]; //Use slave db connection in reporting mode


        //Use slave db connection in reporting mode
    }

    public function getColz()
    {
        return [
            'bank_region_name' => ['label' => 'Branch Region'],
            'branch_name' => ['label' => 'Branch '],
            'description' => ['label' => 'Country Region'],
            'schs' => ['label' => 'Number of Schools'],

        ];
    }

}

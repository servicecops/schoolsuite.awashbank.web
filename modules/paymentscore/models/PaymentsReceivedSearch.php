<?php

namespace app\modules\paymentscore\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;


/**
 * PaymentsReceivedSearch represents the model behind the search form about `app\models\PaymentsReceived`.
 */
class PaymentsReceivedSearch extends PaymentsReceived
{
    public $date_from, $date_to, $transaction_id;

    public $modelSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount'], 'integer'],
            [['reciept_number', 'date_created', 'channel_trans_id', 'channel_memo', 'student_id', 'school_id', 'payment_channel', 'modelSearch', 'channel_process_date', 'channel_payment_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentsReceived::find();
        $this->load($params);

        Yii::trace($params);
        $searchForm = 'student_id';
        $searchModel = new PaymentsReceivedSearch();
//        // add conditions that should always apply here

        $query = new Query();

        $query->select(['pr.id','pr.channel_process_date','concat(first_name,\' \',  last_name) as full_name','sc.school_name','pr.school_id', 'pr.student_id', 'st.first_name','st.last_name','st.middle_name', 'pr.channel_trans_id','pr.reciept_number','pr.channel_payment_type','pr.channel_depositor_name','pr.amount','pc.channel_name'])
            ->from('payments_received pr')
            ->innerJoin('core_student st', 'st.id=pr.student_id')
            ->innerJoin('core_school sc', 'sc.id=pr.school_id')
            ->innerJoin('payment_channels pc', 'pc.id=pr.payment_channel')
            ->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'payment_channel' => $this->payment_channel,
            'amount' => $this->amount,
            'channel_process_date' => $this->channel_process_date,
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return  $dataProvider;
    }

    public function searchStudent($params)
    {
        $query = (new Query())
            ->select(['cs.id','cs.first_name','cs.gender','cs.middle_name','csc.school_code','ct.class_code', 'cs.last_name','ct.class_description', 'cs.school_student_registration_number','csc.school_name','cs.school_id', 'gl.outstanding_balance','csc.default_part_payment_behaviour'])
            ->from('core_student cs')
            ->innerJoin('core_school_class ct', 'cs.class_id= ct.id')
            ->innerJoin('core_school csc', 'cs.school_id= csc.id')
            ->innerJoin('student_account_gl gl', 'cs.student_account_id= gl.id')
            ->where(['cs.student_code'=>$params])
          //  ->orwhere(['cs.school_student_registration_number'=>$params])
            ->all();

        return  $query;
    }

    public function searchOnlineStudentApplicant($params)
    {
        $query = (new Query())
            ->select(['cs.id','cs.first_name','cs.gender','cs.middle_name','csc.school_code','ct.class_code', 'cs.last_name','ct.class_description', 'cs.school_student_registration_number','csc.school_name'])
            ->from('std_online_reg cs')
            ->innerJoin('core_school_class ct', 'cs.class_id= ct.id')
            ->innerJoin('core_school csc', 'cs.school_id= csc.id')
            ->innerJoin('online_student_account_gl gl', 'cs.student_account_id= gl.id')
            ->where(['cs.payment_ref_code'=>$params])
            ->all();
        return  $query;
    }

    public function postPayment($params)
    {
        Yii::trace($params);

        try {
            $assocId = $params;

            $result = \Yii::$app->db->createCommand("CALL process_student_transactions(:associd, :jsonText)")
                ->bindValue(':associd', $assocId)
                ->bindValue(':jsonText', $assocId)
                ->execute();
            Yii::trace($result);
        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return $error;
        }
    }


}

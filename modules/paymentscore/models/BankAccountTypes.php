<?php

namespace app\modules\paymentscore\models;

use Yii;

/**
 * This is the model class for table "bank_account_types".
 *
 * @property integer $id
 * @property string $account_type
 */
class BankAccountTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_account_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_type'], 'required'],
            [['account_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_type' => 'Account Type',
        ];
    }
    public function formFields()
    {
        return [
            [
                'heading'=>'New Bank Account Types',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'id' => ['type'=>'number', 'label'=>'Id'],
                    'account_type' => ['label'=>'Account Type'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'account_type'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Bank Account Types" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

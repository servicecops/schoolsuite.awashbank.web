<?php

namespace app\modules\paymentscore\models;

use app\models\BaseModel;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "core_student".
 *
 * @property string $id
 * @property string $code
 * @property string $name
 * @property string $password

 */
class ExternalPaymentSources extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'external_payment_sources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['code', 'name','password'], 'string', 'max' => 255],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'password' => 'Password',


        ];
    }

}

<?php

namespace app\modules\paymentscore\models;
use Yii;

/**
 * This is the model class for table "school_account_transaction_history".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $amount
 * @property string $posting_user
 * @property string $description
 * @property integer $payment_id
 * @property string $trans_type
 * @property integer $account_id
 * @property boolean $reversal_flag
 * @property boolean $reversed
 * @property string $date_reversed
 * @property integer $balance_after
 *
 * @property PaymentsReceived $payment
 * @property SchoolAccountGl $account
 */
class SchoolAccountTransactionHistory extends \yii\db\ActiveRecord
{
    public $date_from, $date_to;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_account_transaction_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_reversed'], 'safe'],
            [['amount'], 'required'],
            [['amount', 'payment_id', 'account_id', 'balance_after'], 'integer'],
            [['posting_user', 'description', 'trans_type'], 'string'],
            [['reversal_flag', 'reversed'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'amount' => 'Amount',
            'posting_user' => 'Posting User',
            'description' => 'Description',
            'payment_id' => 'Channel',
            'trans_type' => 'Trans Type',
            'account_id' => 'Account',
            'reversal_flag' => 'Reversal Flag',
            'reversed' => 'Reversed',
            'date_reversed' => 'Date Reversed',
            'balance_after' => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentsReceived::className(), ['id' => 'payment_id']);
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(SchoolAccountGl::className(), ['id' => 'account_id']);
    }



}

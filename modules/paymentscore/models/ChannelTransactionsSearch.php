<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ChannelTransactionsSearch represents the model behind the search form about `app\models\ChannelTransactions`.
 */
class ChannelTransactionsSearch extends ChannelTransactions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'account_id', 'balance_after', 'balance_before'], 'integer'],
            [['date_created', 'posting_user', 'description', 'trans_type', 'date_reversed', 'payment_id', 'channel_transaction_processed_date'], 'safe'],
            [['reversal_flag', 'reversed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($acc, $params)
    {
        $query = ChannelTransactions::find()->where(['account_id'=>$acc]);

        $today =date('Y-m-d');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->leftJoin('payments_received pr', 'pr.id=payment_id')
            ->leftJoin('core_school si', 'si.id=pr.school_id')
            ->where(['account_id'=>$acc]);

         $query->andFilterWhere(['between', 'payment_channel_account_transaction_history.date_created', $this->date_created, $today]);

        $query->andFilterWhere([
            'amount' => $this->amount,
            'account_id' => $this->account_id,
            'reversed' => $this->reversed,
            'date_reversed' => $this->date_reversed,
            'balance_after' => $this->balance_after,
            'balance_before' => $this->balance_before,
            'channel_transaction_processed_date' => $this->channel_transaction_processed_date,
        ]);

        $query->andFilterWhere(['ilike', 'posting_user', $this->posting_user])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'si.school_name', $this->payment_id])
            ->andFilterWhere(['ilike', 'trans_type', $this->trans_type])
            ->orderBy('payment_channel_account_transaction_history.date_created DESC');

        return $dataProvider;
    }
}

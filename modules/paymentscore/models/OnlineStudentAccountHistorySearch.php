<?php

namespace app\modules\paymentscore\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\paymentscore\models\StudentAccountHistory;

if (!Yii::$app->session->isActive){
          session_start();
      }

/**
 * StudentAccountHistorySearch represents the model behind the search form about `app\models\StudentAccountHistory`.
 */
class OnlineStudentAccountHistorySearch extends OnlineStudentAccountHistory
{
    public $only_received;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount', 'fee_association_id', 'payment_id', 'account_id', ], 'integer'],
            [['balance_after', 'outstanding_balance_after'], 'string'],
            [['date_created', 'posting_user', 'description', 'trans_type', 'date_reversed'], 'safe'],
            [['reversal_flag', 'reversed', 'only_received'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($account, $params)
    {
        $query = OnlineStudentAccountHistory::find()->where(['account_id'=>$account,
//            'reversal_flag'=>false,
//            'student_account_transaction_history.reversed'=>false
        ]);
        $today =date('Y-m-d');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['between', 'student_account_transaction_history.date_created', $this->date_created, $today]);
        $query->joinWith('payment');

        $query->andFilterWhere([
            'amount' => $this->amount,
            'payments_received.payment_channel' => $this->payment_id,
            'account_id' => $this->account_id,
//            'reversal_flag' => $this->reversal_flag,
//            'reversed' => $this->reversed,
            'date_reversed' => $this->date_reversed,
            'balance_after' => $this->balance_after,
        ]);
        if($this->only_received){
            $query->andWhere('payment_id is not null');
        }

        $query->andFilterWhere(['ilike', 'posting_user', $this->posting_user])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'trans_type', $this->trans_type]);

        $query->orderBy('student_account_transaction_history.date_created DESC');

        unset($_SESSION['exportData']);
        $_SESSION['exportData'] = $dataProvider;

        Yii::trace($dataProvider);

        return $dataProvider;
    }

    public static function getExportData()
    {
    $data = [
            'data'=>$_SESSION['exportData'],
            'fileName'=>'transaction_histories',
            'title'=>'Student Transaction History',
            'exportFile'=>'/core-student/exportTrasaction',
        ];

    return $data;
    }
}

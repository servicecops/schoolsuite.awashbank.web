<?php
/**
 * Created by IntelliJ IDEA.
 * User: ibrahim
 * Date: 9/28/17
 * Time: 8:30 PM
 */

foreach ($res as $k => $v) : ?>
    <?PHP
    $amount = '';
    $debit = false;
    if ($v->DB_CR == 'CR') {
        $amount = $v->TRNAMTCRFRMT;
    } else {
        $amount = $v->TRNAMTDRFRMT;
        $debit = true;
    }
    $amount = $v->DB_CR == 'CR' ? $v->TRNAMTCRFRMT : $v->TRNAMTDBFRMT;

    $negativeBalance = $v->RUNNINGBALANCE < 0;
    ?>
    <tr>
        <td><?= $v->TRNDATEOFTRAN ? $v->TRNDATEOFTRAN : "" ?></td>
        <td><?= $v->NARRDTL ?></td>
        <td><?= $v->TRNBRNCODE . $v->TRNBATNUMBER . $v->TRNBATSLNUM ?></td>
        <td><?= $debit ? '<span style="color:red;"> - ' . $amount . '</span>' : $amount ?></td>
        <td><?= (!$negativeBalance) ? $v->RUNNINGBALANCEFRMT : '<span style="color:red;"> - ' . $v->RUNNINGBALANCEFRMT . '</span>' ?></td>
    </tr>
<?php endforeach; ?>


<?php
/**
 * Created by IntelliJ IDEA.
 * User: ibrahim
 * Date: 9/28/17
 * Time: 8:31 PM
 */

//Timezone to be used for centenary date timestamps
$tz = 'Africa/Kampala';
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
echo $dt->format('Y-m-d');

?>

<?php if (isset($type) && $type=='json') :
    foreach ($res as $k => $v) : ?>
        <?php
        $dt->setTimestamp($v->transactionDate/1000);
        ?>
    <tr>
        <td><?= $dt->format('d/m/Y') ?></td>
        <td><?= $v->transactionDescription ?></td>
        <td><?= $v->transactionReference ?></td>
        <td><?= ($v->transactionAmount>0) ? number_format($v->transactionAmount) : '<span style="color:red;"> ' . number_format($v->transactionAmount) . '</span>' ?></td>
        <td><?= number_format($v->balance) ?></td>
    </tr>
    <?php endforeach;
 else :
    foreach ($res as $k => $v) : ?>
        <tr>
            <td><?= $v->EffectiveDt ?></td>
            <td><?= $v->description ?></td>
            <td><?= $v->BankRefernce ?></td>
            <td><?= ($v->Credit) ? number_format($v->Credit) : '<span style="color:red;"> - ' . number_format($v->Debit) . '</span>' ?></td>
            <td><?= number_format($v->Closing) ?></td>
        </tr>
    <?php endforeach;
endif; ?>



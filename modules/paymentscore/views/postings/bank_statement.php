<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$_SESSION['bank_statement_query']= $model;
$this->title = 'Bank Statement';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index hd-title" data-title="Bank Statement">
<div class="row">


      <div class="col-md-2 "><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> Statement</span></div>
      
      <div class="col-md-7 " ><?php echo $this->render('_account_search', ['model' => $model]); ?></div>
      <div class="col-md-2 ">
        <div >
            <div style="float:left; margin-right:3px">
            <?= Html::a('<i class="fa fa-file-excel-o"></i> &nbsp;Excel',['/paymentscore/postings/bank-statement-pdfexl','export'=>'excel'],array('title'=>'Export to Excel','target'=>'_blank','class'=>'btn btn-info btn-sm'));?>
            </div>
            <div >
            <?= Html::a('<i class="fa fa-file-pdf-o"></i> PDF', ['/paymentscore/postings/bank-statement-pdfexl','export'=>'pdf'], ['title'=>'Export to PDF','target'=>'_blank','class'=>'btn btn-danger btn-sm'] ); ?>
            </div>
        </div>
    </div>


    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
        <table class="table">
        <thead>
        <tr>
        	<th>Effective Date</th>
        	<th>Description</th>
        	<th>Bank Reference</th>
        	<th>Amount</th>
        	<th>Closing</th>
        </tr>
        </thead>
        <tbody>
            <?php
//            Yii::trace('Res ' . json_encode($res));
            if($res) :
                $rows = ['res' => $res, 'type'=>$type];
                if($bankCode =='CENTENARY') :
                   echo $this->render('_cente_statement', $rows);
                elseif($bankCode =='DFCU') :
                   echo $this->render('_dfcu_statement', $rows);
                elseif($bankCode =='STANBIC') :
                   echo $this->render('_stanbic_statement', ['rows'=>$rows, 'res'=>$res]);
                elseif($bankCode =='HFB') :
                   echo $this->render('_hfb_statement', ['rows'=>$rows, 'res'=>$res]);
                else :
                   echo '<tr><td colspan="7" id="no_bank_statement"><span style="color:red;">'. $error. '</span></td></tr>';
                endif;
            elseif($error) :?>
            <tr><td colspan="7" id="no_bank_statement"><span style="color:red;"><?= $error ?></span></td></tr>
            <?php else: ?>
            <tr><td colspan="7" id="no_bank_statement">No transactions found</td></tr>
        <?php endif; ?>
        </tbody>
        </table>
        </div>
</div>




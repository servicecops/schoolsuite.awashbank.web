<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-form hd-title" data-title="School Payout">
    <?php $form = ActiveForm::begin(); ?>
    <div class="col-xs-12 col-lg-12 no-padding">
    	
    	<div class="col-sm-6">
    	<?= $form->field($model, 'amount', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Amount'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
        <?= $form->field($model, 'schoolId')->dropDownList(
        ArrayHelper::map(\app\modules\schoolcore\models\CoreSchool::find()->all(), 'id', 'school_name'), ['prompt'=>'Select School']
        )->label('') ?>
        </div>
   </div>
   
   <div class="col-xs-12 col-lg-12 no-padding">
        
        <div class="col-sm-6">
            <?= $form->field($model, 'instructionDate', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Instruction Date'] ])->textInput()->label('') ?>
        </div>

        <div class="col-sm-6">
        <?= $form->field($model, 'payoutBankDetails', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'PayoutBank Details'] ])->textInput()->label('') ?>
        </div>

   </div>

    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
    <div class="col-xs-6">
        <?= Html::submitButton('Send', ['class' => 'btn btn-block btn-primary']) ?>
    </div>
    <div class="col-xs-6">
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
/**
 * Created by IntelliJ IDEA.
 * User: ibrahim
 * Date: 9/28/17
 * Time: 8:30 PM
 */
foreach($res as $k=>$v) : ?>
    <?php if(!($v->DebitAmount == 0 && $v->CreditAmount==0)) : ?>
    <tr>
        <td><?= $v->Trandate ? date('d/m/y h:i', strtotime($v->Trandate)) : "" ?></td>
        <td><?= $v->Particulars  ?></td>
        <td><?= $v->TranId ?></td>
        <td><?= ($v->DebitAmount > 0) ?  '<span style="color:red;"> - '.number_format($v->DebitAmount).'</span>' : number_format($v->CreditAmount) ?></td>
        <td><?= ($v->Bal_Indicator == 'Cr') ? $v->Balance : '<span style="color:red;"> - '.$v->Balance.'</span>' ?></td>
    </tr>
<?php endif; endforeach; ?>
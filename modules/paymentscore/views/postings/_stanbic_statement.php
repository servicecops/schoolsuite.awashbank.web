<?php
/**
 * Created by IntelliJ IDEA.
 * User: ibrahim
 * Date: 9/28/17
 * Time: 8:30 PM
 */
foreach($res as $k=>$v) : ?>
    <?php if($v->amount > 0) : ?>
    <tr>
        <td><?= $v->date ?></td>
        <td><?= $v->narration  ?></td>
        <td><?= $v->reference  ?></td>
        <td><?= ($v->part_tran_type == 'D') ?  '<span style="color:red;"> - '.number_format($v->amount).'</span>' : number_format($v->amount) ?></td>
        <td><?= number_format($v->closingBal)  ?></td>
    </tr>
<?php endif; endforeach; ?>
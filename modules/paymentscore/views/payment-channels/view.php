<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\paymentscore\models\PaymentChannels */

$this->title = $model->channel_name;
$this->params['breadcrumbs'][] = ['label' => 'Payment Channels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-channels-view hd-title" data-title="<?= $model->channel_name ?>">
<div class="row">
<div class="col-md-12">
    <div class="letter no-padding">
      <div class="row" style="padding-top:100px;">
                <?php if($model->payment_channel_logo): ?>
               <img class="sch-icon" src="data:image/jpeg;base64,<?= $model->logo->image_base64 ?>" height="100" width="100"  alt=""/>
                <?php endif; ?><br>
           <span class="col-md-12 col-sm-12 col-md-12 sch-title"> <?= Html::encode($this->title) ?></span>

           <div class="col-md-12">
            <div class="top-buttons pull-right">
                <?= Html::a('<i class="fa fa-edit"></i> Edit Logo', ['logo', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-edit"></i> Edit Details', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
            </div>
    </div>
    <hr class="l_header">

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table  detail-view'],
        'attributes' => [
            //'id',
            'channel_code',
            //'password',
            'channel_name',
            'channel_email:email',
            'channel_address:ntext',
            'payment_instructions',
            'active:boolean',
        ],
    ]) ?>
    </div>
</div>
</div>

<div class="row">
<div class="col-md-12">
   <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'layout' =>"{items}\n{pager}",
        'columns' => [
            [
              'attribute'=>'date_created',
              'value'=>function($model){
                return date('d/m/y H:i', strtotime($model->date_created));
              },
              'filter'=>DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date_created',
                        'dateFormat' => 'yyyy-MM-dd',
                        'clientOptions' =>[
                              'changeMonth'=> true,
                              'changeYear'=> true,
                              'yearRange'=>'1900:'.(date('Y')+1),
                              'autoSize'=>true,
                              ],
                      ]),

            ],
            [
              'attribute'=>'description',
              'value'=>'description',
              'filter'=>false,
            ],
            [
              'attribute'=>'trans_type',
              'value'=>'trans_type',
              'filter'=>false
            ],
            [
              'attribute'=>'payment_id',
              //'options'=>['class'=>'col-md-1'],
              'value'=>function($model){
                if($model->payment_id){
                  return $model->payment->school->school_name;
                }
              },
            ],
            // [
            //   'attribute'=>'Channel Memo',
            //   'value'=>function($model){
            //   if($model->payment_id)
            //     return $model->payment->channel_memo;
            //   }
            // ],
            [
              'attribute'=>'amount',
              'value'=>function($model){
                if($model->amount < 0)
                    return '<span style="color:red">'.number_format($model->amount, 2).'</span>';
                else
                    return number_format($model->amount, 2);
              },
              'format'=>'html',
              'filter'=>false,
            ],
            [
              'attribute'=>'balance_before',
              'value'=>function($model){
                if($model->balance_before < 0)
                    return '<span style="color:red">'.number_format($model->balance_before, 2).'</span>';
                else
                    return number_format($model->balance_before, 2);
              },
              'format'=>'html',
              'filter'=>false,
            ],
            [
              'attribute'=>'balance_after',
              'value'=>function($model){
                if($model->balance_after < 0)
                    return '<span style="color:red">'.number_format($model->balance_after, 2).'</span>';
                else
                    return number_format($model->balance_after, 2);
              },
              'format'=>'html',
              'filter'=>false,
            ],

        ],
    ]); ?>

</div>

</div>
</div>

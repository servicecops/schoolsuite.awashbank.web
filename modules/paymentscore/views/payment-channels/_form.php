<?php

use app\modules\paymentscore\models\PcProcessingStrategy;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentChannels */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model_titles">
    <div class="col-md-12"><h3 style="color:white"><i class="fa fa-check-square-o"></i>&nbsp;Add Channel Information
        </h3></div>
</div>
<?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess', 'style' => 'width:100%']]); ?>

<div style="padding: 10px;width:100%"></div>
<div class="row">

    <br>
    <div class="col-md-6 ">
        <?= $form->field($model, 'channel_code', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Channel Code']])->textInput()->label('Channel code') ?>
    </div>
    <div class="col-md-6 ">
        <?= $form->field($model, 'channel_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Channel Name']])->textInput()->label('Channel Name') ?>
    </div>
</div>

<div class="row">
    <br>
    <div class="col-md-6">
        <?= $form->field($model, 'channel_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Channel Email']])->textInput()->label('Channel Email') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'channel_address', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Channel Address']])->textInput()->label('Address') ?>
    </div>
</div>

<div class="row">
    <br>
    <div class="col-md-6">
        <?= $form->field($model, 'cert_data', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Certificate Data']])->textArea()->label('Certificate') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'payment_instructions', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Payment Instructions']])->textArea()->label('Payment Instructions') ?>
    </div>
</div>

<div class="row" style="display: none;">
    <br>
    <div class="col-md-6">
        <?= $form->field($model, 'payment_channel_processing_strategy', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Processing Strategy']])->dropDownList(
            ArrayHelper::map(PcProcessingStrategy::find()->all(), 'id', 'codeDesc'), ['prompt' => 'Select Processing Strategy']
        )->label('Processing Strategy') ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'channel_type')->dropDownList(['Bank' => 'Bank', 'Mobile' => 'Mobile'], ['id' => 'user_level_id', 'prompt' => 'Type of User']
        )->label('Type of Channel') ?>
    </div>
</div>

<div class="row">

    <div class="col-md-6 checkbox checkbox-info">
        <br>
        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="PaymentChannels[active]" value="0">
            <input type="checkbox" id="paymentchannels-active" name="PaymentChannels[active]"
                   value="1" <?= $model->active ? 'checked' : '' ?>>
            <label for="paymentchannels-active">Active</label>
        </div>
        <div class="checkbox checkbox-inline checkbox-info" style="display: none;">
            <input type="hidden" name="PaymentChannels[log_for_auto_settlement]" value="0">
            <input type="checkbox" id="paymentchannels-log_for_auto_settlement"
                   name="PaymentChannels[log_for_auto_settlement]"
                   value="1" <?= $model->log_for_auto_settlement ? 'checked' : '' ?>>
            <label for="paymentchannels-log_for_auto_settlement">Log for Auto Settlement</label>
        </div>
    </div>
</div>


<div class="form-group col-xs-12 col-md-6 col-lg-6 no-padding">
    <br>
    <div class="col-xs-6 no-padding-right">
        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
    </div>
    <div class="col-xs-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#channels").addClass('active');
    $("#channels_create").addClass('active');
  });
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentChannels */

$this->title = 'Edit Channel: ' . ' ' . $model->channel_name;
$this->params['breadcrumbs'][] = ['label' => 'Payment Channels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row hd-title" data-title="Edit Payment Channel">
  <div class="col-md-12" style="padding:0px 30px;">
     <div class="row letter">
  <div class="col-xs-12">
    <h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3>
      <hr class="l_header">
      <br>
   </div>
 
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>

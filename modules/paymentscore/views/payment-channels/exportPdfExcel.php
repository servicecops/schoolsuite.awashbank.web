<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="payment_channels">
    <?php  
	$model->sort = false; 

	if($type == 'Excel') {
		echo "<table><tr> <th colspan='7'><h3>Payment Channels</h3> </th> </tr> </table>";
	}
    ?>
    <?= GridView::widget([
        'dataProvider' => $model,
        //'filterModel' => $searchModel,
        'layout'=>"{items}",
        'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'channel_name',
            'channel_email:email',
            'channel_address:ntext',
            // 'active:boolean',
            // 'cert_data:ntext',
            // 'incorrect_auth_count',
            // 'date_created',
            // 'date_modified',
        ],
    ]); ?>

</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentChannelsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Channels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-channels-index hd-title" data-title="Payment Channels">
<div class="row">

    <div class="col-sm-12" style="padding-top: 10px;">
        <div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'layout'=>"{items}\n{pager}",
        'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute'=>'channel_name',
                'value'=>function($model){
                    $img = $model->payment_channel_logo ? '<img src="data:image/jpeg;base64,'.$model->logo->image_base64 .'" width="30px" alt="Logo" /> &nbsp;' : '<span style="padding-left:35px"></span>';
                    return $img."<a class='aclink' href='".Url::to(['/paymentscore/payment-channels/view', 'id'=>$model->id])."'>".$model->channel_name."</a>";
                    },
                'format'=>'raw',
            ],
            'channel_email:email',
            'channel_address:ntext',
            // 'active:boolean',
            // 'cert_data:ntext',
            // 'incorrect_auth_count',
            // 'date_created',
            // 'date_modified',

            [
            'class' => 'yii\grid\ActionColumn',
            'visible'=>Yii::$app->user->identity->user_level=='super_admin',
            ],
        ],
    ]); ?>

        </div>
    </div>
</div>
</div>
<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#channels").addClass('active');
    $("#channels_index").addClass('active');_
  });
JS;
$this->registerJs($script);
?>

<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html; ?>
<?php $form = ActiveForm::begin([
    'action' => ['query-student'],
    'method' => 'post',
    'options' => ['class' => 'formprocess'],
])?>
<div class=" container mb-5">

    <div class="row class-associate">

        <div class="col-lg-5 col-sm-5 col-xs-12">
            <b>Enter Payment Code </b>:
            <div class="col-sm-6">
                <?= $form->field($model, 'student_code', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput()->label('') ?>
            </div>
        </div>
        <div class="form-group col-md-12 ">
            <div class="col-md-6 col-md-offset-3">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

<?php use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action' => 'post-payment',

]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'payment_channel', ['inputOptions' => ['class' => 'form-control ', 'placeholder' => 'Students Class']])->dropDownList(['' => 'Filter class'])->label('') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'json_text', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'First Name']])->textInput()->label('') ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

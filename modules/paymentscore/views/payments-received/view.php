<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsReceived */

$this->title = $model->student->fullname." Trans Receipt: ". $model->reciept_number;
$this->params['breadcrumbs'][] = ['label' => 'Payments Receiveds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-received-view hd-title" data-title="<?= $this->title ?>">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'reciept_number',
            'date_created',
            [
                'attribute'=>'student_id',
                'value'=>$model->student->fullname,
            ],
            [
                'attribute'=>'school_id',
                'value'=>$model->school->school_name,
            ],
            [
                'attribute'=>'payment_channel',
                'value'=>$model->paymentChannel->channel_name,
            ],
            'channel_trans_id',
            'channel_memo',
            'amount',
            'channel_process_date',
            'channel_payment_type',
        ],
    ]) ?>

</div>

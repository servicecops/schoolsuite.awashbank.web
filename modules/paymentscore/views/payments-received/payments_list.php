<?php


use app\modules\banks\models\BankDetails;
use app\modules\paymentscore\models\PaymentChannels;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\web\View;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mobile money payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index hd-title" data-title="Mobile money payments">
    <div class="row">
            <!--            <div class="col-md-2 content-title no-padding" style="padding-top:13px !important;">-->
            <!--                <i class="fa fa-th-list"></i> Mobile money payments-->
            <!--            </div>-->

                <?php $form = ActiveForm::begin([
                    'action' => ['/paymentscore/payments-received/list'],
                    'method' => 'get',
                    'options' => ['class' => 'formprocess']
                ]); ?>
                <ul class=" row menu-list no-padding" style="list-style-type:none">
                    <li class="col-xs-2 no-padding"><?= DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'start_date',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                            'clientOptions' => [
                                'class' => 'form-control',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                        ]); ?></li>
                    <li class="col-xs-2 no-padding-left"><?= DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'end_date',
                            'dateFormat' => 'yyyy-MM-dd',
                            'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                            'clientOptions' => [
                                'class' => 'form-control',
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                        ]); ?></li>

                    <li class="col-xs-2 no-padding"><?= $form->field($searchModel, 'payment_channel')->dropDownList(ArrayHelper::map(PaymentChannels::getMobileChannels(), 'id', 'channel_name'), ['prompt' => 'Channel', 'class' => 'form-control input-sm'])->label(false) ?></li>
                    <li class="col-xs-2 no-padding"><?= $form->field($searchModel, 'settlement_bank_id')->dropDownList(ArrayHelper::map(BankDetails::find()->all(), 'id', 'bank_name'), ['prompt' => 'Bank', 'class' => 'form-control input-sm'])->label(false) ?></li>
                    <li class="col-xs-1 no-padding"><?= $form->field($searchModel, 'settled')->dropDownList(['false' => 'Pending', 'true' => 'Settled'], ['prompt' => 'Status', 'class' => 'form-control input-sm'])->label(false) ?></li>
                    <?php
                    $url = Url::to(['/school-information/schools']);
                    $selectedSchool = empty($searchModel->school_id) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($searchModel->school_id)->school_name;
                    echo '<li class="col-xs-2 no-padding">';
                    echo $form->field($searchModel, 'school_id')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
                        'size'=>'sm',
                        'theme'=>Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => 'Search School',
                            'id'=>'student_selected_school_id',
                            'class'=>'',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],

                        ],
                    ])->label(false);
                    echo '</li>';

                    ?>
                    <li class="col-xs-1 no-padding"><?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></li>
                </ul>
                <?php ActiveForm::end(); ?>
            </div>

    <div class="col-md-4">

        <ul class="row menu-list no-padding" style="list-style-type:none">
            <?php
            echo \yii\bootstrap4\Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf',['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], [
                'class'=>'btn btn-sm btn-danger',
                'target'=>'_blank',
                'data-toggle'=>'tooltip',
                'title'=>'Will open the generated PDF file in a new window'
            ]);
            echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['/export-data/export-excel', 'model'=>get_class($searchModel)], [
                'class'=>'btn btn-sm btn-success',
                'target'=>'_blank'
            ]);
            ?>
        </ul>

    </div>
        </div>


        <div class="col-md-12" style="padding-top: 10px;">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-responsive">
                    <thead>
                    <tr>
                        <th></th>
                        <th class='clink'><?= $sort->link('date_created', ['label' => 'Date']) ?></th>
                        <th class='clink'><?= $sort->link('channel_name', ['label' => 'Source Channel']) ?></th>
                        <th class='clink'><?= $sort->link('bank_name', ['label' => 'Dest Bank']) ?></th>
                        <th class='clink'><?= $sort->link('school_code', ['label' => 'School Code']) ?></th>
                        <th class='clink'><?= $sort->link('school_name', ['label' => 'Dest School']) ?></th>
                        <th class='clink'><?= $sort->link('student_name', ['label' => 'Student']) ?></th>
                        <th class='clink'><?= $sort->link('student_code') ?></th>
                        <th class='clink'><?= $sort->link('settlement_reference', ['label' => 'Bank Ref']) ?></th>
                        <th class='clink'><?= $sort->link('amount') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0" <?php echo !$v['settled'] ? "class='pending-color'" : "" ?> >
                                <td><?= ($v['settled']) ? '<i class="fa fa-check-circle icon-success-color"></i>' : '<i class="fa fa-hourglass-half icon-pending-color"></i>' ?></td>
                                <td><?= ($v['date']) ? date('Y-m-d H:i:s', strtotime($v['date'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['chn']) ? '<img src="' . Url::to(['/import/import/image-link', 'id' => $v['payment_channel_logo']]) . '" width="25px" /> &nbsp;' . $v['chn'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['bank_name']) ? '<img src="' . Url::to(['/import/import/image-link', 'id' => $v['destination_bank_logo']]) . '" width="25px" /> &nbsp;' . $v['bank_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['school_code']) ? $v['school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['settlement_reference']) ? $v['settlement_reference'] : '<span class="not-set"> -- </span>' ?></td>
                                <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>' ?></td>
                                <td data-name="<?= $v['student_name'] ?>" data-toggle=popover
                                    data-href="<?= Url::to(['payments-received/this-settlement', 'id' => $v['id']]) ?>">
                                    <a class="fa fa-arrow-circle-o-right"></a></td>
                            </tr>
                        <?php endforeach;
                    else : ?>
                        <tr>
                            <td colspan="9">No Records found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <?= LinkPager::widget([
                'pagination' => $pages['pages'],
            ]); ?>

        </div>
    </div>






<div class="row">

      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Date</div>
        <div class="col-xs-7 profile-text"><?= date('Y-m-d H:i a', strtotime($data['date'])) ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Source Channel Code</div>
        <div class="col-xs-7 profile-text"><?= $data['source_payment_channel_code'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Source Channel</div>
        <div class="col-xs-7 profile-text"><?= $data['chn'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Destination Bank</div>
        <div class="col-xs-7 profile-text"><?= $data['bank_name'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Destination School</div>
        <div class="col-xs-7 profile-text"><?= $data['school_name'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Payer Phone</div>
        <div class="col-xs-7 profile-text"><?= $data['payer_phone'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Student Name</div>
        <div class="col-xs-7 profile-text"><?= $data['student_name'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Student code</div>
        <div class="col-xs-7 profile-text"><?= $data['student_code'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Receipt Number</div>
        <div class="col-xs-7 profile-text"><?= $data['recpt_no'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Settled</div>
        <div class="col-xs-7 profile-text"><?= $data['settled'] ? 'Settled' : 'pending' ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Settlement Reference</div>
        <div class="col-xs-7 profile-text"><?= $data['settlement_reference'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Last Fetch Date</div>
        <div class="col-xs-7 profile-text"><?= $data['last_fetch_date'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label">Last Process Status</div>
        <div class="col-xs-7 profile-text"><?= $data['last_process_status'] ?></div>
      </div>
      <div class="col-xs-12 no-padding">
        <div class="col-xs-5 profile-label"><b>Amount</b></div>
        <div class="col-xs-7 profile-text"><b><?= number_format($data['amount'], 2) ?></b></div>
      </div>
</div>
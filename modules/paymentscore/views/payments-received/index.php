<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\paymentscore\models\PaymentsReceivedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments Received';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
    <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?php echo $this->title ?></h3></div>
    <div class="col-xs-4 col-sm-4 col-xs-12 no-padding" style="padding-top: 20px !important;"><?php echo $this->render('_search', ['searchModel' => $searchModel]); ?></div>
    <div class="col-lg-4 col-sm-4 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-4 left-padding">
        </div>
        <div class="col-xs-4 left-padding">
            <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; P   DF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn btn-block btn-danger btn-sml', 'target'=>'_blank']) ?>
        </div>
        <div class="col-xs-4 left-padding">
            <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-block btn-primary btn-sml', 'target'=>'_blank']) ?>
        </div>
    </div>
</div>
<div class="payments-received-index hd-title" data-title="Payments Received">
<div class="row">

    <div class="col-sm-12" style="padding-top: 10px;">
    <div class="table-responsive">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}",
        //'filterModel' => $searchModel,
        'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            'channel_process_date',
            'reciept_number',

            [
                'attribute'=>'full_name'
            ],
            [
                'attribute'=>'school_name',
                'visible'=>Yii::$app->user->can('super_admin'),
            ],
            [
                'attribute'=>'channel_name',
            ],
            'channel_trans_id',
            [
                'label' => 'Amount',
                'attribute' => 'amount',
            ],

        ],
    ]); ?>

</div>
</div>

<?php
$script = <<< JS
$("document").ready(function(){
    $("#payments").removeClass('active').addClass('active');
    $("#payments_received").removeClass('active').addClass('active');_
  });
JS;
$this->registerJs($script);
?>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SchoolAccountTransactionHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'School Account Transaction Histories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-account-transaction-history-index hd-title" data-title="School Account Transaction Histories">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create School Account Transaction History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="box-body table table-responsive no-padding">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' =>['class' => 'table table-striped'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date_created',
            'amount',
            'posting_user',
            'description',
            // 'payment_id',
            // 'trans_type',
            // 'account_id',
            // 'reversal_flag:boolean',
            // 'reversed:boolean',
            // 'date_reversed',
            // 'balance_after',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>

<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="payment_aggregators_index">
    <?php  
	if($type == 'Excel') {
		echo "<table><tr> <th colspan='7'><h3> School Transaction History</h3> </th> </tr> </table>";
	}  ?>

  <table class="table table-striped">
        <thead>
        <tr>
            <th>Date Created</th>
            <th>Description</th>
            <th>Payment Code</th>
            <th>Registration Number</th>
            <th>Student Name</th>
            <th>Trans Type</th>
            <th>Channel Code</th>
            <th>Channel Memo</th>
            <th>Amount</th>
        </tr>
        </thead>
        <tbody>
            <?php 
            foreach($query as $k=>$v) : ?>
                <tr data-key="0">
                    <td><?= ($v['date_created']) ? date('d/m/y H:i', strtotime($v['date_created'])): '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['payment_code']) ? $v['payment_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['registration_number']) ? $v['registration_number'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['student_name']) ? $v['student_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['trans_type']) ? $v['trans_type'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_code']) ? $v['channel_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['channel_memo']) ? $v['channel_memo'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['amount']) ? number_format($v['amount'], 2) : '<span class="not-set">(not set) </span>'?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolAccountTransactionHistory */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'School Account Transaction Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-account-transaction-history-view hd-title" data-title="<?= $model->id ?>">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_created',
            'amount',
            'posting_user',
            'description',
            'payment_id',
            'trans_type',
            'account_id',
            'reversal_flag:boolean',
            'reversed:boolean',
            'date_reversed',
            'balance_after',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolAccountTransactionHistorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="school-account-transaction-history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_created') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'posting_user') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'payment_id') ?>

    <?php // echo $form->field($model, 'trans_type') ?>

    <?php // echo $form->field($model, 'account_id') ?>

    <?php // echo $form->field($model, 'reversal_flag')->checkbox() ?>

    <?php // echo $form->field($model, 'reversed')->checkbox() ?>

    <?php // echo $form->field($model, 'date_reversed') ?>

    <?php // echo $form->field($model, 'balance_after') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

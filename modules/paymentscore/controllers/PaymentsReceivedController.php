<?php


namespace app\modules\paymentscore\controllers;


use app\modules\paymentscore\models\AutoSettlementSearch;
use app\modules\paymentscore\models\ExternalPaymentSources;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\PaymentsReceivedSearch;
use app\modules\paymentscore\models\ThirdpartyPaymentsSearch;
use app\modules\schoolcore\models\CoreStudent;
use phpDocumentor\Reflection\Types\Integer;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

//use app\models\FeesDue;
//use app\models\FeeClass;
//use app\models\Classes;
//use app\models\FeesDueSearch;
//use app\modules\logs\models\Logs;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class PaymentsReceivedController extends Controller
{


    public function beforeAction($action)
    {


//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }

//something code right here if user valid

        return true;

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

        ];
    }


    /**
     * Lists all PaymentsReceived models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new PaymentsReceivedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);
    }

    /**
     * Displays a single PaymentsReceived model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return ((Yii::$app->request->isAjax)) ? $this->renderAjax('view', [
            'model' => $this->findModel($id)]) :
            $this->render('view', ['model' => $this->findModel($id)]);
    }

    /**
     * Finds the PaymentsReceived model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentsReceived the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentsReceived::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionList()
    {
        //Check permission first
        if (!Yii::$app->user->can('auto_settlement_trans')) {
            throw new ForbiddenHttpException('No permissions to view mm payments.');
        }
        $request = Yii::$app->request;
        $searchModel = new AutoSettlementSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

        return ($request->isAjax) ? $this->renderAjax('payments_list', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
            $this->render('payments_list', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
    }

    public function actionThirdpartyPayments()
    {
        //Check permission first
        if (!Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permissions to view third party payments.');
        }
        $request = Yii::$app->request;
        $searchModel = new ThirdpartyPaymentsSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = $data['pages'];

        $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']];
        return $request->isAjax ? $this->renderAjax('thirdparty_payments', $res) : $this->render('thirdparty_payments', $res);
    }

    public function actionThisSettlement($id)
    {
        $settlement = AutoSettlementSearch::findSettlement($id);
        return $this->renderPartial('settlement_details', ['data' => $settlement]);
    }



}

<?php

namespace app\modules\paymentscore\controllers;

use app\modules\paymentscore\models\ChannelTransactionsSearch;
use app\modules\paymentscore\models\ImageBank;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\PaymentChannelsSearch;
use Yii;
use yii\db\Query;
use yii\helpers\Json;

use yii\web\Controller;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\logs\models\Logs;


/**
 * PaymentChannelsController implements the CRUD actions for PaymentChannels model.
 */
class PaymentChannelsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentChannels models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if(Yii::$app->user->can('r_chn')){
          $searchModel = new PaymentChannelsSearch();
          $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

          return ($request->isAjax) ? $this->renderAjax('index', [
              'searchModel' => $searchModel,
              'dataProvider' => $dataProvider,
          ]) :
          $this->render('index', [
              'searchModel' => $searchModel,
              'dataProvider' => $dataProvider,
          ]);

        } else {
        throw new ForbiddenHttpException('No permissions to view Channels.');
      }
    }

    /**
     * Displays a single PaymentChannels model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      if(Yii::$app->user->can('r_chn')){
        $request = Yii::$app->request;
        $searchModel = new ChannelTransactionsSearch();
        $model = $this->findModel($id);
        $dataProvider = $searchModel->search($model->payment_channel_account_id, Yii::$app->request->queryParams);
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $model,
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel
        ]) :
        $this->render('view', [
            'model' => $model,
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel
        ]);
      } else {
        throw new ForbiddenHttpException('No permissions to view channel.');
      }
        
    }

    /**
     * Creates a new PaymentChannels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        if(Yii::$app->user->can('rw_chn')){
          $model = new PaymentChannels();

          if ($model->load(Yii::$app->request->post()) && $model->save()) {
              return $this->redirect(['view', 'id' => $model->id]);
          } else {
              return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) :
                      $this->render('create', ['model' => $model]);
          }

      } else {
        throw new ForbiddenHttpException('No permissions to update or create channels.');
      }
    }

    /**
     * Updates an existing PaymentChannels model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        if(Yii::$app->user->can('rw_chn')){
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return ($request->isAjax) ? $this->renderAjax('update', ['model' => $model]) :
                    $this->render('update', ['model' => $model]);
        }

      } else {
        throw new ForbiddenHttpException('No permissions to update or create payment channels.');
      }
    }

    /**
     * Deletes an existing PaymentChannels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {

        if(Yii::$app->user->can('del_chn')){
          
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
        } else {
        throw new ForbiddenHttpException('No permissions to delete a channel.');
      }
    }


    public function actionLogo($id){

        if(Yii::$app->user->can('rw_chn')){
            $request = Yii::$app->request;
           $model = $this->findModel($id);
           $model->scenario = 'photo';
           $imageBank = null;
          $imageBank = $model->payment_channel_logo ? ImageBank::find()->where(['id'=> $model->payment_channel_logo])->limit(1)->one() :  new ImageBank();

          Yii::trace($imageBank);
            try{
               if ($imageBank->load(Yii::$app->request->post())) {
                        $data = $_POST['ImageBank']['image_base64'];
                        list($type, $data) = explode(';', $data);
                         list(, $data)      = explode(',', $data);
                        $imageBank->image_base64 = $data;
                        $imageBank->description = 'Payment Channel Logo: ID - '.$model->id;
                        if($imageBank->save()){
                            $model->payment_channel_logo = $imageBank->id;
                            $model->save(false);
                            Logs::logEvent("Payment channel logo changed: ".$model->channel_name."(".$model->channel_code.")", null, null,null);
                            $this->redirect(['/paymentscore/payment-channels/view', 'id' => $model->id]);
                        }
                }
                 $res = ['imageBank' => $imageBank, 'model' => $model];
                return ($request->isAjax) ? $this->renderAjax('_channel_logo', $res) : $this->render('_channel_logo', $res);
            } catch(\Exception $e){
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace($error);
                Logs::logEvent("Failed to upload channel photo: ".$model->channel_name."(".$model->channel_code.")", $error, null,null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                return \Yii::$app->runAction('/site/error');
            }
        } else {
        throw new ForbiddenHttpException('No permissions to create or update channel logo.');
        }

    }



    /**
     * Finds the PaymentChannels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentChannels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentChannels::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionPaymentChannelsList($q = null, $id = null, $type = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => PaymentChannels::find()->where(['id' => $id])->channel_name];
            return $out;
        }

        $query = new Query;
        $query->select('id, channel_name AS text, payment_channel_logo, channel_code')
            ->from('payment_channels')
            ->andFilterWhere(['active' => true, 'channel_type'=>$type]);
        if($q) $query->where(['ilike', 'channel_name', $q]);

        $query->limit(20)->orderBy('channel_name');
        $command = $query->createCommand(Yii::$app->db);
        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out;
    }
}

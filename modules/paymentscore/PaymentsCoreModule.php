<?php
namespace app\modules\paymentscore;

class PaymentsCoreModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\paymentscore\controllers';
    public function init() {
        parent::init();
    }
}

<?php

namespace app\modules\workflow\controllers;


use app\components\Helpers;
use app\components\ToWords;
use app\models\AuthAssignment;
use app\models\User;
use app\models\UserSchoolAssociation;
use app\modules\banks\models\BankAccountDetails;
use app\modules\banks\models\BankDetails;
use app\modules\feesdue\controllers\FeesDueController;
use app\modules\feesdue\models\FeesDue;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\schoolcore\models\SchoolSections;
use Yii;

use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UnebPaymentsController implements the CRUD actions for UnebPaymentsReceived model.
 */
class WorkflowController extends Controller
{


    const STUDENT_BALANCE_ADJUSTMENT = 'STUDENT_BALANCE_ADJUSTMENT';
    const GROUP_BALANCE_ADJUSTMENT = 'GROUP_BALANCE_ADJUSTMENT';
    const STUDENT_FEE_ASSIGNMENT = 'STUDENT_FEE_ASSIGNMENT';
    const REMOVE_STUDENT_FEE = 'REMOVE_STUDENT_FEE';
    const GROUP_FEE_ASSIGNMENT = 'GROUP_FEE_ASSIGNMENT';
    const REMOVE_GROUP_FEE = 'REMOVE_GROUP_FEE';
    const DELETE_A_GROUP = 'DELETE_A_GROUP';
    const EDIT_FEE = 'EDIT_FEE';
    const ADD_BANK_ACCOUNT = 'ADD_BANK_ACCOUNT';
    const ADD_USER = 'ADD_USER';

    public static function recordGridDetails($workflowRecordDetails)
    {
        switch ($workflowRecordDetails['workflow_record_type']) {
            case '' . self::STUDENT_BALANCE_ADJUSTMENT . '':
                return WorkflowController::studentBalanceAdjustmentDetails($workflowRecordDetails);

            case '' . self::GROUP_BALANCE_ADJUSTMENT . '':
                return WorkflowController::groupBalanceAdjustmentDetails($workflowRecordDetails);

            case '' . self::STUDENT_FEE_ASSIGNMENT . '':
                return WorkflowController::studentFeeAssignmentDetails($workflowRecordDetails);

            case '' . self::REMOVE_STUDENT_FEE . '':
                return WorkflowController::studentRemoveFeeDetails($workflowRecordDetails);

            case '' . self::GROUP_FEE_ASSIGNMENT . '':
                return WorkflowController::groupFeeAssignmentDetails($workflowRecordDetails);

            case '' . self::REMOVE_GROUP_FEE . '':
                return WorkflowController::groupRemoveFeeDetails($workflowRecordDetails);

            case '' . self::DELETE_A_GROUP . '':
                return WorkflowController::deleteGroupFeeDetails($workflowRecordDetails);

            case '' . self::EDIT_FEE . '':
                return WorkflowController::editFeeDetails($workflowRecordDetails);
            case '' . self::ADD_BANK_ACCOUNT . '':
                return WorkflowController::addBankAccount($workflowRecordDetails);
            case '' . self::ADD_USER . '':
                return WorkflowController::addBankUser($workflowRecordDetails);

            default:
                return '';
        }
    }

    private static function studentBalanceAdjustmentDetails($workflowRecordDetails)
    {
        $details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        //   Yii::trace($request_params);

//        if(true)
//            return $request_params;

        if ($details) {

            if ($request_params->mode == 'TO') {
                $request_params->new_balance = $request_params->amount;
            } else {
                $request_params->new_balance = $request_params->amount + $details['account_balance'];
            }


            $html = 'Student Name: <b><a href="' . Url::to(['/schoolcore/core-student/view', 'id' => $details['student_id']]) . '" target="new">' . $details['student_name'] . '</a></b><br>' .

                'Current balance: <b>' . number_format($details['account_balance'], 2) . '</b><br>' .
                'Adjustment Amount: <b>' . number_format($request_params->amount, 2) . '</b><br>' .
                'New Balance: <b>' . number_format($request_params->new_balance, 2) . '</b><br>';
        } else {
            $html = '<p style="color: red">STUDENT RECORD WAS DELETED</p>';
        }

        return $html;
    }

    private static function studentDetails($id)
    {
        return (new Query())->select(['si.id as student_id', 'si.first_name', 'si.last_name', 'si.middle_name', 'sch.school_name', 'gl.account_balance',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name"])
            ->from('core_student si')
            ->innerJoin('core_school sch', 'sch.id = si.school_id')
            ->innerJoin('student_account_gl gl', 'gl.id=si.student_account_id')
            ->where(['si.id' => $id])
            ->one();
    }

    private static function groupBalanceAdjustmentDetails($workflowRecordDetails)
    {

        $request_params = json_decode($workflowRecordDetails['request_params'], false);


//        if(true)
//            return $request_params;
        $html = '';
        $groupDetails = CoreStudentGroupInformation::findOne($workflowRecordDetails['record_id']);
        //  Yii::trace($groupDetails);
        if ($groupDetails) {
            $schoolDetails = CoreSchool::findOne($workflowRecordDetails['school_id']);

            $schoolname = null;
            if (!\app\components\ToWords::isSchoolUser()) {
                //If not school user, display schoolname
                $schoolname = 'School: <b>' . $schoolDetails->school_name . '</b><br>';

            }

            $html = $schoolname .
                'Group Name: <b>' . $groupDetails->group_name . '</b><br>' .
                'Group Description: <b>' . $groupDetails->group_description . '</b><br>' .
                'New Balance: <b>' . $request_params->new_balance . '</b><br>' .
                //'Adjustment Mode: <b>' . $request_params->adjustment_mode . '</b><br>' .
                'Date Created: <b>' . $groupDetails->date_created . '</b><br>';


        }

        return $html;


    }

    private static function studentFeeAssignmentDetails($workflowRecordDetails)
    {
        $details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);
        $concession_amount = null;
        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        //  Yii::trace($request_params);

//        if(true)
//            return $request_params;
        if ($details) {

            $feeDetails = FeesDue::findOne($request_params->fee_id);
            Yii::trace($feeDetails);
            $amount = $feeDetails->due_amount;
            $concessions = Yii::$app->db->createCommand('select fc.percentage  from fee_concessions  fc INNER JOIN student_fee_concessions sfc on sfc.concessions_id = fc.id 
where sfc.student_id =:studentId and fc.fee_id =:feeId')
                ->bindValue(':feeId', $feeDetails->id)
                ->bindValue(':studentId', $details['student_id'])
                ->queryAll();

            //Yii::trace($concessions);

            if ($concessions) {
                $concession_amount = $feeDetails->due_amount * ($concessions[0]['percentage'] / 100);
                $amount = $amount - $concession_amount;
            }


            $schoolname = null;
            if (!\app\components\ToWords::isSchoolUser()) {
                //If not school user, display schoolname
                $schoolname = 'School: <b>' . $details['school_name'] . '</b><br>';

            }

            $html = 'Student Name: <b><a href="' . Url::to(['/schoolcore/core-student/view', 'id' => $details['student_id']]) . '" target="new">' . $details['student_name'] . '</a></b><br>' .
                $schoolname .
                'Fee Description: <b>' . $feeDetails->description . '</b><br>' .
                'Amount: <b>' . number_format($amount, 2) . '</b><br>' .
                'Effective Date: <b>' . $feeDetails->effective_date . '</b><br>';
        } else {
            $html = '<p style="color: red">STUDENT RECORD WAS DELETED</p>';

        }

        return $html;
    }

    private static function studentRemoveFeeDetails($workflowRecordDetails)
    {
        $details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        //   Yii::trace($request_params);

//        if(true)
//            return $request_params;


        if ($details) {

            $feeDetails = FeesDue::findOne($request_params->fee_id);
            $schoolname = null;
            if (!\app\components\ToWords::isSchoolUser()) {
                //If not school user, display schoolname
                $schoolname = 'School: <b>' . $details['school_name'] . '</b><br>';

            }

            $html = 'Student Name: <b><a href="' . Url::to(['/schoolcore/core-student/view', 'id' => $details['student_id']]) . '" target="new">' . $details['student_name'] . '</a></b><br>' .
                $schoolname .
                'Fee Description: <b>' . $feeDetails->description . '</b><br>' .
                'Amount: <b>' . number_format($feeDetails->due_amount, 2) . '</b><br>' .
                'Effective Date: <b>' . $feeDetails->effective_date . '</b><br>';

        } else {
            $html = '<p style="color: red">STUDENT RECORD WAS DELETED</p>';
        }

        return $html;
    }

    private static function groupFeeAssignmentDetails($workflowRecordDetails)
    {
        //$details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        //   Yii::trace($request_params);

//        if(true)
//            return $request_params;

        $feeDetails = FeesDue::findOne($request_params->fee_id);

        $schoolDetails = CoreSchool::findOne($workflowRecordDetails['school_id']);
        $schoolname = null;
        if (!\app\components\ToWords::isSchoolUser()) {
            //If not school user, display schoolname
            $schoolname = 'School: <b>' . $schoolDetails->school_name . '</b><br>';

        }
        $html = $schoolname .
            'Fee Description: <b>' . $feeDetails->description . '</b><br>' .
            'Amount: <b>' . number_format($feeDetails->due_amount, 2) . '</b><br>' .
            'Effective Date: <b>' . $feeDetails->effective_date . '</b><br>';


        return $html;
    }

    private static function groupRemoveFeeDetails($workflowRecordDetails)
    {
        //$details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        // Yii::trace($request_params);

//        if(true)
//            return $request_params;

        $feeDetails = FeesDue::findOne($request_params->fee_id);

        $schoolDetails = CoreSchool::findOne($workflowRecordDetails['school_id']);
        $schoolname = null;
        if (!\app\components\ToWords::isSchoolUser()) {
            //If not school user, display schoolname
            $schoolname = 'School: <b>' . $schoolDetails->school_name . '</b><br>';

        }
        $html = $schoolname .
            'Fee Description: <b>' . $feeDetails->description . '</b><br>' .
            'Amount: <b>' . number_format($feeDetails->due_amount, 2) . '</b><br>' .
            'Effective Date: <b>' . $feeDetails->effective_date . '</b><br>';


        return $html;
    }

    private static function deleteGroupFeeDetails($workflowRecordDetails)
    {
        //$details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        //  Yii::trace($request_params);

//        if(true)
//            return $request_params;

        $groupDetails = CoreStudentGroupInformation::findOne($request_params->gp_id);
        if ($groupDetails) {
            $schoolDetails = CoreSchool::findOne($workflowRecordDetails['school_id']);

            $schoolname = null;
            if (!\app\components\ToWords::isSchoolUser()) {
                //If not school user, display schoolname
                $schoolname = 'School: <b>' . $schoolDetails->school_name . '</b><br>';

            }
            $html = $schoolname .
                'Group Name: <b>' . $groupDetails->group_name . '</b><br>' .
                'Group Description: <b>' . $groupDetails->group_description . '</b><br>' .
                'Date Created: <b>' . $groupDetails->date_created . '</b><br>';
        }


        $html =
            "Item Deleted";


        return $html;
    }

    private static function editFeeDetails($workflowRecordDetails)
    {

//        if(true)
//            return $request_params;

        $feeDetails = FeesDue::findOne($workflowRecordDetails['record_id']);

        $schoolDetails = CoreSchool::findOne($workflowRecordDetails['school_id']);
        $schoolname = null;
        if (!\app\components\ToWords::isSchoolUser()) {
            //If not school user, display schoolname
            $schoolname = 'School: <b>' . $schoolDetails->school_name . '</b><br>';

        }
        $html = $schoolname .
            'Fee Description: <b>' . $feeDetails->description . '</b><br>' .
            'Amount: <b>' . number_format($feeDetails->due_amount, 2) . '</b><br>' .
            'Effective Date: <b>' . $feeDetails->effective_date . '</b><br>';


        return $html;
    }

    private static function addBankAccount($workflowRecordDetails)
    {
        //$details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        // Yii::trace($request_params);

//        if(true)
//            return $request_params;


        $schoolDetails = CoreSchool::findOne($workflowRecordDetails['school_id']);
        $schoolname = '<p style="color: red"> SCHOOL DELETED</p>';
        if($schoolDetails){
            if (!\app\components\ToWords::isSchoolUser()) {
                //If not school user, display schoolname
                $schoolname = 'School: <b>' . $schoolDetails->school_name . '</b><br>';

            }
        }


        $html = $schoolname .
            'Bank Details: <b>' . '</b><br>' .
            'Section Details: <b>' . '</b><br>';


        return $html;
    }

    private static function addBankUser($workflowRecordDetails)
    {
        //$details = WorkflowController::studentDetails($workflowRecordDetails['record_id']);

        $request_params = json_decode($workflowRecordDetails['request_params'], false);

        $userDetails = (array)($request_params);

        if ($userDetails) {
            //Yii::trace((array)$userDetails['userData']['User']);
            //  $arr = (array)$userDetails['userData']['User'];
            //  Yii::trace($arr);
            foreach ($userDetails['userData'] as $k => $v) {
                $details = (array)$v;
                ?>
                <?php

                if ($k == 'User') {
                    $html =

                        'Username: <b>' . ($details['username']) . '</b><br>' .
                        'User Level:  <b>' . ($details['user_level']) . '</b><br>';


                }
            }

        }
        return $html;
    }

    public static function requestWorkflowApproval($request)
    {
          Yii::trace($request);
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $user_id = Yii::$app->user->identity->id;
        try {
            $connection->createCommand("insert
                                    into
                                    workflow_approval_requests (date_requested,
                                    requested_by,
                                    approval_required_permission,
                                    record_id,
                                    request_params,
                                    workflow_record_type,
                                    request_notes,
                                    school_id,
                                    branch_id
                                    )
                                values(
                                now(),
                                :requested_by,
                                :approval_required_permission::json,
                                :record_id,
                                :request_params::json,
                                :workflow_record_type,
                                :request_notes,
                                :school_id,
                                :branch_id
                                )", [
                ':requested_by' => $user_id,
                ':approval_required_permission' => $request['approval_required_permission'],
                ':record_id' => $request['record_id'],
                ':request_params' => $request['request_params'],
                ':workflow_record_type' => $request['workflow_record_type'],
                ':request_notes' => $request['request_notes'],
                ':school_id' => $request['school_id'],
                ':branch_id' => $request['branch_id'],
            ])->execute();

            $insert_id = $connection->getLastInsertID();
            $transaction->commit();
            Yii::trace($insert_id);
            return $insert_id;
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
               Yii::trace($error);
            throw new Exception($error);
        }
    }

    private static function groupDetails($id)
    {

        return (new Query())->select(['gr.id', 'group_name', 'group_description', 'gr.date_created', 'gr.active', 'sch.school_name'])
            ->from('student_group_information gr')
            ->innerJoin('core_school sch', 'sch.id=gr.school_id')
            ->Where(['gr.id' => $id])
            ->one();

    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UnebPaymentsReceived models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        //Check permission first
//        if(!Yii::$app->user->can('schoolpay_admin')){
//            throw new ForbiddenHttpException('No permissions to view this area.');
//        }
        $request = Yii::$app->request;
        $searchModel = $this->workflowSearchModel();
        // Yii::trace(Yii::$app->request->queryParams);
        $data = $this->workflowItems($searchModel, Yii::$app->request->queryParams);
        $dataProvider = $data['query'];

        $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $data['pages'], 'sort' => $data['sort']];
        return $request->isAjax ? $this->renderAjax('workflow_items', $res) : $this->render('workflow_items', $res);
    }

    protected function workflowSearchModel()
    {
        $modal = new DynamicModel(['start_date', 'end_date', 'created_by', 'approval_status']);
        $modal->addRule(['start_date', 'end_date', 'created_by', 'approval_status'], 'safe');
        return $modal;
    }

    protected function workflowItems($searchModal, $params)
    {

        $user_id = Yii::$app->user->identity->id;
        $userlevel = Yii::$app->user->identity->user_level;
        $today = date('Y-m-d', time() + 86400);
        if (!empty($_GET['DynamicModel']['end_date'])) {
            $end_date = $_GET['DynamicModel']['end_date'] ? date('Y-m-d', strtotime($_GET['DynamicModel']['end_date'] . ' +1 day')) : $today;
        } else {
            $end_date = $today;

        }
        $query = (new Query())->select(['workflow.*', 'create_user.username',
            'create_user.firstname as creator_first_name', 'create_user.lastname as creator_last_name',
            "format('%s%s', create_user.firstname, 
             case when create_user.lastname is not null then ' '||create_user.lastname else '' end) as creator_name",
            "format('%s%s', approval_user.firstname, 
             case when approval_user.lastname is not null then ' '||approval_user.lastname else '' end) as approver_name",
        ])
            ->from('workflow_approval_requests workflow')
            ->innerJoin('user create_user', 'create_user.id=workflow.requested_by')
            ->leftJoin('user approval_user', 'approval_user.id=workflow.approved_by')
            ->andWhere(new Expression("(workflow.requested_by = $user_id or jsonb_exists_all(workflow.approval_required_permission, array['$userlevel']) )"));


        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $query->andWhere(['workflow.school_id' => Yii::$app->user->identity->school_id]);

        }


        if (!empty($_GET['DynamicModel']['approval_status'])) {
            $query->andFilterWhere(['approval_status' => $_GET['DynamicModel']['approval_status']]);

        } else {

            $query->andFilterWhere(['approval_status' => 'PENDING']);
        }


        if (!empty($_GET['DynamicModel']['start_date'])) {
            $query->andFilterWhere(['between', 'date_requested', $_GET['DynamicModel']['start_date'], $end_date]);

        }

        if (Yii::$app->user->can('view_by_branch')) {
            //If school user, limit and find by id and school id
            $query->andWhere(['workflow.branch_id' => Yii::$app->user->identity->branch_id]);

        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);
        $sort = new Sort([
            'attributes' => [
                'date_requested', 'workflow_record_type', 'record_id'
            ],
        ]);


        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('workflow.date_requested DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db2), 'pages' => $pages, 'sort' => $sort];
    }

    /**
     * Displays a single UnebPaymentsReceived model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        // Yii::trace($model);

        if ($model['workflow_record_type'] == self::STUDENT_BALANCE_ADJUSTMENT)
            return $this->studentBalanceAdjustmentView($model);

        if ($model['workflow_record_type'] == self::GROUP_BALANCE_ADJUSTMENT)
            // return $this->editFeeView($model);
            return $this->groupBalanceAdjustmentView($model);


        if ($model['workflow_record_type'] == self::EDIT_FEE)
            return $this->editFeeView($model);

        if ($model['workflow_record_type'] == self::STUDENT_FEE_ASSIGNMENT)
            return $this->studentAssignFeeView($model);

        if ($model['workflow_record_type'] == self::REMOVE_STUDENT_FEE)
            return $this->removeFeeFromStudentView($model);

        if ($model['workflow_record_type'] == self::GROUP_FEE_ASSIGNMENT)
            return $this->groupFeeAssignmentView($model);

        if ($model['workflow_record_type'] == self::DELETE_A_GROUP)
            return $this->deleteGroup($model);

        if ($model['workflow_record_type'] == self::REMOVE_GROUP_FEE)
            return $this->removeGroupFeeView($model);

        if ($model['workflow_record_type'] == self::ADD_BANK_ACCOUNT)
            return $this->addBankDetailsView($model);
        if ($model['workflow_record_type'] == self::ADD_USER)
            return $this->addUserView($model);

        throw new Exception('Workflow view not yet implemented');
    }

    /**
     * Finds the UnebPaymentsReceived model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UnebPaymentsReceived the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $query = (new Query())->select(['workflow.*', 'create_user.username',
            'create_user.firstname as creator_first_name', 'create_user.lastname as creator_last_name',
            "format('%s%s', create_user.firstname,
             case when create_user.lastname is not null then ' '||create_user.lastname else '' end) as creator_name",
            "format('%s%s', approval_user.firstname,
             case when approval_user.lastname is not null then ' '||approval_user.lastname else '' end) as approver_name",
        ])
            ->from('workflow_approval_requests workflow')
            ->innerJoin('user create_user', 'create_user.id=workflow.requested_by')
            ->leftJoin('user approval_user', 'approval_user.id=workflow.approved_by')
            ->andFilterWhere(['workflow.id' => $id]);

        $record = $query->one();
        if ($record !== null) {
            return $record;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function studentBalanceAdjustmentView($workflowRecord)
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $student = CoreStudent::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $student = CoreStudent::findOne($workflowRecord['record_id']);
        }

        $details = WorkflowController::studentDetails($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);

        if ($request_params->mode == 'TO') {
            $request_params->new_balance = $request_params->amount;
        } else {
            $request_params->new_balance = $request_params->amount + $details['account_balance'];
        }

        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        $res = ['model' => $student, 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
        return ($request->isAjax) ? $this->renderAjax('_student_balance_adjustment_view', $res) : $this->render('_student_balance_adjustment_view', $res);
    }

    protected function workflowActionModel()
    {
        $modal = new DynamicModel(['notes', 'approve', 'reject', 'item']);
        $modal->addRule(['notes', 'approve', 'reject', 'item'], 'safe');
        $modal->addRule(['item'], 'integer');
        $modal->addRule(['notes', 'approve', 'reject'], 'string');
        return $modal;
    }

    private function groupBalanceAdjustmentView($workflowRecord)
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id

            $studentGroup = CoreStudentGroupInformation::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $studentGroup = CoreStudentGroupInformation::findOne($workflowRecord['record_id']);
        }

        $details = WorkflowController::findMembers($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);
        // Yii::trace($request_params);

        //$request_params->new_balance = $request_params->amount + $details['account_balance'];


        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        $res = ['model' => $studentGroup, 'details' => $details, 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];

        // Yii::trace($res);
        return ($request->isAjax) ? $this->renderAjax('_group_bal_adjustment', $res) : $this->render('_group_bal_adjustment', $res);
    }

    public static function findMembers($id)
    {
        $query = (new Query())->select(['si.id', 'si.student_code', 'si.first_name', 'si.last_name', 'si.middle_name', 'si.gender', 'cls.class_code'])
            ->from('student_group_student sgr')
            ->innerJoin('core_student si', 'si.id=sgr.student_id')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->andWhere(['sgr.group_id' => $id])
            ->orderBy('cls.class_code', 'si.last_name');
        return $query->all();
    }

    private function editFeeView($workflowRecord)
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $feeDue = FeesDue::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $feeDue = FeesDue::findOne($workflowRecord['record_id']);
        }

        $fee_class = FeesDueController::staticFindFeeClasses($feeDue->id);
        $stu_exmp = FeesDueController::staticFindExmpStu($feeDue->id);
        $st_pages = new Pagination(['totalCount' => $stu_exmp->count()]);
        $cl_pages = new Pagination(['totalCount' => $fee_class->count()]);
        $fee_class->offset($cl_pages->offset);
        $fee_class->limit($cl_pages->limit);
        $stu_exmp->offset($st_pages->offset);
        $stu_exmp->limit($st_pages->limit);

        $student_exemp = $stu_exmp->all();
        $class_fee = $fee_class->all();

        $request_params = json_decode($workflowRecord['request_params'], false);

        //   Yii::trace($request_params);
        //Load penalty too
        if (isset($request_params->penalty)) {
            $feeDue->has_penalty = true; //If has penalty has been set now
        }
        $penaltyModel = $feeDue->has_penalty ? FeesDueController::staticFeesDuePenalty($feeDue) : null;


        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        $res = ['model' => $feeDue, 'request_params' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord,
            'student_exemp' => $student_exemp, 'class_fee' => $class_fee, 'cl_pages' => $cl_pages,
            'st_pages' => $st_pages, 'penaltyModel' => $penaltyModel];
        return ($request->isAjax) ? $this->renderAjax('_edit_fee_view', $res) : $this->render('_edit_fee_view', $res);
    }

    private function studentAssignFeeView($workflowRecord)
    {
        try {
            $request = Yii::$app->request;
            $actionModel = $this->workflowActionModel();
            $details = WorkflowController::studentDetails($workflowRecord['record_id']);
            Yii::trace($details);

            $actionModel->item = $workflowRecord['id'];
            $request_params = json_decode($workflowRecord['request_params'], false);

            if (\app\components\ToWords::isSchoolUser()) {
                //If school user, limit and find by id and school id
                $student = CoreStudent::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
            } else {
                $student = CoreStudent::findOne($workflowRecord['record_id']);
            }
                

            if (!$details || !$student) {
                $res = ['model' => $student, 'feeDetails' => '', 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
                return ($request->isAjax) ? $this->renderAjax('_remove_fee_from_student', $res) : $this->render('_remove_fee_from_student', $res);
            }


            $feeDetails = FeesDue::findOne(['id' => $request_params->fee_id]);
            $concessions = null;
            $amount = $feeDetails->due_amount;
            $concessions = Yii::$app->db->createCommand('select fc.percentage  from fee_concessions  fc INNER JOIN student_fee_concessions sfc on sfc.concessions_id = fc.id 
where sfc.student_id =:studentId and fc.fee_id =:feeId')
                ->bindValue(':feeId', $feeDetails->id)
                ->bindValue(':studentId', $details['student_id'])
                ->queryAll();

            // Yii::trace($concessions);

            if ($concessions) {
                $concession_amount = $feeDetails->due_amount * ($concessions[0]['percentage'] / 100);
                $amount = $amount - $concession_amount;
            }

//
//        Yii::trace($feeDetails);
//        Yii::trace($feeDetails->description);

            // $request_params->new_balance = $request_params->amount + $details['account_balance'];


            $res = ['model' => $student, 'concessions' => $concessions, 'feeDetails' => $feeDetails, 'amount' => $amount, 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
            //  Yii::trace($res);


            return ($request->isAjax) ? $this->renderAjax('_student_fee_assignment_view', $res) : $this->render('_student_fee_assignment_view', $res);


        } catch (\Exception $e) {

            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            throw new Exception($error);
        }
    }

    private function removeFeeFromStudentView($workflowRecord)
    {
        $request = Yii::$app->request;

        $details = WorkflowController::studentDetails($workflowRecord['record_id']);


        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $student = CoreStudent::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $student = CoreStudent::findOne($workflowRecord['record_id']);
        }


        $request_params = json_decode($workflowRecord['request_params'], false);
        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        if (!$details || !$student) {
            $res = ['model' => $student, 'feeDetails' => '', 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
            return ($request->isAjax) ? $this->renderAjax('_remove_fee_from_student', $res) : $this->render('_remove_fee_from_student', $res);
        }

        $request_params->new_balance = $request_params->amount + $details['account_balance'];
        $feeDetails = FeesDue::findOne(['id' => $request_params->fee_id]);


        $res = ['model' => $student, 'feeDetails' => $feeDetails, 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];


        return ($request->isAjax) ? $this->renderAjax('_remove_fee_from_student', $res) : $this->render('_remove_fee_from_student', $res);
    }

    private function groupFeeAssignmentView($workflowRecord)
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id

            $studentGroup = CoreStudentGroupInformation::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $studentGroup = CoreStudentGroupInformation::findOne($workflowRecord['record_id']);
        }

        $details = WorkflowController::findMembers($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);


        $feeDetails = FeesDue::findOne($request_params->fee_id);
        //$request_params->new_balance = $request_params->amount + $details['account_balance'];


        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        $res = ['model' => $studentGroup, 'details' => $details, 'feeDetails'=>$feeDetails,'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];

        //Yii::trace($res);
        return ($request->isAjax) ? $this->renderAjax('_group_fee_assignment', $res) : $this->render('_group_fee_assignment', $res);
    }

    private function deleteGroup($workflowRecord)
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id

            $studentGroup = CoreStudentGroupInformation::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $studentGroup = CoreStudentGroupInformation::findOne($workflowRecord['record_id']);
        }

        $details = WorkflowController::findMembers($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);


        //$request_params->new_balance = $request_params->amount + $details['account_balance'];


        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        $res = ['model' => $studentGroup, 'details' => $details, 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
        return ($request->isAjax) ? $this->renderAjax('_delete_group', $res) : $this->render('_delete_group', $res);
    }

    private function removeGroupFeeView($workflowRecord)
    {
        $request = Yii::$app->request;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id

            $studentGroup = CoreStudentGroupInformation::findOne(['id' => $workflowRecord['record_id'], 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $studentGroup = CoreStudentGroupInformation::findOne($workflowRecord['record_id']);
        }

        $details = WorkflowController::findMembers($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);


        //$request_params->new_balance = $request_params->amount + $details['account_balance'];


        $actionModel = $this->workflowActionModel();
        $actionModel->item = $workflowRecord['id'];

        $res = ['model' => $studentGroup, 'details' => $details, 'requestParams' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];

        //Yii::trace($res);
        return ($request->isAjax) ? $this->renderAjax('_remove_group_fee', $res) : $this->render('_remove_group_fee', $res);
    }

    private function addBankDetailsView($workflowRecord)
    {
        $request = Yii::$app->request;

        //  $details = WorkflowController::studentDetails($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);

        //  Yii::trace($request_params);
        //$request_params->new_balance = $request_params->amount + $details['account_balance'];
        $schDetails = CoreSchool::findOne(['id' => $workflowRecord['school_id']]);

        Yii::trace($schDetails);
        $actionModel = $this->workflowActionModel();
        //  Yii::trace($actionModel);



        $actionModel->item = $workflowRecord['id'];

        $res = ['requestParams' => $request_params, 'schDetails' => $schDetails, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
        return ($request->isAjax) ? $this->renderAjax('_add_bank_account', $res) : $this->render('_add_bank_account', $res);
    }

    private function addUserView($workflowRecord)
    {
        $request = Yii::$app->request;

        //  $details = WorkflowController::studentDetails($workflowRecord['record_id']);
        $request_params = json_decode($workflowRecord['request_params'], false);

        //  Yii::trace($request_params);
        //$request_params->new_balance = $request_params->amount + $details['account_balance'];

        //Yii::trace($schDetails);
        $actionModel = $this->workflowActionModel();
        //  Yii::trace($actionModel);


        $actionModel->item = $workflowRecord['id'];

        $res = ['request_params' => $request_params, 'actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];
        return ($request->isAjax) ? $this->renderAjax('_add_user', $res) : $this->render('_add_user', $res);
    }

    public function getBank()
    {
        return $this->hasOne(BankDetails::className(), ['id' => 'bank_name']);
    }

    public function actionDo($action)
    {

        $request = Yii::$app->request;
        $actionModel = $this->workflowActionModel();

        $actionModel->load($request->post());

        //The workflow request model
        $workFlowModel = $this->findModel($actionModel->item);

        if ($workFlowModel['workflow_record_type'] == 'ADD_BANK_ACCOUNT') {
            //Yii::trace($workFlowModel);
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $user_id = Yii::$app->user->identity->id;
            try {
                $bankDetails = ($workFlowModel['request_params']);


                $this->addAccounts($workFlowModel['school_id'], $bankDetails,$workFlowModel);

                //update workflow
                //Set as approved
                $connection = Yii::$app->db;
                $sql2 = "update workflow_approval_requests set approval_status = 'APPROVED', approval_notes = :approval_notes,
				date_approved = :date_approved, approved_by = :user_id where id = :workFlowId";
                $connection->createCommand($sql2)
                    ->bindValue(':date_approved', new Expression('NOW()'))
                    ->bindValue(':approval_notes', "Add/Update bank Information")
                    ->bindValue(':user_id', Yii::$app->user->identity->id)
                    ->bindValue(':workFlowId', $workFlowModel['id'])
                    ->execute();
                $transaction->commit();

                Yii::$app->session->setFlash('workflowInfo', "<i class='fa fa-check-circle-o'></i>&nbsp; Workflow item has been approved successfully");
                return $this->redirect(['/workflow/workflow/index']);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::$app->session->setFlash('workflowError', $error);
                return $this->redirect(['/workflow/workflow/view', 'id' => $workFlowModel['id']]);
            }

        } else if ($workFlowModel['workflow_record_type'] == 'ADD_USER') {
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $user_id = Yii::$app->user->identity->id;
            try {

                $request_params = json_decode($workFlowModel['request_params'], false);
                $userDetails = (array)$request_params;
                $this->addUser($workFlowModel, $userDetails);

                //update workflow
                //Set as approved
                $connection = Yii::$app->db;
                $sql2 = "update workflow_approval_requests set approval_status = 'APPROVED', approval_notes = :approval_notes,
				date_approved = :date_approved, approved_by = :user_id where id = :workFlowId";
                $connection->createCommand($sql2)
                    ->bindValue(':date_approved', new Expression('NOW()'))
                    ->bindValue(':approval_notes', "Add/Update bank Information")
                    ->bindValue(':user_id', Yii::$app->user->identity->id)
                    ->bindValue(':workFlowId', $workFlowModel['id'])
                    ->execute();
                $transaction->commit();

                Yii::$app->session->setFlash('workflowInfo', "<i class='fa fa-check-circle-o'></i>&nbsp; Workflow item has been approved successfully");
                return $this->redirect(['/workflow/workflow/index']);
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::$app->session->setFlash('workflowError', $error);
                return $this->redirect(['/workflow/workflow/view', 'id' => $workFlowModel['id']]);
            }


        } else {


            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $user_id = Yii::$app->user->identity->id;
            try {
                //select * from adjust_student_balance(1000013519, -1000, 10, 'jm', 'paid ko a little', '.0.0.0.0');
                $result = $connection->createCommand("select * from workflow_action(
                        :item, 
                        :userid, 
                        :action, 
                        :notes)", [
                    ':item' => $actionModel->item,
                    ':userid' => $user_id,
                    ':action' => strtoupper($action),
                    ':notes' => $actionModel->notes])->queryAll();

                $transaction->commit();
                $dbResult = $result[0];
                if ($dbResult['return_code'] != 0) {
                    Yii::$app->session->setFlash('workflowError', 'Workflow error (' . $dbResult['return_code'] . ') - ' . $dbResult['return_message']);
                    return $this->redirect(['/workflow/workflow/view', 'id' => $workFlowModel['id']]);
                }

                Yii::$app->session->setFlash('workflowInfo', "<i class='fa fa-check-circle-o'></i>&nbsp; Workflow item has been approved successfully");
                return $this->redirect(['/workflow/workflow/index']);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::$app->session->setFlash('workflowError', $error);
                return $this->redirect(['/workflow/workflow/view', 'id' => $workFlowModel['id']]);
            }
        }
    }

    public function addAccounts($id, $bankDetails,$workFlowModel)
    {


        $accounts = $this->getSchoolAccounts($id);


        $school_sections = $this->getSections($id);

        if (isset($bankDetails)) {
            //$bankDetails;
            //   Yii::trace($bankDetails);

            $bankDetails = json_decode($bankDetails, true);

            //  Yii::trace($bankDetails);
            if (isset($bankDetails['bank_details'])) {
                $this->updateSchAccounts($id, $accounts, $bankDetails['bank_details']);
            }


            if (isset($bankDetails['section_details'])) {
                $this->updateSchSections($id, $school_sections, $bankDetails['section_details']);
            }

        }

    }

    private function getSchoolAccounts($schId)
    {
        $accounts = (new Query())->from('core_bank_account_details ba')->select(['ba.id as bid', 'nb.id as bank_id', 'nb.bank_name', 'ba.account_title', 'ba.account_number', 'ba.account_type'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ba.bank_id')
            ->where(['ba.school_id' => $schId]);
        return $accounts->all();
    }

    private function getSections($schId)
    {
        $sections = (new Query())->from('school_sections ss')->select(['ss.id', 'section_name', 'section_code', 'section_primary_bank', 'nb.bank_name', 'section_account_number', 'bad.account_number as section_bank_account'])
            ->innerJoin('core_nominated_bank nb', 'nb.id=ss.section_primary_bank')
            ->innerJoin('core_bank_account_details bad', 'bad.id=ss.section_account_number')
            ->where(['ss.school_id' => $schId]);
        return $sections->all();
    }

    private function updateSchAccounts($sch_id, $accounts, $posts)
    {
        //  yii::trace($posts);
        // $posts =json_decode($posts, true);
        $db_accounts = array_column($accounts, 'bid'); //id is in bid
        $post_accounts = array_column($posts, 'id');

        $diff_del = array_diff($db_accounts, $post_accounts);
        if (!empty($diff_del)) {
            foreach ($diff_del as $v) {
                $schAcc = CoreBankAccountDetails::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                $schAcc->delete();
            }
        }


        foreach ($posts as $k => $v) {
//            Yii::trace($v);
//            Yii::trace($v['bank_id']);

            $bankAcc = isset($v['id']) ? CoreBankAccountDetails::find()->where(['id' => $v['id']])->limit(1)->one() : new CoreBankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;

//            if (!preg_match('/^\d{13}$/', $bankAcc->account_number))
//                throw new Exception('Account number should be 13 digits');

            $bankAcc->save(false);
        }
    }

    private function updateSchSections($sch_id, $existing_sections, $posted_sections)
    {
        Yii::trace('Posted sections is ' . print_r($posted_sections, true));
        $post_accounts = [];
        $db_accounts = array_column($existing_sections, 'id');

        if ($posted_sections) {
            $post_accounts = array_column($posted_sections, 'id');

        }
        $unset_section_ids = array_diff($db_accounts, $post_accounts); //these are the ones which have been deleted
        //Delete sections the user has deleted from the interface
        //TODO Delete associations too
        Yii::trace('Posted sections is ' . print_r($unset_section_ids, true));

        if (!empty($unset_section_ids)) {
            foreach ($unset_section_ids as $v) {
                $schAcc = SchoolSections::find()->where(['school_id' => $sch_id, 'id' => $v])->limit(1)->one();
                $schAcc->delete();
            }
        }
        if (!empty($posted_sections)) {
            foreach ($posted_sections as $k => $v) {
                $section = isset($v['id']) ? SchoolSections::find()->where(['id' => $v['id'], 'school_id' => $sch_id])->limit(1)->one() : new SchoolSections();
                Yii::trace('The section is ' . print_r($section, true));
                $section->school_id = $sch_id;
                $section->section_name = $v['section_name'];
                $section->active = true;
                $section->section_primary_bank = $v['section_primary_bank'];
                $section->section_account_number = $v['section_account_number'];
                if (!$section->validate()) {
                    throw new Exception('Invalid or missing section details');
                }
                $section->save(true);
            }
        }
    }

    public function addUser($workflow, $userDetails)
    {


        Yii::trace($userDetails);
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        $model = new User(['scenario' => 'create']);
        $selectedVal = [];
        $schools = null;
        $mult = false;
        if (isset($userDetails)) {
            foreach ($userDetails['userData'] as $k => $v) {
                $details = (array)$v;

                if ($k == 'User') {
                    $passwd = $details['password'];

                    if (Helpers::IsSchoolUser($details['user_level'])) {
                        $schs = $details['school_id'];

                        $ct = count($schs);
                        if ($ct == 1) {
                            $model->school_id = $details['school_id'][0];
                        } else if ($ct > 1) {
                            $schools = $details['school_id'];
                            $model->school_id = $details['school_id'][0];
                            $mult = true;

                        }
                    } else {
                        $model->school_id = null;
                    }
                    $model->email = $details['email'];
                    $model->mobile_phone = $details['mobile_phone'];
                    $model->firstname = $details['firstname'];
                    $model->lastname = $details['lastname'];
                    $model->user_level = $details['user_level'];
                    $model->username = strtolower($details['username']);
                    if (isset($details['branch_id'])) {
                        $model->branch_id = $details['branch_id'];

                    }
                    $model->setPassword($passwd);
                    $model->password2 = $model->password;
                    if (!$model->save(false)) {
                        Yii::$app->session->setFlash('error', 'Failed: Sorry, there was a problem creating user. Please try again or contact support for help.');
                    }
                    $model->setUserSchools($schools, $model->id, $mult);
                    $model->setAuthAssignment($model->user_level, strval($model->id));

                    $transaction->commit();
                    Logs::logEvent("New User Created(" . $model->id . "): " . $model->username, null, null);
                    //Send user created email
                    $fullname = $model->getFullname();
                    $emailSubject = 'Awash ESchool Logins | ' . $fullname;

                    $url = Url::to('/site/login', true);

                    $emailText = "Hello $fullname\n
            
            Greetings from  Awash ESchool!\n
            The following are your credentials for  Awash ESchool\n
            Username: $model->username \n
            Password: $passwd \n
            To login, go to $url\n\n
            Thank you for choosing  Awash ESchool.";
                    ToWords::sendEmail($model->email,
                        $emailSubject,
                        $emailText);
                    Yii::$app->session->setFlash('success', 'Successful, Credentials have been set to user\'s email address.');

                    return $this->redirect(['/user/view', 'id' => $model->id]);
                }
            }
        }

    }

    public function actionPoll()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['code' => '99', 'text' => 'Error'];


        $user_id = Yii::$app->user->identity->id;
        $userlevel = Yii::$app->user->identity->user_level;
        $query = (new Query())->select(['count(*)'])
            ->from('workflow_approval_requests workflow')
            ->andWhere(new Expression("(jsonb_exists_all(workflow.approval_required_permission, array['$userlevel']) )"))
            ->andWhere(new Expression("workflow.requested_by <> $user_id "))
            ->andWhere(['approval_status' => 'PENDING']);


        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $query = $query->andWhere(['workflow.school_id' => Yii::$app->user->identity->school_id]);

        }

        $count = $query->count('*');


        $out = ['code' => '0', 'text' => 'Polled', 'count' => $count];
        return $out;
    }

    private function saveAccounts($posted_banks, $sch_id)
    {
        foreach ($posted_banks as $k => $v) {
            $bankAcc = new BankAccountDetails();
            $bankAcc->bank_id = $v['bank_id'];
            $bankAcc->account_type = $v['account_type'];
            $bankAcc->account_title = $v['account_title'];
            $bankAcc->account_number = $v['account_number'];
            $bankAcc->school_id = $sch_id;

//            if (!preg_match('/^\d{13}$/', $bankAcc->account_number))
//                throw new Exception('Account number should be 13 digits');
            $bankAcc->save(false);
        }
    }


}

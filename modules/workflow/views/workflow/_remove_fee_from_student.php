<?php use barcode\barcode\BarcodeGenerator;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="letter">
    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
        <h3 style="text-decoration: underline">REMOVE FEE FROM STUDENT</h3>

    </div>

    <?php if($model){?>
    <div class="col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-sm-2 col-xs-12 no-padding-right">
                    <?PHP
                    if ($model->student_image): ?>
                        <img class="img-thumbnail"
                             src="<?= Url::to(['/import/import/image-link', 'id' => $model->student_image]) ?>"
                             width="130px"/>
                    <?php else : ?>
                        <?= Html::img('@web/web/img/dummy-user.jpg', ['class' => 'img-thumbnail', 'alt' => 'profile photo', 'width' => 130, 'data-toggle' => 'tooltip', 'data-original-title' => 'Tooltip on right']) ?>
                    <?php endif; ?>
                </div>
                <!-- <div  style="margin-top:-150px; margin-left:160px;"> -->
                <div class="col-sm-12 no-padding" style="margin-top:-20px;">
                    <h3> <?= !$model->archived ? $model->fullname : '<span style="color:red">' . $model->fullname . '</span>'; ?></h3>
                    <h4> Payment code: &nbsp; <?= $model->student_code; ?></h4>
                    <b>Reg No
                        : </b><?= ($model->school_student_registration_number) ? $model->school_student_registration_number : "--" ?>
                    <br>
                    <?php if (!$model->archived) : ?>
                        <b>Class /Course
                            : </b><?= ($model->class_id) ? $model->studentClass->class_description : ' --' ?><br>
                    <?php elseif ($model->class_when_archived) : ?>
                        <b>Last Class /Course (before
                            Archiving): </b><?= ($model->lastArchivedClass) ? $model->lastArchivedClass->class_description : ' --' ?>
                        <br>
                    <?php endif; ?>
                    <div id="stu_barcode"></div>
                </div>
                <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
                    <h3>Fee Details </h3>
                    <div class="col-xs-12">
                            <span class="col-xs-12" style="border: 1px dashed #ccc; padding:10px; font-size:16px;">
                                <tt><b>Description :</b> <?= ($feeDetails->description) ? $feeDetails->description : "  --"; ?></tt><br>
                                <tt><b>Amount :</b> <?= ($feeDetails->due_amount) ? number_format($feeDetails->due_amount, 2) : "  --"; ?></tt><br>
                                <tt><b>Effective Date : </b><?= ($feeDetails->effective_date) ? $feeDetails->effective_date : "  --"; ?></tt><br>


                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="l_header" style="margin-top:15px;">
    <span style="font-size:11px; color:#444"><i><?= $model->studentClass->institution0->school_name . ', ' . $model->studentClass->institution0->village . ', TEL : ' . $model->studentClass->institution0->phone_contact_1; ?></i></span>
    <p></p>
    <?php $optionsArray = array(
        'elementId' => 'stu_barcode', /* div or canvas id*/
        'value' => $model->student_code, /* value for EAN 13 be careful to set right values for each barcode type */
        'type' => 'code39',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
    );
    echo BarcodeGenerator::widget($optionsArray); ?>
    Request Notes: <?= $workflowRecord['request_notes'] ?>
        <br>
        <br>
        <hr class="l_normal">
        <br>
        <?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord]); ?>
        <div style="clear: both"></div>
    <?php
    }else{?>

        <div> <p style="color: red">The student in this workflow was deleted, please decline request </p></div>
        <hr class="l_normal">
        <br>
        <?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord]); ?>
        <div style="clear: both"></div>
   <?php }
    ?>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

<?php
/**
 * @var \app\models\InstitutionFeesDuePenalty $penaltyModel
 * @var \app\models\FeesDue $model
 */
Yii::trace($penaltyModel);
$penaltyDelta = isset($request_params->penalty) ? $request_params->penalty : (object)[];
//Overwrite changed fields
foreach ($penaltyDelta as $key => $value) {
    $penaltyModel->$key = $value;
}
?>
<div class="col-md-12 col-xs-12 col-sm-12">
    <hr>
    <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
        <div class="col-lg-6 col-xs-6 profile-label">Penalty Type</div>
        <div class="col-lg-6 col-xs-6 profile-text  <?= isset($penaltyDelta->penalty_type) ? 'alert-warning' : '' ?>"><?= $penaltyModel->penalty_type ? $penaltyModel->penalty_type : '-' ?></div>
    </div>

    <?php if ($penaltyModel->penalty_type == 'PERCENTAGE') : ?>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label">Penalty Percentage</div>
            <div class="col-lg-6 col-xs-6 profile-text  <?= isset($penaltyDelta->penalty_amount) ? ' alert-warning ' : '' ?>"><?= $penaltyModel->penalty_amount ? $penaltyModel->penalty_amount : '' ?>
                %
            </div>
        </div>

    <?php else : ?>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label">Penalty Amount</div>
            <div class="col-lg-6 col-xs-6 profile-text <?= isset($penaltyDelta->penalty_amount) ? ' alert-warning ' : '' ?>"><?= $penaltyModel->penalty_amount ? $penaltyModel->penalty_amount : '' ?></div>
        </div>
    <?php endif; ?>

    <?php if ($penaltyModel->penalty_frequency == 'RECURRENT') : ?>
        <?php
        $penaltyUnits = 'Day';
        if ($penaltyModel->penalty_apply_frequency_unit == 'M') $penaltyUnits = 'Month';
        if ($penaltyModel->penalty_apply_frequency_unit == 'W') $penaltyUnits = 'Week';

        //Work on plural
        if ($penaltyModel->penalty_apply_frequency_term > 1) $penaltyUnits .= 's';
            ?>
            <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label">Apply Penalty</div>
            <div class="col-lg-6 col-xs-6 profile-text <?= (isset($penaltyDelta->penalty_apply_frequency_unit) || isset($penaltyDelta->penalty_apply_frequency_term)) ? ' alert-warning ' : '' ?>">Every <?= $penaltyModel->penalty_apply_frequency_term . " $penaltyUnits" ?></div>
        </div>
    <?php else : ?>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label">Apply Penalty</div>
            <div class="col-lg-6 col-xs-6 profile-text <?= (isset($penaltyDelta->penalty_apply_frequency_unit) || isset($penaltyDelta->penalty_apply_frequency_term)) ? ' alert-warning ' : '' ?>">ONLY ONCE</div>
        </div>
    <?php endif; ?>

    <?php
    $penaltyUnits = 'Days';
    if ($model->arrears_after_units == 'M') $penaltyUnits = 'Months';
    if ($model->arrears_after_units == 'W') $penaltyUnits = 'Weeks';
    ?>


</div>
<div class="col-md-12 col-xs-12 col-sm-12">
    <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
        <div class="col-lg-6 col-xs-6 profile-label">Penalty Starts</div>
        <div class="col-lg-6 col-xs-6 profile-text <?= (isset($penaltyDelta->arrears_after_count) || isset($penaltyDelta->arrears_after_units)) ? ' alert-warning ' : '' ?>"><?= $model->arrears_after_count . " $penaltyUnits after effective date" ?></div>
    </div>
</div>

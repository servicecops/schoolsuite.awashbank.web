<?php
/**
 * FeesDue $fees
 */
?>
<div class="box-body table table-responsive no-padding">
    <h3>Associated Fees</h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Sr.No</th>
            <th>Fee Description</th>
            <th>Amount</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        </thead>
        <tbody>
        <?php

        use yii\helpers\Url;

        if ($fees) :
            $srNo = 1;
            foreach ($fees as $fee) : ?>
                <tr>
                    <td><?= $srNo; ?></td>
                    <td class="clink"><a
                            href="<?= Url::to(['/fees-due/view', 'id' => $fee['id']]) ?>"><?= ($fee['description']) ? $fee['description'] : '<span class="not-set"> -- </span>' ?></a>
                    </td>
                    <td><?= number_format($fee['due_amount'], 2) ?></td>
                    <td><?= ($fee['effective_date']) ? $fee['effective_date'] : '<span class="not-set"> -- </span>' ?></td>
                    <td><?= ($fee['end_date']) ? $fee['end_date'] : '<span class="not-set"> -- </span>' ?></td>
                </tr>
                <?php
                $srNo++;
            endforeach;
        else :
            ?>
            <td colspan="8">No Records found for the search Criteria</td>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<?php

use app\models\PaymentChannels;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnebPaymentsReceivedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="uneb-payments-received-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess']
    ]); ?>
    <ul class=" menu-list no-padding">
        <li class="col-xs-2 no-padding"><?= DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'start_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'From'],
                'clientOptions' => [
                    'class' => 'form-control',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1900:' . (date('Y') + 1),
                    'autoSize' => true,
                ],
            ]); ?></li>
        <li class="col-xs-2 no-padding"><?= DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'end_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control input-sm', 'placeHolder' => 'To'],
                'clientOptions' => [
                    'class' => 'form-control',
                    'changeMonth' => true,
                    'changeYear' => true,
                    'yearRange' => '1900:' . (date('Y') + 1),
                    'autoSize' => true,
                ],
            ]); ?></li>
        <li class="col-xs-2 no-padding"><?= $form->field($searchModel, 'approval_status')->dropDownList(['PENDING' => 'PENDING', 'APPROVED' => 'APPROVED'], ['prompt' => 'Approval Status', 'class' => 'form-control input-sm'])->label(false) ?></li>
        <li class="col-xs-2 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></li>
    </ul>
    <?php ActiveForm::end(); ?>

</div>

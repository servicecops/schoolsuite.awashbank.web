<?php use barcode\barcode\BarcodeGenerator;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="row">
    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if($schDetails){?>


    <div class="letter">
        <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
            <h3 style="text-decoration: underline">ADD/UPDATE BANK ACCOUNT</h3>

        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-xs-12">

                </div>
            </div>
        </div>

        <hr class="l_header" style="margin-top:15px;">
        <span style="font-size:11px; color:#444">
        <p></p>

        Request Notes: <?= $workflowRecord['request_notes'] ?>

            <?php


            $bankdetails = (array)($requestParams);
            ?>
        </span>
  <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
    <h3>Bank Account Details to be considered </h3>

        <table class="table " style="table-layout: fixed ;">
            <thead>

            <tr>
                <th style="text-align: center !important;"> Account Title</th>
                <th style="text-align: center !important;"> Account Type</th>
                <th style="text-align: center !important;"> Account Number</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($bankdetails) :
                foreach ($bankdetails['bank_details'] as $k => $v):
                    $details = (array)$v;
                    Yii::trace($details);
                    ?>

                    <tr>
                        <td><?= $details['account_title'] ?></td>
                        <td><?= $details['account_type'] ?></td>
                        <td><?= $details['account_number'] ?></td>
                    </tr>
                <?php endforeach;
            else :
                ?>
                <tr>
                    <td colspan="4">No account provided for this school</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
      <hr class="l_normal"><br>

</div>




  <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
    <h3>Section Details to be considered </h3>

      <table class="table " style="table-layout: fixed ;">
            <thead>

            <tr>
                <th style="text-align: center !important;"> Section Name</th>
                <th style="text-align: center !important;"> Section Account</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($bankdetails['section_details']) :
                foreach ($bankdetails['section_details'] as $k => $v):
                    $details = (array)$v;
                    Yii::trace($details);
                    ?>

                    <tr>
                            <td><?= $details['section_name'] ?></td>
                            <td><?= $details['section_account_number'] ?></td>
                    </tr>
                <?php endforeach;
            else :
                ?>
                <tr>
                    <td colspan="4">No Sections set for this school</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
  </div>
    <div style="clear: both"></div>
        <?php }else{?>
<div class="col-md-12"><p style="color: red">School record is deleted, please decline this request</p></div>


        <?php }?>

        <hr class="l_normal"><br>
        <?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord]); ?>
    </div>
</div>
    <?php
    $script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
    $this->registerJs($script);
    ?>

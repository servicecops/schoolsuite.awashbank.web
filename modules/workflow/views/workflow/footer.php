<?php
$user_id = Yii::$app->user->identity->id;
$params = ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord];

if($workflowRecord['approval_status'] == 'PENDING') {

    if($workflowRecord['requested_by'] == $user_id) {
        echo $this->render('_submitted_note', $params);
    }else {
        echo $this->render('_workflow_buttons', $params);
    }
}else if($workflowRecord['approval_status'] == 'APPROVED') {
    echo $this->render('_approved_note', $params);
}
?>

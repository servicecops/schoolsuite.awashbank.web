<?php use barcode\barcode\BarcodeGenerator;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="row">
    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>


        <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
            <h3 style="text-decoration: underline">ADD USER</h3>

        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-xs-12">

                </div>
            </div>
        </div>
        <hr class="l_header" style="margin-top:15px;">
        <span style="font-size:11px; color:#444">
        <p></p>

        Request Notes: <?= $workflowRecord['request_notes'] ?>


        </span>
        <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
            <h3>User Details to be considered </h3>


            <?php


            $userDetails = (array)($request_params);

                if ($userDetails) :
                    //Yii::trace((array)$userDetails['userData']['User']);
                  //  $arr = (array)$userDetails['userData']['User'];
                  //  Yii::trace($arr);
                    foreach ($userDetails['userData']as $k => $v):
                        $details = (array)$v;
                                            ?>
                        <?php

                        if($k =='User'){
                            ?>
                            <div>
                                <h3>Username: <b><?= ($details['username'])? $details['username']:'--' ?></b></h3>
                                <h3>First Name:  <b><?= ($details['firstname'])? $details['firstname']:'--' ?></b></h3>
                                <h3>Last Name: <b><?= ($details['lastname'])? $details['lastname']:'--' ?></b> </h3>
                                <h3>Email:  <b><?= ($details['email'])? $details['email']:'--' ?> </h3>
                                <h3>Phone Number: <b><?= ($details['mobile_phone'])? $details['mobile_phone']:'--' ?></b> </h3>
                                <h3>User Level:  <b><?=($details['user_level'])? $details['user_level']:'--'?></b> </h3>
                                <h3>School:<br/> <b><?php if($details['school_id']) {
                                            foreach($details['school_id']as $k => $v){
                                                echo \app\modules\schoolcore\models\CoreSchool::findOne($v)->school_name;?><br/>
                                           </b>
                                           <?php }
                                                }

                                           ?></br> </h3>
                            </div>
                        <?php }
                        ?>


                    <?php endforeach;
                else :
                    ?>
                    <div>
                        <td colspan="4">No account provided for this school</td>
                    </div>
                <?php endif; ?>

            <hr class="l_normal"><br>

        </div>




        <div style="clear: both"></div>

        <hr class="l_normal"><br>
        <?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord]); ?>

</div>
<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

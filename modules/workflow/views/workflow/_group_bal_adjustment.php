<?php use barcode\barcode\BarcodeGenerator;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="letter">
    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
        <h3 style="text-decoration: underline">ADJUST GROUP FEE </h3>

    </div>
    <div class="col-sm-12 col-xs-12">

        <div class="col-sm-12">
            <!-- <div  style="margin-top:-150px; margin-left:160px;"> -->
            <div class="col-sm-12 no-padding" style="margin-top:-20px;">
                <h3> <?= !$model->school_id ? $model->school_id : '<span style="text-align: center">' . $model->school->school_name . '</span>'; ?></h3>
                <p>Group
                    Name: <b> <?= !$model->group_name ? $model->group_name : $model->group_name ?></b></p>
                <p> Group Description: &nbsp; <b><?= $model->group_description; ?></b></p>
                <p> New Balance: <b>&nbsp; <?= $requestParams->new_balance; ?></b></p>
                <p> Adjustment Mode: <b>&nbsp; <?= $requestParams->adjustment_mode; ?></b></p>


                <div id="stu_barcode"></div>
            </div>
        </div>

    </div>

    <hr class="l_normal">
    <br>
    <div class="col-sm-12 col-xs-12 ">
        <?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord]); ?>

    </div>


    <div class="col-sm-12 col-xs-12">

        <?= $this->render('_action_group_members',
            [
                'members' => $details,
            ]);
        ?>


    </div>
    <div style="clear:both"></div>
</div>

<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

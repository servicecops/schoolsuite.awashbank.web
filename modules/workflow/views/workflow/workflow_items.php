<?php

use app\modules\workflow\controllers\WorkflowController;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Workflow Approval Items';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-4 content-title no-padding" style="padding-top:13px !important;">
            <i class="fa fa-th-list"></i> Workflow Approval Items
        </div>
        <div class="col-md-8 no-padding"><?php echo $this->render('_search', ['searchModel' => $searchModel]); ?></div>
        <div class="col-md-12 no-padding">

            <div class="box-body table table-responsive no-padding">
                <h3>Group Members</h3>
                <div class="box-body table table-responsive no-padding">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class='clink'><?= $sort->link('date_requested', ['label' => 'Date']) ?></th>
                            <th class='clink'>Record Details</th>
                            <th class='clink'>Request Type</th>
                            <th class='clink'>Request Notes</th>
                            <th class='clink'>Requested By</th>
                            <th class='clink'>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($dataProvider) :
                            foreach ($dataProvider as $k => $v) : ?>
                                <?php
                                $class = '';
                                if ($v['approval_status'] == 'PENDING')
                                    $class = 'alert-warning';

                                if ($v['approval_status'] == 'REJECTED')
                                    $class = 'alert-danger';

                                if ($v['approval_status'] == 'APPROVED')
                                    $class = 'alert-info';
                                ?>
                                <tr data-key="0">
                                    <td><?= ($v['date_requested']) ? date('d/m/Y H:i', strtotime($v['date_requested'])) : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= WorkflowController::recordGridDetails($v) ?></td>
                                    <td><?= $v['workflow_record_type'] ?></td>
                                    <td><?= $v['request_notes'] ?></td>
                                    <td><?= $v['creator_name'] ?></td>
                                    <td class="<?= $class ?>"><?= $v['approval_status'] ?></td>
                                    <td>
                                        <a href="javascript:;"
                                           onclick="clink('<?= Url::to(['/workflow/workflow/view', 'id' => $v['id']]) ?>')"
                                           title="View"><span class="fa fa-search"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach;
                        else : ?>
                            <tr>
                                <td colspan="9">No Records found</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <?= LinkPager::widget([
                    'pagination' => $pages,
                ]); ?>

            </div>
        </div>


        <div style="clear: both"></div>
    </div>

    <?php
    $script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
    $this->registerJs($script);
    ?>

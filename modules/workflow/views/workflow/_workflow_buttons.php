<div class="image-bank-form">

    <?php use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $form = ActiveForm::begin([
        'action' => ['do'],
        'method' => 'post',
        'options' => ['class' => 'formprocess', 'id' => 'workflowActionsForm']
    ]); ?>
    <div class="col-sm-12 col-xs-12">
        <?= $form->field($actionModel, 'notes')->textarea()->label('Approval / Rejection Notes') ?>
        <?= $form->field($actionModel, 'item')->hiddenInput()->label(false) ?>
    </div>

    <div class="form-group">
        <?= Html::button('Approve <i class="fa fa-check-circle"></i>', ['class' => 'btn btn-success',
            'onclick' => 'confirmAndSubmitWorkflow("workflowActionsForm", "Are you sure you want to confirm this work flow item", "do?action=approve")',
            'value' => 'Approve', 'name' => 'submit']) ?>

        <?= Html::button('Reject <i class="fa fa-times-circle"></i>', ['class' => 'btn btn-danger pull-right',
            'onclick' => 'confirmAndSubmitWorkflow("workflowActionsForm", "Are you sure you want to reject this work flow item", "do?action=reject")',
            'value' => 'Reject', 'name' => 'submit']) ?>

    </div>
    <?php ActiveForm::end(); ?>


</div>


<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $penaltyModel \app\models\InstitutionFeesDuePenalty */
/* @var $model app\models\FeesDue */



$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Fees Dues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row hd-title" data-title="<?= $model->description ?>">

    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="col-md-12">
        <div class="col-md-8 no-padding">
            <h3><i class="fa fa-exchange"></i> &nbsp;<?= Html::encode($this->title) ?></h3>
        </div>


    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 profile-label"><?= $model->getAttributeLabel('school_id') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->school->school_name) ? Html::a($model->school->school_name, ['/school-information/view', 'id' => $model->school->id]) : "--" ?></div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('due_amount') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= isset($request_params->due_amount) ? '<strike>'.$model->due_amount.'</strike> '. $request_params->due_amount : $model->due_amount  ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= isset($request_params->description) ? '<strike>'.$model->description.'</strike> '. $request_params->description : $model->description  ?></div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('effective_date') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->effective_date ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('end_date') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text">
                <?= isset($request_params->end_date) ? '<strike>'.$model->end_date.'</strike> '. $request_params->end_date : $model->end_date  ?>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('created_by') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->createdBy) ? ucwords($model->createdBy->fullname) . ' (' . $model->createdBy->username . ')' : '--'; ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('priority') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->priority ?></div>
        </div>
    </div>

    <?php
    foreach ($request_params as $key => $value) {
        if($key == 'penalty')
            continue;

        $model->$key = $value;
    }
    ?>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label">Fee Type</div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= $model->recurrent ? 'Recurrent' : 'One-Time' ?></div>
        </div>

        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->recurrent ? 'Apply Every' : '' ?></div>
            <?php

            $applyFrequencyHighlight = false;
            if(isset($request_params->apply_frequency) || isset($request_params->apply_frequency_units)) {
                if(isset($request_params->apply_frequency))
                    $model->apply_frequency = $request_params->apply_frequency;

                if(isset($request_params->apply_frequency_unit))
                    $model->apply_frequency_unit = $request_params->apply_frequency_unit;

                $applyFrequencyHighlight = true; //Set to true to highlight in view
            }
            $applyEvery = $model->apply_frequency_unit == 'MONTHLY' ? 'Months' : ($model->apply_frequency_unit == 'WEEKLY' ? 'Weeks' : 'Days');
            ?>
            <div class="col-lg-6 col-xs-6 profile-text alert-warning"><?= $model->recurrent ? $model->apply_frequency . ' ' . $applyEvery : '' ?></div>
        </div>
    </div>

    <?php if ($model->recurrent) : ?>
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                <div class="col-lg-6 col-xs-6 profile-label">Last Apply Date</div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->last_apply_date ? $model->last_apply_date : '-' ?></div>
            </div>

            <div class="col-lg-6 col-sm-6 col-xs-12 no-padding ">
                <div class="col-lg-6 col-xs-6 profile-label">Next Apply Date</div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->next_apply_date ? $model->next_apply_date : '' ?></div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($model->has_penalty) : ?>
        <?= $this->render('_edit_penalty_view', ['penaltyModel'=>$penaltyModel, 'model'=>$model, 'request_params'=>$request_params]); ?>
    <?php endif; ?>



</div>
<p><br></p>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-12 box no-padding">
            <div class="col-md-3 table-responsive no-padding">
                <?php
                echo "<table class ='table-bordered table'>";
                echo "<tr style='background-color:#F9F9F9'>";
                echo "<th class='text-center'> Selected Classes </th>";
                echo "</tr>";
                if (!empty($class_fee)) {
                    foreach ($class_fee as $k => $v) {
                        echo "<tr onclick=\"
                $.urlParam = function(name){
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    if (results==null){
                       return null;
                    }
                    else{
                       return results[1] || 0;
                    }
                }
                var urlval = $.urlParam('id');
                $('.tr_active').removeClass('tr_active');
                $(this).addClass('tr_active');
                $.get( '../fees-due/lists?clid=" . $v['cid'] . "&fid='+urlval, function( data ) {
                $( 'div#exempt' ).html(data);
                $(this).removeAttr('style');
            });\"
        >";
                        echo "<td class='text-center'>" . $v['cdes'] . "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td class='text-center'>No Class Assigned this fee</td>";
                    echo "</tr>";

                }
                echo "</table>";
                ?>

                <div><?= LinkPager::widget(['pagination' => $cl_pages]); ?></div>
            </div>

            <?php Pjax::begin(); ?>
            <div id="exempt" class="col-md-9 table-responsive " style="padding:0 0 0 15px;">
                <?php
                echo "<table id='exempted' class ='table-bordered table table-striped'>";
                echo "<tr>";
                echo "<th class='text-center'> Sr.No </th>";
                echo "<th class='text-center'> Payment code </th>";
                echo "<th class='text-center'> Class </th>";
                echo "<th class='text-center'> Exempted Students </th>";
                echo "</tr>";
                if (!empty($student_exemp)) {
                    $i = 1;
                    if (isset($_GET['page'])) {
                        $i = (($_GET['page'] - 1) * 20) + 1;
                    }
                    foreach ($student_exemp as $k => $v) {
                        echo "<tr>";
                        echo "<td class='text-center'>" . $i . "</td>";
                        echo "<td class='text-center'>" . $v['sn'] . "</td>";
                        echo "<td class='text-center'>" . $v['cl'] . "</td>";
                        echo "<td class='text-center'>" . $v['fn'] . " " . $v['mn'] . " " . $v['ln'] . "</td>";
                        echo "</tr>";
                        $i++;
                    }
                } else {
                    echo "<tr>";
                    echo "<td class='text-center'></td>";
                    echo "<td class='text-center'>No exempted students for this fee</td>";
                    echo "<td class='text-center'></td>";
                    echo "<td class='text-center'></td>";
                    echo "</tr>";

                }
                echo "</table>";
                ?>
                <div><?= LinkPager::widget(['pagination' => $st_pages]); ?></div>
            </div>
            <?php Pjax::end(); ?>

        </div>
    </div>


</div>
    <hr class="l_normal"><br>
<?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord'=>$workflowRecord]); ?>


<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>


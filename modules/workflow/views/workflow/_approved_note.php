<?php

use app\models\User;

?>
<div class="form-group">
<div class="alert alert-info">
    <i class="fa fa-check-circle"></i> Approved on: <?= $workflowRecord['date_approved'] ?> by <?= User::findOne(['id' => $workflowRecord['approved_by']])->username ?>
</div>
</div>


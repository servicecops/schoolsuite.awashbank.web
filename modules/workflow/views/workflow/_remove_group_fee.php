<?php use barcode\barcode\BarcodeGenerator;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-md-12">
    <?php if (\Yii::$app->session->hasFlash('workflowInfo')) : ?>
        <div class="notify notify-success">
            <a href="javascript:;" class='close-notify' data-flash='workflowInfo'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowInfo'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('workflowError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:;" class='close-notify' data-flash='workflowError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('workflowError'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="letter">
        <div class="col-sm-12 col-xs-12 pull-right" style="text-align:center;">
            <h3 style="text-decoration: underline"> REMOVE FEE FROM GROUP</h3>

        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <!-- <div  style="margin-top:-150px; margin-left:160px;"> -->
                    <div class="col-sm-12 no-padding" style="margin-top:-20px;">
                        <h3> <?= !$model->school_id ? $model->school_id : '<span style="text-align: center">' . $model->school->school_name . '</span>'; ?></h3>
                        <h5><b>Group Name: </b><?= !$model->group_name ? $model->group_name : $model->group_name ?></h5>
                        <h5><b> Group Description: </b></>&nbsp; <?= $model->group_description; ?></h4>
                        <b>Date Created: </b><?= ($model->date_created) ? $model->date_created : "--" ?><br>

                        <div id="stu_barcode"></div>
                    </div>
                </div>
            </div>
        </div>

        <hr class="l_normal">
        <br>
        <?= $this->render('footer', ['actionModel' => $actionModel, 'workflowRecord' => $workflowRecord]); ?>


        <div class="col-sm-12 col-xs-12">
            <div class="col-md-8 no-pading">
                <?= $this->render('_action_group_members',
                    [
                        'members' => $details,
                    ]);
                ?>
            </div>

        </div>
        <div style="clear: both"></div>
    </div>

</div>
<?php
$script = <<< JS
    $(document).ready(function(){
      $('.close-notify').click(function(){
          $(".notify").hide();
      });
   });
JS;
$this->registerJs($script);
?>

<?php

namespace app\modules\disciplinary\models;

use app\models\User;
use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class DisciplinarySearch extends Disciplinary
{
    public $modelSearch, $schsearch, $pc_rgnumber, $school_student_registration_number;
    public $date_from, $date_to, $isstudent;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['modelSearch','schsearch'], 'trim'],
            [['date_created', 'created_by','date_from', 'date_to','modelSearch','schsearch','school_student_registration_number','isstudent'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['grades','class_id','school_id','min_mark','max_mark'],
            'fileName' => 'Grades',
            'title' => 'student Datasheet',
            'exportFile' => '/disciplinary/exportPdfExcel',
        ];

        return $data;
    }



    public function search($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        Yii::trace($this->modelSearch);
        Yii::trace($this->is_student);
        $query = new Query();
        $query->select(['di.id', 'di.student_code','di.staff_id', 'di.case', 'di.is_student','di.punishment_served',])
            ->from('core_disciplinary di');

        $query->orFilterWhere(['di.student_code' => $this->modelSearch])
            ->orFilterWhere(['di.staff_id' => $this->modelSearch])
            ->orFilterWhere(['di.is_student' => $this->isstudent]);




        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'staff_id', 'case', 'is_student', 'punishment_served'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('di.date_created');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }
    public function getStaffName()
    {
        return $this->hasOne(User::className(), ['id' => 'staff_id']);
    }

}

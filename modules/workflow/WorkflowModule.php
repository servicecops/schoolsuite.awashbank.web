<?php
namespace app\modules\workflow;

class WorkflowModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\workflow\controllers';
    public function init() {
        parent::init();
    }
}

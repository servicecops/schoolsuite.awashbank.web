<?php

namespace app\modules\banks\models;
use app\modules\banks\models\BankDetails;

use Yii;

/**
 * This is the model class for table "mobile_access_users".
 *
 * @property integer $id
 * @property string $phone_number
 * @property string $name
 * @property string $date_created
 * @property boolean $active
 * @property integer $bank_id
 * @property integer $school_id
 * @property string $user_type
 * @property string $device_id
 * @property string $encryption_key
 */
class MobileAccessUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_access_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone_number', 'device_id'], 'string'],
            [['date_created'], 'safe'],
            [['active'], 'boolean'],
            [['bank_id', 'school_id'], 'integer'],
            [['name', 'user_type', 'encryption_key'], 'string', 'max' => 255],
            [['phone_number'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone_number' => 'Phone Number',
            'name' => 'Name',
            'date_created' => 'Date Created',
            'active' => 'Active',
            'bank_id' => 'Bank ID',
            'school_id' => 'School ID',
            'user_type' => 'User Type',
            'device_id' => 'Device ID',
            'encryption_key' => 'Encryption Key',
        ];
    }

    public function getBank()
    {
        return $this->hasOne(BankDetails::className(), ['id' => 'bank_id']);
    }
}

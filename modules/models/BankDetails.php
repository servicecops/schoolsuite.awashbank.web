<?php

namespace app\modules\banks\models;

use Yii;

/**
 * This is the model class for table "nominated_bank_details".
 *
 * @property integer $id
 * @property string $bank_name
 * @property string $bank_address
 * @property string $bank_code
 * @property string $contact_email
 * @property string $contact_phone
 * @property string $date_created
 * @property integer $bank_logo
 */
class BankDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nominated_bank_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'bank_address', 'bank_code', 'contact_email', 'contact_phone', 'date_created'], 'string'],
            [['bank_logo'], 'integer'],
            [['contact_email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_code'=>'Bank Code',
            'bank_name' => 'Bank Name',
            'bank_logo'=>'Bank Logo',
            'bank_address' => 'Bank Address',
            'contact_email' => 'Contact Email',
            'contact_phone' => 'Contact Phone',
            'date_created' => 'Date Created',
        ];
    }

    public static function findAllNominatedBanks() {
        return self::find()->orderBy(['bank_name'=>SORT_ASC])->all();
    }
}

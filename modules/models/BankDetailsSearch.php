<?php

namespace app\modules\banks\models;

use app\models\PaymentChannels;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\banks\models\BankDetails;
use yii\data\Sort;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * BankDetailsSearch represents the model behind the search form about `app\modules\banks\models\BankDetails`.
 */
class BankDetailsSearch extends BankDetails
{
    /**
     * @inheritdoc
     */
    public $date_from, $channel, $date_to, $school, $settled;

    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['bank_name', 'bank_address', 'contact_email', 'contact_phone', 'date_created', 'date_from', 'date_to', 'channel', 'school', 'settled'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BankDetails::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'bank_address', $this->bank_address])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'date_created', $this->date_created]);

        return $dataProvider;
    }

    public function searchMobileTrans($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $date_query = $bank_query = '';

        if ($this->date_from) {
            $date_query = "where (date_created BETWEEN '" . date('Y-m-d', strtotime($this->date_from)) . "' AND '" . $date_to . "')";
        }

        if (Yii::$app->user->can('bank_user')) {
            $fString = $date_query == '' ? ' where ' : ' and ';
            $bank_query = $fString . " settlement_bank_id = " . Yii::$app->user->identity->bank_id;
        }
        $query = (new Query())->select(['pr.date_created', 'pch.channel_name', 'pch.payment_channel_logo', 'pr.channel_trans_id', 'pr.channel_depositor_phone', 'pr.amount', 'si.payment_code', 'ast.student_name', 'ast.school_name', 'si.school_student_registration_number', 'cls.class_code', 'pr.reciept_number', 'ast.settled', 'ast.date_last_settled', 'ast.settlement_reference'])
            ->from('payments_received pr')
            ->innerJoin('student_information si', 'si.id=pr.student_id')
            ->innerJoin("(select * from auto_settlement_requests " . $date_query . " " . $bank_query . ") ast", "ast.payment_id=pr.id")
            ->innerJoin('payment_channels pch', 'pch.id=pr.payment_channel')
            ->innerJoin('institution_student_classes cls', 'cls.id=si.student_class')
            ->andFilterWhere(['pr.school_id' => $this->school])
            ->andFilterWhere(['ast.settled' => $this->settled])
            ->andFilterWhere(['pch.id' => $this->channel]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'channel_name', 'channel_trans_id', 'channel_depositor_phone', 'amount', 'payment_code', 'school_name', 'student_name', 'school_student_registration_number', 'class_code', 'reciept_number', 'settled', 'date_last_settled', 'settlement_reference'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pr.date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData']['data'] = $query;
        $_SESSION['findData']['key'] = 'MOBILE_TRANS';
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db2), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

    public function searchUnebMno($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = (new Query())->select(['pr.date_created', 'pch.channel_name', 'pch.payment_channel_logo', 'pr.channel_trans_id', 'pr.channel_depositor_phone', 'pr.amount', 'pr.center_name', 'pr.center_number', 'pr.center_level', 'schoolpay_receipt_number', 'pr.bank_settlement_date', 'pr.invoice_number', 'pr.bank_settlement_reference', 'pr.reconciled'])
            ->from('uneb_payments_received pr')
            ->innerJoin('payment_channels pch', 'pch.id=pr.payment_channel')
            ->andFilterWhere(['between', 'pr.date_created', $this->date_from, $date_to])
            ->andFilterWhere(['pr.reconciled' => $this->settled])
            ->andFilterWhere(['pch.id' => $this->channel])
            ->andFilterWhere(['ilike', 'pr.center_name', $this->school]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'channel_name', 'channel_trans_id', 'channel_depositor_phone', 'amount', 'invoice_number', 'center_name', 'center_number', 'center_level', 'schoolpay_receipt_number', 'reconciled', 'bank_settlement_date', 'bank_settlement_reference'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pr.date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData']['data'] = $query;
        $_SESSION['findData']['key'] = 'UNEB_MNO';
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db2), 'pages' => $pages, 'sort' => $sort];

    }

    public function searchAll($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time() + 86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $this->load($params);
        $query = (new Query())->select(['pr.date_created', 'pch.channel_name', 'pch.payment_channel_logo', 'pr.channel_trans_id', 'pr.channel_depositor_phone', 'pr.amount', 'si.payment_code', 'si.first_name', 'si.middle_name', 'si.last_name', 'sch.school_name', 'si.school_student_registration_number', 'cls.class_code', 'pr.reciept_number'])
            ->from('payments_received pr')
            ->innerJoin('school_information sch', 'sch.id=pr.school_id')
            ->innerJoin('payment_channels pch', 'pch.id=pr.payment_channel')
            ->leftJoin('auto_settlement_requests ast', 'pr.id=ast.payment_id')
            ->innerJoin('student_information si', 'si.id=pr.student_id')
            ->innerJoin('institution_student_classes cls', 'cls.id=si.student_class')
            ->andFilterWhere(['between', 'pr.date_created', date('Y-m-d', strtotime($this->date_from)), $date_to])
            ->andFilterWhere(['pr.school_id' => $this->school])
            ->andFilterWhere(['pr.payment_channel' => $this->channel])
            ->andFilterWhere(['pr.reversed' => false])
            ->andFilterWhere(['pr.reversal' => false]);
        if (Yii::$app->user->can('bank_user')) {
            //For each bank, return transactions for their internal channels
            //Plus transactions settled with them
            //Get the bank for its code
            $bank = BankDetails::findOne(Yii::$app->user->identity->bank_id);
            //Select the bank config
            $bankConfig = Yii::$app->params['bankTransactionsConfig'][$bank->bank_code];
            if (empty($bankConfig)) throw new ForbiddenHttpException('Bank config for ' . $bank->bank_code . ' is not defined.');

            $bankInterChannelCodes = $bankConfig['internalChannelCodes']; //An array of channel codes

            $internalPaymentChannels = PaymentChannels::find()->where(['in', 'channel_code', $bankInterChannelCodes])->all();
            $internalPaymentChannelIds = ArrayHelper::getColumn($internalPaymentChannels, 'id');

            $query->andFilterWhere(['or',
                ['in', 'pr.payment_channel', $internalPaymentChannelIds], //From internal codes
                ['ast.settlement_bank_id'=>$bank->id]]); //settled at bank

        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'channel_name', 'channel_trans_id', 'channel_depositor_phone', 'amount', 'payment_code', 'student_name', 'school_name', 'first_name', 'middle_name', 'last_name', 'school_student_registration_number', 'class_code', 'reciept_number',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pr.date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData']['data'] = $query;
        $_SESSION['findData']['key'] = 'SEARCH_ALL';
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db2), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }

    public static function getExportQuery()
    {
        $data = [];
        $key = isset($_SESSION['findData']['key']) ? $_SESSION['findData']['key'] : '';
        if ($key == 'MOBILE_TRANS') {
            $data = [
                'data' => $_SESSION['findData']['data'],
                'fileName' => 'mno_transactions',
                'title' => 'MNO Transactions',
                'exportFile' => '@app/modules/banks/views/reports/export_mno_trans_PdfExcel',
            ];
        } else if ($key == 'SEARCH_ALL') {
            $data = [
                'data' => $_SESSION['findData']['data'],
                'fileName' => 'all_transactions',
                'title' => 'All Transactions',
                'exportFile' => '@app/modules/banks/views/reports/all_exportPdfExcel',
            ];
        } else if ($key == 'UNEB_MNO') {
            $data = [
                'data' => $_SESSION['findData']['data'],
                'fileName' => 'uneb_mobile_transactions',
                'title' => 'UNEB Mobile Transactions',
                'exportFile' => '@app/modules/banks/views/reports/uneb_mno_exportPdfExcel',
            ];
        }
        return $data;
    }


}

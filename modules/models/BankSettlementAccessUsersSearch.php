<?php

namespace app\modules\banks\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\banks\models\BankSettlementAccessUsers;
if (!Yii::$app->session->isActive) session_start();

/**
 * BankSettlementAccessUsersSearch represents the model behind the search form about `app\modules\banks\models\BankSettlementAccessUsers`.
 */
class BankSettlementAccessUsersSearch extends BankSettlementAccessUsers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bank_id', 'batch_process_size'], 'integer'],
            [['access_code', 'access_account_name', 'certificate', 'last_poll_date', 'last_push_date'], 'safe'],
            [['active', 'newest_first'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        
        $query->select(['id', 'access_code', 'access_account_name', 'active', 'certificate', 'bank_id', 'last_poll_date', 'last_push_date', 'batch_process_size', 'newest_first'])
            ->from('bank_settlement_access_users');
        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'bank_id' => $this->bank_id,
            'last_poll_date' => $this->last_poll_date,
            'last_push_date' => $this->last_push_date,
            'batch_process_size' => $this->batch_process_size,
            'newest_first' => $this->newest_first,
        ]);

        $query->andFilterWhere(['ilike', 'access_code', $this->access_code])
            ->andFilterWhere(['ilike', 'access_account_name', $this->access_account_name])
            ->andFilterWhere(['ilike', 'certificate', $this->certificate]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'access_code', 'access_account_name', 'active', 'certificate', 'bank_id', 'last_poll_date', 'last_push_date', 'batch_process_size', 'newest_first'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Bank Settlement Access Users', $query, $this->getVisibleCols(),  'bank_settlement_access_users');
        return ['query'=>$query->all(Yii::$app->db2), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Bank Settlement Access Users', 'search_col_w'=>[4, 5, 3]];
    }


    public function getVisibleCols()
    {
        return [
            'access_code' => ['label'=>'Access Code'],
            'access_account_name' => ['label'=>'Access Account Name'],
            'active' => ['type'=>'number', 'label'=>'Active'],
            'last_poll_date' => ['type'=>'date', 'label'=>'Last Poll Date'],
            'last_push_date' => ['type'=>'date', 'label'=>'Last Push Date'],
            'batch_process_size' => ['type'=>'number', 'label'=>'Batch Process Size'],
            'newest_first' => ['type'=>'number', 'label'=>'Newest First'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],
                    ['url'=>'update', 'title'=>'Update', 'icon'=>'glyphicon glyphicon-edit']
                ]
            ]
        ]; 
    }

    public function getSearchCols()
    {
        return [
//            'id' => ['type'=>'number', 'label'=>'Id'],
            'access_code' => ['label'=>'Access Code', 'width'=>9],
//            'access_account_name' => ['label'=>'Access Account Name'],
//            'active' => ['type'=>'number', 'label'=>'Active'],
//            'certificate' => ['label'=>'Certificate'],
//            'bank_id' => ['type'=>'number', 'label'=>'Bank Id'],
//            'last_poll_date' => ['type'=>'date', 'label'=>'Last Poll Date'],
//            'last_push_date' => ['type'=>'date', 'label'=>'Last Push Date'],
//            'batch_process_size' => ['type'=>'number', 'label'=>'Batch Process Size'],
//            'newest_first' => ['type'=>'number', 'label'=>'Newest First'],
        ]; 
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

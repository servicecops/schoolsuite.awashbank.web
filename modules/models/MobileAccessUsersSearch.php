<?php

namespace app\modules\banks\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\banks\models\MobileAccessUsers;
use yii\db\Query;
use yii\data\Pagination;
use yii\data\Sort;

/**
 * MobileAccessUsersSearch represents the model behind the search form about `app\modules\banks\models\MobileAccessUsers`.
 */
class MobileAccessUsersSearch extends MobileAccessUsers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bank_id', 'school_id'], 'integer'],
            [['phone_number', 'name', 'date_created', 'user_type', 'device_id', 'encryption_key'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['usr.id', 'usr.name', 'usr.device_id', 'usr.user_type', 'usr.phone_number', 'bnk.bank_name'])
            ->from('mobile_access_users usr')
            ->innerJoin('nominated_bank_details bnk', 'bnk.id=usr.bank_id')
            ->andFilterWhere(['bank_id' => $this->bank_id]);

        $query->andFilterWhere(['ilike', 'phone_number', $this->phone_number])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'user_type', $this->user_type])
            ->andFilterWhere(['ilike', 'device_id', $this->device_id]);

            $pages = new Pagination(['totalCount' => $query->count()]);
            $sort = new Sort([
                'attributes' => [
                    'name', 'device_id', 'user_type', 'phone_number', 'bank_name'
                    ],
            ]);
            ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('usr.date_created DESC');
            unset($_SESSION['findData']);
            $_SESSION['findData'] = $query;
            $query->offset($pages->offset)->limit($pages->limit);
            return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public static function getExportQuery() 
    {
        $data = [
                'data'=>$_SESSION['findData'],
                'labels'=>['name', 'device_id', 'user_type', 'phone_number', 'bank_name'],
                'fileName'=>'mobile_access_users', 
                'title'=>'Mobile Users',
                'exportFile'=>'@app/modules/banks/views/mobile-access/exportPdfExcel',
            ];
        return $data;
    }
}

<?php

namespace app\modules\banks\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bank_settlement_access_users".
 *
 * @property integer $id
 * @property string $access_code
 * @property string $access_account_name
 * @property boolean $active
 * @property string $certificate
 * @property integer $bank_id
 * @property string $last_poll_date
 * @property string $last_push_date
 * @property integer $batch_process_size
 * @property boolean $newest_first
 */
class BankSettlementAccessUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_settlement_access_users';
    }

    public function setDefaultValues()
    {
        if(!$this->id) $this->active = true;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_code', 'access_account_name', 'bank_id', 'batch_process_size'], 'required'],
            [['active', 'newest_first'], 'boolean'],
            [['certificate'], 'string'],
            [['bank_id', 'batch_process_size'], 'integer'],
            [['last_poll_date', 'last_push_date'], 'safe'],
            [['access_code', 'access_account_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'access_code' => 'Access Code',
            'access_account_name' => 'Access Account Name',
            'active' => 'Active',
            'certificate' => 'Certificate',
            'bank_id' => 'Bank',
            'last_poll_date' => 'Last Poll Date',
            'last_push_date' => 'Last Push Date',
            'batch_process_size' => 'Batch Process Size',
            'newest_first' => 'Newest First',
        ];
    }
    public function formFields()
    {
        $banks = ArrayHelper::map(BankDetails::find()->all(), 'id', 'bank_name');
        $banks = [''=>'Select Banks']+$banks;
        return [
            [
                'heading'=>'New Bank Settlement Access Users',
                'header_icon'=>'fa fa-plus',
                'col_n'=>2,
                'fields'=> [
                    'access_code' => ['label'=>'Access Code'],
                    'access_account_name' => ['label'=>'Access Account Name'],
                    'bank_id' => ['type'=>'dropdown', 'options'=>$banks, 'Select Bank'],
                    'batch_process_size' => ['type'=>'number', 'label'=>'Batch Process Size'],
                    'certificate' => ['type'=>'textarea', 'label'=>'Certificate'],
                    'active' => ['type'=>'checkbox', 'label'=>'Active'],
                    'newest_first' => ['type'=>'checkbox', 'label'=>'Newest First'],
                ] 
            ]
        ];

    }

    public function getViewFields()
    {
        return [
            ['attribute'=>'id'],
            ['attribute'=>'access_code'],
            ['attribute'=>'access_account_name'],
            ['attribute'=>'active'],
            ['attribute'=>'certificate'],
            ['attribute'=>'bank_id'],
            ['attribute'=>'last_poll_date'],
            ['attribute'=>'last_push_date'],
            ['attribute'=>'batch_process_size'],
            ['attribute'=>'newest_first'],
        ];
    }

    public  function getTitle()
    {
        return $this->isNewRecord ? "Bank Settlement Access Users" : $this->getViewTitle();
    }

    public function getViewTitle()
    {
        return $this->{self::primaryKey()[0]};
    }

}

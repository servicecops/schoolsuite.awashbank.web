<?php

namespace app\modules\banks\controllers;

use Yii;
use app\modules\banks\models\MobileAccessUsers;
use app\modules\banks\models\MobileAccessUsersSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MobileAccessController implements the CRUD actions for MobileAccessUsers model.
 */
class MobileAccessController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MobileAccessUsers models.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('manage_mobile_user')) {
            throw new ForbiddenHttpException('No permissions to Create Users.');
        }
        $request = Yii::$app->request;
        $searchModel = new MobileAccessUsersSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']];

        return ($request->isAjax) ? $this->renderAjax('index', $res) : $this->render('index', $res);
    }

    /**
     * Displays a single MobileAccessUsers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('manage_mobile_user')) {
            throw new ForbiddenHttpException('No permissions to Create Users.');
        }
        $request = Yii::$app->request;
        $res = ['model' => $this->findModel($id)];
        return ($request->isAjax) ? $this->renderAjax('view', $res) : $this->render('view', $res);
    }

    /**
     * Creates a new MobileAccessUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('manage_mobile_user')) {
            throw new ForbiddenHttpException('No permissions to Create Users.');
        }
        $request = Yii::$app->request;
        $model = new MobileAccessUsers();
        $model->active = true;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);
    }

    /**
     * Updates an existing MobileAccessUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('manage_mobile_user')) {
            throw new ForbiddenHttpException('No permissions to Create Users.');
        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->active = true;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);
    }

    /**
     * Deletes an existing MobileAccessUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('manage_mobile_user')) {
            throw new ForbiddenHttpException('No permissions to Create Users.');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MobileAccessUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MobileAccessUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MobileAccessUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace app\modules\banks\controllers;

use Yii;
use yii\web\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\banks\models\Reconciliation;

if (!Yii::$app->session->isActive){
          session_start();  
      }

/**
 * BankDetailsController implements the CRUD actions for BankDetails model.
 */
class ReconciliationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BankDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $req = Yii::$app->request;
        $searchModel = new Reconciliation();
        $data = null;
        $hasData =false;
        if ($searchModel->load(Yii::$app->request->post()) && $searchModel->validate()) {
            $data = $searchModel->bankDestination();
            $hasData = true;
            $_SESSION['br']['model'] = $searchModel;
        }
        return ($req->isAjax) ? $this->renderAjax('index', [ 'data'=>$data, 'searchModel'=>$searchModel, 'hasData'=>$hasData 
            ]) : $this->render('index', ['data'=>$data, 'searchModel'=>$searchModel, 'hasData'=>$hasData ]);
    }

    public function actionChnSch()
    {
        $req = Yii::$app->request;
        $searchModel = new Reconciliation();
        $data = null;
        $hasData =false;
        if ($searchModel->load(Yii::$app->request->post()) && $searchModel->validate()) {
            $data = $searchModel->chnSch();
            $hasData = true;
            $_SESSION['br']['model'] = $searchModel;
        }
        return ($req->isAjax) ? $this->renderAjax('chn_sch', [ 'data'=>$data, 'searchModel'=>$searchModel, 'hasData'=>$hasData 
            ]) : $this->render('chn_sch', ['data'=>$data, 'searchModel'=>$searchModel, 'hasData'=>$hasData ]);
    }

    public function actionSchChannel()
    {
        $req = Yii::$app->request;
        $searchModel = new Reconciliation();
        $data = null;
        $hasData =false;
        if ($searchModel->load(Yii::$app->request->post()) && $searchModel->validate()) {
            $data = $searchModel->schChannel();
            $hasData = true;
            $_SESSION['br']['model'] = $searchModel;
        }
        return ($req->isAjax) ? $this->renderAjax('sch_channel', [ 'data'=>$data, 'searchModel'=>$searchModel, 'hasData'=>$hasData 
            ]) : $this->render('sch_channel', ['data'=>$data, 'searchModel'=>$searchModel, 'hasData'=>$hasData ]);
    }

}
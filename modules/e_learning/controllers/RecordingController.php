<?php


namespace app\modules\e_learning\controllers;


use app\components\TeleconferingHelper;
use app\modules\banks\models\BankAccountDetails;
use app\modules\banks\models\SelfEnrolledBankAccountDetails;
use app\modules\e_learning\models\RecordedSessions;
use app\modules\logs\models\Logs;

use app\modules\online_reg\models\CoreSchoolSelfRegistration;
use app\modules\payment_plan\models\PaymentPlan;
use app\modules\paymentscore\models\ExternalPaymentSources;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\PaymentsReceivedSearch;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use BigBlueButton\Parameters\GetRecordingsParameters;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

//use app\models\FeesDue;
//use app\models\FeeClass;
//use app\models\Classes;
//use app\models\FeesDueSearch;
//use app\modules\logs\models\Logs;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class RecordingController extends Controller
{


    public function beforeAction($action)
    {


//        if (Yii::$app->user->isGuest && Yii::$app->controller->action->id != "login") {
//
//            Yii::$app->user->loginRequired();
//
//        }

//something code right here if user valid

        return true;

    }


    //get recordings
    public function actionGetRecordings(){
        $this->layout = 'loginlayout';

        $bbb=TeleconferingHelper::initialiseBBB();

        $recordingParams = new GetRecordingsParameters();
        // $recordingParams->setRecordID($recordId); // omit to get a list of all recordings
        $recordingParams->setState('any');

        $response = $bbb->getRecordings($recordingParams);

        if (!$response->success()) {
            // handle error
        }

        $records = $response->getRecords();
        // Yii::trace($records);

        foreach($records as $record){
            Yii::trace($record->getPlaybackUrl());
            //check if it exists


            $record->getMeetingId();
            $record->getName();
            //  $record->getParticipantCount();
            $record->getPlaybackLength();
            $record->getPlaybackLength();
            $record->getRecordId();
            $record->getStartTime();
            $existing = RecordedSessions::findOne(['meeting_id'=>$record->getMeetingId()]);
            if($existing){

            }else {
                $recorded = new RecordedSessions();


                $recorded->meeting_id = $record->getMeetingId();
                $recorded->meeting_name = $record->getName();
                //$recorded->participant_count=$record->getParticipantCount();
                $recorded->play_back_length = $record->getPlaybackLength();
                $recorded->record_id = $record->getRecordId();
                $recorded->play_back_url = $record->getPlaybackUrl();
                //$recorded->start_time=$record->getStartTime();
                $recorded->save(false);
            }
        }

//        Logs::logEvent("Saved new recordings: ", null);
//
//        $lastcheck = new LastRecordedSessionsCheck();
//        $lastcheck->was_checked= true;
//        $lastcheck->save(false);

    }

}
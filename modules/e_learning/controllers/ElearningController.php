<?php

namespace app\modules\e_learning\controllers;


use app\components\TeleconferingHelper;
use app\modules\e_learning\models\AttendanceLogs;
use app\modules\e_learning\models\Elearning;
use app\modules\e_learning\models\ElearningSearch;
use app\modules\e_learning\models\LastRecordedSessionsCheck;
use app\modules\e_learning\models\RecordedSessions;
use app\modules\e_learning\models\StudentElearning;
use app\modules\e_learning\models\StudentElearningSearch;
use app\modules\e_learning\models\StudentUploads;
use app\modules\e_learning\models\TeacherUploads;
use app\modules\e_learning\models\Teleconferencing;
use app\modules\e_learning\models\TeleconferencingSearch;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreSubjectSearch;
use BigBlueButton\Parameters\CreateMeetingParameters;
use BigBlueButton\Parameters\GetRecordingsParameters;
use BigBlueButton\Parameters\JoinMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters;

use Ramsey\Uuid\Uuid;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

;


/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class ElearningController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    //list meeting
    public function actionMeeting()
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('meeting') :
            $this->render('meeting');
    }

    /**
     * Lists all FeesDue models.
     * @return mixed
     */

    public function actionAllSubjectWork($subjectId)
    {
        if (Yii::$app->user->can('r_classwork') || !Yii::$app->user->can('non_student')) {
            $request = Yii::$app->request;
            $searchModel = new ElearningSearch();
            //   $studentModel = CoreStudent::find()->where(['web_user_id'=>Yii::$app->user->identity->getId()])->one();

            $data = $searchModel->search($subjectId);
            // Yii::trace($data);
            $data = $data['data'];
            Yii::trace($data);
            $res = ['data' => $data, 'searchModel' => $searchModel];
            return ($request->isAjax) ? $this->renderAjax('index', $data) :
                $this->render('index', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }

    //all teachers subject
    public function actionAllClassSubjectWork($subjectId)
    {
        if (Yii::$app->user->can('r_classwork') || !Yii::$app->user->can('non_student')) {
            $request = Yii::$app->request;
            $searchModel = new ElearningSearch();
            // $studentModel = CoreStudent::find()->where(['id'=>Yii::$app->user->identity->getId()])->one();

            $data = $searchModel->searchTeacher($subjectId);
            // Yii::trace($data);
            $data = $data['data'];
            Yii::trace($data);
            $res = ['data' => $data, 'searchModel' => $searchModel];
            return ($request->isAjax) ? $this->renderAjax('index', $res) :
                $this->render('index', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to view schools.');
        }
    }

    /*
     * View/Download file
     */
    public function actionViewFile($id)
    {
        $response = Yii::$app->response;
        $model = TeacherUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'mp3', 'mp4']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->headers->set('Content-type', $ext);
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);
        return $response;
    }

    /*
     * StudentnView/Download file
     */
    public function actionStudentViewFile($id)
    {
        $response = Yii::$app->response;
        $model = StudentUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'mp3', 'mp4']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->headers->set('Content-type', $ext);
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);
        return $response;
    }
    /*
     * View Video
     */

    /*
  * View/Download file
  */
    public function actionViewVideo($id)
    {
        $request = Yii::$app->request;
        $model = TeacherUploads::find()->where(['id' => $id])->limit(1)->one();

        Yii::trace($model);
        $path = $model->file_path;
        $name =$model->file_name;
        $res = ['path' => $path,'name'=>$name];
        return ($request->isAjax) ? $this->renderAjax('view_video', $res) :
            $this->render('view_video', $res);

//        $name = explode('.', $model->file_name);
//        $ext = $name[count($name) - 1];
//        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif','mp3','mp4']) ? 'inline;' : 'attachment;';
//        $response->format = \yii\web\Response::FORMAT_RAW;
//        $response->headers->set('Content-type', $ext);
//        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');
//
//        $response->content = base64_decode($model->file_data);
//        return $response;
    }



    public function actionStudentViewVideo($id)
    {
        $request = Yii::$app->request;
        $model = StudentUploads::find()->where(['id' => $id])->limit(1)->one();

        Yii::trace($model);
        $path = $model->file_path;
        $res = ['path' => $path];
        return ($request->isAjax) ? $this->renderAjax('view_video', $res) :
            $this->render('view_video', $res);


    }



    /**
     * delete teacher submission
     *
     */
    public function actionRemoveFile($id)
    {
        $model = TeacherUploads::find()->where(['id' => $id])->limit(1)->one();
        $submission_id = $model->submission_id;
        $model->delete();
        return $this->redirect(['/e_learning/elearning/view', 'id' => $submission_id]);
    }

    /**
     * delete student submission
     *
     */
    public function actionRemoveStudentFile($id)
    {
        $model = StudentUploads::find()->where(['id' => $id])->limit(1)->one();
        $submission_id = $model->submission_id;
        $model->delete();
        return $this->redirect(['/e_learning/elearning/view', 'id' => $submission_id]);
    }

    /**
     * Displays a single FeesDue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $searchModel = new Elearning();
        // $schoolData = $searchModel->viewModel(Yii::$app->request->queryParams);
        $schoolData = $searchModel->viewModelItems($id);
        $modeler = $schoolData['models'];
        $attributes = $schoolData['attributes'];
//        return $this->render('view', [
//            'model' => $modeler,
//            'attributes'=>$attributes
//        ]);

        Yii::trace($schoolData);

        $res = ['model' => $modeler,
            'attributes' => $attributes];
        $template = "view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


    }

    public function actionStudentView($id)
    {

        $request = Yii::$app->request;
        $searchModel = new StudentElearning();
        $schoolData = $searchModel->viewStudentModelItems($id);
        $modeler = $schoolData['models'];
        $attributes = $schoolData['attributes'];
//        return $this->render('student_file_view', [
//            'model' => $modeler,
//            'attributes'=>$attributes
//        ]);
        $res = ['model' => $modeler,
            'attributes' => $attributes];
        $template = "student_file_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


    }

    public function actionStudentIndex($subjectId)
    {
        //  if (Yii::$app->user->can('r_fd')) {
        $request = Yii::$app->request;
        $searchModel = new StudentElearningSearch();
        // $studentModel = CoreStudent::find()->where(['web_user_id'=>Yii::$app->user->identity->getId()])->one();

        Yii::trace(Yii::$app->user->identity->id);
        $data = $searchModel->studentSubmissions($subjectId);
        Yii::trace($data);
        $data = $data['data'];
        $res = ['data' => $data, 'searchModel' => $searchModel];
        return ($request->isAjax) ? $this->renderAjax('_student_index.php', $res) :
            $this->render('_student_index.php', $res);

    }

    public function actionAllStudentIndex($subjectId)
    {
        //  if (Yii::$app->user->can('r_fd')) {
        $request = Yii::$app->request;
        $searchModel = new StudentElearningSearch();
        // $studentModel = CoreStudent::find()->where(['id'=>Yii::$app->user->identity->getId()])->one();

        $data = $searchModel->allStudentSubmissions($subjectId);
        Yii::trace($data);
        $data = $data['data'];
        $res = ['data' => $data, 'searchModel' => $searchModel];
        return ($request->isAjax) ? $this->renderAjax('_student_index.php', $res) :
            $this->render('_student_index.php', $res);

    }

    /**
     *
     * /**
     * Updates an existing CoreStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {

                $model->teacher_id = Yii::$app->user->identity->getId();
                $model->uploads = UploadedFile::getInstances($model, 'uploads');
                $model->save(false);
                $model->saveUploads($id);
                $transaction->commit();


                return $this->redirect(['view', 'id' => $model->id]);
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//            $_REQUEST['viewError'] = $error;
            \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $error);
            Yii::trace('Error: ' . $error, 'UPDATE LESSON ROLLBACK');

        }

        return ($request->isAjax) ? $this->renderAjax('update', [
            'model' => $model,]) : $this->render('update', ['model' => $model,]);


    }

    /**
     *
     * /**
     * Updates an existing CoreStudent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateStudentUploads($id, $subjectId)
    {
        $request = Yii::$app->request;
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $model = $this->findStudentModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $post = Yii::$app->request->post();
//                $model->teacher_id = Yii::$app->user->identity->getId();
//
                Yii::trace($post);
//                $model->save(false);
//
//                if($post['uploads']){
//                    $model->uploads = UploadedFile::getInstances($model, 'uploads');
//
//                    $model->saveStudentUploadUpdate($id);
//
//                }
//                $transaction->commit();
//                return $this->redirect(['student_file_view', 'id' => $model->id]);

                $subject = Yii::$app->db->createCommand('SELECT school_id, class_id FROM core_subject where id =:subjectId')
                    ->bindValue(':subjectId', $subjectId)
                    ->queryAll();
                $subject = $subject[0];

                $model->school_id = $subject['school_id'];
                $model->class_id = $subject['class_id'];
                $model->subject_id = $subjectId;
                $model->student_id = Yii::$app->user->identity->getId();

                $model->save(false);
                if ($post['StudentElearning']['uploads']) {
                    $model->uploads = UploadedFile::getInstances($model, 'uploads');

                    $model->saveStudentUploads($id);

                }
                $transaction->commit();
                return $this->redirect(['student-view', 'id' => $model->id]);

            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace('Error: ' . $error, 'CREATE  FEES DUE ROLLBACK');

        }

//        return $this->render('student_upload_update', [
//            'model' => $model,'subjectId'=>$subjectId
//        ]);
        return ($request->isAjax) ? $this->renderAjax('student_upload_update', [
            'model' => $model, 'subjectId' => $subjectId]) : $this->render('student_upload_update', ['model' => $model, 'subjectId' => $subjectId]);

    }

    /**
     * Finds the FeesDue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Elearning the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Elearning::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findTeleconferencingModel($id)
    {
        if (($model = Teleconferencing::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findStudentModel($id)
    {
        if (($model = StudentElearning::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return mixeda
     * @var $penaltyModel InstitutionFeesDuePenalty
     * Creates a new FeesDue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreateLesson($subjectId)
    {
        if (Yii::$app->user->can('rw_lesson')) {
            $request = Yii::$app->request;
            $post = $request->post();

            $model = new Elearning();
            if ($model->load(Yii::$app->request->post())) {
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();
                Yii::trace($request->post());
                try {
                    $grade = Yii::$app->db->createCommand('SELECT school_id, class_id FROM core_subject where id =:subjectId')
                        ->bindValue(':subjectId', $subjectId)
                        ->queryAll();
                    $grade = $grade[0];

                    $model->school_id = $grade['school_id'];
                    $model->class_id = $grade['class_id'];
                    $model->subject_id = $subjectId;
                    $model->teacher_id = Yii::$app->user->identity->getId();
                    $model->uploads = UploadedFile::getInstances($model, 'uploads');

                    $model->save(false);
                    $model->saveUploads();
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $error);
                    Logs::logEvent("Error on creating lesson: ", $error, null);
                    Yii::trace('Error: ' . $error, 'CREATE  ROLLBACK');

                }

            }

            return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) :
                $this->render('create', ['model' => $model]);

        } else {
            throw new ForbiddenHttpException('No permissions to create a Lesson.');
        }


    }

    /**
     * @return mixed
     * @var $penaltyModel InstitutionFeesDuePenalty
     * Creates a new FeesDue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionStudentSubmission($id)
    {
        //if (Yii::$app->user->can('rw_fd')) {
        $request = Yii::$app->request;
        $post = $request->post();

        $model = new StudentElearning();

        if ($model->load(Yii::$app->request->post())) {
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            Yii::trace($request->post());
            try {
                $subject = Yii::$app->db->createCommand('SELECT id, school_id,subject_id, class_id FROM teacher_submissions where id =:id')
                    ->bindValue(':id', $id)
                    ->queryAll();
                $subject = $subject[0];
                $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->identity->getId()])->one();

                $model->school_id = $subject['school_id'];
                $model->class_id = $subject['class_id'];
                $model->subject_id = $subject['subject_id'];
                $model->teacher_submission_id = $id;
                $model->student_id = $studentModel->id;
                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                $model->save(false);
               $msg= $model->saveStudentUploads();
                Yii::trace($msg);
                $transaction->commit();
                return $this->redirect(['student-view', 'id' => $model->id]);
            } catch (\Exception $e) {
                Yii::trace( $e);
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                $_REQUEST['viewError'] = $error;
                \Yii::$app->session->setFlash('viewError', "<i class='fa fa-remove'></i>&nbsp " . $error);
                Yii::trace('Error: ' . $error, 'CREATE  LESSON ROLLBACK');

            }

        }
        $res = ['model' => $model, 'id' => $id];

        return ($request->isAjax) ? $this->renderAjax('student_uploads') :
            $this->render('student_uploads', $res);

//        } else {
//            throw new ForbiddenHttpException('No permissions to create a FEE.');
//        }


    }


    /**
     * Deletes an existing FeesDue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        //  Logs::logEvent("updated Fees: " . $model->description, null, null);
        $model->delete();

        // return $this->redirect(['index']);
        return Yii::$app->runAction('/e_learning/elearning/menu');
    }


    public function actionStudentDelete($id)
    {
        $model = StudentElearning::findOne($id);
        //  Logs::logEvent("updated Fees: " . $model->description, null, null);
        $model->delete();

        // return $this->redirect(['index']);
        return Yii::$app->runAction('/e_learning/elearning/menu');
    }

    public function actionDisplaySubject($classId)
    {
        $request = Yii::$app->request;
        $searchModel = new ElearningSearch();
        $allData = $searchModel->searchSubject($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';
        $res = ['dataProvider' => $dataProvider,'requiresExport' => false,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionDisplayClass()
    {
        $request = Yii::$app->request;
        $searchModel = new Elearning();
        $allData = $searchModel->searchClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Class';

        $res = ['dataProvider' => $dataProvider,'requiresExport' => false,  'requiresExport' => false,'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionDisplayTeachersSubject($classId)
    {
        $request = Yii::$app->request;
        $searchModel = new ElearningSearch();
        $allData = $searchModel->searchTeachersSubject($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';

        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionDisplayTeachersClass()
    {
        $request = Yii::$app->request;
        $searchModel = new Elearning();
        $allData = $searchModel->searchTeachersClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];

        $title = 'Select Class';

        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionDisplayStudentSubject($classId)
    {
        $request = Yii::$app->request;
        $searchModel = new StudentElearningSearch();
        $allData = $searchModel->searchStudentSubject($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';

        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionDisplayStudentClass()
    {
        $request = Yii::$app->request;
        $searchModel = new StudentElearning();
        $allData = $searchModel->searchStudentClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Class';

        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        // return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    public function actionStudentSubject()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $allData = $searchModel->searchStudentSubject(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';

        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    public function actionStudentViewSubmission($subjectId)
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $allData = $searchModel->viewStudentSubject(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';

        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        //return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionSingleStudentViewSubmission()
    {
        $request = Yii::$app->request;
        $searchModel = new CoreSubjectSearch();
        $allData = $searchModel->viewSingleStudentSubject(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];


        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionStudentClassSubject()
    {
        $request = Yii::$app->request;
        $searchModel = new StudentElearningSearch();
        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->identity->getId()])->one();

        $allData = $searchModel->searchStudentClassSubject($studentModel->class_id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $title = 'My teachers Submissions';
        $searchForm = $allData['searchForm'];
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //  return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }

    public function actionStudentClassSubjectSubmission()
    {
        $request = Yii::$app->request;
        $searchModel = new StudentElearningSearch();
        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->identity->getId()])->one();

        $allData = $searchModel->searchStudentClassSubjectSubmission($studentModel->class_id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $title = "My Submissions";
        $searchForm = $allData['searchForm'];
        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


        //return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionStudentViewSubjectSubmission()
    {
        $request = Yii::$app->request;
        $searchModel = new StudentElearningSearch();
        $studentModel = CoreStudent::find()->where(['web_user_id' => Yii::$app->user->identity->getId()])->one();

        $allData = $searchModel->StudentViewSubjectSubmission($studentModel->class_id);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = "My submissions";
        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        //return $this->render('@app/views/common/grid_view', $res);
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

    }


    public function actionDisplayTeachersClassSubmission()
    {
        $request = Yii::$app->request;
        $searchModel = new Elearning();
        $allData = $searchModel->searchTeachersClassSubmission(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Class';
        $res = ['dataProvider' => $dataProvider,'requiresExport' => false, 'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        //return $this->render('@app/views/common/grid_view', $res);
    }

    public function actionDisplayTeachersSubjectSubmission($classId)
    {
        $request = Yii::$app->request;
        $searchModel = new ElearningSearch();
        $allData = $searchModel->searchTeachersSubjectSubmission($classId);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Subject';

        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)
        $template = "@app/views/common/grid_view";

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);

        //return $this->render('@app/views/common/grid_view', $res);
    }


    public function actionClasses()
    {
        $request = Yii::$app->request;
        $searchModel = new TeleconferencingSearch();
        $allData = $searchModel->searchStdClass(Yii::$app->request->queryParams);
        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm = $allData['searchForm'];
        $title = 'Select Class';
        $res = ['dataProvider' => $dataProvider, 'requiresExport' => false,'title' => $title, 'columns' => $columns, 'searchModel' => $searchModel, 'searchForm' => $searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        $template = "@app/views/common/grid_view";
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);


        // return $this->render('@app/views/common/grid_view', $res);
    }

    public function newSearchModel()
    {
        // TODO: Implement newSearchModel() method.
        return new TeleconferencingSearch();
    }

    public function actionCreateMeeting($classId)
    {
        $model = new Teleconferencing();
        $request = Yii::$app->request;
        $model->record = true;
        if ($model->load(Yii::$app->request->post())) {

            $transaction = Teleconferencing::getDb()->beginTransaction();
            try {
                //$newId =$this->saveUser();
                $uuid = Uuid::uuid4();

                Yii::trace($uuid);
                Yii::trace($uuid->toString());
                $model->meeting_id = $uuid->toString();
                $model->created_by = Yii::$app->user->identity->getId();
                $model->class_id = $classId;

                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $model->save(false);

                Logs::logEvent("Successfully created session: ". $model->meeting_id, null);

                $transaction->commit();


                return $this->redirect(['create-meeting-api', 'id' => $model->id]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Create  session ROLLBACK');
                Logs::logEvent("Error on creating session: ", $error, null);


            }

        }
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('_meeting_create', $res) : $this->render('_meeting_create', $res);

    }

    public function actionCreateMeetingApi($id)
    {
        $request = Yii::$app->request;
        try {

            // send API request

            $onlineSession =Teleconferencing::findOne([$id]);
            $meetingName = preg_replace('/\s+/', '+', $onlineSession->meeting_name);
            $meetingID = $onlineSession->meeting_id;
            $welcomeNote = $onlineSession->welcome_note;
            $record =$onlineSession->record;


           $urlLogout=  Url::toRoute(['/site/tele-logout','id' => $meetingID, ],'http');

         //   $urlLogout= 'https://suiteuat.schoolpay.co.ug/site/tele-logout?id='.$meetingID;
         //   $urlCallBack= 'http%3A%2F%2Flocalhost%2Fschoolsuite%2Fe_learning%2Felearning%2Fcall-back%3FmeetingID%3D'.$meetingID;

            $createMeetingParams = new CreateMeetingParameters($meetingID, $meetingName);
               $createMeetingParams->setRecord($record);
//                $createMeetingParams->setModeratorPassword($moderator_password);


           // $isRecordingTrue =true;

            $createMeetingParams->setDuration($onlineSession->meeting_duration);
           $createMeetingParams->setLogoutUrl($urlLogout);
           // $createMeetingParams->setEndCallbackUrl($urlCallBack);
            $createMeetingParams->setWelcomeMessage($welcomeNote);
                if ($record) {
                    $createMeetingParams->setRecord(true);
                    $createMeetingParams->setAllowStartStopRecording(true);
                    $createMeetingParams->setAutoStartRecording(true);
                }
            $bbb=TeleconferingHelper::initialiseBBB();

            //Logs::logEvent("Going to send session: ". $onlineSession->meeting_id, null);


            $response = $bbb->createMeeting($createMeetingParams);
          //  Logs::logEvent("response: ". $response, null);

            if ($response->getReturnCode() == 'FAILED') {
                return 'Can\'t create room! please contact our administrator.';
            } else {
                // process after room created
                /** @var

                Update teleconferencing_table with passwords
                 */

            $onlineSession->moderator_password = $response->getModeratorPassword();
            $onlineSession->attendee_password = $response->getAttendeePassword();
            $onlineSession->date_created =  date('Y-m-d');
            $onlineSession->save(false);


                return $this->redirect(['elearning/join-meeting', 'id' => $id, 'password'=>$onlineSession->moderator_password ]);
            }


            // return $this->redirect(['view-meeting', 'id' => $model->id]);

        } catch (\Exception $e) {
            Yii::trace($e);
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace('Error: ' . $error, 'Create  session ROLLBACK');
            Logs::logEvent("Error on creating session: ", $error, null);
            $res = ['error'=>$error, 'model'=>new Teleconferencing()];
            return ($request->isAjax) ? $this->renderAjax('_meeting_create', $res) : $this->render('_meeting_create', $res);
        }


    }

    public function actionViewRoomDetails($id){

        $request = Yii::$app->request;
        $model = $this->findTeleconferencingModel($id);


        $res =['model'=>$model];
        return ($request->isAjax) ? $this->renderAjax('meeting_room_details', $res) : $this->render('meeting_room_details', $res);


    }



    public function actionJoinMeeting($id, $password)
    {
        $request = Yii::$app->request;
        $onlineSession =Teleconferencing::findOne([$id]);

        $bbb=TeleconferingHelper::initialiseBBB();

        $meetingRunning= $this->meetingRunning($onlineSession->meeting_id);


        $name = Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname;

        $meetingID =$onlineSession->meeting_id;

        // $moderator_password for moderator
        $joinMeetingParams = new JoinMeetingParameters($meetingID, $name, $password);
        $joinMeetingParams->setRedirect(true);
        $url = $bbb->getJoinMeetingURL($joinMeetingParams);

        //$response = $bbb->createMeeting($createMeetingParams);

        if($url){
            $attendanceLog= new AttendanceLogs();
            $attendanceLog->class_id= $onlineSession->class_id;
            $attendanceLog->meeting_id= $onlineSession->meeting_id;
            $attendanceLog->school_id= $onlineSession->school_id;
            $attendanceLog->db_meeting_id =$onlineSession->id;
            $attendanceLog->attendee_id= Yii::$app->user->identity->id;
            $attendanceLog->save(false);

        }

        Yii::trace($url);

        $res = ['url' => $url];
        return $this->redirect($url);

        //  return ($request->isAjax) ? $this->renderAjax('meeting_room', $res) : $this->render('meeting_room', $res);


    }
    public function actionAttendeeJoin($id, $password)
    {
        $request = Yii::$app->request;
        $onlineSession =Teleconferencing::findOne([$id]);

        $bbb=TeleconferingHelper::initialiseBBB();

        $meetingRunning= $this->meetingRunning($onlineSession->meeting_id);
        Yii::trace($meetingRunning);

        if(!$meetingRunning){
            $onlineSession->meeting_active= false;
            $onlineSession->meeting_stop=  new Expression('NOW()');
            $onlineSession->save(false);
           return $this->render('session_ended');
        }





        $name = Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname;

        $meetingID =$onlineSession->meeting_id;

        // $moderator_password for moderator
        $joinMeetingParams = new JoinMeetingParameters($meetingID, $name, $password);
        $joinMeetingParams->setRedirect(true);
        $url = $bbb->getJoinMeetingURL($joinMeetingParams);

        //$response = $bbb->createMeeting($createMeetingParams);

        if($url){
            $attendanceLog= new AttendanceLogs();
            $attendanceLog->class_id= $onlineSession->class_id;
            $attendanceLog->meeting_id= $onlineSession->meeting_id;
            $attendanceLog->school_id= $onlineSession->school_id;
            $attendanceLog->db_meeting_id =$onlineSession->id;
            $attendanceLog->attendee_id= Yii::$app->user->identity->id;
            $attendanceLog->save(false);

        }

        Yii::trace($url);

        $res = ['url' => $url];
        return $this->redirect($url);
        //return ($request->isAjax) ? $this->renderAjax('meeting_room', $res) : $this->render('meeting_room', $res);


    }



    public function actionModeratorRooms(){

        $request= Yii::$app->request;
        $model = new Teleconferencing();
        $query = new Query();
        $query->select(['ol.*','cls.class_code' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->andWhere(['ol.created_by' => Yii::$app->user->identity->id]);

        $query->orderBy('ol.date_created desc');
        $roomDetails = $query->all(Yii::$app->db);
        $title="All Active School Rooms";


        $res =['roomDetails'=>$roomDetails, 'model'=>$model,'title'=>$title];
       Yii::trace($roomDetails);
        return ($request->isAjax) ? $this->renderAjax('moderator_meetings', $res) : $this->render('moderator_meetings', $res);


    }

    public function actionSchoolAttendeeRooms(){

        $request= Yii::$app->request;
        $model = new Teleconferencing();
        $query = new Query();
        $query->select(['ol.*','cls.class_code' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->andWhere(['ol.school_id' => Yii::$app->user->identity->school_id]);

        $query->orderBy('ol.date_created desc');

        $roomDetails = $query->all(Yii::$app->db);
        $title="All Active School Rooms";


        $res =['roomDetails'=>$roomDetails, 'model'=>$model,'title'=>$title];
        Yii::trace($roomDetails);
        return ($request->isAjax) ? $this->renderAjax('all_school_meeting', $res) : $this->render('all_school_meeting', $res);


    }


    public function actionStudentRooms(){

        $request= Yii::$app->request;
        $model = new Teleconferencing();
        $modelStd= CoreStudent::findOne(['student_code'=>Yii::$app->user->identity->username]);

        $query = new Query();
        $query->select(['ol.*','cls.class_code', 'uz.firstname','uz.lastname' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('user uz', 'uz.id =ol.created_by')
            ->andWhere(['ol.school_id' => Yii::$app->user->identity->school_id])
            ->andWhere(['ol.class_id' => $modelStd->class_id])
            ->andWhere(['ol.meeting_active' => true]);
        $query->orderBy('ol.date_created desc');

        $roomDetails = $query->all(Yii::$app->db);
        $title="All Active Class Rooms";


        $res =['roomDetails'=>$roomDetails, 'model'=>$model,'title'=>$title];
        Yii::trace($roomDetails);
        return ($request->isAjax) ? $this->renderAjax('attendee_meetings', $res) : $this->render('attendee_meetings', $res);


    }


    public function actionStdSessionRecordings(){

        $request= Yii::$app->request;
        $model = new Teleconferencing();
        $modelStd= CoreStudent::findOne(['student_code'=>Yii::$app->user->identity->username]);

        $query = new Query();
        $query->select(['ol.*','cls.class_code', 'uz.firstname','uz.lastname' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('user uz', 'uz.id =ol.created_by')
            ->andWhere(['ol.school_id' => Yii::$app->user->identity->school_id])
            ->andWhere(['ol.class_id' => $modelStd->class_id])
            ->andWhere(['ol.meeting_active' => false]);
        $query->orderBy('ol.date_created desc');

        $roomDetails = $query->all(Yii::$app->db);
        $title="All Past Sessions";


        $res =['roomDetails'=>$roomDetails, 'model'=>$model,'title'=>$title];
        Yii::trace($roomDetails);
        return ($request->isAjax) ? $this->renderAjax('std_session_recording', $res) : $this->render('std_session_recording', $res);


    }


    public function actionCallBack($meetingID){

        $request= Yii::$app->request;
        $model = new Teleconferencing();

        $modelStd= CoreStudent::findOne(['student_code'=>Yii::$app->user->identity->username]);
        Yii::trace($meetingID);



    }
    public function actionGetRecording($meetingID){
        $request = Yii::$app->request;
        $response = RecordedSessions::findOne(['meeting_id'=>$meetingID]);
        if(isset($response->play_back_url)){
            $url = $response->play_back_url;

            return $this->redirect($url);
        }else{
            return ($request->isAjax) ? $this->renderAjax('meeting_room') : $this->render('meeting_room');


        }





    }

    public function actionSchSessionReport($meetingID){

        $request= Yii::$app->request;
        $model = new Teleconferencing();
        $query = new Query();
        $query->select(['ol.*','cls.class_code','ols.meeting_name' ,'ols.meeting_active','ols.meeting_start','ols.meeting_stop','ols.meeting_active','uz.lastname','uz.firstname','uz.username' ])
            ->from('session_attendance ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('online_lessons ols', 'ols.id =ol.db_meeting_id')
            ->innerJoin('user uz', 'ol.attendee_id =uz.id')
            ->andWhere(['ol.meeting_id' => $meetingID]);
        $query->orderBy('ols.date_created desc');

        $roomDetails = $query->all(Yii::$app->db);
        $title="Class Room Summary";


        $res =['roomDetails'=>$roomDetails, 'model'=>$model,'title'=>$title];
        Yii::trace($roomDetails);
        return ($request->isAjax) ? $this->renderAjax('session_summary', $res) : $this->render('session_summary', $res);


    }

    public function actionTeacherSessions(){

        $request= Yii::$app->request;
        $model = new Teleconferencing();
        $query = new Query();
        $query->select(['ol.*','cls.class_code','ols.meeting_name' ,'ols.meeting_active','ols.meeting_start','ols.meeting_stop','ols.meeting_active'])
            ->from('session_attendance ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('online_lessons ols', 'ols.id =ol.db_meeting_id')
            ->orderBy('ols.date_created desc');



        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {
                          $query->andWhere(['ols.id' => Yii::$app->user->identity->school_id]);
            }

        $roomDetails = $query->all(Yii::$app->db);
        $title="Class Room Summary";


        $res =['roomDetails'=>$roomDetails, 'model'=>$model,'title'=>$title];
        Yii::trace($roomDetails);
        return ($request->isAjax) ? $this->renderAjax('tr_session_summary', $res) : $this->render('tr_session_summary', $res);


    }

    public function actionIndexSessions()
    {
        if (Yii::$app->user->can('r_student')) {
            $request = Yii::$app->request;

            $searchModel = new TeleconferencingSearch();
            $data = $searchModel->searchSessions(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('index_sessions', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('index_sessions', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);
        } else {
            throw new ForbiddenHttpException('No permissions to view students.');
        }

    }


    public static function meetingRunning($meetingID){
        $bbb=TeleconferingHelper::initialiseBBB();

        $isMeetingRunningParams = new IsMeetingRunningParameters($meetingID);
        $running=false;
        $response = $bbb->isMeetingRunning($isMeetingRunningParams);

        if ($response->success() && $response->isRunning()) {
            // meeting is running

            $running=true;
        }
       return $running;

    }






    public function actionRecordedSessions(){
          $request = Yii::$app->request;
            Yii::trace(Yii::$app->user);
            $searchModel = new TeleconferencingSearch();
            $data = $searchModel->searchMeeting(Yii::$app->request->queryParams);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];

            return ($request->isAjax) ? $this->renderAjax('past_sessions', [
                'searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]) :
                $this->render('past_sessions', ['searchModel' => $searchModel,
                    'dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort']]);




    }

    //get recordings
    public function actionGetRecordings(){
        $this->layout = 'loginlayout';

        $bbb=TeleconferingHelper::initialiseBBB();

        $recordingParams = new GetRecordingsParameters();
       // $recordingParams->setRecordID($recordId); // omit to get a list of all recordings
        $recordingParams->setState('any');

        $response = $bbb->getRecordings($recordingParams);

        if (!$response->success()) {
            // handle error
        }

        $records = $response->getRecords();
       // Yii::trace($records);

        foreach($records as $record){
            Yii::trace($record->getPlaybackUrl());
            //check if it exists


            $record->getMeetingId();
            $record->getName();
          //  $record->getParticipantCount();
            $record->getPlaybackLength();
            $record->getPlaybackLength();
            $record->getRecordId();
            $record->getStartTime();
            $existing = RecordedSessions::findOne(['meeting_id'=>$record->getMeetingId()]);
            if($existing){

            }else {
                $recorded = new RecordedSessions();


                $recorded->meeting_id = $record->getMeetingId();
                $recorded->meeting_name = $record->getName();
                //$recorded->participant_count=$record->getParticipantCount();
                $recorded->play_back_length = $record->getPlaybackLength();
                $recorded->record_id = $record->getRecordId();
                $recorded->play_back_url = $record->getPlaybackUrl();
                //$recorded->start_time=$record->getStartTime();
                $recorded->save(false);
            }
        }

//        Logs::logEvent("Saved new recordings: ", null);
//
//        $lastcheck = new LastRecordedSessionsCheck();
//        $lastcheck->was_checked= true;
//        $lastcheck->save(false);

    }


    //All Sessions
    public function actionMenu(){
//        return $this->render('menu');
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('menu') :
            $this->render('menu');
    }




}

<?php
namespace app\modules\e_learning;

class ElearningModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\e_learning\controllers';
    public function init() {
        parent::init();
    }
}

<?php

namespace app\modules\e_learning\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $fee_id
 * @property integer $class_id
 * @property boolean $applied
 * @property string $created_by
 * @property string $type_of_work
 * @property string $student_id
 * @property string $title
 * @property string $content
 * @property string $school_id
 *

 */
class StudentElearning extends \yii\db\ActiveRecord
{
    public $uploads, $contacts;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_submissions';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'class_id', 'subject_id', 'student_id', 'title', 'content', 'type_of_work'], 'required'],
            [['id', 'class_id', 'school_id', 'subject_id', 'student_id', 'teacher_submission_id'], 'integer'],

            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx, xls,csv, xlsx, mp3,mp4,ppt, pptx', 'maxFiles' => 10, 'maxSize' => 1024 * 1024 * 50,],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_id' => 'Class ID',
            'grade' => 'Grade',
            'student_id' => 'Student ',
            'subject_id' => 'Subject',
            'title' => 'Title',
            'content' => 'Content',
            'type_of_work' => 'Type',
        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getStoredFiles()
    {
        $files = (new Query())->select(['id', 'file_name'])->from('student_uploads')
            ->where(['submission_id' => $this->id]);
        Yii::trace($files);
        return $files->all();
    }

    public function saveStudentUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            foreach ($this->uploads as $file) {
                //check if video or audio
                $upload = new StudentUploads();
                $path = Yii::getAlias('@uploads') . '/' . date('Y/m/d');

                if(!FileHelper::createDirectory($path)) {
                    throw new Exception('Problem saving your uploads to intended path');
                }
                chmod("$path", 0777);

                $path = $path.'/' . $this->school_id .'_'. rand(100, 200) . '_' . str_replace(' ', '_', $file->baseName) . '.' . $file->extension;
                $fname = $file->baseName . '.' . $file->extension;

                if ($file->extension == 'ts' || $file->extension == 'mp3' || $file->extension == 'mp4') {
                    Yii::trace("file extension" . $file->extension);
                    $file->saveAs($path);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_path = $fname;
                    $upload->save(false);
                }
          else {

              // $fname = $file->baseName . '.' . $file->extension;
              $tmpfile_contents = file_get_contents($file->tempName);
              // $upload = new StudentUploads();
              $upload->submission_id = $this->id;
              $upload->file_name = $fname;
              $upload->file_data = base64_encode($tmpfile_contents);

              $upload->save(true);
          }

            }
        }
    }

    public function getTypeOfWork()
    {
        if ($this->type_of_work == 1) {
            return 'Notes';
        } else if ($this->type_of_work == 2) {
            return 'Exercises';
        } else if ($this->type_of_work == 3) {
            return 'Home Work';
        } else return 'Other';
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewStudentModelItems($params)
    {
        $attributes = [

            'title',

            'content',

        ];

        if (($models = StudentElearning::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function searchStudentClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id', 'csc.class_code', 'sch.school_name', 'csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch', 'csc.school_id =sch.id');

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach ($data as $k => $v) {
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/display-student-subject', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];

    }

    public function saveStudentUploadUpdate($id)
    {

        
        if (is_array($this->uploads) && $this->uploads) {
            foreach ($this->uploads as $file) {
                //check if video or audio
                Yii::trace($file->extension);
                $upload = new StudentUploads();
                $fname = $file->baseName . '.' . $file->extension;
                $path = Yii::getAlias('@uploads').'/' . $file->baseName . '.' . $file->extension;

                if ($file->extension == 'ts' || $file->extension == 'mp3' || $file->extension == 'mp4') {
                    if($file->saveAs($path)){
                        Yii::trace("file  saved" . $file->extension);

                        $upload->submission_id = $this->id;
                        $upload->file_name = $fname;
                        $upload->file_path = $fname;
                        $upload->save(false);
                    }else{
                        Yii::trace("failed");
                    }

                } else {

                    $fname = $file->baseName . '.' . $file->extension;
                    $tmpfile_contents = file_get_contents($file->tempName);
                //    $upload = new StudentUploads();
                    Yii::trace('The Id' . $id);
                    $upload->submission_id = $id;
                    $upload->file_name = $fname;
                    $upload->file_data = base64_encode($tmpfile_contents);
                    $upload->save(false);
                    Yii::trace('Saved file ' . $file->baseName);
                }

            }
        }

    }

}

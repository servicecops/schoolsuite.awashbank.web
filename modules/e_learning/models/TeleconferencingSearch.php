<?php

namespace app\modules\e_learning\models;

use app\models\User;
use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class TeleconferencingSearch extends Teleconferencing
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['modelSearch'], 'trim'],
            [['date_created', 'created_by','meeting_active','modelSearch','schsearch'], 'safe'],

        ];
    }
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */

    public function search($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);
        $this->school_student_registration_number = trim($this->school_student_registration_number);

        $query = new Query();
        $query->select(['ol.id', 'ol.meeting_name', 'ol.', 'si.last_name', 'cls.class_code', 'sch.school_name', 'si.student_email', 'si.student_phone',
            "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end,
             case when last_name is not null then ' '||last_name else '' end) as student_name", 'si.web_user_id',
            'si.school_student_registration_number'])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=cls.school_id')
            //
            ->Where(['si.archived' => false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ol.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['ol.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }



        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);


                $query->andWhere(['meeting_name' => trim(strtoupper($this->modelSearch))]);

        }




        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('si.student_code');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {

        $teacherId =Yii::$app->user->identity->getId();

        $query = (new Query())
            ->select(['ol.id','ol.meeting_name', 'ol.moderator_password','ol.attendee_password','sch.school_name','csc.class_description','ol.duration','uz.firstname','uz.lastname'])
            ->from('online_lessons ol')
            ->innerJoin('core_school sch','sch.id =ol.school_id' )
            ->innerJoin('core_school_class csc','csc.id =ol.class_id' )
            ->innerJoin('user uz','uz.id =ol.created_by' )
            ->where(['ol.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
    }
    public function searchSubject($params)
    {
        $teacherId =Yii::$app->user->identity->getId();

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/create-lesson', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Add Lesson', ['elearning/create-lesson', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
    public function searchTeachersSubject($params)
    {
        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['subject_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cs.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cs.id', $the_classId]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/all-class-subject-work', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-class-subject-work', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchTeachersSubjectSubmission($params)
    {

        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/all-student-index', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-student-index', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchTeacher($params)
    {

        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.firstname,ct.lastname, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('"user" ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' )
            ->innerJoin('core_school  sch','sch.id =ts.school_id' )
            ->where(['ts.subject_id'=>$params]);

        $query->orderBy('ts.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach($data as $k =>$v){
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            // $query->andWhere(['tca.class_id' => 18]);
            $query->andwhere(['in', 'sb.id', $the_subjectId]);
            $query->andwhere(['ts.teacher_id'=> Yii::$app->user->identity->getId()]);

        }



        $command = $query->createCommand();
        $data = $command->queryAll();

        $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];
    }


    public function searchStdClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' );


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        }
        elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        $query->andWhere(['csc.active'=>true] );

        $query->orderBy('csc.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['/attendance/rollcall/class-students-list', 'classId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Create Session', ['/e_learning/elearning/create-meeting', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }


    public function searchSessions($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['ol.*','cls.class_code','sch.school_name' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('core_school sch', 'sch.id =ol.school_id');
            //->andWhere(['ol.school_id' => Yii::$app->user->identity->school_id]);

        //$query->orderBy('ol.date_created asc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ol.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['ol.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }

        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

              //Registration numbers usually have a dash - or slass /
                $query->andWhere(['ol.meeting_name' => trim($this->modelSearch)]);

        }



        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'meeting_name', 'meeting_id',
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ol.meeting_start desc');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function searchMeeting($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $query = new Query();
        $query->select(['ol.*','cls.class_code','sch.school_name' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('core_school sch','sch.id = ol.school_id')
            ->andWhere(['ol.created_by' => Yii::$app->user->identity->id]);


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cls.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cls.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['si.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['cls.school_id' => $this->schsearch]);
            }
        }


        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            $query->andWhere(['meeting_name' => trim(strtoupper($this->modelSearch))]);

        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'meeting_id', 'meeting_name',  'school_name', 'class_code',  'date_created'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ol.date_created desc');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

    public function searchStudentMeeting($params)
    {
        $this->load($params);
        //Trim the model searches
        $this->modelSearch = trim($this->modelSearch);

        $modelStd= CoreStudent::findOne(['student_code'=>Yii::$app->user->identity->username]);

        $query = new Query();
        $query->select(['ol.*','cls.class_code', 'uz.firstname','uz.lastname' ])
            ->from('online_lessons ol')
            ->innerJoin('core_school_class cls', 'cls.id =ol.class_id')
            ->innerJoin('user uz', 'uz.id =ol.created_by')
            ->andWhere(['ol.school_id' => Yii::$app->user->identity->school_id])
            ->andWhere(['ol.class_id' => $modelStd->class_id])
            ->andWhere(['ol.meeting_active' => true]);



        if (!(empty($this->modelSearch))) {
            Yii::trace($this->modelSearch);

            $query->andWhere(['meeting_name' => trim(strtoupper($this->modelSearch))]);

        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'meeting_id', 'meeting_name',  'school_name', 'class_code',  'date_created'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('ol.date_created desc');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];

    }

}

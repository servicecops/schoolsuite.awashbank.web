<?php

namespace app\modules\e_learning\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $fee_id
 * @property integer $class_id
 * @property boolean $applied
 * @property string $created_by
 * * @property string $type_of_work
 * @property string $play_back_length
 * @property string $meeting_id
 * @property string $meeting_name
 * @property string $participant_count
 *

 */
class RecordedSessions extends \yii\db\ActiveRecord
{
    public $uploads, $contacts,$schsearch;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recorded_sessions';
    }



    /**
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'meeting_id', 'meeting_name','participant_count',], 'required'],
            [['id', 'class_id','school_id','subject_id'], 'integer'],
            [['play_back_length','record_id','start_time'], 'string'],
            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx, xls, xlsx,csv, mp3,mp4', 'maxFiles' => 10,'maxSize' => 1024 * 1024 * 10, // 10MB


            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_id' => 'Class Name',
            'grade' => 'Grade',
            'teacher_id' => 'Submitted by ',
            'subject_id' => 'Subject Name',
            'title' => 'Topic',
            'content' => 'Content',
        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getStdStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('student_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }
    public function getTypeOfWork(){
        if($this->type_of_work==1){
            return 'Notes';
        }else if($this->type_of_work==2){
            return 'Exercises';
        }else if($this->type_of_work==3){
            return 'Home Work';
        } else return 'Other';
    }

    public function getSubjectName()
    {
        return $this->hasOne(CoreSubject::className(), ['id' => 'subject_id']);
    }
    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('teacher_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
                $saved = Yii::getAlias('@uploads').'/' . $file->baseName . '.' . $file->extension;
                $upload = new TeacherUploads();
                $fname = $file->baseName . '.' . $file->extension;
                if ($file->extension == 'ts' || $file->extension == 'mp3' || $file->extension == 'mp4') {
                    Yii::trace("file extension" . $file->extension);
                    $file->saveAs($saved);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_path = $fname;
                    $upload->save(false);
                } else {

                    $tmpfile_contents = file_get_contents($file->tempName);
                    Yii::trace($tmpfile_contents);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_data = base64_encode($tmpfile_contents);
                    $upload->save(false);
                    Yii::trace('Saved file ' . $file->baseName);
                }
            }
        }
    }

    public function saveUploadUpdate($id)
    {

        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            foreach ($this->uploads as $file) {

                $fname = $file->baseName . '.' . $file->extension;
                $tmpfile_contents = file_get_contents( $file->tempName );
                $upload = new TeacherUploads();
                Yii::trace('The Id'.$id);
                $upload->submission_id = $id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);
            }
        }
    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModelItems($params)
    {
        $attributes = [

            'title',

            'content',

        ];

        if (($models = Elearning::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    public function searchClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' )
               ->where(['csc.active'=>true]) ;

        $query->orderBy('csc.id desc');
        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        }
//        if ( Yii::$app->user->can('is_teacher')) {
//
//            $trQuery = new Query();
//            $trQuery->select(['tc.class_id'])
//                ->from('teacher_class_subject_association tc')
//                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
//                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
//                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);
//
//
//            $commandt = $trQuery->createCommand();
//            $data = $commandt->queryAll();
//
//
//            $the_classId = [];
//            foreach($data as $k =>$v){
//                array_push($the_classId, $v['class_id']);
//            }
//            Yii::trace($the_classId);
//
//            if ($this->class_id && $this->class_id != 0) {
//                $query->andWhere(['csc.id' => intVal($this->class_id)]);
//            } else {
//                // $query->andWhere(['tca.class_id' => 18]);
//                $query->andwhere(['in', 'csc.id', $the_classId]);
//            }
//        }
         elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['elearning/display-teachers-subject', 'classId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/display-teachers-subject', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }
    public function searchTeachersClass(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' )
            ->where(['csc.active'=>true]) ;
        ;

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['elearning/display-subject', 'classId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/display-subject', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }

    public function searchTeachersClassSubmission(array $queryParams)
    {
        $query = (new Query())
            ->select(['csc.id','csc.class_code', 'sch.school_name','csc.class_description'])
            ->from('core_school_class csc')
            ->innerJoin('core_school sch','csc.school_id =sch.id' )
            ->where(['csc.active'=>true]) ;

        $query->orderBy('csc.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'csc.id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['sch.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['sch.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'class_code',
            [
                'label' => 'Class Name',
                'value'=>function($model){
                    return Html::a($model['class_description'], ['elearning/display-teachers-subject-submission', 'classId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/display-teachers-subject-submission', 'classId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];

    }

}

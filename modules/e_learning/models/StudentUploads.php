<?php

namespace app\modules\e_learning\models;

use Yii;

/**
 * This is the model class for table "submission_uploads".
 *
 * @property string $id
 * @property string $date_created
 * @property string $submission
 * @property string $file_name
 * @property string $file_data
 */
class StudentUploads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_uploads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['submission_id', 'file_name', 'file_data'], 'required'],
            [['submission_id'], 'integer'],
            [['file_data'], 'string'],
            [['file_path'], 'string'],
            [['file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_created' => 'Date Created',
            'submission' => 'Submission',
            'file_name' => 'File Name',
            'file_data' => 'File Data',
        ];
    }
}

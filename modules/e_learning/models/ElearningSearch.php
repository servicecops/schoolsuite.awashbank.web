<?php

namespace app\modules\e_learning\models;

use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use Yii;
use yii\base\BaseObject;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class ElearningSearch extends Elearning
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['date_created', 'created_by'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.firstname,ct.lastname, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('"user" ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' )
            ->innerJoin('core_school  sch','sch.id =ts.school_id' )
            ->where(['ts.subject_id'=>$params]);

        $query->orderBy('ts.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach($data as $k =>$v){
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'sb.id', $the_subjectId]);
                $query->andwhere(['ts.teacher_id'=> Yii::$app->user->identity->getId()]);

        }
      


        $command = $query->createCommand();
        $data = $command->queryAll();

       $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
       return ['data'=>$data, 'searchModel' => $searchModel];
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.first_name,ct.last_name, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('core_teacher ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' );

        $query->orderBy('ts.id desc');

        $command = $query->createCommand();
        $data = $command->queryAll();

        $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];


    }
    public function searchSubject($params)
    {
        $teacherId =Yii::$app->user->identity->getId();

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/create-lesson', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Add Lesson', ['elearning/create-lesson', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
    public function searchTeachersSubject($params)
    {
        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('sch_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($the_classId);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['subject_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['cs.id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'cs.id', $the_classId]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/all-class-subject-work', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-class-subject-work', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchTeachersSubjectSubmission($params)
    {

        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            [
                'label' => 'Subject Name',
                'value'=>function($model){
                    return Html::a($model['subject_name'], ['elearning/all-student-index', 'subjectId' => $model['id']], ['class'=>'aclink']);
                },
                'format'=>'html',
            ],
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-student-index', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchTeacher($params)
    {

        $teacherId =Yii::$app->user->identity->getId();
        $query = (new Query());
        $query->select('ts.id,ts.title,cc.class_description,ct.firstname,ct.lastname, sb.subject_name, ts.date_created, ts.type_of_work ')
            ->from('teacher_submissions ts')
            ->innerJoin('"user" ct','ts.teacher_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' )
            ->innerJoin('core_school  sch','sch.id =ts.school_id' )
            ->where(['ts.subject_id'=>$params]);

        $query->orderBy('ts.id desc');

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => $teacherId]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach($data as $k =>$v){
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            // $query->andWhere(['tca.class_id' => 18]);
            $query->andwhere(['in', 'sb.id', $the_subjectId]);
            $query->andwhere(['ts.teacher_id'=> Yii::$app->user->identity->getId()]);

        }



        $command = $query->createCommand();
        $data = $command->queryAll();

        $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'fileName' => 'Classes',
            'title' => 'Class info Datasheet',
            'exportFile' => '/elearning/exportPdfExcel',
        ];

        return $data;
    }



}

<?php

namespace app\modules\e_learning\models;

use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class StudentElearningSearch extends StudentElearning
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['date_created', 'created_by'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $query = (new Query());
        $query = StudentElearning::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'id',
            [
                'label' => 'Type',
                'value' => function ($model) {
                    return $model->typeOfWork;
                },
            ],
            'title',
            'student_id',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],

            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/student-view', 'studentId' => $model['id']]);
                },
            ],



        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

            'school_id' => $this->school_id,
            'class_id' => $this->class_id,
            'title' => $this->title,
        ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $searchForm = 'title';


        $searchModel = new StudentElearningSearch();
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'title',
            'content',

        ];

        if (($models = StudentElearning::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');


    }


    public function studentSubmissions($subjectId)
    {

        $query = (new Query());
        $query->select('ts.id,ts.title,ct.first_name,ct.last_name, ts.date_created, ts.type_of_work ')
            ->from('student_submissions ts')
            ->innerJoin('core_student ct', 'ts.student_id =ct.id')
            ->innerJoin('core_subject sb', 'sb.id =ts.subject_id')
            ->innerJoin('core_school_class cc', 'cc.id =ts.class_id')
            ->where(['ts.subject_id' => $subjectId]);


        if (!Yii::$app->user->can('non_student')) {

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ts.class_id' => intVal($this->class_id)]);
            } else {
                $std = CoreStudent::findOne(['web_user_id'=>Yii::$app->user->identity->getId()]);
                $query->andWhere(['ts.student_id' => $std['id']]);
            }
        }

        $command = $query->createCommand();
        $data = $command->queryAll();


        $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data' => $data, 'searchModel' => $searchModel];


    }
 public function allStudentSubmissions($subjectId)
    {
        $query = (new Query());
        $query->select('ts.id,ts.title,ct.first_name,ct.last_name, ts.date_created, ts.type_of_work ')
            ->from('student_submissions ts')
            ->innerJoin('core_student ct', 'ts.student_id =ct.id')
            ->innerJoin('core_subject sb', 'sb.id =ts.subject_id')
            ->innerJoin('core_school_class cc', 'cc.id =ts.class_id')
            ->where(['sb.id' => $subjectId]);


        if (!Yii::$app->user->can('non_student')) {

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ts.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['ts.student_id' => Yii::$app->user->identity->id]);
            }
        }

        $command = $query->createCommand();
        $data = $command->queryAll();


        $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data' => $data, 'searchModel' => $searchModel];


    }

    public function singleStudentSubmissions($subjectId)
    {
        $query = (new Query());
        $query->select('ts.id,ts.title,ct.first_name,ct.last_name, ts.date_created, ts.type_of_work ')
            ->from('student_submissions ts')
            ->innerJoin('core_student ct','ts.student_id =ct.id' )
            ->innerJoin('core_subject sb','sb.id =ts.subject_id' )
            ->innerJoin('core_school_class cc','cc.id =ts.class_id' )
            ->where(['sb.id'=>$subjectId])
            ->andWhere(['ct.id'=>Yii::$app->user->id]);

        $query->orderBy('ts.id desc');

        if (\app\components\ToWords::isSchoolUser() ) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['ts.class_id' => intVal($this->class_id)]);
            } else {
                $query->andWhere(['ts.school_id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['ts.class_id' => intVal($this->class_id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['sch.id' => $this->schsearch]);
            }
        }

        $command = $query->createCommand();
        $data = $command->queryAll();


        $searchModel = new ElearningSearch();
//        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
        return ['data'=>$data, 'searchModel' => $searchModel];


    }

    public function searchStudentSubject($params)
    {

        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);
        $query->orderBy('cs.id desc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);

        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {

            $trQuery = new Query();
            $trQuery->select(['tc.subject_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()])
                ->andWhere(['tc.class_id' => $params]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_subjectId = [];
            foreach ($data as $k => $v) {
                array_push($the_subjectId, $v['subject_id']);
            }
            Yii::trace($the_subjectId);

            $query->andwhere(['in', 'cs.id', $the_subjectId]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $columns = [
            'subject_code',
            'subject_name',
            'class_description',
            'school_name',
            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/student-index', 'subjectId' => $model['id']]);
                },
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function searchStudentClassSubject($params)
    {



        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);

        $query->orderBy('cs.id desc');



        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/all-subject-work', 'subjectId' => $model['id']]);
                },
                'visible'=>!\Yii::$app->user->can('non_student'),
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
    public function searchStudentClassSubjectSubmission($params)
    {



        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);

        $query->orderBy('cs.id desc');



        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/student-submission', 'id' => $model['id']]);
                },
                'visible'=>!\Yii::$app->user->can('non_student'),
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }

    public function StudentViewSubjectSubmission($params)
    {
        $query = (new Query())
            ->select(['cs.id','cs.subject_code', 'cs.subject_name','cs.date_created','sch.school_name','csc.class_description'])
            ->from('core_subject cs')
            ->innerJoin('core_school sch','sch.id =cs.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cs.class_id' )
            ->where(['cs.class_id'=>$params]);

        $query->orderBy('cs.id desc');



        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'subject_code',
            'subject_name',

            [

                'label' => '',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['elearning/student-index', 'subjectId' => $model['id']]);
                },
                'visible'=>!\Yii::$app->user->can('non_student'),
            ],
            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }




}

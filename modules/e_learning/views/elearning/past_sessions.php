<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Students Information';
?>

<div style="margin-top:20px ;color:#F7F7F7">Student Information</div>
<div class="letter">



    <div class="row" style="margin-top:20px">

        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-md-8"><?php echo $this->render('_search_passed_sessions', ['model' => $searchModel]); ?></div>
    </div>

<div class="row mt-3">

        <?php


        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['schoolcore/core-student/export-pdf', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-danger',
            'target'=>'_blank',
            'data-toggle'=>'tooltip',
            'title'=>'Will open the generated PDF file in a new window'
        ]);


        echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-success',
            'target'=>'_blank'
        ]);

        ?>

</div>

    <div class="row">


                <table class="table table-striped">
                    <thead>
                    <tr>

                        <th class='clink'>Meeting Date</th>
                        <th class='clink'>Meeting Name</th>
                        <th class='clink'>School Name</th>

                        <th class='clink'>Class Code</th>
                        <th class='clink'></th>
                        <th class='clink'>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr data-key="0">
                                <td class="clink"><?= ($v['date_created']) ?  $v['date_created'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['meeting_name']) ? $v['meeting_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>

                                <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                <td>
                                    <?= Html::a('View Attendance', ['elearning/sch-session-report', 'meetingID' =>  $v['meeting_id']], ['class'=>'aclink']) ?>

                                </td>
                                <td>

                                    <?= Html::a('View Recording', ['elearning/get-recording', 'meetingID' =>  $v['meeting_id']]); ?>
                                </td>
                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No Sessions found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>

            <?= LinkPager::widget([
                'pagination' => $pages['pages'], 'firstPageLabel' => 'First',
                'lastPageLabel'  => 'Last'
            ]); ?>
        

    </div>


</div>
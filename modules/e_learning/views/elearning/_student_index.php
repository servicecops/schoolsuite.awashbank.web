<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Student Submissions';
$this->params['breadcrumbs'][] = $this->title;
?>

<p style="color: #F1F4F6">All Student Uploads</p>
<div class="letter">

    <div class="row model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;<?= Html::encode($this->title) ?></h3></div>
    </div>


    <div class="row" >
        <?php
        if(!$data) {
        ?>
        <div ><p> <?php echo "No results found." ?></p></div><?php
        }else{
        Yii::trace($data);
        foreach($data as $k=>$v){

            if($v['type_of_work']==1){
                $type_of_work= 'Notes';
            }else if($v['type_of_work']==2){
                $type_of_work= 'Exercises';
            }else if($v['type_of_work']==3){
                $type_of_work= 'Home Work';
            } else return 'Other';
            ?>



            <div style ="width:30%;float:left; padding:10px; margin:10px;background: #fff; border-radius:5px;">

                <div>
                    <p style="color:#ff0016"><b><?= $searchModel->getAttributeLabel('title') ?></b>:  <?= ($v['title']) ? $v['title'] : "--"  ?> </p>
                   <p><b><?= $searchModel->getAttributeLabel('student_id') ?></b>:  <?= ($v['first_name']).' '.($v['last_name']) ? ($v['first_name']).' '.($v['last_name']) : "--"  ?> </p>
                   <p><b><?= $searchModel->getAttributeLabel('type_of_work') ?></b>:   <?=   ($v['type_of_work']) ? $type_of_work : "--"  ?></p>
                    <p style="color:#ff0016"><b><?= $searchModel->getAttributeLabel('date_created') ?></b>:   <?=   strtotime(($v['date_created'])) ? date('M d, Y', strtotime($v['date_created'])) : "--"  ?></p>
                    <p><?= Html::a('View', ['elearning/student-view', 'id' => $v['id']], ['class' => 'btn btn-sm btn-primary'])?></p>
                </div>
                <div style="clear:both;"></div>
            </div>

        <?php  }
        }
        ?>

    </div>



</div>

<div class="row" style="border-bottom: 1px solid #c5c5c5; padding-bottom: 10px;">
    <div class="col" style="padding-left:10px"><span class="lead text-uppercase">Submission Info</span></div>
</div>

<div class="row">

    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b>Lead Submission</b></div>
            <div class="col-6 profile-text"><?= $model->is_lead ? "Yes" : "No" ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b>Lead Id</b></div>
            <div class="col-6 profile-text"><?= $model->lead_id ?></div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b>Submitted By</b></div>
            <div class="col-6 profile-text"><?= $model->submitter0->fullname ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b>Submitter Phone</b></div>
            <div class="col-6 profile-text"><?= $model->submitter0->telephone ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('account_number') ?></b></div>
            <div class="col-6 profile-text"><?= $model->account_number ?></div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('primary_bank') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->primary_bank) ? $model->primaryBank->bank_name : "--" ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('date_submitted') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->date_submitted) ? date('M d, Y h:i', strtotime($model->date_submitted)) : "--" ?></div>
        </div>
    </div>
    <div class="col-sm-6">
    <div class="row">
        <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('badge_attached') ?></b></div>
        <div class="col-6 profile-text"><?= $model->badge_attached ? "Yes" : "No" ?></div>
    </div>
    </div>
</div>

<div class="row">
    <div class="col-3 profile-text"><b><?= $model->getAttributeLabel('school_name') ?></b></div>
    <div class="col-9 profile-text"><?= $model->school_name ?></div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b>School Address</b></div>
            <div class="col-6 profile-text"><?= $model->address ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b>Country</b></div>
            <div class="col-6 profile-text"><?= $model->country ?></div>
        </div>
    </div>
</div>





<?php if($model->is_old_school || $model->old_school_reason) : ?>
    <div class="row text-danger">
        <div class="col-3 profile-text"><b>Old School - Reason</b></div>
        <div class="col-9 profile-text"><?= ($model->old_school_reason) ? $model->old_school_reason : "--" ?></div>
    </div>
<?php endif; ?>
<?php if($model->blocked) : ?>
    <div class="row text-danger">
        <div class="col-3 profile-text"><b>On Hold - Reason</b></div>
        <div class="col-9 profile-text"><?= ($model->reason_for_blocking) ? $model->reason_for_blocking : "--" ?></div>
    </div>
<?php endif; ?>



<?php if($model->uploaded) : ?>
<br>
<div class="row" style="border-bottom: 1px solid #c5c5c5; padding-bottom: 10px;">
    <div class="col" style="padding-left:10px"><span class="lead text-uppercase">Submission Uploaded</span></div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('uploaded') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->uploaded) ? "Yes" : "No"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('uploaded_by') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->uploaded_by) ? $model->uploadedBy->fullname : "--"  ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-3 profile-text"><b><?= $model->getAttributeLabel('date_uploaded') ?></b></div>
    <div class="col-9 profile-text"><?= ($model->date_uploaded) ? date('M d, Y', strtotime($model->date_uploaded)) : "--"  ?></div>
</div>
<?php endif; ?>


<br>
<div class="row">
    <table class="table" >
        <thead>
        <tr><th colspan="4"><span class="lead">School Contacts</span></th></tr>
        <tr><th> Contact Person</th><th> Phone Number</th> <th> Email</th></tr>
        </thead>
        <tbody>
        <?php
        if($contacts = $model->retrieveContacts) :
            foreach($contacts as $k=>$v): ?>
                <tr>
                    <td><?= $v['contact_name'] ?></td>
                    <td><?= $v['contact'] ?></td>
                    <td><?= $v['email'] ?></td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr><td colspan="4">No school contacts provided</td></tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<?php if($model->cards_generated) : ?>
<div class="row" style="border-bottom: 1px solid #c5c5c5; padding-bottom: 10px;">
    <div class="col" style="padding-left:10px"><span class="lead text-uppercase">Cards Tracker</span></div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('cards_generated_by') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->cards_generated_by) ? $model->cardsGeneratedBy->fullname : "--"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('date_cards_generated') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->date_cards_generated) ? $model->date_cards_generated : "--"  ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('cards_print_status') ?></b></div>
            <div class="col-6 profile-text"><?= $model->printStatus; ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('cards_assigned_to') ?></b></div>
 `           <div class="col-6 profile-text"><?= ($model->cards_assigned_to) ? $model->printerAgent : "--"  ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('date_cards_taken_for_printing') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->date_cards_taken_for_printing) ? $model->date_cards_taken_for_printing : "--"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('date_cards_printed') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->date_cards_printed) ? date('M d, Y', strtotime($model->date_cards_printed)) : "--"  ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('noc_generated') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->noc_generated) ? $model->noc_generated : "--"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('noc_printed') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->noc_printed) ? $model->noc_printed : "--"  ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('cards_taken_by') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->cards_taken_by) ? $model->cards_taken_by : "--"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('valid_noc_taken') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->valid_noc_taken) ? $model->valid_noc_taken : "--"  ?></div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if($model->credentials_sent) : ?>
<p>&nbsp;</p>
<div class="row" style="border-bottom: 1px solid #c5c5c5; padding-bottom: 10px;">
    <div class="col" style="padding-left:10px"><span class="lead text-uppercase">Credentials Tracker</span></div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('credentials_sent_by') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->credentials_sent_by) ? $model->credentialsSentBy->fullname : "--"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('date_credentials_sent') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->date_credentials_sent) ? date('d/m/Y h:i', strtotime($model->date_credentials_sent)) : "--"  ?></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('credential_recipient1') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->credential_recipient1) ? $model->credentialSentTo1->contact_name : "--"  ?></div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-6 profile-text"><b><?= $model->getAttributeLabel('credential_recipient2') ?></b></div>
            <div class="col-6 profile-text"><?= ($model->credential_recipient2) ? $model->credentialSentTo2->contact_name : "--" ?></div>
        </div>
    </div>
</div>
<?php endif; ?>
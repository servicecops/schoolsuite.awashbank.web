 <?php

use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\e_learning\models\OnlineRegistration */
/* @var $form yii\bootstrap4\ActiveForm */
?>




<div class="formz">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Add Lesson</h3></div>
    </div>

    <?= $this->render('@app/views/common/app_notifications', []); ?>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div style="padding: 10px;width:100%"></div>
<p>All fields marked with * are required</p>
    <div class="col-sm-12">
        <?= $form->field($model, 'type_of_work', ['inputOptions'=>[ 'class'=>'form-control'] ])->dropDownList(
            ['Notes'=>'Notes','Exercises'=>'Exercises', 'Home Work'=>'Home Work'],['prompt'=>'Select type of work']) ->label('Type of Work *')?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'title', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Title']])->textInput()->label('Title *') ?>
    </div>
    <div class="col-sm-12">

        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'basic'
        ])->label('Content *') ?>
    </div>
    <h5 class="form-header">Attach Files</h5>
    <div class="form-desc">Please attach all files needed i.e. (doc, docx, pdf, xls, or xlsx), videos(mp3,mp4) and images(png, jpg, jpeg, pdf)
    </div>

    <?=
    $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
        'options' => ['accept' => '*/*'],
        'pluginOptions' => [
            'maxFileSize'=>1024 * 1024 * 100,
            'showUpload' => true,
                'allowedFileExtensions' => ['pdf', 'docx', 'doc','xlsx','pptx', 'csv','xls','mp3','mp4','ts','mkv','ppt']],
    ]);
    ?>
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$script = <<< JS
$(document).ready(function(){

$('#kvFileinputModal').modal('hide').remove();
$( "form#submission_form" ).on( "beforeSubmit", function() {
setTimeout(function(){
$("#loader").addClass("modal-loading");
});
});
});
JS;
$this->registerJs($script);
//?>


<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */

$this->title = 'Create Session Link';
$this->params['breadcrumbs'][] = ['label' => 'Create Meeting Link', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="core-school-create">
    <?php if (isset($error)) : ?>
        <div class="row">
            <div class='alert alert-danger'><?= $error ?></div>
        </div>
    <?php endif; ?>
    <div class="letter">
        <div class="row">
            <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
        </div>
        <div class="row">
            <?= $this->render('_create_meeting_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>

<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression; ?>

<div class="formz">
    <div class="form-group col-sm-12 ">
        <?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

        <p>All fields marked with * are required</p>


            <div class="col-sm-12">
                <?= $form->field($model, 'meeting_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Session Name']])->textInput()->label('Meeting Name *') ?>
            </div>
        <div class="col-sm-12">

            <?= $form->field($model, 'meeting_duration', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Session Duration']])->dropDownList(
                [

                    10 => '10 mins',
                    15=>'15 mins',
                    20=>'20 mins',
                    25=>'25 mins',
                    30=>'30 mins',
                    35=>'35 mins',
                    40=>'40 mins',
                    45=>'45 mins',
                    50=>'50 mins',
                    55=>'55 mins',
                    60=>'60 mins',
                    80=>'80 mins',
                    100=>'100 mins',
                    120=>'120 mins',
                    140=>'140 mins',
                    160=>'160 mins',
                    180=>'180 mins',
                ],
                ['id' => 'send_msg_after_effective_date_in_days','prompt' => 'Select duration in Minutes' ]
            )->label('Session Duration *') ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'welcome_note', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Session Description']])->textarea()->label('Session Description *') ?>
        </div>
    <div class="col-sm-12">
        <div class="checkbox checkbox-inline checkbox-info">
            <input type="hidden" name="Teleconferencing[record]" value="0">
            <input type="checkbox" id="recorded-active" name="Teleconferencing[record]"
                   value="1" <?= ($model->record) ? 'checked' : '' ?> >
            <label for="recorded-active">Record</label>
        </div>        </div>




    </div>
    <div class="form-group col-sm-12 ">
        <div class="col-xs-6">
            <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>


</div>


<?php
$script = <<< JS
    $(".active").removeClass('active');
    $("#classes").addClass('active');
    $("#classes_create").addClass('active');
JS;
$this->registerJs($script);
?>



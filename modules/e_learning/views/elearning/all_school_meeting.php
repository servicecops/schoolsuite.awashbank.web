<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSubject */

$this->title;
$this->params['breadcrumbs'][] = ['label' => 'Core Subject', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="letter">
    <main class="container">

        <div class="title">


            <div class="col mt-5">
                <h5 class="text-center" style="color: #000">Sessions Created by me </h5>
            </div>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Meeting Name</th>
                    <th>Class</th>
                    <th>Moderator Password</th>
                    <th>Attendee Password</th>
                    <th>Meeting Status</th>
                    <th>Start</th>
                    <th>Stop</th>
                    <th>Duration</th>
                </tr>
                </thead>
                <tbody>
                <?php if ($roomDetails) :
                    foreach ($roomDetails as $k => $v) : ?>
                        <tr data-key="0">
                            <td></td>
                            <td><?= ($v['meeting_name']) ? $v['meeting_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['class_id']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['moderator_password']) ? $v['moderator_password'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['attendee_password']) ? $v['attendee_password'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['meeting_active']) ? 'Active' : 'Ended' ?></td>
                            <td><?= ($v['meeting_start']) ? date('Y-m-d H:i', strtotime($v['meeting_start'])) : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['meeting_stop']) ? date('Y-m-d H:i', strtotime($v['meeting_stop'])) : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['meeting_duration']) ? $v['meeting_duration'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td>
                                <?php if($v['meeting_active']){?>
                                    <?= Html::a('Join 2', ['elearning/attendee-join', 'id' =>  $v['id'],'password'=>$v['attendee_password']]); ?>
                                <?php }else{?>
                                    <?= Html::a('View Attendance', ['elearning/sch-session-report', 'meetingID' =>  $v['meeting_id']]); ?>

                                <?php }?>

                            </td>
                            <td></td>
                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No Sessions found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>


    </main>
</div>

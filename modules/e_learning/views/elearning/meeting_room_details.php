<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\e_learning\models\Teleconferencing */


$this->params['breadcrumbs'][] = ['label' => '', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="letter">

    <h1><?= Html::encode($this->title) ?></h1>




    <div class="row" style="margin-top:20px">

        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i>Online Class Room Session Details </span>
        </div>
    </div>
    <br/>

    <div class="row">

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('school_id') ?></div>
                <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>
                    <div class="col-md-6 profile-text"><?= ($model->school_id) ? Html::a($model->institution0->school_name, ['core-school/view', 'id' => $model->institution0->id]) : "--" ?></div>
                <?php else : ?>
                    <div class="col-md-6 profile-text"><?= ($model->school_id) ? $model->institution0->school_name : "--" ?></div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('class_id') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->class_id) ?$model->className->class_description : "--" ?></div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('meeting_name') ?></div>
                <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->meeting_name) ? $model->meeting_name : "--" ?></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;"><b>Created By</b></div>
                <div class="col-lg-6 col-xs-9 profile-text"><b><?=  ($model->created_by) ? $model->otherName->firstname.' '.$model->lastName->lastname : "--"  ?></b></div>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-12">
            <div class="row ">
                <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;"><?= $model->getAttributeLabel('moderators_password') ?></div>
                <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->moderator_password) ? $model->moderator_password : "--" ?></div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-6 col-xs-3 profile-text" style="background: #e9f4fb !important;"><b>Attendee Password</b></div>
                <div class="col-lg-6 col-xs-9 profile-text"><b><?=  ($model->attendee_password) ? $model->attendee_password : "--"  ?></b></div>
            </div>
        </div>
    </div>

    <br>
    <br/>


</div>

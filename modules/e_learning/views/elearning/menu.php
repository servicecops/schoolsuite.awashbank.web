<?php

use app\components\Helpers;
use yii\helpers\Url;
$this->title = "E-learning center";
?>


<div class="card my-1 bg-light">
    <h3 class="card-header display-4 d-flex justify-content-between">E-learning center</h3>
    <div class="row card-body justify-content-center">
        <?php if (Helpers::is('rw_classwork')) : ?>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/display-class']); ?>">
                <div class="display-4 d-flex justify-content-between  text-center"> <span>View class work</span> <i class="fa fa-eye" aria-hidden="true"></i></div>
            </a>
        <?php endif; ?>

        <?php if (Helpers::is('r_classwork') || Helpers::is('non_student')) : ?>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/display-teachers-class']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>Add class work</span>  <i class="fa fa-plus-circle" aria-hidden="true"></i></div>
            </a>

            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/display-teachers-class-submission']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>Student submissions</span>  <i class="fa fa-check-circle"></i></div>
            </a>
        <?php endif; ?>

        <?php if (!Helpers::is('non_student')) : ?>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/student-class-subject']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>View class work</span>  <i class="fa fa-eye" aria-hidden="true"></i></div>
            </a>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/student-view-subject-submission']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>My submissions</span>  <i class="fa fa-check-circle" aria-hidden="true"></i></div>
            </a>

            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/student-rooms']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>My E-Class Rooms</span>  <i class="fa fa-group" aria-hidden="true"></i></div>
            </a>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/std-session-recordings']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>Past E-Sessions</span>  <i class="fa fa-calendar-check-o" aria-hidden="true"></i></div>
            </a>
        <?php endif; ?>
        <?php if (\app\components\ToWords::isSchoolUser() || Helpers::is('schoolsuite_admin')) : ?>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/index-sessions']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>All Sessions Summary</span>  <i class="fa fa-bookmark" aria-hidden="true"></i></div>
            </a>
        <?php endif; ?>
        <?php if (Helpers::is('rw_classwork')) : ?>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/classes']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>Create E-Session</span>  <i class="fa fa-plus" aria-hidden="true"></i></div>
            </a>
            <a class="card col-md-4 m-1 col-sm-6 col-xs-12" href="<?= Url::to(['/e_learning/elearning/moderator-rooms']); ?>">
                <div class="display-4 d-flex justify-content-between text-center"> <span>My E-Sessions</span>  <i class="fa fa-user" aria-hidden="true"></i></div>
            </a>
        <?php endif; ?>

    </div>
</div>

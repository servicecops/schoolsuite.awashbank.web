<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSubject */

$this->title;
$this->params['breadcrumbs'][] = ['label' => 'Core Subject', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="letter">
    <main class="container">

            <div class="title">
                <div class="pull-right">

                    <?php
                    if (\Yii::$app->user->can('rw_lesson')) :

                        ?>
                        <p>
                            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-sm btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>

                        </p>
                    <?php endif; ?>
                </div>
                <div class="align-content-center">
                    <h2></h2><i class="fa fa-book" style="color: red"></i>
                    <h2 style="color: #000" ><?= ($model->title) ? $model->title : ' --' ?>
                    </h2>
                </div>
                <div class="col mt-5">
                    <h5 class="text-center" style="color: #000">Submission for <?= $model->subjectName->subject_name?></h5>
                </div>

                <article >
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12">

                            <div class="col-lg-12 col-sm-6 col-xs-12 no-padding">
                                <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Date Created: </b></div>
                                <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->date_created) ?></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-lg-12 col-sm-6 col-xs-12 no-padding">
                                <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Type of Work </b></div>
                                <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->type_of_work) ? $model->type_of_work : ' --' ?></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-lg-12 col-sm-6 col-xs-12 no-padding bg-row">
                                <div class="col-lg-6 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>Content </b></div>
                                <div class="col-lg-6 col-xs-6 "><?= ($model->content) ? $model->content : ' --' ?></div>
                            </div>

                        </div>



                    </div>


                </article>

            <?php if($model->storedFiles): ?>
                <?= $this->render('files_list', ['uploads' => $model->storedFiles]); ?>
            <?php endif;?>
    </main>
</div>

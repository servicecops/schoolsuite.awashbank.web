<?php

use app\modules\schoolcore\models\CoreSchool;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\modules\e_learning\models\StudentElearning */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="formz">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Add your work</h3></div>
    </div>

    <?php $form = ActiveForm::begin([
        'action' => ['student-submission', 'id'=>$id],
        'method' => 'post',
//        'options' => ['class' => 'formprocess','enctype' => 'multipart/form-data']]);
        'options' => ['enctype' => 'multipart/form-data']]);
    ?>

    <div style="padding: 10px;width:100%"></div>
    <div class="col-sm-12">
        <?= $form->field($model, 'type_of_work', ['inputOptions'=>[ 'class'=>'form-control'] ])->dropDownList(
                [''=>'Select Type',2=>'Exercises', 3=>'Home Work']) ->label('Type of Work *')?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, 'title', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Title']])->textInput()->label('Title *') ?>
    </div>
    <div class="col-sm-12">
        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'custom'
        ])->label('Content *') ?>    </div>



    <h5 class="form-header">Attach Files</h5>
    <div class="form-desc">Please attach all files needed i.e. (doc, docx, pdf, xls, or xlsx), videos(mp3,mp4) and images(png, jpg, jpeg, pdf)
    </div>



    <?=
    $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
        'options' => ['accept' => '*/*'],
        'pluginOptions' => [
                'maxFileSize'=>1024 * 1024 * 50,
            'showUpload' => true,
                'allowedFileExtensions' => ['pdf', 'docx', 'doc', 'xls','xlsx','pptx','png', 'jpg','mp3','mp4', 'jpeg','mkv','ppt']

            ],
    ]);
    ?>
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$script = <<< JS
$(document).ready(function(){
$('#kvFileinputModal').modal('hide').remove();
$( "form#submission_form" ).on( "beforeSubmit", function() {
setTimeout(function(){
$("#loader").addClass("modal-loading");
});
});

});
JS;
$this->registerJs($script);
//?>


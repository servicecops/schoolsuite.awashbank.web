<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\e_learning\models\StudentElearning */


\yii\web\YiiAsset::register($this);
?>
<div class="core-school-view">

    <h3>Uploaded Work</h3>

<!-- c-->
    <?php
    if (!Yii::$app->user->can('non_student')) :  ?>

    <div class="col mt-5">    <?= Html::a('<i class="fa fa-pencil" > Update</i>', ['update-student-uploads', 'subjectId' => $model->subject_id, 'id'=>$model->id],['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash" > Delete</i>', ['student-delete', 'id' => $model->id], [
            'class' => 'btn btn-sm btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>



    </div>

    <?php endif ; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'content',

        ],
    ]) ?>
    <?php if($model->storedFiles): ?>
        <?= $this->render('student_file_list', ['uploads' => $model->storedFiles]); ?>
    <?php endif;?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSubject */

$this->title;
$this->params['breadcrumbs'][] = ['label' => 'Core Subject', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="letter">
    <main class="container">

        <div class="title">


            <div class="col mt-5">
                <h5 class="text-center" style="color: #000"><?php echo $title?> </h5>
            </div>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>Meeting Name</th>
                    <th>Class</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Joining Time</th>

                </tr>
                </thead>
                <tbody>
                <?php if ($roomDetails) :
                    foreach ($roomDetails as $k => $v) : ?>
                        <tr data-key="0">
                            <td></td>
                            <td><?= ($v['meeting_name']) ? $v['meeting_name'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['class_id']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['firstname']) ? $v['firstname'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['lastname']) ? $v['lastname'] : '<span class="not-set">(not set) </span>' ?></td>
                            <td><?= ($v['username']) ? $v['username'] : '<span class="not-set">(not set) </span>' ?></td>

                            <td><?= ($v['joining_time']) ? date('H:i:s', strtotime($v['joining_time'])) : '<span class="not-set">(not set) </span>' ?></td>

                            <td></td>
                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No Sessions found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>


    </main>
</div>

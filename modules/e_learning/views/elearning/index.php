<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Class Work Submissions';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="color:#F1F4F6">Elearning</p>
<div class="letter">
    <div class="row">
    <div class="model_titles">
        <div class="col-sm-12"><h3 ><i class="fa fa-check-square-o"></i>&nbsp;<?= Html::encode($this->title) ?></h3>
        </div>
    </div>
    </div>
    <div class="row">

        <?php
        Yii::trace($data);
        if(!$data) { ?>
            <div><p> <?php echo "No results found." ?></p></div><?php
        }else {
            foreach ($data as $key => $value) {

                if ($value['type_of_work'] == 1) {
                    $type_of_work = 'Notes';
                } else if ($value['type_of_work'] == 2) {
                    $type_of_work = 'Exercises';
                } else if ($value['type_of_work'] == 3) {
                    $type_of_work = 'Home Work';
                } else {
                    $type_of_work = 'Other';
                }

                ?>

                <div style="width:30%;float:left; padding:10px; margin:10px;background: #fff;">

                    <div>

                        <p style="color:#ff0016">
                            <b><?= $searchModel->getAttributeLabel('title') ?></b>: <?= ($value['title']) ? $value['title'] : "--" ?>
                        </p>
                        <p>
                            <b><?= $searchModel->getAttributeLabel('class_id') ?></b>: <?= ($value['class_description']) ? $value['class_description'] : "--" ?>
                        </p>
                        <p>
                            <b><?= $searchModel->getAttributeLabel('teacher_id') ?></b>: <?= ($value['firstname']) . ' ' . ($value['lastname']) ? ($value['firstname']) . ' ' . ($value['lastname']) : "--" ?>
                        </p>
                        <p>
                            <b><?= $searchModel->getAttributeLabel('subject_id') ?></b>: <?= ($value['subject_name']) ? $value['subject_name'] : "--" ?>
                        </p>
                        <p>
                            <b><?= $searchModel->getAttributeLabel('type_of_work') ?></b>: <?= ($value['type_of_work']) ? $type_of_work : "--" ?>
                        </p>
                        <p style="color:#ff0016">
                            <b><?= $searchModel->getAttributeLabel('date_created') ?></b>: <?= ($value['date_created']) ? date('M d, Y', strtotime($value['date_created'])) : "--" ?>
                        </p>

                    </div>
                    <div class="row">
                        <div clas="col">
                            <p><?= Html::a('View', ['elearning/view', 'id' => $value['id']], ['class' => 'btn btn-sm bg-gradient-primary']) ?></p>
                        </div>
                        <?php if (!\Yii::$app->user->can('non_student')):?>
                        <div clas="col">
                            <p><?= Html::a('Submit Work', ['elearning/student-submission', 'id' => $value['id']], ['class' => 'btn btn-sm bg-gradient-primary']) ?></p>
                        </div>
                <?php endif;?>
                    </div>
                </div>


            <?php }
        }
        ?>

    </div>
</div>

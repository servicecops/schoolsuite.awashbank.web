<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transaction Report');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class=" row hd-title" data-title="School Transaction Report">
        <div class=" col-md-12 ">
            <?php $form = ActiveForm::begin(['action' => ['schools-report-data'], 'method' => 'get',
                'id' => 'trans-form',
                'options' => ['class' => 'form-horizontal formprocess']]); ?>
            <div class="col-md-2 ">
                <?= DatePicker::widget([
                        'model' =>$searchModel,
                    'name' => 'date_from',
                    'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : null,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control input-sm',
                        'Placeholder' => 'From',
                        'name' => 'Trans[date_from]',
                        'readonly' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-2 ">
                <?= DatePicker::widget([
                    'model' =>$searchModel,
                    'name' => 'date_to',
                    'value' => (isset($_GET['date_to'])) ? $_GET['date_to'] : null,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control input-sm',
                        'Placeholder' => 'To',
                        'name' => 'Trans[date_to]',
                        'readonly' => true
                    ],
                ]); ?>
            </div>
            <div class="col-sm-3">

                <?php
                $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
                echo $form->field($searchModel, 'branch_region')
                    ->dropDownList(
                        $items,           // Flat array ('id'=>'label')
                        ['prompt' => 'Select Branch Region',
                            'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/branches']) . '?id="+$(this).val(), function( data ) {
                            $( "select#branches" ).html(data);
                        });']    // options
                    )->label(false); ?>

            </div>
            <div class="col-sm-3">

                <?= $form->field($searchModel, 'branch_id')->dropDownList([],

                    ['id' => 'branches',
                    ])->label(false)
                ?>

            </div>

            <div class="col-md-2">
                <?= Html::submitButton('Find', ['class' => 'btn btn-default btn-md']) ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="col-md-12"></div>
            <div class="col-md-12">

            <div class="col-md-12 box-header">
                <h3 class="box-title"><i
                            class="fa fa-info-circle"></i> <?= $this->title . " ( " . date('M d, y', strtotime($from_date)) . " - " . date('M d, y', strtotime($date_to)) . ")" ?>
                </h3>
                <div class="col-md-12 box-tools pull-right">


                    <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                        <?php
                        echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                            'class' => 'btn btn-sm btn-danger',
                            'target' => '_blank',
                            'data-toggle' => 'tooltip',
                            'title' => 'Will open the generated PDF file in a new window'
                        ]);
                        echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                            'class' => 'btn btn-sm btn-success',
                            'target' => '_blank'
                        ]);
                        ?>
                    </ul>
                </div>
            </div><!-- /.box-header -->

            <div class="col-md-12 box-body table-responsive ">

                <?php
                echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";

                if (!empty($dataProvider)) {
                    echo "<tr>";
                    echo "<th class='text-center'>Bank Region</th>";
                    echo "<th class='text-center'>Branch Name</th>";
                    echo "<th class='text-center'>School name</th>";

                    echo "<th class='text-center'>Number of Students</th>";
                    echo "<th class='text-center'>Number of Txns</th>";
                    echo "<th class='text-center'>Transaction Value</th>";

                    echo "</tr>";
                    foreach ($dataProvider as $t => $v) {

                        echo "<tr>";

                        echo "<td class='text-center'>" . $v['bank_region_name'] . "</td>";
                        echo "<td class='text-center'>" . $v['branch_name'] . "</td>";

                        echo "<td class='text-center'>" . $v['school_name'] . "</td>";
                        echo "<td class='text-center'>" . number_format($v['number_of_students']) . "</td>";
                        echo "<td class='text-center'>" . number_format($v['txnz'],2) . "</td>";
                        echo "<td class='text-center'>" . number_format($v['transaction_value'],2) . "</td>";
                        echo "</tr>";

                    }


                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>GRAND TOTAL</th>";
                    echo "<th></th>";
                    echo "<th></th>";

                    echo "<th class='text-center'>" . number_format($count) . "</th>";
                    echo "<th class='text-center'>" . number_format($ttxn,2) . "</th>";
                    echo "<th class='text-center'>" . number_format($sum,2) . "</th>";
                    echo "</tr>";


                }else{
                    echo "<tr>";
                    echo "<th>No Data Found</th>";

                    echo "</tr>";
                }
                echo "</table>";
                ?>
            </div>
        </div>
    </div>


<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#summary_report").addClass('active');
  });
JS;
$this->registerJs($script);

<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;

//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\Controller;

$this->title = Yii::t('app', 'Channel Transaction Performance summary');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row card" data-title="Channel Transaction Summary">
    <div class="card-body">
        <?php $form = ActiveForm::begin(['action' => ['channel'], 'method' => 'get',
            'id' => 'trans-form',
            'options' => ['class' => 'form-horizontal formprocess']]); ?>


        <div class="container mb-3">
            <h4>Choose date range</h4>
            <div class="row">
                <div class="col">

                    <?php
                    echo DatePicker::widget([
                        'name' => 'date_from',
                        'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : '',
                        'type' => DatePicker::TYPE_RANGE,
                        'name2' => 'date_to',
                        'value2' => (isset($_GET['date_to'])) ? $_GET['date_to'] : '',
                        'options' => ['placeholder' => 'Start date'],
                        'options2' => ['placeholder' => 'End date'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col">
                    <?= Html::submitButton('Find', ['class' => 'btn btn-primary btn-sm']) ?>

                </div>

            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <div class="row">


            <h3>
                <i class="fa fa-info-circle"></i> <?= $this->title . " ( <span style='font-size:14px;'><b>" . date('M d, y', strtotime($from_date)) . " &nbsp;&nbsp;-&nbsp;&nbsp;  " . date('M d, y', strtotime($to_date)) . "</b></span>)" ?>
            </h3>
            <div class=" float-right">
                <?php echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['transactions/trans'], ['class' => 'btn btn-back', 'style' => 'color:#fff']); ?>
                <?php echo Html::a('<i class="fa fa-file-excel"></i> Excel', ['transactions/channel-trans-list', 'channellistexcelexport' => 'channellistexcel'], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn-sm btn-success', 'style' => 'color:#fff')); ?>
                <?php echo Html::a('<i class="fa fa-file-pdf"></i> PDF', array('transactions/channel-trans-list', 'channellistexport' => 'channellistpdf'), array('title' => 'Export to PDF', 'target' => '_blank', 'class' => 'btn-sm btn-warning', 'style' => 'color:#fff')); ?>
            </div>


            <div class="table-responsive no-padding" style="table-layout: fixed !important;">
                <?php
                $nil = '--';
                if (!empty($channel_data)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th colspan='5'></th>";
                    echo "<th colspan='2'>USSD</th>";

                    echo "<th colspan='2'>TELLER</th>";
                    echo "<th colspan='2'>A-APP</th>";
                    echo "<th colspan='2'>A-USSD</th>";
                    echo "<th colspan='2'>POS</th>";
                    echo "<th colspan='2'>MPOS</th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th>Bank Region</th>";
                    echo "<th>Country Region</th>";
                    echo "<th>Branch</th>";
                    echo "<th >School Name</th>";
                    echo "<th >Students</th>";
                    echo "<th >No Txn</th>";
                    echo "<th > Txn V</th>";
                    echo "<th >No Txn</th>";
                    echo "<th > Txn V</th>";
                    echo "<th >No Txn</th>";
                    echo "<th > Txn  V</th>";
                    echo "<th >No Txn</th>";
                    echo "<th > Txn  V</th>";
                    echo "<th >No Txn</th>";
                    echo "<th > Txn V</th>";
                    echo "<th >No Txn</th>";
                    echo "<th > Txn V</th>";

                    //echo "<th >Number of Transactions</th>";

                    echo "</tr>";
                    foreach ($channel_data as $t => $y)
                        foreach ($y as $k => $v) {

                            echo "<tr>";
                            echo "<td >" . $v['regionname'] . "</td>";
                            echo "<td >" . $v['ctyregion'] . "</td>";
                            echo "<td >" . $v['branchname'] . "</td>";
                            echo "<td >" . $v['schoolname'] . "</td>";
                            echo "<td >" . $v['stds'] . "</td>";
                            // echo "<td >";
                            // Yii::trace($v['transactions']);
                            if (!($v['transactions'])) {
                                Yii::trace("No txns");
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                                echo "<td >" . $nil . "</td>";
                            } else {

                                foreach ($v['transactions'] as $x):
                                    Yii::trace($x);
                                    //foreach ($z as $p => $o) {
//                                        print_r($o);echo "</br>";
//                                        echo "<td >USSD</td>";


                                    if (strcasecmp('USSD', $x['chn']) == 0) {

                                        echo "<td >" . $x['chn'] . ' ' . $x['no_of_txn'] . "</td>";
                                        echo "<td >" . $x['txn_valu'] . "</td>";

                                    }
                                   else if (strcasecmp('TELLER CHANNEL', $x['chn']) == 0) {

                                        echo "<td >" . $x['chn'] . ' ' . $x['no_of_txn'] . "</td>";
                                        echo "<td >" . $x['txn_valu'] . "</td>";

                                    }
                                   else{
                                       echo "<td >" .'--' . "</td>";
                                       echo "<td >" . '--' . "</td>";
                                   }
                                endforeach;
//
                                echo "</tr>";
                            }
                        }

                    echo "</table>";
                } else echo "No results found";
                ?>

            </div>


        </div>
    </div>


    <table>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
        </tr>
        <tr>
            <td>a</td>
            <td>b</td>
            <td>c</td>
        </tr>
        <tr>
            <td>e</td>
            <td></td>

            <td>f</td>
        </tr>
        <tr>
            <td>t</td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>


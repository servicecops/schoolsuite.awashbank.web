<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transaction History');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php if(!empty($query))
echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
?>
<br>
<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
<?php

if(!empty($channel_data))
{
    echo "<table class ='table-bordered table table-striped'>";
    echo "<tr>";
    echo "<th class='text-center'>Channel name</th>";
    echo "<th class='text-center'>Channel Type</th>";
    echo "<th class='text-center'>Total Amount Transacted</th>";
    echo "<th class='text-center'>Number of Transactions</th>";

    echo "</tr>";
    $i = 1;
    foreach($channel_data as $t=>$v)
    {       
        if(($i%2) == 0)
         {
            $bgcolor = "#FFFFFF";
         }
         else
         {
            $bgcolor = "#E3E3E3";
         }      
        echo '<tr align=center bgcolor='.$bgcolor.'>';
        echo "<td class='text-center'>".$v['cn']."</td>";
        echo "<td class='text-center'>".$v['ct']."</td>";
        echo "<td class='text-center'>".$v['sum']."</td>";
        echo "<td class='text-center'>".$v['count']."</td>";
        echo "</tr>";
        $i++;  
            
    }
    echo "<tr>";
        echo "<td style='border-right:0px'><b>Total</b></td>";
        echo "<td style='border-left:0px'></td>";
        echo "<td class='text-center'><b>".$sum." </b></td>";
        echo "<td class='text-center'><b>".$count."</b></td>";
    echo "</tr>";
    
echo "</table>";
}
?>
</div>
</div>
<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\Controller;

$this->title = Yii::t('app', 'Channel Transaction summary');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row card" data-title="Channel Transaction Summary">
    <div class="card-body">
    <?php $form = ActiveForm::begin(['action' => ['channel-summary'],'method' => 'get',
        'id' => 'trans-form',
        'options' => [  'class'  => 'form-horizontal formprocess'] ]); ?>





        <div class="container mb-3">
            <h4>Choose date range</h4>
            <div class="row">
                <div class="col">

                    <?php
                    echo DatePicker::widget([
                        'name' => 'date_from',
                        'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : '',
                        'type' => DatePicker::TYPE_RANGE,
                        'name2' => 'date_to',
                        'value2' => (isset($_GET['date_to'])) ? $_GET['date_to'] : '',
                        'options' => ['placeholder' => 'Start date'],
                        'options2' => ['placeholder' => 'End date'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col">
                    <?= Html::submitButton('Find' , ['class' => 'btn btn-primary btn-sm' ]) ?>

                </div>

            </div>
        </div>

    <?php ActiveForm::end(); ?>

<div class="row">






          <h3><i class="fa fa-info-circle"></i> <?= $this->title." ( <span style='font-size:14px;'><b>".date('M d, y', strtotime($from_date))." &nbsp;&nbsp;-&nbsp;&nbsp;  ".date('M d, y', strtotime($to_date))."</b></span>)" ?></h3>
          <div class=" float-right">
        <?php echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['transactions/trans'],['class'=>'btn btn-back', 'style'=>'color:#fff']);?>
            <?php echo Html::a('<i class="fa fa-file-excel"></i> Excel',['transactions/channel-trans-list','channellistexcelexport'=>'channellistexcel'],array('title'=>'Export to Excel','target'=>'_blank','class'=>'btn-sm btn-success', 'style'=>'color:#fff'));?>
            <?php echo Html::a('<i class="fa fa-file-pdf"></i> PDF',array('transactions/channel-trans-list','channellistexport'=>'channellistpdf'),array('title'=>'Export to PDF','target'=>'_blank','class'=>'btn-sm btn-warning', 'style'=>'color:#fff')); ?>
          </div>


 <div class="table-responsive no-padding">
<?php
if(!empty($channel_data))
{
    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
    echo "<tr>";
    echo "<th class='text-center'>Region</th>";
    echo "<th class='text-center'> branch name</th>";
    echo "<th class='text-center'>Channel name</th>";
    echo "<th class='text-center'>Channel Type</th>";
    echo "<th class='text-center'>Total Amount Transacted</th>";
    echo "<th class='text-center'>Number of Transactions</th>";

    echo "</tr>";
    foreach($channel_data as $t=>$v)
    {
        echo "<tr>";
        echo "<td class='text-center'>".$v['branch_name']."</td>";
        echo "<td class='text-center'>".$v['bank_region_name']."</td>";
        echo "<td class='text-center'>".$v['cn']."</td>";
        echo "<td class='text-center'>".$v['ct']."</td>";
        echo "<td class='text-center'>".$v['ct']."</td>";
        echo "<td class='text-center'>".$v['ct']."</td>";
        echo "<td class='text-center'>".number_format($v['sum'], 2)."</td>";
        echo "<td class='text-center'>".number_format($v['count'])."</td>";
        echo "</tr>";

    }

    echo "<tr>";
        echo "<th></th>";
        echo "<th></th>";
        echo "<th></th>";
        echo "<th></th>";
    echo "</tr>";
    echo "<tr>";
        echo "<th class='text-center' rowspan='2'>GRAND TOTAL</th>";
        echo "<th></th>";
        echo "<th class='text-center'>".number_format($sum, 2)."</th>";
        echo "<th class='text-center'>".number_format($count)."</th>";
    echo "</tr>";

echo "</table>";
} else echo "No results found";
?>

</div>

</div>
</div>
</div>


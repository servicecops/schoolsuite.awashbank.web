<?php

use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\SchoolAccountTransactionHistory;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

?>



<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
<?php
if(!empty($student_data))
{
  $s_trans = new SchoolAccountTransactionHistory();
  $s_info = new CoreStudent();
  $pay_r = new PaymentsReceived();
  $sc = new CoreSchoolClass();
	echo "<table class ='table-bordered table table-striped'>";
	echo "<tr  class=success ><th>SI No.</th>";
	foreach($tr_list as $s){
			echo "<th class='text-center'>".Html::activeLabel($s_trans,$s)."</th>";
	}
  foreach($pr_list as $s){
      echo "<th class='text-center'>".Html::activeLabel($pay_r,$s)."</th>";
  }
  foreach($st_list as $s){
      echo "<th class='text-center'>".Html::activeLabel($s_info,$s)."</th>";
  }
  foreach($sc_list as $s){
      echo "<th class='text-center'>".Html::activeLabel($sc,$s)."</th>";
  }

	echo "</tr>";
	$i = 1;
	foreach($student_data as $t=>$sd)
	{
		if(($i%2) == 0)
		 {
			$bgcolor = "#FFFFFF";
		 }
		 else
		 {
			$bgcolor = "#E3E3E3";
		 }
		echo '<tr align=center bgcolor='.$bgcolor.'>';
		echo "<td style='text-align:center;'>".$i."</td>";
		foreach($tr_list as $s){
      if($s == 'date_created'){
        echo "<td class='text-center'>".date('M d, Y | g:i:s', strtotime($sd[$s]))."</td>";
      }else{
        echo "<td class='text-center'>".$sd[$s]."</td>";
      }

		}
    foreach($pr_list as $s){
    echo "<td class='text-center'>".$sd[$s]."</td>";
    }
    foreach($st_list as $s){
    echo "<td class='text-center'>".$sd[$s]."</td>";
    }
    foreach($sc_list as $s){
    echo "<td class='text-center'>".$sd[$s]."</td>";
    }
	$i++;
	echo "</tr>";

	}

echo "</table>";
}
else
	echo  "<h1 style=\"color:red;text-align:center;margin-top:10%;margin-right:60%\">No Record To Display</h1>";
?>

</div>

</div>


<div class="container">
    <div class="checkbox checkbox-info">
        <div>
            <input type="checkbox" class="checkall selectall" id="check_all_id">
            <label for="check_all_id"><b> Check All</b> </label>
        </div>
    </div>
    <div class="row">
    <div class="col-sm">
        <div>
            <input type="checkbox" class="individual" name="s_trans[]" id="date_created" value="pr.date_created"/>
            <label for="date_created" class="checkbox-inline"> &nbsp;Date </label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="s_trans[]" id="description" value="pr.channel_memo"/>
            <label for="description" class="checkbox-inline">&nbsp;Description </label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="s_trans[]" id="amount" value="pr.amount"/>
            <label for="amount" class="checkbox-inline">&nbsp;Amount </label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="sclas[]" id="class_code" value="ls.class_code"/>
            <label for="class_code" class="checkbox-inline">&nbsp;Class</label>
        </div>
    </div>

    <div class="col-sm">
        <div>
            <input type="checkbox" class="individual" name="pch[]" id="channel" value="pr.channel_memo"/>
            <label for="channel" class="checkbox-inline">&nbsp; Channel</label>
        </div>

        <div>
            <input type="checkbox" class="individual" name="pch[]" id="payment_type" value="pr.channel_payment_type"/>
            <label for="payment_type" class="checkbox-inline">&nbsp;Ch Payment Type</label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="pch[]" id="ch_depositor_name"
                   value="pr.channel_depositor_name"/>
            <label for="ch_depositor_name" class="checkbox-inline">&nbsp;Ch Depositor Name</label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="pch[]" id="ch_depositor_branch"
                   value="pr.channel_depositor_branch"/>
            <label for="ch_depositor_branch" class="checkbox-inline">&nbsp;Ch Depositor Branch
            </label>
        </div>
    </div>
    <div class="col-sm">
        <div>
            <input type="checkbox" class="individual" name="sinfo[]" id="first_name" value="si.first_name"/>
            <label for="first_name" class="checkbox-inline">&nbsp; Student First Name</label>
        </div>

        <div>
            <input type="checkbox" class="individual" name="sinfo[]" id="middle_name" value="si.middle_name"/>
            <label for="middle_name" class="checkbox-inline">&nbsp;Student Middle Name</label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="sinfo[]" id="last_name" value="si.last_name"/>
            <label for="last_name" class="checkbox-inline">&nbsp;Student Last Name </label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="sinfo[]" id="registration_number"
                   value="si.school_student_registration_number"/>
            <label for="registration_number" class="checkbox-inline">&nbsp;Registration Number </label>
        </div>
    </div>
    <div class="col-sm">
        <div>
            <input type="checkbox" class="individual" name="sinfo[]" id="guardian_name" value="si.guardian_name"/>
            <label for="guardian_name" class="checkbox-inline">&nbsp;Guardian </label>
        </div>
        <div>
            <input type="checkbox" class="individual" name="sinfo[]" id="guardian_phone" value="si.guardian_phone"/>
            <label for="guardian_phone" class="checkbox-inline">&nbsp;Guardian Phone </label>
        </div>

        <div>
            <input type="checkbox" class="individual" name="pch[]" id="channel_trans_id" value="pr.channel_trans_id"/>
            <label for="channel_trans_id" class="checkbox-inline">&nbsp; Channel Trans id</label>
        </div>

        <div>
            <input type="checkbox" class="individual" name="pch[]" id="channel_depositor_phone"
                   value="pr.channel_depositor_phone"/>
            <label for="channel_depositor_phone" class="checkbox-inline">&nbsp; Payer Phone</label>
        </div>
    </div>
</div>

<?php




$script = <<< JS

    

$(".checkall").click(function(){
$(".checkbox_inline").prop("checked",$(this).prop("checked"));
});


$(".selectall").click(function(){
$(".individual").prop("checked",$(this).prop("checked"));
});
JS;
$this->registerJs($script);
?>

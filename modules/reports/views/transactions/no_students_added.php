<?php

use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
use yii\jui\DatePicker;

//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeesDueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Schools without Student Data';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letters">
    <div class="fees-due-index hd-title" data-title="Schools without Student Data">


        <div class="row">

            <div class="col-md-12">
                <div class="col-sm-3 col-md-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> &nbsp;Schools Without Student Data</h3></div>

            </div>
            <div class="col-md-12">

                <?php $form = ActiveForm::begin([
                    'action' => ['/reports/transactions/schools-without-student-data'],
                    'method' => 'get',
                    'options' => ['class' => 'formprocess'],
                ]); ?>
                <ul style="list-style-type:none" class="row">

                    <div class="col-sm-3">

                        <?php
                        $items = ArrayHelper::map(\app\modules\schoolcore\models\RefRegion\BranchRegion::find()->all(), 'id', 'bank_region_name');
                        echo $form->field($searchModel, 'branch_region')
                            ->dropDownList(
                                $items,           // Flat array ('id'=>'label')
                                ['prompt' => 'Select Branch Region',
                                    'onchange' => '$.post( "' . Url::to(['/schoolcore/core-school/branches']) . '?id="+$(this).val(), function( data ) {
                            $( "select#branches" ).html(data);
                        });']    // options
                            )->label(false); ?>

                    </div>
                    <div class="col-sm-3">

                        <?= $form->field($searchModel, 'branch_id')->dropDownList([],

                            ['id' => 'branches',
                            ])->label(false)
                        ?>

                    </div>

                    <li class="col-md-2"><?= Html::submitButton('Search <i class="fa fa-search"></i>', ['class' => 'btn btn-info btn-sm']) ?></li>
                </ul>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="col-sm-12 col-md-12 no-padding" style="padding-top: 20px !important;">
                <div class="pull-right">


                    <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                        <?php
                        echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                            'class' => 'btn btn-sm btn-danger',
                            'target' => '_blank',
                            'data-toggle' => 'tooltip',
                            'title' => 'Will open the generated PDF file in a new window'
                        ]);
                        echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                            'class' => 'btn btn-sm btn-success',
                            'target' => '_blank'
                        ]);
                        ?>
                    </ul>

                </div>

            </div>

            <div class="col-md-12">
                <div class="">
                    <table class="table table-striped">
                        <thead  class="bg-colorz table thead">
                        <tr >

                            <th>Bank Region</th>
                            <th>Country Region</th>
                            <th>Branch</th>
                            <th>School ID</th>
                            <th>School Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($dataProvider) :
                            foreach($dataProvider as $k=>$v) : ?>
                                <?php
                                Yii::trace($v);
                                ?>
                                <tr>
                                    <?php ?>
                                    <td><?= ($v['bank_region_name']) ? $v['bank_region_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['branch_name']) ? $v['branch_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['external_school_code']) ? $v['external_school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>



                                    <td></td>
                                </tr>
                            <?php endforeach;
                        else :
                            ?>
                            <tr><td colspan="7">No School Lists found </td></tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>
                <?= LinkPager::widget([
                    'pagination' => $pages['pages'],
                ]);?>


            </div>
        </div>

    </div>
</div>

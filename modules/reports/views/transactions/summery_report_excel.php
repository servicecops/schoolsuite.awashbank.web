<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transaction History');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="hd-title" data-title="School Transaction History">
<?php if(!empty($query))
echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
?>
<br>
<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
<?php

if(!empty($student_data))
{
    echo "<table class ='table-bordered table table-striped'>";
    echo "<tr>";
    echo "<th class='text-center'>School Code</th>";
    echo "<th class='text-center'>School name</th>";
    echo "<th class='text-center'>Bank Name</th>";
    echo "<th class='text-center'>Total Amount Transacted</th>";
    echo "<th class='text-center'>Number of Transactions</th>";

    echo "</tr>";
    $i = 1;
    foreach($student_data as $t=>$v)
    {
        if(($i%2) == 0)
         {
            $bgcolor = "#FFFFFF";
         }
         else
         {
            $bgcolor = "#E3E3E3";
         }
        echo '<tr align=center bgcolor='.$bgcolor.'>';
        echo "<td class='text-center'>".$v['sc']."</td>";
        echo "<td class='text-center'>".$v['sn']."</td>";
        echo "<td class='text-center'>".$v['bn']."</td>";
        echo "<td class='text-center'>".number_format($v['sum'], 2)."</td>";
        echo "<td class='text-center'>".number_format($v['count'])."</td>";
        echo "</tr>";
        $i++;

    }

echo "</table>";
}
?>
</div>
</div>
</div>

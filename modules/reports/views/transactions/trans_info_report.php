<?php

use app\modules\paymentscore\models\PaymentsReceived;
use app\modules\paymentscore\models\SchoolAccountTransactionHistory;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transactions');
$this->params['breadcrumbs'][] = $this->title;

$_SESSION['query']=$query;
$_SESSION['selected_list']=$selected_list;
$_SESSION['tr_list']=$tr_list;
$_SESSION['st_list']=$st_list;
$_SESSION['sc_list']=$sc_list;
$_SESSION['pr_list']=$pr_list;
?>

<div class="row card" data-title="School Transactions">
    <div class="col-md-12 card-body">

        <div class="box-header">
          <h3 class="box-title"><i class="fa fa-info-circle"></i> <?= $this->title ?></h3>
          <div class="box-tools pull-right">
        <?php echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['transactions/trans'],['class'=>'btn btn-sm btn-danger', ]);?>
    		<?php echo Html::a('<i class="fa fa-file-excel"></i> Excel',['transactions/selected-trans-list','translistexcelexport'=>'translistexcel'],array('title'=>'Export to Excel','target'=>'_blank','class'=>'btn btn-sm btn-success', 'style'=>'color:#fff'));?>
    		<?php echo Html::a('<i class="fa fa-file-pdf"></i> PDF',array('transactions/selected-trans-list','translistexport'=>'translistpdf'),array('title'=>'Export to PDF','target'=>'_blank','class'=>'btn btn-sm btn-warning', 'style'=>'color:#fff')); ?>
          </div>
        </div><!-- /.box-header -->

 <div class="col-md-12 box-body table  table-responsive no-padding">
<?php
if(!empty($student_data))
{
	$s_trans = new SchoolAccountTransactionHistory();
  $s_info = new CoreStudent();
  $pay_r = new PaymentsReceived();
  $sc = new CoreSchoolClass();


	echo "<table class ='table table-striped'>";
	echo "<tr>";
	foreach($tr_list as $s){
			echo "<th class='text-center'>".Html::activeLabel($s_trans,$s)."</th>";
	}
  foreach($pr_list as $s){
      echo "<th class='text-center'>".Html::activeLabel($pay_r,$s)."</th>";
  }
  foreach($st_list as $s){
      echo "<th class='text-center'>".Html::activeLabel($s_info,$s)."</th>";
  }
  foreach($sc_list as $s){
      echo "<th class='text-center'>".Html::activeLabel($sc,$s)."</th>";
  }

	echo "</tr>";
	foreach($student_data as $t=>$sd)
	{
		echo "<tr>";
		foreach($tr_list as $s){
      if($s == 'date_created'){
        echo "<td class='text-center'>".date('M d, Y | g:i:s', strtotime($sd[$s]))."</td>";
      }else{
        echo "<td class='text-center'>".$sd[$s]."</td>";
      }

		}
    foreach($pr_list as $s){
    echo "<td class='text-center'>".$sd[$s]."</td>";
    }
    foreach($st_list as $s){
    echo "<td class='text-center'>".$sd[$s]."</td>";
    }
    foreach($sc_list as $s){
    echo "<td class='text-center'>".$sd[$s]."</td>";
    }

		echo "</tr>";

	}

echo "</table>";
}
?>
<div style="padding-left:2em"><?= LinkPager::widget([ 'pagination' => $pages]); ?></div>

</div>
</div>
</div>


<?php
$script = <<< JS
$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#trans_report").addClass('active');
  });
JS;
$this->registerJs($script);
?>



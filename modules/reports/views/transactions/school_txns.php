<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
use yii\jui\DatePicker;

//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FeesDueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inactive Schools';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="letters">
    <div class="fees-due-index hd-title" data-title="Inactive Schools">


        <div class="row">

            <div class="col-md-12">
                <div class="col-sm-3 col-md-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> &nbsp;Inactive Schools</h3></div>
                <div class=" col-md-12 ">
                    <?php $form = ActiveForm::begin(['action' => ['inactive-schools'], 'method' => 'get',
                        'id' => 'trans-form',
                        'options' => ['class' => 'form-horizontal formprocess']]); ?>
                    <div class="col-md-4 ">
                        <?= DatePicker::widget([
                            'name' => 'date_from',
                            'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : null,
                            'dateFormat' => 'yyyy-MM-dd',
                            'clientOptions' => [
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                            'options' => [
                                'class' => 'form-control input-sm',
                                'Placeholder' => 'Select Last Transaction Date',
                                'name' => 'Trans[date_from]',
                                'readonly' => true
                            ],
                        ]); ?>
                    </div>


                    <div class="col-md-2">
                        <?= Html::submitButton('Find', ['class' => 'btn btn-default btn-md']) ?>

                    </div>
                </div>
                <?php ActiveForm::end(); ?>

                <div class="col-sm-12 col-md-12 no-padding" style="padding-top: 20px !important;">
                    <div class="pull-right">


                            <ul class="row pull-right menu-list no-padding" style="list-style-type:none">
                                <?php
                                echo Html::a('<i class="fa far fa-file-pdf-o"></i> <span>Download Pdf</span>', ['/export-data/export-to-pdf', 'model' => get_class($searchModel)], [
                                    'class' => 'btn btn-sm btn-danger',
                                    'target' => '_blank',
                                    'data-toggle' => 'tooltip',
                                    'title' => 'Will open the generated PDF file in a new window'
                                ]);
                                echo Html::a('<i class="fa far fa-file-excel-o"></i> Download Excel', ['/export-data/export-excel', 'model' => get_class($searchModel)], [
                                    'class' => 'btn btn-sm btn-success',
                                    'target' => '_blank'
                                ]);
                                ?>
                            </ul>

                    </div>

                </div>

            </div>
            <div class="col-md-12">
                <div class="">
                    <table class="table table-striped">
                        <thead  class="bg-colorz table thead">
                        <tr>

                            <th>Bank Region</th>
                            <th>Country Region</th>
                            <th>Branch</th>
                            <th>School ID</th>
                            <th>School Name</th>
                            <th>Number of Students</th>
                            <th>Last Transaction Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($dataProvider) :
                            foreach($dataProvider as $k=>$v) : ?>
                                <?php
                                Yii::trace($v);
                                ?>
                                <tr>
                                    <?php ?>
                                    <td><?= ($v['bank_region_name']) ? $v['bank_region_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['branch_name']) ? $v['branch_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['external_school_code']) ? $v['external_school_code'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['Stds']) ? $v['Stds'] : '<span class="not-set">(not set) </span>' ?></td>
                                    <td><?= ($v['last_transaction_date']) ? date('d/m/y H:i', strtotime($v['last_transaction_date'])) : '<span class="not-set">(not set) </span>' ?></td>



                                    <td></td>
                                </tr>
                            <?php endforeach;
                        else :
                            ?>
                            <tr><td colspan="7">No School Lists found </td></tr>
                        <?php endif; ?>

                        </tbody>
                    </table>
                </div>
                <?= LinkPager::widget([
                    'pagination' => $pages['pages'],
                ]);?>


            </div>
        </div>

    </div>
</div>

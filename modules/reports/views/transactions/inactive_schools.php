<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transaction Summary');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class=" row hd-title" data-title="School Transaction Summary">
        <div class=" col-md-12 ">
            <?php $form = ActiveForm::begin(['action' => ['inactive-schools'], 'method' => 'get',
                'id' => 'trans-form',
                'options' => ['class' => 'form-horizontal formprocess']]); ?>
            <div class="col-md-4 ">
                <?= DatePicker::widget([
                    'name' => 'date_from',
                    'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : null,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control input-sm',
                        'Placeholder' => 'From',
                        'name' => 'Trans[date_from]',
                        'readonly' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-4 ">
                <?= DatePicker::widget([
                    'name' => 'date_to',
                    'value' => (isset($_GET['date_to'])) ? $_GET['date_to'] : null,
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1900:' . (date('Y') + 1),
                        'autoSize' => true,
                    ],
                    'options' => [
                        'class' => 'form-control input-sm',
                        'Placeholder' => 'To',
                        'name' => 'Trans[date_to]',
                        'readonly' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-2">
                <?= Html::submitButton('Find', ['class' => 'btn btn-default btn-md']) ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="col-md-12"></div>
            <div class="col-md-12">

            <div class="col-md-12 box-header">
                <h3 class="box-title"><i
                            class="fa fa-info-circle"></i> <?= $this->title . " ( " . date('M d, y', strtotime($from_date)) . " - " . date('M d, y', strtotime($to_date)) . ")" ?>
                </h3>
                <div class="col-md-12 box-tools pull-right">
                    <div class=" pull-right">
<!--                        --><?php //echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['transactions/trans'], ['class' => 'btn btn-back', 'style' => 'color:#fff']); ?>
<!--                        --><?php //echo Html::a('<i class="fa fa-file-excel-o"></i> Excel', ['transactions/summary-perf-list', 'summerylistexcelexport' => 'summerylistexcel'], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn btn-info', 'style' => 'color:#fff')); ?>
<!--                        --><?php //echo Html::a('<i class="fa fa-file-pdf-o"></i> PDF', array('transactions/summary-perf-list', 'summerylistexport' => 'summerylistpdf'), array('title' => 'Export to PDF', 'target' => '_blank', 'class' => 'btn btn-warning', 'style' => 'color:#fff')); ?>
<!--                    -->
                    </div>
                </div>
            </div><!-- /.box-header -->

            <div class="col-md-12 box-body table-responsive ">
                <?php
                if (!empty($student_data)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'>Bank Region</th>";
                    echo "<th class='text-center'>School Code</th>";
                    echo "<th class='text-center'>School name</th>";

                    echo "<th class='text-center'>Number of Students</th>";
                    echo "<th class='text-center'>Transaction Value</th>";

                    echo "</tr>";
                    foreach ($student_data as $t => $v) {
                        Yii::trace($student_data);
                        echo "<tr>";

                        echo "<td class='text-center'>" . $v['bank_region_name'] . "</td>";
                        echo "<td class='text-center'>" . $v['school_code'] . "</td>";
                        echo "<td class='text-center'>" . $v['school_name'] . "</td>";
                        echo "<td class='text-center'>" . number_format($v['number_of_students']) . "</td>";
                        echo "<td class='text-center'>" . number_format($v['transaction_value']) . "</td>";
                        echo "</tr>";

                    }


                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>GRAND TOTAL</th>";
                    echo "<th></th>";
                    echo "<th></th>";

                    echo "<th class='text-center'>" . number_format($count) . "</th>";
                    echo "<th class='text-center'>" . number_format($sum) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                }
                ?>
            </div>
        </div>
    </div>


<?php
$script = <<< JS
$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#summary_report").addClass('active');
  });
JS;
$this->registerJs($script);

<?php

use app\modules\reports\models\BalanceAdjustmentHelperModel;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\SchoolCampuses;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = Yii::t('app', 'Balance Adjustment Report');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row card" data-title='Balance Adjustment Report'>
        <div class="card-body">
        <div class="box-info box-solid">
            <?php $form = ActiveForm::begin([
                'action' => ['balance-adjustment-report'],
                'method' => 'post',
                'id' => 'stu_info-form',
                'options' => ['class' => 'formprocess'],
            ]); ?>
            <div class="box-body">
                <div class="row">
                    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?php
                            $url = Url::to(['/schoolcore/core-school/schoollist']);
                            $selectedSchool = empty($model->school_id) ? '' : CoreSchool::findOne($model->school_id)->school_name;
                            echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                                'initValueText' => $selectedSchool, // set the initial display text
//                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'placeholder' => 'Filter School',
                                    'id' => 'student_school_search',
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],

                                ],
                            ])->label('School'); ?>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?= $form->field($model, 'student_campus')->dropDownList(['prompt' => 'Select Campus'], ['id' => 'student_campus_list']) ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?= $form->field($model, 'class_id')->dropDownList(['prompt' => 'Select Class'], ['id' => 'student_class_list']) ?>
                        </div>

                    <?php elseif (\app\components\ToWords::isSchoolUser()) : ?>
                        <?php if (Yii::$app->user->identity->school->hasModule('SCHOOL_CAMPUSES')) : ?>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <?= $form->field($model, 'student_campus')->dropDownList(ArrayHelper::map(SchoolCampuses::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'campus_name'), ['prompt' => '---All Campuses---']); ?>
                            </div>
                        <?php endif; ?>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?= $form->field($model, 'class_id')->dropDownList(ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'class_code'), ['prompt' => '---All Courses---']); ?>
                        </div>

                    <?php endif; ?>

                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <?= $form->field($model, 'date_from')->widget(DatePicker::className(),
                            [
                                'dateFormat' => 'yyyy-MM-dd',
                                'clientOptions' => [
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '1900:' . (date('Y') + 1),
                                    'autoSize' => true,
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                    'Placeholder' => 'Select date',
                                ],
                            ])->label('From'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <?= $form->field($model, 'date_to')->widget(DatePicker::className(),
                            [
                                'dateFormat' => 'yyyy-MM-dd',
                                'clientOptions' => [
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '1900:' . (date('Y') + 1),
                                    'autoSize' => true,
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                    'Placeholder' => 'Select date',
                                ],
                            ])->label('To'); ?>
                    </div>



                </div>


            </div> <!--/ box-body -->
            <div class="mt-3 mb-3">
                <?= Html::submitButton('Generate', ['class' => 'btn btn-primary']) ?>
                <?php ActiveForm::end(); ?>
            </div> <!--/ box -->
        </div>

    <div class="row">





                    <h3><i
                                class="fa fa-info-circle"></i> <?= $this->title ?>
                    </h3>




                <div class="table-responsive no-padding">

                    <div class="pull-right">
                    </div>
                    <?php
                    if (!empty($student_data)) {
                        echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                        echo "<tr>";
                        echo "<th class='text-center'>Date</th>";
                        echo "<th class='text-center'>Payment Code</th>";
                        echo "<th class='text-center'>Student name</th>";
                        echo "<th class='text-center'>Student Class</th>";
                        echo "<th class='text-center'>User Name</th>";
                        echo "<th class='text-center'>IP Address</th>";
                        echo "<th class='text-center'>Details</th>";
                        echo "<th class='text-center'>Reason</th>";
                        echo "<th class='text-center'>Balance Before</th>";
                        echo "<th class='text-center'>Amount posted</th>";
                        echo "<th class='text-center'>Balance After</th>";

                        echo "</tr>";
                        foreach ($student_data as $t => $v) {
                            echo "<tr>";
                            echo "<td class='text-center'>" . $v['event_date'] . "</td>";
                            echo "<td class='text-center'>" . $v['student_code'] . "</td>";
                            echo "<td class='text-center'>" . $v['student_name'] . "</td>";
                            echo "<td class='text-center'>" . $v['class_description'] . "</td>";
                            echo "<td class='text-center'>" . $v['user_full_name'] . "</td>";
                            echo "<td class='text-center'>" . $v['ip_address'] . "</td>";
                            echo "<td class='text-center'>" . $v['event_action'] . "</td>";
                            echo "<td class='text-center'>" . $v['reason'] . "</td>";
                            echo "<td class='text-center'>" . $v['balance_before'] . "</td>";
                            echo "<td class='text-center'>" . $v['amount'] . "</td>";
                            echo "<td class='text-center'>" . $v['balance_after'] . "</td>";
                            echo "</tr>";

                        }

                        echo "</table>";
                    } else echo "No results found";
                    ?>
                </div>

        <div class="col-xs-12"><?= LinkPager::widget(['pagination' => $pages]); ?></div>
    </div>
    </div>
    </div>
<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->class_id;
//
//$campusurl = Url::to(['/school-information/campuslists']);
//$campus = $model->student_campus;


$feesUrl = Url::to(['/schoolcore/core-school/feeslists' ]);

$campusurl = Url::to(['/schoolcore/core-school/campuslists']);
$campus = $model->student_campus;



$script = <<< JS


    
    
var schoolChanged = function() {
        var sch = $("#student_school_search").val();
        if(sch){
            $.get('$url', {id : sch}, function( data ) {
                    $('#student_class_list').html(data);
                    $('#student_class_list').val('$cls');
                });
        
        $.get('$campusurl', {id : sch}, function( data ) {
                    $('#student_campus_list').html(data);
                    $('#student_campus_list').val('$campus');
                });
;
        }
        
    }
    



$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#balance_adjustment_report").addClass('active');
    
    $('body').on('change', '#student_school_search', function(){
         schoolChanged();
    });
    
    //By default trigger change in case school is already set
    schoolChanged();
    
  });
JS;
$this->registerJs($script);

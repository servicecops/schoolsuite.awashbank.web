<?php

use app\modules\paymentscore\models\PaymentChannels;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\SchoolCampuses;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;

$this->title = 'School Transaction History';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
// $model = new StuMaster();
// $info = new StuInfo();
// $city =new City();
?>

<div class="row " data-title="School Transaction History">
    <div class="card-body">

        <h2 class="page-header">
            <i class="fa fa-search"></i> School Transaction History &nbsp;<span style="font-size:22px;">( Select Criteria)</span>
        </h2>


        <?php if (\Yii::$app->getSession()->hasFlash('transerror')) : ?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?= \Yii::$app->getSession()->getFlash('transerror'); ?>
            </div>
        <?php endif; ?>

        <div class="box-info box-solid">
            <?php $form = ActiveForm::begin([
                'action' => ['trans'],
                'method' => 'get',
                'id' => 'trans-form',
                'options' => ['class' => 'formprocess'],
            ]); ?>
            <div class="box-body">
                <div class="row">
                    <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?php
                            $url = Url::to(['/schoolcore/core-school/schools']);
                            $selectedSchool = empty($model->account_id) ? '' : CoreSchool::findOne($model->school_account_id)->school_name;
                            echo $form->field($model, 'account_id')->widget(Select2::classname(), [
                                'initValueText' => $selectedSchool, // set the initial display text
//                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => [
                                    'placeholder' => 'Filter School',
                                    'name' => 'Trans[school]',
                                    'id'=>'student_school_search',
                                    'onchange' => '$.post( "' . Url::to(['class-lists']) . '?id="+$(this).val(), function( data ) {
                            $( "select#student_class_list" ).html(data);
                        });'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],

                                ],
                            ])->label('School'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($stu, 'campus_id')->dropDownList(['prompt' => 'Select Campus'], ['id' => 'student_campus_list']) ?>
                        </div>



                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($stu, 'class_id')->dropDownList(['' => 'Select Class'], ['name' => 'Trans[class]', 'id' => 'student_class_list']) ?>
                        </div>

                    <?php elseif (\app\components\ToWords::isSchoolUser()) : ?>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($stu, 'class_id')->dropDownList(
                                ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'class_code'), ['prompt' => '-- ALL --', 'name' => 'Trans[class]', 'id' => 'student_class_list']) ?>
                        </div>
                        <?php if (Yii::$app->user->identity->school->hasModule('SCHOOL_CAMPUSES')) : ?>
                            <div class="col-xs-12 col-sm-6 col-lg-6">
                                <?= $form->field($stu, 'campus_id')->dropDownList(ArrayHelper::map(SchoolCampuses::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'campus_name'), ['prompt' => '---All Campuses---']); ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'payment_id')->dropDownList(
                            ArrayHelper::map(PaymentChannels::find()->all(), 'id', 'channel_code'), ['prompt' => '-- ALL --', 'name' => 'Trans[channel]']) ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'date_from')->widget(DatePicker::className(),
                            [
                                'model' => $model,
                                'attribute' => 'date_from',
                                'dateFormat' => 'yyyy-MM-dd',
                                'clientOptions' => [
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '1900:' . (date('Y') + 1),
                                    'autoSize' => true,
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                    'Placeholder' => 'Select date',
                                    'name' => 'Trans[date_from]',
                                    'readonly' => true
                                ],
                            ])->label('From'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'date_to')->widget(DatePicker::className(),
                            [
                                'model' => $model,
                                'attribute' => 'date_to',
                                'dateFormat' => 'yyyy-MM-dd',
                                'clientOptions' => [
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '1900:' . (date('Y') + 1),
                                    'autoSize' => true,
                                ],
                                'options' => [
                                    'class' => 'form-control',
                                    'Placeholder' => 'Select date',
                                    'name' => 'Trans[date_to]',
                                    'readonly' => true
                                ],
                            ])->label('To'); ?>
                    </div>

                </div>
                <hr>
                <div class="row">
                    <?php echo $this->render('trans_select_form'); ?>
                </div>

            </div> <!--/ box-body -->
            <div class="box-footer">
                <?= Html::submitButton('Generate', ['class' => 'btn btn-primary generate-btn']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php
//$url = Url::to(['/schoolcore/core-school-classes/lists']);
//$cls = $stu->class_id;
//
//
//
//$script = <<< JS
//var schoolChanged = function() {
//        var sch = $("#student_school_search").val();
//        $.get('$url', {id : sch}, function( data ) {
//                    $('#student_class_list').html(data);
//                    $('#student_class_list').val('$cls');
//                });
//
//
//    }
//
//
//
//
//$("document").ready(function(){
// $(".active").removeClass('active');
//    $("#reports").addClass('active');
//    $("#trans_report").addClass('active');
//
//    $('body').on('change', '#student_school_search', function(){
//         schoolChanged();
//    });
//
//  });
//JS;
//$this->registerJs($script);
//?>


<?php
$url = Url::to(['/schoolcore/core-school-classes/lists']);
$cls = $stu->class_id;


$campusurl = Url::to(['/schoolcore/core-school/campuslists']);
$campus = $stu->campus_id;

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#student_school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('#student_class_list').html(data);
                    $('#student_class_list').val('$cls');
                });
        
        $.get('$campusurl', {id : sch}, function( data ) {
                    $('#student_campus_list').html(data);
                    $('#student_campus_list').val('$campus');
                });
    }
    



$("document").ready(function(){ 
 $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#trans_report").addClass('active');
    
    $('body').on('change', '#student_school_search', function(){
         schoolChanged();
    });
    
  });
JS;
$this->registerJs($script);
?>

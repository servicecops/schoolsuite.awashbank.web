<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Performance Report');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="hd-title" data-title="School Performance Report">
<?php if(!empty($query))
echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
?>
<br>
<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
<?php

if(!empty($student_data))
{
    echo "<table class ='table-bordered table table-striped'>";
    echo "<tr>";
    echo "<th class='text-center'>School Code</th>";
    echo "<th class='text-center'>School name</th>";
    echo "<th class='text-center'>Number of Students</th>";

    echo "<th class='text-center'>Transaction Value</th>";

    echo "</tr>";
    $i = 1;
    foreach($student_data as $t=>$v)
    {       
        if(($i%2) == 0)
         {
            $bgcolor = "#FFFFFF";
         }
         else
         {
            $bgcolor = "#E3E3E3";
         }      
        echo '<tr align=center bgcolor='.$bgcolor.'>';
        echo "<td class='text-center'>".$v['school_code']."</td>";
        echo "<td class='text-center'>".$v['school_name']."</td>";
        echo "<td class='text-center'>".number_format($v['number_of_students'])."</td>";
        echo "<td class='text-center'>".number_format($v['transaction_value'])."</td>";
        echo "</tr>";
        $i++;  
            
    }


    echo "<tr>";
    echo "<th></th>";
    echo "<th></th>";
    echo "<th></th>";
    echo "<th></th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th class='text-center' rowspan='2'>GRAND TOTAL</th>";
    echo "<th></th>";

    echo "<th class='text-center'>".number_format($count)."</th>";
    echo "<th class='text-center'>".number_format($sum)."</th>";
    echo "</tr>";

    echo "</table>";
}
?>
</div>
</div>
</div>
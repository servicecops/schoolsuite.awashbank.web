<?php


/* @var $this \yii\web\View */

use app\models\Classes;
use app\models\SchoolCampuses;
use app\models\SchoolInformation;
use app\modules\reports\models\TransactionFeePerformanceHelperModel;
use app\modules\reports\models\TransactionFeeUnpaidPerformanceHelperModel;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\jui\DatePicker;

$this->title = Yii::t('app', 'Fees Performance Report');
$this->params['breadcrumbs'][] = $this->title;

$ownSchool = \Yii::$app->user->can('own_sch');
$userSchool = null;
$userSchoolId = null;
if ($ownSchool) {
    $userSchool = Yii::$app->user->identity->school;
    $userSchoolId = $userSchool->id;
}
?>
<div class="row" data-title='Fees Performance Report'>


                <div class="box-header">
                    <h3 class="box-title"><i
                                class="fa fa-info-circle"></i> <?= $this->title ?>
                    </h3>

                </div><!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                    <?php
                    if ($paid) {
                        ?>

                        <div class="box-tools pull-right">

                            <?php
                            echo Html::a('<i class="fa fa-file-excel-o"></i> Excel', ['/export-data/export-pexcel', 'model' => get_class(new TransactionFeePerformanceHelperModel())], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn btn-info', 'style' => 'color:#fff')); ?>
                        </div>
                        <?php
                        if (!empty($student_data)) {

                            echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                            echo "<tr>";
                            echo "<th class='text-center'>Date</th>";

                            echo "<th class='text-center'>Student Code</th>";
                            echo "<th class='text-center'>Student name</th>";
                            echo "<th class='text-center'>Fee Name</th>";
                            echo "<th class='text-center'>Fee Paid Amount</th>";
                            echo "<th class='text-center'>Details</th>";
                            echo "<th class='text-center'>Class</th>";
                            echo "<th class='text-center'>Student Number</th>";
                            echo "<th class='text-center'>Guardian Number</th>";

                            echo "</tr>";
                            foreach ($student_data as $t => $v) {
                                echo "<tr>";
                                if (isset($v['payment_date'])) {
                                    echo "<td class='text-center'>" . $v['payment_date'] . "</td>";

                                } else {
                                    echo "<td class='text-center'>" . "--" . "</td>";

                                }
                                echo "<td class='text-center'>" . $v['student_code'] . "</td>";
                                echo "<td class='text-center'>" . $v['student_name'] . "</td>";
                                echo "<td class='text-center'>" . $v['fee_name'] . "</td>";
                                echo "<td class='text-center'>" . number_format($v['amount'], 2) . "</td>";
                                echo "<td class='text-center'>" . $v['description'] . "</td>";
                                echo "<td class='text-center'>" . $v['class_code'] . "</td>";
                                echo "<td class='text-center'>" . $v['student_phone'] . "</td>";
                                echo "<td class='text-center'>" . $v['guardian_phone'] . "</td>";
                                echo "</tr>";

                            }

                            echo "</table>";
                        }else{
                            echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                            echo "<tr>";
                            echo "<th class='text-center'>No Matching Details Found</th>";
                            echo "</tr>";

                            echo "</table>";
                        }
                    } else {
                        ?>
                        <div class="box-tools pull-right">

                            <?php

                            echo Html::a('<i class="fa fa-file-excel-o"></i> Excel ', ['/export-data/export-pexcel', 'model' => get_class(new TransactionFeeUnpaidPerformanceHelperModel())], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn btn-info', 'style' => 'color:#fff')); ?>
                        </div>
                        <?php
                        if (!empty($student_data)) {
                            echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                            echo "<tr>";
                            echo "<th class='text-center'>Date</th>";

                            echo "<th class='text-center'>Student Code</th>";
                            echo "<th class='text-center'>Student name</th>";
                            echo "<th class='text-center'>Fee Name</th>";
                            echo "<th class='text-center'>Fee Amount</th>";
                               echo "<th class='text-center'>Class</th>";
                            echo "<th class='text-center'>Student Number</th>";
                            echo "<th class='text-center'>Guardian Number</th>";

                            echo "</tr>";
                            foreach ($student_data as $t => $v) {
                                echo "<tr>";
                                if (isset($v['date_created'])) {
                                    echo "<td class='text-center'>" . $v['date_created'] . "</td>";

                                } else {
                                    echo "<td class='text-center'>" . "--" . "</td>";

                                }
                                echo "<td class='text-center'>" . $v['student_code'] . "</td>";
                                echo "<td class='text-center'>" . $v['student_name'] . "</td>";
                                echo "<td class='text-center'>" . $v['fee_name'] . "</td>";
                                echo "<td class='text-center'>" . number_format($v['payment_amount'], 2) . "</td>";
                                echo "<td class='text-center'>" . $v['class_code'] . "</td>";
                                echo "<td class='text-center'>" . $v['student_phone'] . "</td>";
                                echo "<td class='text-center'>" . $v['guardian_phone'] . "</td>";
                                echo "</tr>";

                            }

                            echo "</table>";
                        }else{
                            echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                            echo "<tr>";
                            echo "<th class='text-center'>No Matching Details Found</th>";
                            echo "</tr>";

                            echo "</table>";
                        }
                    }
                    ?>
                </div>

        <div class="col-xs-12"><?= LinkPager::widget(['pagination' => $pages]); ?></div>

</div>

<?php
$url = Url::to(['/classes/lists']);
$cls = $model->class_id;

$campusurl = Url::to(['/school-information/campuslists']);
$campus = $model->student_campus;


$feesUrl = Url::to(['/school-information/feeslists']);

$script = <<< JS

var changeFeesOptions = function(p) {
        $('#fees_list').html('');
        for (var key in p) {
            if (p.hasOwnProperty(key)) {
                $('#fees_list').append('<option value='+key+'>'+p[key]+'</option>')
            }
        }
    }
    
    var populateFeesList = function(){
    var sch = $("#student_school_search").val();
    if(!sch) {
        sch = '$userSchoolId';
    }
    if(!sch) return;
    
    $.get('$feesUrl', {id : sch}, function(data){
                changeFeesOptions(data);
            })  
    }
    
    
var schoolChanged = function() {
    var sch = $("#student_school_search").val();
    if(!sch) {
        sch = '$userSchoolId';
    }
    if(!sch) return;
            $.get('$url', {id : sch}, function( data ) {
                    $('#student_class_list').html(data);
                    $('#student_class_list').val('$cls');
                });
        
        $.get('$campusurl', {id : sch}, function( data ) {
                    $('#student_campus_list').html(data);
                    $('#student_campus_list').val('$campus');
                });
        
        
        populateFeesList();
        
        
    }
    



$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#performance_report").addClass('active');
    
    $('body').on('change', '#student_school_search', function(){
         schoolChanged();
    });
    
    //By default trigger change in case school is already set
    schoolChanged();
    
  });
JS;
$this->registerJs($script);
?>

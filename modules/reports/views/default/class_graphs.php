<?php 
use yii\helpers\Html; 
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
?>
<p style="color:#F7F7F7">Class report</p>

<div class="row hd-title" data-title="Class Graphs">
<div class="col-md-12">
<?= Highcharts::widget([
    'scripts' => [
         'highcharts-3d',
         'modules/exporting', 
         // 'themes/gray'    
      ],
   'options' => [
      'credits'=>[
          'enabled'=>false
      ],
      'chart'=> [
            'backgroundColor'=>'#E3EEF1',
            'type'=> 'column',
            'margin'=> 75,
            'options3d'=> [
                'enabled'=> true,
                'alpha'=> 15,
                'beta'=> -6,
                'depth'=> 70
            ]
        ],
      'colors'=>['#F45B5B', '#F7A35C', '#2B908F', '#808080', '#7999d5', '#ffff36'],
      'title' => ['text' => 'Transactions Nature per class ('.$period.')'],
      'plotOptions'=> [
            'column'=> [
                'depth'=> 25
            ]
        ],
      
      'xAxis' => [
         'categories' => $schClasses
      ],
      'yAxis' => [
         'title' => ['text' => 'Total Channel Amount Transacted'],
      ],
      'legend' => [
            'layout'=> 'horizontal',
            'align'=> 'right',
            'verticalAlign'=> 'bottom',
            'borderWidth'=> 2
        ],
     
      'series' => $classTrans
   ]
]); ?>
</div>
</div>

<div class="row">
  <div class="col-md-12">
<?= Highcharts::widget([
    'scripts' => [
         'highcharts-3d',
         'modules/exporting', 
         // 'themes/gray'    
      ],
   'options' => [
      'credits'=>[
          'enabled'=>false
      ],
      'chart'=> [
            'backgroundColor'=>'#f9f9f9',
            'type'=> 'column',
            'margin'=> 75,
            'options3d'=> [
                'enabled'=> true,
                'alpha'=> 15,
                'beta'=> -6,
                'depth'=> 70
            ]
        ],
      'colors'=>['#F45B5B', '#2B908F'],
      'title' => ['text' => 'Number of Student by Gender'],
      'plotOptions'=> [
            'column'=> [
                'depth'=> 25
            ]
        ],
      
      'xAxis' => [
         'categories' => $schClasses
      ],
      'yAxis' => [
         'title' => ['text' => 'Total Number of Student by Gender'],
      ],
      'legend' => [
            'layout'=> 'horizontal',
            'align'=> 'right',
            'verticalAlign'=> 'bottom',
            'borderWidth'=> 2
        ],
     
      'series' => $classGen
   ]
]); ?>
</div>
</div>

<?php 
$script = <<< JS
$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#class_graphs").addClass('active');
});
JS;
$this->registerJs($script);
?>

<?php 
use yii\helpers\Html; 
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
?>
<br/>
<p style="color:#F7F7F7">Channel report</p>

<div class="hd-title" data-title="Channel Graphs">
<?= Highcharts::widget([
    'scripts' => [
         //'highcharts-more', 
         'modules/exporting', 
         // 'themes/gray'    
      ],
   'options' => [
      'exporting'=>[
        'enabled'=>true 
      ],
      'credits'=>[
          'enabled'=>false
      ],
      'chart'=> [
            'backgroundColor'=>'#E3EEF1',
        ],
      'colors'=>['#F45B5B', '#F7A35C', '#2B908F', '#808080', '#7999d5', '#ffff36'],
      'title' => ['text' => 'Monthly Channel Transactional Total Amount'],
      'xAxis' => [
         'categories' => $months
      ],
      'yAxis' => [
         'title' => ['text' => 'Total Monthly Amount'],
         'plotLines'=> [
            [
                'value'=> 0,
                'width'=> 2,
                'color'=> '#808080'
            ]]
      ],
      'legend' => [
            'layout'=> 'horizontal',
            'align'=> 'center',
            'verticalAlign'=> 'bottom',
            'borderWidth'=> 0
        ],
        'plotOptions'=> [
            'line'=> [
                'dataLabels'=> [
                    'enabled'=> true
                ],
                'enableMouseTracking'=> true
            ]
        ],
      'series' => $monTrans
   ]
]); ?>


<?= Highcharts::widget([
   'options' => [
      'credits'=>[
          'enabled'=>false
      ],
      'chart'=> [
            'backgroundColor'=>'#f9f9f9',
        ],
      'colors'=>['#F45B5B', '#F7A35C', '#2B908F', '#808080', '#7999d5', '#ffff36'],
      'title' => ['text' => 'Channel Transactional Count'],
      'xAxis' => [
         'categories' => $months
      ],
      'yAxis' => [
         'title' => ['text' => 'Number of Transactions'],
         'plotLines'=> [[
                'value'=> 0,
                'width'=> 1,
                'color'=> '#808080'
            ]]
      ],
      'legend' => [
            'layout'=> 'horizontal',
            'align'=> 'center',
            'verticalAlign'=> 'bottom',
            'borderWidth'=> 0
        ],
        'plotOptions'=> [
            'line'=> [
                'dataLabels'=> [
                    'enabled'=> true
                ],
                'enableMouseTracking'=> true
            ]
        ],
      'series' => $transCount
   ]
]); ?>
</div>
<?php 
$script = <<< JS
$("document").ready(function(){
  $(".active").removeClass('active'); 
    $("#reports").addClass('active');
    $("#trans_graphs").addClass('active');
});
JS;
$this->registerJs($script);
?>

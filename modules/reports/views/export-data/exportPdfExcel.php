<?php
use yii\helpers\Html;
?>
<div class="student-information">
    <?php
        if($type == 'Excel') {
            echo "<table><tr> <th colspan='7'><h3> Students Fees Datasheet</h3> </th> </tr> </table>";
        }
    ?>

    <table class="table">
        <thead>
        <tr>
            <?php if($type == 'Pdf') { echo "<th>Sr No</td>"; } ?>
            <th>Payment Code</th><th>First Name</th><th>Last Name</th> <th>Fee Name</th><th>Fee Paid Amount</th><th>Details</th><th>&nbsp;</th></tr>
        </thead>
        <tbody>
            <?php 
            $no = 1; 
            foreach($query as $sinfo) : ?>
                <tr>
                   <?php if($type == 'Pdf') {
                            echo "<td>".$no."</td>";
                        } ?>
                    <td><?= ($sinfo['student_code']) ? $sinfo['student_code'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['first_name']) ? $sinfo['first_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['last_name']) ? $sinfo['last_name'] : '<span class="not-set">(not set) </span>' ?></td>

                    <td><?= ($sinfo['fee_name']) ? $sinfo['fee_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['amount']) ? number_format($sinfo['amount'], 2)  : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($sinfo['description']) ? $sinfo['description'] : '<span class="not-set">(not set) </span>'?></td>
                </tr>
                <?php $no++; ?>
            <?php endforeach; ?>
        </tbody>
        </table>

</div>

<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */


$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";

use kartik\grid\GridView;
use yii\helpers\Html;

use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreSchoolClass;

?>


    <div style="width:100%;background-color: #e6ecff;padding:20px">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <h3><?php echo $schoolDetail['school_name'] ?></h3>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Location: <?php echo $schoolDetail['description'] ?><br>
                District: <?php echo $schoolDetail['district_name'] ?><br>
                Address: <?php echo $schoolDetail['village'] ?><br>
            </div>
            <div class="col-md-4">
                    <div style="margin-left: 25px">
                        <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>
                    </div>
            </div>
            <div class="col-md-4">
                Tel:<?php echo $schoolDetail['phone_contact_1'] ?><br>
                MOBILE: <?php echo $schoolDetail['phone_contact_2'] ?><br>
                EMAIL:<?php echo $schoolDetail['contact_email_1'] ?>

            </div>
        </div>

       
        <div style="width:150px"></div>
        <br></br>
        <div class="row">
''
            <div class = "col-md-12">
             </div>
              
               <?php
                    $model = $data['model'];

                    $selectedClass = empty($model->class_id) ? '' : CoreSchoolClass::findOne($model->class_id)->class_code;

                    $selectedTerm = empty($model->term_id) ? '' : CoreTerm::findOne($model->term_id)->term_name;

                    $staffInfo = empty($model->teacher_id) ? '' : CoreStaff::findOne($model->teacher_id);
                    $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

                    echo '<h5><b>' . $selectedTerm . ' - ' . $model->timetable_type . ' TimeTable  </b></h5>' ;
                    echo '<h5> Class - ' . $selectedClass . '</h5>';
                    if (!empty($model->teacher_id)) {
                        echo '<h5> Teacher - ' . $selectedStaff .  '</h5>';
                    }
                    echo "<br>";
                    
                   echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                   echo "<tr>";
                   echo "<th class='text-center'>#</th>";
                   echo "<th class='text-center'>MON</th>";
                   echo "<th class='text-center'>TUE</th>";
                   echo "<th class='text-center'>WED</th>";
                   echo "<th class='text-center'>THUR</th>";
                   echo "<th class='text-center'>FRI</th>";
                   echo "<th class='text-center'>SAT</th>";
                   echo "<th class='text-center'>SUN</th>";
                   echo "</tr>";


                   foreach($data['timetable_data'] as $key  =>  $value){   
                        echo "<tr>";
                        echo "<td class='text-center'>" . $key . "</td>";
                        //Mon
                        echo "<td class = 'text-center'>";
                            if (count($value) > 0) {
                                foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Mon') {
                                        echo "<span> - " . $schedule['title'] . "</span><br>";
                                        echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            } else{
                               echo " --- ";
                            }
                        echo "</td>";
                       //Tue
                       echo "<td class = 'text-center'>";

                            if (count($value) > 0) {
                               foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Tue') {
                                      echo "<span> - " . $schedule['title'] . "</span><br>";
                                      echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            }else{
                              echo "---";
                            }
    
                        echo "</td>";
                        //Wed
                        echo "<td class = 'text-center'>";
                            if (count($value) > 0) {
                                foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Wed') {
                                       echo "<span> - " . $schedule['title'] . "</span><br>";
                                       echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            }else{
                              echo " --- ";
                        }
                        echo "</td>";
                        //THur
                        echo "<td class = 'text-center'>";
                            if (count($value) > 0) {
                                foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Thu') {
                                        echo "<span> - " . $schedule['title'] . "</span><br>";
                                        echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            }else{
                               echo " --- ";
                            }
                        echo "</td>";
                        //Fri
                        echo "<td class = 'text-center'>";
                            if (count($value) > 0) {
                                foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Fri') {
                                        echo "<span> - " . $schedule['title'] . "</span><br>";
                                        echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            }else{
                              echo "---";
                            }
                        echo "</td>";
                        //Sat
                        echo "<td class = 'text-center'>";

                            if (count($value) > 0) {
                                foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Sat') {
                                       echo "<span> - " . $schedule['title'] . "</span><br>";
                                       echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            }else{
                              echo "---";
                            }
                        echo "</td>";
                        //Sun.
                        echo "<td class = 'text-center'>";
                            if (count($value) > 0) {
                                foreach ($value as $key => $schedule) {
                                    if ($schedule['startDay'] == 'Sun') {
                                        echo "<span> - " . $schedule['title'] . "</span><br>";
                                        echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                    }
                                }
                            }else{
                              echo " --- ";
                            }
                        echo "</td>";

                        echo "</tr>";

                    }
                    echo "</table>";
                ?>

            </div>

        <div style="clear:both"></div>

        <div class="row">
            <br><br>
            <h5>Powered By</h5>
        </div>
        <div class="row">
            <?= Html::img('@web/web/img/no_bg_logo.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '65px']); ?>
        </div>
        <div class="row"
             style="height:50px; margin-top:70px;background-image: linear-gradient(180deg,#2B3990 10%,#0068AD 100%);">
            <div style="display: table;color:white;margin: auto;"><i>All rights reserved</i></div>
        </div>
    </div>
    <br>


<?php

use yii\helpers\Html;
use app\modules\planner\models\CoreStaff;

?>



<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
<?php
if(!empty($meeting_data))
{    

	echo "<table class ='table-bordered table table-striped'>";
	echo "<tr  class=success ><th >SI No.</th>";
	foreach($selected_list as $s)
	{
		echo "<th>" . $s . "</th>";

	}
	echo "</tr>";
	$i = 1;
	foreach($meeting_data as $t=>$sd)
	{
	    if(($i%2) == 0){
			$bgcolor = "#FFFFFF";
	    }else {
			$bgcolor = "#E3E3E3";
		}

		$staffInfo = empty($sd['chairperson']) ? '' : CoreStaff::findOne($sd['chairperson']);
        $chairedBy = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

		echo '<tr bgcolor='. $bgcolor .'>';
		echo "<td>". $i ."</td>";
		
		echo "<td>" . $sd['title'] . "</td>";
		echo "<td>"  . $sd['timetable_schedule'] . "</td>";
		echo "<td>" . $sd['datetime'] . "</td>";
		echo "<td>" . $chairedBy . "</td>";
		echo "<td>" . $sd['class_code'] . "</td>";
		echo "<td>" . $sd['attendee_list'] . "</td>";
		echo "<td>" . $sd['absentee_list'] . "</td>";
		$i++;
		echo "</tr>";

	}

      echo "</table>";
    }
    else
	    echo  "<h1 style=\"color:red;text-align:center;margin-top:10%;margin-right:60%\">No Record To Display</h1>";
?>

</div>

</div>

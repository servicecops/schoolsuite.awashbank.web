<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\planner\models\CoreStaff;
use app\modules\timetable\models\TimetableSchedule;


$this->title = Yii::t('app', 'Examination Timetable Report');
$this->params['breadcrumbs'][] = $this->title;


$schoolId = Yii::$app->user->identity->school_id;

$classList = ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $schoolId ])->all(), 'id', 'class_code');


?>
    <div class="row card" data-title='Examination Timetable Report'>
        <div class="card-body">

            <h3>
                <i class="fa fa-info-circle"></i> <?= $this->title ?>
            </h3>
        <div class="box-info box-solid">
            <?php $form = ActiveForm::begin([
                'action' => ['examination-time-table-report'],
                'method' => 'post',
                'id' => 'exam_report_id',
                'options' => ['class' => 'formprocess'],
            ]); 
            ?>  
                <div class = "row">
                    <div class = "col-md-4">
                        <?php
                
                            $url = Url::to(['/planner/planner/terms-list']);
                            
                            $selectedTerm = empty($model->term_id) ? '' : CoreTerm::findOne($model->term_id)->term_name;

                            echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                                'options' => ['multiple'=>false, 'placeholder' => 'Search Terms ...'],
                                'initValueText' => $selectedTerm, // set the initial display text
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 2,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $url,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],

                            ],
                        ])->label('Term name'); ?>
                    </div>
                    
                    <div class = "col-md-4 class-report-class-list">
                        <?php
                                 
                            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                                    'data' => $classList,
                                    'options' => [
                                        'placeholder' => 'Select class list'
                                   ],
                                    'pluginOptions' => [
                                          'allowClear' => true
                                    ],
                            ])->label('Class');

                        ?>
                      
                    </div>

                    <div class="col-md-4">
                       <?php
                            $url = Url::to(['/planner/planner/staff-list']);
                            $staffInfo = empty($model->teacher_id) ? '' : CoreStaff::findOne($model->teacher_id);
                            $selectedStaff = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';
                 
                            echo $form->field($model, 'teacher_id')->widget(Select2::classname(), [
                                'options' => ['multiple' => false, 'placeholder' => 'Search by firstname or lastname'],
                                'initValueText' => $selectedStaff, // set the initial display text
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 3,
                                    'language' => [
                                          'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                            ]
                        ])->label('Teacher/ Lecturer'); 
                       ?>
                    </div>

                </div>
                
                <div class = "row">
                    <div class = "col-md-4">

                    <?php
                
                        $timetableUrl = Url::to(['timetable-schedule-list']);
                
                        $selectedTimetable = empty($model->exam_id) ? '' : TimetableSchedule::findOne($model->exam_id)->title;

                        echo $form->field($model, 'exam_id')->widget(Select2::classname(), [
                           'options' => ['multiple'=>false, 'placeholder' => 'Search Examination by title..'],
                            'initValueText' => $selectedTimetable, // set the initial display text
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                   'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                            ],
                        ])->label('Examination Title'); ?>
                           
                    </div>
                </div>

            </div> <!--/ box-body -->
            <div class="mt-3 mb-3">
                <?= Html::submitButton('Generate', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-danger']) ?>
                <?php ActiveForm::end(); ?>
            </div> <!--/ box -->
        </div>

    <div class="row">

        <div class="table-responsive no-padding">

            <?php
                if (!empty($timetable_data['reformatedSchedules'])){

                        echo Html::a('<i class="fa far fa-file-pdf"></i>Download Pdf', ['/reports/export-data/export-examination-timetable-report', 'id' => $schoolId ], [
                          'class'=>'btn btn-sm btn-danger float-right',
                          'target'=>'_blank',
                          'data-toggle'=>'tooltip',
                           'title'=>'Will open the generated PDF file in a new window'
                        ]);

                        $selectedClass = empty($model->class_id) ? '' : CoreSchoolClass::findOne($model->class_id)->class_code;

                        echo '<h5><b>Examination - ' . $selectedTerm . '</b></h5>';
                        echo '<h5> Class - ' . $selectedClass . '</h5>';
                        if (!empty($model->teacher_id)) {
                            echo '<h5> Teacher - ' . $selectedStaff .  '</h5>';
                        }
                        $monthList = $timetable_data['months'];
                        if (count($monthList) > 1) {
                           echo '<h5> Duration - ' . $monthList[0] . ' - ' . $monthList[1] . '</h5>' ;
                        } else {
                           echo '<h5> Duration - ' . $monthList[0] . '</h5>';
                        }
                        echo "<br>";
                       
                        $weekNumber = 0;

                        foreach($timetable_data['reformatedSchedules'] as $week_data){
                            $weekNumber = $weekNumber + 1; 

                            echo "<h5> WEEK # " . $weekNumber  .  "</h5>";
                            echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                            echo "<tr>";
                            echo "<th class='text-center'> WEEK #" . $week_data['week'] .  "</th>";
                            echo "<th class='text-center'>MON</th>";
                            echo "<th class='text-center'>TUE</th>";
                            echo "<th class='text-center'>WED</th>";
                            echo "<th class='text-center'>THUR</th>";
                            echo "<th class='text-center'>FRI</th>";
                            echo "<th class='text-center'>SAT</th>";
                            echo "<th class='text-center'>SUN</th>";
                            echo "</tr>";
    
    
                            foreach($week_data['schedules'] as $key  =>  $value){   
                                echo "<tr>";
                                //what about the filtered schedules for particular timeRow.
    
                                echo "<td class='text-center'>" . $key . "</td>";
    
                                //Mon
                                echo "<td class = 'text-center'>";
                                    if (count($value) > 0) {
                                        foreach ($value as $key => $schedule) {
                                           if ($schedule['startDay'] == 'Mon') {
                                                echo "<span> - " . $schedule['title'] . "</span><br>";
                                                echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                           }
                                        }
                                    }else{
                                        echo " --- ";
                                    }
                                echo "</td>";
                                //Tue
                                echo "<td class = 'text-center'>";
    
                                    if (count($value) > 0) {
                                        foreach ($value as $key => $schedule) {
                                            if ($schedule['startDay'] == 'Tue') {
                                                echo "<span> - " . $schedule['title'] . "</span><br>";
                                                echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                            }
                                        }
                                    }else{
                                        echo "---";
                                    }
                                
                                echo "</td>";
                                //Wed
                                echo "<td class = 'text-center'>";
                                    if (count($value) > 0) {
                                        foreach ($value as $key => $schedule) {
                                            if ($schedule['startDay'] == 'Wed') {
                                                echo "<span> - " . $schedule['title'] . "</span><br>";
                                                echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                            }
                                        }
                                    }else{
                                        echo " --- ";
                                    }
                                echo "</td>";
                                //THur
                                echo "<td class = 'text-center'>";
    
                                    if (count($value) > 0) {
                                        foreach ($value as $key => $schedule) {
                                            if ($schedule['startDay'] == 'Thu') {
                                                echo "<span> - " . $schedule['title'] . "</span><br>";
                                                echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                            }
                                        }
                                    }else{
                                       echo " --- ";
                                    }
                                echo "</td>";
                                //Fri
                                echo "<td class = 'text-center'>";
                                        if (count($value) > 0) {
                                            foreach ($value as $key => $schedule) {
                                                if ($schedule['startDay'] == 'Fri') {
                                                    echo "<span> - " . $schedule['title'] . "</span><br>";
                                                    echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                                }
                                            }
                                        }else{
                                           echo "---";
                                        }
                                echo "</td>";
                                //Sat
                                echo "<td class = 'text-center'>";
    
                                    if (count($value) > 0) {
                                        foreach ($value as $key => $schedule) {
                                            if ($schedule['startDay'] == 'Sat') {
                                                echo "<span> - " . $schedule['title'] . "</span><br>";
                                                echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                            }
                                        }
                                    }else{
                                        echo "---";
                                    }
                                echo "</td>";
                                //Sun.
                                echo "<td class = 'text-center'>";
                                    if (count($value) > 0) {
                                        foreach ($value as $key => $schedule) {
                                            if ($schedule['startDay'] == 'Sun') {
                                                echo "<span> - " . $schedule['title'] . "</span><br>";
                                                echo  $schedule['location']? "<span><b>ROOM - " . $schedule['location'] . "</b></span><br>" : "";
                                            }
                                        }
                                    }else{
                                        echo " --- ";
                                    }
                                echo "</td>";
    
                                echo "</tr>";
                            }
                            
                            echo "</table>";
                            echo "<hr>";

                            //echo "<br></br>";

                        }
                        
                    } else echo "No results found";
                    ?>
                </div>

            <div class="col-xs-12"><?= LinkPager::widget(['pagination' => $pages]); ?></div>
        </div>
      </div>
    </div>
    
<?php

$script = <<< JS

    
var timetableTypeChanged = function() {

    var selectedCategory = $("#select_timetable_type").val();

    if(selectedCategory === 'student-group'){
        $('.class-report-class-list').hide();
        $('.class-report-student-group').show();
    }else{
         
        $('.class-report-class-list').show();
        $('.class-report-student-group').hide();
    }
        
}
    



$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#reports").addClass('active');
    //$("#balance_adjustment_report").addClass('active');
    
    $('body').on('change', '#select_timetable_type', function(){
         timetableTypeChanged();
    });
    
    //By default trigger change in case school is already set
    //timetableTypeChanged();
    
  });
JS;
$this->registerJs($script);
?>

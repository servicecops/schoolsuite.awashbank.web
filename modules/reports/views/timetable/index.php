<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;
  
  
$this->title = Yii::t('app', 'Timetable Reports');
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row card" data-title='Meeting Minutes Report'>
    <div class="card-body">

        <?php if (\Yii::$app->getSession()->hasFlash('meeting_report_error')) : ?>
                <div class="alert alert-danger alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                    </button>
                    <?= \Yii::$app->getSession()->getFlash('meeting_report_error'); ?>
                </div>
        <?php endif; ?>

        <h2>
            <i class="fa fa-info-circle"></i> <?= $this->title ?>
        </h2>

        <div class = "row">
            <div class = "col-md-12">
               <?= Html::a('Class/Lecture Report</i>', ['/reports/timetable/class-time-table-report'], ['class' => 'btn btn-primary']) ?> &nbsp;&nbsp;
               <?= Html::a('Meeting Minutes Report', ['/reports/timetable/meeting-minutes-report'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div><br>
        <div class = "row">
            <div class = "col-md-12">
               <?= Html::a('Teacher Assignment Collision Detection', ['/reports/timetable/teacher-assignment-collision-detection-report'], ['class' => 'btn btn-primary']) ?>
               <?= Html::a('Examination Report', ['/reports/timetable/examination-time-table-report'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>

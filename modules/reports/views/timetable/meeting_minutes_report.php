<?php

use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreStudentGroupInformation;
use app\modules\planner\models\CoreStaff;
use app\modules\reports\models\MeetingMinutesHelperModel;


$this->title = Yii::t('app', 'Meeting Minutes Report');
$this->params['breadcrumbs'][] = $this->title;

$calendarCategory = [
    'examination'=> 'Examination','lesson'=> 'Teaching',
    'student-group' => 'Student Group', 
    //'extra-curriculars' => 'Extracurriculars(Sports or MDD)'
];

$schoolId = Yii::$app->user->identity->school_id;

$groupData = CoreStudentGroupInformation::find()->where(['school_id' => $schoolId])->asArray()->all();
$groupList = ArrayHelper::map($groupData, 'id', 'group_name');

$classList = ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $schoolId ])->all(), 'id', 'class_code');

$staffUrl = Url::to(['/planner/planner/staff-list']);

?>
    <div class="row card" data-title='Meeting Minutes Report'>
        <div class="card-body">

            <?php if (\Yii::$app->getSession()->hasFlash('meeting_report_error')) : ?>
                <div class="alert alert-danger alert-dismissible">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                    </button>
                    <?= \Yii::$app->getSession()->getFlash('meeting_report_error'); ?>
                </div>
            <?php endif; ?>

            <h3>
                <i class="fa fa-info-circle"></i> <?= $this->title ?>
            </h3>
            <div class="box-info box-solid">
                <?php $form = ActiveForm::begin([
                   'action' => ['meeting-minutes-report'],
                   'method' => 'post',
                   'id' => 'meeting-minutes-report',
                   'options' => ['class' => 'formprocess'],
                ]); 
                ?> 

                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'date_from')->widget(DatePicker::className(),
                            [
                                'dateFormat' => 'yyyy-MM-dd',
                                'clientOptions' => [
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '2000:' . (date('Y') + 1),
                                    'autoSize' => true,
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => 'Select date',
                            ],
                        ])->label('From'); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model, 'date_to')->widget(DatePicker::className(),
                            [
                               'dateFormat' => 'yyyy-MM-dd',
                               'clientOptions' => [
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '2000:' . (date('Y') + 1),
                                    'autoSize' => true,
                            ],
                            'options' => [
                               'class' => 'form-control',
                               'Placeholder' => 'Select date',
                            ],
                        ])->label('To'); ?>
                  </div>

                </div>

                <div class = "row">
                    <div class = "col-md-6">
                        <?php
                                 
                            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                                    'data' => $classList,
                                    'options' => [
                                        'placeholder' => 'Select class list'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                            ])->label('Class');
     
                        ?>
                    </div>
                    <div class = "col-md-6">
                       <?php
                            $staffInfo = empty($model->chairperson) ? '' : CoreStaff::findOne($model->chairperson);
                            $selectedChairperson = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

                            echo $form->field($model, 'chairperson')->widget(Select2::classname(), [
                               'options' => ['multiple' => false, 'placeholder' => 'Search by firstname'],
                               'initValueText' => $selectedChairperson, // set the initial display text
                               'pluginOptions' => [
                                   'allowClear' => true,
                                   'minimumInputLength' => 3,
                                   'language' => [
                                       'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => $staffUrl,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                ],
                            ])->label('Chairperson'); 
       
                
                        ?>
                    </div>
                </div>
                <hr>

                </div> <!--/ box-body -->
                <div class="mt-3 mb-3">
                   <?= Html::submitButton('Generate', ['class' => 'btn btn-primary']) ?>
                   <?= Html::resetButton('Reset', ['class' => 'btn btn-danger']) ?>
                <?php ActiveForm::end(); ?>
            </div> <!--/ box -->
        </div>

    <div class="row">

        <div class="table-responsive no-padding">

            <?php
                if(!empty($meeting_data)){
                    echo '<div class="pull-right">';
                        echo Html::a('<i class="fa far fa-file-pdf"></i>Download Pdf', ['/reports/export-data/export-meeting-minutes-report-as-pdf', 'id' => $schoolId ], [
                           'class'=>'btn btn-sm btn-danger float-right',
                           'target'=>'_blank',
                           'data-toggle'=>'tooltip',
                           'title'=>'Will open the generated PDF file in a new window'
                        ]);

                        echo Html::a('<i class="fa far fa-file-excel"></i>Download Excel', ['/reports/export-data/export-meeting-minutes-report-as-excel', 'id' => $schoolId ], [
                           'class'=>'btn btn-sm btn-success float-right',
                           'target'=>'_blank',
                           'data-toggle'=>'tooltip',
                           'title'=>'Will open the generated EXCEL  file in a new window'
                        ]);
                    echo '</div>';
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'>Title</th>";
                    echo "<th class='text-center'>Schedule</th>";
                    echo "<th class='text-center'>Meeting Time</th>";
                    echo "<th class='text-center'>Chaired By</th>";
                    echo "<th class='text-center'>No. of Attendee</th>";
                    echo "<th class='text-center'>No. of Absentee</th>";

                    echo "</tr>";
                    foreach($meeting_data as $t=>$v)
                    {   
                        $staffInfo = empty($v['chairperson']) ? '' : CoreStaff::findOne($v['chairperson']);
                        $chairedBy = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';
                        echo "<tr>";
                        echo "<td class='text-center'>". Html::a( $v['title'], ['/timetable/time-table-schedule-meeting/view', 'id' => $v['id']],['target'=>'_blank'])  ."</td>";
                        echo "<td class='text-center'>". $v['timetable_schedule'] ."</td>";
                        echo "<td class='text-center'>". $v['datetime'] ."</td>";
                        echo "<td class='text-center'>". $chairedBy  . "</td>";
                        echo "<td class='text-center'>". $v['attendee_list'] . "</td>";
                        echo "<td class='text-center'>". $v['absentee_list'] ."</td>";
                        echo "</tr>";

                    }
                    echo "</table>";
                } 
                   else echo "No results found";
                  ?>
                </div>

            <div class="col-xs-12"><?= LinkPager::widget(['pagination' => $pages]); ?></div>
        </div>
      </div>
    </div>
    
<?php


$script = <<< JS

    
var timetableTypeChanged = function() {

    var selectedCategory = $("#select_timetable_type").val();

    if(selectedCategory === 'student-group'){
        $('.class-report-class-list').hide();
        $('.class-report-student-group').show();
    }else{
         
        $('.class-report-class-list').show();
        $('.class-report-student-group').hide();
    }
        
}
    



$("document").ready(function(){ 
    $(".active").removeClass('active');
    $("#reports").addClass('active');
    //$("#balance_adjustment_report").addClass('active');
    
    $('body').on('change', '#select_timetable_type', function(){
         timetableTypeChanged();
    });
    
    //By default trigger change in case school is already set
    //timetableTypeChanged();
    
  });
JS;
$this->registerJs($script);
?>

<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\Controller;

$this->title = Yii::t('app', 'Student Subject Attendance Summary');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row card" data-title="Attendance Summary">
    <div class="card-header"><?php if(!empty($query))
            echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
        ?></div>
    <div class="card-body">

        <div class="row">

            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($tepresent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Students Present</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";
                    echo "<th class='text-center'>Present</th>";

                    echo "</tr>";
                    foreach ($tepresent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";
                        echo "<td class='text-center'>" . number_format($data['presents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>TOTAL</th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "<th class='text-center'>" . number_format($sumPr) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                } else echo "No Present Students Results Found";
                ?>
                <div style="clear:both"></div>
            </div>

            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($teabsent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Students Absent</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Absent</th>";

                    echo "</tr>";
                    foreach ($teabsent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";

                        echo "<td class='text-center'>" . number_format($data['absents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>TOTAL</th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "<th class='text-center'>" . number_format($sumAb) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                } else echo "No Absent Students Results Found";
                ?>
                <div style="clear:both"></div>
            </div>

            <h3 style="margin: 50px">
                <i class="fa fa-info-circle"></i> <?=  " Gender Based Attendance -Girls ( <span style='font-size:14px;'><b>" . date('M d, y', strtotime($from_date)) . " &nbsp;&nbsp;-&nbsp;&nbsp;  " . date('M d, y', strtotime($to_date)) . "</b></span>)" ?>
            </h3>


            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($gepres)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Girls Present</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Present</th>";

                    echo "</tr>";
                    foreach ($gepres as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";

                        echo "<td class='text-center'>" . number_format($data['presents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>TOTAL</th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "<th class='text-center'>" . number_format($sumgePr) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                } else echo "No Present Students Results Found";
                ?>
                <div style="clear:both"></div>
            </div>

            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($geabsent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Girls Absent</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Absent</th>";

                    echo "</tr>";
                    foreach ($geabsent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";

                        echo "<td class='text-center'>" . number_format($data['absents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>TOTAL</th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "<th class='text-center'>" . number_format($sumgeAb) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                } else echo "No Absent Students Results Found";
                ?>
                <div style="clear:both"></div>
            </div>
            <div style="min-height: 50px"></div>

            <h3 style="margin: 50px">
                <i class="fa fa-info-circle"></i> <?=  " Gender Based Attendance -Boys ( <span style='font-size:14px;'><b>" . date('M d, y', strtotime($from_date)) . " &nbsp;&nbsp;-&nbsp;&nbsp;  " . date('M d, y', strtotime($to_date)) . "</b></span>)" ?>
            </h3>
            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($bopres)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Boys Present</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Present</th>";

                    echo "</tr>";
                    foreach ($bopres as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";

                        echo "<td class='text-center'>" . number_format($data['presents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>TOTAL</th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "<th class='text-center'>" . number_format($sumboPr) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                } else echo "No Present Students Results Found";
                ?>
                <div style="clear:both"></div>
            </div>

            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($boabsent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Boys Absent</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Absent</th>";

                    echo "</tr>";
                    foreach ($boabsent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";
                        echo "<td class='text-center'>" . number_format($data['absents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center' rowspan='2'>TOTAL</th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "<th class='text-center'>" . number_format($sumboAb) . "</th>";
                    echo "</tr>";

                    echo "</table>";
                } else echo "No Absent Students Results Found";
                ?>
                <div style="clear:both"></div>
            </div>















        </div>
    </div>
</div>


<?php

use app\modules\schoolcore\models\CoreStudent;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
?>

<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
<?php
if(!empty($student_data))
{
	$s_info = new CoreStudent();
	echo "<table class ='table-bordered table table-striped'>";
	echo "<tr  class='center' ><th >SI No.</th>";
	foreach($selected_list as $s)
	{
			echo "<th>  ".Html::activeLabel($s_info,$s)."</th>";

	}
	echo "</tr>";
	$i = 1;
	foreach($student_data as $t=>$sd)
	{
		echo "<tr>";
		echo "<td>  ".$i."</td>";
		foreach($selected_list as $s){
		echo "<td>  ".$sd[$s]."</td>";
		}

		$i++;
		echo "</tr>";

	}

echo "</table>";
}
else
	echo  "<h1 style=\"color:red;text-align:center;margin-top:10%;margin-right:60%\">No Record To Display</h1>";
?>

</div>

</div>

<?php
/* @var $this yii\web\View */
use yii\helpers\Html; 
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
$this->title = 'Student Growth';

?>
<div class="col-xs-12 no-padding" style="margin-bottom: 10px;">
	<div id="chart_count_content" class="col-xs-12 no-padding">
		<?= Highcharts::widget([
				'scripts' => [
				     //'highcharts-more', 
				     'modules/exporting', 
				     // 'themes/gray'    
				  ],
				'options' => [
					'exporting'=>[
					    'enabled'=>true 
					  ],
				  'credits'=>[
					      'enabled'=>false
					  ],
				  'chart'=> [
				        'backgroundColor'=>'#fff',
				    ],
				  'colors'=>['#046DC9','#FE2626', '#FFE600', '#48CFF4', '#AB0E96', '#E8BB1A'],
				  'title' => ['text' => '<b>Students Added Per Month (Growth Curve) </b>'],
				  'xAxis' => [
				     'categories' => $months
				  ],
				  'yAxis' => [
				     'title' => ['text' => 'Number of Students'],
				     'plotLines'=> [[
				            'value'=> 0,
				            'width'=> 1,
				            'color'=> '#808080'
				        ]]
				  ],
				  'legend' => [
				        'layout'=> 'horizontal',
				        'align'=> 'center',
				        'verticalAlign'=> 'bottom',
				        'borderWidth'=> 0
				    ],
				    'plotOptions'=> [
				        'line'=> [
				            'dataLabels'=> [
				                'enabled'=> true
				            ],
				            'enableMouseTracking'=> true
				        ]
				    ],
				  'series' => $growthCount
				]
				]); ?>
	</div>
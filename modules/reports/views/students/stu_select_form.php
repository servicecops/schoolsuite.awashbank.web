
<div class="container">
    <div class="checkbox checkbox-info">
        <div>
            <input type="checkbox" class="checkall selectall" id="check_all_id">
            <label for="check_all_id"><b> Check All</b> </label>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="first_name" value="first_name"/><label
                        for="first_name" class="checkbox-inline"> &nbsp;First Name</label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="middle_name" value="middle_name"/>
                <label for="middle_name" class="checkbox-inline">&nbsp;Middle Name</label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="last_name" value="last_name"/> <label
                        for="last_name" class="checkbox-inline">&nbsp;Last Name </label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="student_account_id"
                       value="student_account_id"/>
                <label for="student_account_id" class="checkbox-inline">&nbsp;Student Account </label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="class_code" value="class_code"/> <label
                        for="class_code" class="checkbox-inline">&nbsp;Student Class </label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="student_phone" value="student_phone"/>
                <label for="student_phone" class="checkbox-inline"> &nbsp;Student Phone </label>
            </div>
        </div>

        <div class="col-sm">
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="student_code" value="student_code"/>
                <label for="student_code" class="checkbox-inline">&nbsp;Student Code</label>
            </div>

            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="student_email" value="student_email"/>
                <label for="student_email" class="checkbox-inline">&nbsp;Student Email</label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="guardian_name" value="guardian_name"/>
                <label for="guardian_name" class="checkbox-inline">&nbsp;Guardian </label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="guardian_email" value="guardian_email"/>
                <label for="guardian_email" class="checkbox-inline">&nbsp;Guardian Email </label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="guardian_phone" value="guardian_phone"/>
                <label for="guardian_phone" class="checkbox-inline">&nbsp;Guardian Phone </label>
            </div>
        </div>
        <div class="col-sm">
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="gender" value="gender"/>
                <label for="gender" class="checkbox-inline">&nbsp;Gender </label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="school_student_registration_number"
                       value="school_student_registration_number"/>
                <label for="school_student_registration_number" class="checkbox-inline">&nbsp;Reg No.</label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="guardian_relation"
                       value="guardian_relation"/>
                <label for="guardian_relation" class="checkbox-inline"> &nbsp;Relationship</label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="outstanding_balance"
                       value="outstanding_balance"/>
                <label for="outstanding_balance" class="checkbox-inline">&nbsp;Outstanding Balance</label>
            </div>
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="account_balance"
                       value="account_balance"/>
                <label for="account_balance" class="checkbox-inline">&nbsp;Account Balance</label>
            </div>

            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="day_boarding" value="day_boarding"/>
                <label for="day_boarding" class="checkbox-inline">&nbsp;Day / Boarding</label>
            </div>
        </div>
    </div>
</div>

<?php




$script = <<< JS

    

$(".checkall").click(function(){
$(".checkbox_inline").prop("checked",$(this).prop("checked"));
});


$(".selectall").click(function(){
$(".individual").prop("checked",$(this).prop("checked"));
});
JS;
$this->registerJs($script);
?>

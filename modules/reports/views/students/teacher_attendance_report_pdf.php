<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\web\Controller;

$this->title = Yii::t('app', 'Teacher Attendance Summary');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row card" data-title="Attendance Summary">
    <div class="card-header"><?php if(!empty($query))
            echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
        ?></div>
    <div class="card-body">

        <div class="row">

            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($tepresent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Teachers Present</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";
                    echo "<th class='text-center'>Present</th>";

                    echo "</tr>";
                    foreach ($tepresent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";
                        echo "<td class='text-center'>" . number_format($data['presents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";

                    echo "</tr>";

                    echo "</table>";
                } else echo "No Present Teacher Results Found";
                ?>
                <div style="clear:both"></div>
            </div>

            <div style="min-height: 50px"></div>

            <h3 style="margin: 50px">
                <i class="fa fa-info-circle"></i> <?=  " Gender Based Attendance ( <span style='font-size:14px;'><b>" . date('M d, y', strtotime($from_date)) . " &nbsp;&nbsp;-&nbsp;&nbsp;  " . date('M d, y', strtotime($to_date)) . "</b></span>)" ?>
            </h3>
            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($tmpresent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Male Teachers Present</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Present</th>";

                    echo "</tr>";
                    foreach ($tmpresent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";

                        echo "<td class='text-center'>" . number_format($data['presents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";
                    echo "<tr>";

                    echo "</tr>";

                    echo "</table>";
                } else echo "No Male Present Teacher Found";
                ?>
                <div style="clear:both"></div>
            </div>

            <div class="table-responsive no-padding" style="margin-bottom:10px">

                <?php
                if (!empty($tfpresent)) {
                    echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
                    echo "<tr>";
                    echo "<th class='text-center'><b>Female Teachers Present</b></th>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'></th>";

                    echo "</tr>";
                    echo "<tr>";
                    echo "<th class='text-center'></th>";
                    echo "<th class='text-center'>Class Code</th>";
                    echo "<th class='text-center'>Subject Code</th>";

                    echo "<th class='text-center'>Absent</th>";

                    echo "</tr>";
                    foreach ($tfpresent as $data) {
                        Yii::trace($data);

                        echo "<tr>";
                        echo "<th class='text-center'></th>";

                        echo "<td class='text-center'>" . $data['class_code'] . "</td>";
                        echo "<td class='text-center'>" . $data['subject_code'] . "</td>";
                        echo "<td class='text-center'>" . number_format($data['absents']) . "</td>";
                        echo "</tr>";


                    }

                    echo "<tr>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "</tr>";


                    echo "</table>";
                } else echo "No Female Present Teacher Found";
                ?>
                <div style="clear:both"></div>
            </div>







        </div>
    </div>
</div>


<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\Controller;

$this->title = Yii::t('app', 'School Transaction History');
$this->params['breadcrumbs'][] = $this->title;

?>
<?php if(!empty($query))
echo "<span style='font-weight:6px;font-size:10px;'>Results from ".$query['from']. " to ". $query['to'];
?>

<div class="grid-view user-data">
<div class="box-body table-responsive no-padding">
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <?php
                    $x=1;
                    ?>
                    <tr>
                        <th></th>
                        <th class='clink'>Date  Rollcalled </th>
                        <th class='clink'>Class</th>
                        <th class='clink'>Subject</th>
                        <th class='clink'>First Name</th>
                        <th class='clink'>Last Name</th>
                        <th class='clink'>Ip Address</th>
                        <th class='clink'>Attendance</th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if(!empty($tr_subject_data)):

                        Yii::trace($tr_subject_data);
                        foreach ($tr_subject_data as $data) : ?>
                            <tr data-key="0">
                                <td><?php echo $x++?></td>
                                <td>
                                    <?= ($data['date_created']) ? $data['date_created'] : '<span class="not-set">(not set) </span>' ?>
                                </td> <td>
                                    <?= ($data['class_code']) ? $data['class_code'] : '<span class="not-set">(not set) </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['subject_code']) ? $data['subject_code'] : '<span class="not-set">(not set) </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['firstname']) ? $data['firstname'] : '<span class="not-set">(not set) </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['lastname']) ? $data['lastname'] : '<span class="not-set">(not set) </span>' ?>
                                </td>
                                <td >

                                    <?= ($data['ip_address']) ? $data['ip_address'] : '<span class="not-set">(not set) </span>' ?></td>

                                <td>
                                    <?= ($data['present']) ? "Present" : "Absent" ?>

                                </td>

                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No Teachers found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>


        </div>
    </div>
</div>
</div>

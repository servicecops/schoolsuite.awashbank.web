<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\date\DatePicker;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\web\Controller;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Teacher Subject Attendance Information';
?>
<i class="fa fa-info-circle"></i> <?= $this->title . " ( <span style='font-size:14px;'><b>" . date('M d, y', strtotime($from_date)) . " &nbsp;&nbsp;-&nbsp;&nbsp;  " . date('M d, y', strtotime($to_date)) . "</b></span>)" ?>
<br>

<div class="row card" data-title="Attendance Summary">
    <div class="card-body">
        <?php $form = ActiveForm::begin(['action' => ['tr-summary'], 'method' => 'get',
            'id' => 'trans-form',
            'options' => ['class' => 'form-horizontal formprocess']]); ?>


        <div class="container mb-3">
            <h4>Choose date range</h4>
            <div class="row">
                <div class="col">

                    <?php
                    echo DatePicker::widget([
                        'name' => 'date_from',
                        'value' => (isset($_GET['date_from'])) ? $_GET['date_from'] : '',
                        'type' => DatePicker::TYPE_RANGE,
                        'name2' => 'date_to',
                        'value2' => (isset($_GET['date_to'])) ? $_GET['date_to'] : '',
                        'options' => ['placeholder' => 'Start date'],
                        'options2' => ['placeholder' => 'End date'],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col">
                    <?= Html::submitButton('Find', ['class' => 'btn btn-primary btn-sm']) ?>

                </div>

            </div>
        </div>

        <?php ActiveForm::end(); ?>



<div class="row mt-3">
    <div class=" float-right">
        <?php echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['transactions/trans'], ['class' => 'btn btn-back', 'style' => 'color:#fff']); ?>
        <?php echo Html::a('<i class="fa fa-file-excel"></i> Excel', ['students/tr-summary-list', 'trsubjectexcelexport' => 'channellistexcel'], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn-sm btn-success', 'style' => 'color:#fff')); ?>
        <?php echo Html::a('<i class="fa fa-file-pdf"></i> PDF', array('students/tr-summary-list', 'trsubjectexport' => 'channellistpdf'), array('title' => 'Export to PDF', 'target' => '_blank', 'class' => 'btn-sm btn-warning', 'style' => 'color:#fff')); ?>
    </div>


</div>




<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <?php
                $x=1;
                ?>
                <tr>
                    <th></th>
                    <th class='clink'>Date </th>
                    <th class='clink'>Class</th>
                    <th class='clink'>Subject</th>
                    <th class='clink'>First Name</th>
                    <th class='clink'>Last Name</th>
                    <th class='clink'>Ip Address</th>
                    <th class='clink'>Attendance</th>


                </tr>
                </thead>
                <tbody>
                <?php

                if ($bopres) :

                    Yii::trace($bopres);
                    foreach ($bopres as $data) : ?>
                        <tr data-key="0">
                            <td><?php echo $x++?></td>
                            <td>
                                <?= ($data['date_created']) ? $data['date_created'] : '<span class="not-set">(not set) </span>' ?>
                            </td> <td>
                                <?= ($data['class_code']) ? $data['class_code'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td>
                                <?= ($data['subject_code']) ? $data['subject_code'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td>
                                <?= ($data['firstname']) ? $data['firstname'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td>
                                <?= ($data['lastname']) ? $data['lastname'] : '<span class="not-set">(not set) </span>' ?>
                            </td>
                            <td >

                                <?= ($data['ip_address']) ? $data['ip_address'] : '<span class="not-set">(not set) </span>' ?></td>

                            <td>
                                <?= ($data['present']) ? "Present" : "Absent" ?>

                            </td>

                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No Teachers found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>


    </div>
</div>



<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\SchoolCampuses;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use kartik\select2\Select2;

use yii\web\JsExpression;

$this->title = 'Student Info Report';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
// $model = new StuMaster();
// $info = new StuInfo();
// $city =new City();
?>
    <div class="row card" data-title="Student Info Report">
    <div class="card-body">

    <h2 class="text-dark">
        <i class="fa fa-info-circle"></i> Student Info Report <span style="font-size:22px;">( Select Criteria)</span>
    </h2>

    <?php if (\Yii::$app->getSession()->hasFlash('studerror')) : ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?= \Yii::$app->getSession()->getFlash('studerror'); ?>
        </div>
    <?php endif; ?>

    <div class="box-info box-solid">
        <?php $form = ActiveForm::begin([
            'action' => ['stu-info'],
            'method' => 'get',
            'id' => 'stu_info-form',
            'options' => ['class' => 'formprocess'],
        ]); ?>
        <div class="box-body">
            <div class="row">
                <?php if (Yii::$app->user->can('schoolsuite_admin')) : ?>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?php
                        $url = Url::to(['/schoolcore/core-school/schoollist']);
                        $selectedSchool = empty($model2->school) ? '' : CoreSchool::findOne($model2->school)->school_name;
                        echo $form->field($model2, 'school')->widget(Select2::classname(), [
                            'initValueText' => $selectedSchool, // set the initial display text
//                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'placeholder' => 'Filter School',
                                'id' => 'student_school_search',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 3,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                                ],
                                'ajax' => [
                                    'url' => $url,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],

                            ],
                        ])->label('School'); ?>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model2, 'student_campus')->dropDownList(['prompt' => 'Select Campus'], ['id' => 'student_campus_list']) ?>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model2, 'class_id')->dropDownList(['prompt' => 'Select Class'], ['id' => 'student_class_list']) ?>
                    </div>

                <?php elseif (\app\components\ToWords::isSchoolUser()) : ?>
                    <div class="col-xs-12 col-sm-6 col-lg-6">
                        <?= $form->field($model2, 'class_id')->dropDownList(ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'class_code'), ['prompt' => '---All Courses---']); ?>
                    </div>
                    <?php if (Yii::$app->user->identity->school->hasModule('SCHOOL_CAMPUSES')) : ?>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <?= $form->field($model2, 'student_campus')->dropDownList(ArrayHelper::map(SchoolCampuses::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all(), 'id', 'campus_name'), ['prompt' => '---All Campuses---']); ?>
                        </div>
                    <?php endif; ?>

                <?php endif; ?>

                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <?= $form->field($model2, 'gender')->dropDownList(['' => '--All--', 'M' => 'Male', 'F' => 'Female']) ?>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <?= $form->field($model2, 'date_from')->widget(DatePicker::className(),
                        [
                            'dateFormat' => 'yyyy-MM-dd',
                            'clientOptions' => [
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'Placeholder' => 'Select date',
                            ],
                        ])->label('From'); ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <?= $form->field($model2, 'date_to')->widget(DatePicker::className(),
                        [
                            'dateFormat' => 'yyyy-MM-dd',
                            'clientOptions' => [
                                'changeMonth' => true,
                                'changeYear' => true,
                                'yearRange' => '1900:' . (date('Y') + 1),
                                'autoSize' => true,
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'Placeholder' => 'Select date',
                            ],
                        ])->label('To'); ?>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <br><br>

                    <div class="radio radio-danger radio-inline">
                        <input type="radio" id="neg" name="DynamicModel[bal_status]" value="neg">
                        <label for="neg"> Negative Bal</label>
                    </div>
                    <div class="radio radio-info radio-inline">
                        <input type="radio" id="pos" name="DynamicModel[bal_status]" value="pos">
                        <label for="pos">Positive Bal</label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" id="zero" name="DynamicModel[bal_status]" value="zero">
                        <label for="zero">Zero Bal</label>
                    </div>
                    <div class="radio radio-info radio-inline">
                        <input type="radio" name="DynamicModel[bal_status]" value="" checked>
                        <label for="zero">All</label>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <?php echo $this->render('stu_select_form'); ?>
            </div>

        </div> <!--/ box-body -->
        <div class="box-footer">
            <?= Html::submitButton('Generate', ['class' => 'btn btn-primary generate-btn']) ?>
            <?php ActiveForm::end(); ?>
        </div> <!--/ box -->
    </div>
    </div>
    </div>
<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model2->class_id;

$campusurl = Url::to(['/schoolcore/core-school/campuslists']);
$campus = $model2->student_campus;

$script = <<< JS
var schoolChanged = function() {
        var sch = $("#student_school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('#student_class_list').html(data);
                    $('#student_class_list').val('$cls');
                });
        
        $.get('$campusurl', {id : sch}, function( data ) {
                    $('#student_campus_list').html(data);
                    $('#student_campus_list').val('$campus');
                });
    }
    



$("document").ready(function(){ 
  $(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#stu_report").addClass('active');
    
    $('body').on('change', '#student_school_search', function(){
         schoolChanged();
    });
    
  });
JS;
$this->registerJs($script);
?>

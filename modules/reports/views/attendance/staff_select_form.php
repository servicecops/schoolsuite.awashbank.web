
<div class="container">
    <div class="checkbox checkbox-info">
        <div>
            <input type="checkbox" class="checkall selectall" id="check_all_id">
            <label for="check_all_id"><b> Check All</b> </label>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="first_name" value="first_name"/><label
                        for="first_name" class="checkbox-inline"> &nbsp;First Name</label>
            </div>

            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="last_name" value="last_name"/> <label
                        for="last_name" class="checkbox-inline">&nbsp;Last Name </label>
            </div>

            <div>
                <input type="checkbox" class="individual" name="s_info[]" id="user_level" value="user_level"/> <label
                        for="user_level" class="checkbox-inline">&nbsp;The User Level </label>
            </div>
          <?php  if(Yii::$app->user->can('schoolpay_admin')){ //For admin context, school is required?>

              <div>
                  <input type="checkbox" class="individual" name="s_info[]" id="user_level" value="school_name"/> <label
                          for="school_name" class="checkbox-inline">&nbsp;School Name</label>
              </div>

                   <?php }?>
        </div>


    </div>
</div>

<?php




$script = <<< JS

    

$(".checkall").click(function(){
$(".checkbox_inline").prop("checked",$(this).prop("checked"));
});


$(".selectall").click(function(){
$(".individual").prop("checked",$(this).prop("checked"));
});
JS;
$this->registerJs($script);
?>

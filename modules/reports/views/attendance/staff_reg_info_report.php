<?php

use app\modules\schoolcore\models\CoreStudent;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

$this->title = Yii::t('app', 'Staff Info Report');
$this->params['breadcrumbs'][] = $this->title;

$_SESSION['query']=$query;
$_SESSION['selected_list']=$selected_list;
?>

<div class="row card">
    <div>
        <h3>List of Teachers Enrolled</h3>
    </div>
    <div class="float-right">
        <?php echo Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['/reports/attendance/staff-info'],['class'=>'btn-sm btn-danger', ]);?>
        <?php echo Html::a('<i class="fa fa-file-excel"></i> Excel',['students/selected-student-list','studentlistexcelexport'=>'studentlistexcel'],array('title'=>'Export to Excel','target'=>'_blank','class'=>'btn-sm btn-success', 'style'=>'color:#fff'));?>
        <?php echo Html::a('<i class="fa fa-file-pdf"></i> PDF',array('students/selected-student-list','studentlistexport'=>'studentlistpdf'),array('title'=>'Export to PDF','target'=>'_blank','class'=>'btn-sm btn-warning', 'style'=>'color:#fff')); ?>
    </div>
    <div class="card-body">
        <div style="overflow-x:auto;">
            <?php
            if(!empty($student_data))
            {
                $s_info = new \app\modules\schoolcore\models\CoreStaff();
                echo "<table class ='table table-striped'>";
                echo "<thead>";
                echo "<tr>";
                foreach($selected_list as $s)
                {
                    echo "<th class='text-left'>".Html::activeLabel($s_info,$s)."</th>";
                }
                echo "</tr>";
                echo "</thead>";
                foreach($student_data as $t=>$sd)
                {
                    echo "<tr>";
                    foreach($selected_list as $s){
                        echo "<td class='text-left'>".$sd[$s]."</td>";
                    }

                    echo "</tr>";

                }

                echo "</table>";
            }
            ?>
            <div style="padding-left:2em"><?= LinkPager::widget([ 'pagination' => $pages]); ?></div>
        </div>
    </div>
</div>

<?php
$script = <<< JS
$("document").ready(function(){ 
	$(".active").removeClass('active');
    $("#reports").addClass('active');
    $("#stu_report").addClass('active');
  });
JS;
$this->registerJs($script);
?>



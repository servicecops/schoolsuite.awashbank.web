<?php

namespace app\modules\reports\models;


/**
 * StudentSearch represents the model behind the search form about `app\models\Student`.
 */
class TransactionFeeUnpaidPerformanceHelperModel
{

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['date_created', 'student_code', 'student_name', 'fee_name', 'amount','class_code','student_phone','guardian_phone'],
            'fileName' => 'Fees Performance',
            'title' => 'Fees Performance',
            'exportFile' => '/student/exportPdfExcel',
        ];

        return $data;
    }

}

<?php

namespace app\modules\reports\models;


/**
 * MeetingMinutesHelper represents the model behind the report for meeting minutes `.
 */
class MeetingMinutesHelperModel
{

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            //'data' => $_SESSION['meeting_report_data'],
            'labels' => ['title', 'timetable_schedule', 'datetime', 'chaiperson', 'attendee_list', 'absentee_list'],
            'fileName' => 'Meeting Minutes Report',
            'title' => 'Meeting Minutes Performance',
            //'exportFile' => '/student/exportPdfExcel', //does not exist
            'exportFile' => '@app/views/common/exportPdfExcel'
        ];

        return $data;
    }

}
<?php

namespace app\modules\reports\models;


/**
 * StudentSearch represents the model behind the search form about `app\models\Student`.
 */
class TransactionFeePerformanceHelperModel
{

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['payment_date', 'student_code', 'student_name', 'fee_name', 'amount','description', 'class_code','student_phone','guardian_phone'],
            'fileName' => 'Fees Performance',
            'title' => 'Fees Performance',
            'exportFile' => 'exportPdfExcel',
        ];

        return $data;
    }

}

<?php

namespace app\modules\reports\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Expression;
use yii\db\Query;

/**
 * LoginForm is the model behind the login form.
 */
class Reports extends \yii\db\ActiveRecord
{

    /**
     * This is the model class for table "school_information".
     *
     * @property integer $id
     * @property integer $acct_no
     * @property string $depositor_payee_nm
     * @property string $crncy_cd_iso
     * @property string $dr_cr_ind
     * @property string $origin_bu_id
     * @property string $stmnt_bal
     * @property string $trans_desc
     * @property integer $tran_dt
     * @property string $tran_journal_id
     * @property string $txn_amt
     * @property string $txn_reference
     * @property string $school_id
     */

    public $date_from, $date_to, $branch_region, $branch_id, $from_date, $to_date;


    public static function tableName()
    {
        return 'recon_temporary_table';
    }

    public static function getExportQuery()
    {


        $data = [
            'data' => $_SESSION['findData']['query'],
            'labels' => $_SESSION['findData']['cols'],
            'title' => $_SESSION['findData']['title'],
            'fileName' => $_SESSION['findData']['file_name'],
            'exportFile' => $_SESSION['findData']['exportFile'],
        ];
        return $data;
    }

    /**
     * @inheritdoc
     *
     * /**
     * @return array the validation rules.
     */

    public function rules()
    {


        return [
            [['acct_no', 'transaction_id', 'description', 'currency', 'school_id', 'booking_date', 'amount'], 'string'],

            [['acct_no', 'amount', 'school_id'], 'integer'],
            [['date_from', 'date_to', 'branch_id', 'branch_region',], 'safe'],
            [['from_date', 'to_date',], 'safe'],
        ];
    }

    public function NoSchTxnis()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive) {
            session_start();
        }

        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days")) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date . "+1 days"));
        $_SESSION['query'] = ['from' => $from_date, 'to' => $to_date];

        $subquery = (new Query)
            ->select('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.date_created,cs.branch_id,cs.branch_region,cs.region')
            ->from('core_school cs')
            ->leftJoin('payments_received pr', 'pr.school_id=cs.id')
            ->where(['pr.id' => null]);

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'tr.date_created', 'ab.branch_name', 'abr.bank_region_name', 'ref.description', 'count(cs2.school_id) as Stds'])
            ->from(['tr' => $subquery])
            ->innerJoin('core_student cs2', 'cs2.school_id = tr.id')
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy('tr.id ,tr.external_school_code,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function NoSchTxns($from_date)
    {


        $subquery = (new Query)
            ->select('cs.date_created,gl.last_transaction_date,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.date_created,cs.branch_id,cs.branch_region,cs.region')
            ->from('core_school cs')
            ->innerJoin('school_account_gl gl', 'gl.id =cs.school_account_id')
            ->leftJoin('payments_received pr', 'pr.school_id=cs.id')
            ->where(['pr.id' => null])
            ->orwhere(['<=', 'gl.last_transaction_date', $from_date]);

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.last_transaction_date', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'tr.date_created', 'ab.branch_name', 'abr.bank_region_name', 'ref.description', 'count(cs2.school_id) as Stds'])
            ->from(['tr' => $subquery])
            ->innerJoin('core_student cs2', 'cs2.school_id = tr.id')
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy('tr.id ,tr.external_school_code,tr.last_transaction_date,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.last_transaction_date ASC');
        unset($_SESSION['findData']);
        // $_SESSION['findData'] = $query;
        $this->setSession('Inactive Schools', $query, $this->getColz(), 'inactive_schools');
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    protected function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;


        $_SESSION['findData']['exportFile'] = '@app/views/common/exportPdfExcel';

//        if(isset($_SESSION['PENALTY_MODE']=='penalty')) {
//            $_SESSION['findData']['exportFile'] = '@app/modules/schoolcore/views/school-information/exportPenaltyPdfExcel';
//
//        }
    }

    public function getColz()
    {
        return [
            'bank_region_name' => ['label' => 'Country Region'],
            'description' => ['label' => 'Region'],
            'branch_name' => ['label' => 'Branch '],
            'external_school_code' => ['label' => 'School ID '],
            'school_name' => ['label' => 'School Nme '],
            'Stds' => ['label' => 'Number of Students '],
            'last_transaction_date' => ['label' => 'Last Transaction Date'],

        ];
    }

    public function SchUser($from_date)
    {


        $subquery = (new Query)
            ->select('distinct( user_name ) , school_id')
            ->from('web_console_log cs')
            ->innerJoin('school_account_gl gl', 'gl.id =cs.school_account_id')
            ->leftJoin('payments_received pr', 'pr.school_id=cs.id')
            ->where(['pr.id' => null])
            ->orwhere(['<=', 'gl.last_transaction_date', $from_date]);

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.last_transaction_date', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'tr.date_created', 'ab.branch_name', 'abr.bank_region_name', 'ref.description', 'count(cs2.school_id) as Stds'])
            ->from(['tr' => $subquery])
            ->innerJoin('core_student cs2', 'cs2.school_id = tr.id')
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy('tr.id ,tr.external_school_code,tr.last_transaction_date,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.last_transaction_date ASC');
        unset($_SESSION['findData']);
        // $_SESSION['findData'] = $query;
        $this->setSession('Inactive Schools', $query, $this->getColz(), 'inactive_schools');
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function getCols()
    {
        return [
            'brn' => ['label' => 'Country Region'],
            'br' => ['label' => 'Branch '],
            'sc' => ['label' => 'School ID '],
            'sn' => ['label' => 'School Name '],
            'sum' => ['label' => 'Number of Transactions '],
            'count' => ['label' => 'Transactions Value'],

        ];
    }

    public function NoFeesSet($params)
    {

        $this->load($params);
//        $today = date('Y-m-d', time() + 86400);
//        //If admin and no filters at all, limit to past week
//        //Since this could fetch so many records
//        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
//            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
//        }
//
//
//        $date_to = $this->date_to ? $this->date_to : $today;
//

        $subquery = (new Query)
            ->select('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.branch_id,cs.branch_region,cs.region')
            ->from('core_school cs')
            ->leftJoin('institution_fees_due fd', 'fd.school_id=cs.id')
            ->where(['fd.id' => null]);

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'ab.branch_name', 'abr.bank_region_name', 'ref.description', 'count(cs2.school_id) as Stds'])
            ->from(['tr' => $subquery])
            ->innerJoin('core_student cs2', 'cs2.school_id = tr.id')
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');

//
//        if (isset($this->date_from)) {
////If school user, limit and find by id and school id
//            $subquery->andFilterWhere(['between', 'fd.date_created', $this->date_from, $date_to]);
//
//        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy('tr.id ,tr.external_school_code,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.date_created ASC');
        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;

        // $_SESSION['findData'] = $query;
        $this->setSession('No set Fees', $query, $this->getColf(), 'school_without_set_fees');
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }
public function feesSet($params)
    {

        $this->load($params);
        $today = date('Y-m-d', strtotime($this->date_to . ' 1 days'));
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }


        $date_to = $this->date_to ? $this->date_to : $today;
        $date_to = date('Y-m-d', strtotime($date_to . ' +1 day'));




        $subquery = (new Query)
            ->select(['distinct(cs.id) as sch','cs.date_created','cs.external_school_code','cs.school_name','cs.school_code', 'cs.branch_id','cs.branch_region','cs.region','ab.branch_name','abr.bank_region_name','ref.description','count(cs2.school_id) as Stds'])
            ->from('core_school cs')
            ->innerJoin('core_student cs2', 'cs2.school_id = cs.id')
            ->innerJoin('institution_fees_due fd', 'fd.school_id = cs.id')
            ->innerJoin('awash_branches ab', 'ab.id = cs.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = cs.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=cs.branch_region');
        $subquery->groupBy('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.branch_id,cs.branch_region,cs.region,ab.branch_name,abr.bank_region_name,ref.description');


//        $query = (new Query)
//            ->select(['distinct(tr.sch)','tr.date_created','tr.school_name','tr.external_school_code','tr.school_code', 'tr.branch_name','tr.bank_region_name','tr.description','tr.Stds'])
//            ->from(['tr'=>$subquery])
//            ->innerJoin('institution_fees_due fd', 'fd.school_id = tr.sch');


        if (isset($this->date_from)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['between', 'fd.date_created', $this->date_from, $date_to]);

        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $subquery->andWhere(['cs.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $subquery->andWhere(['cs.branch_region' => Yii::$app->user->identity->region_id]);

        }

        $pages = new Pagination(['totalCount' => $subquery->count()]);
        $sort = new Sort([
            'attributes' => [
                'cs.external_school_code', 'cs.school_name', 'cs.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $subquery->orderBy($sort->orders) : $subquery->orderBy('cs.date_created ASC');
        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;

        // $_SESSION['findData'] = $query;
        $this->setSession('Schools with set Fees', $subquery, $this->getColf(), 'school_with_set_fees');
    $subquery->offset($pages->offset)->limit($pages->limit);
        return ['query' => $subquery->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function getColf()
    {
        return [
            'bank_region_name' => ['label' => 'Country Region'],
            'description' => ['label' => 'Region'],
            'branch_name' => ['label' => 'Branch '],
            'external_school_code' => ['label' => 'School ID '],
            'school_name' => ['label' => 'School Nme '],
            'Stds' => ['label' => 'Number of Students '],

        ];
    }

    public function NoStudents($params)
    {
        $this->load($params);
//        $today = date('Y-m-d', time() + 86400);
//        //If admin and no filters at all, limit to past week
//        //Since this could fetch so many records
//        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
//            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
//        }
//
//
//        $date_to = $this->date_to ? $this->date_to : $today;


        $subquery = (new Query)
            ->select('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.branch_id,cs.branch_region,cs.region')
            ->from('core_school cs')
            ->leftJoin('core_student cs2', 'cs2.school_id = cs.id')
            ->where(['cs2.school_id' => null]);

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'ab.branch_name', 'abr.bank_region_name', 'ref.description'])
            ->from(['tr' => $subquery])
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');

//
//        if (isset($this->date_from)) {
////If school user, limit and find by id and school id
//            $subquery->andFilterWhere(['between', 'cs2.created_at', $this->date_from, $date_to]);
//
//        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }
        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy('tr.id ,tr.external_school_code,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.date_created ASC');
        unset($_SESSION['findData']);

        $this->setSession('Schools Without Student Data', $query, $this->getColn(), 'school_without_std_data');

        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function getColn()
    {
        return [
            'bank_region_name' => ['label' => 'Bank Region'],
            'description' => ['label' => 'Country Region'],
            'branch_name' => ['label' => 'Branch '],
            'external_school_code' => ['label' => 'School ID '],
            'school_name' => ['label' => 'School Nme '],

        ];
    }

    public function schPerfomance($params, $from_date, $to_date)
    {


        $this->load($params);
        $date_to = date('Y-m-d', strtotime($to_date . ' +1 day'));


        $subquery = (new Query)
            ->select('si.id as sch_id, si.school_name, si.school_code, bank_region_name,branch_name,count(si.school_name) ,  sum(pr.amount) as val, count(pr.school_id) as txns, ')
            ->from('payments_received pr ')
            ->innerJoin('core_school si', 'si.id= pr.school_id')
            ->innerJoin('awash_branches ab', 'ab.id = si.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = si.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=si.branch_region')
            ->groupBy('si.id, si.school_name, si.school_code, bank_region_name,branch_name');


        if (isset($from_date)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['between', 'pr.date_created', $from_date, $date_to]);

        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['si.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['si.branch_region' => $this->branch_region]);

        }
        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $subquery->andWhere(['si.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $subquery->andWhere(['si.branch_region' => Yii::$app->user->identity->region_id]);

        }


        $query = new Query();
        $query->select(['trans.school_name ,trans.branch_name,trans.school_code,bank_region_name,count(std.school_id) as number_of_students, trans.val as transaction_value, trans.txns as txnz '])
            ->from(['trans' => $subquery])
            ->innerJoin('core_student std', 'std.school_id  = trans.sch_id');


        $query->groupBy('trans.school_name , trans.val , std.school_id ,trans.school_code,bank_region_name, trans.txns, trans.branch_name');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'trans.external_school_code', 'trans.school_name', 'trans.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('trans.val DESC');
        unset($_SESSION['findData']);

        $this->setSession('Schools Transaction Summary', $query, $this->getColT(), 'school_transaction_report');

      //  $query->offset($pages->offset)->limit($pages->limit);

        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function getColT()
    {
        return [
            'bank_region_name' => ['label' => 'Branch Region'],
            'branch_name' => ['label' => 'Branch '],
            'school_name' => ['label' => 'School Nme '],
            'number_of_students' => ['label' => 'Number of Students '],
            'txnz' => ['label' => 'Number of Transactions'],
            'transaction_value' => ['label' => 'Transaction Value '],

        ];
    }

    public function Students($params)
    {

        $this->load($params);
        $today = date('Y-m-d', strtotime($this->date_to . ' 1 days'));
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }


        $date_to = $this->date_to ? $this->date_to : $today;
        $date_to = date('Y-m-d', strtotime($date_to . ' +1 day'));



        $subquery = (new Query)
            ->select('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.branch_id,cs.branch_region,cs.region, count(cs2.school_id) as stds')
            ->from('core_school cs')
            ->innerJoin('core_student cs2', 'cs2.school_id = cs.id')
            ->where(['cs2.archived' => false, 'cs2.active'=>true])
            ->groupBy('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.branch_id,cs.branch_region,cs.region');

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'ab.branch_name', 'abr.bank_region_name', 'ref.description', 'tr.stds'])
            ->from(['tr' => $subquery])
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');
        if (isset($this->date_from)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['between', 'cs2.created_at', $this->date_from, $date_to]);

        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }
        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }


        $query->groupBy('tr.id ,tr.stds,tr.external_school_code,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.date_created ASC');
        unset($_SESSION['findData']);

        $this->setSession('Schools With Student Data', $query, $this->getColp(), 'school_with_std_data');

        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function getColp()
    {
        return [
            'bank_region_name' => ['label' => 'Branch Region'],
            'description' => ['label' => 'Country Region'],
            'branch_name' => ['label' => 'Branch '],
            'external_school_code' => ['label' => 'School ID '],
            'school_name' => ['label' => 'School Nme '],
            'stds' => ['label' => 'Number of Students '],

        ];
    }
    public function getColTx()
    {
        return [
            'bank_region_name' => ['label' => 'Branch Region'],
            'description' => ['label' => 'Country Region'],
            'branch_name' => ['label' => 'Branch '],
            'external_school_code' => ['label' => 'School ID '],
            'school_name' => ['label' => 'School Nme '],
            'last_transaction_date' => ['label' => 'Last Transaction Date'],
            'status' => ['label' => 'Status '],

        ];
    }
   public function getColstd()
    {
        return [
            'bank_region_name' => ['label' => 'Branch Region'],
            'description' => ['label' => 'Country Region'],
            'branch_name' => ['label' => 'Branch '],
            'school_name' => ['label' => 'School Name '],
            'ttusers' => ['label' => 'Total Number of Users '],
            'actuser' => ['label' => 'Active  Users '],
            'inactive' => ['label' => 'Inactive  Users '],
            'stds' => ['label' => 'Number of Students '],

        ];
    }
    public function getColstaff()
    {
        return [
            'bank_region_name' => ['label' => 'Branch Region'],
            'description' => ['label' => 'Country Region'],
            'branch_name' => ['label' => 'Branch '],
            'school_name' => ['label' => 'School Name '],
            'ttusers' => ['label' => 'Total Number of Users '],
            'actuser' => ['label' => 'Active  Users '],
            'inactive' => ['label' => 'Inactive  Users '],


        ];
    }

    public function channelPerfomance($params)
    {

        $subquery = (new Query)
            ->select('cs.date_created,cs.id,cs.external_school_code,cs.school_name,cs.school_code, cs.branch_id,cs.branch_region,cs.region')
            ->from('core_school cs')
            ->leftJoin('core_student cs2', 'cs2.school_id = cs.id')
            ->where(['cs2.school_id' => null]);

        $query = new Query();
        $query->select(['tr.id', 'tr.date_created', 'tr.external_school_code', 'tr.school_name', 'tr.external_school_code', 'ab.branch_name', 'abr.bank_region_name', 'ref.description'])
            ->from(['tr' => $subquery])
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');

        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $query->andWhere(['tr.branch_region' => Yii::$app->user->identity->region_id]);

        }
        $query->groupBy('tr.id ,tr.external_school_code,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.date_created ASC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function branchActivity($params)
    {

        $this->load($params);
        $today = date('Y-m-d', strtotime($this->date_to . ' 1 days'));
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }


        $date_to = $this->date_to ? $this->date_to : $today;
        $date_to = date('Y-m-d', strtotime($date_to . ' +1 day'));


        $subquery1 = (new Query)
            ->select(['pr.id as pid', 'pr.school_id'])
            ->from('payments_received pr');
        if (isset($from_date)) {
//If school user, limit and find by id and school id
            $subquery1->andFilterWhere(['between', 'pr.date_created', $from_date, $date_to]);

        }
        $subquery2 = new Query();
        $subquery2->select(['school_name', 'branch_name', 'description', 'bank_region_name',
            'count(trans.pid) as number_of_transactions',
            'case when count(trans.pid)=0 then true else null end as inactive',
            'case when count(trans.pid)>0 then true else null end as active'])
            ->from(['trans' => $subquery1])
            ->rightJoin('core_school si', 'si.id = trans.school_id ')
            ->innerJoin('awash_branches ab', 'ab.id = si.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = si.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=si.branch_region')
            ->groupBy('school_name,branch_name, description,bank_region_name');


        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery2->andFilterWhere(['si.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery2->andFilterWhere(['si.branch_region' => $this->branch_region]);

        }
        if (Yii::$app->user->can('view_by_branch')) {
//If school user, limit and find by id and school id
            $subquery2->andWhere(['si.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {
//If school user, limit and find by id and school id
            $subquery2->andWhere(['si.branch_region' => Yii::$app->user->identity->region_id]);

        }


        $subquery3 = new Query();
        $subquery3->select(['trans2.branch_name', 'trans2.description', 'trans2.bank_region_name', 'count(trans2.inactive) as inactive_schools',
            'count(trans2.active) as active_schools'])
            ->from(['trans2' => $subquery2])
            ->groupBy('branch_name, description,bank_region_name');

        $query = new Query();
        $query->select(['trans3.branch_name', 'trans3.description', 'trans3.bank_region_name', 'trans3.inactive_schools', 'trans3.active_schools'])
            ->from(['trans3' => $subquery3])
            ->groupBy('trans3.branch_name,trans3.description ,trans3.bank_region_name,trans3.inactive_schools,trans3.active_schools');


        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'branch_name', 'branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('branch_name DESC');
        unset($_SESSION['findData']);

        $this->setSession('Schools Transaction Summary', $query, $this->getColT(), 'school_transaction_report');

        $query->offset($pages->offset)->limit($pages->limit);

        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function schoolStatus($params)
    {

        $request = Yii::$app->request;
        $this->load($params);

        Yii::trace($this->date_from);
        if (!Yii::$app->session->isActive) {
            session_start();
        }

        $from_date = empty($this->date_from) ? date('Y-m-d', strtotime("-90 days")) : date('Y-m-d', strtotime($this->date_from . ' -90 days'));
        Yii::trace($from_date);
       // $_SESSION['query'] = ['from' => $from_date, 'to' => $to_date];

        $subquery = (new Query)
            ->select(['cs.id as ids', 'cs.date_created', 'gl.last_transaction_date',
                'cs.external_school_code', 'cs.school_name', 'cs.school_code', 'cs.branch_id', 'cs.branch_region', 'cs.region',
                new Expression("case when gl.last_transaction_date is null or gl.last_transaction_date <= '$from_date'  then 'INACTIVE' else 'ACTIVE' end as status")
            ])
            ->from('core_school cs')
            ->join('full outer join', 'school_account_gl gl', 'cs.school_account_id =gl.id');
            //->where(['gl.last_transaction_date' => $from_date]);
           // ->bindValue(':last_txn_date', $from_date);
        //  'case when "gl.last_transaction_date" is null or "gl.last_transaction_date" <= "from_date"  then "INACTIVE" else "ACTIVE" end as status'

        // tr.status,
        $query = new Query();
        $query->select(['tr.ids', 'tr.date_created', 'tr.last_transaction_date', 'tr.external_school_code', 'tr.school_name', 'ab.branch_name',
'abr.bank_region_name', 'ref.description', 'tr.status'])
            ->from(['tr' => $subquery])
           // ->innerJoin('core_student cs2', 'cs2.school_id = tr.ids')
            ->innerJoin('awash_branches ab', 'ab.id = tr.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = tr.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=tr.branch_region');
           // $query->where(['cs2.archived'=>false, 'cs2.active'=>true]);


//        if (isset($this->date_from)) {
////If school user, limit and find by id and school id
//            $subquery->andFilterWhere(['between', 'gl.last_transaction_date', $this->date_from, $end_date]);
//
//        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }

        if (Yii::$app->user->can('view_by_branch')) {

            $subquery->andWhere(['cs.branch_id' => Yii::$app->user->identity->branch_id]);

        }

        if (Yii::$app->user->can('view_by_region')) {

            $subquery->andWhere(['cs.branch_region' => Yii::$app->user->identity->region_id]);

        }


        $query->groupBy('tr.ids,tr.status ,tr.last_transaction_date,tr.external_school_code,tr.school_name ,tr.date_created,tr.date_created,ab.branch_name,abr.bank_region_name,tr.external_school_code,ref.description');

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('tr.last_transaction_date asc');
        unset($_SESSION['findData']);

        $this->setSession('Schools Status', $query, $this->getColTx(), 'school_with_std_data');


        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function stdUserz($params)
    {

        $this->load($params);
        $today = date('Y-m-d', strtotime($this->date_to . ' 1 days'));
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }


        $date_to = $this->date_to ? $this->date_to : $today;
        $date_to = date('Y-m-d', strtotime($this->date_to . ' +1 day'));


        $subquery = (new Query)
            ->select(['distinct(username ) , u.school_id '])
            ->from('user u')
            ->innerjoin('web_console_log wcl', 'u.school_id = wcl.school_id')
            ->where(['u.user_level' => 'is_student']);
        $subquery2 = (new Query)
            ->select(['cs.id, cs.school_name, count(u.school_id)  as ttusers , cs.branch_id, cs.region, cs.branch_region'])
            ->from('user u')
            ->innerjoin('core_school cs','cs.id = u.school_id')
            ->where(['u.user_level' => 'is_student'])
            ->groupBy('cs.school_name , cs.id');

        $subquery1 = (new Query)
            ->select(['count(tr.school_id) as actuser ,tr.school_id'])
            ->from(['tr' => $subquery])
            ->groupBy('tr.school_id');
       // $subquery1->all();



        $subquery3 = (new Query)
            ->select(['tt.id','tt.school_name', 'tt.ttusers', 'tt.branch_id', 'tt.region', 'tt.branch_region'])
            ->from(['tt' => $subquery2]);
          //  ->groupBy('tt.id ');


        $query = (new Query);
        $query->select(['ab.branch_name','rr.description', 'school_name','abr.bank_region_name' ,'ttusers','actuser',
        'school_name','abr.bank_region_name' , 'count(cs.school_id) as stds ',new Expression('ttusers-actuser as inactive')])
           ->from(['tz' => $subquery3])
            ->innerJoin(['ty' => $subquery1], 'ty.school_id = tz.id')
            ->innerJoin('core_student cs', 'cs.school_id = tz.id')
            ->innerJoin('awash_branches ab', 'ab.id  =tz.branch_id')
            ->innerJoin('ref_region rr', 'rr.id  = tz.region')
            ->innerJoin('awash_bank_region abr', ' abr.id=tz.branch_region');

        if (isset($this->date_from)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['between', 'wcl.event_date', $this->date_from, $date_to]);

        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery2->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery2->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }
        if (Yii::$app->user->can('view_by_branch')) {

            $subquery2->andWhere(['cs.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {

            $subquery2->andWhere(['cs.branch_region' => Yii::$app->user->identity->region_id]);

        }


        $query->groupBy('ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser');
        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
              //  'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('abr.bank_region_name DESC');
        unset($_SESSION['findData']);

        $this->setSession('Schools Student Users', $query, $this->getColstd(), 'student_users');

       $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

    public function schUserz($params)
    {

        $this->load($params);
        $today = date('Y-m-d', strtotime($this->date_to . ' 1 days'));
        //If admin and no filters at all, limit to past week
        //Since this could fetch so many records
        if (Yii::$app->user->can('schoolpay_admin') && empty($params)) {
            $this->date_from = date('Y-m-d', strtotime($this->date_to . ' -30 days'));
        }


        $date_to = $this->date_to ? $this->date_to : $today;
        $date_to = date('Y-m-d', strtotime($this->date_to . ' +1 day'));


        $subquery = (new Query)
            ->select(['cs.id', 'cs.school_name', 'count(u.school_id)  as ttusers' , 'cs.branch_id', 'cs.region', 'cs.branch_region'])
            ->from('user u')
            ->innerjoin('core_school cs','cs.id = u.school_id ')
            ->where(['<>','u.user_level' , 'is_student'])
            ->groupBy('cs.school_name , cs.id');
        $subquery2 = (new Query)
            ->select(['distinct( user_name )', 'school_id'])
            ->from('web_console_log wcl');


        $subquery1 = (new Query)
            ->select(['tt.id','tt.school_name', 'tt.ttusers', 'tt.branch_id', 'tt.region', 'tt.branch_region'])
            ->from(['tt' => $subquery]);
            //->groupBy('tr.school_id');
        // $subquery1->all();



        $subquery3 = (new Query)
            ->select(['count(tr.school_id) as actuser' ,'tr.school_id'])
            ->from(['tr' => $subquery2])
         ->groupBy('tr.school_id');


        $query = (new Query);
        $query->select(['school_name','ab.branch_name','rr.description', 'school_name','abr.bank_region_name' ,'ttusers','actuser',new Expression('ttusers-actuser as inactive')])
            ->from(['tz' => $subquery3])
            ->innerJoin(['ty' => $subquery1], 'tz.school_id = ty.id')
            ->innerJoin('core_student cs', 'cs.school_id = ty.id')
            ->innerJoin('awash_branches ab', 'ab.id  =ty.branch_id')
            ->innerJoin('ref_region rr', 'rr.id  = ty.region')
            ->innerJoin('awash_bank_region abr', ' abr.id=ty.branch_region');

        if (isset($this->date_from)) {
//If school user, limit and find by id and school id
            $subquery2->andFilterWhere(['between', 'wcl.event_date', $this->date_from, $date_to]);

        }
        if (isset($this->branch_id)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_id' => $this->branch_id]);

        }
        if (isset($this->branch_region)) {
//If school user, limit and find by id and school id
            $subquery->andFilterWhere(['cs.branch_region' => $this->branch_region]);

        }
        if (Yii::$app->user->can('view_by_branch')) {

            $subquery->andWhere(['cs.branch_id' => Yii::$app->user->identity->branch_id]);

        }
        if (Yii::$app->user->can('view_by_region')) {

            $subquery->andWhere(['cs.branch_region' => Yii::$app->user->identity->region_id]);

        }

        $query->groupBy('ab.branch_name,rr.description, school_name,abr.bank_region_name ,ttusers,actuser');
        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                //  'tr.external_school_code', 'tr.school_name', 'tr.date_created', 'ab.branch_name', 'abr.branch_region_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('abr.bank_region_name DESC');
        unset($_SESSION['findData']);

        $this->setSession('Schools Staff Users', $query, $this->getColstaff(), 'school_staff_user_report');

        //$query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];


// return $request->isAjax ? $this->renderAjax('school_txns', $res) : $this->render('school_txns', $res);

    }

}

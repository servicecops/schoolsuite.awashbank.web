<?php

namespace app\modules\reports\models;


/**
 * StudentSearch represents the model behind the search form about `app\models\Student`.
 */
class BalanceAdjustmentHelperModel
{

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['event_date', 'student_code', 'student_name', 'class_description', 'user_full_name', 'ip_address', 'reason', 'balance_before', 'amount', 'balance_after'],
            'fileName' => 'Balance Adjustment Report',
            'title' => 'Balance Adjustment Report',
            'exportFile' => '/student/exportPdfExcel',
        ];

        return $data;
    }

}

<?php

namespace app\modules\reports\models;

use Yii;
use yii\base\BaseObject;
use yii\db\Query;

class HomeStats extends \yii\db\ActiveRecord
{

    public function sysAdmin()
    {
        $request = Yii::$app->request;
        $stuCount = (new Query())->from('core_student')->where(['active' => 't', 'archived' => 'f'])->count('*', Yii::$app->db);
        $schCount = (new Query())->from('core_school')->where(['active' => 't'])->count('*', Yii::$app->db);
        $inStuCount = (new Query())->from('core_student')->where(['active' => 'f'])->count('*', Yii::$app->db);
        $inSchCount = (new Query())->from('core_school')->where(['active' => 'f'])->count('*', Yii::$app->db);
        $inTrCount = (new Query())->from('core_staff')->where(['active' => 't'])->count('*', Yii::$app->db);
        $schsUsingAttendanceCount = (new Query())->from('teacher_clockin_attendance')->count('distinct(school_id)', Yii::$app->db);



        for ($i = 11; $i >= 0; $i--) {
            $thistime = @strtotime(@date('Y-m-01') . " -$i months");
            $months[@date("Ym", $thistime)] = @date("M Y", $thistime);
            $dummy[@date("Ym", $thistime)] = 0;
        }
        $d = @strtotime(@date('Y-m-01') . " -11 months");
        $lastday = @date('Y-m-d', $d);
        $today = @date('Y-m-d', time() + 86400);

        $monthsVal = array_values($months);
        $monTrans = $transCount = [];
        $channels = (new Query())
            ->select(['id', 'channel_name'])
            ->from('payment_channels')
            ->orderBy('date_created')
            ->all(Yii::$app->db);

        $monthlyData = (new Query())
            ->select(['EXTRACT(YEAR FROM date_created) AS yr', 'EXTRACT(MONTH FROM date_created) AS mon', 'payment_channel AS chn', 'SUM(amount) AS sm', 'count(amount) AS count'])
            ->from('payments_received pr')
            ->andWhere("pr.date_created::date>='$lastday'")
            ->andWhere("pr.date_created::date<='$today'")
//            ->andWhere(['between', 'pr.date_created::date', $lastday, $today])
            ->groupBy(['EXTRACT(YEAR FROM date_created)', 'payment_channel', 'EXTRACT(MONTH FROM date_created)'])
            ->orderBy(['EXTRACT(YEAR FROM date_created)' => 'SORT_DESC', 'EXTRACT(MONTH FROM date_created)' => 'SORT_ASC', 'payment_channel' => 'SORT_ASC'])
            ->all(Yii::$app->db);


        $schCats = (new Query())->select(['sct.description', 'COUNT(sch.id) AS count'])
            ->from('core_school sch')
            ->innerJoin('core_control_school_types sct', 'sct.id=sch.school_type')
            ->groupBy(['sct.description'])->all(Yii::$app->db);

        $users = (new Query())->select(['auth.description', 'COUNT(usr.id) AS count'])
            ->from('user usr')
            ->innerJoin('auth_item auth', 'auth.name=usr.user_level')
            ->groupBy(['auth.description'])->all(Yii::$app->db);

        $schTypes = array();
        $usrTypes = array();
        $ttle = ['sch' => 0, 'usr' => 0];
        foreach ($schCats as $v) {
            $schTypes[] = [$v['description'] . ' (' . number_format($v['count']) . ')', $v['count']];
            $ttle['sch'] = $ttle['sch'] + $v['count'];
        }

        foreach ($users as $v) {
            $usrTypes[] = [$v['description'] . ' (' . number_format($v['count']) . ')', $v['count']];
            $ttle['usr'] = $ttle['usr'] + $v['count'];
        }


        foreach ($channels as $chn) {
            $sums = $counts = $dummy;
            foreach ($monthlyData as $data) {
                if ($data['chn'] == $chn['id']) {
                    $m = str_pad($data['mon'], 2, '0', STR_PAD_LEFT);
                    $sums[$data['yr'] . $m] = intval($data['sm']);
                    $counts[$data['yr'] . $m] = intval($data['count']);
                }
            }
            $monTrans[] = ['name' => $chn['channel_name'], 'data' => array_values($sums),];
            $transCount[] = ['name' => $chn['channel_name'], 'data' => array_values($counts),];
            $sums = $counts = $dummy;
        }


        return ['months' => $monthsVal, 'monTrans' => $monTrans, 'transCount' => $transCount,
            'schCount' => ['active' => $schCount, 'inactive' => $inSchCount],
            'stuCount' => ['active' => $stuCount, 'inactive' => $inStuCount],
            'trCount' => ['active' => $inTrCount, 'schools_attendance'=>$schsUsingAttendanceCount],
            'schTypes' => $schTypes, 'usrTypes' => $usrTypes, 'ttle' => $ttle
        ];

    }

    public function schAdmin($searchModel = null)
    {
        Yii::trace($searchModel);
        $request = Yii::$app->request;
        $stuCount = (new Query())->from('core_student')->where(['active' => 't', 'archived' => 'f','school_id'=> Yii::$app->user->identity->school_id])->count('*', Yii::$app->db);
        $inStuCount = (new Query())->from('core_student')->where(['active' => 'f', 'school_id'=> Yii::$app->user->identity->school_id])->count('*', Yii::$app->db);
        $inTrCount = (new Query())->from('core_staff')->where(['active' => 't', 'school_id'=> Yii::$app->user->identity->school_id])->count('*', Yii::$app->db);

        $sch = Yii::$app->user->identity->school_id;
//        $years = Yii::$app->db2->createCommand("SELECT to_char(date_created, 'YYYY') as yr, to_char(date_created, 'Mon') as month FROM payments_received  WHERE reversed=false and reversal=false and school_id=" . $sch . " GROUP BY yr, month")->queryAll();
//        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        //Curly brackets do away with inconsistent double quotes
        $paymentChannelQuery = (new Query())->select(['{{C}}.channel_code', "to_char({{R}}.date_created, 'month') as yr", 'SUM({{R}}.amount) as amount'])
            ->from("{{payment_channels}} {{C}}")
            ->innerJoin('{{payments_received}} {{R}}', '{{C}}.id={{R}}.payment_channel')
            ->andWhere('{{R}}.reversed=false')
            ->andWhere('{{R}}.reversal=false')
            ->andFilterWhere(['{{R}}.school_id' => Yii::$app->user->identity->school_id])
            ->groupBy(['{{C}}.id', '{{C}}.channel_code', 'yr']);

        $startDate = null;
        $endDate = null;
        if ($searchModel) {
            if (isset($searchModel->start_date)) {
                $paymentChannelQuery->andFilterWhere(['>=', '{{R}}.date_created::date', $searchModel->start_date]);
                $startDate = $searchModel->start_date;
            }
            if (isset($searchModel->end_date)) {
                $paymentChannelQuery->andFilterWhere(['<=', '{{R}}.date_created::date', $searchModel->end_date]);
                $endDate = $searchModel->end_date;
            }
        }


//        $paymentChannels = Yii::$app->db2->createCommand("SELECT C.channel_code, to_char(R.date_created, 'month') as yr, SUM(R.amount) as amount FROM payment_channels C  JOIN payments_received R ON C.id=R.payment_channel WHERE R.reversed=false and R.reversal=false and R.school_id=" . Yii::$app->user->identity->school_id . " GROUP BY C.id, C.channel_code, yr ")->queryAll();
        $paymentChannels = $paymentChannelQuery->all(Yii::$app->db);

        $schGen = (new Query())->select(['gender', 'COUNT(gender) AS count'])->from('core_student')
            ->where(['class_id' => array_column($this->schCl(), 'id')])->groupBy(['gender'])->all(Yii::$app->db);

        $clStu = (new Query())->select(['sc.class_code', 'COUNT(si.id) AS count'])
            ->from('core_school_class sc')
            ->leftJoin('core_student si', 'si.class_id=sc.id')
            ->where(['sc.school_id' => $sch])
            ->andWhere(['<>', 'sc.class_code', '__ARCHIVE__'])
            ->groupBy(['sc.class_code'])->all(Yii::$app->db);

        $gender = [];
        foreach ($schGen as $v) {
            $gender[] = [($v['gender'] == 'M') ? 'Male (' . number_format($v['count']) . ') ' : 'Female (' . number_format($v['count']) . ') ', intval($v['count'])];
        }

        $clIDs = array_filter(array_column($this->schCl(), 'id'));
        $bal = $this->outstandingBal($clIDs);
        $paid = $this->collectedAmount($sch, $startDate, $endDate);
        $paidUnpaid = [['Outstanding Balance (' . number_format($bal, 2) . ')', intval($bal)], ['Amount Collected (' . number_format($paid, 2) . ' )', intval($paid)]];



        $periods = ['Today', 'Week', 'Month', 'Older'];
        $durations = ['1'=>'Today', '7'=>'Week', '30'=>'Month', '300'=>'Older'];

        //Instantiate rank with defaults
        $class_rank = [];
        foreach ($periods as $period) {
            $class_rank[$period] = [
                'l' => [
                    'cdes' => '',
                    'count' => 0,
                    'sum' => 0,
                ], 'h' => [
                    'cdes' => '',
                    'count' => 0,
                    'sum' => 0,
                ],
            ];
        }


        foreach ($durations as $k=>$v) {
            $thisRankList = $this->classRank($k, $startDate, $endDate);
            //For each returned rank record
            foreach ($thisRankList as $rank) {
                //If sum is less than the current $class_rank[period][l][sum], overwrite it
                //since it means this is the effective lowect collection class
                if($class_rank[$v]['l']['sum']==0 || $class_rank[$v]['l']['sum']>$rank['sum']) {
                    $class_rank[$v]['l'] = $rank;
                }

                //If sum is more than the current $class_rank[period][h][sum], overwrite it
                //since it means this is the effective highest collection class
                if($class_rank[$v]['h']['sum']<$rank['sum']) {
                    $class_rank[$v]['h'] = $rank;
                }
            }

        }




        //Instantiate rank with defaults
        $channel_rank = [];
        foreach ($periods as $period) {
            $channel_rank[$period] = [
                'l' => [
                    'cdes' => '',
                    'count' => 0,
                    'sum' => 0,
                ], 'h' => [
                    'cdes' => '',
                    'count' => 0,
                    'sum' => 0,
                ],
            ];
        }

        foreach ($durations as $k=>$v) {
            $thisRankList = $this->channelRank($k, $startDate, $endDate);
            //For each returned rank record
            foreach ($thisRankList as $rank) {
                //If sum is less than the current $channel_rank[period][l][sum], overwrite it
                //since it means this is the effective lowect collection class
                if($channel_rank[$v]['l']['sum']==0 || $channel_rank[$v]['l']['sum']>$rank['sum']) {
                    $channel_rank[$v]['l'] = $rank;
                }

                //If sum is more than the current $channel_rank[period][h][sum], overwrite it
                //since it means this is the effective highest collection class
                if($channel_rank[$v]['h']['sum']<$rank['sum']) {
                    $channel_rank[$v]['h'] = $rank;
                }
            }

        }

//        $channelList = $this->chnn();
//        $cl_today = $this->diffWithSum($channelList, $this->classRank('1', $startDate, $endDate), 'cdes');
//        $cl_week = $this->diffWithSum($channelList, $this->classRank('7', $startDate, $endDate), 'cdes');
//        $cl_month = $this->diffWithSum($channelList, $this->classRank('30', $startDate, $endDate), 'cdes');
//        $cl_older = $this->diffWithSum($channelList, $this->classRank('300', $startDate, $endDate), 'cdes');
//
//        $class_rank = ['Today' => ['h' => current($cl_today), 'l' => end($cl_today)],
//            'Week' => ['h' => current($cl_week), 'l' => end($cl_week)],
//            'Month' => ['h' => current($cl_month), 'l' => end($cl_month)],
//            'Older' => ['h' => current($cl_older), 'l' => end($cl_older)],
//        ];

//        $ch_today = $this->diffWithSum($channelList, $this->channelRank('1'), 'cdes');
//        $ch_week = $this->diffWithSum($channelList, $this->channelRank('7'), 'cdes');
//        $ch_month = $this->diffWithSum($channelList, $this->channelRank('30'), 'cdes');
//        $channel_rank = ['Today' => ['h' => current($ch_today), 'l' => end($ch_today)],
//            'Week' => ['h' => current($ch_week), 'l' => end($ch_week)],
//            'Month' => ['h' => current($ch_month), 'l' => end($ch_month)],
//        ];


        $channels = array();
        $year = "";
        foreach ($paymentChannels as $payChannel) {
            $channels[] = [$payChannel['channel_code'], $payChannel['amount']];
            $year .= $payChannel['yr'];
        }

        $classes = array();
        foreach ($clStu as $v) {
            $classes[] = [$v['class_code'] . ' (' . number_format($v['count']) . ')', intval($v['count'])];
        }

//        $amountCollected = Yii::$app->db2->createCommand();


        $renderArr = ['classes' => $classes, 'class_rank' => $class_rank, 'channel_rank' => $channel_rank, 'gender' => $gender, 'paidUnpaid' => $paidUnpaid,
            'stuCount' => ['active' => $stuCount, 'inactive' => $inStuCount],
            'trCount' => ['active' => $inTrCount]];

        return $renderArr;

    }

    protected function diffWithSum($arr1, $arr2, $col)
    {

        $rArray = $arr2;
        $dff = array_diff(array_column($arr1, $col), array_column($arr2, $col));
        foreach ($dff as $v) {
            $rArray[] = ['cdes' => $v, 'count' => 0, 'sum' => 0];
        }
        return $rArray;
    }

    protected function classRank($d, $startDate, $endDate)
    {
        $query = (new Query())
            ->select(['cl.class_description AS cdes', 'SUM(pr.amount) AS sum', 'COUNT(pr.amount) AS count'])
            ->from('payments_received {{pr}}')
            ->leftJoin('core_student {{si}}', '{{si}}.id = {{pr}}.student_id')
            ->leftJoin('core_school_class {{cl}}', '{{cl}}.id = {{si}}.class_id')
            ->where(['pr.school_id' => Yii::$app->user->identity->school_id])
            ->andWhere("{{pr}}.date_created::date > current_date - interval '" . $d . "' day")
            ->groupBy('cl.class_description')
            ->orderBy('SUM(pr.amount) DESC');

        //Add filter range
        if ($startDate) $query->andWhere(['>=', '{{pr}}.date_created::date', $startDate]);
        if ($endDate) $query->andWhere(['<=', '{{pr}}.date_created::date', $endDate]);


        $classRank = $query->all(Yii::$app->db);
        return $classRank;
    }

    // protected function

    protected function schCl()
    {
        $classModels = Yii::$app->db->createCommand("SELECT id, class_code, class_description AS cdes FROM core_school_class WHERE school_id=" . Yii::$app->user->identity->school_id . " ORDER BY class_code")->queryAll();
        return $classModels;
    }

    protected function channelRank($d, $startDate, $endDate)
    {
        $query = (new Query())
            ->select(['{{pc}}.channel_name AS cdes', 'SUM({{pr}}.amount) AS sum', 'COUNT({{pr}}.amount) AS count'])
            ->from('payments_received {{pr}}')
            ->leftJoin('payment_channels {{pc}}', '{{pc}}.id = {{pr}}.payment_channel')
            ->where(['{{pr}}.school_id' => Yii::$app->user->identity->school_id])
            ->andWhere("{{pr}}.date_created > current_date - interval '" . $d . "' day")
            ->groupBy('{{pc}}.channel_name')
            ->orderBy('SUM({{pr}}.amount) DESC');

        //Add filter range
        if ($startDate) $query->andWhere(['>=', '{{pr}}.date_created::date', $startDate]);
        if ($endDate) $query->andWhere(['<=', '{{pr}}.date_created::date', $endDate]);

        $channelRank = $query
            ->all(Yii::$app->db);
        return $channelRank;
    }


    protected function outstandingBal($cl)
    {
        $outstandingBal = (new Query())
            ->from('student_account_gl sgl')
            ->join('join', 'core_student si', 'si.student_account_id = sgl.id')
            ->where(['IN', 'class_id', $cl])
            ->andWhere(['si.active' => true])
            ->sum('sgl.outstanding_balance', Yii::$app->db);
        return abs($outstandingBal);
    }

    protected function collectedAmount($sch, $startDate, $endDate)
    {
        $query = (new Query())
            ->from('payments_received {{pr}}')
            ->innerJoin('core_student {{si}}', '{{si}}.id={{pr}}.student_id')
            ->where(['{{pr}}.school_id' => $sch])
            ->andWhere(['{{si}}.active' => true]);

        //Add filter range
        if ($startDate) $query->andWhere(['>=', '{{pr}}.date_created::date', $startDate]);
        if ($endDate) $query->andWhere(['<=', '{{pr}}.date_created::date', $endDate]);

        $totalAmount = $query
            ->sum('amount', Yii::$app->db);
        return abs($totalAmount);
    }

    protected function chnn()
    {
        $channels = (new Query())
            ->select(['channel_name AS cdes'])
            ->from('payment_channels')
            ->all(Yii::$app->db);
        return $channels;
    }

}

<?php
namespace app\modules\reports\models;

use Yii;
use yii\db\Query;

class GrowthStats extends \yii\db\ActiveRecord{

    public static function studentGrowth()
    {
        for ($i = 11; $i >= 0; $i--) {
            $thistime = @strtotime( @date( 'Y-m-01' )." -$i months");
            $months[@date("Ym", $thistime)] = @date("M Y", $thistime);
            $dummy[@date("Ym", $thistime)] = 0;
        }
        $d= @strtotime(@date('Y-m-01')." -11 months");
        $lastday = @date('Y-m-d', $d);
        $today = @date('Y-m-d', time()+86400);
        
            $request = Yii::$app->request;
            $monthsVal = array_values($months);
            $growthCount = [];

            $monthlyData = (new \yii\db\Query())
                ->select(['EXTRACT(YEAR FROM date_created) AS yr', 'EXTRACT(MONTH FROM date_created) AS mon','count(id) AS count'])
                ->from('student_information')
                ->andWhere(['between', 'date_created', $lastday, $today])
                ->groupBy(['EXTRACT(YEAR FROM date_created)', 'EXTRACT(MONTH FROM date_created)'])
                ->orderBy(['EXTRACT(YEAR FROM date_created)'=>'SORT_DESC', 'EXTRACT(MONTH FROM date_created)'=>'SORT_ASC'])
                ->all(Yii::$app->db2);

                $counts = $dummy;
                foreach($monthlyData as $data){
                    $m = str_pad($data['mon'], 2, '0', STR_PAD_LEFT);
                    $counts[$data['yr'].$m] = intval($data['count']);
                }
                $growthCount[] = ['name'=>'Students', 'data'=>array_values($counts), ];

                $res = [ 'months'=>$monthsVal, 'growthCount'=>$growthCount];

            return $res;
        
        
    }


}

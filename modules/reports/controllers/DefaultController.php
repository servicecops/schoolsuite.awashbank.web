<?php

namespace app\modules\reports\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

class DefaultController extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
    	for ($i = 11; $i >= 0; $i--) {
			$thistime = @strtotime( @date( 'Y-m-01' )." -$i months");
			$months[@date("Ym", $thistime)] = @date("M Y", $thistime);
			$dummy[@date("Ym", $thistime)] = 0;
		}
		$d= @strtotime(@date('Y-m-01')." -11 months");
		$lastday = @date('Y-m-d', $d);
		$today = @date('Y-m-d', time()+86400);

    	if(\app\components\ToWords::isSchoolUser()){
    		$request = Yii::$app->request;
	    	$monthsVal = array_values($months);
	    	$monTrans = $transCount = [];
	    	$channels = (new \yii\db\Query())
	    		->select(['id', 'channel_name'])
	    		->from('payment_channels')
	    		->all(Yii::$app->db);

			$monthlyData = (new \yii\db\Query())
				->select(['EXTRACT(YEAR FROM date_created) AS yr', 'EXTRACT(MONTH FROM date_created) AS mon', 'payment_channel AS chn', 'SUM(amount) AS sm', 'count(amount) AS count'])
				->from('payments_received')
				->where(['school_id'=>Yii::$app->user->identity->school_id])
				->andWhere(['between', 'date_created', $lastday, $today])
				->groupBy(['EXTRACT(YEAR FROM date_created)', 'payment_channel', 'EXTRACT(MONTH FROM date_created)'])
				->orderBy(['EXTRACT(YEAR FROM date_created)'=>'SORT_DESC', 'EXTRACT(MONTH FROM date_created)'=>'SORT_ASC', 'payment_channel'=>'SORT_ASC'])
				->all(Yii::$app->db);

				foreach($channels as $chn){
					$sums = $counts = $dummy;
					foreach($monthlyData as $data){
					if($data['chn']==$chn['id']){
						$m = str_pad($data['mon'], 2, '0', STR_PAD_LEFT);
						$sums[$data['yr'].$m] = intval($data['sm']);
						$counts[$data['yr'].$m] = intval($data['count']);
						}
					}
					$monTrans[] = ['name'=>$chn['channel_name'], 'data'=>array_values($sums), ];
					$transCount[] = ['name'=>$chn['channel_name'], 'data'=>array_values($counts), ];
					$sums = $counts = $dummy;
				}

	        return ($request->isAjax) ? $this->renderAjax('index', [
	        	'months'=>$monthsVal,
	        	'monTrans'=>$monTrans,
	        	'transCount'=>$transCount,
	        	]) :
	        	$this->render('index', [
		        	'months'=>$monthsVal,
		        	'monTrans'=>$monTrans,
		        	'transCount'=>$transCount,
		        ]);
    	} else {
        throw new ForbiddenHttpException('User is not affiliated to any school.');
      }

    }

    public function actionClas()
    {
    	if(\app\components\ToWords::isSchoolUser()){
    	$request = Yii::$app->request;
    	$d= @strtotime(@date('Y-m-01')." -11 months");
		$lastday = @date('Y-m-d', $d);
		$today = @date('Y-m-d');

    	$schClasses = $classDummy  = $classTrans = $classIds = [];
    	$classes = (new \yii\db\Query())
    		->select(['id', 'class_code'])
    		->from('core_school_class')
    		->where(['school_id'=>Yii::$app->user->identity->school_id])
    		->orderBy('id')
    		->all(Yii::$app->db);

    	$channels = (new \yii\db\Query())
    		->select(['id', 'channel_name'])
    		->from('payment_channels')
    		->all(Yii::$app->db);

    	foreach($classes as $cl){
    		$classDummy[$cl['id']] = 0;
    		$schClasses[] = $cl['class_code'];
    		$classIds[] = $cl['id'];
    	}

    	$classData= (new \yii\db\Query())
			->select([ 'si.class_id as cl', 'pr.payment_channel AS chn', 'SUM(pr.amount) AS sm'])
			->from('payments_received pr')
			->join('join', 'core_student si', 'si.id = pr.student_id')
			->where(['pr.school_id'=>Yii::$app->user->identity->school_id])
			->andWhere(['between', 'pr.date_created', $lastday, $today])
			->groupBy([ 'si.class_id', 'pr.payment_channel'])
			->orderBy('pr.payment_channel')
			->all(Yii::$app->db);

		$classGender= (new \yii\db\Query())
			->select(['class_id AS cl', 'gender AS g', 'count(id) as count'])
			->from('core_student si')
			->where(['class_id'=>$classIds])
			->groupBy(['class_id', 'gender'])
			->orderBy('class_id', 'gender')
			->all(Yii::$app->db);

		$schFem = (new \yii\db\Query())
			->select(['id'])
			->from('core_student')
			->where(['gender'=>'F', 'class_id'=>$classIds])
			->count('*', Yii::$app->db);

		$schMale = (new \yii\db\Query())
			->select(['id'])
			->from('core_student')
			->where(['gender'=>'M', 'class_id'=>$classIds])
			->count('*', Yii::$app->db);

		foreach($channels as $chn){
				$sums = $classDummy;
				foreach($classData as $data){
				if($data['chn']==$chn['id']){
					$sums[$data['cl']] = intval($data['sm']);
					}
				}
				$sums = array_values($sums);
				$classTrans[] = ['name'=>$chn['channel_name'], 'data'=>$sums, 'showInLegend'=>true,];
				$sums  = $classDummy;
			}
			$gen =[['id'=>'F', 'name'=>'Female'], ['id'=>'M', 'name'=>'Male']];
			foreach($gen as $g){
				$counts = $classDummy;
				foreach($classGender as $data){
				if($data['g']==$g['id']){
					$counts[$data['cl']] = intval($data['count']);
				}
			}
			$counts = array_values($counts);
			$classGen[] = ['name'=>$g['name'], 'data'=>$counts];
			$counts  = $classDummy;
			$classGen[] = [
							'type' => 'pie',
							'name' => 'Total Students',
							'data' => [['name'=>'Female', 'y'=>$schFem], ['name'=>'Male', 'y'=>$schMale]],
							'center' => [30, -25],
							'size' => 88,
							'showInLegend' => false,
							'dataLabels' => [
								'enabled' => false,
							],
						];

		}
		return ($request->isAjax) ? $this->renderAjax('class_graphs', [
			'period'=>$lastday.' - '.$today,
        	'schClasses'=>$schClasses,
        	'classTrans'=>$classTrans,
        	'classData'=>$classData,
        	'classGen'=>$classGen
        	]) :
			$this->render('class_graphs', [
				'period'=>$lastday.' - '.$today,
	        	'schClasses'=>$schClasses,
	        	'classTrans'=>$classTrans,
	        	'classData'=>$classData,
	        	'classGen'=>$classGen
	        	]);
		} else {
        throw new ForbiddenHttpException('User is not affiliated to any school.');
      }
    }


}

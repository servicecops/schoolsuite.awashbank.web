<?php

namespace app\modules\reports\controllers;


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudentSearch;
use DateInterval;
use DateTime;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\modules\timetable\models\TimetableScheduleMeeting;
//use app\modules\timetable\models\Timetable;
use app\modules\timetable\models\TimetableSchedule;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * Timetable Controller implements the reporting for timetable module
 */
class TimetableController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
    * Render the index page
    */
    public function actionIndex(){
        $request = Yii::$app->request;

        $res = ['default-report' => 'class-timetable-report'];

        return $request->isAjax ? $this->renderAjax('index', $res) : $this->render('index', $res);

    }

    /**
     * Generate the class / lecture timetable reports
     */
    public function actionClassTimeTableReport(){

        $request = Yii::$app->request;

        $model = new DynamicModel([
            'date_from','date_to', 'school_id', 'class_id', 'term_id',
            'timetable_type', 'student_group_id', 'teacher_id'
        ]);

        $model->addRule(['school_id', 'term_id', 'timetable_type'], 'required');

        $model->addRule(['school_id', 'class_id', 'timetable_type', 'student_group_id', 'teacher_id'], 'safe');

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $model->school_id = $sch;
        }

        //Default view options
        $pages = new Pagination(['totalCount' => 0]);
        $res = ['timetable_data'=>[], 'pages'=>$pages, 'model'=>$model];

        if(!$request->post()) {
            return $request->isAjax ? $this->renderAjax('class_timetable_report', $res) : $this->render('class_timetable_report', $res);
        }

        if(!$model->load($request->post()) || !$model->validate()){
            return $request->isAjax ? $this->renderAjax('class_timetable_report', $res) : $this->render('class_timetable_report', $res);
        }

        //$_SESSION['query'] = ['from'=>$model->date_from,'to'=>$model->date_to];

        unset($_SESSION['class_timetable_report']);

        $selectedClassId  = empty($model->student_group_id)? $model->class_id : $model->student_group_id;
        $query = new Query;

        $searchParams = array(
            'teacher_id' => $model->teacher_id,
            'class_id' => $selectedClassId,
            'school_id' => $model->school_id,
            'term_id' => $model->term_id,
            'timetable_type' => $model->timetable_type
        );


        $timetable_data = TimetableSchedule::generateTimeScheduleReportData($searchParams);

        $displayTimetable = $this->transformDynamicTimeTableDisplay($timetable_data);

        $res = ['timetable_data'=>$displayTimetable, 'pages'=>$pages, 'model'=>$model];
        //for download report via pdf purposes.
        $session = Yii::$app->session;
        $session->set('class_timetable_report', $res);

        return $request->isAjax ? $this->renderAjax('class_timetable_report', $res) : $this->render('class_timetable_report', $res);

    }


    /**
     * transform  the timetable schedules into display time format
     * @param array $timetableData
     * @return mixed
     */
    private function transformTimeTableDisplay($timetableData){
        $reformatedSchedules = array();
        // timePeriods.
        $lesson_time_8to9 = [];
        $lesson_time_9to10 = [];
        $lesson_time_10to11 = [];
        $lesson_time_11to12 = [];
        $lesson_time_12to13 = [];
        $lesson_time_13to14 = [];
        $lesson_time_14to15 = [];
        $lesson_time_15to16 = [];
        $lesson_time_16to17 = [];
        $lesson_time_17to18 = [];
        $lesson_time_18to19 = [];
        $lesson_time_19to20 = [];


        foreach($timetableData as $schedule){
            $startDateTime = strtotime($schedule['starts']);
            $endDateTime = strtotime($schedule['ends']);

            $scheduleStart = date('D', $startDateTime);
            $scheduleEnd  = date('D', $endDateTime);
            $scheduleDuration = $this->differenceInHours($startDateTime, $endDateTime);
            $startTime = date('H:i', $startDateTime); //H--> 24 hours while h -> 12 hours
            $endTime = date('H:i', $endDateTime);
            $startingLessonHour = date('H', $startDateTime);
            $endingLessonHour = date('H', $endDateTime);

            $timeSchedule = array(
                'id' =>  $schedule['id'],
                'title' => $schedule['title'],
                'timetable_id' => $schedule['timetable_id'],
                'selected_teacher' => $schedule['selected_teacher'],
                'starts' => $startDateTime,
                'ends' => $endDateTime,
                'startDay' => $scheduleStart,
                'endDay' => $scheduleEnd,
                'duration' => $scheduleDuration,
                'startTime' => $startTime,
                'endTime' => $endTime,
                'location' => $schedule['location']
            );


            //classify the schedules according to the time period.
            //should be 1 hour  for now ..
            if ( ($startingLessonHour >=8) &&  ($startingLessonHour <9)) {
                array_push($lesson_time_8to9, $timeSchedule);
            } else  if ( (($startingLessonHour >=9) &&  ($startingLessonHour <10) ) || (($endingLessonHour >=9 && $startingLessonHour == 8 ) )) {
                array_push($lesson_time_9to10, $timeSchedule);
            } else  if ( (($startingLessonHour >=10) &&  ($startingLessonHour <11) ) || (($endingLessonHour >=10 && $startingLessonHour == 9 )  )) {
                array_push($lesson_time_10to11, $timeSchedule);
            } else  if ( ($startingLessonHour >=11) &&  ($startingLessonHour <12) || (($endingLessonHour >=11 && $startingLessonHour == 10 )  )) {
                array_push($lesson_time_11to12, $timeSchedule);
            } else  if ( ($startingLessonHour >=12) &&  ($startingLessonHour <13) || (($endingLessonHour >=12 && $startingLessonHour == 11 )) ){
                array_push($lesson_time_12to13, $timeSchedule);
            } else  if ( ($startingLessonHour >=13) &&  ($startingLessonHour <14) || (($endingLessonHour >=13 && $startingLessonHour == 12 )) ){
                array_push($lesson_time_13to14, $timeSchedule);
            } else  if ( ($startingLessonHour >=14) &&  ($startingLessonHour <15)  || (($endingLessonHour >=14 && $startingLessonHour == 13 )) ) {
                array_push($lesson_time_14to15, $timeSchedule);
            } else  if ( ($startingLessonHour >=15) &&  ($startingLessonHour <16) || (($endingLessonHour >=15 && $startingLessonHour == 14 )) ) {
                array_push($lesson_time_15to16, $timeSchedule);
            } else  if ( ($startingLessonHour >=16) &&  ($startingLessonHour <17)  || (($endingLessonHour >=16 && $startingLessonHour == 15 ))) {
                array_push($lesson_time_16to17, $timeSchedule);
            } else  if ( ($startingLessonHour >=17) &&  ($startingLessonHour <18)   || (($endingLessonHour >=17 && $startingLessonHour == 16 ))) {
                array_push($lesson_time_17to18, $timeSchedule);
            } else  if ( ($startingLessonHour >=18) &&  ($startingLessonHour <19)  || (($endingLessonHour >=18 && $startingLessonHour == 17 ))) {
                array_push($lesson_time_18to19, $timeSchedule);
            } else  if ( ($startingLessonHour >=19) &&  ($startingLessonHour <20)  || (($endingLessonHour >=19 && $startingLessonHour == 20 ))) {
                array_push($lesson_time_19to20, $timeSchedule);
            }


        }


        $reformatedSchedules = array(
            '8:00 - 9:00' => $lesson_time_8to9,
            '9:00 - 10:00' => $lesson_time_9to10,
            '10:00 - 11:00' => $lesson_time_10to11,
            '11:00 - 12:00' => $lesson_time_11to12,
            '12:00 - 13:00' => $lesson_time_12to13,
            '13:00 - 14:00' => $lesson_time_13to14,
            '14:00 - 15:00' => $lesson_time_14to15,
            '15:00 - 16:00' => $lesson_time_15to16,
            '16:00 - 17:00' => $lesson_time_16to17,
            '17:00 - 18:00' => $lesson_time_17to18,
            '18:00 - 19:00' => $lesson_time_18to19,
            '19:00 - 20:00' => $lesson_time_19to20
        );

        return $reformatedSchedules;

    }

    /**
     * calculate the time difference in hours
     * @param string $startdate
     * @param string $enddate
     * @return float
     */
    private function differenceInHours($startdate,$enddate){
        $difference = abs($startdate - $enddate)/3600;
        return round($difference,2); //round to 2 dps
    }


    /**
     *  Send the class timetable report as pdf via email
     */
    public function actionEmailClassTimetableReport(){

    }

    /**
     * Generate the meeting minute reports
     */
    public function actionMeetingMinutesReport(){

        $request = Yii::$app->request;

        $model = new DynamicModel([
            'date_from','date_to','class_id', 'chairperson', 'secretary'
        ]);

        $model->addRule(['date_from', 'date_to' ], 'required');

        $model->addRule(['class_id', 'date_from','date_to', 'chairperson', 'secretary'], 'safe');

        //Default view options
        $pages = new Pagination(['totalCount' => 0]);
        $res = ['meeting_data'=>[], 'pages'=>$pages, 'model'=>$model];

        if(!$request->post()) {
            return $request->isAjax ? $this->renderAjax('meeting_minutes_report', $res) : $this->render('meeting_minutes_report', $res);
        }

        if(!$model->load($request->post()) || !$model->validate()){
            return $request->isAjax ? $this->renderAjax('meeting_minutes_report', $res) : $this->render('meeting_minutes_report', $res);
        }


        $_SESSION['query'] = ['from'=>$model->date_from,'to'=>$model->date_to];


        $query = new Query;


        if (!empty($model->chairperson)) {

            $query->select([
                'ts.id','ts.title','ts.timetable_schedule_id','ts.datetime', 'ts.chairperson','ts.secretary',
                'ts.absentee_list', 'ts.attendee_list','cl.class_code','tim.selected_class'
            ])
            ->from('timetable_schedule_meeting ts')
            ->innerJoin('timetable_schedule tim', 'tim.id=ts.timetable_schedule_id')
            ->innerJoin('core_school_class cl', 'cl.id=tim.selected_class')
            ->andWhere(['ts.chairperson' => $model->chairperson ])
            ->andFilterWhere(['tim.selected_class' => $model->class_id]);


        } else {

            $query->select([
                'ts.id','ts.title','ts.timetable_schedule_id','ts.datetime', 'ts.chairperson','ts.secretary',
                'ts.absentee_list', 'ts.attendee_list','cl.class_code','tim.selected_class'
            ])
            ->from('timetable_schedule_meeting ts')
            ->innerJoin('timetable_schedule tim', 'tim.id=ts.timetable_schedule_id')
            ->innerJoin('core_school_class cl', 'cl.id=tim.selected_class')
            //->andWhere(['ts.chairperson' => $model->chairperson ])
            ->andWhere(['tim.selected_class' => $model->class_id]);

        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('ts.id DESC');

        $meeting_data = $query->all(Yii::$app->db);

        unset($_SESSION['meeting_report_data']);

        $displayMeeting = $this->transformMeetingMinutesDisplay($meeting_data);

        $res = ['meeting_data'=>$displayMeeting, 'pages'=>$pages, 'model'=>$model];
        //for download report via pdf purposes.
        $session = Yii::$app->session;
        $session->set('meeting_report_data', $res);

        return $request->isAjax ? $this->renderAjax('meeting_minutes_report', $res) : $this->render('meeting_minutes_report', $res);

    }

    /**
     * Generate the Teacher Assignment Collision Detection reports
     */
    public function actionTeacherAssignmentCollisionDetectionReport(){

        $request = Yii::$app->request;

        $model = new DynamicModel([
            'date_from','date_to', 'school_id', 'class_id', 'term_id',
            'timetable_type', 'teacher_id','student_group_id', 'school_id'
        ]);

        $model->addRule(['school_id', 'term_id', 'timetable_type', 'teacher_id'], 'required');

        $model->addRule(['school_id', 'class_id', 'timetable_type', 'teacher_id'], 'safe');

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $model->school_id = $sch;
        }

        //Default view options
        $pages = new Pagination(['totalCount' => 0]);
        $res = ['timetable_data'=>[], 'pages'=>$pages, 'model'=>$model];

        if(!$request->post()) {
            return $request->isAjax ? $this->renderAjax('teacher_assignment_collision_report', $res) : $this->render('teacher_assignment_collision_report', $res);
        }

        if(!$model->load($request->post()) || !$model->validate()){
            return $request->isAjax ? $this->renderAjax('teacher_assignment_collision_report', $res) : $this->render('teacher_assignment_collision_report', $res);
        }

        $searchParams = array(
            'teacher_id' => $model->teacher_id,
            'class_id' =>  $model->class_id,
            'school_id' => $model->school_id,
            'term_id' => $model->term_id,
            'timetable_type' => $model->timetable_type
        );

        $timetable_data = TimetableSchedule::generateTimeScheduleDataForTeacherCollision($searchParams);

        $displayTimetable = $this->transformDynamicTimeTableDisplay($timetable_data);
        Yii::trace($displayTimetable);

        $res = ['timetable_data'=>$displayTimetable, 'pages'=>$pages, 'model'=>$model];
        //for exporting report via PDF
        $session = Yii::$app->session;
        $session->set('teacher_collision_report_data', $res);

        return $request->isAjax ? $this->renderAjax('teacher_assignment_collision_report', $res) : $this->render('teacher_assignment_collision_report', $res);

    }


     /**
     * transform  the meeting minutes data into meeting report display
     * @param array $meeting_data
     * @return mixed
     */
    private function transformMeetingMinutesDisplay($meeting_data){
        $meeting_report = array();

        foreach($meeting_data as $meeting){
            array_push($meeting_report, [
                'id' => $meeting['id'],
                'title' => $meeting['title'],
                'timetable_schedule' => TimetableSchedule::findOne($meeting['timetable_schedule_id'])->title,
                'datetime' => ($meeting['datetime'])? date('Y-m-d', strtotime($meeting['datetime'])) : '',
                'chairperson' => $meeting['chairperson'],
                'class_code' => $meeting['class_code'],
                'absentee_list' => $this->determineNumberOfItems($meeting['absentee_list']),
                'attendee_list' => $this->determineNumberOfItems($meeting['attendee_list'])
            ]);
        }

        return $meeting_report;
    }

    /**
     * Count the number of elements in array
     * for attendeeList and absenteeList
     * @param array $attendeeList
     * @return integer;
     */
    private function determineNumberOfItems($attendeeList){
        $listItems = json_decode($attendeeList);
        $splitList  = preg_split('/[\ \n\,]+/', $listItems);

        $cleanedMemberList = array();

        foreach($splitList as $member){
           array_push($cleanedMemberList, $member);
        }

        return count($cleanedMemberList);

    }

    /**
     * Generate the examination timetable reports
     */
    public function actionExaminationTimeTableReport(){

        $request = Yii::$app->request;

        $model = new DynamicModel([
            'date_from','date_to', 'school_id', 'class_id', 'term_id',
            'teacher_id', 'exam_id'
        ]);

        $model->addRule(['school_id', 'term_id', 'class_id'], 'required');

        $model->addRule(['school_id', 'class_id', 'teacher_id'], 'safe');

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $model->school_id = $sch;
        }

        //Default view options
        $pages = new Pagination(['totalCount' => 0]);
        $res = ['timetable_data'=>[], 'pages'=>$pages, 'model'=>$model];

        if(!$request->post()) {
            return $request->isAjax ? $this->renderAjax('exam_timetable_report', $res) : $this->render('exam_timetable_report', $res);
        }

        if(!$model->load($request->post()) || !$model->validate()){
            return $request->isAjax ? $this->renderAjax('exam_timetable_report', $res) : $this->render('exam_timetable_report', $res);
        }

        //$_SESSION['query'] = ['from'=>$model->date_from,'to'=>$model->date_to];

        unset($_SESSION['exam_timetable_report']);

        $selectedClassId  = empty($model->student_group_id)? $model->class_id : $model->student_group_id;
        $query = new Query;

        $searchParams = array(
            'teacher_id' => $model->teacher_id,
            'class_id' => $selectedClassId,
            'school_id' => $model->school_id,
            'term_id' => $model->term_id,
            //'timetable_type' => $model->timetable_type
            'timetable_type' => 'examination'
        );


        $timetable_data = TimetableSchedule::generateTimeScheduleReportData($searchParams);

        //figure out how to spread the time schedules
        $displayWeeks = $this->getExaminationWeekTimetableDisplay($timetable_data);
        Yii::trace("Examination weeks");
        Yii::trace($displayWeeks);

        $res = ['timetable_data'=>$displayWeeks, 'pages'=>$pages, 'model'=>$model];
        //for download report via pdf purposes.
        $session = Yii::$app->session;
        $session->set('exam_timetable_report', $res);

        return $request->isAjax ? $this->renderAjax('exam_timetable_report', $res) : $this->render('exam_timetable_report', $res);

    }

    /**
     * Get the week numbers for the  examination timetable
     */
    private function getExaminationWeekTimetableDisplay($timetableData){
       $reformatedSchedules = array();
       $weeks = array();

        foreach($timetableData as $schedule ){
            $startDateTime = strtotime($schedule['starts']);
            $selectedWeek = date("W", $startDateTime);
           //$selectedMonth = date("m", $startDateTime); //gives numerical month
            $selectedMonth = date("F", $startDateTime);
            array_push($weeks, [
               'week' => $selectedWeek,
               'month' => $selectedMonth
            ]);
        }

        $uniqueWeeks = array_unique(array_column($weeks, 'week'));
        $uniqueMonths = array_unique(array_column($weeks, 'month'));

        //filter the timetable schedules by weeks.
        foreach($uniqueWeeks as $week){

            $foundLesson = array_filter($timetableData, function($item) use ($week){ //$dayDate closure
                $selectedWeek = date("W", strtotime($item['starts']));
                return $selectedWeek == $week;
            });



            //$timeSchedules = $this->transformTimeTableDisplay($foundLesson);
            $timeSchedules = $this->transformDynamicTimeTableDisplay($foundLesson);
            array_push($reformatedSchedules, [
                'week' => $week,
                'schedules' => $timeSchedules
            ]);
        }
        Yii::info("exams timetable");
        Yii::info($reformatedSchedules);

        $months = array_values($uniqueMonths);
        $sortedMonths = TimetableSchedule::getSortedMonth($months);

        $result = array(
            'reformatedSchedules' =>  $reformatedSchedules,
            'months' => $sortedMonths
        );
        Yii::trace("final exam result");
        Yii::trace($result);

        return $result;

    }


    public function actionTimetableScheduleList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $schoolId = Yii::$app->user->identity->school_id;
            $query->select([
                    'ts.id, ts.title'
                ])
                ->from('timetable_schedule ts')
                ->innerJoin('core_school_class cl', 'cl.id=ts.selected_class')
                ->innerJoin('timetable tim', 'tim.id=ts.timetable_id')
                ->andWhere(['tim.calendar_category' => 'examination'])
                ->andFilterWhere(['ilike', 'ts.title', $q]);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $queryResult = array_values($data);
            $staffData = array();
            foreach ($queryResult as $value) {
                array_push($staffData, array(
                    'id' => $value['id'],
                    'text' => $value['title']
                ));
            }

            $out['results'] = $staffData;
        }

        return $out;
    }


    /**
     * transform  the timetable schedules into display time format
     * @param array $timetableData
     * @return mixed
     */
    private function transformDynamicTimeTableDisplay($timetableData){

        $schedules = array();

        foreach($timetableData as $schedule){
            //Yii::info($schedule);
            $startDateTime = strtotime($schedule['starts']);
            $endDateTime = strtotime($schedule['ends']);

            $scheduleStart = date('D', $startDateTime);
            $scheduleEnd  = date('D', $endDateTime);
            $scheduleDuration = $this->differenceInHours($startDateTime, $endDateTime);
            $startTime = date('H:i', $startDateTime); //H--> 24 hours while h -> 12 hours
            $endTime = date('H:i', $endDateTime);

            $timeSchedule = array(
                'id' =>  $schedule['id'],
                'title' => $schedule['title'],
                'timetable_id' => $schedule['timetable_id'],
                'selected_teacher' => $schedule['selected_teacher'],
                'starts' => $startDateTime,
                'ends' => $endDateTime,
                'startDay' => $scheduleStart,
                'endDay' => $scheduleEnd,
                'duration' => $scheduleDuration,
                'startTime' => $startTime,
                'endTime' => $endTime,
                'location' => $schedule['location'],
                'single_lesson_duration' => $schedule['average_schedule_duration']
            );

            array_push($schedules, $timeSchedule);

        }

        //checking the single lesson duration.
        // $countSingleLessonDuration = array_count_values(array_column($schedules, 'single_lesson_duration'));
        // Yii::trace("single lesson duration");
        // Yii::trace($countSingleLessonDuration); //average schedule duration

        //then filter them according to time period
        $startDateTime = '2021-10-29 8:00';
        $noOfLessons = 12;
        $singleLessonDuration = !empty($schedules[0]['single_lesson_duration']) ? $schedules[0]['single_lesson_duration'] : '+1 hour'; //by default for Examination
        $lessonTimePeriods = [];

        $lessonDivider = 1; //it must in hours
        if ($singleLessonDuration == '+1 hour') {
            $lessonDivider = 1; // 1/1 = 1
        } else if ($singleLessonDuration == '+40 minutes') {
            $lessonDivider = 0.666667; // 40 mins = 0.6667 hours
        }else if ($singleLessonDuration == '+30 minutes'){
            $lessonDivider = 0.5; // 30 minutes = 0.5 hours
        }else if ($singleLessonDuration == '+45 minutes') {
            $lessonDivider = 0.75; // 45 minutes = 0.75 hours
        }

        //1. create lesson hour time period array.
        for ($i=0; $i < $noOfLessons ; $i++) {
            $previousDateTime = $startDateTime;
            $startDateTime = date('Y-m-d H:i',strtotime($singleLessonDuration,strtotime($startDateTime)));
            $startTime = date("H:i",strtotime($startDateTime));
            $previousTime = date("H:i",strtotime($previousDateTime));

            //filter the lessons/schedules according to the lessontime.
            $foundLessons = array_filter($schedules,
                                function($item) use ($previousTime, $startTime) { //$dayDate closure
                                    $selectedLessonStart = date("H:i", $item['starts']);
                                    $selectedLessonEnd = date("H:i", $item['ends']);

                                return $selectedLessonStart == $previousTime;

                           });
            Yii::info("let see found lesson");
            Yii::info($foundLessons);
            //let deal with lessons with more than 1 hour schedule.
            foreach($foundLessons as $lessonItem){
                if (($lessonItem['duration']/$lessonDivider) == 1) {
                    // lessons within single lesson duration (1 hr )
                    $singleLesson = $previousTime . ' - ' . $startTime;
                    $lessonTimePeriods[$singleLesson][] = $lessonItem;

                } else if (($lessonItem['duration']/$lessonDivider) > 1) {
                    # code... lesson more than one single lesson duration..
                    $splitLessonItems = ($lessonItem['duration'])/$lessonDivider; //per hour
                    //create the different schedule items
                    $startSplitLesson = $previousDateTime;
                    $endSplitLesson = date('Y-m-d H:i',strtotime($singleLessonDuration,strtotime($startSplitLesson)));

                    for ($i=0; $i < $splitLessonItems; $i++) {

                        $startSplitTime =  date("H:i",strtotime($startSplitLesson));
                        $endSplitTime = date("H:i", strtotime($endSplitLesson));
                        $moreThanSingleLesson = $startSplitTime . ' - ' . $endSplitTime;

                        $lessonTimePeriods[$moreThanSingleLesson][] = $lessonItem;

                        $startSplitLesson = $endSplitLesson;
                        $endSplitLesson = date('Y-m-d H:i',strtotime($singleLessonDuration,strtotime($endSplitLesson)));

                    }

                } else if (($lessonItem['duration']/$lessonDivider) < 1) {

                    # code... less than one lesson duration (less than 1 hr or 40 minutes or 30 minutes )
                    $startLessThanLesson = $previousDateTime;
                    $lessThanRemainingMinutes = ( ($lessonItem['duration']/$lessonDivider) * 60 );
                    $endLessThanLesson = date('Y-m-d H:i',strtotime('+'. $lessThanRemainingMinutes ,strtotime($startLessThanLesson)));

                    $startLessTime =  date("H:i",strtotime($startLessThanLesson));
                    $endLessTime = date("H:i", strtotime($endLessThanLesson));
                    $lessThanSingleLesson = $startLessTime . ' - ' . $endLessTime;

                    $lessonTimePeriods[$lessThanSingleLesson][] = $lessonItem;
                    // Yii::info("less than single lesson");
                    // Yii::info($lessThanSingleLesson);
                    //u need to test this pliz..
                    // how shall i display it to the timetable display
                }

            }
        }

        return $lessonTimePeriods;

    }

    /**
     * Check if the timeBlock exists (8:00 - 9:00)
     * to avoid duplicate time blocks
     * @param array $lessonTimePeriods
     * @param string $timeBlock
     * @return mixed
     */
    private function checkExistingTimeBlock($lessonTimePeriods,$scheduleBlock){
        $existingTimeBlock = false;

        foreach($lessonTimePeriods as $timeBlock){
            foreach($timeBlock as $k => $v){
                // Yii::trace($k);
                // Yii::trace($v);
                if ($k == $scheduleBlock) {
                    $existingTimeBlock = true;
                    Yii::trace("time block already exists");
                }
            }
        }

        return $existingTimeBlock;
    }


}

<?php

namespace app\modules\reports\controllers;

use kartik\mpdf\Pdf;
use Yii;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\DynamicModel;
use app\modules\reports\models\GrowthStats;

/**
 * StudentsController implements the CRUD actions for Student model.
 */
class StudentsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionStuInfo()
    {
        if (!Yii::$app->user->can('view_student_report')) {
            throw new ForbiddenHttpException('No permissions to access this report');
        }
        $request = Yii::$app->request;
        $selected_list = $query = NULL;
        $student_data = array();
        $model2 = new DynamicModel(['school','bal_status', 'class_id', 'gender', 'date_from', 'date_to','student_campus']);
        if(Yii::$app->user->can('schoolpay_admin')){ //For admin context, school is required
            $model2->addRule(['school'], 'required');
            $model2->addRule(['school','bal_status', 'class_id', 'gender', 'date_from', 'date_to','student_campus','school_id'], 'safe');

        }

        if( !empty($_GET['s_info']))
        {
            if(!empty($_GET['DynamicModel']['school']))
            {
                $query .="s_info.school_id=".$_GET['DynamicModel']['school']. " AND ";
            }
            if(!empty($_GET['DynamicModel']['class_id']))
            {
                $query .="class_id=".$_GET['DynamicModel']['class_id']. " AND ";
            }
            if(!empty($_GET['DynamicModel']['gender']))
            {
                $query .="gender='".$_GET['DynamicModel']['gender']."' AND ";
            }
            if(!empty($_GET['DynamicModel']['date_from']))
            {
                $date_to = $_GET['DynamicModel']['date_to'] ? $_GET['DynamicModel']['date_to'] : date('Y-m-d');
                $query .="s_info.created_at BETWEEN '".$_GET['DynamicModel']['date_from']."' AND '".$date_to."' AND ";
            }
            if(!empty($_GET['DynamicModel']['bal_status']))
            {
                $bal_status = $_GET['DynamicModel']['bal_status'];
                if($bal_status == 'neg'){
                    $query .="outstanding_balance < 0 AND ";
                }else if($bal_status == 'pos'){
                    $query .="account_balance > 0 AND ";
                } else {
                    //Zero account balance
                    $query .="account_balance = 0 AND ";
                }

            }
            if(!empty($_GET['DynamicModel']['student_campus']))
            {
                $query .= " campus_id = ".$_GET['DynamicModel']['student_campus']." AND ";

            }

            $selected_list=$_GET['s_info'];

            $query1= new Query();
            $query1 ->select($selected_list)
                ->from('core_student s_info')
                ->join('join','student_account_gl s_acc','s_acc.id = s_info.student_account_id')
                ->join('join','core_school_class s_c','s_c.id = s_info.class_id')
                ->where($query.'s_info.active = true');
            if(\app\components\ToWords::isSchoolUser() ){
                $query1->andWhere(['s_c.school_id'=>Yii::$app->user->identity->school_id]);
            }
            $pages = new Pagination(['totalCount' => $query1->count('*', Yii::$app->db)]);
            $query1->offset($pages->offset);
            $query1->limit($pages->limit);

            $student_data = $query1->all(Yii::$app->db);
            if(empty($student_data)){
                \Yii::$app->getSession()->setFlash('studerror',"<i class='fa fa-exclamation-triangle'></i> <b> No Record For This Criteria.</b> ");
                return $this->redirect(['stu-info']);
            }
            return ($request->isAjax) ? $this->renderAjax('stu_info_report',[
                'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'pages' => $pages
            ]) :
                $this->render('stu_info_report',[
                    'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'pages' => $pages
                ]);
        }

        return ($request->isAjax) ? $this->renderAjax('stu_report', ['model2'=>$model2, 'selected_list'=>$selected_list, ]) : $this->render('stu_report', ['model2'=>$model2, 'selected_list'=>$selected_list, ]);
    }
    // student info report export to pdf and excel
    public function actionSelectedStudentList()
    {
        if(!empty($_REQUEST['studentlistexport']))
        {
            $query= $_SESSION['query'];
            $selected_list=$_SESSION['selected_list'];
            $query1= new Query();
            $query1 ->select($selected_list)
                    ->from('core_student s_info')
                    ->join('join','student_account_gl s_acc','s_acc.id = s_info.student_account_id')
                    ->join('join','core_school_class s_c','s_c.id = s_info.class_id')
                    ->where($query.'s_info.active = true');
            if(\app\components\ToWords::isSchoolUser() ){
                $query1->andWhere(['s_info.school_id'=>Yii::$app->user->identity->school_id]);
            }
            $query1->limit(200);

            $student_data = $query1->all(Yii::$app->db);

//            $html = $this->renderPartial('stu_info_report_pdf', array(
//                'student_data'=>$student_data,
//                'selected_list'=>$selected_list,
//            ));

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('stu_info_report_pdf',[
                        'student_data'=>$student_data,
                        'selected_list'=>$selected_list,                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print students transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);

            ob_clean();
            return $pdf->render();         }
        else if(!empty($_REQUEST['studentlistexcelexport']))
        {

            $query= $_SESSION['query'];
            $selected_list=$_SESSION['selected_list'];
            $query1= new Query();
            $query1 ->select($selected_list)
                    ->from('core_student s_info')
                    ->join('join','student_account_gl s_acc','s_acc.id = s_info.student_account_id')
                    ->join('join','core_school_class s_c','s_c.id = s_info.class_id')
                    ->where($query.'s_info.active = true');
            if(\app\components\ToWords::isSchoolUser() ){
                $query1->andWhere(['s_info.school_id'=>Yii::$app->user->identity->school_id]);
            }
            $student_data = $query1->all(Yii::$app->db);

            $file = $this->renderPartial('stu_info_report_excel', array(
                    'student_data'=>$student_data,
                    'selected_list'=>$selected_list,
                    ));
            $fileName = "Student_info_report".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }
        else
        {
            $query=$_SESSION['query'];
            $selected_list=null;
        }

    }

    public function actionGrowth(){
        $request = Yii::$app->request;
        if(Yii::$app->user->can('schoolpay_admin')){
            $res = GrowthStats::studentGrowth();
            return ($request->isAjax) ? $this->renderAjax('growth_graphs', $res) : $this->render('growth_graphs', $res);
        } else {
        throw new ForbiddenHttpException('User is not allowed to view this graph.');
      }

    }

    public function actionLists($id)
    {
        $countClasses = Classes::find()
                ->where(['institution' => $id])
                ->count('*', Yii::$app->db2);

        $classes = Classes::find()
                ->where(['institution' => $id])
                ->orderBy('class_code')
                ->all(Yii::$app->db2);

        if($countClasses > 0){
                echo "<option value=''> All Classes </option>";
            foreach($classes as $myClass){
                echo "<option value='".$myClass->id."'>".$myClass->class_description."</option>";
            }
        }
        else{
                echo "<option> -- </option>";
            }
    }

    public function actionAttendance()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('Only School users are allowed to view attendance summary');
        }


        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        //
        $sumPr = $sumAb = $sumgeAb= $sumgePr=$sumboAb= $sumboPr=0;

        $tepresent = Yii::$app->db->createCommand('select cs.id ,sb.subject_code,cs.class_code, count(DISTINCT(sa.student_id)) as presents , sa.school_id from student_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tepresent);

        if($tepresent){
            foreach ($tepresent as $data){
                Yii::trace($data['id']);
                $sumPr= $sumPr + $data['presents'];
                Yii::trace($sumPr);

            }
        }



        $teabsent = Yii::$app->db->createCommand('select cs.id ,cs.class_code,sb.subject_code, count(DISTINCT(sa.student_id)) as absents , sa.school_id from student_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = false and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($teabsent);

        if($teabsent){
            foreach ($teabsent as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumAb= $sumAb + $data['absents'];
                Yii::trace($sumAb);

            }
        }



        $geabsent = Yii::$app->db->createCommand('select cs.id ,cs.class_code,sb.subject_code, count(DISTINCT(sa.student_id)) as absents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = false and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','F')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($geabsent);

        if($geabsent){
            foreach ($geabsent as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumgeAb= $sumgeAb + $data['absents'];
                Yii::trace($sumgeAb);

            }
        }

        $gepres = Yii::$app->db->createCommand('select cs.id ,sb.subject_code,cs.class_code, count(DISTINCT(sa.student_id)) as presents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','F')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($gepres);

        if($gepres){
            foreach ($gepres as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumgePr= $sumgePr + $data['presents'];
                Yii::trace($sumgePr);

            }
        }


        $boabsent = Yii::$app->db->createCommand('select cs.id ,sb.subject_code,cs.class_code, count(DISTINCT(sa.student_id)) as absents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = false and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','M')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($boabsent);

        if($boabsent){
            foreach ($boabsent as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumboAb= $sumboAb + $data['absents'];
                Yii::trace($sumboAb);

            }
        }

        $bopres = Yii::$app->db->createCommand('select cs.id ,cs.class_code,sb.subject_code, count(DISTINCT(sa.student_id)) as presents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','M')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($bopres);

        if($bopres){
            foreach ($bopres as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumboPr= $sumboPr + $data['presents'];
                Yii::trace($sumboPr);

            }
        }



        $res= ['sumPr'=>$sumPr,
            'sumgeAb'=>$sumgeAb,
            'sumgePr'=>$sumgePr,
            'sumboAb'=>$sumboAb,
            'sumboPr'=>$sumboPr,
            'sumAb'=>$sumAb,
            'teabsent' => $teabsent,
            'tepresent' => $tepresent,
            'gepres' => $gepres,
            'geabsent' => $geabsent,
            'bopres' => $bopres,
            'boabsent' => $boabsent,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('attendance_report', $res) : $this->render('attendance_report', $res);



    }

    public function actionAttendanceExport()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
       /* $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];*/
        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        //
        $sumPr = $sumAb = $sumgeAb= $sumgePr=$sumboAb= $sumboPr=0;

        $tepresent = Yii::$app->db->createCommand('select cs.id ,sb.subject_code,cs.class_code, count(DISTINCT(sa.student_id)) as presents , sa.school_id from student_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tepresent);

        if($tepresent){
            foreach ($tepresent as $data){
                Yii::trace($data['id']);
                $sumPr= $sumPr + $data['presents'];
                Yii::trace($sumPr);

            }
        }



        $teabsent = Yii::$app->db->createCommand('select cs.id ,cs.class_code,sb.subject_code, count(DISTINCT(sa.student_id)) as absents , sa.school_id from student_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = false and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($teabsent);

        if($teabsent){
            foreach ($teabsent as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumAb= $sumAb + $data['absents'];
                Yii::trace($sumAb);

            }
        }



        $geabsent = Yii::$app->db->createCommand('select cs.id ,cs.class_code,sb.subject_code, count(DISTINCT(sa.student_id)) as absents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = false and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','F')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($geabsent);

        if($geabsent){
            foreach ($geabsent as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumgeAb= $sumgeAb + $data['absents'];
                Yii::trace($sumgeAb);

            }
        }

        $gepres = Yii::$app->db->createCommand('select cs.id ,sb.subject_code,cs.class_code, count(DISTINCT(sa.student_id)) as presents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','F')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($gepres);

        if($gepres){
            foreach ($gepres as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumgePr= $sumgePr + $data['presents'];
                Yii::trace($sumgePr);

            }
        }


        $boabsent = Yii::$app->db->createCommand('select cs.id ,sb.subject_code,cs.class_code, count(DISTINCT(sa.student_id)) as absents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = false and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','M')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($boabsent);

        if($boabsent){
            foreach ($boabsent as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumboAb= $sumboAb + $data['absents'];
                Yii::trace($sumboAb);

            }
        }

        $bopres = Yii::$app->db->createCommand('select cs.id ,cs.class_code,sb.subject_code, count(DISTINCT(sa.student_id)) as presents , sa.school_id from student_attendance sa 
INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER join core_student si on si.id = sa.student_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.student_attended = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and si.gender =:gender
GROUP BY cs.id , cs.class_code,sa.school_id,sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':gender','M')
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($bopres);

        if($bopres){
            foreach ($bopres as $data){
                Yii::trace($data['id']);
                Yii::trace($data);
                $sumboPr= $sumboPr + $data['presents'];
                Yii::trace($sumboPr);

            }
        }



        $res= ['sumPr'=>$sumPr,
            'sumgeAb'=>$sumgeAb,
            'sumgePr'=>$sumgePr,
            'sumboAb'=>$sumboAb,
            'sumboPr'=>$sumboPr,
            'sumAb'=>$sumAb,
            'teabsent' => $teabsent,
            'tepresent' => $tepresent,
            'gepres' => $gepres,
            'geabsent' => $geabsent,
            'bopres' => $bopres,
            'boabsent' => $boabsent,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];



        $search_time = ['to'=>$to_date, 'from'=>$from_date];

        if(!empty($_REQUEST['studentsubjectsummaryexport']))
        {
//            $html = $this->renderPartial('channel_report_pdf',[
//                        'channel_data'=>$channel_data,'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
//                ]);

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('student_subject_attendance_report_pdf',['sumPr'=>$sumPr,
                        'sumgeAb'=>$sumgeAb,
                        'sumgePr'=>$sumgePr,
                        'sumboAb'=>$sumboAb,
                        'sumboPr'=>$sumboPr,
                        'sumAb'=>$sumAb,
                        'teabsent' => $teabsent,
                        'tepresent' => $tepresent,
                        'gepres' => $gepres,
                        'geabsent' => $geabsent,
                        'bopres' => $bopres,
                        'boabsent' => $boabsent,
                        'to_date'=>$to_date,
                        'from_date'=>$from_date,
                        'query'=>$search_time,
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'student_subject_attendance_summary',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student Subject Attendance Summary||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);

            ob_clean();
//            return Yii::$app->pdf->exportData('Channel Transactions summary','summery_report',$html);
            return $pdf->render();
        }
        elseif(!empty($_REQUEST['studentsubjectsummaryexcelexport']))
        {
            $file = $this->renderPartial('student_subject_attendance_report_excel',[
                'sumPr'=>$sumPr,
                'sumgeAb'=>$sumgeAb,
                'sumgePr'=>$sumgePr,
                'sumboAb'=>$sumboAb,
                'sumboPr'=>$sumboPr,
                'sumAb'=>$sumAb,
                'teabsent' => $teabsent,
                'tepresent' => $tepresent,
                'gepres' => $gepres,
                'geabsent' => $geabsent,
                'bopres' => $bopres,
                'boabsent' => $boabsent,
                'to_date'=>$to_date,
                'from_date'=>$from_date,
                'query'=>$search_time
            ]);
            $fileName = "student_subject_attendance_summary_excel".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }
    }

    public function actionTrsubAttendance()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('Only School users are allowed to view attendance summary');
        }


        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        date_default_timezone_set('Africa/Kampala');

        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        Yii::trace($from_date);
        Yii::trace($to_date);
        Yii::trace($end_date);
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        //
        $sumPr = $sumAb = $sumfPr= $summPr=$sumboAb= $sumboPr=0;

        $tepresent = Yii::$app->db->createCommand('select DISTINCT(cs.id) ,cs.class_code, sb.subject_code,count(DISTINCT(sa.teacher_id)) as presents , sa.school_id from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.present = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id
GROUP BY cs.id , cs.class_code,sa.school_id, sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tepresent);

        if($tepresent){
            foreach ($tepresent as $data){
                Yii::trace($data['id']);
                $sumPr= $sumPr + $data['presents'];
                Yii::trace($sumPr);

            }
        }



        $tfpresent = Yii::$app->db->createCommand('select DISTINCT(cs.id) ,cs.class_code, sb.subject_code,count(DISTINCT(sa.teacher_id)) as presents , sa.school_id from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id
INNER JOIN "user" uz on uz.id = sa.teacher_id

where sa.present = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and uz.gender =\'F\'
GROUP BY cs.id , cs.class_code,sa.school_id, sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tfpresent);

        if($tfpresent){
            foreach ($tfpresent as $data){
                Yii::trace($data['id']);
                $sumfPr= $sumfPr + $data['presents'];
                Yii::trace($sumfPr);

            }
        }

        $tmpresent = Yii::$app->db->createCommand('select DISTINCT(cs.id) ,cs.class_code, sb.subject_code,count(DISTINCT(sa.teacher_id)) as presents , sa.school_id from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id
INNER JOIN "user" uz on uz.id = sa.teacher_id

where sa.present = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and uz.gender =\'M\'
GROUP BY cs.id , cs.class_code,sa.school_id, sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tmpresent);

        if($tmpresent){
            foreach ($tmpresent as $data){
                Yii::trace($data['id']);
                $summPr= $summPr + $data['presents'];
                Yii::trace($summPr);

            }
        }



        $res= ['sumPr'=>$sumPr,
            'summPr'=>$summPr,
            'sumfPr'=>$sumfPr,
            'sumboAb'=>$sumboAb,
            'sumboPr'=>$sumboPr,
            'sumAb'=>$sumAb,
            'tepresent' => $tepresent,
            'tmpresent' => $tmpresent,

            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('teacher_attendance_report', $res) : $this->render('teacher_attendance_report', $res);



    }

    public function actionTrsubAttendanceExport()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        date_default_timezone_set('Africa/Kampala');

        /*$from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        Yii::trace($from_date);
        Yii::trace($to_date);
        Yii::trace($end_date);
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];*/
        //

        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $sumPr = $sumAb = $sumfPr= $summPr=$sumboAb= $sumboPr=0;

        $tepresent = Yii::$app->db->createCommand('select DISTINCT(cs.id) ,cs.class_code, sb.subject_code,count(DISTINCT(sa.teacher_id)) as presents , sa.school_id from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id

where sa.present = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id
GROUP BY cs.id , cs.class_code,sa.school_id, sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tepresent);

        if($tepresent){
            foreach ($tepresent as $data){
                Yii::trace($data['id']);
                $sumPr= $sumPr + $data['presents'];
                Yii::trace($sumPr);

            }
        }



        $tfpresent = Yii::$app->db->createCommand('select DISTINCT(cs.id) ,cs.class_code, sb.subject_code,count(DISTINCT(sa.teacher_id)) as presents , sa.school_id from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id
INNER JOIN "user" uz on uz.id = sa.teacher_id

where sa.present = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and uz.gender =\'F\'
GROUP BY cs.id , cs.class_code,sa.school_id, sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tfpresent);

        if($tfpresent){
            foreach ($tfpresent as $data){
                Yii::trace($data['id']);
                $sumfPr= $sumfPr + $data['presents'];
                Yii::trace($sumfPr);

            }
        }

        $tmpresent = Yii::$app->db->createCommand('select DISTINCT(cs.id) ,cs.class_code, sb.subject_code,count(DISTINCT(sa.teacher_id)) as presents , sa.school_id from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
INNER JOIN core_subject sb on sb.id = sa.subject_id
INNER JOIN "user" uz on uz.id = sa.teacher_id

where sa.present = true and sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id and uz.gender =\'M\'
GROUP BY cs.id , cs.class_code,sa.school_id, sb.subject_code' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($tmpresent);

        if($tmpresent){
            foreach ($tmpresent as $data){
                Yii::trace($data['id']);
                $summPr= $summPr + $data['presents'];
                Yii::trace($summPr);

            }
        }


        $search_time = ['to'=>$to_date, 'from'=>$from_date];

        if(!empty($_REQUEST['trsubjectexport']))
        {
//            $html = $this->renderPartial('channel_report_pdf',[
//                        'channel_data'=>$channel_data,'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
//                ]);

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('teacher_attendance_report_pdf',[
                        'sumPr'=>$sumPr,
                        'summPr'=>$summPr,
                        'sumfPr'=>$sumfPr,
                        'sumboAb'=>$sumboAb,
                        'sumboPr'=>$sumboPr,
                        'sumAb'=>$sumAb,
                        'tepresent' => $tepresent,
                        'tmpresent' => $tmpresent,
                        'to_date'=>$to_date,
                        'from_date'=>$from_date,
                        'query'=>$search_time,
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print channel transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);

            ob_clean();
//            return Yii::$app->pdf->exportData('Channel Transactions summary','summery_report',$html);
            return $pdf->render();
        }
        elseif(!empty($_REQUEST['trsubjectexcelexport']))
        {
            $file = $this->renderPartial('teacher_attendance_report_excel',[
                'sumPr'=>$sumPr,
                'summPr'=>$summPr,
                'sumfPr'=>$sumfPr,
                'sumboAb'=>$sumboAb,
                'sumboPr'=>$sumboPr,
                'sumAb'=>$sumAb,
                'tepresent' => $tepresent,
                'tmpresent' => $tmpresent,

                'to_date'=>$to_date,
                'from_date'=>$from_date,
                'query'=>$search_time
            ]);
            $fileName = "teacher_attendance_report_excel".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }



    }

    public function actionTrSummary(){

        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('Only School users are allowed to view attendance summary');
        }


        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $bopres = Yii::$app->db->createCommand('select cs.id ,cs.class_code, uz.firstname ,uz.lastname,sa.ip_address, sb.subject_code, sa.date_created,sa.present
from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
inner join "user" uz on uz.id = sa.teacher_id
INNER JOIN core_subject sb on sb.id = sa.subject_id
where sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
        Yii::trace($bopres);


        $res= [
            'bopres' => $bopres,
            'to_date'=>$to_date,
            'from_date'=>$from_date
        ];

        return $request->isAjax ? $this->renderAjax('tr_summary', $res) : $this->render('tr_summary', $res);

    }

    public function actionTrSummaryList(){

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }

        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        /*$from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-1days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];*/
        $query = Yii::$app->db->createCommand('select cs.id ,cs.class_code, uz.firstname ,uz.lastname,sa.ip_address, sb.subject_code, sa.date_created,sa.present
from teacher_subject_attendance sa INNER JOIN core_school_class cs on cs.id = sa.class_id
inner join "user" uz on uz.id = sa.teacher_id
INNER JOIN core_subject sb on sb.id = sa.subject_id
where sa.date_created >= :from_date and sa.date_created < :end_date and sa.school_id =:school_id' )
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':school_id',Yii::$app->user->identity->school_id)
            ->queryAll();
//        Yii::trace($bopres);


        $search_time = ['to'=>$to_date, 'from'=>$from_date];

        if(!empty($_REQUEST['trsubjectexport']))
        {
//            $html = $this->renderPartial('channel_report_pdf',[
//                        'channel_data'=>$channel_data,'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
//                ]);

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('tr_subject_attendance_report_pdf',['tr_subject_data'=>$query,
                        'query'=>$search_time,
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print channel transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);

            ob_clean();
//            return Yii::$app->pdf->exportData('Channel Transactions summary','summery_report',$html);
            return $pdf->render();
        }
        elseif(!empty($_REQUEST['trsubjectexcelexport']))
        {
            $file = $this->renderPartial('tr_subject_attendance_report_excel',[
                'tr_subject_data'=>$query, 'query'=>$search_time
            ]);
            $fileName = "tr_subject_attendance_report_excel".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }

    }


}

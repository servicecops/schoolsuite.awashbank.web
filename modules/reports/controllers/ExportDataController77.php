<?php

namespace app\modules\reports\controllers;


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudentSearch;
use DateInterval;
use DateTime;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\modules\timetable\models\TimetableScheduleMeeting;
//use app\modules\timetable\models\Timetable;
use app\modules\timetable\models\TimetableSchedule;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * ExportData Controller implements the exporting for the reports module
 */
class ExportDataController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /***
     * Generate the pdf for the class timetable report
     */
    public function actionExportClassTimetableReport($id){
          
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $session = Yii::$app->session;
        
        $schoolDetail = $this->findSchoolDetails($id);
        Yii::trace($schoolDetail);

        $res = array(
            'schoolDetail' => $schoolDetail,
            'data' => $session->get('class_timetable_report')
        );

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('export_class_timetable_report', $res),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Term II class timetable for P.6',
                'SetSubject' => 'Class/Lecture timetable report ',
                'SetHeader' => [' Class Timetable report ||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }


    /**
     * Retrieve the school details
     * @param integer $schoolId
     * @return mixed
     */
    private function findSchoolDetails($schoolId){
        $query = (new Query())->select(['si.id', 'si.school_name', 'si.location',
                      'si.phone_contact_1', 'si.phone_contact_2', 'si.contact_email_1', 'si.school_code', 
                      'si.village', 'si.school_logo','ref.description', 'dist.district_name' ])
                  ->from('core_school si')
                  ->innerJoin('ref_region ref', 'ref.id=si.region')
                  ->innerJoin('district dist', 'dist.id=si.district')
                  ->where(['si.id'=>$schoolId])
                  ->limit(1)
                  ->one(Yii::$app->db); //Use the backup db
                  //->all();
        return $query ? $query : null;
    }
    

    /**
     * Generate the pdf for the meeting minutes report
     * @param integer $id
     * @return mixed
     */
    public function actionExportMeetingMinutesReportAsPdf($id){
          
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $session = Yii::$app->session;
        
        $schoolDetail = $this->findSchoolDetails($id);

        $res = array(
            'schoolDetail' => $schoolDetail,
            'data' => $session->get('meeting_report_data')
        );

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('export_meeting_minutes_report', $res),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Meeting Minutes Report',
                'SetSubject' => 'Meeting Minutes report ',
                'SetHeader' => [' Meeting minutes report ||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Meeting minutes, schoolsuite , Export, PDF, MPDF, Output, Privacy, Policy',
            ]
        ]);
        return $pdf->render();
    }

    /**
     * Generate the execl report for the meeting minutes report
     * @return mixed
     */
    public function actionExportMeetingMinutesReportAsExcel(){

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $session = Yii::$app->session;
        

        $selected_list = ['Title', 'Timetable Schedule', 'Meeting Time', 'Chaired By', 'Class','No. of Attendee', 'No. of Absentee'];
        $meetingInfo = $session->get('meeting_report_data');


        $file = $this->renderPartial('meeting_minutes_excel_report', array(
                   'meeting_data'=>$meetingInfo['meeting_data'],
                   'selected_list'=>$selected_list,
               ));
        
        $fileName = "Meeting_minute_report_".date('YmdHi').'.xls';
        $options = ['mimeType'=>'application/vnd.ms-excel'];

        return Yii::$app->excel->exportExcel($file, $fileName, $options);
    }
    

    /**
     * Generate the pdf for the teacher assignment collision detection  report
     * @param integer $id
     * @return mixed
     */
    public function actionExportTeacherCollisionDetectionReportAsPdf($id){
          
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $session = Yii::$app->session;
        
        $schoolDetail = $this->findSchoolDetails($id);

        $res = array(
            'schoolDetail' => $schoolDetail,
            'data' => $session->get('teacher_collision_report_data')
        );

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('teacher_assignment_collision_report_pdf', $res),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Teacher Assignment Collision Detection Report',
                'SetSubject' => 'Teacher Assignment Collision Detection Report ',
                'SetHeader' => [' Teacher Assignment Collision Report ||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Teacher Assignment, schoolsuite , Export, PDF, MPDF, Output, Privacy, Policy',
            ]
        ]);
        return $pdf->render();
    }


    /**
     * Generate the pdf for the examination timetable report
     * @param integer $id
     * @return  mixed
     */
    public function actionExportExaminationTimetableReport($id){
          
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $session = Yii::$app->session;
        
        $schoolDetail = $this->findSchoolDetails($id);

        $res = array(
            'schoolDetail' => $schoolDetail,
            'data' => $session->get('exam_timetable_report')
        );

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('examination_timetable_report_pdf', $res),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Examination Timetable Report',
                'SetSubject' => 'Examination Timetable Report ',
                'SetHeader' => [' Examination Timetable report ||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }





}
<?php

namespace app\modules\reports\controllers;


use app\modules\paymentscore\models\SchoolAccountTransactionHistory;
use app\modules\reports\models\Reports;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudentSearch;
use DateInterval;
use DateTime;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * StudentsController implements the CRUD actions for Student model.
 */
class TransactionsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }



    public function actionTrans()
    {
        if (!Yii::$app->user->can('view_transaction_report')) {
            throw new ForbiddenHttpException('No permissions to access this report');
        }
        $request = Yii::$app->request;
        $query = NULL;
        $selected_list = array('sach.balance_before as student_balance_before', 'sach.balance_after as student_balance_after');
        $tr_list = $st_list = $sc_list = $pr_list = array();
        $student_data = array();
        $model = new SchoolAccountTransactionHistory();
        $stu = new CoreStudentSearch(['scenario'=>'search']);


        if( !empty($_GET['s_trans']) )
        {
            // if (Yii::$app->request->isAjax) {
            // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            // return ActiveForm::validateMultiple([$model]);
            // }
            if(!empty($_GET['Trans']['date_from']) && !empty($_GET['Trans']['date_to']))
            {
                $query .="tr.date_created >= '".$_GET['Trans']['date_from']. "' AND ";
            }
            if(!empty($_GET['Trans']['date_to']))
            {
                $plusOneDay = DateTime::createFromFormat('Y-m-d', $_GET['Trans']['date_to'])
                    ->add(new DateInterval('P1D'))
                    ->format('Y-m-d');
                //Add one day to end date and use less than
                $query .="tr.date_created < '".$plusOneDay."' AND ";
            }
            if(!empty($_GET['Trans']['school']))
            {
                $query .="tr.account_id=".$_GET['Trans']['school']." AND ";
            }
            if(!empty($_GET['Trans']['channel']))
            {
                $query .="pr.payment_channel=".$_GET['Trans']['channel']." AND ";
            }if(!empty($_GET['Student']['campus_id']))
        {
            $query .="si.campus_id=".$_GET['Student']['campus_id']." AND ";
        }
            if(!empty($_GET['Trans']['class']))
            {
                $query .="si.class_id=".$_GET['Trans']['class'];
            }

            if(!empty($_GET['CoreStudentSearch']['campus_id']))
            {
                $query .="si.campus_id=".$_GET['CoreStudentSearch']['campus_id'];
            }

            if(!empty($_GET['s_trans']))
            {
                foreach($_GET['s_trans'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('tr.', '', $trans);
                    $st_list[] = $trm;
                }
            }
            if(!empty($_GET['sinfo']))
            {
                foreach($_GET['sinfo'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('si.', '', $trans);
                    $st_list[] = $trm;

                }
            }
            if(!empty($_GET['pch']))
            {
                foreach($_GET['pch'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('pr.', '', $trans);
                    $pr_list[] = $trm;

                }
            }
            if(!empty($_GET['sclas']))
            {
                foreach($_GET['sclas'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('ls.', '', $trans);
                    $sc_list[] = $trm;

                }
            }


            $query = rtrim($query, ' AND ');
            echo $model->validate();
            $query1= new \yii\db\Query();
            $query1 ->select($selected_list)
                ->from('school_account_transaction_history tr')
                ->join('join','payments_received pr','pr.id = tr.payment_id')
                ->join('join','core_student si','si.id = pr.student_id')
                ->join('join','student_account_transaction_history sach','sach.payment_id = pr.id')
                ->join('join', 'core_school_class ls', 'ls.id = si.class_id')
                ->join('join', 'core_school sch', 'sch.id = si.school_id')
                ->where($query);


            if(Yii::$app->user->can('own_sch') ){
                $query1->andWhere(['pr.school_id'=>Yii::$app->user->identity->school_id]);
            }
            if(Yii::$app->user->can('view_by_region') ){
                $query1->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);
            }



            http://localhost/awash/reports/transactions/summery
            $pages = new Pagination(['totalCount' => $query1->count('*', Yii::$app->db2)]);
            $query1->offset($pages->offset)
                ->limit($pages->limit)
                ->orderBy('tr.date_created DESC');

            $student_data = $query1->all(Yii::$app->db2);
            if(empty($student_data)){
                \Yii::$app->getSession()->setFlash('transerror',"<i class='fa fa-exclamation-triangle'></i> <b> No Record For This Criteria.</b> ");
                return $this->redirect(['trans']);
            }

            $res = ['student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'pages' => $pages, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list];
            return $request->isAjax ? $this->renderAjax('trans_info_report', $res) : $this->render('trans_info_report', $res);

        }

        $res = ['model'=>$model, 'stu'=>$stu,'query'=>$query,'selected_list'=>$selected_list  ];
        return $request->isAjax ? $this->renderAjax('trans_report', $res) : $this->render('trans_report', $res);

    }



    public function actionSupplementaryTrans()
    {
        if (!Yii::$app->user->can('view_transaction_report')) {
            throw new ForbiddenHttpException('No permissions to access this report');
        }
        $request = Yii::$app->request;
        $query = NULL;
        $selected_list = array();
        $tr_list = $st_list = $sc_list = $pr_list = array();
        $student_data = array();
        $model = new SchoolAccountTransactionHistory();
        $stu = new CoreStudentSearch(['scenario'=>'search']);


        if( !empty($_GET['s_trans']) )
        {
            Yii::trace("== beg selectd items");
            Yii::trace($_GET['s_trans']);
            // if (Yii::$app->request->isAjax) {
            // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            // return ActiveForm::validateMultiple([$model]);
            // }
            if(!empty($_GET['Trans']['date_from']) && !empty($_GET['Trans']['date_to']))
            {
                $query .="pr.date_created >= '".$_GET['Trans']['date_from']. "' AND ";
            }
            if(!empty($_GET['Trans']['date_to']))
            {
                $plusOneDay = DateTime::createFromFormat('Y-m-d', $_GET['Trans']['date_to'])
                    ->add(new DateInterval('P1D'))
                    ->format('Y-m-d');
                //Add one day to end date and use less than
                $query .="pr.date_created < '".$plusOneDay."' AND ";
            }
            if(!empty($_GET['Trans']['school']))
            {
                $query .="pr.school_id=".$_GET['Trans']['school']." AND ";
            }
            if(!empty($_GET['Trans']['channel']))
            {
                $query .="pr.payment_channel=".$_GET['Trans']['channel']." AND ";
            }if(!empty($_GET['Student']['campus_id']))
        {
            $query .="si.campus_id=".$_GET['Student']['campus_id']." AND ";
        }
            if(!empty($_GET['Trans']['class']))
            {
                $query .="si.class_id=".$_GET['Trans']['class'];
            }

            if(!empty($_GET['CoreStudentSearch']['campus_id']))
            {
                $query .="si.campus_id=".$_GET['CoreStudentSearch']['campus_id'];
            }

            if(!empty($_GET['s_trans']))
            {


                foreach($_GET['s_trans'] as $trans){
                    $selected_list[] = $trans;

                    $trm = str_replace('pr.', '', $trans);
                    $st_list[] = $trm;

                }
            }
            if(!empty($_GET['sinfo']))
            {
                foreach($_GET['sinfo'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('si.', '', $trans);
                    $st_list[] = $trm;

                }
            }
            if(!empty($_GET['pch']))
            {
                foreach($_GET['pch'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('pr.', '', $trans);
                    $pr_list[] = $trm;

                }
            }
            if(!empty($_GET['sclas']))
            {
                foreach($_GET['sclas'] as $trans){
                    $selected_list[] = $trans;
                    $trm = str_replace('ls.', '', $trans);
                    $sc_list[] = $trm;

                }
            }

            Yii::trace("==selectd items 2");
            Yii::trace($selected_list);

            $query = rtrim($query, ' AND ');
            echo $model->validate();
            $query1= new \yii\db\Query();
            $query1 ->select($selected_list)
                ->from('supplementary_fee_payments_received pr')
                ->join('join','core_student si','si.id = pr.student_id')
                ->join('join','student_account_transaction_history sach','sach.payment_id = pr.id')
                ->join('join', 'core_school_class ls', 'ls.id = si.class_id')
                ->join('join', 'core_school sch', 'sch.id = si.school_id')
                ->where($query);


            if(Yii::$app->user->can('own_sch') ){
                $query1->andWhere(['pr.school_id'=>Yii::$app->user->identity->school_id]);
            }
            if(Yii::$app->user->can('view_by_region') ){
                $query1->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);
            }



            http://localhost/awash/reports/transactions/summery
            $pages = new Pagination(['totalCount' => $query1->count('*', Yii::$app->db2)]);
            $query1->offset($pages->offset)
                ->limit($pages->limit)
                ->orderBy('pr.date_created DESC');

            $student_data = $query1->all(Yii::$app->db2);
            if(empty($student_data)){
                \Yii::$app->getSession()->setFlash('transerror',"<i class='fa fa-exclamation-triangle'></i> <b> No Record For This Criteria.</b> ");
                return $this->redirect(['trans']);
            }

            $res = ['student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'pages' => $pages, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list];
            return $request->isAjax ? $this->renderAjax('supplementary_trans_info_report', $res) : $this->render('supplementary_trans_info_report', $res);

        }

        $res = ['model'=>$model, 'stu'=>$stu,'query'=>$query,'selected_list'=>$selected_list  ];
        return $request->isAjax ? $this->renderAjax('supplementary_trans_report', $res) : $this->render('supplementary_trans_report', $res);

    }



    public function actionSummery()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
          session_start();
        }

        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];

//        $reportQuery = "si.school_code AS sc, si.school_name AS sn, sum(amount) AS sum, count(amount) AS count, nb.bank_name AS bn, si.bank_account_number AS ban ".
//                        " payments_received pr ".
//                        " inner join school_information si on si.id = pr.school_id ".
//                        " inner join nominated_bank_details nb on si.bank_name = nb.id " .
//                        " where pr.id > (select min(id) from payments_received where date_created>:from_date) ".
//                        " and pr.reversed=false and pr.reversal=false " .
//                        " and pr.date_created betweeen :from_date and :end_date ";




        $query= new \yii\db\Query();
        $query ->select(['si.school_code AS sc', 'si.school_name AS sn','br.branch_name as br','abr.bank_region_name AS brn', 'sum(amount) AS sum', 'count(amount) AS count', 'nb.bank_name AS bn', 'si.bank_account_number AS ban'])
                ->from('payments_received pr')
                ->innerJoin('core_school si','si.id = pr.school_id')
                ->innerJoin('core_nominated_bank nb', 'si.bank_name = nb.id')
                ->innerJoin('awash_branches br', 'br.id = si.branch_id')
                ->innerJoin('awash_bank_region abr', ' abr.id=si.branch_region')


                ->andWhere("pr.id>=(select min(id) from payments_received where date_created>'$from_date')") //compound condition
                ->andWhere(['pr.reversed'=>false, 'pr.reversal'=>false])
                ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);

        $countQuery = new \yii\db\Query();
        $countQuery ->select(['distinct(school_id)'])
            ->from('payments_received pr')
            ->andWhere(['pr.reversed'=>false, 'pr.reversal'=>false])
            ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $query->where(['pr.school_id'=>$sch]);
            $countQuery->where(['pr.school_id'=>$sch]);
        }

        if(Yii::$app->user->can('view_by_region')){
            //If school user, limit and find by id and school id
            $sch = Yii::$app->user->identity->region_id;
            $query->where(['si.branch_region'=>$sch]);
            $countQuery->where(['si.branch_region'=>$sch]);

        }

        $query->groupBy(['si.school_code', 'si.school_name', 'nb.bank_name', 'br.branch_name','si.bank_account_number','abr.bank_region_name']);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);
      //  $pages = new Pagination(['totalCount' => $query->count()]);
        $query->offset($pages->offset)
           ->limit($pages->limit)
            ->orderBy('sum(amount) DESC');
        unset($_SESSION['findData']);
        $searchModel =new Reports();
        // $_SESSION['findData'] = $query;
        //$this->setSession('transaction Summary', $query, $searchModel->getCols(),  'schools_transaction_summary');
        $student_data = $query->all(Yii::$app->db2);

        $res = ['student_data'=>$student_data, 'pages'=>$pages, 'to_date'=>$to_date, 'from_date'=>$from_date,'searchModel'=>$searchModel];
        return $request->isAjax ? $this->renderAjax('school_trans', $res) : $this->render('school_trans', $res);

    }



    public function actionChannel()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
          session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
       // $end_date = date('Y-m-d', strtotime($to_date));
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $query= new \yii\db\Query();
        $query ->select(['pc.channel_name AS cn', 'pc.channel_type AS ct', 'sum(pr.amount) AS sum', 'count(pr.amount) AS count'])
                ->from('payments_received pr')
                ->innerJoin('payment_channels pc','pc.id = pr.payment_channel')
                ->innerJoin('core_school sch','sch.id = pr.school_id')
                ->andWhere("pr.id>=(select min(id) from payments_received where date_created>='$from_date')") //compound condition
                ->andWhere(['pr.reversed'=>false, 'pr.reversal'=>false])
                ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $query->andWhere(['pr.school_id'=>$sch]);
        }


        if(Yii::$app->user->can('view_by_region')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);

        }

        $query->groupBy(['pc.id', 'pc.channel_name', 'pc.channel_type']);

        $channel_data = $query->all(Yii::$app->db);
        $sum = $count = 0;
        foreach ($channel_data as $v){
            $sum= $sum + $v['sum'];
            $count= $count + $v['count'];
        }
        $res = ['channel_data'=>$channel_data, 'sum'=>$sum, 'count'=>$count, 'to_date'=>$to_date, 'from_date'=>$from_date];
        return $request->isAjax ? $this->renderAjax('channel_trans', $res) : $this->render('channel_trans', $res);
    }

    public function actionChnSummary()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $query= new \yii\db\Query();
        $query ->select(['pc.channel_name AS cn', 'pc.channel_type AS ct', 'br.branch_name','abr.bank_region_name','sum(pr.amount) AS sum', 'count(pr.amount) AS count'])
            ->from('payments_received pr')
            ->innerJoin('payment_channels pc','pc.id = pr.payment_channel')
            ->innerJoin('core_school sch','sch.id = pr.school_id')
            ->innerJoin('awash_branches br', 'br.id = sch.branch_id')
            ->innerJoin('awash_bank_region abr', ' abr.id=sch.branch_region')

            ->andWhere("pr.id>=(select min(id) from payments_received where date_created>'$from_date')") //compound condition
            ->andWhere(['pr.reversed'=>false, 'pr.reversal'=>false])
            ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $query->andWhere(['pr.school_id'=>$sch]);
        }


        if(Yii::$app->user->can('view_by_region')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);

        }

        $query->groupBy(['pc.id', 'pc.channel_name', 'pc.channel_type','br.branch_name','abr.bank_region_name']);
        //$query->orderBy(['abr.bank_region_name DESC']);

        $channel_data = $query->all(Yii::$app->db);
        $sum = $count = 0;
        foreach ($channel_data as $v){
            $sum= $sum + $v['sum'];
            $count= $count + $v['count'];
        }
        $res = ['channel_data'=>$channel_data, 'sum'=>$sum, 'count'=>$count, 'to_date'=>$to_date, 'from_date'=>$from_date];
        return $request->isAjax ? $this->renderAjax('channel_trans_summary', $res) : $this->render('channel_trans_summary', $res);
    }





    // student info report export to pdf and excel
    public function actionSelectedTransList()
    {
        if (!Yii::$app->session->isActive){
          session_start();
        }
        $query = $_SESSION['query'];
        $selected_list = $_SESSION['selected_list'];
        $tr_list = $_SESSION['tr_list'];
        $st_list = $_SESSION['st_list'];
        $sc_list = $_SESSION['sc_list'];
        $pr_list = $_SESSION['pr_list'];
        $query1= new \yii\db\Query();

        $query1 ->select($selected_list)
            ->from('school_account_transaction_history tr')
            ->innerJoin('payments_received pr','pr.id = tr.payment_id')
            ->innerJoin('core_student si','si.id = pr.student_id')
            ->innerJoin( 'core_school_class ls', 'ls.id = si.class_id')
            ->join('join','student_account_transaction_history sach','sach.payment_id = pr.id')

            ->andWhere($query);
        if(\app\components\ToWords::isSchoolUser()){
            $query1->andWhere(['pr.school_id'=>Yii::$app->user->identity->school_id]);
        }
        $query1->orderBy('tr.date_created DESC');
        $student_data = $query1->all(Yii::$app->db);
        if(!empty($_REQUEST['translistexport']))
        {
//            $html = $this->renderPartial('trans_report_pdf',[
//                        'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list
//                ]);
                    ini_set('max_execution_time', '300');
                    ini_set("pcre.backtrack_limit", "10000000");
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('trans_report_pdf',[
                        'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print students transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            ob_clean();
            return $pdf->render();        }
        elseif(!empty($_REQUEST['translistexcelexport']))
        {
            $file = $this->renderPartial('trans_report_excel',[
                        'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list
                ]);
            $fileName = "transactions_info_report".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }
        else
        {
            $query=$_SESSION['query'];
            $selected_list=null;
        }
    }

    // student info report export to pdf and excel
    public function actionSelectedSuppList()
    {
        if (!Yii::$app->session->isActive){
          session_start();
        }
        $query = $_SESSION['query'];
        $selected_list = $_SESSION['selected_list'];
        $tr_list = $_SESSION['tr_list'];
        $st_list = $_SESSION['st_list'];
        $sc_list = $_SESSION['sc_list'];
        $pr_list = $_SESSION['pr_list'];
        $query1= new \yii\db\Query();

        $query1 ->select($selected_list)
            ->from('supplementary_fee_payments_received pr','pr.id = tr.payment_id')
            ->innerJoin('core_student si','si.id = pr.student_id')
            ->innerJoin( 'core_school_class ls', 'ls.id = si.class_id')
           // ->join('join','student_account_transaction_history sach','sach.payment_id = pr.id')

            ->andWhere($query);
        if(\app\components\ToWords::isSchoolUser()){
            $query1->andWhere(['pr.school_id'=>Yii::$app->user->identity->school_id]);
        }
        $query1->orderBy('pr.date_created DESC');
        $student_data = $query1->all(Yii::$app->db);
        if(!empty($_REQUEST['translistexport']))
        {
//            $html = $this->renderPartial('trans_report_pdf',[
//                        'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list
//                ]);
            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('trans_report_pdf',[
                        'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print students transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);
            ob_clean();
            return $pdf->render();        }
        elseif(!empty($_REQUEST['translistexcelexport']))
        {
            $file = $this->renderPartial('trans_report_excel',[
                        'student_data'=>$student_data,'selected_list'=>$selected_list,'query'=>$query, 'tr_list'=>$tr_list, 'st_list'=>$st_list, 'sc_list'=>$sc_list, 'pr_list'=>$pr_list
                ]);
            $fileName = "transactions_info_report".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }
        else
        {
            $query=$_SESSION['query'];
            $selected_list=null;
        }
    }

    // student info report export to pdf and excel
    public function actionSummeryTransList()
    {
        if (!Yii::$app->session->isActive){
          session_start();
        }

        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        $query= new \yii\db\Query();
        $query ->select(['si.school_code AS sc', 'si.school_name AS sn', 'sum(amount) AS sum', 'count(amount) AS count', 'nb.bank_name AS bn', 'si.bank_account_number AS ban'])
            ->from('payments_received pr')
            ->innerJoin('core_school si','si.id = pr.school_id')
            ->innerJoin('core_nominated_bank nb', 'si.bank_name = nb.id')
            ->andWhere("pr.id>=(select min(id) from payments_received where date_created>'$from_date')") //compound condition
            ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $query->andWhere(['pr.school_id'=>$sch]);
        }
        $query->groupBy(['si.school_code', 'si.school_name', 'nb.bank_name', 'si.bank_account_number'])
            ->orderBy('sum(amount) DESC');

        $student_data = $query->all(Yii::$app->db2);
        $search_time = ['to'=>$to_date, 'from'=>$from_date];

        if(!empty($_REQUEST['summerylistexport']))
        {
            $html = $this->renderPartial('summery_report_pdf',['student_data'=>$student_data,'query'=>$search_time]);

            ob_clean();
            return Yii::$app->pdf->exportData('School Transactions summary','summery_report',$html);
        }
        elseif(!empty($_REQUEST['summerylistexcelexport']))
        {

            $file = $this->renderPartial('summery_report_excel',['student_data'=>$student_data,'query'=>$search_time]);
            $fileName = "school_transactions_history".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }

    }

    // Channel info report export to pdf and excel
    public function actionChannelTransList()
    {
        if (!Yii::$app->session->isActive){
          session_start();
        }
        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        $query= new \yii\db\Query();
        $query ->select(['pc.channel_name AS cn', 'pc.channel_type AS ct', 'sum(pr.amount) AS sum', 'count(pr.amount) AS count'])
            ->from('payments_received pr')
            ->innerJoin('payment_channels pc','pc.id = pr.payment_channel')
            ->andWhere("pr.id>=(select min(id) from payments_received where date_created>'$from_date')") //compound condition
            ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);

        if(\app\components\ToWords::isSchoolUser()){
            $sch = Yii::$app->user->identity->school_id;
            $query->andWhere(['pr.school_id'=>$sch]);
        }
        $query->groupBy(['pc.channel_name', 'pc.channel_type']);

        $channel_data = $query->all(Yii::$app->db);
        $sum = $count = 0;
        foreach ($channel_data as $v){
            $sum= $sum + $v['sum'];
            $count= $count + $v['count'];
        }
        $search_time = ['to'=>$to_date, 'from'=>$from_date];

        if(!empty($_REQUEST['channellistexport']))
        {
//            $html = $this->renderPartial('channel_report_pdf',[
//                        'channel_data'=>$channel_data,'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
//                ]);

            $pdf = new Pdf([
                'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                'destination' => Pdf::DEST_BROWSER,
                'content' => $this->renderPartial('channel_report_pdf',['channel_data'=>$channel_data,
                        'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
                    ]
                ),
                'options' => [
                    // any mpdf options you wish to set
                ],
                'methods' => [
                    'SetTitle' => 'Print channel transactions',
                    'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                    'SetHeader' => ['Student transactions||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                    'SetFooter' => ['|Page {PAGENO}|'],
                    'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                ]
            ]);

            ob_clean();
//            return Yii::$app->pdf->exportData('Channel Transactions summary','summery_report',$html);
            return $pdf->render();
        }
        elseif(!empty($_REQUEST['channellistexcelexport']))
        {
            $file = $this->renderPartial('channel_report_excel',[
                        'channel_data'=>$channel_data,'query'=>$search_time, 'sum'=>$sum, 'count'=>$count
                ]);
            $fileName = "channel_transactions_summery".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }

    }

    public function actionLists($acc)
    {
        $stu = CoreSchool::find()->where(['school_account_id'=>$acc])->limit(1)->one();
        $id = $stu->id;
        $countClasses = CoreSchoolClass::find()
                ->where(['school_id' => $id])
                ->count('*', Yii::$app->db2);

        $classes = CoreSchoolClass::find()
                ->where(['school_id' => $id])
                ->orderBy('class_code')
                ->all(Yii::$app->db2);

        if($countClasses > 0){
                echo "<option value=''> All Classes </option>";
            foreach($classes as $myClass){
                echo "<option value='".$myClass->id."'>".$myClass->class_description."</option>";
            }
        }
        else{
                echo "<option> -- </option>";
            }
    }

    public function actionClassLists($id)
    {
        $stu = CoreSchool::find()->where(['school_account_id'=>$id])->limit(1)->one();
        $sch = $stu->id;
        $countClasses = CoreSchoolClass::find()
            ->where(['school_id' => $sch])
            ->count('*', Yii::$app->db);

        $classes = CoreSchoolClass::find()
            ->where(['school_id' => $sch])
            ->orderBy('class_code')
            ->all(Yii::$app->db);

        if($countClasses > 0){
            echo "<option value=''> All Classes </option>";
            foreach($classes as $myClass){
                echo "<option value='".$myClass->id."'>".$myClass->class_description."</option>";
            }
        }
        else{
            echo "<option> No classes found </option>";
        }

    }


//
//    public function actionFeesPerformance()
//    {
//        $request = Yii::$app->request;
//        $model = new DynamicModel(['date_from','date_to', 'school_id', 'class_id', 'fees_list']);
//        $model->addRule(['school_id'], 'required');
//        $model->addRule(['date_from','date_to', 'school_id', 'class_id', 'fees_list'], 'safe');
//
//        if(!$model->fees_list) $model->fees_list = [];
//
//        if(\app\components\ToWords::isSchoolUser()){
//            $sch = Yii::$app->user->identity->school_id;
//            $model->school_id = $sch;
//        }
//
//        //Default view options
//        $pages = new Pagination(['totalCount' => 0]);
//        $res = ['student_data'=>[], 'pages'=>$pages, 'model'=>$model];
//
//
//        if(!$request->post()) {
//            if(!$model->fees_list) $model->fees_list=null;
//            return $request->isAjax ? $this->renderAjax('fees_performance', $res) : $this->render('fees_performance', $res);
//        }
//
//        if(!$model->load($request->post()) || !$model->validate()){
//            if(!$model->fees_list) $model->fees_list=null;
//            return $request->isAjax ? $this->renderAjax('fees_performance', $res) : $this->render('fees_performance', $res);
//        }
//
//
//
//
//
//        $_SESSION['query'] = ['from'=>$model->date_from,'to'=>$model->date_to];
//
//
////        if($model->student_campus == 0) $model->student_campus = null;
//        $query = new Query();
//        $query->select(['history.*', 'fees_due.due_amount', 'fees_due.description as fee_name',
//            'payments.amount as payment_amount', 'payments.payment_channel', 'payments.date_created as payment_date',
//            'student.student_code', "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) as student_name"])
//            ->from('institution_fees_due_student_payment_transaction_history history')
//            ->innerJoin('institution_fees_due fees_due', 'history.fee_id=fees_due.id')
//            ->innerJoin('core_school sch', 'sch.id=fees_due.school_id')
//            ->innerJoin('payments_received payments', 'payments.id=history.payment_id')
//            ->innerJoin('core_student student', 'student.id=history.student_id')
//            ->andWhere(['payments.reversed' => false])
//            ->andWhere(['payments.reversal' => false])
//            ->andFilterWhere(['student.class_id' => $model->class_id])
////            ->andFilterWhere(['student.campus_id' => $model->student_campus])
//            ->andFilterWhere(['fees_due.school_id' => $model->school_id])
//            ->andFilterWhere(['in', 'fees_due.id', $model->fees_list]);
//
//
//        if(Yii::$app->user->can('view_by_region')){
//            //If school user, limit and find by id and school id
//            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);
//
//        }
//
//        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
//
//        unset($_SESSION['findData']);
//        $_SESSION['findData'] = $query;
//
//        $query->offset($pages->offset)
//            ->limit($pages->limit)
//            ->orderBy('payments.id DESC');
//
//        $student_data = $query->all(Yii::$app->db);
//
//        $res = ['student_data'=>$student_data, 'pages'=>$pages, 'model'=>$model];
//        if(!$model->fees_list) $model->fees_list=null;
//        return $request->isAjax ? $this->renderAjax('fees_performance', $res) : $this->render('fees_performance', $res);
//
//    }
//


    public function actionFeesPerformance()
    {
        $request = Yii::$app->request;
        $model = new DynamicModel(['date_from','date_to', 'school_id', 'class_id', 'student_campus', 'fees_list','paid']);
        $model->addRule(['school_id'], 'required');
        $model->addRule(['date_from','date_to', 'school_id', 'class_id', 'student_campus', 'fees_list','paid'], 'safe');

        if(!$model->fees_list) $model->fees_list = [];

        if(Yii::$app->user->can('own_sch')){
            $sch = Yii::$app->user->identity->school_id;
            $model->school_id = $sch;
        }

        //Default view options
        $pages = new Pagination(['totalCount' => 0]);
        $res = ['student_data'=>[], 'pages'=>$pages, 'model'=>$model,'paid'=>$model->paid];


        if(!$request->get()) {
            if(!$model->fees_list) $model->fees_list=null;
            return $request->isAjax ? $this->renderAjax('fees_performance', $res) : $this->render('fees_performance', $res);
        }

        if(!$model->load($request->get()) || !$model->validate()){
            if(!$model->fees_list) $model->fees_list=null;
            return $request->isAjax ? $this->renderAjax('fees_performance', $res) : $this->render('fees_performance', $res);
        }


        $_SESSION['query'] = ['from'=>$model->date_from,'to'=>$model->date_to];


        if($model->student_campus == 0) $model->student_campus = null;
        $query = new Query();

        if($model->paid) {


            $query->select(['history.*', 'fees_due.due_amount', 'fees_due.description as fee_name',
                'payments.amount as payment_amount', 'payments.payment_channel', 'payments.date_created as payment_date', 'sc.class_code', 'student.student_phone','student.guardian_phone',
                'student.student_code', "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) as student_name"])
                ->from('institution_fees_due_student_payment_transaction_history history')
                ->innerJoin('institution_fees_due fees_due', 'history.fee_id=fees_due.id')
                ->innerJoin('payments_received payments', 'payments.id=history.payment_id')
                ->innerJoin('core_student student', 'student.id=history.student_id')
                ->innerJoin('core_school_class sc', 'student.class_id=sc.id')
                ->andWhere(['payments.reversed' => false])
                ->andWhere(['payments.reversal' => false])
                ->andFilterWhere(['student.class_id' => $model->class_id])
                ->andFilterWhere(['student.campus_id' => $model->student_campus])
                ->andFilterWhere(['fees_due.school_id' => $model->school_id])
                ->andFilterWhere(['in', 'fees_due.id', $model->fees_list])
                ->andFilterWhere(['between', 'payments.date_created', $model->date_from,$model->date_to]);

        }
        else{
            $query->select(['history.*', 'fees_due.description as fee_name', 'fees_due.date_created','history.date_created as payment_date','fees_due.due_amount as amount', 'sc.class_code', 'student.student_phone','student.guardian_phone',
                'fees_due.due_amount as payment_amount', 'student.student_code', "format('%s%s%s', first_name, case when middle_name is not null then ' '||middle_name else '' end, case when last_name is not null then ' '||last_name else '' end) as student_name"])
                ->from('institution_fee_student_association history')
                ->innerJoin('institution_fees_due fees_due', 'history.fee_id=fees_due.id')
                ->innerJoin('core_student student', 'student.id=history.student_id')
                ->innerJoin('core_school_class sc', 'student.class_id=sc.id')
                ->andWhere(['<','history.fee_outstanding_balance', 0])
                ->andFilterWhere(['student.student_class' => $model->class_id])
                ->andFilterWhere(['student.campus_id' => $model->student_campus])
                ->andFilterWhere(['fees_due.school_id' => $model->school_id])
                ->andFilterWhere(['in', 'fees_due.id', $model->fees_list])
                ->andFilterWhere(['between', 'fees_due.effective_date', $model->date_from,$model->date_to]);
            // ->having(['count','>' ,1]);
        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('history.id DESC');

        $student_data = $query->all(Yii::$app->db2);

        $res = ['student_data'=>$student_data, 'pages'=>$pages, 'model'=>$model,'paid'=>$model->paid];
        if(!$model->fees_list) $model->fees_list=null;
        return $request->isAjax ? $this->renderAjax('fees_performance_info', $res) : $this->render('fees_performance_info', $res);

    }
    public function actionBalanceAdjustmentReport()
    {
        $request = Yii::$app->request;
        $model = new DynamicModel(['date_from','date_to', 'school_id', 'class_id', 'student_campus']);
        $model->addRule(['school_id'], 'required');
        $model->addRule(['date_from','date_to', 'school_id', 'class_id', 'student_campus'], 'safe');

        if(Yii::$app->user->can('own_sch')){
            $sch = Yii::$app->user->identity->school_id;
            $model->school_id = $sch;
        }

        //Default view options
        $pages = new Pagination(['totalCount' => 0]);
        $res = ['student_data'=>[], 'pages'=>$pages, 'model'=>$model];

        if(!$request->post()) {
            return $request->isAjax ? $this->renderAjax('balance_adjustment', $res) : $this->render('balance_adjustment', $res);
        }

        if(!$model->load($request->post()) || !$model->validate()){
            return $request->isAjax ? $this->renderAjax('balance_adjustment', $res) : $this->render('balance_adjustment', $res);
        }


        $_SESSION['query'] = ['from'=>$model->date_from,'to'=>$model->date_to];



        if($model->student_campus == 0) $model->student_campus = null;

        $query = new Query();
        $query->select([
            'wl.*',
            "format('%s%s', si.first_name, case when si.last_name is not null then ' '||si.last_name else '' end) as student_name",
            'student_code',
            'class_description',
            '(balance_after - amount) as balance_before',
            'balance_after',
            'amount',
            'sh.description as reason',
            "format('%s%s', us.firstname,  case when us.lastname is not null then ' '||us.lastname else '' end) as user_full_name",
            ])
            ->from('web_console_log wl')
            ->innerJoin('core_student si', 'wl.affected_student_id=si.id')
            ->innerJoin('core_school_class cl', 'cl.id=si.class_id')
            ->innerJoin('core_school sch', 'sch.id=si.school_id')
            //->leftJoin('web_console_users us', 'us.id=wl.web_user_id')
            ->leftJoin('user us', 'us.id=wl.web_user_id')
            ->leftJoin('student_account_transaction_history sh', 'sh.id=wl.student_th_id')
            ->andWhere(['event_action_id' => 'ADJUST_BALANCE'])
            ->andFilterWhere(['si.class_id' => $model->class_id])
            ->andFilterWhere(['si.campus_id' => $model->student_campus])
            ->andFilterWhere(['si.school_id' => $model->school_id]);

        if(Yii::$app->user->can('view_by_region')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);

        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db2)]);

        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;

        $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy('wl.id DESC');

        $student_data = $query->all(Yii::$app->db2);

        $res = ['student_data'=>$student_data, 'pages'=>$pages, 'model'=>$model];
        return $request->isAjax ? $this->renderAjax('balance_adjustment', $res) : $this->render('balance_adjustment', $res);

    }



    public function  actionSchoolPerformance(){
        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $model = new DynamicModel(['date_from','date_to', 'branch_id', 'branch_region']);
        $model->addRule(['school_id'], 'required');
        $model->addRule(['date_from','date_to', 'branch_id', 'branch_region'], 'safe');



        $from_date = empty($model->date_from) ? date('Y-m-d', strtotime("-30 days") ) : $model->date_from;

        $to_date = empty($model->date_to) ? date('Y-m-d') : $model->date_to;
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));



        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];


        $student_data = Yii::$app->db->createCommand("select  trans.school_name ,trans.branch_name,trans.school_code,bank_region_name,count(std.school_id) as number_of_students, trans.val as transaction_value, trans.txns as txnz  from
(select  si.id as schId, si.school_name, si.school_code, bank_region_name,branch_name,count(si.school_name) ,  sum(pr.amount) as val, count(pr.school_id) as txns from 
payments_received pr 
inner join core_school si on pr.school_id = si.id
inner join awash_bank_region abr on abr.id=si.branch_region
inner join awash_branches ab on ab.id=si.branch_id
WHERE pr.date_created >=:start_date AND pr.date_created < :end_date
  
AND pr.reversal = false AND pr.reversed = false --Exclude reversals and reversed transactions
AND si.branch_id =:branch_i
GROUP BY si.school_name, si.id ,si.school_code,bank_region_name,branch_name
ORDER BY sum(pr.amount) DESC,
count(si.school_name) desc)trans
inner join core_student std  on std.school_id  = trans.schId
group by  trans.school_name , trans.val , std.school_id ,trans.school_code,bank_region_name, trans.txns, trans.branch_name
order by  trans.val desc;")

            ->bindValue(':start_date',$from_date)
            ->bindValue(':end_date',$end_date)
            ->bindValue(':branch_region',$branch_region)
            ->bindValue(':branch_id',$branch_id)
            ->queryAll();



//        $countQuery = new \yii\db\Query();
//        $countQuery ->select(['distinct(school_id)'])
//            ->from('payments_received pr')
//            ->andWhere(['pr.reversed'=>false, 'pr.reversal'=>false])
//            ->andWhere(['between', 'pr.date_created', $from_date, $end_date]);
        $sum = $count= $ttxn = 0;
        foreach ($student_data as $v){
            $sum= $sum + $v['transaction_value'];
            $count= $count + $v['number_of_students'];
            $ttxn= $ttxn + $v['txnz'];

        }


        $res = ['student_data'=>$student_data, 'to_date'=>$to_date, 'from_date'=>$from_date,'sum'=>$sum, 'count'=>$count,'ttxn'=>$ttxn,'model'=>$model];
        return $request->isAjax ? $this->renderAjax('school_perfomance', $res) : $this->render('school_perfomance', $res);

    }

    // student info report export to pdf and excel
    public function actionSummaryPerfList()
    {
        if (!Yii::$app->session->isActive){
            session_start();
        }

        $s_query = $_SESSION['query'];
        $from_date = $s_query['from'] ? $s_query['from']: date('Y-m-d', strtotime("-30 days") );
        $to_date = $s_query['to'] ? $s_query['to'] : date('Y-m-d');
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));

        $student_data = Yii::$app->db->createCommand("select  trans.school_name ,trans.school_code,count(std.school_id) as number_of_students, trans.val as transaction_value  from
(select  si.id as schId, si.school_name, si.school_code, count(si.school_name) ,  sum(pr.amount) as val from 
payments_received pr inner join core_school si on pr.school_id = si.id
LEFT JOIN auto_settlement_requests asr on pr.id = asr.payment_id
WHERE pr.date_created >=:start_date AND pr.date_created < :end_date
AND pr.reversal = false AND pr.reversed = false --Exclude reversals and reversed transactions
GROUP BY si.school_name, si.id ,si.school_code
ORDER BY sum(pr.amount) DESC,
count(si.school_name) desc)trans
inner join core_student std  on std.school_id  = trans.schId
group by  trans.school_name , trans.val , std.school_id ,trans.school_code
order by  trans.val desc;")

            ->bindValue(':start_date',$from_date)
            ->bindValue(':end_date',$end_date)
            ->queryAll();

        $search_time = ['to'=>$to_date, 'from'=>$from_date];
        $sum = $count = 0;
        foreach ($student_data as $v){
            $sum= $sum + $v['transaction_value'];
            $count= $count + $v['number_of_students'];
        }
        if(!empty($_REQUEST['summerylistexport']))
        {
            $html = $this->renderPartial('summary_perf_report_pdf',['student_data'=>$student_data,'query'=>$search_time,'sum'=>$sum, 'count'=>$count]);

            ob_clean();
            return Yii::$app->pdf->exportData('School Performance Report','summery_report',$html);
        }
        elseif(!empty($_REQUEST['summerylistexcelexport']))
        {

            $file = $this->renderPartial('summary_perf_report_excel',['student_data'=>$student_data,'query'=>$search_time,'sum'=>$sum, 'count'=>$count]);
            $fileName = "school_performance_report".date('YmdHis').'.xls';
            $options = ['mimeType'=>'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($file, $fileName, $options);
        }

    }

    public function actionInactiveSchools()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }

        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-90 days") ) : date('Y-m-d', strtotime($_GET['date_from'] . ' -90 days'));


        $_SESSION['query'] = ['from'=>$from_date];
            $searcModel = new Reports();
            $data = $searcModel->NoSchTxns($from_date);
            $dataProvider = $data['query'];
            $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
            $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel];
            return ($request->isAjax) ? $this->renderAjax('school_txns', $res) :
                $this->render('school_txns', $res);


    }

    public function actionSchoolsWithoutFees()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }

        //$from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];


        //$_SESSION['query'] = ['from'=>$from_date];
        $searcModel = new Reports();
        $data = $searcModel->NoFeesSet(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('no_set_fees', $res) :
            $this->render('no_set_fees', $res);


    }

    public function actionSchoolsWithFees()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }

        //$from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];


        //$_SESSION['query'] = ['from'=>$from_date];
        $searcModel = new Reports();
        $data = $searcModel->feesSet(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('set_fees', $res) :
            $this->render('set_fees', $res);


    }
 public function actionSchoolsWithoutStudentData()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }


        //$_SESSION['query'] = ['from'=>$from_date];
        $searcModel = new Reports();
        $data = $searcModel->NoStudents(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('no_students_added', $res) :
            $this->render('no_students_added', $res);


    }
    public function actionSchoolsReportData()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];
       // $to_date = ;

        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];;

        $searcModel = new Reports();
        $data = $searcModel->schPerfomance(Yii::$app->request->queryParams,$from_date,$to_date);
        $dataProvider = $data['query'];

        $sum = $count= $ttxn = 0;
        foreach ($dataProvider as $v){
            $sum= $sum + $v['transaction_value'];
            $count= $count + $v['number_of_students'];
            $ttxn= $ttxn + $v['txnz'];

        }

        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel,'sum'=>$sum, 'count'=>$count,'ttxn'=>$ttxn,'from_date'=>$from_date,'date_to'=>$to_date];
        return ($request->isAjax) ? $this->renderAjax('school_perfomance', $res) :
            $this->render('school_perfomance', $res);


    }
    public function actionSchoolsWithStudentData()
    {

        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $searcModel = new Reports();


        $data = $searcModel->Students( Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'pages' => $pages, 'sort' => $data['sort'],'searchModel'=>$searcModel,'model'=>$searcModel];
        return ($request->isAjax) ? $this->renderAjax('students_added', $res) :
            $this->render('students_added', $res);


    }
    public function actionChannelPerfomance()
    {
        $request = Yii::$app->request;
        if (!Yii::$app->session->isActive){
            session_start();
        }
        $from_date = empty($_GET['date_from']) ? date('Y-m-d', strtotime("-30 days") ) : $_GET['date_from'];
        $to_date = empty($_GET['date_to']) ? date('Y-m-d') : $_GET['date_to'];
        $end_date = date('Y-m-d', strtotime($to_date."+1 days"));
        $_SESSION['query'] = ['from'=>$from_date,'to'=>$to_date];
        $query= new \yii\db\Query();
        $query ->select(['ab.branch_name','abr.bank_region_name', 'ref.description', 'sch.school_name','count(std.school_id) as stds ','sch.school_code','sch.id'])

            ->from('core_school sch','sch.id = pr.school_id')
            ->innerJoin('core_student std', 'sch.id = std.school_id')
            ->innerJoin('awash_branches ab', 'ab.id = sch.branch_id')
            ->innerJoin('ref_region ref', 'ref.id = sch.region')
            ->innerJoin('awash_bank_region abr', 'abr.id=sch.branch_region');
        $query->andWhere(['in','sch.id',[69,2,1,20,56,4,7]]);
        //$query->orWhere(['sch.id'=>2]);


        if(Yii::$app->user->can('view_by_region')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_region' => Yii::$app->user->identity->region_id]);

        }

        $query->groupBy(['ab.branch_name','abr.bank_region_name', 'ref.description',  'sch.school_name','sch.school_code','sch.id']);

        $school_data = $query->all(Yii::$app->db);
        $schools=[];
        $res=[];
Yii::trace($school_data);
        foreach($school_data as $sch)
        {
            //get channel perfomance per school
            $queryTxn= new \yii\db\Query();
            $queryTxn ->select(['sch.id AS id', 'sch.school_name AS sch', 'chn.channel_name AS cn', 'count(chn.channel_name) AS count', 'SUM(pr.amount) AS sum' ])
                ->from('payments_received pr')
                ->innerJoin('payment_channels chn', 'pr.payment_channel=chn.id' )
                ->innerJoin('core_school sch','pr.school_id = sch.id')
                ->innerJoin('core_nominated_bank bd','sch.bank_name = bd.id')
                ->Where(['pr.reversed'=>false, 'pr.reversal'=>false])
                ->andWhere(['between', 'pr.date_created', $from_date, $end_date])
                //->andWhere(['in','pr.school_id',[69,2]]);
                ->andWhere(['pr.school_id'=>$sch['id']])
                ->groupBy(['chn.channel_name', 'chn.channel_type','sch.school_name','sch.id']);
             //   ->orderBy(['sch.school_name DESC']);
            $queryTxn->orderBy('COALESCE(chn.channel_name) DESC,SUM(pr.amount) DESC');
           $queryTxn->limit(3);
           $school_txn_data = $queryTxn->all(Yii::$app->db);

           //Yii::trace($school_txn_data);
            $chns =[];
            if(!$school_txn_data){
                $chns=[];
            }else{
                foreach ($school_txn_data as $k=>$v){

                    $chn = $v['cn'];
                    $details =[
                        'chn'=>$v['cn'],
                        'no_of_txn'=>$v['count'],
                        'txn_valu'=>$v['sum']
                    ];


                  //  $res1 = [$chn=>$details];
                    array_push($chns,$details);

                }
           //     Yii::trace($chns);

            }

//            Yii::trace($school_txn_data);

            $schCode=$sch['school_code'];

            $schA =[

            'schoolname'=>$sch['school_name'],
            'branchname'=>$sch['branch_name'],
            'regionname'=>$sch['bank_region_name'],
            'ctyregion'=>$sch['description'],
            'stds'=>$sch['stds'],
             'transactions'=>   $chns

            ];
            $res = [$schCode=>$schA];
            //Yii::trace($res);

            array_push($schools,$res);

        }
        Yii::trace($schools);

        $res = ['channel_data'=>$schools, 'sum'=>0, 'count'=>0, 'to_date'=>$to_date, 'from_date'=>$from_date];
        return $request->isAjax ? $this->renderAjax('channel_trans_summaries', $res) : $this->render('channel_trans_summaries', $res);
    }






}

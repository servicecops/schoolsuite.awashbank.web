<?php

namespace app\modules\recon\controllers;

use app\components\AwashCbsHelperHelper;


use app\components\AwashStatementHelper;
use app\components\ToWords;
use app\modules\paymentscore\models\PaymentChannels;
use app\modules\recon\models\Reconciliation;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use Exception;
use stdClass;
use Yii;
use app\models\ReplyForm;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Client;
use yii\db\Query;
use yii\helpers\Json;
use yii\base\DynamicModel;
use app\modules\banks\models\BankAccountDetails;
use yii\base\ErrorException;
use app\modules\logs\models\Logs;


class ReconciliationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionCreditPaymentChannel()
    {
        if (Yii::$app->user->can('postings')) {
            $request = Yii::$app->request;
            $model = new DynamicModel(['amount', 'depositDetails', 'paymentChannelId', 'account_id']);
            $model->addRule(['amount', 'depositDetails', 'paymentChannelId', 'account_id'], 'required');
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user_name = Yii::$app->user->identity->username;
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $connection->createCommand("select credit_payment_channel(
                        :account_id, 
                        :depositDetails, 
                        :amount, 
                        :user_name)")
                        ->bindValue(':account_id', $model->account_id)
                        ->bindValue(':depositDetails', $model->depositDetails)
                        ->bindValue(':amount', $model->amount)
                        ->bindValue(':user_name', $user_name)
                        ->execute();

                    $transaction->commit();
                    Logs::logEvent("payment channel credited - " . $model->depositDetails . " (" . $model->amount . ")", null, null);
                    $response = "Channel has been credited successfully";
                    return ($request->isAjax) ? $this->renderAjax('posting_response', ['response' => $response]) : $this->render('posting_response', ['response' => $response]);
                } catch (Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Logs::logEvent("Failed to credit paynement channel", $error, null);
                    Yii::$app->session->setFlash('actionFailed', $error);
                    return Yii::$app->runAction('/student/error');
                }

            } else
                return ($request->isAjax) ? $this->renderAjax('feesform', ['model' => $model]) :
                    $this->render('feesform', ['model' => $model]);
        } else {
            throw new ForbiddenHttpException('No permissions to trigger this service.');
        }

    }

    public function actionPaymentchannelAccs($id)
    {

        $main_pc = (new Query())->select(['pgl.description', 'pgl.balance', 'pgl.id'])->from('payment_channels pc')->innerJoin('payment_channel_account_gl pgl', 'pc.payment_channel_account_id=pgl.id')->where(['pc.id' => $id])->all();

        $sec_pc = (new Query())->select(['pgl.description', 'pgl.balance', 'nb.bank_name', 'pgl.id'])->from('payment_channel_secondary_accounts sec')->innerJoin('payment_channel_account_gl pgl', 'pgl.id=sec.account_id')->innerJoin('nominated_bank_details nb', 'nb.id=sec.bank_id')->where(['sec.payment_channel' => $id])->all();
        $result = "<thead><tr>#<th>&nbsp;</th><th>Description</th><th>Balance</th><th>Bank</th></tr></thead>";
        foreach ($main_pc as $k => $v) {
            $result .= "<tr><td><input type='radio' name='DynamicModel[account_id]' id='" . 'radio' . $v['id'] . "' value='" . $v['id'] . "'><label for='" . 'radio' . $v['id'] . "'>&nbsp;</label></td><td>" . $v['description'] . " </td><td>" . $v['balance'] . "</td><td></td></tr>";
        }
        foreach ($sec_pc as $k => $v) {
            $result .= "<tr><td><input type='radio' name='DynamicModel[account_id]' id='" . 'radio' . $v['id'] . "' value='" . $v['id'] . "'><label for='" . 'radio' . $v['id'] . "'>&nbsp;</label></td><td>" . $v['description'] . " </td><td>" . $v['balance'] . "</td><td>" . $v['bank_name'] . "</td></tr>";
        }

        return $result;

    }

    public function actionSchoolPayout()
    {
        if (Yii::$app->user->can('postings')) {
            $request = Yii::$app->request;
            $model = new DynamicModel(['amount', 'instructionDate', 'payoutBankDetails', 'payoutBank', 'schoolId']);
            $model->addRule(['amount', 'instructionDate', 'payoutBank', 'schoolId'], 'required');

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $client = new Client();
                $user = Yii::$app->user->identity->username;

//            $response = Json::decode($response->content);
//            return ($request->isAjax) ? $this->renderAjax('posting_response', ['response'=>$response]) :
//                    $this->render('posting_response', ['response'=>$response]);
            } else
                return ($request->isAjax) ? $this->renderAjax('schpayout_form', ['model' => $model]) :
                    $this->render('schpayout_form', ['model' => $model]);
        } else {
            throw new ForbiddenHttpException('No permissions to trigger this service.');
        }
    }


    private function bibStatement($schCode, $model)
    {
        $from_date = date("m/d/Y", strtotime($model->from_date));
        $to_date = date("m/d/Y", strtotime($model->to_date));
        $res = AwashCbsHelperHelper::getStatement($schCode->account_number, $from_date, $to_date);
        $error = '';
        if ($res->responseCode == '00') {
            $res = $res->history;
            Logs::logEvent("Bank Statement Request for school account (" . $schCode->account_number . ")", null, null);
        } else {
            $error = $res->responseText;
            $res = [];
            Logs::logEvent("Failed Bank Statement Request for school account (" . $schCode->account_number . ")", $error, null);

        }

        return ['res' => $res, 'type' => 'json', 'error' => $error];
    }


    public function actionAccLists($id)
    {
        $countAccounts = BankAccountDetails::find()
            ->where(['school_id' => $id])
            ->count();

        $accounts = BankAccountDetails::find()
            ->where(['school_id' => $id])
            ->all();

        if ($countAccounts > 0) {
            echo "<option value=''> Select Account </option>";
            foreach ($accounts as $v) {
                echo "<option value='" . $v->id . "'>" . $v->account_number . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }

    private function getThisAccount($acc)
    {
        $acc = CoreBankAccountDetails::find()->where(['id' => $acc])->limit(1)->one();
        return $acc;
    }

    private function checkBank($sch)
    {
        $query = (new Query())->select(['nb.bank_name', 'nb.bank_code'])->from('core_school sch')->innerJoin('nominated_bank_details nb', 'nb.id=sch.bank_name')->where(['sch.id' => $sch])->limit(1)->one();
        return $query;
    }

    /**
     * @param $account - Numeric, the account id for the bank_account_details_record
     * @return array|record - The bank details id and bank_code
     * Gets the bank details for the supplied account id
     */
    private function checkAccountBank($account)
    {
        $query = (new Query())->select(['nbd.id', 'nbd.bank_code', 'nbd.bank_name', 'sch.school_code'])
            ->from('core_bank_account_details bad')
            ->innerJoin('core_nominated_bank nbd', 'nbd.id=bad.bank_id')
            ->innerJoin('core_school sch', 'sch.id=bad.school_id')
            ->where(['bad.id' => $account])->limit(1)->one();
        return $query;
    }


    public function actionFetchStatement()
    {
        if (ToWords::isSchoolUser()) {
            $enabled = CoreSchool::find()
                ->where(['id' => Yii::$app->user->identity->school_id])
                ->limit(1)->one()->enable_bank_statement;
            if (!$enabled) {
                Yii::$app->session->setFlash('actionFailed', "BANK STATEMENT IS NOT ENABLED FOR THIS SCHOOL. Please contact SchoolPay admin");
                return Yii::$app->runAction('/student/error');
            }
        }
        if (!Yii::$app->user->can('view_statement'))
            throw new ForbiddenHttpException('No permissions to trigger this service.');


        $request = Yii::$app->request;
        $res = "";
        $error = "";
        $bankCode = "";
        $result = "";
        $type = "array"; //The result type
        $model = new DynamicModel(['school', 'acc', 'from_date', 'to_date']);
        $model->addRule(['acc', 'from_date', 'to_date'], 'required')
            ->addRule(['school'], 'safe');
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {


                $sch = (ToWords::isSchoolUser()) ? Yii::$app->user->identity->school_id : $model->school;
                $acc = $this->getThisAccount($model->acc);
                $bank = $this->checkAccountBank($model->acc);
                $bankCode = $bank['bank_code'];
                Yii::trace($acc);

                if ($bankCode == 'AWASH') {


                    //Take the bib statement route
                    if ($acc) {

                        $processedRes = $this->awashStatement($acc, $model, $bankCode);

                        if ($processedRes['res']) $res = json_decode($processedRes['res']);
                        if ($processedRes['error']) $error = $processedRes['error'];
                        if ($processedRes['type']) $type = $processedRes['type'];


                        $result = $res;

                        Yii::$app->db->createCommand("delete from recon_temporary_table where school_id =:sch")
                            ->bindValue(':sch', $sch)
                            ->execute();


                        if ($result) {

                            $acct_no = $res->accountId;
                            foreach ($result->items as $k => $v) {

                                $amountDetails = $v->amount;
                                Yii::trace($amountDetails);

                                $tempModel = new Reconciliation();
                                $tempModel->description = $v->description;
                                $tempModel->acct_no = $acct_no;
                                $tempModel->amount = $amountDetails->amount;
                                $tempModel->currency = $amountDetails->currency;
                                $tempModel->transaction_id = $v->transactionId;
                                $tempModel->booking_time = date("Y-m-d h:i:s", strtotime($v->bookingDateTime));
                                $tempModel->school_id = $sch;
                                $tempModel->save(false);

                            }



                            $transaction->commit();
                        }

                        $recon = $this->reconcile($model, $sch);

                        $result = [
                            'awashStatementTxns' => $recon['awashStatementTxns'],
                            'awashSchPay' => $recon['awashSchPay'],
                            'matching' => $recon['matching'],
                            'to_date' => $recon['to_date'],
                            'from_date' => $recon['from_date'],
                            'sch' => $sch


                        ];


                    }
//                        return $this->centeJSONStatement($acc, $model, $bankCode); //Use json api
                } else {
                    $error = $bank['bank_name'] . " does not provide bank statements.";
                }
            }
        } catch (Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to retrieve bank statement", $error, null);
            Yii::$app->session->setFlash('actionFailed', $error);
            return Yii::$app->runAction('/site/error');
        }

        $results = ['model' => $model, 'res' => $res, 'error' => $error, 'bankCode' => $bankCode, 'type' => $type, 'result' => $result];

        return ($request->isAjax) ? $this->renderAjax('bank_statement', $results) : $this->render('bank_statement', $results);


    }


    public function actionPostPayments($schoolId)
    {
        $logs = [];
        $requests = Yii::$app->request;
        $connection = Yii::$app->db;
        // $transaction = $connection->beginTransaction();
        try {
            $bibStatementTxns = Yii::$app->db->createCommand("SELECT
                                                    rc.acct_no,
                                                    rc.depositor_payee_nm,
                                                    rc.crncy_cd_iso,
                                                    rc.dr_cr_ind,
                                                    rc.origin_bu_id,
                                                    rc.stmnt_bal,
                                                    rc.tran_desc,
                                                    rc.tran_dt,
                                                    rc.tran_journal_id,
                                                    rc.txn_amt,
                                                    rc.txn_reference,
                                                    rc.school_id ,rc.regno, rc.the_ref
       
                                                FROM
                                                    (select *, case when left(tran_desc, 3)='251' OR left(tran_desc,3)='APP' then  tran_journal_id else txn_reference end as the_ref from recon_temporary_table where school_id =:school_id ) rc
                                                    LEFT JOIN 
                                                    payments_received pr 
                                                    ON rc.the_ref = pr.channel_trans_id 
                                                WHERE
                                                    pr.ID IS NULL and rc.regno is not null   
                                                ORDER BY
                                                    tran_dt DESC")
                ->bindValue(':school_id', $schoolId)
                ->queryAll();
            //allow part payments in school
            $school = CoreSchool::findOne([$schoolId]);
            $schBehaviour = $school->default_part_payment_behaviour;
            if (!$school->default_part_payment_behaviour) {
                $school->default_part_payment_behaviour = true;
                $school->save();
            }
            Yii::trace("school after update");
            Yii::trace($school->default_part_payment_behaviour);

            foreach ($bibStatementTxns as $data) {

                // allow part payments
                $code = (int)$data['regno'];
                $std = CoreStudent::findOne(['payment_code' => $code]);
                if ($std) {

                    Yii::trace($std->allow_part_payments);
                    $stdStatus = $std->allow_part_payments;
                    if (!$std->allow_part_payments) {
                        $std->allow_part_payments = true;
                        $std->save();
                    }

                    
                    $paymentChanel = PaymentChannels::findOne(['id' => 1]);
                    $password = $paymentChanel->password;
                    $accessCode = $paymentChanel->channel_code;
                    $url = Yii::$app->params['postPayment'];
                    $nonce = Yii::$app->security->generateRandomString(10);
                    //Using Teller as channel

                    $hash = md5($password . $nonce);

                    //map $data to incoming paymentrequest
                    $req = (object)[
                        "reference" => $data['regno'],
                        "amount" => (string)$data['txn_amt'],
                        "transactionId" => $data['the_ref'],
                        "channelTealId" => $data['tran_desc'],
                        "processDate" => $data['tran_dt'],
                        "accessCode" => $accessCode,
                        "accessPassword" => $password,
                        "hash" => $hash
                    ];
                    $request = json_encode($req);


                    $client = new Client([
                        'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
                    ]);


                    Yii::trace('Connecting to  ' . $url . ' to send ' . $request . ' with headers' . $nonce . ' and hash' . $hash);


                    $response = $client->createRequest()
                        ->setMethod('POST')
                        ->setUrl($url)
                        ->addHeaders(['content-type' => 'application/json'])
                        ->addHeaders(['nonce' => $nonce])
                        ->setContent($request)
                        ->setOptions(['sslVerifyPeer' => false,
                            CURLOPT_SSL_VERIFYHOST => false,
                            CURLOPT_SSL_VERIFYPEER => false])
                        ->send();

                    $content = $response->getContent();

                    if (!$response->isOk) {
                        return (object)['responseCode' => 500, 'responseText' => 'Could not connect to BIB Payment service. Please try again later. HTTP Code: ' . $response->getStatusCode()];
                    }
                
                    // re-update part payment status
                    $std->allow_part_payments = $stdStatus;
                    $std->save();

                    $json_decode = $content;


                    $incomingRequest = json_decode($json_decode, true);
                    Logs::logEvent("Response for posting" . $incomingRequest['returnMessage'] . " url connected to is: " . $url, null, null);

                    $msg = ['message' => $incomingRequest['returnMessage'], 'affectedStudent' => $data['regno']];
                    array_push($logs, $msg);
                }

            }
            //re-update
            Yii::trace("school after update");

            $school->default_part_payment_behaviour = $schBehaviour;
            $school->save();
            Yii::trace($school->default_part_payment_behaviour);


            //Yii::trace($logs);

        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Failed to post bank statement payments", $error, null);
            Yii::$app->session->setFlash('actionFailed', $error);
            return Yii::$app->runAction('/student/error');
        }
        return ($requests->isAjax) ? $this->renderAjax('show_logs', ['logs' => $logs, 'data' => $bibStatementTxns]) :
            $this->render('show_logs', ['logs' => $logs, 'data' => $bibStatementTxns]);

    }


    public function reconcile($model, $sch)
    {
        $from_date = date("Y-m-d ", strtotime($model->from_date));
        // $to_date = date("Y-m-d", strtotime($model->to_date));
        $to_date = date('Y-m-d', strtotime($model->to_date . "+1 days"));


        $awashStatementTxns = Yii::$app->db->createCommand("SELECT
                                                    rc.acct_no,
                                                    rc.description,
                                                    rc.amount,
                                                    rc.currency,
                                                    rc.transaction_id,
                                                    rc.acct_no,
                                                    rc.booking_time,
                                                    rc.school_id 
                                                FROM
                                                    (select * from recon_temporary_table 
                                                    where booking_time >=:from_date AND booking_time <=:end_date and school_id =:school_id ) rc
                                                    LEFT JOIN 
                                                    payments_received pr 
                                                    ON rc.transaction_id = pr.channel_trans_id 
                                                WHERE
                                                    pr.ID IS NULL 
                                                ORDER BY
                                                    booking_time DESC")
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date', $to_date)
            ->bindValue(':school_id', $sch)
            ->queryAll();


//        $bibSchPay = Yii::$app->db->createCommand('
//
//select DISTINCT(pr.channel_trans_id),pr.amount,pr.reciept_number,pr.student_id, pr.date_created, pr.channel_memo, concat(si.last_name,\' \',si.first_name) as std_name,  sch.school_name, cls.class_code from payments_received pr left JOIN recon_temporary_table rc on pr.channel_trans_id = rc.txn_reference
//
//inner join student_information si on si.id = pr.student_id
//inner join core_school sch on sch.id = pr.school_id
//inner join institution_student_classes cls on cls.id = si.student_class
//
//where pr.date_created >=:from_date and pr.date_created <= :end_date and pr.school_id=:school_id and rc.id is null order by date_created desc;' )
//            ->bindValue(':from_date', $from_date)
//            ->bindValue(':end_date',$to_date)
//            ->bindValue(':school_id',$sch)
//            ->queryAll();


        $awashSchPay = Yii::$app->db->createCommand("
                                    SELECT DISTINCT
                                ( pr.channel_trans_id ),
                                pr.amount,
                                pr.reciept_number,
                                pr.student_id,
                                pr.date_created,
                                pr.channel_memo,
                                sch.school_name
                                FROM
                                payments_received pr
                                LEFT JOIN 
                            (select * from recon_temporary_table where booking_time >=:from_date AND booking_time <=:end_date and school_id =:school_id ) rc
                            ON pr.channel_trans_id = rc.transaction_id
                           INNER JOIN core_school sch ON sch.ID = pr.school_id
                               WHERE
                                pr.date_created >=:from_date 
                                AND pr.date_created <= :end_date 
                                AND pr.school_id =:school_id 
                                AND rc.ID IS NULL 
                            ORDER BY
                                date_created DESC")
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date', $to_date)
            ->bindValue(':school_id', $sch)
            ->queryAll();

//        $matching = Yii::$app->db->createCommand('
//select DISTINCT(pr.channel_trans_id),pr.amount,pr.reciept_number,pr.student_id, pr.date_created, pr.channel_memo, concat(si.last_name,\' \',si.first_name) as std_name,  sch.school_name, cls.class_code from payments_received pr inner JOIN recon_temporary_table rc on pr.channel_trans_id = rc.txn_reference
//
//inner join student_information si on si.id = pr.student_id
//inner join core_school sch on sch.id = pr.school_id
//inner join institution_student_classes cls on cls.id = si.student_class
//where pr.date_created >=:from_date and pr.date_created <= :end_date and pr.school_id=:school_id order by date_created desc;' )
//            ->bindValue(':from_date', $from_date)
//            ->bindValue(':end_date',$to_date)
//            ->bindValue(':school_id',$sch)
//            ->queryAll();


        $matching = Yii::$app->db->createCommand("SELECT DISTINCT
                                    ( pr.channel_trans_id ),
                                    pr.amount,
                                    pr.reciept_number,
                                    pr.student_id,
                                    pr.date_created,
                                    pr.channel_memo,
                                    sch.school_name
                                 
                                FROM
                                    payments_received pr
                                    INNER JOIN 
                                (select * from recon_temporary_table where booking_time >=:from_date AND booking_time <=:end_date and school_id =:school_id ) rc
                                ON pr.channel_trans_id = rc.transaction_id
                               INNER JOIN core_school sch ON sch.ID = pr.school_id

                                WHERE
                                    pr.date_created >=:from_date 
                                    AND pr.date_created <= :end_date 
                                    AND pr.school_id =:school_id 
                                ORDER BY
                                    date_created DESC")
            ->bindValue(':from_date', $from_date)
            ->bindValue(':end_date', $to_date)
            ->bindValue(':school_id', $sch)
            ->queryAll();


        $res = [
            'awashStatementTxns' => $awashStatementTxns,
            'awashSchPay' => $awashSchPay,
            'matching' => $matching,
            'to_date' => $to_date,
            'from_date' => $from_date
        ];

        return $res;



    }


    public static function postPayment($request)
    {
//        "account_no":"1000010002149", "to_date":"11/20/2009", "from_date":"11/10/2009"
//        Yii::trace("From $startDate");
//        Yii::trace("To $startDate");
        $request = ['account_no' => trim($account), 'from_date' => $startDate, 'to_date' => $endDate];

        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport' // only cURL supports the options we need
        ]);

        $baseUrl = Yii::$app->params['BIBCbsBaseUrl'];
        $url = $baseUrl . '/queryAccountStatement';
        $req = json_encode($request);
        Yii::trace('Connecting to  ' . $url . ' to send ' . $req);


        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($req)
            ->setOptions(['sslVerifyPeer' => false,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false])
            ->send();

        $content = $response->getContent();

        if (!$response->isOk) {
            return (object)['responseCode' => 500, 'responseText' => 'Could not connect to BIB statements service. Please try again later. HTTP Code: ' . $response->getStatusCode()];
        }


        $json_decode = json_decode($content);
        Yii::trace($json_decode);
        return $json_decode;
    }

    public function actionBankStatementPdfexl($export)
    {
//        session_start();
        $model = $_SESSION['bank_statement_query'];
        $res = "";
        $error = "";
        $bankCode = "";
        $json = false;
        if ($model->acc) {
            $acc = $this->getThisAccount($model->acc);
            $bank = $this->checkAccountBank($model->acc);
            $bankCode = $bank['bank_code'];
            $sch = (ToWords::isSchoolUser()) ? Yii::$app->user->identity->school_id : $model->school;

            if ($bankCode == 'AWASH') {
                //Take the bib statement route
                if ($acc) {
//                        $res = $this->centeStatement($acc, $model);
                    $recon = $this->reconcile($model, $sch);

                    $result = [
                        'bibStatementTxns' => $recon['bibStatementTxns'],
                        'bibSchPay' => $recon['bibSchPay'],
                        'matching' => $recon['matching'],
                        'to_date' => $recon['to_date'],
                        'from_date' => $recon['from_date'],


                    ];
//                        return $this->centeJSONStatement($acc, $model, $bankCode); //Use json api
                }
            } else {
                $error = $bank['bank_name'] . " does not provide bank statements.";
            }
        }

        $result = ['model' => $model, 'result' => $result, 'error' => $error, 'bankCode' => $bankCode, 'type' => $export, 'json' => $json];
        $html = $this->renderPartial('extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/BIB-Logo-Transparent.jpg.jpg');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'Bank Statement (' . $model->from_date . ' - ' . $model->to_date . ')', 'school_bank_statement', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_bank_recon_statement" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }


    private function awashStatement($acc, $model, $bankCode, $export = false)
    {
        $error = $res = null;
        $response = AwashStatementHelper::getStatement($acc->account_number, $model->from_date, $model->to_date);
        Yii::trace($response);
        if ($response->returnCode == 0) {
            $res = $response->returnObject;
            Yii::trace($res);
            Logs::logEvent("Bank Statement Request for school account (" . $acc->account_number . ")", null, null);
        } else {
            $error = $response->returnMessage;
            Logs::logEvent("Failed Bank Statement Request for school account (" . $acc->account_number . ")", $error, null);

        }
        return ['res' => $res, 'type' => 'json', 'error' => $error];
    }

}

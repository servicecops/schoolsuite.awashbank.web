<?php

namespace app\modules\recon\controllers;

use app\components\AwashCbsHelperHelper;


use app\components\AwashStatementHelper;
use app\components\CentenaryJSONStatementHelper;
use app\components\HousingFinanceStatementHelper;
use app\components\StanbicStatementHelper;
use app\modules\schoolcore\models\CoreBankAccountDetails;
use app\modules\schoolcore\models\CoreSchool;
use Yii;
use app\models\ReplyForm;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Client;
use yii\db\Query;
use yii\helpers\Json;
use yii\base\DynamicModel;
use yii\base\ErrorException;
use app\modules\logs\models\Logs;


class PostingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionCreditPaymentChannel()
    {
        if (Yii::$app->user->can('postings')) {
            $request = Yii::$app->request;
            $model = new DynamicModel(['amount', 'depositDetails', 'paymentChannelId', 'account_id']);
            $model->addRule(['amount', 'depositDetails', 'paymentChannelId', 'account_id'], 'required');
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user_name = Yii::$app->user->identity->username;
                $connection = Yii::$app->db;
                $transaction = $connection->beginTransaction();
                try {
                    $connection->createCommand("select credit_payment_channel(
                        :account_id, 
                        :depositDetails, 
                        :amount, 
                        :user_name)")
                        ->bindValue(':account_id', $model->account_id)
                        ->bindValue(':depositDetails', $model->depositDetails)
                        ->bindValue(':amount', $model->amount)
                        ->bindValue(':user_name', $user_name)
                        ->execute();

                    $transaction->commit();
                    Logs::logEvent("payment channel credited - " . $model->depositDetails . " (" . $model->amount . ")", null, null);
                    $response = "Channel has been credited successfully";
                    return ($request->isAjax) ? $this->renderAjax('posting_response', ['response' => $response]) : $this->render('posting_response', ['response' => $response]);
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Logs::logEvent("Failed to credit paynement channel", $error, null);
                    \Yii::$app->session->setFlash('actionFailed', $error);
                    return \Yii::$app->runAction('/student/error');
                }

            } else
                return ($request->isAjax) ? $this->renderAjax('feesform', ['model' => $model]) :
                    $this->render('feesform', ['model' => $model]);
        } else {
            throw new ForbiddenHttpException('No permissions to trigger this service.');
        }

    }

    public function actionPaymentchannelAccs($id)
    {

        $main_pc = (new Query())->select(['pgl.description', 'pgl.balance', 'pgl.id'])->from('payment_channels pc')->innerJoin('payment_channel_account_gl pgl', 'pc.payment_channel_account_id=pgl.id')->where(['pc.id' => $id])->all();

        $sec_pc = (new Query())->select(['pgl.description', 'pgl.balance', 'nb.bank_name', 'pgl.id'])->from('payment_channel_secondary_accounts sec')->innerJoin('payment_channel_account_gl pgl', 'pgl.id=sec.account_id')->innerJoin('core_nominated_bank nb', 'nb.id=sec.bank_id')->where(['sec.payment_channel' => $id])->all();
        $result = "<thead><tr>#<th>&nbsp;</th><th>Description</th><th>Balance</th><th>Bank</th></tr></thead>";
        foreach ($main_pc as $k => $v) {
            $result .= "<tr><td><input type='radio' name='DynamicModel[account_id]' id='" . 'radio' . $v['id'] . "' value='" . $v['id'] . "'><label for='" . 'radio' . $v['id'] . "'>&nbsp;</label></td><td>" . $v['description'] . " </td><td>" . $v['balance'] . "</td><td></td></tr>";
        }
        foreach ($sec_pc as $k => $v) {
            $result .= "<tr><td><input type='radio' name='DynamicModel[account_id]' id='" . 'radio' . $v['id'] . "' value='" . $v['id'] . "'><label for='" . 'radio' . $v['id'] . "'>&nbsp;</label></td><td>" . $v['description'] . " </td><td>" . $v['balance'] . "</td><td>" . $v['bank_name'] . "</td></tr>";
        }

        return $result;

    }

    public function actionSchoolPayout()
    {
        if (Yii::$app->user->can('postings')) {
            $request = Yii::$app->request;
            $model = new DynamicModel(['amount', 'instructionDate', 'payoutBankDetails', 'payoutBank', 'schoolId']);
            $model->addRule(['amount', 'instructionDate', 'payoutBank', 'schoolId'], 'required');

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $client = new Client();
                $user = Yii::$app->user->identity->username;

//            $response = Json::decode($response->content);
//            return ($request->isAjax) ? $this->renderAjax('posting_response', ['response'=>$response]) :
//                    $this->render('posting_response', ['response'=>$response]);
            } else
                return ($request->isAjax) ? $this->renderAjax('schpayout_form', ['model' => $model]) :
                    $this->render('schpayout_form', ['model' => $model]);
        } else {
            throw new ForbiddenHttpException('No permissions to trigger this service.');
        }
    }

    public function actionStatement()
    {
        if(Yii::$app->user->can('own_sch')){
            $enabled = CoreSchool::find()
                ->where(['id'=>Yii::$app->user->identity->school_id])
                ->limit(1)->one()->enable_bank_statement;
            if(!$enabled){
                \Yii::$app->session->setFlash('actionFailed', "BANK STATEMENT IS NOT ENABLED FOR THIS SCHOOL. Please contact SchoolPay admin");
                return \Yii::$app->runAction('/student/error');
            }
        }
        if (Yii::$app->user->can('view_statement')) {
            $request = Yii::$app->request;
            $res = "";
            $error = "";
            $bankCode = "";
            $type = "array"; //The result type
            $model = new DynamicModel(['school', 'acc', 'from_date', 'to_date']);
            $model->addRule(['acc', 'from_date', 'to_date'], 'required')
                ->addRule(['school'], 'safe');
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $sch = (Yii::$app->user->can('own_sch')) ? Yii::$app->user->identity->school_id : $model->school;
                $acc = $this->getThisAccount($model->acc);
                $bank = $this->checkAccountBank($model->acc);
                $bankCode = $bank['bank_code'];
                if ($bankCode == 'AWASH') {
                    if ($acc) {
                        $processedRes = $this->awashStatement($acc, $model, $bankCode);

                        if ($processedRes['res']) $res = json_decode($processedRes['res']);
                        if ($processedRes['error']) $error = $processedRes['error'];
                        if ($processedRes['type']) $type = $processedRes['type'];

                    }
                } else {
                    $error = $bank['bank_name'] . " does not provide bank statements.";
                }
                Yii::trace($res);
            }
            $result = ['model' => $model, 'res' => $res, 'error' => $error, 'bankCode' => $bankCode, 'type' => $type];
            return ($request->isAjax) ? $this->renderAjax('bank_statement', $result) : $this->render('bank_statement', $result);

        } else {
            throw new ForbiddenHttpException('No permissions to trigger this service.');
        }
    }


    private function awashStatement($acc, $model, $bankCode, $export = false)
    {
        $error = $res = null;
        $response = AwashStatementHelper::getStatement($acc->account_number, $model->from_date, $model->to_date);
        Yii::trace($response);
        if ($response->returnCode == 0) {
            $res = $response->returnObject;
            Yii::trace($res);
            Logs::logEvent("Bank Statement Request for school account (" . $acc->account_number . ")", null, null);
        } else {
            $error = $response->returnMessage;
            Logs::logEvent("Failed Bank Statement Request for school account (" . $acc->account_number . ")", $error, null);

        }
        return ['res' => $res, 'type' => 'json', 'error' => $error];
    }

    public function actionBankStatementPdfexl($export)
    {
        $model = $_SESSION['bank_statement_query'];
        Yii::trace($model);
        $res = "";
        $error = "";
        $bankCode = "";
        $json = false;
        if ($model->acc) {
            $acc = $this->getThisAccount($model->acc);
            $bank = $this->checkAccountBank($model->acc);
            $bankCode = $bank['bank_code'];
            if ($bankCode == 'AWASH') {
                //Take the centenary statement route
                if ($acc) {
//                    $res = $this->centeStatement($acc, $model);
                    $processedRes = $this->awashStatement($acc, $model, $bankCode);
                    if ($processedRes['res']) $res = json_decode($processedRes['res']);
                    if ($processedRes['error']) $error = $processedRes['error'];
                    if ($processedRes['type'] && ($processedRes['type'] == 'json')) $json = true;
                }
            }else {
                $error = $bank['bank_name'] . " does not provide bank statements.";
            }
        }

        $result = ['model' => $model, 'res' => $res, 'error' => $error, 'bankCode' => $bankCode, 'type' => $export, 'json' => $json];
        $html = $this->renderPartial('extract_to_pdf', $result);
        if ($export == 'pdf') {
            ob_clean();
            $watermark = Url::to('web/img/awash_logo2.png');
            return Yii::$app->pdf->exportStatementWithWaterMark(
                'Bank Statement (' . $model->from_date . ' - ' . $model->to_date . ')', 'school_bank_statement', $html, 'web/css/pdf.css', $watermark);
        } else if ($export == 'excel') {
            $fileName = "School_bank_statement" . date('YmdHis') . '.xls';
            $options = ['mimeType' => 'application/vnd.ms-excel'];

            return Yii::$app->excel->exportExcel($html, $fileName, $options);
        } else {
            $model = $_SESSION['bank_statement_query'];
        }
    }

    public function actionAccLists($id)
    {
        $countAccounts = CoreBankAccountDetails::find()
            ->where(['school_id' => $id])
            ->count();

        $accounts = CoreBankAccountDetails::find()
            ->where(['school_id' => $id])
            ->all();

        if ($countAccounts > 0) {
            echo "<option value=''> Select Account </option>";
            foreach ($accounts as $v) {
                echo "<option value='" . $v->id . "'>" . $v->account_number . "</option>";
            }
        } else {
            echo "<option value=''> -- </option>";
        }
    }

    private function getThisAccount($acc)
    {
        $acc = CoreBankAccountDetails::find()->where(['id' => $acc])->limit(1)->one();
        return $acc;
    }

    private function checkBank($sch)
    {
        $query = (new Query())->select(['nb.bank_name', 'nb.bank_code'])->from('core_school sch')->innerJoin('core_nominated_bank nb', 'nb.id=sch.bank_name')->where(['sch.id' => $sch])->limit(1)->one();
        return $query;
    }

    /**
     * @param $account - Numeric, the account id for the bank_account_details_record
     * @return array|record - The bank details id and bank_code
     * Gets the bank details for the supplied account id
     */
    private function checkAccountBank($account)
    {
        $query = (new Query())->select(['nbd.id', 'nbd.bank_code', 'nbd.bank_name', 'sch.school_code'])
            ->from('core_bank_account_details bad')
            ->innerJoin('core_nominated_bank nbd', 'nbd.id=bad.bank_id')
            ->innerJoin('core_school sch', 'sch.id=bad.school_id')
            ->where(['bad.id' => $account])->limit(1)->one();
        return $query;
    }


}

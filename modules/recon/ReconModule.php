<?php
namespace app\modules\recon;

class ReconModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\recon\controllers';
    public function init() {
        parent::init();
    }
}

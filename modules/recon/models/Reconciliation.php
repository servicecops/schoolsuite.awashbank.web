<?php

namespace app\modules\recon\models;

use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;

/**
 * LoginForm is the model behind the login form.
 */
class Reconciliation extends \yii\db\ActiveRecord
{

    /**
     * This is the model class for table "school_information".
     *
     * @property integer $id
     * @property integer $acct_no
     * @property string $depositor_payee_nm
     * @property string $crncy_cd_iso
     * @property string $dr_cr_ind
     * @property string $origin_bu_id
     * @property string $stmnt_bal
     * @property string $trans_desc
     * @property integer $tran_dt
     * @property string $tran_journal_id
     * @property string $txn_amt
     * @property string $txn_reference
     * @property string $school_id

     */




    public static function tableName()
    {
        return 'recon_temporary_table';
    }



    /**
     * @inheritdoc
     *
    /**
     * @return array the validation rules.
     */

    public function rules()
    {


        return [
            [['acct_no', 'transaction_id','description','currency','school_id','booking_date','amount'], 'string'],

            [['acct_no', 'amount','school_id'], 'integer'],

        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $errMsg = 'Incorrect username or password. This account will lock after the 3rd attempt';
            $user = $this->getUser();
            $errModel = (new Query())->select(['locked'])->from('web_console_users')->where(['username' => strtolower($this->username)])->limit(1)->one();
            if ($errModel['locked'] == true) {
                $errMsg = "This Account has been Locked. Contact SchoolPay on email info@berhanbanksc.com";
            }
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, $errMsg);
                return;
            }
            if ($user->school_id != null) {
                $school = SchoolInformation::find()->where(['id' => $user->school_id])
                    ->limit(1)->one();
                Yii::trace($school);
                if (!$school->active) {
                    $this->addError($attribute, "The associated school is not enabled. Contact the schoolpay administrator for assistance");
                    return;
                }
            }


        }
    }

    /**
     * hason     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        $user = $this->getUser();
        if ($this->validate()) {
            $user->scenario = 'count';
            $user->incorrect_access_count = 0;
            $user->save(false);
            return $user;
        } else {
            $user = $this->getUser();
            if ($user !== null) {
                $user->scenario = 'count';
                $user->incorrect_access_count += 1;
                if ($user->incorrect_access_count >= 3) {
                    $user->locked = true;
                    $user->incorrect_access_count = 0;
                }
                $user->save(false);
            }
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername(strtolower($this->username));
        }

        return $this->_user;
    }
}

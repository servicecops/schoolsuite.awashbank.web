<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$_SESSION['bank_statement_query'] = $model;
$this->title = 'Bank Statement';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row student-index hd-title" data-title="Bank Statement">


    <div class="col-md-12">
        <div class="col-md-3 col-xs-12 no-padding"><span style="font-size:22px;">&nbsp;<i class="fa fa-th-list"></i> Statements</span>
        </div>

        <div class="col-md-6 col-xs-12 no-padding"><?php echo $this->render('_account_search', ['model' => $model]); ?></div>
        <div class="col-md-3 col-xs-12 no-padding">
            <ul class="menu-list pull-right">
                <li>
                    <?= Html::a('<i class="fa fa-file-excel-o"></i> &nbsp;Excel', ['/recon/postings/bank-statement-pdfexl', 'export' => 'excel'], array('title' => 'Export to Excel', 'target' => '_blank', 'class' => 'btn btn-info btn-sm')); ?>
                </li>
                <li>
                    <?= Html::a('<i class="fa fa-file-pdf-o"></i> PDF', ['/recon/postings/bank-statement-pdfexl', 'export' => 'pdf'], ['title' => 'Export to PDF', 'target' => '_blank', 'class' => 'btn btn-danger btn-sm']); ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table">
                <thead>
                <tr>
                    <th>Effective Date</th>
                    <th>Description</th>
                    <th>Bank Reference</th>
                    <th>Amount</th>
                    <th>Closing</th>
                </tr>
                </thead>
                <tbody>
                <?php



                if ($res) :
                    $rows = ['res' => $res, 'type' => $type];
                    if ($bankCode == 'AWASH') :
                        echo $this->render('recon_summary', ['result'=>$result, 'res'=>$res]);

                    else :
                        echo '<tr><td colspan="7" id="no_bank_statement"><span style="color:red;">' . $error . '</span></td></tr>';
                    endif;
                elseif ($error) :

                    ?>
                    <tr>
                        <td  ><span style="color:red;"><?= $error ?></span></td>
                    </tr>
                <?php else:

                    ?>

                    <tr>

                        <td colspan="7" id="no_bank_statement">No transactions found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>




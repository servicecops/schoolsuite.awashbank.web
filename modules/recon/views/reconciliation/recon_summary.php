<?php


use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\web\Controller;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Statement Reconciliation Summary';
?>
<br>

<div class="row card" data-title="Attendance Summary">




<?php
$awashStatementTxns =$result['awashStatementTxns'];
$awashSchPay =$result['awashSchPay'];


?>


    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">
                <table class="table table-striped">
                    <thead>
                    <?php
                    $x=1;
                    ?>
                    <h3>Transactions on Bank Statement but not on Schoolpay</h3>


                    <tr>
                        <th></th>

                        <th class='clink'>Transaction Date</th>
                        <th class='clink'>Account Number</th>
                        <th class='clink'>Transaction ID</th>
                        <th class='clink'>Transaction Description</th>
                        <th class='clink'>Currency</th>
                        <th class='clink'>Amount</th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if ($awashStatementTxns) :
                        foreach ($awashStatementTxns as $data) : ?>
                            <tr data-key="0">
                                <td><?php echo $x++?></td>

                                <td>
                                    <?= ($data['booking_time']) ? (date('d-M-Y', strtotime($data['booking_time'])))  : '<span class="not-set"> -- </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['acct_no']) ? $data['acct_no'] : '<span class="not-set"> -- </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['transaction_id']) ? $data['transaction_id'] : '<span class="not-set"> -- </span>' ?>
                                </td>

                                <td>
                                    <?= ($data['description']) ? $data['description'] : '<span class="not-set"> --  </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['currency']) ? $data['currency'] : '<span class="not-set"> --  </span>' ?>
                                </td>

                                <td>
                                    <?= ($data['amount']) ? $data['amount'] : '<span class="not-set"> --  </span>' ?>
                                </td>







                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No data found </td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="box-body table table-responsive no-padding">
                <h3>Transactions on SchoolPay but not on Bank Statement</h3>
                <table class="table table-striped">
                    <thead>
                    <?php
                    $x=1;
                    ?>
                    <tr>
                        <th></th>

                        <th class='clink'>Date Created</th>
                        <th class='clink'>Amount</th>
                        <th class='clink'>Receipt Number</th>
                        <th class='clink'>Channel Trans ID</th>
                        <th class='clink'>Channel Memo</th>
                        <th class='clink'>School Name</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if ($awashSchPay) :

                        foreach ($awashSchPay as $data) : ?>
                            <tr data-key="0">
                                <td><?php echo $x++?></td>

                                <td>
                                    <?= ($data['date_created']) ? (date('d-M-Y', strtotime($data['date_created']))) : '<span class="not-set"> -- </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['amount']) ? $data['amount'] : '<span class="not-set"> -- </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['reciept_number']) ? $data['reciept_number'] : '<span class="not-set"> --  </span>' ?>
                                </td>

                                <td>
                                    <?= ($data['channel_trans_id']) ? $data['channel_trans_id'] : '<span class="not-set"> --  </span>' ?>
                                </td>

                                <td>
                                    <?= ($data['channel_memo']) ? $data['channel_memo'] : '<span class="not-set"> --  </span>' ?>
                                </td>
                                <td>
                                    <?= ($data['school_name']) ? $data['school_name'] : '<span class="not-set"> --  </span>' ?>
                                </td>



                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No data found </td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>



        </div>
    </div>




<?php
$bibStatementTxns =$result['bibStatementTxns'];
$bibSchPay =$result['bibSchPay'];


?>
<div class="col-md-12">
        <table class="table">
        <?php if($type=='excel') : ?>
            <tr><td colspan="7">STATEMENT RECONCILIATION (<?= $model->from_date?> - <?= $model->to_date ?>)</td><tr>
        <?php endif; ?>
        <thead>
        <tr>
        	<th></th>

        </tr>
        </thead>
        <tbody>

        </tbody>
        </table>
    <table class="table table-striped">
        <thead>
        <?php
        $x=1;
        ?>
        <h3>Transactions on BankStatement but not on ESchool</h3>

        <tr>
            <th></th>

            <th class='clink'>Transaction Date</th>
            <th class='clink'>Account Number</th>
            <th class='clink'>Depositors Payee </th>
            <th class='clink'>Statement Balance</th>
            <th class='clink'>Transaction Description</th>
            <th class='clink'>Transaction Journal ID</th>
            <th class='clink'>Amount</th>
            <th class='clink'>Transaction Reference</th>


        </tr>
        </thead>
        <tbody>
        <?php

        if ($bibStatementTxns) :

            Yii::trace($bibStatementTxns);
            foreach ($bibStatementTxns as $data) : ?>
                <tr data-key="0">
                    <td><?php echo $x++?></td>

                    <td>
                        <?= ($data['tran_dt']) ? (date('d-M-Y', strtotime($data['tran_dt']))) : '<span class="not-set"> -- </span>' ?>
                    </td>
                    <td>
                        <?= ($data['acct_no']) ? $data['acct_no'] : '<span class="not-set"> -- </span>' ?>
                    </td>
                    <td>
                        <?= ($data['depositor_payee_nm']) ? $data['depositor_payee_nm'] : '<span class="not-set"> -- </span>' ?>
                    </td>

                    <td>
                        <?= ($data['stmnt_bal']) ? $data['stmnt_bal'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['tran_desc']) ? $data['tran_desc'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['tran_journal_id']) ? $data['tran_journal_id'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['txn_amt']) ? $data['txn_amt'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['txn_reference']) ? $data['txn_reference'] : '<span class="not-set"> --  </span>' ?>
                    </td>






                </tr>
            <?php endforeach;
        else :?>
            <tr>
                <td colspan="8">No data found </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>
<div class="box-body table table-responsive no-padding">
    <h3>Transactions on SchoolPay but not on Bank Statement</h3>
    <table class="table table-striped">
        <thead>
        <?php
        $x=1;
        ?>
        <tr>
            <th></th>

            <th class='clink'>Date Created</th>
            <th class='clink'>Amount</th>
            <th class='clink'>Receipt Number</th>
            <th class='clink'>Channel Trans ID</th>
            <th class='clink'>Student Name</th>
            <th class='clink'>Channel Memo</th>
            <th class='clink'>School Name</th>
            <th class='clink'>Class Name</th>


        </tr>
        </thead>
        <tbody>
        <?php

        if ($bibSchPay) :

            Yii::trace($bibSchPay);
            foreach ($bibSchPay as $data) : ?>
                <tr data-key="0">
                    <td><?php echo $x++?></td>

                    <td>
                        <?= ($data['date_created']) ? (date('d-M-Y', strtotime($data['date_created']))) : '<span class="not-set"> -- </span>' ?>
                    </td>
                    <td>
                        <?= ($data['amount']) ? $data['amount'] : '<span class="not-set"> -- </span>' ?>
                    </td>
                    <td>
                        <?= ($data['reciept_number']) ? $data['reciept_number'] : '<span class="not-set"> --  </span>' ?>
                    </td>

                    <td>
                        <?= ($data['channel_trans_id']) ? $data['channel_trans_id'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['std_name']) ? $data['std_name'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['channel_memo']) ? $data['channel_memo'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['school_name']) ? $data['school_name'] : '<span class="not-set"> --  </span>' ?>
                    </td>
                    <td>
                        <?= ($data['class_code']) ? $data['class_code'] : '<span class="not-set"> --  </span>' ?>
                    </td>


                </tr>
            <?php endforeach;
        else :?>
            <tr>
                <td colspan="8">No data found </td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

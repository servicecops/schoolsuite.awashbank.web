<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\SchoolInformation;
use kartik\select2\Select2;
use app\modules\banks\models\BankAccountDetails;
use yii\jui\DatePicker;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentsReceivedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['fetch-statement'],
        'method' => 'post',
        'options'=>['id'=>'bank_statement_form'],
    ]); ?>

    <?php if(\Yii::$app->user->can('schoolpay_admin')) : ?>
        <div class="col-md-3 no-padding">
            <?php
            $url = Url::to(['/schoolcore/core-school/schoollist']);
            $selectedSchool = empty($model->school) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->school)->school_name;
            echo $form->field($model, 'school')->widget(Select2::classname(), [
                'initValueText' => $selectedSchool, // set the initial display text
                'size'=>'sm',
                'theme'=>Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Filter School',
                    'id'=>'statement_school_selected_id',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],

                ],
            ])->label(false); ?>
        </div>
        <div class="col-md-2 no-padding"><?= $form->field($model, 'acc', ['inputOptions'=>[ 'class'=>'form-control input-sm']])->dropDownList(
                [''=>'Select Account'])->label(false) ?></div>
    <?php elseif(\app\components\ToWords::isSchoolUser()) : ?>
        <div class="col-md-3 no-padding"><?= $form->field($model, 'acc')->dropDownList(
                ArrayHelper::map(\app\modules\schoolcore\models\CoreBankAccountDetails::find()->where(['school_id'=>Yii::$app->user->identity->school_id])->all(), 'id', 'account_number'), [''=>'Select Account', 'class'=>'form-control input-sm']
            )->label(false) ?></div>
    <?php endif; ?>

    <div class="col-md-3 no-padding">
        <?= $form->field($model, 'from_date')->widget(DatePicker::className(),
            [
                'model' => $model,
                'attribute' => 'from_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'From Date'],
                'clientOptions' =>[
                    'class'=>'form-control',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=>'1900:'.(date('Y')+1),
                    'autoSize'=>true,
                ],
            ])->label(false) ?>
    </div>

    <div class="col-md-3 no-padding">
        <?= $form->field($model, 'to_date')->widget(DatePicker::className(),
            [
                'model' => $model,
                'attribute' => 'to_date',
                'dateFormat' => 'yyyy-MM-dd',
                'options'=>['class'=>'form-control input-sm', 'placeHolder'=>'To Date'],
                'clientOptions' =>[
                    'class'=>'form-control',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=>'1900:'.(date('Y')+1),
                    'autoSize'=>true,
                ],
            ])->label(false) ?>
    </div>

    <div class="col-md-1 no-padding"><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
    <div class="col-md-12 " id="has-error-date_overflow"></div>
    <?php ActiveForm::end(); ?>

</div>

<?php
$url = Url::to(['/recon/postings/acc-lists']);
$acc = $model->acc;
$sch = $model->school;
$script = <<< JS
$("document").ready(function(){ 
    var sch = '$sch';
    if(sch){
        $.get('$url', {id : sch}, function( data ) {
            $('select#dynamicmodel-acc').html(data);
            $('select#dynamicmodel-acc').val('$acc');
        });
    }
    $('body').on('change', '#statement_school_selected_id', function(){

        $.get('$url', {id : $(this).val()}, function( data ) {
            $('select#dynamicmodel-acc').html(data);
            $('select#dynamicmodel-acc').val('$acc');
        });
    });

    $('#bank_statement_form').on('beforeSubmit', function(e){
        var uri = $(this).attr('action');
        var formdata = $(this).serialize();
        var method = $(this).attr('method');

        var from_date = $('input#dynamicmodel-from_date').val();
        var end_date = $('input#dynamicmodel-to_date').val();
        var oneDay = 24*60*60*1000;
        var firstDate = new Date(from_date);
        var secondDate = new Date(end_date);
        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
        if(diffDays > 30){
          $('#has-error-date_overflow').html('<span style="color:red;">Period Should not exceed 30 days.</span>');
          return false;
        } else {
            $.ajax({
                  url: uri,
                  type: method,
                  async: true,
                  data: formdata,
                   beforeSend: function(){
                    $("#loader").addClass("modal-loading"); 
                  },
                  complete: function(){
                      $("#loader").removeClass("modal-loading");
                  },
                  success: function (data) {
                    $("div#view_content").html(data);
                        var title = $('.hd-title').attr('data-title');
                        $("#title-header").html(title);
                        document.title = typeof title !=='undefined' ? title : 'School Pay';
                        return false;
                  },
                  error: function (xhr, ajaxOptions, thrownError) {
                    $("td#no_bank_statement").html('<span style="color:red;">Unable to connect to the server. Try again later</span>');
                    return false;
                  }
              });
        }
        return false;
    });
});
JS;
$this->registerJs($script);
?>

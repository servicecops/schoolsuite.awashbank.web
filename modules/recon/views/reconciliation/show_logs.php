<?php


use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\web\Controller;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Statement Reconciliation Summary';
?>
<br>

<div class="row card" data-title="Attendance Summary">



<?php
if($data):
?>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table table-responsive no-padding">


                <table class="table table-striped">
                    <thead>
                    <?php
                    $x=1;
                    ?>
                    <h3>Logs from posting transactions from Bank statement</h3>
                    <div class="pull-right">


                        <button class="btn btn-sm btn-info" onclick="clink('<?= Url::to(['/reconciliation/fetch-statement']) ?>')">Back</button>

                    </div>

                    <tr>

                        <th class='clink'>#</th>
                        <th class='clink'>Message</th>
                        <th class='clink'>Affected Student</th>



                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if ($logs) :
                        foreach ($logs as $k=>$v) : ?>
                            <tr data-key="0">
                                <td><?php echo $x++?></td>


                                <td>
                                    <?= ($v['message']) ? $v['message'] : '<span class="not-set"> -- </span>' ?>
                                </td>
                                <td>
                                    <?= ($v['affectedStudent']) ? $v['affectedStudent'] : '<span class="not-set"> -- </span>' ?>
                                </td>




                            </tr>
                        <?php endforeach;
                    else :?>
                        <tr>
                            <td colspan="8">No Logs found </td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>




        </div>
    </div>

   <?php else :?>

        <p colspan="8">No Valid student fee transaction data was found </p>

<?php endif; ?>

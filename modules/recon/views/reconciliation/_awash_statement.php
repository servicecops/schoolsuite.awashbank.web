<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Mark
 * Date: 9/28/17
 * Time: 8:31 PM
 */

//{"txn_amt":140000,"tran_dt":"16-Nov-2009","stmnt_bal":142000,"dr_cr_ind":"CR","tran_desc":"101 - Cash Deposit",
//    "acct_no":"1000010002149","origin_bu_id":641,"tran_journal_id":15254,"depositor_payee_nm":null,"crncy_cd_iso":"ETB",
//    "channel_desc":"Cash Operations"}

?>
<?php if (isset($res->items)) { ?>
    <?php foreach ($res->items as $k => $v) : ?>
        <tr>
            <td><?= $v->bookingDateTime ?></td>
            <td><?= $v->description ?></td>
            <td><?= $v->transactionId ?></td>
            <td><?= ($v->creditDebitIndicator == 'CREDIT') ? number_format($v->amount->amount, 2) : '<span style="color:red;"> -' . number_format($v->amount->amount, 2) . '</span>' ?></td>
            <td><?= number_format(0, 2) ?></td>
        </tr>
    <?php endforeach; ?>
<?php } ?>


<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\PaymentChannels;

/* @var $this yii\web\View */
/* @var $model app\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-form hd-title" data-title="Credit Payment Channel">
    <?php $form = ActiveForm::begin(); ?>
    <div class="letter">
    <div class="row">
    <div class="col-xs-12 col-lg-12 no-padding">
        <div class="col-xs-12 col-sm-6 ">
        <?= $form->field($model, 'paymentChannelId')->dropDownList(
        ArrayHelper::map(PaymentChannels::find()->all(), 'id', 'channel_name'), [ 'id'=>'payment_channel_select', 'prompt'=>'Select Channel']
        )->label('') ?>
        </div>
    </div>
    <div class="col-xs-12">
    <table id="returned-accounts" class="table"></table>
     <div style="color:red;margin-left: -20px;"> <?= $form->errorSummary($model, ['header' => '']); ?></div>
    </div>





    <div class="col-xs-12 col-lg-12 no-padding">
    	
    	<div class="col-sm-6">
    	<?= $form->field($model, 'amount', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Amount'] ])->textInput()->label('') ?>
        </div>
        <div class="col-sm-6">
        <?= $form->field($model, 'depositDetails', ['inputOptions'=>[ 'class'=>'form-control', 'placeholder'=> 'Deposit Details'] ])->textInput()->label('') ?>
        </div>

   </div>


    <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
    <div class="col-xs-6">
        <?= Html::submitButton('Send', ['class' => 'btn btn-block btn-primary']) ?>
    </div>
    <div class="col-xs-6">
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
    </div>
    </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

<?php 
$url = Url::to(['/postings/paymentchannel-accs']);
$script = <<< JS
$("document").ready(function(){ 
    var va = $("#payment_channel_select").val();
    if(va){
        $.get('$url', {id : va}, function( data ) {
                    $('table#returned-accounts').html(data);
                });
    }
    $("#payment_channel_select").change(function(){
        $.get('$url', {id : $(this).val()}, function( data ) {
                    $('table#returned-accounts').html(data);
                });
    });
    
  });
JS;
$this->registerJs($script);
?>
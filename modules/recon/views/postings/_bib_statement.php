<?php
/**
 * Created by IntelliJ IDEA.
 * User: John Mark
 * Date: 9/28/17
 * Time: 8:31 PM
 */

//{"txn_amt":140000,"tran_dt":"16-Nov-2009","stmnt_bal":142000,"dr_cr_ind":"CR","tran_desc":"101 - Cash Deposit",
//    "acct_no":"1000010002149","origin_bu_id":641,"tran_journal_id":15254,"depositor_payee_nm":null,"crncy_cd_iso":"ETB",
//    "channel_desc":"Cash Operations"}

?>

<?php foreach ($res as $k => $v) : ?>
    <tr>
        <td><?= $v->tran_dt ?></td>
        <td><?= $v->tran_desc ?></td>
        <td><?= $v->tran_journal_id ?></td>
        <td><?= ($v->dr_cr_ind == 'CR') ? number_format($v->txn_amt, 2) : '<span style="color:red;"> -' . number_format($v->txn_amt, 2) . '</span>' ?></td>
        <td><?= number_format($v->stmnt_bal, 2) ?></td>
    </tr>
<?php endforeach; ?>



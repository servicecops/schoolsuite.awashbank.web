<div class="col-xs-12">
        <table class="table">
        <?php if($type=='excel') : ?>
            <tr><td colspan="7">BANK STATEMENT (<?= $model->from_date?> - <?= $model->to_date ?>)</td><tr>
        <?php endif; ?>
        <thead>
        <tr>
        	<th>Effective Date</th>
        	<th>Description</th>
        	<th>Bank Reference</th>
        	<th>Amount</th>
        	<th>Closing</th>
        </tr>
        </thead>
        <tbody>
            <?php
            if($res) :
                $rows = ['res' => $res];
                if($bankCode =='AWASH') :
                    echo $this->render('_awash_statement', $rows);
                else :
                    echo '<tr><td colspan="7" id="no_bank_statement"><span style="color:red;">'. $error. '</span></td></tr>';
                endif;
            else :?>
            <tr><td colspan="7">No transactions found</td><tr>
        <?php endif; ?>
        </tbody>
        </table>
</div>

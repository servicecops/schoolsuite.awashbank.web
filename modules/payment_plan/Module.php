<?php

namespace app\modules\payment_plan;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\payment_plan\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
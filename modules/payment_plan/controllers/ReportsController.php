<?php

namespace app\modules\payment_plan\controllers;

use app\modules\payment_plan\models\Reports;
use Yii;

class ReportsController extends DefaultController
{
    public function actionOutstanding()
    {
        $this->grantAccess('plan', 'index');
        $searchModel = new Reports();
        $request = Yii::$app->request;
        $data = $searchModel->searchOutstanding(Yii::$app->request->queryParams);

        $template = "plans_report_summary";
        $res = ['searchModel' => $searchModel,'data' => $data, 'mod'=>'plan'];
//        return print_r($data);
        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }

    public function actionSubscribers($id)
    {
        $this->grantAccess('plan', 'index');
        $searchModel = new Reports();
        $searchModel->payment_plan_id = $id;
        $request = Yii::$app->request;
        $data = $searchModel->searchSubscribers(Yii::$app->request->queryParams);

        $template = "@app/views/common/the_index";
        $res = ['searchModel' => $searchModel,'data' => $data, 'mod'=>'plan'];

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }
    public function actionCancelledSubscribers($id)
    {
        $this->grantAccess('plan', 'index');
        $searchModel = new Reports();
        $searchModel->payment_plan_id = $id;
        $request = Yii::$app->request;
        $data = $searchModel->searchCancelledSubscribers(Yii::$app->request->queryParams);

        $template = "@app/views/common/the_index";
        $res = ['searchModel' => $searchModel,'data' => $data, 'mod'=>'plan'];

        return ($request->isAjax) ? $this->renderAjax($template, $res) : $this->render($template, $res);
    }
}

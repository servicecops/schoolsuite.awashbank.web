<?php

namespace app\modules\payment_plan\models;

use Yii;

/**
 * This is the model class for table "payment_plan_logs".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $ip_address
 * @property string $event
 * @property string $event_date
 * @property boolean $error
 */
class PlanLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_plan_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_plan_id', 'student_id'], 'integer'],
            [['ip_address', 'event'], 'string'],
            [['event_date', 'payment_plan_id', 'student_id'], 'safe'],
            [['error'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'ip_address' => 'Ip Address',
            'event' => 'Event',
            'event_date' => 'Event Date',
            'error' => 'Error',
        ];
    }

    public static function logEvent($event, $payment_plan=null, $student_id=null, $error=false){
        $log = new PlanLogs();
        $log->event = $event;
        $log->user_id = Yii::$app->user->id;
        $log->ip_address = Yii::$app->request->userIP;
        $log->event_date = date('Y-m-d H:i:s');
        $log->error = $error ? $error : null;
        $log->payment_plan_id = $payment_plan ? $payment_plan : null;
        $log->student_id = $student_id ? $student_id : null;
        $log->save();
    }
}

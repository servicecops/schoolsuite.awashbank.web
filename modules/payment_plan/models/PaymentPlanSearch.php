<?php

namespace app\modules\payment_plan\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
if (!Yii::$app->session->isActive) session_start();

/**
 * PaymentPlanSearch represents the model behind the search form about `app\modules\payment_plan\models\PaymentPlan`.
 */
class PaymentPlanSearch extends PaymentPlan
{
    public $date_from, $date_to, $student_id, $fee_id, $payment_plan_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number_of_installments', 'schedule_1_percentage', 'schedule_2_percentage', 'schedule_3_percentage', 'schedule_4_percentage', 'schedule_5_percentage', 'schedule_6_percentage', 'schedule_7_percentage', 'schedule_8_percentage', 'schedule_9_percentage', 'schedule_10_percentage', 'school_id'], 'integer'],
            [['date_from', 'date_to', 'payment_plan_term', 'description', 'payment_plan_id', 'fee_id', 'student_id'], 'safe'],
            [['fixed_amount_per_payment'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        if(\app\components\ToWords::isSchoolUser()){
            $this->school_id = Yii::$app->user->identity->school_id;
        }

        $query->select(['pln.id', 'pln.date_created', 'payment_plan_term', 'fixed_amount_per_payment', 'description', 'number_of_installments', 'sch.school_name'])
            ->from('payment_plan pln')
            ->innerJoin('core_school sch', 'sch.id=pln.school_id');
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'fixed_amount_per_payment' => $this->fixed_amount_per_payment,
            'number_of_installments' => $this->number_of_installments,
            'school_id' => $this->school_id])
            ->andFilterWhere(['>=', 'pln.date_created', $this->date_from])
            ->andFilterWhere(['<=', 'pln.date_created', $this->date_to]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'date_created', 'payment_plan_term', 'fixed_amount_per_payment', 'description', 'number_of_installments', 'school_name'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pln.id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Payment Plan', $query, $this->getVisibleCols(),  'payment_plan');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Payment Plan', 'search_col_w'=>[2, 7, 3]];
    }

    public function searchCancelled($params)
    {
        $this->load($params);
        $query = new Query();
        if(\app\components\ToWords::isSchoolUser()){
            $this->school_id = Yii::$app->user->identity->school_id;
        }

        $query->select(['pln.id', 'pln.date_created', 'payment_plan_term', 'fixed_amount_per_payment', 'description', 'number_of_installments', 'sch.school_name'])
            ->from('payment_plan pln')
            ->innerJoin('core_school sch', 'sch.id=pln.school_id');
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'fixed_amount_per_payment' => $this->fixed_amount_per_payment,
            'number_of_installments' => $this->number_of_installments,
            'school_id' => $this->school_id,

            ])
            ->andFilterWhere(['>=', 'pln.date_created', $this->date_from])
            ->andFilterWhere(['<=', 'pln.date_created', $this->date_to]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'date_created', 'payment_plan_term', 'fixed_amount_per_payment', 'description', 'number_of_installments', 'school_name'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pln.id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Payment Plan', $query, $this->getVisibleCols(),  'payment_plan');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Payment Plan', 'search_col_w'=>[2, 7, 3]];
    }



    public function searchStudentPlans($params)
    {
        $this->load($params);
        $query = new Query();

        $query->select(['pla.id','pl.description as plan_desc', 'fd.description as fee_desc', 'fd.due_amount'])
            ->from(['payment_plan_fee_student_association pla'])
            ->innerJoin('payment_plan pl', 'pl.id=pla.payment_plan_id')
            ->innerJoin('institution_fees_due fd', 'fd.id=pla.fee_id')
            ->andFilterWhere(['pla.student_id'=>$this->student_id])
            ->andFilterWhere(['>=', 'fd.end_date', date('Y-m-d')]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pla.id');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Student Payment Plan', $query, $this->getPlanStudentsCols(),  'student_payment_agreements');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getPlanStudentsCols(), 'searchCols'=>[], 'title'=>'Student Payment Agreements', 'search_col_w'=>[2, 7, 3]];
    }

    public function getPlanStudentsCols()
    {
        return [
            'plan_desc' => ['label'=>'Payment Plan'],
            'fee_desc' => ['label'=>'Fee'],
            'due_amount' => ['type'=>'number', 'label'=>'Amount'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'/plan/default/agreement', 'title'=>'Agreement', 'icon'=>'fa fa-print', 'options'=>"target='_blank'", 'class'=>''],
                ]
            ],
        ];
    }

    public function searchPlanSchedule($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();

        $query->select(['id', 'schedule_date', 'total_fee_amount', 'total_schedule_amount', 'schedule_balance', 'description', 'schedule_paid_so_far'])
            ->from('payment_plan_student_payment_schedule');
        $query->andFilterWhere([
            'student_id' => $this->student_id,
            'fee_id' => $this->fee_id,
            'cancelled'=>false,
            'payment_plan_id' => $this->payment_plan_id,
        ]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'schedule_date', 'total_fee_amount', 'total_schedule_amount', 'schedule_balance', 'description', 'schedule_paid_so_far'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Payment Schedule', $query, $this->getScheduleCols(),  'payment_schedule');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getScheduleCols(), 'searchCols'=>[], 'title'=>'Payment Schedule', 'search_col_w'=>[2, 7, 3]];
    }

    public function searchCancelledPlanSchedule($params)
    {
        $this->load($params);
        $today = date('Y-m-d', time()+86400);
        $date_to = $this->date_to ? date('Y-m-d', strtotime($this->date_to . ' +1 day')) : $today;
        $query = new Query();

        $query->select(['id', 'schedule_date', 'total_fee_amount', 'total_schedule_amount', 'schedule_balance', 'description', 'schedule_paid_so_far'])
            ->from('payment_plan_student_payment_schedule');
        $query->andFilterWhere([
            'student_id' => $this->student_id,
            'fee_id' => $this->fee_id,
            'cancelled'=>true,
            'payment_plan_id' => $this->payment_plan_id,
        ]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                'id', 'schedule_date', 'total_fee_amount', 'total_schedule_amount', 'schedule_balance', 'description', 'schedule_paid_so_far'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('id');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Payment Schedule', $query, $this->getScheduleCols(),  'payment_schedule');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getScheduleCols(), 'searchCols'=>[], 'title'=>'Payment Schedule', 'search_col_w'=>[2, 7, 3]];
    }



    public function getVisibleCols()
    {
        $cols = [
            'date_created' => ['type'=>'date', 'label'=>'Date Created'],
            'description' => ['label'=>'Description'],
            'payment_plan_term' => ['label'=>'Payment Plan Term'],
            'number_of_installments' => ['type'=>'number', 'label'=>'Number Of Installments'],
        ];

        if(!\app\components\ToWords::isSchoolUser()){
            $cols['school_name'] = ['label'=>'School'];
        }
        $cols['actions'] = [
            'type'=>'action',
            'links' => [
                ['url'=>'/plan/reports/subscribers', 'title'=>'Subscribers', 'icon'=>'fa fa-user'],
                ['url'=>'/plan/reports/cancelled-subscribers', 'title'=>'Subscribers', 'icon'=>'fa fa-user-times'],
                ['url'=>'view', 'title'=>'View', 'icon'=>'fa fa-search'],
                ['url'=>'update', 'title'=>'Update', 'icon'=>'fa fa-edit'],
            ]
        ];

        return $cols;
    }

    public function getScheduleCols()
    {
        return [
            'schedule_date' => ['type'=>'date', 'label'=>'Schedule Date'],
            'description' => ['label'=>'Description'],
            'total_schedule_amount' => ['type'=>'number', 'label'=>'Amount'],
            'schedule_balance' => ['type'=>'number', 'label'=>'Balance'],
            'schedule_paid_so_far' => ['type'=>'number', 'label'=>'Total paid so far'],
        ];
    }

    public function getSearchCols()
    {
        $cols = [
            'date_from' => ['type'=>'date', 'label'=>'From'],
            'date_to' => ['type'=>'date', 'label'=>'To'],
        ];
        if(!\app\components\ToWords::isSchoolUser()){
            $cols['school_id'] = ['type'=>'search', 'mode'=>'school', 'label'=>'Filter School'];
        }

        return $cols;
    }

    protected function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
        $_SESSION['findData']['exportFile'] = '@app/views/common/exportPdfExcel';
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>$_SESSION['findData']['exportFile'],
        ];
        return $data;
    }

}

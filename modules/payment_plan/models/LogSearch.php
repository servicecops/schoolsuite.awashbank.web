<?php

namespace app\modules\payment_plan\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use app\modules\payment_plan\models\PlanLogs;
if (!Yii::$app->session->isActive) session_start();

/**
 * LogSearch represents the model behind the search form about `app\modules\payment_plan\models\PlanLogs`.
 */
class LogSearch extends PlanLogs
{
    public $date_from, $date_to, $username, $school;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'payment_plan_id'], 'integer'],
            [['ip_address', 'event', 'event_date', 'payment_plan_id', 'date_from', 'date_to', 'username', 'school'], 'safe'],
            [['error'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        $this->load($params);
        $query = new Query();
        if(\app\components\ToWords::isSchoolUser()){
            $this->school = Yii::$app->user->identity->school_id;
        }

        $query->select([
            "usr.username||': '||concat_ws(' ', usr.firstname,usr.lastname) as user",
            "concat_ws(' ', si.first_name, si.middle_name, si.last_name) as student", 'si.id as student_id',
            'ip_address', 'event', 'event_date', 'error', 'pl.description', 'usr.school_id'])
            ->from('payment_plan_logs log')
            ->innerJoin('user usr', 'usr.id=log.user_id')
            ->innerJoin('payment_plan pl', 'pl.id=log.payment_plan_id')
            ->leftJoin('core_student si', 'si.id=log.student_id');
        $query->andFilterWhere([
            'pl.school_id' => $this->school,
            'log.user_id' => $this->user_id,
            'log.payment_plan_id' => $this->payment_plan_id])
            ->andFilterWhere(['>=', 'log.event_date', $this->date_from])
            ->andFilterWhere(['<=', 'log.event_date', $this->date_to])
            ->andFilterWhere(['ilike', 'usr.username', $this->username]);


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [
                  'event_date', 'error'
                ],
            ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('log.event_date DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Plan Logs', $query, $this->getVisibleCols(),  'payment_plan_logs');
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getVisibleCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>'Plan Logs', 'search_col_w'=>[2, 7, 3]];
    }


    public function getVisibleCols()
    {
        return [
            'event_date' => ['type'=>'date', 'label'=>'Event Date', 'has-error'=>'error'],
            'event' => ['label'=>'Event', 'has-error'=>'error'],
            'student' => ['type'=>'url', 'url'=>['link'=>'/schoolcore/core-student/view', 'params'=>['id'=>'student_id'], 'desc'=>'student'], 'label'=>'Student'],
            'description' => ['label'=>'Payment Plan', 'has-error'=>'error'],
            'user' => ['label'=>'User'],
            'ip_address' => ['label'=>'Ip Address', 'has-error'=>'error'],
        ];
    }

    public function getSearchCols()
    {
        return [
            'username' => ['label'=>'User Name'],
            'date_from' => ['type'=>'date', 'label'=>'From'],
            'date_to' => ['type'=>'date', 'label'=>'To'],
        ];
    }

    private function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData']['query'],
            'labels'=>$_SESSION['findData']['cols'],
            'title'=>$_SESSION['findData']['title'],
            'fileName'=>$_SESSION['findData']['file_name'],
            'exportFile'=>'@app/views/common/exportPdfExcel',
        ];
        return $data;
    }

}

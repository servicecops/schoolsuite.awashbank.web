<?php
namespace app\modules\payment_plan\models;

use app\modules\feesdue\models\FeesDue;
use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;

if (!Yii::$app->session->isActive) session_start();
class Reports extends PaymentPlanSearch
{
    public function rules()
    {
        return [
            [['date_from', 'date_to', 'payment_plan_term', 'description', 'payment_plan_id', 'fee_id', 'student_id', 'school_id'], 'safe'],
        ];
    }

    public function searchOutstanding($params)
    {
        $this->load($params);
        if(\app\components\ToWords::isSchoolUser()){
            $this->school_id = Yii::$app->user->identity->school_id;
        }

        $query = new Query();
        $perc = [];
        for($i=1; $i<=10; $i++){
            $perc[] = "schedule_{$i}_percentage";
        }
        $query->select(array_merge($perc, ['pln.id', 'pln.description', 'sch.school_name', 'json_agg(distinct(fd.id, fd.due_amount, fd.effective_date, fd.end_date, fd.description)) as fee', 'count(scd.total_schedule_amount) as installment', 'pln.number_of_installments as size', 'count(scd.installment_id) as count', "scd.installment_id",  'sum(scd.total_schedule_amount) as total_amount', 'sum(scd.schedule_balance) as outstanding']))
            ->from('payment_plan_student_payment_schedule scd')
            ->innerJoin('payment_plan pln', 'scd.payment_plan_id=pln.id')
            ->innerJoin('institution_fees_due fd', 'fd.id=scd.fee_id')
            ->innerJoin('core_school sch', 'sch.id=pln.school_id');
        $query->groupBy(['pln.id', 'pln.description', 'scd.installment_id', 'sch.school_name']);

        $query->andFilterWhere([
            'scd.fee_id'=>$this->fee_id,
            'scd.payment_plan_id'=>$this->payment_plan_id,
            'pln.school_id'=>$this->school_id])
            ->andFilterWhere(['>=', 'fd.effective_date', $this->date_from])
            ->andFilterWhere(['<=', 'fd.end_date', $this->date_to])
            ->andFilterWhere(['<>', 'scd.installment_id', 0]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('pln.id DESC');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession('Payment plan Summary', $query, [],  'payment_plan_summary');
        $_SESSION['findData']['exportFile'] = '@app/modules/payment_plan/views/reports/exportPdfExcel';

        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort];
    }

    public function getSearchMainCols()
    {
        $fees = $cols = [];
        $w =2;
        if(\app\components\ToWords::isSchoolUser()){
            $w=3;
            $fees = ArrayHelper::map(FeesDue::find()->andFilterWhere(['school_id'=>$this->school_id, 'approval_status'=>true])->andWhere("effective_date <= '". date("Y-m-d")."' AND end_date >= '".date("Y-m-d")."'")->orderBy('date_created')->all(), 'id', 'selectDesc');
        } else $cols['school_id']=['type'=>'search', 'mode'=>'school', 'label'=>'Select School'];

        $cols = array_merge($cols, [
            'fee_id' => [ 'type'=>'dropdown', 'options'=>$fees, 'inputOptions'=>['prompt'=>'Filter by fee', 'class'=>'form-control input-sm']],
            'date_from' => ['type'=>'date', 'label'=>'Effective Date'],
            'date_to' => ['type'=>'date', 'label'=>'End Date', 'width'=>$w],
        ]);
        return $cols;
    }

    public function searchSubscribers($params)
    {
        $this->load($params);
        $plan = PaymentPlan::find()->where(['id'=>$this->payment_plan_id])->limit(1)->one();
        $this->school_id = $plan->school_id;
        $query = new Query();
        $query->select(['scd.student_id as id', 'si.student_code', "string_agg(distinct(to_char(fd.due_amount, '9,999,999,999')||' | '||fd.effective_date||' - '||fd.end_date ), '<br/>') as fees", "concat_ws(' ', si.first_name, si.middle_name, si.last_name ) as name", 'si.guardian_name', "concat_ws('  ', si.student_phone, si.guardian_phone) as phone", 'sum(scd.total_schedule_amount) as total_amount', 'sum(scd.schedule_balance) as outstanding'])
            ->from('payment_plan_student_payment_schedule scd')
            ->innerJoin('core_student si', 'si.id=scd.student_id')
            ->innerJoin('institution_fees_due fd', 'fd.id=scd.fee_id');
        $query->groupBy(['scd.student_id', 'si.student_code', 'si.first_name', 'si.middle_name', 'si.last_name', 'si.guardian_name', 'si.guardian_phone', 'si.student_phone'])
            ->andFilterWhere([
                'scd.payment_plan_id'=>$this->payment_plan_id,
                'scd.fee_id'=>$this->fee_id, 'scd.cancelled'=>false])
            ->andFilterWhere(['>=', 'fd.effective_date', $this->date_from])
            ->andFilterWhere(['<=', 'fd.end_date', $this->date_to])
            ->andFilterWhere(['<>', 'scd.installment_id', 0]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('sum(scd.schedule_balance)');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession($plan->description, $query, $this->getSubscriberCols(),  'plan_subscribers_'.$plan->description);
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getSubscriberCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>$plan->description, 'search_col_w'=>[3, 6, 3]];
    }

    public function searchCancelledSubscribers($params)
    {
        $this->load($params);
        $plan = PaymentPlan::find()->where(['id'=>$this->payment_plan_id])->limit(1)->one();
        $this->school_id = $plan->school_id;
        $query = new Query();
        $query->select(['scd.student_id as id', 'si.student_code', "string_agg(distinct(to_char(fd.due_amount, '9,999,999,999')||' | '||fd.effective_date||' - '||fd.end_date ), '<br/>') as fees", "concat_ws(' ', si.first_name, si.middle_name, si.last_name ) as name", 'si.guardian_name', "concat_ws('  ', si.student_phone, si.guardian_phone) as phone", 'sum(scd.total_schedule_amount) as total_amount', 'sum(scd.schedule_balance) as outstanding'])
            ->from('payment_plan_student_payment_schedule scd')
            ->innerJoin('core_student si', 'si.id=scd.student_id')
            ->innerJoin('institution_fees_due fd', 'fd.id=scd.fee_id');
        $query->groupBy(['scd.student_id', 'si.student_code', 'si.first_name', 'si.middle_name', 'si.last_name', 'si.guardian_name', 'si.guardian_phone', 'si.student_phone'])
            ->andFilterWhere([
                'scd.payment_plan_id'=>$this->payment_plan_id,
                'scd.fee_id'=>$this->fee_id, 'scd.cancelled'=>true])
            ->andFilterWhere(['>=', 'fd.effective_date', $this->date_from])
            ->andFilterWhere(['<=', 'fd.end_date', $this->date_to])
            ->andFilterWhere(['<>', 'scd.installment_id', 0]);

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]); //Use slave db connection in reporting mode
        $sort = new Sort([
            'attributes' => [],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('sum(scd.schedule_balance)');
        $query->offset($pages->offset)->limit($pages->limit);
        $this->setSession($plan->description, $query, $this->getSubscriberCols(),  'plan_subscribers_'.$plan->description);
        return ['query'=>$query->all(Yii::$app->db), 'pages'=>$pages, 'sort'=>$sort, 'cols'=>$this->getSubscriberCols(), 'searchCols'=>$this->getSearchCols(), 'title'=>$plan->description, 'search_col_w'=>[3, 6, 3]];
    }



    public function getSubscriberCols()
    {
        return [
            'student_code'=>['label'=>'Payment code'],
            'name' => ['label'=>'Student'],
            'guardian_name' => ['label'=>'Parent'],
            'phone' => ['label'=>'Parent / Student Phone'],
            'fees' => ['label'=>'Applied Fees | Applied Dates'],
            'total_amount' => ['type'=>'number', 'label'=>'Total Amount'],
            'outstanding' => ['type'=>'number', 'label'=>'Balance'],
            'actions' => [
                'type'=>'action',
                'links' => [
                    ['url'=>'/schoolcore/core-student/view', 'title'=>'Student', 'icon'=>'fa fa-search'],
                ]
            ]
        ];
        return $cols;
    }

    public function getSearchCols()
    {
        $fees = ArrayHelper::map(FeesDue::find()->andFilterWhere(['school_id'=>$this->school_id, 'approval_status'=>true])->andWhere("effective_date <= '". date("Y-m-d")."' AND end_date >= '".date("Y-m-d")."'")->orderBy('date_created')->all(), 'id', 'selectDesc');
        return [
            'student_id' => ['label'=>'Payment code'],
            'fee_id' => [ 'type'=>'dropdown', 'options'=>$fees, 'inputOptions'=>['prompt'=>'Filter by fee', 'class'=>'form-control input-sm']],
            'date_from' => ['type'=>'date', 'label'=>'Effective Date'],
            'date_to' => ['type'=>'date', 'label'=>'End Date', 'width'=>2],
        ];
    }
}

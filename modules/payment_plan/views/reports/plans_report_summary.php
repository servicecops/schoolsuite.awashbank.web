<?php
use yii\helpers\Url;
use yii\helpers\Html;
$records = $data['query'];
?>
<div class="col-md-12">
    <div>
        <div class="col-sm-2 col-xs-12 no-padding"><span style="font-size:22px;"><i class="fa fa-th-list"></i> Plans Summary</span></div>
        <div class="col-sm-7 col-xs-12 no-padding-right" ><?php echo $this->render('@app/views/common/_thesearch', ['model' => $searchModel, 'cols'=>$searchModel->searchMainCols]); ?></div>
        <div class="col-sm-3 col-xs-12 no-padding">
           <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['/export-data/export-to-pdf', 'model'=>get_class($searchModel)], ['class' => 'btn  btn-danger btn-sm', 'target'=>'_blank']) ?>

                    <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model'=>get_class($searchModel)], ['class' => 'btn btn-primary btn-sm', 'target'=>'_blank']) ?>

        </div>
    </div>
</div>

<div class="table table-responsive">
    <table class="table">
        <thead>
        <th>Payment Plan</th>
        <th>Applied Fees & Dates</th>
        <th>Installments</th>
        <th class="text-right">Total Pledged Amount </th>
        <th class="text-right">Total Outstanding </th>
        <th class="text-right"> &nbsp;&nbsp;</th>
        </thead>
        <tbody>
        <?php if($records) :
            $row = 0;
            $i = 1;
            $bg = 'transparent';
            $total_amount =0;
            $total_bal =0;
            foreach($records as $k=>$v) :
                $size = $v['size'];
                $paddingTop = $total_amount ==0 ? 20 : 7;

                if($row != $v['id']) :
                    $bg = ($i%2) ? '#E3EEF1':'transparent';
                    $i++;
                ?>
                <tr>
                    <td rowspan="<?= $size+2 ?>" style="<?='background-color:'.$bg ?>">
                        <span class="text-uppercase text-bold"><?= $v['description'] ?></span><br/>
<!--                        <span style="font-size: 12px;">--><?//= $v['school_name'] ?><!--</span> <br/>-->
                        <a style="font-size:16px;" class="aclink" href="<?= Url::to(['/plan/reports/subscribers', 'id'=>$v['id']]).'&'.Yii::$app->request->queryString ?>" title="Subscribers">
                            Subscribers: <?= $v['count'] ?>
                        </a>
                    </td>

                    <td rowspan="<?= $size+2 ?>" style="<?='background-color:'.$bg ?>" >
                        <?php foreach (json_decode($v['fee']) as $key=>$value) {
                            $thisYr = date('Y');
                            $f3_year = date('Y', strtotime($value->f3));
                            $f4_year = date('Y', strtotime($value->f4));
                            $date = $value->f3." - ". $value->f4."<br>";
                            if ( $f3_year == $thisYr && $f4_year ==$thisYr){
                                $date = date('M d', strtotime($value->f3)) ." - ". date('M d', strtotime($value->f4))."<br>";
                            } else if($f3_year == $f4_year){
                                $date = date('M d', strtotime($value->f3)) ." - ". date('M d, Y', strtotime($value->f4))."<br>";
                            }

                            echo ucfirst($value->f5).' : '.number_format($value->f2)."<div style='font-size: smaller;font-weight: bold; margin-top:-3px;padding-bottom: 3px;'><i>".$date."</i></div>";

                        } ?>
                    </td>
                </tr>
                <?php
                endif;
                $row = $v['id'];
                $installment = $v['installment_id'];
                ?>
                <tr style="background-color: <?= $bg ?>">
                    <td style="padding-top:<?=$paddingTop?>px;">Installment <?= $installment.' - '.$v["schedule_{$installment}_percentage"]. '% '?></td>
                    <td style="padding-top:<?=$paddingTop?>px;" class="text-right"><?= number_format($v['total_amount'])?></td>
                    <td style="padding-top:<?=$paddingTop?>px;" class="text-right"><?= number_format($v['outstanding']) ?></td>
                    <td>&nbsp;</td>
                </tr>
                <?php
                $total_amount = $total_amount+ $v['total_amount'];
                $total_bal = $total_bal+ $v['outstanding'];

                if($size == $v['installment_id']) : ?>
                <tr style="background-color: <?= $bg ?>; ">
                    <td class="plan_total"><code>Total </code></td>
                    <td class="plan_total text-right"><code><?= number_format($total_amount)?></code></td>
                    <td class="plan_total text-right"><code><?= number_format($total_bal) ?></code></td>
                    <td class="plan_total text-right">
                        <a class="aclink" href="<?= Url::to(['/plan/reports/subscribers', 'id'=>$v['id']]).'&'.Yii::$app->request->queryString ?>" title="Subscribers"><i class="fa fa-search"></i> &nbsp;&nbsp;</a>
                    </td>
                </tr>
                <?php
                $total_amount =0;
                $total_bal =0;
                endif;
            endforeach;
            else : ?>
                <tr><td colspan=4>No records found</td></tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>

<?php
$school_id = $searchModel->school_id;
$url = Url::to(['/fees-due/search-fees']);
$script = <<< JS
$(document).ready(function(){
    var school_id = +'$school_id';
    if(school_id){
        $.get('$url', {id:school_id}, function(data) {
            $('#reports-fee_id').html(data);
        });
    }
    
    $('#search_school_id').on('change', function() {
      $.get('$url', {id:$(this).val()}, function(data) {
            $('#reports-fee_id').html(data);
        });
    })
    
    
});
JS;
$this->registerJs($script);
?>

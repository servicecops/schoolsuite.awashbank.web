<?php
use yii\helpers\Html;

$session = $subSessionIndex ? $_SESSION['findData'][$subSessionIndex] : $_SESSION['findData'];
$records = $query;
$title = $session['title'];
if($type == 'Excel') {
    echo "<table><tr> <th colspan={}><h3>".$title."</h3> </th> </tr> </table>";
}
?>
<table class="table" width="100%">
    <thead>
    <th>Payment Plan</th>
    <th>Applied Fees & Dates</th>
    <th>Installments</th>
    <th class="text-right">Total Pledged Amount </th>
    <th class="text-right">Total Outstanding </th>
    </thead>
    <tbody>
    <?php if($records) :
        $row = 0;
        $i = 1;
        $bg = 'transparent';
        $total_amount =0;
        $total_bal =0;
        foreach($records as $k=>$v) :
            $size = $v['size'];
            $paddingTop = $total_amount ==0 ? 20 : 7;

            if($row != $v['id']) :
                $bg = ($i%2) ? '#E3EEF1':'transparent';
                $i++;
                ?>
                <tr>
                    <td rowspan="<?= $size+2 ?>" style="<?='background-color:'.$bg ?>">
                        <span class="text-uppercase text-bold"><?= $v['description'] ?></span><br/>
                        Subscribers: <?= $v['count'] ?>
                    </td>

                    <td rowspan="<?= $size+2 ?>" style="<?='background-color:'.$bg ?>" >
                        <?php foreach (json_decode($v['fee']) as $key=>$value) {
                            $thisYr = date('Y');
                            $f3_year = date('Y', strtotime($value->f3));
                            $f4_year = date('Y', strtotime($value->f4));
                            $date = $value->f3." - ". $value->f4."<br>";
                            if ( $f3_year == $thisYr && $f4_year ==$thisYr){
                                $date = date('M d', strtotime($value->f3)) ." - ". date('M d', strtotime($value->f4))."<br>";
                            } else if($f3_year == $f4_year){
                                $date = date('M d', strtotime($value->f3)) ." - ". date('M d, Y', strtotime($value->f4))."<br>";
                            }

                            echo ucfirst($value->f5).' : '.number_format($value->f2)."<div style='font-size: smaller;font-weight: bold; margin-top:-3px;padding-bottom: 3px;'><i>".$date."</i></div>";

                        } ?>
                    </td>
                </tr>
            <?php
            endif;
            $row = $v['id'];
            $installment = $v['installment_id'];
            ?>
            <tr style="background-color: <?= $bg ?>">
                <td style="padding-top:<?=$paddingTop?>px;">Installment <?= $installment.' - '.$v["schedule_{$installment}_percentage"]. '% '?></td>
                <td style="padding-top:<?=$paddingTop?>px;" class="text-right"><?= number_format($v['total_amount'])?></td>
                <td style="padding-top:<?=$paddingTop?>px;" class="text-right"><?= number_format($v['outstanding']) ?></td>
            </tr>
            <?php
            $total_amount = $total_amount+ $v['total_amount'];
            $total_bal = $total_bal+ $v['outstanding'];

            if($size == $v['installment_id']) : ?>
                <tr style="background-color: <?= $bg ?>; ">
                    <td class="plan_total"><code>Total </code></td>
                    <td class="plan_total text-right"><code><?= number_format($total_amount)?></code></td>
                    <td class="plan_total text-right"><code><?= number_format($total_amount) ?></code></td>
                </tr>
                <?php
                $total_amount =0;
                $total_bal =0;
            endif;
        endforeach;
    else : ?>
        <tr><td colspan=4>No records found</td></tr>
    <?php endif; ?>
    </tbody>
</table>
<?= $this->render('@app/views/common/create', ['model'=>$model, 'mod'=>'plan']); ?>

<?php
$script = <<< JS
$(document).ready(function(){
    var no_of_installments = '$model->number_of_installments';
    var addInstallments = function(number) {
            var columns = "<div id='schedule_fields'>";
            var md = 3;
            number = +number;
            var num = parseInt(100/number);
            var rm = 100 % number;
            var total = 0;
            if(number == 3) md = 4;
            else if(number <3) md = 6;
            for(var i=1; i<=number; i++){
                var ph;
                var storedvalue = storedValue(i);
                if((number ==no_of_installments) && storedvalue) ph = storedvalue;
                else {
                    ph = num;
                    if(i==1) ph = num + rm;   
                }
                total +=ph;
                console.log(storedValue(i));
                columns += "<div class='col-md-6 col-md-"+md+"'>"+
                      "<div class='form-group field-paymentplan-schedule_"+i+"_percentage'>"+
                      "<label class='control-label' for='paymentplan-schedule_"+i+"_percentage'>Schedule "+i+"  ( % )</label>"+
                      "<input type='number' required value="+ph+" id='paymentplan-schedule_"+i+"_percentage' class='form-control input-md schedule' name='PaymentPlan[schedule_"+i+"_percentage]' placeholder='eg. "+ph+"'>"+
                      "<div class='help-block'></div>"+
                      "</div></div>";
            }
            for(var i=number+1; i <=10; i++){
                columns += "<input type='hidden' value='0'  name='PaymentPlan[schedule_"+i+"_percentage]' />"
            }
            columns += "<div class='col-md-12' style='margin:10px 0 20px 0'>" +
             "<code id='total_perc' style='border: 1px dashed #e2e2e2; font-size:20px;padding:10px 20px;background-color:#f8fafb'>Total: "+total+" %</code>" +
              "</div></div>";
            $('#schedule_fields').remove();
            $('#form_fields_section_1').append(columns);
        }
    var storedValue = function(pos) {
        var value = 0;
          if(pos == 1) value = '$model->schedule_1_percentage';
          else if(pos == 2) value = '$model->schedule_2_percentage';
          else if(pos == 3) value = '$model->schedule_3_percentage';
          else if(pos == 4) value = '$model->schedule_4_percentage';
          else if(pos == 5) value = '$model->schedule_5_percentage';
          else if(pos == 6) value = '$model->schedule_6_percentage';
          else if(pos == 7) value = '$model->schedule_7_percentage';
          else if(pos == 8) value = '$model->schedule_8_percentage';
          else if(pos == 9) value = '$model->schedule_9_percentage';
          else if(pos == 10) value = '$model->schedule_10_percentage';
          return +value;
    }
    
    if(no_of_installments) addInstallments(no_of_installments);
    $('#paymentplan-number_of_installments').on('change', function() {
      if($(this).val()) addInstallments($(this).val()); 
    });
    
    $('body').on('keyup', 'input.schedule', function() {
         var sum = 0;
        $('input.schedule').each(function() {
            sum += +($(this).val());
        });
        $('#total_perc').html("Total: "+sum+" %");
        if(sum ==100) $('#schedule_total_error').remove();
    });
    
    
    $("form#w0").on("submit", function (event, messages) {
         var total = 0;
        $('input.schedule').each(function() {
            total += +($(this).val())
        });
        
        if(total !=100){
            $('#schedule_total_error').remove();
            $('#schedule_fields').append("<div id='schedule_total_error' style='padding-bottom:10px;' class='col-xs-12 text-danger'>Total %age must be 100.   &nbsp;"+total+" given.</div>");
            return false;
        }
    });
});

JS;
$this->registerJs($script);
?>

<?php

use app\modules\feesdue\models\FeesDue;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\payment_plan\models\PaymentPlan;
use yii\widgets\ActiveForm;

?>
<p>Payment Plans</p>

<?php $form = ActiveForm::begin([
    'action' => ['/plan/default/cancel-student-plan', 'id' => $model->id],
    'method' => 'post',
    'options' => ['class' => 'formprocess', 'id' => 'assign_payment_plan_form_modal']
]); ?>
<div class="col-xs-12">
    <b style="color:#D82625; font-size: 17px;">Important: </b> A fee will only be available if it's <b>APPROVED</b> and
    between it's <b>effective</b> & <b>end </b>date.
</div>

<div class="col-xs-12">
    <?= $form->field($model2, 'fee')->dropDownList(
        ArrayHelper::map(FeesDue::find()->where(['school_id' => $model->school_id, 'approval_status' => true])->andWhere("effective_date <= '" . date("Y-m-d") . "' AND end_date >= '" . date("Y-m-d") . "'")->orderBy('date_created')->all(), 'id', 'selectDesc'), ['prompt' => 'Select Fee', 'class' => 'form-control input-sm']
    )->label('') ?>
</div>
<div class="col-xs-12">
    <?= $form->field($model2, 'payment_plan_id')->dropDownList(
        ArrayHelper::map(PaymentPlan::find()->where(['school_id' => $model->school_id])->all(), 'id', 'description'), ['prompt' => 'Select Plan', 'class' => 'form-control input-sm']
    )->label('') ?>
</div>
<div class="col-xs-12">
    <?= $form->field($model2, 'cancellation_reason', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Reason']])->textInput() ?>

</div>
<div class="form-group col-xs-12 no-padding" style="margin-top:20px;">
    <div class="col-xs-6">
        <?= Html::submitButton('Cancel Plan', ['class' => 'btn btn-block btn-info', 'data-confirm' => 'Are you sure you want Cancela this Plan']) ?>
    </div>
    <div class="col-xs-6">
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php
$script = <<< JS
    $(document).ready(function(){
        $('form#assign_payment_plan_form_modal').yiiActiveForm('validate');
          $("form#assign_payment_plan_form_modal").on("afterValidate", function (event, messages) {
              if($(this).find('.has-error').length) {
                        return false;
                } else {
                    $('.modal').modal('hide');
                }
            });
   });
JS;
$this->registerJs($script);
?>

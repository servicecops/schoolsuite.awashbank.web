<?php

use yii\helpers\Html;
use yii\helpers\Url;
$pad_length = 4;
$pad_char = 0;
$str_type = 'd';
$format = "%{$pad_char}{$pad_length}{$str_type}";
?>
<table align="center">
    <tr>
        <td><img src="<?= Yii::getAlias('@web/web/img/PNG1.png') ?>" height="40" width="94" /></td>
        <td style="color:red;font-size:22px;" align="right" width="85.8%"><tt><?= sprintf($format, $data['id']); ?></tt></td>
    </tr>
</table>
<div class="row" style="margin-top:-30px;">
    <div style="text-align:center;">
        <?php if($data['school_logo']) : ?>
            <img src="<?= Url::to(['/import/import/image-link2', 'id'=>$data['school_logo']]) ?>" height="65" width="65" />
        <?php else :?>
            <img src="<?= Yii::getAlias('@web/web/img/icon_4.png') ?>" height=65, width=65 />
        <?php endif; ?>
        <div style="font-size:18px;font-weight: bold; padding-top:10px;"><?= $data['school_name'] ?></div>
        <div style="font-size:10px;padding-top:5px;"><?= $data['physical_address'] ?></div>
        <div style="font-size:15px;font-weight: bold; padding-top:5px;padding-bottom:20px;">PARENT-STUDENT SCHOOL FEES PAYMENTS PLAN AGREEMENT</div>
    </div>

</div>

<table  width="100%">
    <tr>
        <td><b>Student's Details</b></td>
        <td align="right"><b>Parent's Details</b></td>
    </tr>
    <tr>
        <td style="line-height: 20px;" valign="top">
            Name: <?= $data['name']?><br/>
            Student code: <?= $data['student_code']?><br>
            Reg Number: <?= $data['reg_no']?><br>
            Contact: <?= $data['student_phone']?>
        </td>
        <td style="line-height: 20px;" align="right" valign="top">
            Name: <?= $data['guardian_name'] ? $data['guardian_name'] : '_______________________________'?><br/>
            Contact: <?= $data['guardian_phone'] ? $data['guardian_phone'] : '_______________________________'?>
        </td>
    </tr>
</table>
<br/>
&nbsp;<span style="font-size:11px;">Date: <?= date('M d, Y')?></span><br/>

<h3>Re: PAYMENT PLAN AGREEMENT</h3>
<div align="justify">
This letter confirms <b><?= $data['school_name']?>’s</b> agreement with the above noted
parent/ Guardian of the above-mentioned student, in which they acknowledge their indebtedness
to <?= $data['school_name']?> as specified below and promise to pay the amount due in monthly
installments as follows:<br/>
</div>
<h4><?= $data['plan_desc']?></h4>
<table width="100%">
    <tr>
        <td><b>Installment</b></td>
        <td><b>Schedule Date</b></td>
        <td align="right" style="padding-left: 20px;"><b>Amount</b></td>
        <td align="right" style="padding-left: 20px;"><b>Balance</b></td>
    </tr>
    <?php
    $total_amount = 0;
    $total_bal = 0;
    foreach ($installments as $k=>$v) :
        $total_amount += $v['total_schedule_amount'];
        $total_bal += $v['schedule_balance'];
        $installment = $v['installment_id'];
        ?>
    <tr>
        <td><?= $v['description']." - ".$v["schedule_{$installment}_percentage"]."%"; ?></td>
        <td><?= $v['schedule_date']?></td>
        <td align="right" style="padding-left: 20px;"><code><?= number_format($v['total_schedule_amount'])?></code></td>
        <td align="right" style="padding-left: 20px;"><code><?= number_format($v['schedule_balance'])?></code></td>
    </tr>
    <?php endforeach; ?>
    <tr >
        <td colspan="2"><b>Total</b></td>
        <td align="right" style="padding-left: 20px;"><b><code><?= number_format($total_amount)?></code></b></td>
        <td align="right" style="padding-left: 20px;"><b><code><?= number_format($total_bal)?></code></b></td>
    </tr>
</table>

<br/>
<br/>

<div align="justify">
I <b>hereby agree</b> to this payment agreement schedule for school fees charges incurred at
<?= $data['school_name']?> until the total school fees  balance for <b><?= $data['name']?></b> is <b>Zero</b>.<br>
My Failure to make payments without notification to <?= $data['school_name']?> may result in
further collection action. <?= $data['school_name']?> will have full discretion for unpaid
school fees dues and will take necessary action to collect any unpaid balances.
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<table  width="100%">
    <tr>
        <td style="line-height: 20px;" valign="top">
            ...................................................................<br>
            <?= $data['guardian_name']?><br>
            PARENT/GUARDIAN
        </td>
        <td  style="line-height: 20px;" align="right" valign="top">
            ...................................................................<br>
            HEADMASTER/HEADMISTRESS/DIRECTOR
        </td>
    </tr>
</table>

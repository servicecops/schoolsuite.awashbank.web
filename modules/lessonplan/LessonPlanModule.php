<?php
namespace app\modules\lessonplan;

class LessonPlanModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\lessonplan\controllers';
    public function init() {
        parent::init();
    }
}
<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->topic_title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="classes-view hd-title"
     data-title="<?=  $model->topic_title  ?>">
</div>

<div class="row letter">
    <div class="col-md-6">
        <div class="row ">

            <div class="col-lg-6 col-xs-9 profile-text"><h3><b><?= ($model->topic_title) ? $model->topic_title : "--" ?></b></h3></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-plus">Add Sub Topic</i>', ['/lessonplan/lesson-plan-sub-topic/create', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this lesson plan topic?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

</div>

<br><br>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Title</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->topic_title) ? $model->topic_title : "--" ?></div>
        </div>
    </div>
</div>
<div class = "row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"> Number of Periods</div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->no_of_periods) ? $model->no_of_periods : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Term</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $model->term_id ? CoreTerm::findOne($model->term_id)->term_name : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('date_created') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->date_created) ?  date('Y-m-d', strtotime($model->date_created)) : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> Created By </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $model->created_by ? User::findOne($model->created_by)->username : "--" ?></div>
        </div>
    </div>
</div>

<div class = "row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-4 profile-label">General Objectives</div>
            <div class="col-lg-8 profile-text"><?= ($model->topic_objectives) ? $model->topic_objectives : "--" ?></div>
        </div>
    </div>
</div>

<br>



</div>

<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;


$schoolId = Yii::$app->user->identity->school_id;
$classList = ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $schoolId ])->all(), 'id', 'class_code');



/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="container card" style="width:90%;">
    <div class = "row">
          <div class = "col-xs-12">
              <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create Lesson Plan Topic</h3>': '<h3><i class="fa fa-edit"></i> Edit Lesson Plan Topic</h3>' ?>
             <hr class="l_header"></hr>
          </div> 
    </div>
    <br></br>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>

    <p>All fields marked * are required</p>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'topic_title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ->label('Title *')?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'no_of_periods', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput([
                'type' => 'number'
            ]) ?>
        </div>
    </div>
    
   
    <div class="row">
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                    'data' => $classList,
                    'options' => ['placeholder' => 'Select the class'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Class *');
            ?>
        </div>
        <div class="col-sm-6">
            <?php
                
                $url = Url::to(['/planner/planner/terms-list']);
                $selectedTerm = empty($model->term_id) ? '' : CoreTerm::findOne($model->term_id)->term_name;

                echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                    'options' => ['multiple'=>false, 'placeholder' => 'Search Terms ...'],
                    'initValueText' => $selectedTerm, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        'language' => [
                             'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Enter the Term name *'); ?>
        </div>
    </div>

    <div class = "row">
        <div class = "col-md-8">
           <?= $form->field($model, 'topic_objectives')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                'preset' => 'basic'
                //'preset' => 'standard'
             ]) ->label('Topic Objective *')?>
        </div>
    </div>

    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSubject;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;


$schoolId = Yii::$app->user->identity->school_id;
$subjectList  = ArrayHelper::map(CoreSubject::find()->where(['school_id'=>$schoolId])->all(), 'id', 'subject_name');

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="container card" style="width:90%;">
    <div class = "row">
          <div class = "col-xs-12">
              <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create Lesson Plan</h3>': '<h3><i class="fa fa-edit"></i> Edit Timetable</h3>' ?>
             <hr class="l_header"></hr>
          </div> 
    </div>
    <br></br>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    <p>All fields marked with * are required</p>
    <div class="row">
        <div class="col">
            <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Title *') ?>
        </div>
        <div class="col">
            <?= $form->field($model, 'description', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
        </div>
    </div>
    
   
    <div class="row">
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'subject_id')->widget(Select2::classname(), [
                    'data' => $subjectList,
                    'options' => ['placeholder' => 'Select the subject'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Subject *');
            ?>
        </div>
        <div class="col-sm-6">
            <?php
                
                // $url = Url::to(['/planner/planner/terms-list']);
                // $selectedTerm = empty($model->term_id) ? '' : CoreTerm::findOne($model->term_id)->term_name;

                // echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                //     'options' => ['multiple'=>false, 'placeholder' => 'Search Terms ...'],
                //     'initValueText' => $selectedTerm, // set the initial display text
                //     'pluginOptions' => [
                //         'allowClear' => true,
                //         'minimumInputLength' => 2,
                //         'language' => [
                //              'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                //         ],
                //         'ajax' => [
                //             'url' => $url,
                //             'dataType' => 'json',
                //             'data' => new JsExpression('function(params) { return {q:params.term}; }')
                //         ],

                //     ],
                // ])->label('Enter the Term name'); ?>
        </div>
    </div>
    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;
use app\modules\lessonplan\models\LessonPlanSubTopic;
use app\modules\schoolcore\models\CoreSubject;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="classes-view hd-title"
     data-title="<?=  $model->title  ?>">
</div>

<div class="row letter">
    <div class="col-md-6">
        <div class="row ">

            <div class="col-lg-6 col-xs-9 profile-text"><h3><b><?= ($model->title) ? $model->title : "--" ?></b></h3></div>
        </div>
    </div>
    <div class = "col-md-6">
        <p style="float:right">
            <?= Html::a('<i class="fa fa-plus">Add Topic</i>', ['/lessonplan/lesson-plan-topic/create', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this lesson plan?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    </div>

</div>

<br><br>

<div class = "letter">
<div class="row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label">Title</div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->title) ? $model->title : "--" ?></div>
        </div>
    </div>
</div>
<div class = "row">
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('description') ?></div>
            <div class="col-lg-6 col-xs-6 profile-text"><?= ($model->description) ? $model->description : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> Subject </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $model->subject_id ? CoreSubject::findOne($model->subject_id)->subject_name : "--" ?></div>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"><?= $model->getAttributeLabel('date_created') ?></div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= ($model->date_created) ?  date('Y-m-d', strtotime($model->date_created)) : "--" ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row ">
            <div class="col-lg-6 col-xs-3 profile-label"> Created By </div>
            <div class="col-lg-6 col-xs-9 profile-text"><?= $model->created_by ? User::findOne($model->created_by)->username : "--" ?></div>
        </div>
    </div>
</div>

<br>

<div class = "row">
    <h5>TEACHING SEQUENCE</h5>
    <div class="table-responsive no-padding">
        <?php
              echo "<table class ='table-bordered table table-striped' style='margin-bottom:0'>";
              echo "<tr>";
              echo "<th class='text-center'>#</th>";
              echo "<th class='text-center'>TOPIC</th>";
              echo "<th class='text-center'>SUB - TOPICS</th>";
              echo "<th class='text-center'>NO. OF PERIODS</th>";

              echo "</tr>";
              $topicCounter = 0;
              foreach($topics as $t=>$v)
              {   

                $topicCounter++;
                echo "<tr>";
                echo "<td class='text-center'>". $topicCounter ."</td>";
                echo "<td class='text-center'>". Html::a( $v['topic_title'], ['/lessonplan/lesson-plan-topic/view', 'id' => $v['id']])  ."</td>";
                echo "<td class='text-center'>";

                   $subTopics = LessonPlanSubTopic::find()->where([
                                    'topic_id' => $v['id']
                               ])->orderBy('id')
                                  ->asArray()
                                  ->all();
                    foreach ($subTopics as $key => $value) {
                        echo "<p>" . Html::a( $value['title'], ['/lessonplan/lesson-plan-sub-topic/view', 'id' => $value['id']]) . "</p>";

                        echo "<hr></hr>";
                    }  
                echo "</td>";
                echo "<td class='text-center'>";
                    foreach ($subTopics as $key => $value) {
                         echo "<p>" . $value['no_of_periods'] . "</p>";
                         echo "<hr></hr>"; 
                    }  
                echo "</td>";
                echo "</tr>";

              }
              echo "</table>";

          ?>
    </div>
</div>

</div>

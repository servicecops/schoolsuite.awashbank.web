<?php

use app\models\User;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;


$schoolId = Yii::$app->user->identity->school_id;
$classList = ArrayHelper::map(CoreSchoolClass::find()->where(['school_id' => $schoolId ])->all(), 'id', 'class_code');



/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreTerm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="container card" style="width:90%;">
    <div class = "row">
          <div class = "col-xs-12">
              <?= ($model->isNewRecord)? '<h3><i class="fa fa-plus"></i> Create Lesson Plan Sub Topic</h3>': '<h3><i class="fa fa-edit"></i> Edit Lesson Plan Sub Topic</h3>' ?>
             <hr class="l_header"></hr>
          </div> 
    </div>
    <br></br>

    <?php $form = ActiveForm::begin(['options' => ['class' => 'formprocess']]); ?>
    <p>All fields marked with * are required</p>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'title', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput()->label('Title *') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'no_of_periods', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput([
                'type' => 'number'
            ])->label('No of periods *') ?>
        </div>
    </div>


    <div class = "row">
        <div class = "col-md-6">
           <?= $form->field($model, 'specific_objectives')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                //'preset' => 'basic'
                'preset' => 'standard'
             ]) ?>
        </div>
        <div class = "col-md-6">
           <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                'preset' => 'standard'
             ]) ?>
        </div>
    </div>

    <div class = "row">
        <div class = "col-md-6">
           <?= $form->field($model, 'teaching_strategy')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                'preset' => 'standard'
             ])->label("Teaching and Learning Strategy") ?>
        </div>
        <div class = "col-md-6">
           <?= $form->field($model, 'summary_notes')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                'preset' => 'standard'
             ]) ?>
        </div>
    </div>

    
    <div class="card-footer">
        <div class="row">
            <div class="col-xm-6">
                <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
            </div>
            <div class="col-xm-6">
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

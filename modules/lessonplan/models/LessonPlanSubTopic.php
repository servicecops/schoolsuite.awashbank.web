<?php

namespace app\modules\lessonplan\models;

use Yii;

/**
 * This is the model class for table "lesson_plan_sub_topic".
 *
 * @property int $id
 * @property string $title
 * @property int $no_of_periods
 * @property int $topic_id
 * @property string|null $specific_objectives
 * @property string|null $content
 * @property string|null $teaching_strategy
 * @property string|null $summary_notes
 * @property int|null $created_by
 * @property string|null $date_created
 * @property string|null $date_modified
 */
class LessonPlanSubTopic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_plan_sub_topic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'no_of_periods', 'topic_id'], 'required'],
            [['title', 'specific_objectives', 'content', 'teaching_strategy', 'summary_notes'], 'string'],
            [['no_of_periods', 'topic_id', 'created_by'], 'default', 'value' => null],
            [['no_of_periods', 'topic_id', 'created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'no_of_periods' => 'No Of Periods',
            'topic_id' => 'Topic ID',
            'specific_objectives' => 'Specific Objectives',
            'content' => 'Content',
            'teaching_strategy' => 'Teaching Strategy',
            'summary_notes' => 'Summary Notes',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
}

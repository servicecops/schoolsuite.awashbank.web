<?php

namespace app\modules\lessonplan\models;

use Yii;

/**
 * This is the model class for table "lesson_plan".
 *
 * @property int $id
 * @property string $title
 * @property string|null $description
 * @property int|null $subject_id
 * @property int $school_id
 * @property int|null $created_by
 * @property boolean $active
 * @property string|null $date_created
 */
class LessonPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'school_id','subject_id'], 'required'],
            [['title', 'description'], 'string'],
            [['subject_id', 'school_id', 'created_by'], 'default', 'value' => null],
            [['subject_id', 'school_id', 'created_by'], 'integer'],
            [['active'], 'boolean'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'subject_id' => 'Subject ID',
            'school_id' => 'School ID',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
        ];
    }
}

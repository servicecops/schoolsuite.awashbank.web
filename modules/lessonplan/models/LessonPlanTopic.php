<?php

namespace app\modules\lessonplan\models;

use Yii;

/**
 * This is the model class for table "lesson_plan_topic".
 *
 * @property int $id
 * @property string $topic_title
 * @property int|null $no_of_periods
 * @property int $class_id
 * @property int $term_id
 * @property string $topic_objectives
 * @property int $created_by
 * @property int $lesson_plan_id
 * @property boolean $active
 * @property string|null $date_created
 * @property string|null $date_modified
 */
class LessonPlanTopic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_plan_topic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['topic_title', 'class_id', 'term_id', 'topic_objectives', 'created_by'], 'required'],
            [['topic_title', 'topic_objectives'], 'string'],
            [['no_of_periods', 'class_id', 'term_id', 'created_by'], 'default', 'value' => null],
            [['no_of_periods', 'class_id', 'term_id', 'created_by'], 'integer'],
            [['date_created', 'date_modified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topic_title' => 'Topic Title',
            'no_of_periods' => 'No Of Periods',
            'class_id' => 'Class ID',
            'term_id' => 'Term ID',
            'topic_objectives' => 'Topic Objectives',
            'created_by' => 'Created By',
            'date_created' => 'Date Created',
            'date_modified' => 'Date Modified',
        ];
    }
}

<?php

namespace app\modules\lessonplan\controllers;

use Yii;
use app\controllers\BaseController;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\timetable\models\Timetable;
use app\modules\logs\models\Logs;
use app\modules\lessonplan\models\LessonPlanTopic;
use app\modules\lessonplan\models\LessonPlanSubTopic;
use app\modules\lessonplan\models\LessonPlanSearch;



if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * LessonPlanTopicController that handles the lesson plan topic and sub topics
 */
class LessonPlanTopicController extends BaseController
{
     

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Display the lesson syllabus
     */
    public function actionIndex(){
        
        $request = Yii::$app->request;

        $res = ['searchModel' => 'hello search', 'dataProvider' => 'data provider'];
        return ($request->isAjax) ? $this->renderAjax('index',$res ) :
                    $this->render('index', $res);
    }


    //you must override these methods to use BaseControler.
    /**
     * Creates a new LessonPlanTopic model
     * @return Timetable
     */
    public function newModel()
    {
        $model = new LessonPlanTopic();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return LessonPlanTopicSearch
     */
    public function newSearchModel()
    {
        $searchModel = new LessonPlanSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create Lesson PLan Topic';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }


     /**
     * Creates a new LessonPlanTopic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write to the Lesson Plan');
        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new LessonPlanTopic();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();

                $model->lesson_plan_id = $request->get('id');

                if( $model->save(false) ){
                     Logs::logEvent("Created New Lesson Plan Topic : " . $model->id, null, null);                    
                }
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed create new lesson plan topic : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }


    /**
     * Displays a single LessonPlan  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {  
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') )
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);


        $responseData = [
            'model' => $model
        ];

        return ((Yii::$app->request->isAjax)) ? 
            $this->renderAjax('view',$responseData) 
                :
            $this->render('view', $responseData);


    }


    /**
     * Updates an existing Lesson Plan Topic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        if (!Yii::$app->user->can('rw_timetable')) {
            throw new ForbiddenHttpException('No permission to Write to the Lesson Plan');
        }

        $model = $this->findModel($id);
        try {
            if ($model->load(Yii::$app->request->post())) {


                if( $model->save(false) ){
                    Logs::logEvent("Updated the Lesson Plan: " . $model->id, null, null);                    
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }


        //$model->date_created = ($model->date_created)? date('Y-m-d', strtotime($model->date_created)) : '' ;

        return $this->render('update', [
            'model' => $model,
        ]);
    }




}
<?php

namespace app\modules\disciplinary\models;

use app\models\User;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $reported_by
 * @property string $date_created
 * @property string $punishment_served
 * @property string $punishment
 * @property integer $description
 * @property string $case
 * @property boolean $is_student
 * @property integer $staff_id
 * @property integer $student_code
 * @property integer $school_id
 * @property integer $class_id
 *

 */
class Disciplinary extends \yii\db\ActiveRecord
{
    public $school_student_registration_number,$school_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'core_disciplinary';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_student','reported_by', 'punishment_served','punishment','description','case','date_created'], 'required'],
            [['id', 'student_code','staff_id','approved_by','school_id','class_id'], 'integer'],
            [['is_student','punishment_served'], 'boolean'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reported_by' => 'Reported By',
            'punishment_served' => 'Punishment Served',
            'punishment' => 'Punishment',
            'description' => 'Description',
            'case' => 'Case',
            'staff_id' => 'Staff Id',
            'student_code' => 'Student Code',
            'is_student' => 'Disciplinary Case For',
        ];
    }

//    public function getSchoolName()
//    {
//        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
//    }
//    public function getClassName()
//    {
//        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
//    }


    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {

        $attributes = [
            'is_student',
            'student_code',
            'staff_id',
            'case',
            'description',
            'punishment',
            'approved_by',
            'punishment_served',
            'date_created',
            'reported_by',
            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
//            [
//                'label' => 'Student Name',
//                'value' => function ($model) {
//                    return $model->stdName->first_name.' '.$model->userName->last_name;
//                },
//            ],
            [
                'label' => 'Staff Name',
                'value' => function ($model) {
                    return $model->staffName->firstname.' '.$model->userName->lastname;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],
            'date_created',
            'date_updated',
            [
                'label' => 'Approved By',
                'value' => function ($model) {
                    return $model->userName->firstname.' '.$model->userName->lastname;
                },
            ],


        ];

        if (($models = Disciplinary::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }
    public function getUserName()
    {
        return $this->hasOne(User::className(), ['id' => 'approved_by']);
    }
    public function getStaffName()
    {
        return $this->hasOne(User::className(), ['id' => 'staff_id']);
    }
    public function getStdName()
    {
        return $this->hasOne(CoreStudent::className(), ['student_code' => 'student_code']);
    }

}

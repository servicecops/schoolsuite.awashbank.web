<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreGrades */

$this->title = "Disciplinary case ";
$this->params['breadcrumbs'][] = ['label' => 'Core Discipline', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="core-school-view">

    <div class="row">

        <div class="col-sm-10 "><span style="font-size:20px">&nbsp;<i
                        class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        <div class="col-sm-2">
            <?= Html::a('<i class="fa fa-edit">Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                 <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-sm btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
        </div>
    </div>


    <div class="row">

        <div class="col border-right bg-gradient-light">
            <div class="card bg-gradient-light">
                <div class="card-body">


                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('is_student') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->is_student) ? "Student" : "Staff" ?>
                        </div>


                    </div>
                    <hr class="style14">

                    <?php if ($model->is_student): ?>
                        <div class="row">
                            <div class="col">
                                <b style="color: #000"><?= $model->getAttributeLabel('student_code') ?></b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->student_code) ? $model->student_code : "--" ?>
                            </div>


                        </div>

                        <hr class="style14">

                        <div class="row">
                            <div class="col">
                                <b style="color: #000">Student Name</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->student_code) ? $model->stdName->first_name . ' ' . $model->stdName->last_name : "--" ?>
                            </div>


                        </div>

                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000"><?= $model->getAttributeLabel('class_id') ?></b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->class_id) ? $model->className->class_description : "--" ?>
                            </div>


                        </div>
                        <hr class="style14">

                    <?php endif ?>

                    <?php if (!$model->is_student): ?>

                        <div class="row">
                            <div class="col">
                                <b style="color: #000"><?= $model->getAttributeLabel('staff_id') ?></b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->staff_id) ? $model->staffName->firstname . ' ' . $model->staffName->lastname . ' (' . $model->staffName->username . ')' : "-- " ?>
                            </div>


                        </div>
                        <hr class="style14">
                    <?php endif; ?>
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('case') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->description) ? $model->description : "--" ?>
                        </div>


                    </div>
                    <hr class="style14">
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('description') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->description) ? $model->description : " --" ?>
                        </div>


                    </div>
                    <hr class="style14">
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('punishment') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->punishment) ? $model->punishment : "--" ?>
                        </div>


                    </div>
                    <hr class="style14">
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('approved_by') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->approved_by) ? $model->userName->firstname . ' ' . $model->userName->lastname . ' (' . $model->userName->username . ')' : " --" ?>
                        </div>


                    </div>
                    <hr class="style14">
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('punishment_served') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->punishment_served) ? "Yes" : "No" ?>
                        </div>


                    </div>
                    <hr class="style14">
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('reported_by') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= ($model->reported_by) ? $model->reported_by : " --" ?>
                        </div>


                    </div>
                    <hr class="style14">
                    <div class="row">
                        <div class="col">
                            <b style="color: #000"><?= $model->getAttributeLabel('date_created') ?></b>
                        </div>
                        <div class="col" style="color: #21211f">
                            <?= $model->date_created ?>
                        </div>


                    </div>


                </div>
            </div>
        </div>
    </div>


</div>

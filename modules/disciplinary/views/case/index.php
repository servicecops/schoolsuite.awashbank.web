<?php

use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentSearch;
use kartik\export\ExportMenu;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreStudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disciplinary Cases';
?>
<h1 class="h3 mb-0 text-gray-800"><?= Html::encode($this->title) ?></h1>
<br>

<?php echo $this->render('_search', ['model' => $searchModel]); ?>


<div class="row mt-3">
    <div class="pull-right">
        <?php
        echo Html::a('<i class="fa far fa-envelope"></i> Send email', ['email'], [
            'class'=>'btn btn-sm btn-info',
            'data-toggle'=>'tooltip',
            'title'=>'Send an email to all students'
        ]);
        ?>
    </div>


    <div class="pull-right">
        <?php
        echo Html::a('<i class="fa far fa-file-pdf"></i> Download Pdf', ['core-student/export-pdf', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-danger',
            'target'=>'_blank',
            'data-toggle'=>'tooltip',
            'title'=>'Will open the generated PDF file in a new window'
        ]);
        echo Html::a('<i class="fa far fa-file-excel"></i> Download Excel', ['export-data/export-excel', 'model' => get_class($searchModel)], [
            'class'=>'btn btn-sm btn-success',
            'target'=>'_blank'
        ]);
        ?>
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <div class="box-body table table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                <?php
                $x=1;
                ?>
                <tr>
                    <th></th>
                    <th class='clink'><?= $sort->link('is_student') ?></th>
                    <th class='clink'><?= $sort->link('case', ['label' => 'Case']) ?></th>
                    <th class='clink'><?= $sort->link('punishment_served', ['label' => 'Punishment Served.']) ?></th>
                    <th class='clink'><?= $sort->link('student_code', ['label' => 'Student Name']) ?></th>
                    <th class='clink'><?= $sort->link('staff_id', ['label' => 'Staff Id.']) ?></th>


                </tr>
                </thead>
                <tbody>
                <?php

                if ($dataProvider) :
                    foreach ($dataProvider as $k => $v) : ?>
                        <tr data-key="0">
                            <td><?php echo $x++?></td>
                            <td><?= ($v['is_student']) ? 'Yes' : 'No' ?></td>
                            <td><?= ($v['case']) ? $v['case'] : '<span class="not-set">-- </span>' ?></td>
                            <td><?= ($v['punishment_served']) ? 'Yes' : 'No' ?></td>
                            <td><?= ($v['student_code']) ? $v['student_code'] : '<span class="not-set">-- </span>' ?></td>
                            <td><?= ($v['staff_id']) ? $v['staff_id']  : '<span class="not-set">--</span>' ?></td>
                            <td>
                                <?= Html::a('<i class="fas fa-edit"></i>', ['case/update', 'id' =>  $v['id']]); ?>
                                <?= Html::a('<i class="fa  fa-ellipsis-h"></i>', ['case/view', 'id' => $v['id']], ['class'=>'aclink']) ?>

                            </td>
                        </tr>
                    <?php endforeach;
                else :?>
                    <tr>
                        <td colspan="8">No case found</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>


    </div>
</div>


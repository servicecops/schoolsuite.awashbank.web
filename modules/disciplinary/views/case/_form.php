<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'Disciplinary';
?>

<div class="container card">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="card-body">
        <div class="row">


            <div class="col-sm-6">
                <?= $form->field($model, 'is_student', ['inputOptions' => ['class' => 'form-control',
                    'placeholder' => 'Description',
                    'disabled' => $model->is_student,]])->dropDownList(
                    ['1' => 'Student', '0' => 'Staff'],
                    ['id' => 'user_type_select', 'prompt' => 'Select']
                )?>
            </div>

            <div class="col-sm-6 " id="std_div">
                <?php
                $url = Url::to(['/disciplinary/case/std-list']);
                $cityDesc = empty($model->student_code) ? '' : \app\modules\schoolcore\models\CoreStudent::findOne($model->student_code);
                $selectedSchool = $cityDesc;
                echo $form->field($model, 'student_code')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search student code ...'],
                    'initValueText' => $selectedSchool, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 10,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Enter Student Code'); ?>


            </div>
            <div class="col-sm-6 " id="staff_div">
                <?php
                $url = Url::to(['/disciplinary/case/staff-list']);
                $cityDesc = empty($model->staff_id) ? '' : User::findOne($model->username);
                $selectedSchool = $cityDesc;
                echo $form->field($model, 'staff_id')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search Staff username...'],
                    'initValueText' => $selectedSchool, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 4,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ])->label('Enter Staff Username'); ?>


            </div>
        </div>


        <div class="row">
            <div class="col">
                <?= $form->field($model, 'case', ['labelOptions' => ['style' => 'color:#041f4a']])->textInput() ?>

            </div>
            <div class="col">
                <?= $form->field($model, 'description', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>
            </div>
        </div>
        <div class="row">


            <div class="col">
                <?= $form->field($model, 'punishment', ['labelOptions' => ['style' => 'color:#041f4a']])->textarea() ?>

            </div>
            <div class="col">
                <?= $form->field($model, 'punishment_served', ['inputOptions' => ['class' => 'form-control',
                    'placeholder' => 'Served',
                    'disabled' => $model->is_student,]])->dropDownList(
                    ['1' => 'Yes', '0' => 'No'],
                    ['prompt' => 'Select']
                )?>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <?= $form->field($model, 'reported_by', ['labelOptions' => ['style' => 'color:#041f4a']]) ?>
            </div>
            <div class="col">
                <?php
                $url = Url::to(['/disciplinary/case/staff-list']);
                $cityDesc = empty($model->approved_by) ? '' : User::findOne($model->username);
                $selectedSchool = $cityDesc;
                echo $form->field($model, 'approved_by')->widget(Select2::classname(), [
                    'options' => ['multiple' => false, 'placeholder' => 'Search Staff Who approved this case...'],
                    'initValueText' => $selectedSchool, // set the initial display text
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 4,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],

                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col">

            </div>
            <div class="col">
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'create' : 'Update', ['class' => 'btn bg-gradient-primary']) ?>
                    <?= Html::resetButton('Reset Form', ['class' => 'btn bg-gradient-light']) ?>
                </div>
            </div>
            <div class="col">

            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS



$("document").ready(function(){ 
    function evaluateRecurrentVisibility() {
        if($('#user_type_select').val() === '0') {
           $('#std_div').hide()
           $('#staff_div').show()
        }else {
           $('#std_div').show() 
           $('#staff_div').hide() 
        }
}

   

evaluateRecurrentVisibility();


    //On changing user type, hide or show div controls
   $('body').on('change', '#user_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   
    });
   


JS;
$this->registerJs($script);
?>

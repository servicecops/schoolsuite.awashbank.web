<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Disciplinary Case';
$this->params['breadcrumbs'][] = ['label' => 'Core Disciplinary', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Create New Disciplinary Case</h3></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
namespace app\modules\disciplinary;

class DisciplinaryModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\disciplinary\controllers';
    public function init() {
        parent::init();
    }
}

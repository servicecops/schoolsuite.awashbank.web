<?php
namespace app\modules\noticeboard;

class NoticeModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\noticeboard\controllers';
    public function init() {
        parent::init();
    }
}
<?php

namespace app\modules\noticeboard\controllers;

use app\modules\noticeboard\models\NoticeBoard;
use app\modules\noticeboard\models\NoticeBoardSearch;
use Yii;
use app\controllers\BaseController;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\db\Query;

use app\models\User;
use app\modules\logs\models\Logs;

use app\modules\schoolcore\models\CoreStudent;
use yii\web\UploadedFile;




if (!Yii::$app->session->isActive) {
    session_start();
}


/**
 * NoticeBoardController that handles the lesson plan/syllabus
 */
class NoticeBoardController extends BaseController
{
     

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Display the NoticeBoard
     */
    public function actionIndex(){
        
        $request = Yii::$app->request;

        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }


        $searchModel = new NoticeBoardSearch();
        $searchParams = Yii::$app->request->queryParams;
        $schoolId = Yii::$app->user->identity->school_id;
        $searchParams['school_id'] = $schoolId;
        $searchParams['active'] = true;
        Yii::trace($searchParams);


            //school admin has access to all NoticeBoards
            $dataProvider = $searchModel->search($searchParams);


        return ($request->isAjax) ? $this->renderAjax('index', [
                     'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
                $this->render('index', ['searchModel' => $searchModel,
                            'dataProvider' => $dataProvider]);
    }


    //you must override these methods to use BaseControler.
    /**
     * Creates a new NoticeBoard model
     * @return NoticeBoard
     */
    public function newModel()
    {
        $model = new NoticeBoard();
        $model->created_by = Yii::$app->user->identity->getId();
        return $model;
    }

    /**
     * @return NoticeBoardSearch
     */
    public function newSearchModel()
    {
        $searchModel = new NoticeBoardSearch();
        return $searchModel;
    }

    public function createModelFormTitle()
    {
        return 'Create NoticeBoard';
    }

    /**
     * searches fields.
     * @return mixed
     */
    public function actionSearch()
    {
        $searchModel = $this->newSearchModel();
        $allData = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider = $allData['dataProvider'];
        $columns = $allData['columns'];
        $searchForm  = $allData['searchForm'];
        $res = ['dataProvider'=>$dataProvider, 'columns'=>$columns,'searchModel'=>$searchModel,'searchForm'=>$searchForm];
        // return $request->isAjax ? $this->renderAjax('card_summary', $res)

        return $this->render('@app/views/common/grid_view', $res);
    }


     /**
     * Creates a new NoticeBoard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *  @return mixed
     */
    public function actionCreate()
    {   

        if (!Yii::$app->user->can('rw_announcements')) {
            throw new ForbiddenHttpException('Sorry, you don not have permissions to create Announcements. ');
        }

        if (empty(Yii::$app->user->identity) ) {
            return \Yii::$app->runAction('/site/login');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new NoticeBoard();

         try {
            if ($model->load(Yii::$app->request->post())) {

                $model->created_by = Yii::$app->user->identity->getId();
                //if u own the school
                $schoolId = Yii::$app->user->identity->school_id;
                $model->school_id = $schoolId;
                if($model->start_date >$model->end_date){
                    throw new Exception('Start date cannot be greater than end date');
                }

                if( $model->save(false) ){
                    Logs::logEvent("Created NoticeBoard : " . $model->id, null, null);
                }
                 
                $transaction->commit();
                
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

             \Yii::$app->session->setFlash('viewError', $error);
            Logs::logEvent("Failed create new NoticeBoard : ", $error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);

    }


    /**
     * Displays a single NoticeBoard  model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {  
        // if (!Yii::$app->user->can('r_timetable') || \Yii::$app->user->can('non_student') )
        //     throw new ForbiddenHttpException('No permissions to view schools timetable');
        
        if (empty(Yii::$app->user->identity) ) {
             
            return \Yii::$app->runAction('/site/login');
        }
        
        $model = $this->findModel($id);



        $responseData = [
            'model' => $model
        ];

        return ((Yii::$app->request->isAjax)) ? 
            $this->renderAjax('view',$responseData) 
                :
            $this->render('view', $responseData);


    }


    /**
     * Updates an existing NoticeBoard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('rw_announcements')) {
            throw new ForbiddenHttpException('Sorry, you don not have permissions to update Announcements. ');
        }

        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post())) {

                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                if( $model->save(false) ){
                    Logs::logEvent("Updated the NoticeBoard: " . $model->id, null, null);
                    //$model->saveAndUpdateUploads(); //ofcourse first delete old uploads n upload new ones
                }
                return $this->redirect(['view', 'id' => $model->id]);

            }
        } catch (Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
        }


        //$model->date_created = ($model->date_created)? date('Y-m-d', strtotime($model->date_created)) : '' ;

        return $this->render('update', [
            'model' => $model,
        ]);
    }








}
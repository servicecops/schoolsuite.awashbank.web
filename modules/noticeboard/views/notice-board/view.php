<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\models\User;
use app\modules\dissertation\models\Dissertation;


/* @var $this yii\web\View */
/* @var $model app\models\Classes */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$staffInfo = empty($model->supervisor) ? '' : CoreStaff::findOne($model->supervisor);
$supervisor = $staffInfo ? $staffInfo->first_name . ' ' . $staffInfo->last_name : '';

?>


<div class="row ">
    <div class="col-md-12">
        <h3><i class="fa fa-plus"></i> <?= ($model->title) ? $model->title : "--" ?></h3>
        <hr class="l_header"></hr>
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>



    <div class="col-md-12">
        <?php if (Yii::$app->user->can('rw_announcements')) : ?>
        <div class="pull-right">

                <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this announcement?',
                        'method' => 'post',
                    ],
                ]) ?>




        </div>
        <?php endif; ?>
    </div>


    <br><br>

    <div class="col-md-12 ">
        <div class="row">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Title</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->title) ? $model->title : ' --' ?></div>
            </div>
            <hr>
            <hr class="l_header">


            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Start Date</div>
                <div class="col-md-6"
                     style=" background: #fff;padding:10px"><?= ($model->start_date) ? date('Y-m-d', strtotime($model->start_date)) : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">End Date</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->end_date) ? date('Y-m-d', strtotime($model->end_date)) : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Announcement/Notice</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= ($model->announcement) ? $model->announcement : ' --' ?></div>
            </div>
            <hr class="l_header">
            <div class="col-md-12 ">
                <div class="col-md-3" style="background: #F0F3FF; padding:10px;margin-bottom: 5px;">Uploaded By</div>
                <div class="col-md-6"
                     style="background: #fff; padding:10px"><?= $model->created_by ? User::findOne($model->created_by)->username : "--" ?></div>
            </div>
        </div>


        <br>
    </div>


</div>

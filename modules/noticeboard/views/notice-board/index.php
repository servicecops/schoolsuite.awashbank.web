<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planner\models\TimetableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Announcements';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row core-school-index">
    <?php

    $columns = [
        [
            'label' => 'Title',
            'value' => function ($model) {
                return Html::a($model['title'], [
                    'view', 'id' => $model['id']
                ]);

            },
            'format' => 'raw',
        ],
        'announcement',


        [
            'label' => 'Start Date',
            'value' => function ($model) {

                return date('Y-m-d', strtotime($model->start_date)) ;
            },
            'format' => 'raw'

        ],
        [
            'label' => 'End Date',
            'value' => function ($model) {

                return date('Y-m-d', strtotime($model->end_date)) ;
            },
            'format' => 'raw'

        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-eye"></i>', ['view', 'id' => $model['id']]);
            },
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-edit"></i>', ['update', 'id' => $model['id']]);
            },
        ]
        ////
    ];
    ?>


    <div class="row">

        <div class="col-md-12" style="margin-top:20px">

            <div class="col"><span style="font-size:20px;color:#2c3844">&nbsp;<i
                            class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
        </div>
        <div class="col-md-12">



                <div class="float-right">
                    <?php
                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $columns,
                        'target' => '_blank',
                        'exportConfig' => [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            ExportMenu::FORMAT_EXCEL => false,

                        ],
                        'dropdownOptions' => [
                            'label' => 'Export Announcements',
                            'class' => 'btn btn-outline-secondary'
                        ]
                    ])
                    ?>

        </div>
    </div>
    <div class="col-md-12">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <div class="col-md-12">

        <div class="float-right">
            <?php if (Yii::$app->user->can('rw_announcements')) : ?>

                <?= Html::a('<i class="fa fa-plus"></i> Add Announcements', ['create',], ['class' => 'btn btn-sm btn-primary aclink']) ?>
            <?php endif; ?>

        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => $columns,
            'resizableColumns' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>
</div>

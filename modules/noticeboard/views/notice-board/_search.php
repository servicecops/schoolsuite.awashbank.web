<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use app\modules\planner\models\CoreStaff;
use app\modules\schoolcore\models\CoreStudent;

?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>

    <div class="col-md-8">
        <?= $form->field($model, 'title', ['inputOptions' => ['class' => 'form-control input-sm']])->textInput(['placeHolder' => 'Search By Title'])->label(false) ?>

    </div>


    <div class="col-md-3">

        <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>


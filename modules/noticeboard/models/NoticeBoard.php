<?php

namespace app\modules\noticeboard\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\base\NotSupportedException;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * This is the model class for table "dissertation".
 *
 * @property int $id
 * @property string $title
 * @property int $student_id
 * @property string|null $degree_program
 * @property string|null $submission_date
 * @property string $department
 * @property string $summary
 * @property int $supervisor
 * @property int $school_id
 * @property int|null $created_by
 */
class NoticeBoard extends \yii\db\ActiveRecord
{   
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notice_board';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'start_date', 'end_date', 'announcement',  'school_id'], 'required'],
            [['title', 'announcement', ], 'string'],
            [[ 'school_id', 'created_by'], 'default', 'value' => null],
            [['start_date','end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'start_date' => 'Student ID',
            'end_date' => 'Degree Program',
            'announcement' => 'Announcement',
            'school_id' => 'School ID',
            'created_by' => 'Created By',
        ];
    }




}

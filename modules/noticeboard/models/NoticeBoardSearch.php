<?php

namespace app\modules\noticeboard\models;

use app\modules\noticeboard\models\NoticeBoard;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * NoticeBoardSearch represents the model behind the search form of `app\modules\NoticeBoard\models\NoticeBoard`.
 */
class NoticeBoardSearch extends NoticeBoard
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','summary', 'submission_date', 'student_id' ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $schoolId = Yii::$app->user->identity->school_id;

        $query = NoticeBoard::find()->where([
                                  'school_id'=> $schoolId
                ]);
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        


        

        $query->andFilterWhere(['ilike', 'title', $this->title]);

         //what about start date and end date.
        //  $query->andFilterWhere(['>=', 'start_time', $this->start_time ]);
        //  $query->andFilterWhere(['<=', 'end_time', $this->end_time ]);


        return $dataProvider;
    }

}
<?php

use app\modules\feesdue\models\FeesDue;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>
<?php

$fees = FeesDue::find()->where(['school_id'=>$model->school_id, 'approval_status'=>true, 'recurrent' => false])
    ->andWhere("end_date >= '".date("Y-m-d")."'")
    ->andWhere("recurrent = false")
    ->orderBy('date_created')
    ->all();

if($state){
  echo $state['message'];
  }?>

<div class="col-xs-12 no-padding" style="background: #01b5d2">
<div class="wizard">
    <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">1</span> Edit Group Information</a>
    <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">2</span> Edit Group Members</a>
    <a  class="col-xs-3 current"><span class="badge badge-inverse">3</span> Assign Fee</a>
</div>
</div>

<div class="col-xs-12" style="background: white;padding:10px; margin-bottom: 5px;">
<div class="row">
 <div class="col-xs-12">
    <span style="font-size: 16px; color:#3c8dbc;">Note: A fee will only be available if it's <b>APPROVED</b> and  between  it's <b>effective & end </b>date</span>
    <br><br>
 </div>
<div class="col-xs-12 no-pading">
    <span style="font-size:20px;"><b><?= "Group Members - ".ucwords($model->group_name)."</b> (".$model->school->school_name.")" ?></span>
    <?php if($members) : 

    $form = ActiveForm::begin([
            'action' => ['assign-fee', 'id'=>$model->id],
            'method' => 'post',
            'options'=>['class'=>'formprocess'],
        ]); 
    ?>
    <div>
    <div class="col-xs-3 no-padding" >
    <?= $form->field($model2, 'fee_id')->dropDownList(
            ArrayHelper::map($fees, 'id', 'selectDesc'), ['prompt'=>'Select Fee', 'class'=>'form-control input-sm']
            )->label(false) ?>       
    </div>

      <?= Html::submitButton('<b>Assign A Fee</b>', ['class' => 'btn btn-primary btn-sm', 'data-confirm'=>'Please confirm to apply this fee' ]) ?>
      <?php ActiveForm::end(); ?>
  <?php endif;?>
  <div class="pull-right">
      <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>"><button class="btn btn-default btn-sm">Back to Groups</button></a>
  </div>
  </div>
  </div>

</div>   
    
</div>

<?= $this->render('_action_group_members',
    [
        'members'=>$members,
    ]);
?>

<?php

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreStudent */


$tableOptions = isset($tableOptions) ? $tableOptions : 'table table-responsive-sm table-bordered table-striped table-sm';
$layout = isset($layout) ? $layout : "{summary}\n{items}";

use Da\QrCode\QrCode;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii2assets\printthis\PrintThis;

?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">
    <?php if ($schDetails): ?>

        <div id="PrintPage">
            <p>
                <?php
                echo PrintThis::widget([
                    'htmlOptions' => [
                        'id' => 'PrintPage',
                        'btnClass' => 'btn btn-info',
                        'btnId' => 'PrintPage',
                        'btnText' => 'Print Student Reports',
                        'btnIcon' => 'fa fa-print'
                    ],
                    'options' => [
                        'debug' => false,
                        'importCSS' => true,
                        'importStyle' => false,
                        'loadCSS' => "path/to/my.css",
                        'pageTitle' => "",
                        'removeInline' => false,
                        'printDelay' => 333,
                        'header' => null,
                        'formValues' => true,
                    ]
                ]);
                ?>
            </p>

            <?php /** @var TYPE_NAME $output */
            foreach ($schDetails as $k => $v) {

                ?>
                <div style="break-after:page">
                    <div class="container mt-5 mb-5 card">
                        <div class="card-body" style="border: 2px solid #0068AD;">
                            <div class="row">
                                <div class="col">

                                </div>
                                <div class="col-md-auto text-center">
                                    <div>
                                        <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>
                                    </div>
                                    <div>
                                        <h2>
                                            <?php
                                            echo $v['school_name']; ?>
                                        </h2>
                                    </div>
                                    <div>
                                        P.O.BOX <?= $v['box_no']; ?> <?= $v['district']; ?>
                                    </div>
                                    <div>
                                        Tel Office: <?= $v['phone_contact_1']; ?>
                                    </div>
                                    <div>
                                        Email: <?= $v['contact_email_1']; ?>
                                    </div>
                                </div>
                                <div class="col">

                                </div>
                            </div>
                            <hr class="l_header mt-4" style="border-top:2px solid #0068AD;">
                            <div>

                                <h2 class="text-uppercase text-center"> <?php if (isset($term)) {
                                        echo $term['term_name'];
                                    } ?>- Report</h2>
                                <div class="row">
                                    <div class="col-md-12">

                                        ?>

                                        <table style="margin: 0px auto;width: 953px;">
                                            <tr>
                                                <td>
                                                    <span style="font-weight: 500;font-size: 17px; color: #504e4e">Student Name: </span>
                                                    <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; "><?php echo $v['first_name'] . ' ' . $v['middle_name'] . ' ' . $v['last_name'] ?></span>
                                                </td>
                                                <td>
                                                    <span style="font-weight: 500;font-size: 17px; color: #504e4e">Student Code: </span>
                                                    <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; "><?php echo $v['student_code'] ?></span>
                                                </td>
                                                <td>
                                                    <span style="font-weight: 500;font-size: 17px; color: #504e4e">Position: </span>
                                                    <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; ">....</span>
                                                </td>


                                            </tr>

                                            <tr>
                                                <td>
                                                    <span style="font-weight: 500;font-size: 17px; color: #504e4e">Class Name: </span>
                                                    <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; ">
                           <?php echo $v['class_description'] ?>
                        </span>
                                                </td>


                                                <td>
                                                    <span style="font-weight: 500;font-size: 17px; color: #504e4e">Year: </span>
                                                    <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; ">
                           <!-- --><?php /*echo date('Y', strtotime($v['term']['term_ends'])); */ ?>
                        </span>
                                                </td>
                                                <td>
                                                    <span style="font-weight: 500;font-size: 17px; color: #504e4e">Next term begins on: </span>
                                                    <span style="width: 100%;text-align: left;border-bottom: 1px solid #000; line-height: 0.1em; margin: 10px 0 20px;padding:0 0px; ">
                            .................
                        </span>
                                                </td>
                                            </tr>

                                            <tr>


                                            </tr>
                                        </table>
                                        <hr class="l_header mt-4" style="border-top: 2px solid #0068AD;">
                                    </div>

                                </div>

                                <div style="width:150px"></div>
                                <h2 class="text-uppercase text-center"> Results</h2>
                                <div class="row">


                                    <table class="table table-striped table-bordered detail-view">
                                        <thead>
                                        <tr>
                                            <th>Subject Code</th>
                                            <th>Subject Name</th>
                                            <?php if ((strcasecmp('SEMESTER', $model->term_type) == 0)) { ?>
                                                <th> I SEM</th>
                                                <th> II SEM</th>

                                            <?php } else if ((strcasecmp('QUARTER', $model->term_type) == 0)) { ?>

                                                <th> I QUA</th>
                                                <th> II QUA</th>
                                                <th> III QUA</th>
                                                <th> IV QUA</th>

                                            <?php } ?>

                                            <th>Average</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        $finalResults = Yii::$app->db->createCommand('select crgr.id,crgr.final_mark, crgr.grade, crgr.comment, cs2.school_name ,
       csc.id as class_id,
       cs2.school_code ,cs2.box_no,cs2.district,
       cs2.phone_contact_1,cs2.contact_email_1, cs.first_name , cs.middle_name ,cs.student_code, cs.last_name , crgr.final_mark ,
       crgr.final_mark_semester_one,crgr.final_mark_semester_two,crgr.final_mark_quarter_one,crgr.final_mark_quarter_two,crgr.final_mark_quarter_three,crgr.final_mark_quarter_four,
       cs3.subject_name ,
       cs3.subject_code , csc.class_code ,csc.class_description
from cumulative_marks_for_subject_per_semester crgr inner join core_student cs 
on cs.id = crgr.student_id 
inner join core_school cs2 on cs2.id=cs.school_id 
inner join core_subject cs3 on cs3.id = crgr.subject_id 
inner join core_school_class csc  on csc.id =cs.class_id 
where crgr.report_id =:reportId
                and crgr.student_id =:stdId
                order by subject_code asc')
                                            ->bindValue(':stdId', $v['student_id'])
                                            ->bindValue(':reportId', $v['report_id'])
                                            ->queryAll();

                                        $semoneTT = $semtwoTT = $qtoneTT = $qttwoTT = $qtthreeTT = $qtfourTT = 0;
                                        $semoneX = $semtwoX = $qtoneX = $qttwoX = $qtthreeX = $qtfourX = 0;
                                        $semoneY = $semtwoY = $qtoneY = $qttwoY = $qtthreeY = $qtfourY = 0;
                                        $semoneXA = $semtwoXA = $qtoneXA = $qttwoXA = $qtthreeXA = $qtfourXA = 0;
                                        $semoneYA = $semtwoYA = $qtoneYA = $qttwoYA = $qtthreeYA = $qtfourYA = 0;


                                        foreach ($finalResults as $kr => $vr) {
                                            Yii::trace($vr);

                                            $subjects = $vr["subject_code"];


                                            if (isset($vr['subject_name'])) {
                                                $subject = $vr['subject_name'];
                                            } else {
                                                $subject = '--';
                                            }
                                            // $semoneY = $semoneY++;
//                    $semtwoY= $semtwoY++;
//                    $qtoneY = $qtoneY++;
//                    $qttwoY = $qttwoY++;
//                    $qtthreeY= $qtthreeY++;
//                    $qtfourY= $qtfourY++;

                                            ?>

                                            <tr>

                                                <td> <?php echo $vr['subject_code'] ?> </td>
                                                <td> <?php echo $vr['subject_name'] ?> </td>
                                                <?php if ((strcasecmp('SEMESTER', $model->term_type) == 0)) { ?>
                                                    <td> <?php echo $vr['final_mark_semester_one'];
                                                        $semoneY++ ?> </td>
                                                    <td> <?php echo $vr['final_mark_semester_two'];
                                                        $semtwoY++ ?> </td>

                                                <?php } else if ((strcasecmp('QUARTER', $model->term_type) == 0)) { ?>

                                                    <td> <?php echo $vr['final_mark_quarter_one'];
                                                        $qtoneY++; ?> </td>
                                                    <td> <?php echo $vr['final_mark_quarter_two'];
                                                        $qttwoY++; ?> </td>
                                                    <td> <?php echo $vr['final_mark_quarter_three'];
                                                        $qtthreeY++; ?> </td>
                                                    <td> <?php echo $vr['final_mark_quarter_four'];
                                                        $qtfourY++; ?> </td>

                                                <?php } ?>

                                                <?php if ((strcasecmp('SEMESTER', $model->term_type) == 0)) { ?>
                                                    <td>
                                                        <?php
                                                        if (isset($vr['final_mark_semester_one']) &&
                                                            !isset($vr['final_mark_semester_two'])) {

                                                            echo $vr['final_mark_semester_one'];
                                                        } else if (isset($vr['final_mark_semester_two'])) {
                                                            echo(($vr['final_mark_semester_one'] + $vr['final_mark_semester_two']) / 2);
                                                        }

                                                        ?>
                                                    </td>


                                                <?php } else if ((strcasecmp('QUARTER', $model->term_type) == 0)) { ?>

                                                    <td>
                                                        <?php
                                                        if (isset($vr['final_mark_quarter_one']) &&
                                                            !isset($vr['final_mark_quarter_two']) && !isset($vr['final_mark_quarter_three']) && !isset($vr['final_mark_quarter_four'])) {

                                                            echo $vr['final_mark_quarter_one'];
                                                        } else if (
                                                            isset($vr['final_mark_quarter_two']) && !isset($vr['final_mark_quarter_three']) && !isset($vr['final_mark_quarter_four'])) {
                                                            echo(($vr['final_mark_quarter_one'] + $vr['final_mark_quarter_two']) / 2);
                                                        } else if (isset($vr['final_mark_quarter_three']) && !isset($vr['final_mark_quarter_four'])) {
                                                            echo(($vr['final_mark_quarter_one'] + $vr['final_mark_quarter_two'] + $vr['final_mark_quarter_three']) / 3);
                                                        } else if (isset($vr['final_mark_quarter_four'])) {
                                                            echo(($vr['final_mark_quarter_one'] + $vr['final_mark_quarter_two'] + $vr['final_mark_quarter_three'] + $vr['final_mark_quarter_four']) / 4);
                                                        }
                                                        ?>

                                                    </td>


                                                <?php } ?>



                                                <?php
                                                $semoneTT = $semoneTT + $vr['final_mark_semester_one'];
                                                $qtoneTT = $qtoneTT + $vr['final_mark_quarter_one'];
                                                $qttwoTT = $qttwoTT + $vr['final_mark_quarter_two'];
                                                $qtthreeTT = $qtthreeTT + $vr['final_mark_quarter_three'];
                                                $qtfourTT = $qtfourTT + $vr['final_mark_quarter_four'];
                                                $semtwoTT = $semtwoTT + $vr['final_mark_semester_two'];


                                                if ($semoneY > 0) {
                                                    $semoneYA = $semoneTT / $semoneY;

                                                }
                                                if ($semtwoY > 0) {
                                                    $semtwoYA = $semtwoTT / $semtwoY;

                                                }
                                                if ($qtoneY > 0) {
                                                    $qtoneYA = $qtoneTT / $qtoneY;

                                                }
                                                if ($qttwoY > 0) {
                                                    $qttwoYA = $qttwoTT / $qttwoY;

                                                }
                                                if ($qtthreeY > 0) {
                                                    $qtthreeYA = $qtthreeTT / $qtthreeY;

                                                }
                                                if ($qtfourY > 0) {
                                                    $qtfourYA = $qtfourTT / $qtfourY;

                                                }

                                                ?>
                                            </tr>


                                        <?php } ?>


                                        <tr>
                                            <td class="text-success">TOTAL</td>
                                            <td></td>

                                            <?php if ((strcasecmp('SEMESTER', $model->term_type) == 0)) { ?>
                                                <td> <?= ($semoneTT) ? $semoneTT : ' ' ?> </td>
                                                <td> <?= ($semtwoTT) ? $semtwoTT : ' ' ?> </td>

                                            <?php } else if ((strcasecmp('QUARTER', $model->term_type) == 0)) { ?>

                                                <td> <?= ($qtoneTT) ? $qtoneTT : ' ' ?> </td>
                                                <td> <?= ($qttwoTT) ? $qttwoTT : ' ' ?> </td>
                                                <td> <?= ($qtthreeTT) ? $qtthreeTT : ' ' ?> </td>
                                                <td> <?= ($qtfourTT) ? $qtfourTT : ' ' ?> </td>

                                            <?php } ?>
                                            <td></td>
                                            <td></td>
                                            <td></td>


                                        </tr>
                                        <tr>
                                            <td class="text-success">AVERAGE</td>
                                            <td></td>


                                            <?php if ((strcasecmp('SEMESTER', $model->term_type) == 0)) { ?>
                                                <td> <?= ($semoneYA) ? $semoneYA : ' ' ?> </td>
                                                <td> <?= ($semtwoYA) ? $semtwoYA : ' ' ?> </td>

                                            <?php } else if ((strcasecmp('QUARTER', $model->term_type) == 0)) { ?>

                                                <td> <?= ($qtoneYA) ? $qtoneYA : ' ' ?> </td>
                                                <td> <?= ($qttwoYA) ? $qttwoYA : ' ' ?> </td>
                                                <td> <?= ($qtthreeYA) ? $qtthreeYA : ' ' ?> </td>
                                                <td> <?= ($qtfourYA) ? $qtfourYA : ' ' ?> </td>

                                            <?php } ?>
                                            <td></td>
                                            <td></td>
                                            <td></td>


                                        </tr>


                                        </tbody>
                                    </table>

                                </div>
                                .

                                <div style="clear:both"></div>

                                <hr class="l_header mt-4" style="border-top: 2px solid #0068AD;">

                                <div class="row mt-3">

                                    <div class="col-2">Class Teacher's remarks:</div>
                                    <div class="col-10">
                                        <hr class="mt-4" style="border-top: 1px dotted #0068AD;padding:5px">
                                        <hr class="mt-4" style="border-top: 1px dotted #0068AD;padding:5px">
                                        <hr class="mt-4" style="border-top: 1px dotted #0068AD;">

                                    </div>
                                </div>


                                <div class="row mt-3">
                                    <div class="col-4"></div>
                                    <?php
                                    Yii::trace($staff);


                                    ?>
                                    <div class="col-8"><span><strong
                                                    class="mr-5"><strong>NAME</strong>: <?php if ($staff){
                                                    echo $staff['first_name'] . ' ' . $v['last_name'];
                                                } else{ ?>: .............. <?php } ?></span><br/> <span> <strong>SIGNATURE</strong>:
                <?php if ($staff): ?>
                    <img height="40" width="60" src="data:image/jpeg;base64,<?= $staff['image_base64'] ?>"
                    />
                <?php else : ?>
                    <span></span>
                <?php endif; ?>
                </span></div>
                                </div>

                                <hr class="mt-4" style="border-top: 1px dotted #0068AD;">

                                <div class="row mt-3">

                                    <div class="col-4">Head Teacher's remarks:</div>
                                    <div class="col-8">
                                        <hr class="mt-4" style="border-top: 1px dotted #0068AD;padding:5px">
                                        <hr class="mt-4" style="border-top: 1px dotted #0068AD;padding:5px">
                                        <hr class="mt-4" style="border-top: 1px dotted #0068AD;">
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-4"></div>
                                    <div class="col-8"><span class="mr-5"><strong>NAME</strong>: <?php if ($hm) {
                                                echo $hm['first_name'] . ' ' . $hm['last_name'];
                                            } else { ?>: .............. <?php } ?></span> <span>SIGNATURE: ................</span>
                                    </div>
                                </div>
                                <hr class="mt-2" style="border-top: 1px dotted #0068AD;">

                                <div class="row mt-3">
                                    <table style="width: 800px;">
                                        <tr>
                                            <td>
                                                <h5>Powered By</h5><br>
                                                <?= Html::img('@web/web/img/no_bg_logo.png', ['alt' => 'logo', 'class' => 'light-logo', 'height' => '60px']); ?>
                                            </td>
                                            <td style="text-align:right; ">
                                                <?php
                                                $qrCode = (new QrCode($v['student_code']))
                                                    ->setSize(100)
                                                    ->setMargin(5);
                                                echo '<img style="float: right;" src="' . $qrCode->writeDataUri() . '">';
                                                ?>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php

            }

            ?>
        </div>

    <?php elseif (!$schDetails) : ?>

        <div class="text-center">
            <?= Yii::$app->session->setFlash('danger', 'No records found'); ?>


        </div>

        <p><?= \yii\helpers\Html::a('Back to previous page', Yii::$app->request->referrer); ?></p>
    <?php endif; ?>
</div>

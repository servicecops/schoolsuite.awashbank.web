<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = $model->report_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">
    <div class="student-group-information-view">
        <div class="col-xs-12 no-padding" style="background: #efefef">
            <div class="wizard">
            </div>
        </div>

        <?php

        Yii::trace("teacher:  " . Yii::$app->user->can('is_teacher'));
        Yii::trace("school:  " . \app\components\ToWords::isSchoolUser());
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="letter">
                    <div class="row">
                        <div class='col-md-4'><h1><?= Html::encode($this->title) ?></h1></div>
                        <div class='col-md-8' style="padding-top:27px;">
    <span class="pull-right">

        <a class="aclink btn btn-default btn-sm" href="<?= Url::to(['student-report/index']) ?>">Back to Groups</a>


    </span>
                            <?php if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')): ?>

                                <?= Html::a('<i class="fa fa-plus"></i>Select Subject', ['student-report/select-subject', 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-sm']) ?>
                            <?php endif ?>

                            <?= Html::a('<i class="fa fa-edit"></i> Generate Report', ['the-class-report', 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-sm']) ?>
                            <?= Html::a('<i class="fa fa-edit"></i> Update', ['update', 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-sm']) ?>
                            <?= Html::a('<i class="fa fa-remove"></i> Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this Group?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                            

                        </div>
                    </div>
                    <hr class="l_header">

                    <?= DetailView::widget([
                        'model' => $model,
                        'options' => ['class' => 'table  detail-view'],
                        'attributes' => [
                            'report_name',
                            'report_description',
                            'date_created',
                            'date_modified',
                            'active:boolean',
                        ],
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Add Payment Code';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="model_titles">
    <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;<?php echo $this->title?></h3></div>
</div>

<div class="codes_input">
    <span style="font-size:20px;font-weight:500;">&nbsp;Paste payment codes (Separate with spaces or commas)</span>
    <?php $form = ActiveForm::begin([
        'action' => ['student-report', 'id'=>$id],
        'method' => 'post',
        'options'=>['class'=>'formprocess'],
    ]); ?>
    <textarea class="form-control" id="student_codes" name="student_codes" rows="6" placeholder="Paste student codes"></textarea>

    <?= Html::submitButton('<b>Submit</b>', ['class' => 'btn btn-primary btn-sm']) ?>
    <?php ActiveForm::end(); ?>
</div>


<?php
$script = <<< JS

$(function () {
    
$(".code_link").click(function(){
      $(".select_tbl").css('display', 'none');
      $(".codes_input").css('display', 'block');
    });
    $(".back_link").click(function(){
      $(".select_tbl").css('display', 'block');
      $(".codes_input").css('display', 'none');
    });
    
    $("#empty-group-button").bind('click', function() {
       $("#empty-group").val('empty-group') 
    })
});





JS;
$this->registerJs($script);
?>

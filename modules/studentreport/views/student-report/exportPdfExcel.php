<?php
use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="fees_due_index">
    <?php  
	if($type == 'Excel') {
		echo "<table><tr> <th colspan='7'><h3> Student Report Groups</h3> </th> </tr> </table>";
	}
    ?>
    <table class="table table-striped table-responsive">
        <thead>
        <tr><?php if(Yii::$app->user->can('schoolpay_admin')){echo "<th>School Name</th>"; } ?> <th>Date</th> <th>Report Name</th><th>Description</th><th>Active</th></tr>
        </thead>
        <tbody>
            <?php 
            foreach($query as $k=>$v) : ?>
                <tr>
                    <?php if(Yii::$app->user->can('schoolpay_admin')) : ?>
                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <?php endif; ?>
                    <td><?= ($v['date_created']) ? $v['date_created'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['report_name']) ? $v['report_name'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= ($v['report_description']) ? $v['report_description'] : '<span class="not-set">(not set) </span>' ?></td>
                    <td><?= $v['active']==true ?  "yes" : "no"; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
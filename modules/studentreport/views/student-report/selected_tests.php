<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action' => ['assign', 'id' => $model->id],
    'method' => 'post',
    'id' => 'assign-form',
    'options' => ['class' => 'formprocess'],
]);
?>

<input id="empty-group" name="empty-group" type="hidden" value="">

<div class="row">
    <div class="col-xs-12 no-pading">
        <span style="font-size:20px;font-weight:500;">&nbsp;Selected Tests</span><br>
        <?php if ($data) :
            Yii::trace($data);
            ?>

            <div class="pull-right">
                <?= Html::submitButton('<b>Empty Group <i class="fa fa-trash-o"></i></b>', ['name' => 'empty-group-button', 'id' => 'empty-group-button', 'class' => 'btn btn-danger btn-sm', 'data-confirm' => 'Are you sure you want to remove all tests from this report']) ?>
            </div>
            <div class="pull-right">
                <?= Html::submitButton('<b>Remove Selected Tests <i class="fa fa-trash"></i></b>', ['class' => 'btn btn-danger btn-sm generate-btn', 'data-confirm' => 'Are you sure you want to remove the selected tests from this report']) ?>
            </div>


            <div class="dropdown pull-right">
                <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Take Action
                    <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="clink"><a href="<?= Url::to(['student-report/calculate-finalmark', 'id'=>$model->id,'subId'=>$subId]) ?>">Generate Cumulative Marks
                            </a></li>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>
<fieldset>
    <div class="box-body table table-responsive no-padding">


        <table class="table table-striped">
            <thead style="background: white;padding:10px">
            <tr>
                <th style="padding:0 10px;"><span class="checkbox checkbox-danger checkbox-circle"
                                                  style="margin:6px;"><input type="checkbox" class="checkall"
                                                                             onclick="checkall()"
                                                                             id="check_all_id2"><label
                                for="check_all_id2">Select All</label></span></th>
                <th>Sr.No</th>
                <th>Test Description</th>
                <th>Test Date</th>
                <th>Subject Code</th>
                <th>Type</th>
                <th>Composition</th>

            </tr>
            </thead>
            <tbody>
            <?php
            if ($data) :
                $srNo = 1;
                foreach ($data as $k => $v) : ?>

                    <tr>
                        <td style=" git statuspadding:0 10px;">
                            <span class="checkbox checkbox-danger checkbox-circle" style="margin:6px;"><input type="checkbox"
                                                                                                class="checkbox"
                                                                                                name="remove_stu[]"
                                                                                                id="<?= $v['id']; ?>"
                                                                                                value="<?= $v['id'] ?>"><label
                                        class='col-xs-12 no-padding' for="<?= $v['id']; ?>"></label></span></td>
                        <td><?= $srNo; ?></td>
                        <td><?= ($v['test_description']) ? $v['test_description'] : '<span class="not-set"> -- </span>' ?></td>
                        <td><?= ($v['start_date']) ? $v['start_date'] : '<span class="not-set"> -- </span>' ?></td>
                        <td><?= ($v['subject_code']) ? $v['subject_code'] : '<span class="not-set"> -- </span>' ?></td>
                        <td><?= ($v['semester_type_id']) ? $v['semester_type_id'] : '<span class="not-set"> -- </span>' ?></td>
                        <td><?= ($v['assessment_composition']) ? $v['assessment_composition'] : '<span class="not-set"> -- </span>' ?></td>


                    </tr>

                    <?php
                    $srNo++;
                endforeach;
            else :
                ?>
                <td colspan="8">No Records found for the search Criteria</td>
            <?php endif; ?>

            </tbody>
        </table>


    </div>
</fieldset>
<?php ActiveForm::end(); ?>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = 'Add New Group';
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-group-information-create row hd-title" data-title="New Student Group">
    <div class='row'>
        <div class="col-md-12 no-padding" style="background: #efefef">
            <div class="wizard">
                <a class="current col-xs-4"><span class="badge">1</span> Create New Group</a>
                <a class="col-xs-4 gray"><span class="badge">2</span> Assign Students</a>
                <a class="col-xs-3 gray"><span class="badge badge-inverse">3</span> Take Action</a>
            </div>
            <br>
        </div>
    </div>
    <br>
</div>

<div class="row">
    <div class="col-md-12"><h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3><br></div>
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>


<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Student Group Informations';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">Reports Information</p>

<div class="letter">
    <div class="core-student-group-information-index hd-title" data-title="Student Groups">
        <div class="row" style="background: white;padding:10px;">
            <span style="font-size: 16px; color:#3c8dbc;">Use a  report group to temporarily put together tests to be considered in when generating a report book. Think of a group as a "basket" of tests.</span><br><br>
<br/>
            <span>Deleting a test group does not delete the tests from the system.</span> <br>

        </div>

        <div class="row">


            <div class="col-md-5 "><h3 class="box-title"><i class="fa fa-th-list"></i> &nbsp;&nbsp;Report Groups</h3>
            </div>

            <?php
            $col_w = 9;
            if (Yii::$app->user->can('schoolpay_admin')) :
                $col_w = 4 ?>
                <div class="col-md-5 " style="padding-top: 20px !important;">
                    <?= $this->render('_search_group', ['searchModel' => $searchModel]); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-8 " style="padding-top: 20px !important;">
                <div class="pull-right">
                    <?= Html::a('<i class="fa fa-plus "></i>&nbsp;&nbsp;  Add New Report', ['student-report/class-group'], ['class' => 'btn btn-primary btn-sm pull-right']) ?>
                    <?= Html::a('<i class="fa fa-file-excel-o"></i>&nbsp;&nbsp; PDF', ['export-pdf', 'model' => get_class($searchModel)], ['class' => 'btn btn-danger pull-right', 'target' => '_blank']) ?>
                    <?= Html::a('<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp; EXCEL', ['/export-data/export-excel', 'model' => get_class($searchModel)], ['class' => 'btn btn-primary  pull-right', 'target' => '_blank']) ?>

                </div>
            </div>

        </div>
        <div class="row">
                <table class="table table-striped">
                    <thead class="bg-colorz table thead">
                    <tr><?php if (Yii::$app->user->can('schoolpay_admin')) {
                            echo "<th>" . $sort->link('school_name') . "</th>";
                        } ?>
                        <th><?= $sort->link('date_created') ?></th>
                        <th>Class Code</th>
                        <th><?= $sort->link('report_name') ?></th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($dataProvider) :
                        foreach ($dataProvider as $k => $v) : ?>
                            <tr>
                                <td><?= ($v['date_created']) ? date('Y-m-d h:g:i', strtotime($v['date_created'])) : '<span class="not-set">(not set) </span>' ?></td>
                                <?php if (Yii::$app->user->can('schoolpay_admin')) : ?>
                                    <td><?= ($v['school_name']) ? $v['school_name'] : '<span class="not-set">(not set) </span>' ?></td>
                                <?php endif; ?>
                                <td><?= ($v['class_code']) ? $v['class_code'] : '<span class="not-set">(not set) </span>' ?></td>

                                <td class="clink"><a
                                            href="<?= Url::to(['student-report/view-sem-report', 'id' => $v['id']]) ?>"><?= ($v['description']) ? $v['description'] : '<span class="not-set">(not set) </span>' ?></a>
                                </td>
                                <td><?= $v['active'] == true ? "<i class='fa  fa-check'></i>" : "no"; ?></td>

                                <td class="clink"><a
                                            href="<?= Url::to(['student-report/terms', 'id' => $v['id']]) ?>"><i class='fa  fa-exchange'></i></a>
                                </td>
                            </tr>
                        <?php endforeach;
                    else : ?>
                        <tr>
                            <td colspan="6">No group found</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
                <?= LinkPager::widget([
                    'pagination' => $pages['pages'],
                ]); ?>
                <div style="padding-bottom: 180px;"><p>&nbsp;</p></div>

        </div>
    </div>
</div>

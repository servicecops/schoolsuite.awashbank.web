<?php
use yii\helpers\Url;
$average = '(This report uses average value to compute final marks)';
$composition = '(This report uses compositions assigned to compute final report)';
?>
<div class="row">
    <div class="col-md-12">
        <div style="display: table;margin: auto;">
            <h2><?php echo $reportName['report_name']; ?></h2>
            <i style="color:red"><?php if ($reportName['average']) {
                    echo $average;
                } else {
                    echo $composition;
                }; ?></i>
        </div>
    </div>
    <span class="col-md-12"></span>
</div>
<div style="width:150px"></div>
<div class="row">
    <div class="dropdown pull-right">
        <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Generate Student Report
            <span class="caret"></span></button>
        <ul class="dropdown-menu dropdown-menu-right">
            <?php
            if($average){?>
                <li class="clink"><a href="<?= Url::to(['student-report/the-class-report', 'id' => $id]) ?>">For Class</a>
            </li>
            <li class="clink"><a href="<?= Url::to(['student-report/the-student-report', 'id' => $id,]) ?>">For
                    Student</a></li>
         <?php  } else{?>
                <li class="clink"><a href="<?= Url::to(['student-report/the-class-report-composition', 'id' => $id]) ?>">For Class</a>
                </li>
                <li class="clink"><a href="<?= Url::to(['student-report/the-student-report-composition', 'id' => $id,]) ?>">For
                        Student</a></li>
                <?php
            }
            ?>

        </ul>
    </div>


</div>

<div class="row">

    <table class="table table-striped table-bordered detail-view">
        <thead>
        <tr>
            <th>Subject Name</th>
            <th>Subject Name</th>
            <th>Test Description</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($results
                 as  $k=>$v) { ?>
            <tr>
                <td>
                    <?php echo  $v['subject_code'] ?>
                </td>
                <td>
                    <?php echo  $v['subject_name'] ?>
                </td>

                <td>
                    <?php echo $v['test_description'] ?>
                </td>
              
            </tr>
        <?php }

        ?>
        </tbody>
    </table>


</div>


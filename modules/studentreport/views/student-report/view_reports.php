<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Semester Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">
    <div class="student-group-information-view">
        <div class="col-xs-12 no-padding" style="background: #efefef">
            <div class="wizard">
            </div>
        </div>

        <?php

        Yii::trace("teacher:  " . Yii::$app->user->can('is_teacher'));
        Yii::trace("school:  " . \app\components\ToWords::isSchoolUser());
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="letter">
                    <div class="row">
                        <div class='col-md-4'><h1><?= Html::encode($this->title) ?></h1></div>
                        <div class='col-md-8' style="padding-top:27px;">
    <span class="pull-right">

        <a class="aclink btn btn-default btn-sm" href="<?= Url::to(['student-report/index']) ?>">Back to Groups</a>
 <?php if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')): ?>

     <?= Html::a('<i class="fa fa-plus"></i>Select Subject', ['student-report/select-subject', 'id' => $model->id], ['class' => 'aclink btn btn-primary btn-sm']) ?>
 <?php endif ?>

    </span>



                        </div>
                    </div>
                    <hr class="l_header">


                    <div class="col-md-12">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;">
                                <b>Description</b></div>
                            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= $model->description ?></div>
                        </div>
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;"><b>School Name</b></div>
                            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->school_id) ? \app\modules\schoolcore\models\CoreSchool::findOne(['id' => $model->school_id])->school_name : "--" ?></div>
                        </div>

                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class="col-md-3 col-sm-3 col-xs-6 profile-text" style="background: #e9f4fb !important;">
                                <b>Class Code</b></div>
                            <div class="col-md-9 col-sm-9 col-xs-6 profile-text"><?= ($model->class_id) ? \app\modules\schoolcore\models\CoreSchoolClass::findOne(['id' => $model->class_id])->class_code : "--" ?></div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

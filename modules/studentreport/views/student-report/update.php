<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = 'Edit Group Information: ' . ' ' . $model->report_name;
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-group-information-update row hd-title" data-title="Edit Group Information">
<div class='col-xs-12'>
<div class="col-xs-12 no-padding" style="background: #efefef">
<div class="wizard">
   </div>
</div>
</div>
<div class='col-xs-12'>
<div class="letter">
  <div class="row">
	<div class="col-xs-12"> <h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3></div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
</div>
</div>

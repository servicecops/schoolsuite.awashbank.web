<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student_group_form">
    <?php
    if($this->context->action->id == 'update')
        $action = ['update', 'id'=>$_REQUEST['id']];
    else
        $action = ['create'];
    ?>

    <?php $form = ActiveForm::begin([
        'action' => $action,
        'method' => 'post',
        'options'=>['class'=>'formprocess'],
    ]); ?>
    <div class="row">
        <?php if(!\app\components\ToWords::isSchoolUser()): ?>
            <?php if($model->isNewRecord): ?>
                <div class="col-md-3 col-sm-4 no-padding-right">
                    <?php
                    $url = Url::to(['/schoolcore/core-school/active-schoollist']);
                    $selectedSchool = empty($model->school_id) ? '' : \app\modules\schoolcore\models\CoreSchool::findOne($model->school_id)->school_name;
                    echo $form->field($model, 'school_id')->widget(Select2::classname(), [
                        'initValueText' => $selectedSchool, // set the initial display text
                        'size'=>'sm',
                        'theme'=>Select2::THEME_BOOTSTRAP,
                        'options' => [
                            'placeholder' => 'Assign School',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                            ],
                            'ajax' => [
                                'url' => $url,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],

                        ],
                    ])->label(false); ?>
                </div>

            <?php endif; ?>
        <?php endif; ?>


        <div class="col-md-3 col-sm-4 no-padding-right">
            <?= $form->field($model, 'report_name', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Enter Report Name']])->textInput(['maxlength' => true])->label(false) ?>
        </div>

        <div class="col-md-3 col-sm-4 no-padding-right">
            <?= $form->field($model, 'report_description', ['inputOptions'=>[ 'class'=>'form-control input-sm', 'placeholder'=> 'Description']])->textInput(['maxlength' => true])->label(false) ?>
        </div>



    <div class="col-md-3 col-sm-4 no-padding-right">
        <input type="checkbox" id="report-active" name="Report[active]" value="1" checked>
        <label for="report-active">Active</label>
    </div>



    <div class="col-md-2 no-padding-right">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Add Group' : 'Edit Group', ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-info btn-sm']) ?>
    </div>
    </div>
    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupInformation */

$this->title = 'Add Report Group';
$this->params['breadcrumbs'][] = ['label' => 'Student Group Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">
<div class="student-group-information-create row hd-title" data-title="New Report Group">

    <div class="row" style="background: #efefef">
        <div class="wizard">
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12"><h3><i class="fa fa-plus"></i>&nbsp;&nbsp;<?= Html::encode($this->title) ?></h3></div>
    <p>&nbsp;</p>
</div>
<?php $form = ActiveForm::begin([
    'method' => 'post',
    'options' => ['class' => 'formprocess'],
]); ?>
    <p>All fields marked * are required</p>


<div class="row">

    <?php if (\app\components\ToWords::isSchoolUser()) : ?>
        <div class="col-md-4 col-sm-4 no-padding-right">
            <?php
            $data = CoreSchoolClass::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

            echo $form->field($model, 'class_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'class_code'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['placeholder' => 'Find Class'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Class *'); ?>
        </div>
    <?php endif; ?>



    <div class="col-md-4 col-sm-4 no-padding-right">
        <?= $form->field($model, 'report_name', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Enter Report Name']])->textInput(['maxlength' => true])->label('Report Name *') ?>
    </div>


    <div class="col-md-4 col-sm-4 no-padding-right">
        <?= $form->field($model, 'report_description', ['inputOptions' => ['class' => 'form-control input-sm', 'placeholder' => 'Description']])->textInput(['maxlength' => true])->label('Report Description *') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'term_type', ['inputOptions' => ['class' => 'form-control',
            'placeholder' => 'Term Type',
        ]])->dropDownList(
            ['SEMESTER' => 'Semester', 'QUARTER' => 'Quarter'],
            ['id' => 'term_type_select', 'prompt' => 'Term Type']
        )->label('Term Type') ?>
    </div>


    <div class="col-md-12 col-sm-4 no-padding-right">
        <p>Activate Report</p>
        <div class="checkbox checkbox-info">
            <input type="checkbox" id="report-active" name="Report[active]" value="1"
                   checked>
            <label for="corestudentgroupinformation-active">Active</label>
        </div>
    </div>


    <div class="col-md-2 no-padding-right">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Add Report Group' : 'Edit Group', ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-info btn-sm']) ?>
        </div>
    </div>
</div>


<?php ActiveForm::end(); ?>


</div>

<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$classes = $model->classes ? $model->classes : [];
$script = <<< JS
    $("document").ready(function(){ 
       var sch = $("#group_school_search").val();
        if(sch){
            $.post( '$url/'+sch, function( data ) {
                $( "select#group_class_select" ).html(data);
            })
        }
      
    });
JS;
$this->registerJs($script);
?>
<?php
$url = Url::to(['/schoolcore/core-school-class/lists']);
$cls = $model->class_id;


$script = <<< JS
var schoolChanged = function() {
        var sch = $("#school_search").val();
        $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
        
        
         $.get('$url', {id : sch}, function( data ) {
                    $('select#student_class_drop').html(data);
                    $('#student_class_drop').val('$cls');
                });
        
    
    }
    
$("document").ready(function(){
    
    if($("#school_search").val()){
        schoolChanged();
    }
    
    $('body').on('change', '#school_search', function(){
         schoolChanged();
    });
  });
JS;
$this->registerJs($script);
?>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\feesdue\models\FeesDue;
use app\modules\payment_plan\models\PaymentPlan;
use yii\widgets\ActiveForm;
?>
<?php if($state){
    echo $state['message'];
}?>

<div class="col-xs-12 no-padding" style="background: #01b5d2">
    <div class="wizard">
        <a href="<?= Url::to(['core-student-group/update', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">1</span> Edit Group Information</a>
        <a href="<?= Url::to(['core-student-group/assign', 'id'=>$model->id]) ?>" class="aclink col-xs-4"><span class="badge">2</span> Edit Group Members</a>
        <a  class="col-xs-3 current"><span class="badge badge-inverse">3</span> Assign Payment Plan</a>
    </div>
</div>

<div class="col-xs-12" style="background: white;padding:10px; margin-bottom: 5px;">
    <div class="row">
        <div class="col-xs-12">
            <span style="font-size: 16px; color:#3c8dbc;">Note: A fee will only be available if it's <b>APPROVED</b> and  between  it's <b>effective & end </b>date</span>
            <br><br>
        </div>
        <div class="col-xs-12">
            <span style="font-size:20px;"><b><?= "Group Members - ".ucwords($model->group_name)."</b> (".$model->school->school_name.")" ?></span>
            <?php if($members) :

            $form = ActiveForm::begin([
                'action' => ['assign-plan', 'id'=>$model->id],
                'method' => 'post',
                'options'=>['class'=>'formprocess'],
            ]);
            ?>
            <div class="col-xs-12 no-padding">
                <div class="col-xs-3 no-padding" >
                    <?= $form->field($model2, 'fee_id')->dropDownList(
                        ArrayHelper::map(FeesDue::find()->where(['school_id'=>$model->school_id, 'approval_status'=>true])->andWhere("effective_date <= '". date("Y-m-d")."' AND end_date >= '".date("Y-m-d")."'")->orderBy('date_created')->all(), 'id', 'selectDesc'), ['prompt'=>'Select Fee', 'class'=>'form-control input-sm']
                    )->label(false) ?>
                </div>
                <div class="col-xs-3 no-padding" >
                    <?= $form->field($model2, 'payment_plan')->dropDownList(
                        ArrayHelper::map(PaymentPlan::find()->where(['school_id'=>$model->school_id])->all(), 'id', 'description'), ['prompt'=>'Select Plan', 'class'=>'form-control input-sm']
                    )->label(false) ?>
                </div>

                <?= Html::submitButton('<b>Assign Payment Plan</b>', ['class' => 'btn btn-primary btn-sm', 'data-confirm'=>'Please confirm to apply this fee' ]) ?>
                <?php ActiveForm::end(); ?>
                <?php endif;?>
                <div class="pull-right">
                    <a class="aclink" href="<?= Url::to(['core-student-group/index']) ?>"><button class="btn btn-default btn-sm">Back to Groups</button></a>
                </div>
            </div>
        </div>

    </div>
</div>

<?= $this->render('_action_group_members',
    [
        'members'=>$members,
    ]);
?>

<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;


$disableControls = false;

$allowEdit = true;
?>
<?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>


<?php
$form = ActiveForm::begin([
'action' => ['assign', 'id' => $model->id, 'subId'=>$subId],
'method' => 'post',
'options' => ['class' => 'formprocess'],
]); ?>
<div class="row">
    <div class="col-xs-12 no-pading">
        <span style="font-size:20px;font-weight:500px;">&nbsp;Add Subject Assignments</span>
        <?php if ($data) : ?>
            <div class="pull-right">
                <?= Html::submitButton('<b>Assign </b>', ['class' => 'btn btn-primary btn-sm generate-btn', 'data-confirm' => 'Confirm to add selected assignments']) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<fieldset>
    <div class="box-body table table-responsive no-padding">
        <table class="table">
            <thead>
            <tr style="background: white;padding:10px">

                <th>Test Description</th>
                <th>Test Date</th>
                <th>Subject code</th>
                <th class='clink'>Total Mark(%)</th>
                <th class='clink'></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($data) :
                $srNo = 1;
                foreach ($data as $k => $v) : ?>

                    <tr>

                        <td>
                            <?= ($v['test_description']) ? $v['test_description'] : '<span class="not-set"> -- </span>' ?></td>
                        <td><?= ($v['start_date']) ? $v['start_date'] : '<span class="not-set"> -- </span>' ?></td>
                        <td>
                            <input type="hidden" name="selectedtest[subject_id]"
                                   id="<?= $v['subject_id']; ?>"
                                   value="<?= $v['subject_id'] ?>"/>
                            <?= ($v['subject_code']) ? $v['subject_code'] : '<span class="not-set"> -- </span>' ?></td>
                        <td>
                            <input type="hidden" name="selectedtest[total_marks]"
                                   id="<?= $v['total_marks']; ?>"
                                   value="<?= $v['total_marks'] ?>"/>
                            <?= ($v['total_marks']) ? $v['total_marks'] : '<span class="not-set"> -- </span>' ?>
                        </td>
                        <td><span class="checkbox checkbox-info checkbox-circle"
                                  style="margin:6px;">
                                    <input type="checkbox"
                                           class="checkbox"
                                           name="selectedtest[assessment_id]"
                                           id="<?= $v['test_id']; ?>"
                                           value="<?= $v['test_id'] ?>">
                                    <label class='col-xs-12 no-padding' for="<?= $v['test_id']; ?>"></label>
                                </span></td>


                    </tr>
                    <?php
                    $srNo++;
                endforeach;
            else :
                ?>
                <td colspan="8">No Records found for the search Criteria</td>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</fieldset>
<?php ActiveForm::end(); ?>



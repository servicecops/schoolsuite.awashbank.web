<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin([
        'action' => ['assign', 'id' => $model->id, 'average'=>$average],
        'method' => 'get',
        'options' => ['class' => 'formprocess'],
    ]); ?>

<div class="row">

    <div class="col-md-2 "><p>Search Subject</p></div><br>


    <div class="col-md-6 ">
        <?= $form->field($searchModel, 'class_subject')->dropDownList(
            ArrayHelper::map(\app\modules\schoolcore\models\CoreSubject::find()->where(['class_id' => $model->class_id])->all(), 'id', 'subject_name'), ['prompt' => 'Filter subject', 'class' => 'form-control input-sm']
        )->label(false) ?>
    </div>

    <div class="col-md-2 "><?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
</div>
    <?php ActiveForm::end(); ?>



<?php

use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\web\JsExpression;
use yii\bootstrap4\ActiveForm;


$disableControls = false;

$allowEdit = true;
?>
<?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
    <div class="notify notify-danger">
        <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
        <div class="notify-content">
            <?= \Yii::$app->session->getFlash('viewError'); ?>
        </div>
    </div>
<?php endif; ?>

<div class="row">

    <h3>Subject Assessments Selections</h3>
    <div class="col-md-12 no-padding"
         style="background:#efefef" >
        <div class="wizard">
            <a class="current col-md-3"><span class="badge">1</span> Select Subject</a>
            <a class="col-md-3 gray"><span class="badge">2</span>  Assign Assessments</a>
            <a class="col-md-3 gray"><span class="badge badge-inverse">3</span> Generate Final Mark</a>
        </div>
    </div>



    <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Please Click on subject to be considered
    </div>



    <div class="col-md-12 col-lg-12 no-padding">

          <table class="table table-striped">
            <thead>
            <tr>

                <th class='clink'>Subject Code</th>
                <th class='clink'>Subject Name</th>
                <th class='clink'></th>
            </tr>
            </thead>
             <tbody>
            <?php if ($data) :
             foreach ($data as $k => $v) : ?>
             <tr data-key="0">
                 <td class="clink"><?= ($v['subject_code']) ? '<a href="' . Url::to(['core-subject/view', 'id' => $v['subject_id']]) . '">' . $v['subject_code'] . '</a>' : '<span class="not-set">(not set) </span>' ?></td>
                 <td><?= ($v['subject_name']) ? $v['subject_name'] : '<span class="not-set">(not set) </span>' ?></td>
                 <td>
                     <?= Html::a('<i class="fa  fa-eye"></i>', ['student-report/assign-assessments', 'id' => $model->id, 'subId'=>$v['subject_id']], ['class' => 'aclink']) ?>
                 </td>
             </tr>
             <?php endforeach;
            else :?>
                <tr>
                    <td colspan="8">No Subjects found</td>
                </tr>
            <?php endif; ?>
             </tbody>
          </table>




    </div>


</div>



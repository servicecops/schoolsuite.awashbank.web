<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'action' => ['composition', 'id'=>$id, 'testId'=>$testId,'subjectId'=>$subjectId,'average'=>$average],
    'method' => 'post',
    'id'=>'assign-form',
    'options'=>['class'=>'formprocess'],
]);

?>

<div class="row">

    <div class="col-sm-4">
        <?= $form->field($model, 'composition',['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Marks']])->textInput()->label('Test Composition') ?>
    </div>

</div>
<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

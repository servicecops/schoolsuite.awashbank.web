<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolInformation */

$this->title = "Permissions Assignment";
$this->params['breadcrumbs'][] = ['label' => 'School Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!--    grid-->
<p>Reports & Terms</p>
<div class=" letter ">

    <div class="row class-associate">

        <div class="col-md-5 ">
            <b>Terms Available</b>:
            <div class="form-group has-feedback">
                <input name="search_av" type="text" class="form-control perm-search" placeholder="Search..." data-target="available"/>
            </div>
            <?php
            echo Html::listBox('selected', '', $available, [
                'id' => 'available',
                'multiple' => true,
                'size' => 20,
                'class'=>'form-control',
                'style' => 'width:100%;height:350px;padding:5px']);
            ?>
        </div>

            <div class="col-lg-2 col-sm-2 col-xs-12 text-center" style="padding-top:100px;">
                <br><br>
                <?php
                echo Html::a('>>', '#', ['class' => 'btn btn-block btn-primary', 'title' => 'Assign', 'data-action' => 'assign']) . '<br><br>';
                echo Html::a('<<', '#', ['class' => 'btn btn-block btn-danger', 'title' => 'Delete', 'data-action' => 'delete']) . '<br>';
                ?>
                <br><br>
            </div>

        <div class="col-lg-5 col-sm-5 col-xs-12">
            <b>Assigned</b>:
            <div class="form-group has-feedback">
                <input name="search_asgn" type="text" class="form-control perm-search" placeholder="Search..." data-target="assigned"/>
            </div>
            <?php
            echo Html::listBox('selected', '', $assigned, [
                'id' => 'assigned',
                'multiple' => true,
                'size' => 20,
                'class'=>'form-control',
                'style' => 'width:100%;height:350px;padding:5px']);
            ?>
        </div>
    </div>
        <?php $this->render('_script',['id'=>$id]); ?>
</div>

<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\studentreport\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
// By John Mark

$this->title = 'Assign Subjects';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">
    <div class="student-group hd-title" data-title="<?= $this->title; ?>">
        <div class="row">
            <div class="col-md-12 no-padding"
                 style="background:#efefef">
                <div class="wizard">
                    <a class="col-md-3"><span class="badge">1</span> Select Subject</a>
                    <a class="current col-md-3 gray"><span class="badge">2</span> Assign Assessments</a>
                    <a class="col-md-3 gray"><span class="badge badge-inverse">3</span> Generate Final Mark</a>
                </div>
            </div>


            <div class="col-md-12 " style="background: white;padding:10px">
                <span style="font-size:20px;color:#3c8dbc"><b><?= ucwords($model->description) . "  -  " ?> </b></span>
                <span style="font-size:18px;color:#3c8dbc"><b><?= ucwords($model->school->school_name) ?> </b></span>
                <hr class="l_header">
            </div>
            <div class="col-md-12 " style="background: white;padding:10px">

                <div class="col-md-12 ">
                    <h3 class="box-title" style="font-size:19px;">&nbsp;<i
                                class="fa fa-th-list"></i>&nbsp;&nbsp;Subject Assessments Selections</h3></div>

            </div>
            <div class="col-md-12 " style="background: white;padding:10px">
                <div  class="col-md-12 " style="background: white;padding:10px">
                    <div class="col-md-12"><b style="color:#D82625; font-size: 17px;">Important: </b> Please Click on
                        subject to be considered, summation should not be greater than <b>100%</b>
                  then click on test <b>Take Action</b> button to use the group.</span>
                    <span style="font-size:15px;color:#613030;"><b> Note:</b> The search result will contain only <b>500 records</b> that are not already assigned to this group.</span>
                    <a class="aclink" href="<?= Url::to(['./student-report/index']) ?>">Back to Groups</a>
                </div>

            </div>

            <div class="row">
                <!-- Title and filters -->

                <!-- Note -->
                <!-- Selection Table -->
                <div class="col-sm-5 col-xs-12" style="padding-top: 10px;">
                    <div class="select_tbl">
                        <?php echo $this->render('select_assessment', ['data' => $data, 'model' => $model,'subId'=>$subId]); ?>
                    </div>
                    <!-- Payment codes -->
                    <div class="codes_input" style="display:none;">

                    </div>
                    <!--  -->
                </div>
                <!-- Group Members Table -->
                <div class="col-sm-7 col-xs-12" style="padding-top: 10px;">
                    <?php echo $this->render('selected_tests', ['data' => $members, 'average' => false, 'model' => $model,'subId'=>$subId]); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $script = <<< JS

$(function () {
    
$(".code_link").click(function(){
      $(".select_tbl").css('display', 'none');
      $(".codes_input").css('display', 'block');
    });
    $(".back_link").click(function(){
      $(".select_tbl").css('display', 'block');
      $(".codes_input").css('display', 'none');
    });
    
    $("#empty-group-button").bind('click', function() {
       $("#empty-group").val('empty-group') 
    })
});





JS;
    $this->registerJs($script);
    ?>

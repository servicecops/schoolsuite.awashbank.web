<?php


use app\modules\schoolcore\models\CoreSchool;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentGroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="classes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => ['class'=>'formprocess']
    ]); ?>

    <div class="row">
        <div class="col-md-10 no-padding">
            <?php
            $url = Url::to(['/schoolcore/core-school/active-schoollist']);
            $selected = empty($searchModel->school_id) ? '' : CoreSchool::findOne($searchModel->school_id)->school_name;
            echo $form->field($searchModel, 'school_id')->widget(Select2::classname(), [
                'initValueText' => $selected, // set the initial display text
                'theme'=>Select2::THEME_BOOTSTRAP,
                'size'=>'sm',
                'options' => [
                    'placeholder' => 'Select School',
                    'id'=>'search_school_id',
                    'class'=>'form-control',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => $url,
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                ],
            ])->label(false);
            ?>
        </div>
        <div class="col-xs-1 no-padding">
            <?= Html::submitButton("<i class='fa fa-search'></i>", ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

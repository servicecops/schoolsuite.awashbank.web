<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'Social Media Links';
?>

<div class="container card">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="card-body">



        <div class="row">
            <div class="col-md-8">
                <i class="fa fa-facebook-square"></i>
            </div>
                <div class="col-md-8">

                    <?= $form->field($model, 'facebook_link', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Facebook Link']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <i class="fa fa-twitter-square"></i>

            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'twitter_link', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Twitter Link']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <i class="fa fa-linkedin-square"></i>

            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'linkedin_link', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Linkedin Link']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <i class="fa fa-skyatlas"></i>
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'telegram_link', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Telegram Link']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>
        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-xm-6">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                </div>
                <div class="col-xm-6">
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                </div>
            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS



$("document").ready(function(){ 
    function evaluateRecurrentVisibility() {
        if($('#user_type_select').val() === '0') {
           $('#std_div').hide()
           $('#staff_div').show()
        }else {
           $('#std_div').show() 
           $('#staff_div').hide() 
        }
}

   

evaluateRecurrentVisibility();


    //On changing user type, hide or show div controls
   $('body').on('change', '#user_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   
    });
   


JS;
$this->registerJs($script);
?>

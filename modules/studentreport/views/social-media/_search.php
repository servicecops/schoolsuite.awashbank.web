<?php


use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\jui\DatePicker;
use yii\web\JsExpression;
?>


<div class="container bg-white" style="width:100%;">
    <div >
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row" style="padding: 10px">


                <div class="col-md-6">
                    <?= $form->field($model, 'isstudent', ['inputOptions' => ['class' => 'form-control',
                        'placeholder' => 'Description',
                        'disabled' => $model->is_student,]])->dropDownList(
                        ['1' => 'Student', '0' => 'Staff'],
                        ['id' => 'user_type_select', 'prompt' => 'Select']
                    )->label(false)?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'modelSearch', ['inputOptions'=>[ 'class'=>'form-control'],])->textInput(['placeHolder'=>'Search Student code or Staff Id','title' => 'Enter student code or staff Id',

                        'data-toggle' => 'tooltip',

                        'data-trigger' => 'hover',

                        'data-placement' => 'bottom'])->label(false) ?>

                </div>

            </div>
            <div><?= Html::submitButton('Search', ['class' => 'btn bg-gradient-primary']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

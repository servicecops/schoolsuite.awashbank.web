<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\schoolcore\models\CoreSchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Social Media Links';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">



    <?php

    $columns = [

        'facebook_link',
        'twitter_link',
        'linkedin_link',
        [
            'label' => 'School Name',
            'value' => function ($model) {
                return $model['school_name'];
            },
        ],
        'date_created',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['social-media/view', 'id' => $model['id']]);
            },
        ],
        ////
    ];
    ?>


    <div class="row" style="margin-top:20px">

        <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
    </div>


    <?php if (\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('is_teacher')) : ?>
        <?= Html::a('Add new Social Media Link', ['create'], ['class' => 'btn btn-sm btn-primary  float-right']) ?>
    <?php endif; ?>



    <div class="mt-3">
        <div class="float-right">

        </div>

        <?= GridView::widget([
            'dataProvider'=> $dataProvider,
//    'filterModel' => $searchModel,
            'columns' => $columns,
            'resizableColumns'=>true,
//    'floatHeader'=>true,
            'responsive'=>true,
            'responsiveWrap' => false,
            'bordered' => false,
            'striped' => true,
        ]); ?>

    </div>
</div>


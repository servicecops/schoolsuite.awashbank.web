<?php

use kartik\form\ActiveForm;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\LinkPager;
//use yii\widgets\LinkPager;


/* @var $this yii\web\View */
/* @var $model app\modules\studentreport\models\CoreSchoolCircular */

$this->title = $model->circular_subject;
$this->params['breadcrumbs'][] = ['label' => 'Classes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">

    <div class="classes-view hd-title"
         data-title="<?= $model->circular_subject . ' - <b>' . 0 . '</b> Students' ?>">
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="col-xs-12">
                    <div class="pull-right">

                        <div class="dropdown">
                            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                Download Circulars
                                <span class="caret"></span></button>
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-expanded="false">
                                Generate Circular
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item"
                                   href="<?= Url::to(['customized-circular-generator', 'id' => $model->id, 'classes' => true]); ?>">For
                                    class</a>
                                <a class="dropdown-item"
                                   href="<?= Url::to(['customized-circular-generator', 'student_codes' => true, 'id' => $model->id]); ?>">For
                                    student</a>
                            </div>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="clink">
                                    <?= Html::a('For Class', ['customized-circular-generator', 'id' => 8, 'classes' => "true"]) ?>
                                </li>
                                <li class="clink"><a
                                            href="<?= Url::to(['circular-generator', 'circular_id' => $model->id, 'student_codes' => "true"]) ?>">For
                                        Student</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="pull-left">


                        <?= Html::a('<i class="fa fa-edit"></i>&nbsp;&nbsp; Edit Circular', ['school-circular/update-customised-circular', 'id' => $model->id], ['class' => 'btn  btn-info btn-sm']) ?>

                        <?= Html::a('<i class="fa fa-remove"></i> Delete Circular', ['delete-customised-circular', 'id' => $model->id], ['class' => 'btn btn-sm btn-danger', 'data-method' => 'post', 'data-confirm' => "Are you sure you want to circular this class"]) ?>

                        <?= Html::a('<i class="fa fa-eye"></i> View All Students', ['school-circular/customised', 'circularId' => $model->id], ['class' => 'btn btn-sm btn-success', 'data-method' => 'post']) ?>

                    </div>
                </div>
            </div>
        </div>
        <br><br>
    </div>

    <div>

    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('circular_subject') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->circular_subject ?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('term') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->termName->term_name ?></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('circular_subject') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->circular_subject ?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('circular_name') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->circular_name ?></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-label"><?= $model->getAttributeLabel('C.C') ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= $model->ccc_description ?></div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row ">
                <div class="col-lg-6 col-xs-6 profile-label"><?= "" ?></div>
                <div class="col-lg-6 col-xs-6 profile-text"><?= "" ?></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-xs-6 profile-label"><?= $model->getAttributeLabel('the_body') ?></div>
        <div class="col-lg-9 col-xs-6 profile-text"><?= $model->the_body ?></div>

    </div>

</div>




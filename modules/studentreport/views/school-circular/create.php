<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\studentreport\models\CoreSchoolCircular */

$this->title = 'Create Core School Circular';
$this->params['breadcrumbs'][] = ['label' => 'Core School Circulars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-circular-create">

    <div class=" col-md-12 no-padding">
        <h3 class="box-title"><i class="fa fa-plus"></i> <?= Html::encode($this->title) ?></h3>
    </div>

    <div class=" col-md-12 no-padding">
        <?=
        $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>



</div>

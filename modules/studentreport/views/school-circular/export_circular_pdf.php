<?php

use Da\QrCode\QrCode;
use yii\helpers\Html;

use app\assets\SiteAsset;

/* @var $this yii\web\View */
?>

<?php
/** @var TYPE_NAME $circular_data */
foreach ($circular_data as $data):
    ?>
    <div class="container">
        <div class="row">
            <table class="grid-width-100" style="width: 100%;">
                <tbody>
                <tr>
                    <td colspan="3" class="text-center" style=" ">
                        <h2><?php
                            echo $data['school_name']; ?></h2>
                    </td>

                </tr>


                <tr>
                    <td style="width: 260px">P.O.BOX <?= $data['box_no']; ?></td>
                    <td rowspan="2">
                        <?= Html::img('@web/web/img/sch_logo_icon.jpg', ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>
                    </td>
                    <td style=""><span
                                class="pull-right">Tel Office: <?= $data['phone_contact_1']; ?><br> <?= $data['phone_contact_2']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td style=" ">Our Reference
                        No:<?= $data['school_reference_no'] . "" . $data['id']; ?></td>
                    <td><p class="text-center">Email:<?= $data['contact_email_1']; ?></p></td>
                </tr>
                </tbody>
            </table>
            <hr class="l_header">
        </div>
    </div>
    <div class="row">

        <div class="row">
            <div class="col-md-12">
                <p class="bold"><?= $data['citation'] ?> of <?= $data['first_name'] . " " . $data['last_name']; ?></p>
            </div>
            <div class="col-md-12">
                <h4 class="text-center" style="text-decoration: underline;">RE:<?= $data['subject'] ?>
                </h4>
            </div>

            <div class="col-md-12">
                <p><?= $data['body'] ?></p>
            </div>

            <div class="col-md-12">
                <p>Yours Faithfully,</p>
                <?= Html::img('@web/web/uploads/signatures/' . $data['file_attachment'], ['class' => 'sch-icon', 'alt' => 'profile photo', 'height' => 100, 'width' => 100]) ?>
                <p><?= $data['name']; ?></p>
                <p><?= $data['position']; ?></p>
                <p><?= $data['contact']; ?></p>
            </div>
            <?php
            $qrCode = (new QrCode('216001234'))
                ->setSize(100)
                ->setMargin(5);
            echo '<img style="float: right;" src="' . $qrCode->writeDataUri() . '">';
            ?>
        </div>
        <div class="row">
            <hr class="l_header">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h3>Our Mission:</h3>"<?= $data['vision']; ?>"
            </div>
            <div class="col-md-2"></div>
        </div>

    </div>
    <pagebreak/>
<?php endforeach; ?>
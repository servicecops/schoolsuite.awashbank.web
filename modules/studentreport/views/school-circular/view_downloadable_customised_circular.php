<?php

use yii\helpers\Html;
use yii\helpers\Url;
$pad_length = 4;
$pad_char = 0;
$str_type = 'd';
$format = "%{$pad_char}{$pad_length}{$str_type}";
?>
<table align="center">
    <tr>
        <td><img src="<?= Yii::getAlias('@web/web/img/PNG1.png') ?>" height="40" width="94" /></td>
        <td style="color:red;font-size:22px;" align="right" width="85.8%"><tt><?= sprintf($format, $data['id']); ?></tt></td>
    </tr>
</table>
<div class="row" style="text-align:center;">
    <?php if($data['school_logo']) : ?>
        <img src="<?= Url::to(['/import/import/image-link2', 'id'=>$data['school_logo']]) ?>" height="65" width="65" />
    <?php else :?>
        <img src="<?= Yii::getAlias('@web/web/img/icon_4.png') ?>" height=65, width=65 />
    <?php endif; ?>
    <div style="font-size:18px;font-weight: bold; padding-top:10px;"><?= $data['school_name'] ?></div>


</div>



<table  width="100%">

    <tr>
        <td style="line-height: 20px;" valign="top">
            Our Ref:  <?= $data['circular_ref_no'] ? $data['circular_ref_no'] : '_______________________________'?><br/>
            Your Ref:  _______________________________ <br>

        </td>

        <td style="line-height: 20px;" align="right" valign="top">
            Email:  <?= $data['contact_email_1'] ? $data['contact_email_1'] : '_______________________________'?><br/>
            Phone Number: <?= $data['phone_contact_1'] ? $data['phone_contact_1'] : '_______________________________'?>
        </td>
    </tr>
</table>

<hr class="l_header">

<br/>
<div class="row" style="margin-top:3px;">


</div>
&nbsp;<span style="font-size:11px;">Date:<?= $data['effective_date'] ? $data['effective_date'] : date('M d, Y') ?> </span><br/>

<h3>Re: <span style="text-decoration: underline"><?= $data['circular_subject']?></span> </h3>
<div align="justify">

<?= $data['the_body']?>
</div>


<p>&nbsp;</p>
<p>&nbsp;</p>
<table  width="100%">
    <tr>
        <td style="line-height: 20px;" valign="top">
            Yours in Service<br/>
            ...................................................................<br>
           -<br>

        </td>

    </tr>
</table>

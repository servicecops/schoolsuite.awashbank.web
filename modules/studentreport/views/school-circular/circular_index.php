<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\studentreport\models\CoreSchoolCircularSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core School Circulars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>SCHOOL CIRCULARS</div>
<div class="letter mt-3">
    <div class="row text-center">
        <div class="row mt-12">
        <p class="align-center">Select Circular Type</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h3>Generic</h3>
            <p>All content thing is created within the system</p>
            <p>General Purpose</p>
            <p>Affects all student</p>
            <p>Can send by email</p>
            <?= Html::a('Generic Circular', ['index'], ['class' => 'btn btn-sm btn-primary  float-left']) ?>

        </div>
        <div class="col-md-4">
            <h3>Uploaded Version</h3>
            <p>Allows you to upload your already made circular</p>
            <p>General Purpose</p>
            <p>Affects all student</p>
            <?= Html::a('Upload Circular', ['index-uploaded-circular'], ['class' => 'btn btn-sm btn-primary  float-left']) ?>

        </div>
        <div class="col-md-4">
            <h3>Customised Version</h3>
            <p>Allows you to create ciculars for specific students or class</p>
            <p>Can exempt students in a class</p>
            <p>Affects all student</p>
            <p>Can send by email</p>

            <?= Html::a('Customised Circular', ['index-customised-circulars'], ['class' => 'btn btn-sm btn-primary  float-left']) ?>

        </div>
    </div>
</div>

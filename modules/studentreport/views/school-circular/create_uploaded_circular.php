<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'Upload Circulars';
?>

<div class="container card">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="card-body">


            <div class="row" style="margin-top:20px;margin-bottom:20px">

                <div class="col-md-3 "><span style="font-size:20px;color:#2c3844">&nbsp;<i class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
            </div>





        <div class="row">
            <div class="col-md-6">

                <?= $form->field($model, 'circular_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Circular Name']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>


            <div class="col-md-6">
                <?= $form->field($model, 'circular_description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Circular description']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>

        </div>
        <h5 class="form-header">Attach Files</h5>
        <div class="form-desc">Please attach one file needed i.e. (doc, docx, pdf))
        </div>

        <div class="row">
            <?= $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
                'options' => ['accept' => '*/*'],
                'pluginOptions' => ['allowedFileExtensions' => ['pdf', 'doc','docx'], 'showUpload' => false,],
            ])->label(false);
            ?>

        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-xm-6">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                </div>
                <div class="col-xm-6">
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                </div>
            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS



$("document").ready(function(){ 
    function evaluateRecurrentVisibility() {
        if($('#user_type_select').val() === '0') {
           $('#std_div').hide()
           $('#staff_div').show()
        }else {
           $('#std_div').show() 
           $('#staff_div').hide() 
        }
}

   

evaluateRecurrentVisibility();


    //On changing user type, hide or show div controls
   $('body').on('change', '#user_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   
    });
   


JS;
$this->registerJs($script);
?>

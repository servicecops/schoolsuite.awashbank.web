<?php

use app\modules\studentreport\models\SchoolESealsUploads;
use app\modules\studentreport\models\SignatureUploads;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2assets\printthis\PrintThis;

$pad_length = 4;
$pad_char = 0;
$str_type = 'd';
$format = "%{$pad_char}{$pad_length}{$str_type}";
?>
<?php if ($circular_data): ?>
<div class="row mt-5">`</div>
<div id="PrintPage">
<p>
    <?php
    echo PrintThis::widget([
        'htmlOptions' => [
            'id' => 'PrintPage',
            'btnClass' => 'btn btn-info',
            'btnId' => 'PrintPage',
            'btnText' => 'Print Circular',
            'btnIcon' => 'fa fa-print'
        ],
        'options' => [
            'debug' => false,
            'importCSS' => true,
            'importStyle' => false,
            'loadCSS' => "path/to/my.css",
            'pageTitle' => "",
            'removeInline' => false,
            'printDelay' => 333,
            'header' => null,
            'formValues' => true,
        ]
    ]);
    ?>
</p>

    <?php
    /** @var TYPE_NAME $circular_data */
    foreach ($circular_data as $info):
        ?>

<div id="btnPrintThis">
<div class="mt-3 letter ">
    <div>
     <span>
    <img src="<?= Yii::getAlias('@web/web/img/PNG1.png') ?>" height="40" width="94" />
</span>
        <span class="pull-right" style="color:red;font-size:22px;">
        <?= sprintf($format, $data['id']); ?>
    </span>
    </div>

    <div class="text-center">
        <?php if($data['school_logo']) : ?>
            <img src="<?= Url::to(['/import/import/image-link2', 'id'=>$data['school_logo']]) ?>" height="65" width="65" />
        <?php else :?>
            <img src="<?= Yii::getAlias('@web/web/img/icon_4.png') ?>" height=65, width=65 />
        <?php endif; ?>
        <div style="font-size:18px;font-weight: bold; padding-top:10px;"><?= $data['school_name'] ?></div>
        <div>
            <?= $data['physical_address']?>
        </div>
        <div>
            Email:  <?= $data['contact_email_1'] ? $data['contact_email_1'] : '_______________________________'?>,
            Phone Number: <?= $data['phone_contact_1'] ? $data['phone_contact_1'] : '_______________________________'?>
        </div>

    </div>
    <hr class="l_header mt-3">
    <div class="row mt-3">
        Our Ref:  <?= $data['circular_ref_no'] ? $data['circular_ref_no'] : '_______________________________'?><br/>
        Your Ref:  _____________________________ <br/>


        TO<br/>
            STUDENT NAME: <?= $info['first_name'] . " " . $info['last_name']; ?><br/>
            STUDENT CODE: <?= $info['student_code'] . " " . $info['last_name']; ?><br/>
            CLASS: <?= $info['class_name']; ?><br/>


    </div>
    <div class="row mt-4">Date:<?= $data['effective_date'] ? $data['effective_date'] : date('M d, Y') ?> </div>
    <div class="text-center">
        <h3>RE: <span style="text-decoration: underline"><?= $data['circular_subject']?></span> </h3>
    </div>


    <div align="justify">

        <p>Dear Parent/Guardian of <span style="font-size: 17px; font: bold"><?= $info['first_name'] . " " . $info['last_name']; ?></span></p><br/>
        <div class="content-container">
        <span class="contents">

            <?php $msg_text = $data['the_body'];


          //  echo str_replace("world","Peter","Hello world!");
			//	$msg_text = str_replace(msg_text,'{OUTSTANDING_BALANCE}',$the_account_balance );
				$msg_text = str_replace('{GUARDIAN_NAME}', $info['guardian_name']?$info['guardian_name']:'',$msg_text);
				$msg_text = str_replace('{STUDENT_PAYMENT_CODE}',$info['student_code']?$info['student_code']:'',$msg_text);
				$msg_text = str_replace('{SCHOOL_NAME}', $data['school_name'], $msg_text);
				$msg_text = str_replace('{STUDENT_CLASS}', $info['class_name'] ? $info['class_name']:'',$msg_text);
				$msg_text = str_replace('{STUDENT_NAME}',  $info['first_name'] . " " . $info['last_name'],$msg_text);

            ?>


            <?= $msg_text?></span>
        </div>
    </div>



    <div class="row">
    <div class="col">
        <div class="row">
            <p>Yours in Service</p>
        </div>
        <div class="row">
            <p><?php echo $signature ?></p>
        </div>
        <div class="row">

            ...................................................................

        </div>

        <div class="row">
            <?= $data['individual_name']?>,
        </div>
        <div class="row">
       <?= $data['individual_position']?>
        </div>
    </div>
    <div class="col">
        <?php if($data['school_stamp']) : ?>
            <img src="data:image/jpeg;base64,<?= $data['school_stamp'] ?>" width="130px" class="profile_img" />
        <?php endif; ?>
    </div>
    <div class="col">

    </div>
    </div>
    <hr class="l_header mt-3">
    <div class="row">
        <div style='text-align:left;'>
            <?php if($data['phone_contact_1']) : ?>
                Tel: <?=$data['phone_contact_1'] ?>
            <?php endif; ?>
            <?php if($data['phone_contact_1']) : ?>
                , Email: <?=$data['contact_email_1'] ?>
            <?php endif; ?>

        </div>
        <div style='text-align:left;'>
            <?php if($smedia) : ?>
            <span></span>
            <i class='fa fa-facebook-square ml-2'></i> <?=$smedia['facebook_link'] ?>  <i class='fa fa-twitter-square ml-2'></i> <?=$smedia['twitter_link'] ?> <i class='fa fa-linkedin-square ml-2'></i> <?=$smedia['linkedin_link'] ?> </div>
        <?php endif; ?>
    </div>
</div>

</div>

    <?php endforeach; ?>
</div>
<?php elseif (!$circular_data) : ?>

    <div class="text-center">
        <?= Yii::$app->session->setFlash('danger', 'No records found');?>


    </div>

    <p><?= \yii\helpers\Html::a( 'Back to previous page', Yii::$app->request->referrer);?></p>
<?php endif; ?>

<?php

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\schoolcore\models\CoreSchoolClass;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\studentreport\models\CoreSchoolCircular */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="formz">


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div style="padding: 10px;width:100%"></div>

    <div class="row">

        <?php if (\app\components\ToWords::isSchoolUser()) : ?>
            <div class="col-sm-6">
                <?php
                $data = CoreTerm::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

                echo $form->field($model, 'term_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($data, 'id', 'term_name'),
                    'language' => 'en',
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => ['placeholder' => 'Find Term'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Term *'); ?>
            </div>
        <?php endif; ?>
        <div class="col-sm-6">
            <?= $form->field($model, 'circular_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Title']])->textInput(['autocomplete' => 'off']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'circular_subject', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Title']])->textInput(['autocomplete' => 'off']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'circular_ref_no', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Title']])->textInput(['autocomplete' => 'off']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'effective_date', ['labelOptions' => ['style' => 'color:#041f4a']])->Input('date') ?>
        </div>
    </div>
    <div class="row">

                <div class="col-sm-12">

           <?= $form->field($model, 'the_body')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'basic'
        ]) ?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'ccc_description', ['inputOptions' => ['class' => 'form-control', 'placeholder' => '']])->textInput(['autocomplete' => 'off'])->label("C.C (PLEASE SEPERATE WITH A COMMAS IF MORE THAN ONE") ?>
        </div>
        <div class="col-sm-6">
            <?php
            $data = \app\modules\studentreport\models\SignatureSealMedia::find()->where(['school_id' => Yii::$app->user->identity->school_id])->all();

            echo $form->field($model, 'signatories')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($data, 'id', 'individual_name'),
                'language' => 'en',
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => [
                    'placeholder' => 'Find signatory',
                    'id' => 'group_signatures_select',
                    'multiple' => false,
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Choose signatories'); ?>
        </div>


    </div>
    <h5 class="form-header">Attach School stamp</h5>
    <div class="form-desc">Please attach stamp image(required files are png, jpg, jpeg, pdf)
    </div>
    <div class="row">
        <?= $form->field($model, 'school_stamp[]')->widget(FileInput::classname(), [
            'options' => ['accept' => '*/*'],
            'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'png','jpeg'], 'showUpload' => false,],
        ])->label(false);
        ?>

    </div>



</div>


<div class="card-footer">
    <div class="row">
        <div class="col-xm-6">
            <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
        </div>
        <div class="col-xm-6">
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\studentreport\models\CoreSchoolCircularSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<div >
    <div class="row text-center">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options'=>['class'=>'formprocess'],
        ]); ?>

        <div class="row mt-12">


            <div class="col">
                <?= $form->field($model, 'circular_name', ['inputOptions'=>[ 'class'=>'form-control'],])->textInput(['title' => 'Enter circular name',

                    'data-toggle' => 'tooltip',

                    'data-trigger' => 'hover',

                    'data-placement' => 'bottom'])->label(false) ?>

            </div>
            <div>

                <?= Html::submitButton('Search', ['class' => 'btn btn-info btn-sm']) ?></div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

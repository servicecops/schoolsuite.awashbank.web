<?php

use kartik\export\ExportMenu;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\studentreport\models\CoreSchoolCircularSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Core School Circulars';
$this->params['breadcrumbs'][] = $this->title;
?>
<p style="margin-top:20px ;color:#F7F7F7">.</p>
<div class="letter">

    <?php

    $columns = [
        ['class' => 'yii\grid\SerialColumn'],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {

                return Html::a($model['circular_subject'], ['school-circular/view', 'id' => $model['id']], ['class' => 'aclink']);

            },
        ],
        'term_name',
        'school_name',
        'description',
        'date_modified',
        'attachment_file',
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="fa fa-edit"></i>', ['school-circular/update', 'id' => $model['id']]);
            },
        ],
        [

            'label' => '',
            'format' => 'raw',
            'value' => function ($model) {

                return Html::a('<i class="fa  fa-ellipsis-h"></i>', ['school-circular/view', 'id' => $model['id']], ['class' => 'aclink']);

            },
        ]
    ];
    ?>

        <div class="row">
            <div class="col-md-12">

                <div class="col-md-3 col-xs-12 no-padding"><span style="font-size:20px">&nbsp;<i
                                class="fa fa-th-list"></i> <?php echo $this->title ?></span></div>
                <div class="col-md-4 col-xs-12 no-padding"><?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
                <div class="col-md-4 col-xs-12 no-padding">

                    <?php if (\app\components\ToWords::isSchoolUser() || Yii::$app->user->can('is_teacher')) : ?>
                        <?= Html::a('Add new Circular', ['create'], ['class' => 'btn btn-sm btn-primary  float-right']) ?>
                    <?php endif; ?>

                </div>
            </div>

        </div>

        <div class="mt-3">
            <div class="float-right">

            </div>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
                'columns' => $columns,
                'resizableColumns' => true,
//    'floatHeader'=>true,
                'responsive' => true,
                'responsiveWrap' => false,
                'bordered' => false,
                'striped' => true,
            ]); ?>

        </div>
    </div>

</div>

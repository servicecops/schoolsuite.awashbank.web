<div class="col-sm-4">
    <?=
    $form->field($model, 'circular_print_out')->widget(FileInput::classname(), [
        'options' => ['accept' => '*/*'],
        'pluginOptions' => ['allowedFileExtensions' => ['pdf', 'docx', 'doc'], 'showUpload' => false,],
    ]);
    ?>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreGrades */

$this->title = "Administrative E-Signatures ";
$this->params['breadcrumbs'][] = ['label' => 'Administrative E-Signature', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$school =\app\modules\schoolcore\models\CoreSchool::findOne(['id'=>Yii::$app->user->identity->school_id]);
Yii::trace($school);
\yii\web\YiiAsset::register($this);
?>
<p style="color: #F1F4F6">Administrative E-Signatures</p>
<div class="letter">

    <?php if (\Yii::$app->session->hasFlash('stuAlert')) : ?>
        <div class="notify notify-success">
            <a href="javascript:" class='close-notify' data-flash='stuAlert'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('stuAlert'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (\Yii::$app->session->hasFlash('viewError')) : ?>
        <div class="notify notify-danger">
            <a href="javascript:" class='close-notify' data-flash='viewError'>&times;</a>
            <div class="notify-content">
                <?= \Yii::$app->session->getFlash('viewError'); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="core-school-view">

        <div class="row">

            <div class="col-sm-10 "><span style="font-size:20px">&nbsp;<i
                            class="fa fa-th-list"></i> <?php echo $this->title ?> for <?= $school['school_name']?></span></div>
            <div class="col-sm-2">
                <?= Html::a('<i class="fa fa-edit" >Update</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash" >Delete</i>', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>


        <div class="row">

            <div class="col border-right bg-gradient-light">
                <div class="card bg-gradient-light">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000"> Name
                                </b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->individual_name) ? ($model->individual_name) : "--" ?>
                            </div>
                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                <b style="color: #000"> Phone Number</b>
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->individual_phone) ? $model->individual_phone : "--" ?>
                            </div>
                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                Email
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->individual_email) ? $model->individual_email : "--" ?>
                            </div>
                        </div>
                        <hr class="style14">
                        <div class="row">
                            <div class="col">
                                Position
                            </div>
                            <div class="col" style="color: #21211f">
                                <?= ($model->individual_position) ? $model->individual_position : "--" ?>
                            </div>
                        </div>
                        <hr class="style14">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if($model->storedFiles): ?>
        <?= $this->render('files_list', ['uploads' => $model->storedFiles]); ?>
    <?php endif;?>
</div>

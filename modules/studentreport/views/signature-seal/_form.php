<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \app\models\SignupForm */

use app\models\User;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use kartik\file\FileInput;
use kartik\password\PasswordInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use app\models\AuthItem;

$this->title = 'Administrative E-Signatures';
?>

<div class="container card">

    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
    <div class="card-body">
        <div class="row">



        </div>


        <div class="row">
              <div class="col-md-6">

                    <?= $form->field($model, 'individual_name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Full Name']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>


            <div class="col-md-6">
                <?= $form->field($model, 'individual_position', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Position held']])->textInput(['autocomplete' => 'off'])->label(false) ?>
            </div>


            <div class="col-md-6">
                <?= $form->field($model, 'individual_phone', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']])->textInput(['type'=>'tel','autocomplete' => 'off'])->label(false) ?>
            </div>


            <div class="col-md-6">
                <?= $form->field($model, 'individual_email', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Enter Email ']])->textInput(['type'=>'email','autocomplete' => 'off'])->label(false) ?>
            </div>
        </div>
        <h5 class="form-header">Attach Files</h5>
        <div class="form-desc">Please attach signature image needed (png, jpg, jpeg,pdf)
        </div>
        <div class="row">
           <?= $form->field($model, 'uploads[]')->widget(FileInput::classname(), [
            'options' => ['accept' => '*/*'],
            'pluginOptions' => ['allowedFileExtensions' => ['jpg', 'png',], 'showUpload' => true,],
            ])->label(false);
            ?>

        </div>

        <div class="card-footer">
            <div class="row">
                <div class="col-xm-6">
                    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => 'btn btn-block btn-primary']) ?>
                </div>
                <div class="col-xm-6">
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-block']) ?>
                </div>
            </div>
        </div>

    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

$script = <<< JS



$("document").ready(function(){ 
    function evaluateRecurrentVisibility() {
        if($('#user_type_select').val() === '0') {
           $('#std_div').hide()
           $('#staff_div').show()
        }else {
           $('#std_div').show() 
           $('#staff_div').hide() 
        }
}

   

evaluateRecurrentVisibility();


    //On changing user type, hide or show div controls
   $('body').on('change', '#user_type_select', function(){
       evaluateRecurrentVisibility();
   });
   
   
   
    });
   


JS;
$this->registerJs($script);
?>

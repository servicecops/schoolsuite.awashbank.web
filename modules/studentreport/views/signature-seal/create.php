<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\schoolcore\models\CoreSchool */

$this->title = 'Social Media Links';
$this->params['breadcrumbs'][] = ['label' => 'Administrative E-Signatures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="core-school-create">
    <div class="model_titles">
        <div class="col-sm-12"><h3><i class="fa fa-check-square-o"></i>&nbsp;Create Administrative E-Signatures</h3></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

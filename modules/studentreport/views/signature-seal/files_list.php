<?PHP
use app\components\Helpers;
use yii\helpers\Url;
?>

<br>
<div class="row">
    <table class="table" >
        <thead>
        <tr><th colspan="4"><span class="lead">Attached Files</span></th></tr>
        <tr><th> File name</th><th><div class="float-right">
                    </div></th></tr>
        </thead>
        <tbody>
        <?php
        if($uploads) :
            foreach($uploads as $k=>$v):


                $name = explode( '.', $v['file_name'] );
                $ext = $name[count($name)-1];
                $target = in_array($ext, ['png', 'jpg', 'jpeg', 'gif','mp3','mp4']) ? '_blank' : '_self';

                ?>
                <tr>
                    <td><?= $v['file_name'] ?></td>
                    <td style="padding:0 20px">
                        <?php// if(Helpers::can('manage_files')) : ?>
                        <div class="float-right">



                            <a style="font-size:22px;" href="<?= Url::to(['view-file', 'id'=>$v['id']]) ?>" target="<?= $target ?>"><i class="fa fa-download text-primary"></i></a>


                            <a class="aclink confirm" style="font-size:22px; padding-left: 10px;" href="<?= Url::to(['remove-file', 'id'=>$v['id']]) ?>" data-method="post" data-confirm="Confirm to delete file"><i class="fa fa-close text-danger"></i></a>

                        </div>


                    </td>
                </tr>
            <?php endforeach;
        else :
            ?>
            <tr><td colspan="4">No Files provided</td></tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>

<?php

namespace app\modules\studentreport\controllers;

use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\studentreport\models\CircularSignatures;
use app\modules\studentreport\models\SchoolSocialMedia;
use kartik\mpdf\Pdf;
use Yii;
use app\modules\logs\models\Logs;

use yii\base\BaseObject;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Social Media implements the CRUD actions for social Media model.
 */
class SocialMediaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Social Media link models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new SchoolSocialMedia();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }
    /**
     * Displays a single Social Media link model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) :
            $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * Finds the Social Media link model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Social Media link the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolSocialMedia::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


   

    /**
     * Creates a new Social Media link model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_social_media') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a Social media links.');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolSocialMedia();

        try {
            if ($model->load(Yii::$app->request->post())) {

                $incoming = Yii::$app->request->post();
                $post = $incoming['SchoolSocialMedia'];

                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $model->created_by = Yii::$app->user->identity->getId();

                if ($model->save(false)) {
                    Logs::logEvent("Created New Social Media Link: " . $model->id, null, null);
               }


                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id], 200);

                //   }
            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();;
            Logs::logEvent("Failed to new social meedia link : ", $model_error, null);
            \Yii::$app->session->setFlash('actionFailed', $model_error);

        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;

        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_social_media', ['sch' => $model->school_id])) {
            if ($model->load(Yii::$app->request->post())) {

                $post = $model->load(Yii::$app->request->post());
                Yii::trace($post);
                $transaction = SchoolSocialMedia::getDb()->beginTransaction();
                try {
                    //$newId =$this->saveUser();
                    $model->created_by = Yii::$app->user->identity->getId();
                    $model->save(false);


                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Yii::trace('Error: ' . $error, 'Update Social Media Links ROLLBACK');
                    \Yii::$app->session->setFlash('actionFailed', $model_error);

                }

            }
            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }
    /**
     * Deletes an existing CoreSchoolClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

}

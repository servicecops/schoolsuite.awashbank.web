<?php

namespace app\modules\studentreport\controllers;

use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\studentreport\models\CircularSignatures;
use app\modules\studentreport\models\SchoolSocialMedia;
use app\modules\studentreport\models\SignatureSealMedia;
use app\modules\studentreport\models\SignatureUploads;
use kartik\mpdf\Pdf;
use Yii;
use app\modules\logs\models\Logs;

use yii\base\BaseObject;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Social Media implements the CRUD actions for social Media model.
 */
class SignatureSealController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Social Media link models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new SignatureSealMedia();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }

    /**
     * Displays a single Social Media link model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) :
            $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * Finds the Social Media link model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SignatureSealMedia link the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SignatureSealMedia::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*
        * View/Download file
        */
    public function actionViewFile($id)
    {
        $response = Yii::$app->response;
        $model = SignatureUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'mp3', 'mp4']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->headers->set('Content-type', $ext);
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);
        return $response;
    }


    /**
     * Creates a new Social Media link model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        /*if (!Yii::$app->user->can('rw_social_media') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a Social media links.');
        }*/

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SignatureSealMedia();

        try {
            if ($model->load(Yii::$app->request->post())) {


                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;

                }

                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                $model->save(false);
                $model->saveUploads();
                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id]);


            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to new administrative signature : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;

        $model = $this->findModel($id);

        if (Yii::$app->user->can('rw_social_media', ['sch' => $model->school_id])) {
            try {
                $connection = Yii::$app->db;

                $transaction = $connection->beginTransaction();
                if ($model->load(Yii::$app->request->post())) {

                    if (\app\components\ToWords::isSchoolUser()) {
                        $model->school_id = Yii::$app->user->identity->school_id;

                    }

                    $model->uploads = UploadedFile::getInstances($model, 'uploads');

                    $model->save(false);
                    $model->saveUploadUpdate($id);
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);

                }
            } catch (\Exception $e) {
                Yii::trace($e);
                $transaction->rollBack();
                $model_error = $e->getMessage();
                Logs::logEvent("Failed to new administrative signature : ", $model_error, null);
            }

            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }

    /**
     * Deletes an existing CoreSchoolClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * delete signature submission
     *
     */
    public function actionRemoveFile($id)
    {
        $model = SignatureUploads::find()->where(['id' => $id])->limit(1)->one();
        $submission_id = $model->submission_id;
        $model->delete();
        return $this->redirect(['view', 'id' => $submission_id]);
    }
}

<?php

namespace app\modules\studentreport\controllers;

use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\studentreport\models\CircularSignatures;
use app\modules\studentreport\models\CircularUploads;
use app\modules\studentreport\models\CustomisedCircularAssociations;
use app\modules\studentreport\models\CustomisedCircularExemptions;
use app\modules\studentreport\models\SchoolSocialMedia;
use app\modules\studentreport\models\SignatureUploads;
use app\modules\studentreport\models\UploadedCircular;
use kartik\mpdf\Pdf;
use Yii;
use app\modules\logs\models\Logs;
use app\modules\studentreport\models\CoreSchoolCircular;
use app\modules\studentreport\models\CoreSchoolCircularSearch;
use yii\base\BaseObject;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SchoolCircularController implements the CRUD actions for CoreSchoolCircular model.
 */
class SchoolCircularController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoreSchoolCircular models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('No permissions to view school circulars.');
        }
        $searchModel = new CoreSchoolCircularSearch();
        $request = Yii::$app->request;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) : $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    /**
     * Displays a single CoreSchoolCircular model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) :
            $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * Finds the CoreSchoolCircular model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoreSchoolCircular the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CoreSchoolCircular::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDownloadCirculars($id)
    {

        $query = new Query();
        $query->select(['csc.*', 'sc.id as student_circular_id', 'c_s.first_name', 'c_s.last_name', 'c_s.student_code', 'cs.school_name', 'cs.box_no', 'cs.contact_email_1', 'cs.contact_email_2', 'phone_contact_1', 'phone_contact_2', 'cs.vision', 'cs.school_reference_no'])
            ->from('student_circulars sc')
            ->innerJoin('core_school_circular csc', 'sc.circular_id=csc.id')
            ->innerJoin('core_school cs', 'csc.school_id=cs.id')
            ->innerJoin('core_student c_s', 'sc.student_id=c_s.id')
            ->where(['sc.circular_id' => $id]);
        $command = $query->createCommand();
        $data = $command->queryAll();
        Yii::trace($data);

        // get signatures attached
        $db = Yii::$app->db;
//        $signature = $db->createCommand('select cs.* from core_signatures cs left join circular_signatures  css on (cs.id=css.signature_id) where circular_id=:circular_id',
//            [':circular_id' => $id])->queryOne();
        $signature = '';
        Yii::trace($signature);
//            return $this->render('export_circular_pdf', ['circular_data' => $data, 'signature' => $signature
//            ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('export_circular_pdf', ['circular_data' => $data, 'signature' => $signature
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print student report',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
//                    'SetHeader' => ['Reports||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => [],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();

    }

    /**
     * Creates a new CoreSchoolCircular model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionCreate()
    {
        $model = new CoreSchoolCircular();
        $request = Yii::$app->request;
        if ($model->load(Yii::$app->request->post())) {
            $transaction = CoreSchoolCircular::getDb()->beginTransaction();
//            try {
                $data =Yii::$app->request->post();
                $model->created_by = Yii::$app->user->identity->getId();
                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $print_out = UploadedFile::getInstance($model, 'circular_print_out');
                Yii::trace($print_out);
                //attach the soft copy of the circular
                if (!is_null($print_out)) {
                    $model->attachment_file = $print_out->name;
                    Yii::trace($print_out->name);
                    $tmp_img = explode('.', $print_out->name);
                    $ext = end($tmp_img); //new image extension

                    $model->attachment_file = Yii::$app->security->generateRandomString() . ".{$ext}";
                    Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/school_circulars/';
                    $path = Yii::$app->params['uploadPath'] . $model->attachment_file;
                    $print_out->saveAs($path);
                }

            $stampUpload = UploadedFile::getInstances($model, 'school_stamp');
            if (is_array($stampUpload) && $stampUpload) {
                foreach ($stampUpload as $file) {
                    $tmpfile_contents = file_get_contents($file->tempName);
                    $model->school_stamp = base64_encode($tmpfile_contents);
                }
            }



                $model->created_by = Yii::$app->user->identity->getId();
                $model->date_modified = date('Y-m-d H:i:s');
                $model->circular_type = 'Generic';
                $model->signature_id = $data['CoreSchoolCircular']['signatories'];
                $model->save(false);

                $transaction->commit();
                Logs::logEvent("Add New School circular with  id: " . $model->id, null, null);
                return $this->redirect(['view', 'id' => $model->id]);
         /*   } catch (\Exception $exception) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $exception->getMessage();
                Yii::trace('Error: ' . $error, 'Create  School circular');
            }*/
        }
        return ($request->isAjax) ? $this->renderAjax('create', ['model' => $model]) : $this->render('create', ['model' => $model]);
    }

    /**
     * @param null $classes
     * @param null $student_codes
     * @return string
     * @throws \yii\base\InvalidArgumentException
     * @throws \yii\db\Exception
     */
    public function actionCircularGenerator($circular_id, $classes = null, $student_codes = null)
    {
        $searchModel = CoreSchoolCircular::findOne($circular_id);
        $model = new CoreSchoolCircular();
        $request = Yii::$app->request;
        $clsstudentIds = [];
        $studentIds = [];
        $recipient_members = [];
        $recipient = 'std_class';
        $connection = Yii::$app->db;

        $transaction = $connection->beginTransaction();

        try {
            if ($model->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();
                // Yii::trace($data);
                if ($data['CoreSchoolCircular']['classes']) {
                    // Yii::trace("..... classses.........");
                    //get student classes
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->andWhere(['in', 'class_id', array_values($data['CoreSchoolCircular']['classes'])])
                        ->all();
                    foreach ($query as $k => $v) {
                        array_push($clsstudentIds, $v['id']);
                    }
                    $recipient_members = $clsstudentIds;

                    if ($data['exclude_student_codes']) {
                        // Yii::trace($data['exclude_student_codes']);
                        //get student codes
                        $codes = $data['exclude_student_codes'];
                        $p_codes = preg_split("/[\s,]+/", $codes);
                        $p_codes = array_unique(array_filter($p_codes));
                        $query = (new Query())->select(['id'])->from('core_student')
                            ->andWhere(['in', 'student_code', $p_codes])->all();
                        foreach ($query as $k => $v) {
                            $stdExemption = new CustomisedCircularExemptions();

                            $stdExemption->circular_id = $circular_id;
                            $stdExemption->student_number = $v['id'];
                            $stdExemption->save(false);
                        }


                    }


                } else if ($data['student_codes']) {
                    //  Yii::trace("..... codes.........");
                    //Yii::trace($_POST['student_codes']);
//                    get student codes
                    $codes = $data['student_codes'];
                    $recipient = 'by_student_codes';
                    $p_codes = preg_split("/[\s,]+/", $codes);
                    $p_codes = array_unique(array_filter($p_codes));
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->where(['in', 'student_code', $p_codes])->all();
                    foreach ($query as $k => $v) {
                        array_push($studentIds, $v['id']);
                    }
                    $recipient_members = $studentIds;
                }


                $circularAss = new CustomisedCircularAssociations();
                $circularAss->circular_id = $circular_id;
                $circularAss->recipients_group = json_encode($recipient_members);
                $circularAss->recipient_group_type = $recipient;
                $circularAss->save(false);


                $transaction->commit();
                $attachRealData = $connection->createCommand("select process_customised_circular(
                        :ciruclar_id
                      )", [':ciruclar_id' => $circular_id])->execute();
                if ($attachRealData) {
                    return $this->redirect(['customised', 'circularId' => $circular_id]);
                } else {
                    throw new Exception("Data Transalation not succesful, try again later or contact contactcenter@awashbank.com");
                }
            }


        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace('Error: ' . $error, 'Generate circular callback');
        }
        $res = ['student_codes' => $student_codes, 'circular_id' => $circular_id, 'classes' => $classes, 'model' => $model];
        return ($request->isAjax) ? $this->renderAjax('circular_selector', $res) : $this->render('circular_selector', $res);

    }

    /**
     * Updates an existing CoreSchoolCircular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $data=Yii::$app->request->post();
            $model->signature_id = $data['CoreSchoolCircular']['signatories'];
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CoreSchoolCircular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCircular($id)
    {
        $query = (new Query())->select([
            'sch.school_name', 'sch.school_logo', 'csc.circular_type','csc.signature_id', 'csc.school_stamp',
            "concat_ws(' ', sch.village, sch.district) as physical_address",'sds.individual_name','sds.individual_position',
            'csc.id', 'csc.school_id', 'sch.phone_contact_1', 'sch.contact_email_1', 'circular_ref_no', 'effective_date', 'circular_subject', 'the_body'
        ])
            ->from('core_school_circular csc')
            ->innerJoin('core_school sch', 'sch.id=csc.school_id')
            ->innerJoin('district dst', 'dst.id=sch.district')
            ->innerJoin('school_digital_signature sds', 'sds.id=csc.signature_id')
            ->where(['csc.id' => $id])
            ->limit(1)
            ->one();
        if (!$query) {
            throw new NotFoundHttpException('Circular could not generate, make sure you ave attached a signature.');
        }

        $socialMedia = SchoolSocialMedia::findOne(['school_id' => Yii::$app->user->identity->school_id]);
        $sch_sign = $query['signature_id'];
        $signature='<img src="data:image/jpeg;base64,' . SignatureUploads::findOne(['submission_id' => $sch_sign])->file_data . '" width="130px" class="profile_img" /> ';

        Yii::trace($signature);

//        $html = $this->renderPartial('view_circular', ['data' => $query,'signature'=>$signature, 'smedia' => $socialMedia]);

        return $this->render('view_circular', [
            'data' => $query,'signature'=>$signature, 'smedia' => $socialMedia
        ]);
/*
        ob_clean();
        $sch_logo = $query['school_logo'];

        $watermarkimage = $sch_logo ? Url::to(['/import/import/image-link2', 'id' => $sch_logo]) : null;


        $watermark = $watermarkimage;
        return Yii::$app->pdf->exportCircular(['title' => 'School Circular',
            'filename' => 'school_circular', 'html' => $html, 'logo' => $watermark]);*/

    }

    public function actionViewCustomised($id, $code)
    {
        $query = (new Query())->select([
            'sch.school_name', 'sch.school_logo', 'csc.circular_type',
            "concat_ws(' ', sch.village, sch.district) as physical_address",
            'csc.id', 'csc.school_id', 'sch.phone_contact_1', 'sch.contact_email_1', 'circular_ref_no', 'effective_date', 'circular_subject', 'the_msg'
        ])
            ->from('core_school_circular csc')
            ->innerJoin('core_school sch', 'sch.id=csc.school_id')
            ->innerJoin('district dst', 'dst.id=sch.district')
            ->innerJoin('circular_customised_std_msg msg', 'csc.id=msg.circular_id')
            ->where(['csc.id' => $id])
            ->andWhere(['msg.payment_code' => $code])
            ->limit(1)
            ->one();
        if (!$query) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $socialMedia = SchoolSocialMedia::findOne(['school_id' => Yii::$app->user->identity->school_id]);

        $html = $this->renderPartial('view_custom_circular', ['data' => $query, 'smedia' => $socialMedia]);


        ob_clean();
        $sch_logo = $query['school_logo'];


        $watermarkimage = $sch_logo ? Url::to(['/import/import/image-link2', 'id' => $sch_logo]) : null;


        $watermark = $watermarkimage;
        return Yii::$app->pdf->exportCircular(['title' => 'School Circular',
            'filename' => 'school_circular', 'html' => $html, 'logo' => $watermark]);

    }

    public function actionUploadCircular()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('No permission to Upload circular.');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new UploadedCircular();

        try {
            if ($model->load(Yii::$app->request->post())) {


                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                    $model->created_by = Yii::$app->user->identity->id;

                }

                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                $uploadedCircularId = $model->saveUploads();
//                $model->saveUploads();
                $model->upload_id = $uploadedCircularId;
                $model->save(false);
                $transaction->commit();
                return $this->redirect(['view-uploaded-circular', 'id' => $model->id]);


            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = $e->getMessage();
            Logs::logEvent("Failed to new administrative signature : ", $model_error, null);
        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create_uploaded_circular', $res) : $this->render('create_uploaded_circular', $res);


    }

    /**
     * Displays a single Social Media link model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionViewUploadedCircular($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view_uploaded_circular', [
            'model' => $this->findUploadedCircularModel($id),
        ]) :
            $this->render('view_uploaded_circular', [
                'model' => $this->findUploadedCircularModel($id),
            ]);
    }

    protected function findUploadedCircularModel($id)
    {
        if (($model = UploadedCircular::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * delete signature submission
     *
     */
    public function actionRemoveFile($id)
    {
        $model = CircularUploads::find()->where(['id' => $id])->limit(1)->one();
        $submission_id = $model->submission_id;
        $model->delete();
        return $this->redirect(['school-circular/view-uploaded-circular', 'id' => $submission_id]);
    }

    /*
     * View/Download file
     */
    public function actionViewFile($id)
    {
        $response = Yii::$app->response;
        $model = CircularUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'mp3', 'mp4']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->headers->set('Content-type', $ext);
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);
        return $response;
    }

    public function actionUpdateUploadedCircular($id)
    {
        $request = Yii::$app->request;

        $model = $this->findUploadedCircularModel($id);

        if (Yii::$app->user->can('rw_social_media', ['sch' => $model->school_id])) {
            try {
                $connection = Yii::$app->db;

                $transaction = $connection->beginTransaction();
                if ($model->load(Yii::$app->request->post())) {

                    if (\app\components\ToWords::isSchoolUser()) {
                        $model->school_id = Yii::$app->user->identity->school_id;

                    }

                    $model->uploads = UploadedFile::getInstances($model, 'uploads');

                    $model->save(false);

                    $model->saveUploadUpdate($id);
                    $transaction->commit();
                    return $this->redirect(['view-uploaded-circular', 'id' => $model->id]);

                }
            } catch (\Exception $e) {
                Yii::trace($e);
                $transaction->rollBack();
                $model_error = $e->getMessage();
                Logs::logEvent("Failed to new administrative signature : ", $model_error, null);
            }

            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('create_uploaded_circular', $res) : $this->render('create_uploaded_circular', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }

    public function actionDeleteUploadedCircular($id)
    {
        $this->findUploadedCircularModel($id)->delete();

        return $this->redirect(['index-uploaded-circular']);
    }

    public function actionIndexUploadedCircular()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');

        $request = Yii::$app->request;
        $searchModel = new UploadedCircular();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index_uploaded_circular', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index_uploaded_circular', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }

    public function actionCreateCustomisedCircular()
    {
        $model = new CoreSchoolCircular();
        $request = Yii::$app->request;
        Yii::trace("am here");
        if ($model->load(Yii::$app->request->post())) {
            $transaction = CoreSchoolCircular::getDb()->beginTransaction();
            try {
                $model->created_by = Yii::$app->user->identity->getId();
                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $data = Yii::$app->request->post();

                $stampUpload = UploadedFile::getInstances($model, 'school_stamp');
                if (is_array($stampUpload) && $stampUpload) {
                    foreach ($stampUpload as $file) {
                        $tmpfile_contents = file_get_contents($file->tempName);
                        $model->school_stamp = base64_encode($tmpfile_contents);
                    }
                }

                $model->the_body = $data['CoreSchoolCircular']['message_template'];
                $model->created_by = Yii::$app->user->identity->getId();
                $model->circular_type = 'Customised';
                $model->signature_id = $data['CoreSchoolCircular']['signatories'];
                $model->save(false);

                $transaction->commit();
                Logs::logEvent("Add New School circular with  id: " . $model->id, null, null);
                return $this->redirect(['view-customised-circular', 'id' => $model->id]);
            } catch (\Exception $exception) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $exception->getMessage();
                Yii::trace('Error: ' . $error, 'Create  School circular');
            }
        }
        return ($request->isAjax) ? $this->renderAjax('create_customised_circular', ['model' => $model]) : $this->render('create_customised_circular', ['model' => $model]);
    }

    public function actionUpdateCustomisedCircular($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            if ($data['CoreSchoolCircular']['message_template']) {
                $model->the_body = $data['CoreSchoolCircular']['message_template'];

            }
            $model->created_by = Yii::$app->user->identity->getId();
            $model->date_modified = date('Y-m-d H:i:s');
            $model->circular_type = 'Customised';
            $model->signature_id = $data['CoreSchoolCircular']['signatories'];
            $model->save(false);
            return $this->redirect(['school-circular/view-customised-circular', 'id' => $model->id]);
        }

        return $this->render('create_customised_circular', [
            'model' => $model,
        ]);
    }

    public function actionViewCustomisedCircular($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view_customised_circular', [
            'model' => $this->findModel($id),
        ]) :
            $this->render('view_customised_circular', [
                'model' => $this->findModel($id),
            ]);
    }

    public function actionIndexCustomisedCirculars()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('No permissions to view school circulars.');
        }
        $searchModel = new CoreSchoolCircularSearch();
        $request = Yii::$app->request;
        $dataProvider = $searchModel->searchCustomised(Yii::$app->request->queryParams);
        return ($request->isAjax) ? $this->renderAjax('index_customised', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) : $this->render('index_customised', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionCustomised($circularId)
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('No permissions to view school circulars.');
        }
        $searchModel = new CoreSchoolCircularSearch();
        $request = Yii::$app->request;
        $dataProvider = $searchModel->searchAllCustomised($circularId);
        return ($request->isAjax) ? $this->renderAjax('index_all_customised', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) : $this->render('index_all_customised', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionManageCirculars()
    {
        if (!\app\components\ToWords::isSchoolUser()) {
            throw new ForbiddenHttpException('No permissions to view school circulars.');
        }
        $searchModel = new CoreSchoolCircularSearch();
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('circular_index') : $this->render('circular_index'
        );
    }


    public function actionCustomizedCircularGenerator($id, $classes = null, $student_codes = null)
    {

        $data = Yii::$app->request->post();
        Yii::trace($data);
        $model = new CoreSchoolCircular();
        $request = Yii::$app->request;
        $studentIds = [];
        $studentClassArray = [];

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            Yii::trace($data);
//            try {
                if (isset($data['CoreSchoolCircular']['classes'])) {
                    Yii::trace("..... classses.........");
                    Yii::trace($data['CoreSchoolCircular']['classes']);
                    $studentClassArray = $data['CoreSchoolCircular']['classes'];
                    //get student classes
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->andWhere(['in', 'class_id', array_values($data['CoreSchoolCircular']['classes'])])
                        ->all();

                    if (isset($_POST['exclude_student_codes'])) {
                        Yii::trace($_POST['exclude_student_codes']);
                        //get student codes
                        $codes = $_POST['exclude_student_codes'];
                        $p_codes = preg_split("/[\s,]+/", $codes);

                        $p_codes = array_unique(array_filter($p_codes));
                        Yii::trace($p_codes);
                        $query = (new Query())->select(['id'])->from('core_student')
                            ->andWhere(['in', 'class_id', array_values($data['CoreSchoolCircular']['classes'])])
                            ->andWhere(['not in', 'student_code', $p_codes])->all();

                    }
//                $query = array_unique($query);
                    foreach ($query as $v) {
                        array_push($studentIds, $v['id']);
                    }

                } else if (isset($data['CoreSchoolCircular']['student_class'])) {
                   // Yii::trace("..... codes.........");
                 //   Yii::trace($_POST['student_codes']);
//                    get student codes
                    $studentClass = $data['CoreSchoolCircular']['student_class'];
                    $codes = $_POST['student_codes'];
                    $p_codes = preg_split("/[\s,]+/", $codes);
                    $p_codes = array_unique(array_filter($p_codes));
                    $query = (new Query())->select(['id','class_id'])
                        ->from('core_student')
                        ->where(['in', 'student_code', $p_codes])
                        ->all();
//                $query = array_unique($query);
                    foreach ($query as $k => $v) {
                        array_push($studentIds, $v['id']);
                        array_push($studentClassArray, $v['class_id']);

                    }
                }


//                Yii::trace($studentIds);
//                Yii::trace($studentClassArray);
                // get signatures attached
                /*$db = Yii::$app->db;
                $signature = $db->createCommand('select cs.* from core_signatures cs left join circular_signatures  css on (cs.id=css.signature_id) where css.circular_id=:circular_id',
                    [':circular_id' => $circular_id])->queryOne();
                Yii::trace($signature);*/
                //now generator the PDF's
                $student_query = new Query();
                $student_query->select(['c_s.first_name', 'c_s.last_name','cl.class_description','cl.class_code', 'c_s.student_code','c_s.guardian_name', 'cs.school_name', 'cs.box_no', 'cs.contact_email_1', 'cs.contact_email_2', 'phone_contact_1', 'phone_contact_2', 'cs.vision', 'cs.school_reference_no'])
                    ->from('core_student c_s')
                    ->innerJoin('core_school cs', 'c_s.school_id=cs.id')
                    ->innerJoin('core_school_class cl', 'cl.school_id=cs.id')
                    ->where(['in', 'c_s.id', $studentIds])
                    ->andWhere(['in', 'cl.id', $studentClassArray]);
                $command = $student_query->createCommand();
                $student_data = $command->queryAll();


                //Yii::trace($student_data);
                $circular_data = [];

                foreach ($student_data as $v) {
                   // Yii::trace($v);
                    /*    ini_set('max_execution_time', '300');
                        ini_set("pcre.backtrack_limit", "10000000");*/
                    $circular_data[] = array(
                        'school_name' => $v['school_name'],
                        'box_no' => $v['box_no'],
                        'phone_contact_1' => $v['phone_contact_1'],
                        'phone_contact_2' => $v['phone_contact_2'],
                        'contact_email_1' => $v['contact_email_1'],
                        'first_name' => $v['first_name'],
                        'last_name' => $v['last_name'],
                        'student_code' => $v['student_code'],
                        'class_name' => $v['class_description'],
                        'class_code' => $v['class_code'],
                        'vision' => $v['vision'],
                        'guardian_name' => $v['guardian_name']
                    );
                }
            //    Yii::trace($circular_data);


            $query2 = (new Query())->select([
                'sch.school_name', 'sch.school_logo', 'csc.circular_type','csc.signature_id', 'csc.school_stamp',
                "concat_ws(' ', sch.village, sch.district) as physical_address",'sds.individual_name','sds.individual_position',
                'csc.id', 'csc.school_id', 'sch.phone_contact_1', 'sch.contact_email_1', 'circular_ref_no', 'effective_date', 'circular_subject', 'the_body'
            ])
                ->from('core_school_circular csc')
                ->innerJoin('core_school sch', 'sch.id=csc.school_id')
                ->innerJoin('district dst', 'dst.id=sch.district')
                ->innerJoin('school_digital_signature sds', 'sds.id=csc.signature_id')
                ->where(['csc.id' => $id])
                ->limit(1)
                ->one();
            if (!$query2) {
                throw new NotFoundHttpException('Circular could not generate, make sure you ave attached a signature.');
            }

            $socialMedia = SchoolSocialMedia::findOne(['school_id' => Yii::$app->user->identity->school_id]);
            $sch_sign = $query2['signature_id'];
            $signature='<img src="data:image/jpeg;base64,' . SignatureUploads::findOne(['submission_id' => $sch_sign])->file_data . '" width="130px" class="profile_img" /> ';

            Yii::trace($signature);

//        $html = $this->renderPartial('view_circular', ['data' => $query,'signature'=>$signature, 'smedia' => $socialMedia]);

         /*   return $this->render('view_circular', [
                'data' => $query,'signature'=>$signature, 'smedia' => $socialMedia
            ]);*/

                return $this->render('generate_customized_circular', [
                    'circular_data' => $circular_data,'data' => $query2,'signature'=>$signature, 'smedia' => $socialMedia
                ]);

          /*  } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace('Error: ' . $error, 'Generate circular callback');
            }*/
        }

        $res = ['student_codes' => $student_codes,  'classes' => $classes, 'model' => $model,'circular_id'=>$id];
        return ($request->isAjax) ? $this->renderAjax('circular_selector', $res) : $this->render('circular_selector', $res);

    }
}

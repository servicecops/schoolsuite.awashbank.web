<?php

namespace app\modules\studentreport\controllers;

use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\studentreport\models\CircularSignatures;
use app\modules\studentreport\models\SchoolESealsUploads;
use app\modules\studentreport\models\SchoolMissionVision;
use kartik\mpdf\Pdf;
use Yii;
use app\modules\logs\models\Logs;

use yii\base\BaseObject;
use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Social Media implements the CRUD actions for social Media model.
 */
class SchoolMissionVisionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Social Media link models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('r_sch'))
            throw new ForbiddenHttpException('No permissions to view schools.');
Yii::trace("here");
        $request = Yii::$app->request;
        $searchModel = new SchoolMissionVision();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return ($request->isAjax) ? $this->renderAjax('index', [
            'searchModel' => $searchModel, 'dataProvider' => $dataProvider]) :
            $this->render('index', ['searchModel' => $searchModel,
                'dataProvider' => $dataProvider]);


    }
    /**
     * Displays a single Social Media link model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]) :
            $this->render('view', [
                'model' => $this->findModel($id),
            ]);
    }

    /**
     * Finds the Social Media link model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Social Media link the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SchoolMissionVision::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }




    /**
     * Creates a new Social Media link model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('rw_social_media') && !Yii::$app->user->can('schoolpay_admin')) {
            throw new ForbiddenHttpException('No permission to Write or Update to a Social media links.');
        }

        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;
        $model = new SchoolMissionVision();

        try {
            if ($model->load(Yii::$app->request->post())) {

                $incoming = Yii::$app->request->post();
                $post = $incoming['SchoolMissionVision'];

                if (\app\components\ToWords::isSchoolUser()) {
                    $model->school_id = Yii::$app->user->identity->school_id;
                }
                $model->created_by = Yii::$app->user->identity->getId();
                $model->uploads = UploadedFile::getInstances($model, 'uploads');

                $model->save(false);
                $model->saveUploads();

                Logs::logEvent("Created New Social Media Link: " . $model->id, null, null);

                $transaction->commit();
                return $this->redirect(['view', 'id' => $model->id], 200);

                //   }
            }
        } catch (\Exception $e) {
            Yii::trace($e);
            $transaction->rollBack();
            $model_error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();;
            Logs::logEvent("Failed to new school mission vision : ", $model_error, null);
            \Yii::$app->session->setFlash('actionFailed', $model_error);

        }

        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create', $res) : $this->render('create', $res);


    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;

        $model = $this->findModel($id);
        if (Yii::$app->user->can('rw_social_media', ['sch' => $model->school_id])) {
            if ($model->load(Yii::$app->request->post())) {

                $post = $model->load(Yii::$app->request->post());
                Yii::trace($post);
                $transaction = SchoolMissionVision::getDb()->beginTransaction();
                try {
                    //$newId =$this->saveUser();
                    $model->created_by = Yii::$app->user->identity->getId();
                    $model->uploads = UploadedFile::getInstances($model, 'uploads');

                    $model->save(false);
                    $model->saveUploadUpdate($id);


                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                    Yii::trace('Error: ' . $error, 'Update failed Vision Mission seal ROLLBACK');
                    \Yii::$app->session->setFlash('actionFailed', $error);

                }

            }
            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);

        } else {
            throw new ForbiddenHttpException('No permissions to create or update students.');
        }

    }
    /**
     * Deletes an existing CoreSchoolClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionRemoveFile($id)
    {
        $model = SchoolESealsUploads::find()->where(['id' => $id])->limit(1)->one();
        $submission_id = $model->submission_id;
        $model->delete();
        return $this->redirect(['view', 'id' => $submission_id]);
    }
    /*
       * View/Download file
       */
    public function actionViewFile($id)
    {
        $response = Yii::$app->response;
        $model = SchoolESealsUploads::find()->where(['id' => $id])->limit(1)->one();
        $name = explode('.', $model->file_name);
        $ext = $name[count($name) - 1];
        $disposition = in_array($ext, ['png', 'jpg', 'jpeg', 'gif', 'mp3', 'mp4']) ? 'inline;' : 'attachment;';
        $response->format = \yii\web\Response::FORMAT_RAW;
        $response->headers->set('Content-type', $ext);
        $response->headers->set('Content-Disposition', $disposition . ' filename="' . $model->file_name . '"');

        $response->content = base64_decode($model->file_data);
        return $response;
    }

}

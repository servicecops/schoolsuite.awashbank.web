<?php

namespace app\modules\studentreport\controllers;


use app\models\AuthItem;
use app\modules\attendance\models\Rollcall;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreStudent;
use app\modules\schoolcore\models\CoreStudentGroupStudent;
use app\modules\schoolcore\models\CoreTerm;
use app\modules\studentreport\models\CumulativeStudentMarksPerSubject;
use app\modules\studentreport\models\Report;
use app\modules\studentreport\models\ReportGroupSubject;
use app\modules\studentreport\models\ReportSearch;
use app\modules\studentreport\models\ReportSubjectAssociation;
use app\modules\studentreport\models\ReportSubjectComposition;
use app\modules\studentreport\models\ReportTermAssociation;
use app\modules\studentreport\models\SubjectSemesterAssementAssocaition;
use yii\web\Response;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

if (!Yii::$app->session->isActive) {
    session_start();
}

/**
 * FeesDueController implements the CRUD actions for FeesDue model.
 */
class StudentReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new ReportSearch();
        $data = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']];
        return $request->isAjax ? $this->renderAjax('index', $res) : $this->render('index', $res);
    }
    /**
     * Lists all current Semesters attached to Report models.
     * @return mixed
     */
    public function actionSemesterReports()
    {
        $request = Yii::$app->request;
        $searchModel = new ReportSearch();
        $data = $searchModel->searchSemReports(Yii::$app->request->queryParams);
        $dataProvider = $data['query'];
        $pages = ['pages' => $data['pages'], 'page' => $data['cpage']];
        $res = ['dataProvider' => $dataProvider, 'searchModel' => $searchModel, 'pages' => $pages, 'sort' => $data['sort']];
        return $request->isAjax ? $this->renderAjax('semester_reports_index', $res) : $this->render('semester_reports_index', $res);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        return $request->isAjax ? $this->renderAjax('view', ['model' => $model]) :
            $this->render('view', ['model' => $model]);
    }

    /**
     * Displays a single semester Report model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewSemReport($id)
    {
        $request = Yii::$app->request;
        $model = ReportTermAssociation::findOne($id);



        return $request->isAjax ? $this->renderAjax('view_reports', ['model' => $model]) :
            $this->render('view_reports', ['model' => $model]);
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = null;
        if (\app\components\ToWords::isSchoolUser()) {
            //If school user, limit and find by id and school id
            $model = Report::findOne(['id' => $id, 'school_id' => Yii::$app->user->identity->school_id]);
        } else {
            $model = Report::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Report model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Report();

        if ($model->load(Yii::$app->request->post())) {
            if (\app\components\ToWords::isSchoolUser()) {
                $model->school_id = Yii::$app->user->identity->school_id;
            }
            if (!isset($_POST['Report']['active'])) {
                $model->active = false;
            }
            $model->class_id = $_POST['Report']['classes'];
            if ($model->save()) {
                return $this->redirect(['student-report/assign', 'id' => $model->id]);
                // return Yii::$app->runAction('/student-group/index');
            }
        } else {
            $res = ['model' => $model];
            return ($request->isAjax) ? $this->renderAjax('create', $res) :
                $this->render('create', $res);
        }
    }

    public function actionClassGroup()
    {
        $request = Yii::$app->request;
        $model = new Report();

        if ($model->load(Yii::$app->request->post())) {
            if (\app\components\ToWords::isSchoolUser()) {
                $model->school_id = Yii::$app->user->identity->school_id;
            }

            if (!isset($_POST['Report']['active'])) {
                $model->active = false;
            }
//            if (!isset($_POST['Report']['average'])) {
//                $model->average = false;
//            }
            $connection = Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                if ($model->validate()) {
                    $model->created_by = Yii::$app->user->identity->getId();;
                    $model->save(false);
                    if ($model->classes) {
                        $query = (new Query())->select(['id'])->from('core_subject')
                            ->andWhere(['in', 'class_id', array_values($model->classes)])->all();
                        Yii::trace($query);


                        foreach ($query as $k => $v) {

                            $student = new Report();
                            $student->group_id = $model->id;
                            $student->subject_id = $v['id'];
                            $student->save(false);
                        }

                    }

                    $transaction->commit();

                    Logs::logEvent("Report group created: (" . $model->id . ")", null, null);
                    return $this->redirect(['student-report/view', 'id' => $model->id]);
                }else{
                    Yii::trace($model->errors);
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Logs::logEvent("Error creating report group: ", $error, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->redirect('student-errror/error');
            }
        }
        //setting initial values of class searchncreate
        $selectedVal = [];
        $model->classes = !$model->classes ? [] : $model->classes;
        if (count($model->classes) > 0) {
            foreach ($model->classes as $v) {
                $selectedVal[$v] = CoreSchoolClass::findOne($v)->class_code;
            }
        }
        $res = ['model' => $model, 'selectedVal' => $selectedVal];
        return ($request->isAjax) ? $this->renderAjax('create_from_class', $res) :
            $this->render('create_from_class', $res);

    }

    /**
     * Updates an existing Report model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if (\app\components\ToWords::isSchoolUser()) {
                $model->school_id = Yii::$app->user->identity->school_id;
            }
            if ($model->save()) {
                // return $this->redirect(['/student-group/index']);
                return Yii::$app->runAction('/studentreport/student-report/index');
            }
        }
        $selectedVal = [];
        $res = ['model' => $model];
        return ($request->isAjax) ? $this->renderAjax('create_from_class', $res) :
            $this->render('create_from_class', $res);
       // return ($request->isAjax) ? $this->renderAjax('update', $res) : $this->render('update', $res);
    }

    public function actionAssign($id)
    {

        //Yii::trace($average);
        $request = Yii::$app->request;
        $model = ReportTermAssociation::findOne([$id]);
        $searchModel = new ReportSearch();
        $school = CoreSchool::findOne(['id' => $model->school_id]);
        $class = CoreSchoolClass::findOne(['id' => $model->class_id]);
        $data = [];

        // $model2 = new DynamicModel(['gender','student_class', 'searchTerm', 'day_boarding']);
        if ($searchModel->load(Yii::$app->request->get())) {
            $grp = ['grId' => $model->id, 'sch' => $model->school_id, 'cls' => $model->class_id];
            $data = $searchModel->findSubjects($grp, $_GET['ReportSearch']);
            Yii::trace($data);
        }

        if (isset($_POST['empty-group']) && $_POST['empty-group'] == 'empty-group') {
            $this->emptyGroup($model->id);
             $this->addTestComposition($model->id, $_POST['Report'], $model->school_id);
//
        } else if (isset($_POST['selectedtest'])) {

            $data =$_POST['selectedtest'];
            $model2 = new SubjectSemesterAssementAssocaition();
            $model2->report_id = $id;
            $model2->assessment_id = $data['assessment_id'];
            $model2->subject_id =$data['subject_id'];
            $model2->assessment_composition =$data['total_marks'];
            $model2->school_id =$model->school_id;
            $model2->class_id =$model->class_id;
            $model2->semester_type_id =$model->term_id;

            $model2->save(false);
            $this->redirect(['student-report/assign-assessments','id'=>$id,'subId'=>$subId]);


            //  $this->addMembers($model->id, $_POST['selected_stu'],$BOT,$MOT,$EOT, $model->school_id);
        } else if (isset($_POST['remove_stu'])) {
            $this->removeMembers($model->id, $_POST['remove_stu']);
        }

        $members = $searchModel->findClassSubject($model->id);
        $res = ['model' => $model, 'searchModel' => $searchModel, 'average' => false, 'data' => $data, 'members' => $members, 'school' => $school, 'class' => $class];
        return $request->isAjax ? $this->renderAjax('assign_subjects', $res) :
            $this->render('assign_subjects', $res);

    }

    public function actionSelectSubject($id)
    {
        if (!\app\components\ToWords::isSchoolUser() &&! Yii::$app->user->can('is_teacher')) {
            throw new ForbiddenHttpException('No permissions to Assign Assements');

        }

        $request = Yii::$app->request;
        $model = ReportTermAssociation::findOne([$id]);
        $searchModel = new ReportSearch();
        $school = CoreSchool::findOne(['id' => $model->school_id]);
        $class = CoreSchoolClass::findOne(['id' => $model->class_id]);

        $trQuery = new Query();
        $trQuery->select(['tc.subject_id', 'sub.subject_code','sub.subject_name' ])
            ->from('teacher_class_subject_association tc')
            ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
            ->innerJoin('user uz', 'cs.id=uz.school_user_id')
            ->innerJoin('core_subject sub', 'sub.id=tc.subject_id')
            ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


        $commandt = $trQuery->createCommand();
        $data = $commandt->queryAll();


        $res = ['model' => $model, 'searchModel' => $searchModel, 'data'=>$data];
        return $request->isAjax ? $this->renderAjax('select_subjects', $res) :
            $this->render('select_subjects', $res);

    }

    public function actionAssignAssessments($id, $subId)
    {
        if (!\app\components\ToWords::isSchoolUser() &&! Yii::$app->user->can('is_teacher')) {
            throw new ForbiddenHttpException('No permissions to Assign Assements');

        }

        $request = Yii::$app->request;
        $model = ReportTermAssociation::findOne([$id]);
        $searchModel = new ReportSearch();

        $termDetail = CoreTerm::findOne(['id' => $model->term_id]);

        $query1 = SubjectSemesterAssementAssocaition::find()->select(['assessment_id'])->where(['report_id'=>$id]);

        $school = CoreSchool::findOne(['id' => $model->school_id]);
        $class = CoreSchoolClass::findOne(['id' => $model->class_id]);

        $trQuery = new Query();
        $trQuery->select(['ct.subject_id','ct.id as test_id','ct.test_description','start_date', 'total_marks','sub.subject_code','sub.subject_name',
            'sub.id as subject_id',
            ])
            ->from('core_test ct')
            ->innerJoin('core_term tm', 'tm.id=ct.term_id')
            ->innerJoin('core_subject sub', 'sub.id=ct.subject_id')
            ->Where(['between','ct.start_date',$termDetail->term_starts,$termDetail->term_ends])
            ->andWhere(['ct.subject_id'=>$subId]);

        $trQuery->andWhere(['not in', 'ct.id', $query1]);

        $commandt = $trQuery->createCommand();
        $data = $commandt->queryAll();
        $members = $searchModel->findClassSubject($model->id);


        $res = ['model' => $model, 'searchModel' => $searchModel, 'data'=>$data,'members'=>$members,'subId'=>$subId];
        return $request->isAjax ? $this->renderAjax('assign_subjects', $res) :
            $this->render('assign_subjects', $res);

    }


    public function actionCalculateFinalmark($id, $subId)
    {
        if (!\app\components\ToWords::isSchoolUser() &&! Yii::$app->user->can('is_teacher')) {
            throw new ForbiddenHttpException('No permissions to Assign Assements');

        }

        $request = Yii::$app->request;
        $model = ReportTermAssociation::findOne([$id]);

        $query1 = SubjectSemesterAssementAssocaition::find()->select(['assessment_id'])->where(['report_id'=>$id,'subject_id'=>$subId]);

       // $students = CoreStudent::findAll(['class_id'=>$model->class_id]);



        $terms = CoreTerm::findOne(['id'=>$model->term_id]);

        Yii::trace("====\n terms");
        Yii::trace($terms);
        Yii::trace($terms->term_type_detail);
        $stdQuery = new Query();
        $stdQuery->select(['cs.id',
        ])
            ->from('core_student cs')
            ->Where(['cs.class_id' => $model->class_id]);

        $stdCmd = $stdQuery->createCommand();
        $students = $stdCmd->queryAll();


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {

            if($students) {
                foreach ($students as $k=>$y) {

                    Yii::trace("student Id: ". $y['id']);


                    //get student marks per assessment
                    $trQuery = new Query();
                    $trQuery->select(['cm.student_id', 'cm.test_id', 'cm.marks_obtained'
                    ])
                        ->from('core_marks cm')
                        ->innerJoin('core_student st', 'st.id=cm.student_id')
                        ->andWhere(['cm.student_id' => $y['id']]);

                    $trQuery->andWhere(['in', 'cm.test_id', $query1]);

                    $commandt = $trQuery->createCommand();
                    $stdResults = $commandt->queryAll();


                    $totalmarks = 0;

                    //add up student marks

                    Yii::trace($stdResults);
                    foreach ($stdResults as  $x) {
                        Yii::trace($x['marks_obtained']);
                        $totalmarks = $totalmarks+ $x['marks_obtained'];

                       // Yii::trace("total mark: " .$totalmarks);
                    }

                    //insert final student mark in finalmark table

                    //check if value already exits
                    //Yii::trace("existingMark " . $existingMark);


                    Yii::trace("\n=======\n term type ============\n");
                    Yii::trace($terms->term_type_detail);

                        if((strcasecmp('SEMESTER_ONE', $terms->term_type_detail) == 0)){

                            $finalMarks = new CumulativeStudentMarksPerSubject();
                            $finalMarks->student_id = $y['id'];
                           // $finalMarks->final_mark = $totalmarks;
                            $finalMarks->report_id = $model->report_id;
                            $finalMarks->semester_id = $model->term_id;
                            $finalMarks->report_term_association = $id;
                            $finalMarks->school_id = $model->school_id;
                            $finalMarks->class_id = $model->class_id;
                            $finalMarks->subject_id = $subId;


                            $finalMarks->final_mark_semester_one = $totalmarks;
                            $finalMarks->save(false);
                        }
                        else if((strcasecmp('QUARTER_ONE', $terms->term_type_detail) == 0)){

                            $finalMarks = new CumulativeStudentMarksPerSubject();
                            $finalMarks->student_id = $y['id'];
                            $finalMarks->report_id = $model->report_id;
                            $finalMarks->semester_id = $model->term_id;
                            $finalMarks->report_term_association = $id;
                            $finalMarks->school_id = $model->school_id;
                            $finalMarks->class_id = $model->class_id;
                            $finalMarks->subject_id = $subId;


                            $finalMarks->final_mark_quarter_one = $totalmarks;
                            $finalMarks->save(false);
                        }
                        else if((strcasecmp('SEMESTER_TWO', $terms->term_type_detail) == 0)){

                            //update existing record
                            $existingMark = CumulativeStudentMarksPerSubject::findOne(['student_id' => $y['id'], 'report_id' => $model->report_id, 'subject_id' => $subId]);

                            if($existingMark){
                                $existingMark->final_mark_semester_two = $finalMarks;
                                $existingMark->save(false);
                            }else {
                                $finalMarks = new CumulativeStudentMarksPerSubject();
                                $finalMarks->student_id = $y['id'];
                                $finalMarks->report_id = $model->report_id;
                                $finalMarks->semester_id = $model->term_id;
                                $finalMarks->report_term_association = $id;
                                $finalMarks->school_id = $model->school_id;
                                $finalMarks->class_id = $model->class_id;
                                $finalMarks->subject_id = $subId;
                                $finalMarks->final_mark_semester_two = $totalmarks;
                                $finalMarks->save(false);
                            }
                        }
                        else if((strcasecmp('QUARTER_TWO', $terms->term_type_detail) == 0)){

                            //update existing record
                            $existingMark = CumulativeStudentMarksPerSubject::findOne(['student_id' => $y['id'], 'report_id' => $model->report_id, 'subject_id' => $subId]);

                            if($existingMark){
                                $existingMark->final_mark_quarter_two = $finalMarks;
                                $existingMark->save(false);
                            }else {
                                $finalMarks = new CumulativeStudentMarksPerSubject();
                                $finalMarks->student_id = $y['id'];
                                $finalMarks->report_id = $model->report_id;
                                $finalMarks->semester_id = $model->term_id;
                                $finalMarks->report_term_association = $id;
                                $finalMarks->school_id = $model->school_id;
                                $finalMarks->class_id = $model->class_id;
                                $finalMarks->subject_id = $subId;
                                $finalMarks->final_mark_quarter_two = $totalmarks;
                                $finalMarks->save(false);
                            }
                        }
                        else if((strcasecmp('QUARTER_THREE', $terms->term_type_detail) == 0)){

                            //update existing record
                            $existingMark = CumulativeStudentMarksPerSubject::findOne(['student_id' => $y['id'], 'report_id' => $model->report_id, 'subject_id' => $subId]);

                            if($existingMark){
                                $existingMark->final_mark_quarter_three = $finalMarks;
                                $existingMark->save(false);
                            }else {
                                $finalMarks = new CumulativeStudentMarksPerSubject();
                                $finalMarks->student_id = $y['id'];
                                $finalMarks->report_id = $model->report_id;
                                $finalMarks->semester_id = $model->term_id;
                                $finalMarks->report_term_association = $id;
                                $finalMarks->school_id = $model->school_id;
                                $finalMarks->class_id = $model->class_id;
                                $finalMarks->subject_id = $subId;
                                $finalMarks->final_mark_quarter_three = $totalmarks;
                                $finalMarks->save(false);
                            }
                        }
                        else if((strcasecmp('QUARTER_FOUR', $terms->term_type_detail) == 0)){

                            //update existing record
                            $existingMark = CumulativeStudentMarksPerSubject::findOne(['student_id' => $y['id'], 'report_id' => $model->report_id, 'subject_id' => $subId]);

                            if($existingMark){
                                $existingMark->final_mark_quarter_four = $finalMarks;
                                $existingMark->save(false);
                            }else {
                                $finalMarks = new CumulativeStudentMarksPerSubject();
                                $finalMarks->student_id = $y['id'];
                                $finalMarks->report_id = $model->report_id;
                                $finalMarks->semester_id = $model->term_id;
                                $finalMarks->report_term_association = $id;
                                $finalMarks->school_id = $model->school_id;
                                $finalMarks->class_id = $model->class_id;
                                $finalMarks->subject_id = $subId;
                                $finalMarks->final_mark_quarter_four = $totalmarks;
                                $finalMarks->save(false);
                            }
                        }






                }
                $transaction->commit();
              ///  Yii::trace("commit");
                $userId = Yii::$app->user->identity->getId();
                Logs::logEvent("Successfuly generated cumulative mark for: " . $subId . ", sememster" . $model->term_id . " ,by: " . $userId, null, null);

                $this->redirect(['student-report/assign-assessments','id'=>$id,'subId'=>$subId]);
            }

            Logs::logEvent("No students found for this class " ,null, null);

        }catch (\Exception $e){

            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Yii::trace('Error: ' . $error, 'CREATE  COMPOSITION ROLLBACK');
            Logs::logEvent("Error creating Test Composition: ", $error, null, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            Yii::trace($error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }


    }


    public function actionGenerateReport($id)
    {
        if (!\app\components\ToWords::isSchoolUser() &&! Yii::$app->user->can('is_teacher')) {
            throw new ForbiddenHttpException('No permissions to Assign Assements');

        }

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        Yii::trace($model->class_id);

        $query1 = CumulativeStudentMarksPerSubject::find()->select(['assessment_id'])->where(['report_id'=>$id,'subject_id'=>$subId]);

        // $students = CoreStudent::findAll(['class_id'=>$model->class_id]);

        $stdQuery = new Query();
        $stdQuery->select(['cs.id',
        ])
            ->from('core_student cs')
            ->Where(['cs.class_id' => $model->class_id]);

        $stdCmd = $stdQuery->createCommand();
        $students = $stdCmd->queryAll();


        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {

            if($students) {
                foreach ($students as $k=>$y) {

                    Yii::trace("student Id: ". $y['id']);


                    //get student marks per assessment
                    $trQuery = new Query();
                    $trQuery->select(['cm.student_id', 'cm.test_id', 'cm.marks_obtained'
                    ])
                        ->from('core_marks cm')
                        ->innerJoin('core_student st', 'st.id=cm.student_id')
                        ->andWhere(['cm.student_id' => $y['id']]);

                    $trQuery->andWhere(['in', 'cm.test_id', $query1]);

                    $commandt = $trQuery->createCommand();
                    $stdResults = $commandt->queryAll();


                    $totalmarks = 0;

                    //add up student marks

                    Yii::trace($stdResults);
                    foreach ($stdResults as  $x) {
                        Yii::trace($x['marks_obtained']);
                        $totalmarks = $totalmarks+ $x['marks_obtained'];

                        // Yii::trace("total mark: " .$totalmarks);
                    }

                    //insert final student mark in finalmark table

                    //check if value already exits
                    $existingMark = CumulativeStudentMarksPerSubject::findOne(['student_id' => $y['id'], 'report_id' => $id, 'semester_id' => $model->term_id, 'subject_id' => $subId]);
                    //Yii::trace("existingMark " . $existingMark);
                    if (!$existingMark) {
                        $finalMarks = new CumulativeStudentMarksPerSubject();
                        $finalMarks->student_id = $y['id'];
                        $finalMarks->final_mark = $totalmarks;
                        $finalMarks->report_id = $id;
                        $finalMarks->semester_id = $model->term_id;
                        $finalMarks->school_id = $model->school_id;
                        $finalMarks->class_id = $model->class_id;
                        $finalMarks->subject_id = $subId;
                        $finalMarks->save(false);
                    }


                }
                $transaction->commit();
                ///  Yii::trace("commit");
                $userId = Yii::$app->user->identity->getId();
                Logs::logEvent("Successfuly generated cumulative mark for: " . $subId . ", sememster" . $model->term_id . " ,by: " . $userId, null, null);

            }

            Logs::logEvent("No students found for this class " ,null, null);

        }catch (\Exception $e){

            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

            \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

            Yii::trace('Error: ' . $error, 'CREATE  COMPOSITION ROLLBACK');
            Logs::logEvent("Error creating Test Composition: ", $error, null, null);
            \Yii::$app->session->setFlash('actionFailed', $error);
            Yii::trace($error);
            return \Yii::$app->runAction('schoolcore/core-staff/error');
        }


    }







    private function emptyGroup($id)
    {
        Yii::$app->db->createCommand("delete from report_group_test where group_id = :group_id ")
            ->bindValue(':group_id', $id)
            ->execute();
        Yii::$app->db->createCommand("delete from class_report_gp_results where report_id = :group_id ")
            ->bindValue(':group_id', $id)
            ->execute();
    }



    private function removeMembers($id, $sel)
    {
        if ($sel) {
            foreach ($sel as $k => $v) {
                $model2 = ReportGroupSubject::find()->where(['group_id' => $id, 'test_id' => $v])
                    ->limit(1)->one();
                $model2->delete();

                Yii::$app->db->createCommand("delete from class_report_gp_results where report_id = :group_id")
                    ->bindValue(':group_id', $id)
                    ->execute();
            }
        }
    }


    public function actionRemoveStudents($id)
    {
        if (!Yii::$app->user->can('rw_student')) {
            throw new ForbiddenHttpException('No permissions to drops students');
        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model2 = new DynamicModel(['reason']);
        $model2->addRule(['reason'], 'required');
        $state = '';
        if (isset($_POST['action'])) {
            $action = $_POST['action'];
            if ($action == 'del_student' && $_POST['stu']) {
                $model2->reason = "No reason";
                $student = CoreStudentGroupStudent::find()->where(['group_id' => $id, 'student_id' => $_POST['stu']])
                    ->limit(1)->one();
                if ($student) {
                    $message = '<div class="alert alert-success"> Student successfully removed from group </div>';
                    $student->delete();
                    $state = ['status' => 'SUCCESS', 'message' => $message];
                }
            }
        }

        $members = CoreStudentGroupSearch::findMembers($id);
        if ($model2->load(Yii::$app->request->post()) && $model2->validate()) {
            $res = $this->deleteStudents($id, $members, $model2->reason);
            $state = $res['state'];
            $members = $res['members'];
        };
        $res = ['members' => $members, 'model' => $model, 'model2' => $model2, 'state' => $state];
        return $request->isAjax ? $this->renderAjax('drop_students', $res) : $this->render('drop_students', $res);

    }


    /**
     * Deletes an existing Report model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $members = ReportGroupSubject::find()->where(['group_id' => $id])->all();
        if ($members) {
            foreach ($members as $k => $v) {
                $v->delete();
            }
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionError()
    {
        $request = Yii::$app->request;
        return ($request->isAjax) ? $this->renderAjax('error') : $this->render('error');
    }

    public function actionRemoveFlash()
    {

        if (\Yii::$app->session->hasFlash('stuAlert')) {
            \Yii::$app->session->removeFlash('stuAlert');
            return 'removed';
        } else {
            return 'not removed';
        }
    }

    public function actionShowComposition($id,$average)
    {
        try {

            $model = new ReportSearch();
//            $searchModel = new ReportSearch();
            $request = Yii::$app->request;
            //composition
            $newRe = Yii::$app->db->createCommand('SELECT distinct(subject_code),report_name,subject_name,subject_id,
      
       test_description,composition, comp_id
   FROM (
     SELECT rp.id, rp.report_name,ct.test_description, sub.subject_name, sub.subject_code, ass.subject_id,comp.composition, comp.id as comp_id  
FROM report rp 
inner join report_subjects_association ass on ass.report_id = rp."id"
inner join report_subject_composition comp on comp.report_subject = ass.id
inner join core_test ct on ct.id = comp.test_id
inner join core_subject sub on sub.id = ass.subject_id  where rp.id = :groupId
   ) s
  GROUP BY subject_code,subject_name,subject_id,report_name,test_description,composition,comp_id')
                ->bindValue(':groupId', $id)
                ->queryAll();
            Yii::trace($newRe);

            $members = $model->findClassSubject($id);
            yii::trace("i wonder");
     Yii::trace($members);

            $report_name = Yii::$app->db->createCommand('SELECT * FROM report where id  = :reportId')
                ->bindValue(':reportId', $id)
                ->queryOne();
            Yii::trace($report_name);

            $subject_name = Yii::$app->db->createCommand('SELECT * FROM core_subject sub inner join report_subjects_association ass on ass.subject_id = sub.id  WHERE ass.report_id =:reportId')
                ->bindValue(':reportId', $id)
                ->queryOne();
            Yii::trace($subject_name);


            return ($request->isAjax) ? $this->renderAjax('_report_composition',
                ['results' => $newRe,'average'=>$average,'model' => $model, 'subjectName' => $subject_name, 'reportName' => $report_name, 'id' => $id, 'reportData'=>$members]) : $this->render('_report_composition', ['results' => $newRe,'average'=>$average, 'model' => $model, 'subjectName' => $subject_name, 'reportName' => $report_name, 'id' => $id,'reportData'=>$members]);


        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }
    }

    public function actionComposition($id, $testId, $subjectId, $average)
    {
        $connection = Yii::$app->db;
        $model = new ReportSubjectComposition();
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {
            try {
                $data = Yii::$app->request->post();

                //save report subject Association




                    $reportSubjectAss = new ReportSubjectAssociation();
                    $reportSubjectAss->report_id = $id;
                    $reportSubjectAss->subject_id = $subjectId;
                    $reportSubjectAss->save(false);


                    //save report subject test composition
                    $reportSubComposition = new ReportSubjectComposition();
                    $reportSubComposition->test_id = $testId;
                    $reportSubComposition->composition = $data['ReportSubjectComposition']['composition'];
                    $reportSubComposition->report_subject = $reportSubjectAss->id;
                    $reportSubComposition->save(false);



                $transaction->commit();
                Logs::logEvent("Added Composition" . $model->id, null, null);


                return $this->runAction('assign', ['id' => $id, 'average' => $average]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'CREATE  COMPOSITION ROLLBACK');
                Logs::logEvent("Error creating Test Composition: ", $error, null, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }
        }

        return ($request->isAjax) ? $this->renderAjax('_addTestComposition', ['model' => $model, 'id' => $id, 'testId' => $testId, 'subjectId' => $subjectId, 'average' => $average]) :
            $this->render('_addTestComposition', ['model' => $model, 'id' => $id, 'testId' => $testId, 'subjectId' => $subjectId, 'average' => $average]
            );
    }

    public function actionCompositionUpdate($itemId,$average,$id)
    {
        $connection = Yii::$app->db;
        $model = new ReportSubjectComposition();
        $transaction = $connection->beginTransaction();
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {
            try {
                $data = Yii::$app->request->post();

                //save report subject Association

                $reportId = ReportSubjectComposition::findOne(['id'=>$itemId]);
                //save report subject test composition

                $reportId->composition = $data['ReportSubjectComposition']['composition'];

                $reportId->save(false);


                $transaction->commit();
                Logs::logEvent("Added Composition" . $model->id, null, null);


                return $this->runAction('assign', ['id' => $id, 'average' => $average]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'CREATE  COMPOSITION ROLLBACK');
                Logs::logEvent("Error creating Test Composition: ", $error, null, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }
        }

        return ($request->isAjax) ? $this->renderAjax('_updateTestComposition', ['model' => $model, 'itemId' => $itemId,'average'=>$average, 'id'=>$id]) :
            $this->render('_updateTestComposition', ['model' => $model, 'itemId' => $itemId,'average'=>$average, 'id'=>$id]
            );
    }




    public function actionStudentReport($id)
    {

        $request = Yii::$app->request;
        $model = new CoreStudent();
//        $model2 = new Report();
        $connection = Yii::$app->db;
        try {

            $codes = $_POST['student_codes'];
            $studentIds = [];
            $output = [];
            $studentInfo = [];
            $grading = [];
            $subject_results_arry = [];
            $output_result = [];
            $subjects = [];
            Yii::trace($codes);
            if ($codes) {
                $p_codes = preg_split("/[\s,]+/", $codes);
                $p_codes = array_unique(array_filter($p_codes));
                $query = (new Query())->select(['id'])->from('core_student')
                    ->where(['in', 'student_code', $p_codes])->all();

                Yii::trace($query);
                Yii::trace($p_codes);
                foreach ($query as $k => $v) {
                    array_push($studentIds, $v['id']);


                    Yii::trace($studentIds);

                    $classId = Yii::$app->db->createCommand('SELECT class_id FROM report WHERE id =:reportId')
                        ->bindValue(':reportId', $id)
                        ->queryOne();
                    Yii::trace($classId['class_id']);


                    $term = Yii::$app->db->createCommand('SELECT ct.term_name, ct.term_ends FROM report rp  inner join core_term ct on ct.id =rp.term_id WHERE rp.id =:reportId')
                        ->bindValue(':reportId', $id)
                        ->queryOne();


                    $staff = Yii::$app->db->createCommand('SELECT sf.first_name, sf.last_name, bk.image_base64 FROM core_staff sf inner join 
    core_school_class cl on cl.class_teacher = sf.id inner join image_bank bk on bk.id = sf.signature WHERE cl.id =:classId')
                        ->bindValue(':classId', $classId['class_id'])
                        ->queryOne();
                    // Yii::trace($staff);


                    $schDetails = Yii::$app->db->createCommand('select distinct(cs2.school_name) ,csc.id as class_id, cs2.school_code ,cs2.box_no,cs2.district,cs2.phone_contact_1,cs2.contact_email_1, cs.first_name , cs.middle_name ,cs.student_code, cs.last_name ,  csc.class_code ,csc.class_description, cs.id as student_id,crgr.report_id
from class_report_gp_results crgr inner join core_student cs 
on cs.id = crgr.student_id 
inner join core_school cs2 on cs2.id=cs.school_id 
inner join core_subject cs3 on cs3.id = crgr.subject_id 
inner join core_school_class csc  on csc.id =cs.class_id 
where crgr.report_id =:reportId  and crgr.student_id in (:studentCodes)')

                        ->bindValue(':studentCodes', implode(',', $studentIds))
                        ->bindValue(':reportId', $id)
                        ->queryAll();

                    Yii::trace($schDetails);

                    return ($request->isAjax) ? $this->renderAjax('_export_student_class_report_pdf', ['schDetails' => $schDetails, 'model' => $model, 'term' => $term, 'staffInfo' => $staff,
                        'grading' => $grading]) :
                        $this->render('_export_student_class_report_pdf', ['schDetails' => $schDetails, 'model' => $model, 'term' => $term, 'staffInfo' => $staff,
                            'grading' => $grading]);
//                Yii::trace($output);
//
//                                return ($request->isAjax) ? $this->renderAjax('export_student_report_pdf', ['output' => $output, 'model' => $model,]) :
//                    $this->render('export_student_report_pdf', ['output' => $output, 'model' => $model,]);


                }

            }
        }catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Yii::$app->session->setFlash('error', $error);

        }


    }


    public function actionStudentReportComposition($id)
    {

        $request = Yii::$app->request;
        $model = new CoreStudent();
//        $model2 = new Report();
        $connection = Yii::$app->db;
        try {

            $codes = $_POST['student_codes'];
            $studentIds = [];
            $output = [];
            $studentInfo = [];
            $grading = [];
            $subject_results_arry = [];
            $output_result = [];
            $subjects = [];
            Yii::trace($codes);
            if ($codes) {
                $p_codes = preg_split("/[\s,]+/", $codes);
                $p_codes = array_unique(array_filter($p_codes));
                $query = (new Query())->select(['id'])->from('core_student')
                    ->where(['in', 'student_code', $p_codes])->all();

                Yii::trace($query);
                Yii::trace($p_codes);
                foreach ($query as $k => $v) {
                    array_push($studentIds, $v['id']);


                    Yii::trace($studentIds);

                    $classId = Yii::$app->db->createCommand('SELECT class_id FROM report WHERE id =:reportId')
                        ->bindValue(':reportId', $id)
                        ->queryOne();
                    Yii::trace($classId['class_id']);


                    $term = Yii::$app->db->createCommand('SELECT ct.term_name, ct.term_ends FROM report rp  inner join core_term ct on ct.id =rp.term_id WHERE rp.id =:reportId')
                        ->bindValue(':reportId', $id)
                        ->queryOne();


                    $staff = Yii::$app->db->createCommand('SELECT sf.first_name, sf.last_name, bk.image_base64 FROM core_staff sf inner join 
    core_school_class cl on cl.class_teacher = sf.id inner join image_bank bk on bk.id = sf.signature WHERE cl.id =:classId')
                        ->bindValue(':classId', $classId['class_id'])
                        ->queryOne();
                    // Yii::trace($staff);


                    //select test results

                    $reportTests = Yii::$app->db->createCommand('select rgt.type, rgt.group_id ,rsc.composition, cs.class_id,mks.student_id , mks.marks_obtained, mks.created_by,rgt.test_id,ct.subject_id, cs2.subject_name 
from report_group_test rgt  inner join core_marks mks on mks.test_id = rgt.test_id 
inner join core_student cs on cs.id = mks.student_id
inner join core_test ct on ct.id= mks.test_id 
inner join core_subject cs2 on cs2.id = ct.subject_id 
inner join report_subject_composition rsc on rsc.test_id =rgt.test_id 

where group_id =:reportId and mks.student_id in (:studentCodes)')

                        ->bindValue(':studentCodes', implode(',', $studentIds))
                        ->bindValue(':reportId', $id)
                        ->queryAll();

                    yii::trace($reportTests);

//
                    foreach ($reportTests as $kv => $vk) {
//            yii::trace($kv);
//            yii::trace($vk);
                        //check if record exists;
                        $testDetails = Yii::$app->db->createCommand('SELECT *  FROM core_test  WHERE id  =:test_id')
                            ->bindValue(':test_id', $vk['test_id'])
                            ->queryOne();
                        // Yii::trace($testDetails);


                        $existingRecord = Yii::$app->db->createCommand('SELECT *  FROM class_report_gp_results rpr WHERE rpr.report_id =:reportId and rpr.subject_id =:subject_id and rpr.student_id=:stdId')
                            ->bindValue(':reportId', $id)
                            ->bindValue(':subject_id', $testDetails['subject_id'])
                            ->bindValue(':stdId', $vk['student_id'])
                            ->queryOne();

                        Yii::trace($existingRecord);
                        if (!$existingRecord) {

                            if ($vk['type'] == 'BOT') {
                                $sql2 = "INSERT INTO class_report_gp_results (
                            student_id, 
                              subject_id, 
                              report_id,
                                     bot
                              )
                                VALUES (
                              :student, 
                              :subject, 
                              :report, 
                              :bot
                                )";
                                $fileQuery = $connection->createCommand($sql2);
                                $fileQuery->bindValue(':student', $vk['student_id']);
                                $fileQuery->bindValue(':subject', $vk['subject_id']);
                                $fileQuery->bindValue(':report', $id);
                                $fileQuery->bindValue(':bot',  round($vk['marks_obtained']*($vk['composition']/100)));
                                //Insert file
                                $fileQuery->execute();
                                Yii::trace('Saved results ');

                            }
//                else{
//
//                }
                            if ($vk['type'] == 'MOT') {
                                $sql2 = "INSERT INTO class_report_gp_results (
                            student_id, 
                              subject_id, 
                              report_id,
                                     mot
                              )
                                VALUES (
                              :student, 
                              :subject, 
                              :report, 
                              :mot
                                )";
                                $fileQuery = $connection->createCommand($sql2);
                                $fileQuery->bindValue(':student', $vk['student_id']);
                                $fileQuery->bindValue(':subject', $vk['subject_id']);
                                $fileQuery->bindValue(':report', $id);
                                $fileQuery->bindValue(':mot',  round($vk['marks_obtained']*($vk['composition']/100)));
                                //Insert file
                                $fileQuery->execute();
                                Yii::trace('Saved results ');

                            }
//                else{
//
//                }


                            if ($vk['type'] == 'EOT') {
                                $sql2 = "INSERT INTO class_report_gp_results (
                            student_id, 
                              subject_id, 
                              report_id,
                                     eot
                              )
                                VALUES (
                              :student, 
                              :subject, 
                              :report, 
                              :eot
                                )";
                                $fileQuery = $connection->createCommand($sql2);
                                $fileQuery->bindValue(':student', $vk['student_id']);
                                $fileQuery->bindValue(':subject', $vk['subject_id']);
                                $fileQuery->bindValue(':report', $id);
                                $fileQuery->bindValue(':eot', ($vk['marks_obtained']*($vk['composition']/100)));
                                //Insert file
                                $fileQuery->execute();
                                Yii::trace('Saved results ');

                            }
//                else{
//
//                }

                        } else {
                            if ($vk['type'] == 'BOT') {
                                $connection->createCommand("update class_report_gp_results set bot = :bot where student_id = :stdId and report_id =:report and subject_id =:subject")
                                    ->bindValue(':bot',  round($vk['marks_obtained']*($vk['composition']/100)))
                                    ->bindValue(':report', $id)
                                    ->bindValue(':subject', $testDetails['subject_id'])
                                    ->bindValue(':stdId', $vk['student_id'])
                                    ->execute();
                            }
                            if ($vk['type'] == 'MOT') {

                                $connection->createCommand("update class_report_gp_results set mot = :mot where student_id = :stdId and report_id =:report and subject_id =:subject")
                                    ->bindValue(':mot',  round($vk['marks_obtained']*($vk['composition']/100)))
                                    ->bindValue(':report', $id)
                                    ->bindValue(':subject', $testDetails['subject_id'])
                                    ->bindValue(':stdId', $vk['student_id'])
                                    ->execute();
                            }
                            if ($vk['type'] == 'EOT') {

                                $connection->createCommand("update class_report_gp_results set eot = :eot where student_id = :stdId and report_id =:report and subject_id =:subject")
                                    ->bindValue(':eot',  round($vk['marks_obtained']*($vk['composition']/100)))
                                    ->bindValue(':report', $id)
                                    ->bindValue(':subject', $testDetails['subject_id'])
                                    ->bindValue(':stdId', $vk['student_id'])
                                    ->execute();
                            }

                        }

                    }




                    $schDetails = Yii::$app->db->createCommand('select distinct(cs2.school_name) ,csc.id as class_id, cs2.school_code ,cs2.box_no,cs2.district,cs2.phone_contact_1,cs2.contact_email_1, cs.first_name , cs.middle_name ,cs.student_code, cs.last_name ,  csc.class_code ,csc.class_description, cs.id as student_id,crgr.report_id
from class_report_gp_results crgr inner join core_student cs 
on cs.id = crgr.student_id 
inner join core_school cs2 on cs2.id=cs.school_id 
inner join core_subject cs3 on cs3.id = crgr.subject_id 
inner join core_school_class csc  on csc.id =cs.class_id 
where crgr.report_id =:reportId  and crgr.student_id in (:studentCodes)')

                        ->bindValue(':studentCodes', implode(',', $studentIds))
                        ->bindValue(':reportId', $id)
                        ->queryAll();

                    Yii::trace($schDetails);





//
//
                    return ($request->isAjax) ? $this->renderAjax('_export_student_class_report_pdf_compositions', ['schDetails' => $schDetails, 'model' => $model, 'term' => $term, 'staffInfo' => $staff,
                        'grading' => $grading]) :
                        $this->render('_export_student_class_report_pdf_compositions', ['schDetails' => $schDetails, 'model' => $model, 'term' => $term, 'staffInfo' => $staff,
                            'grading' => $grading]);
                Yii::trace($output);
//
//                                return ($request->isAjax) ? $this->renderAjax('export_student_report_pdf', ['output' => $output, 'model' => $model,]) :
//                    $this->render('export_student_report_pdf', ['output' => $output, 'model' => $model,]);


                }

            }
        }catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            Yii::$app->session->setFlash('error', $error);

        }


    }



    public function getArrayFiltered($aFilterKey, $aFilterValue, $array)
    {
        $filtered_array = array();
        foreach ($array as $value) {
            if (isset($value->$aFilterKey)) {
                if ($value->$aFilterKey == $aFilterValue) {
                    $filtered_array[] = $value;
                }
            }
        }

        return $filtered_array;
    }

    public function actionTheStudentReport($id)
    {
        $request = Yii::$app->request;
        $model = new Report();

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
//            return $this->redirect(['student-report/student-report', 'id' => $model->id]);

//            $passwd = $data['CoreStudent']['new_pass'];
//            $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
//            $model->setPassword($passwd);
//            if ($model->save(false)) {
//                Logs::logEvent("User password reset(" . $model->id . "): " . $model->student_code, null, null);
//                Yii::$app->session->setFlash('success', 'Thank you for using schoolsuite. Your password has been successfully reset.');
//                return $this->redirect(['/site/index']);
//            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('_add_student_code', ['model' => $model, 'id' => $id]) :
                $this->render('_add_student_code', ['model' => $model, 'id' => $id]);
        }

    }

    public function actionTheStudentReportComposition($id)
    {
        $request = Yii::$app->request;
        $model = new Report();

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
//            return $this->redirect(['student-report/student-report', 'id' => $model->id]);

//            $passwd = $data['CoreStudent']['new_pass'];
//            $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
//            $model->setPassword($passwd);
//            if ($model->save(false)) {
//                Logs::logEvent("User password reset(" . $model->id . "): " . $model->student_code, null, null);
//                Yii::$app->session->setFlash('success', 'Thank you for using schoolsuite. Your password has been successfully reset.');
//                return $this->redirect(['/site/index']);
//            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('_add_student_code_composition', ['model' => $model, 'id' => $id]) :
                $this->render('_add_student_code_composition', ['model' => $model, 'id' => $id]);
        }

    }

    public function actionClassReport($id)
    {

        $request = Yii::$app->request;
        $model =Report::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
//            return $this->redirect(['student-report/student-report', 'id' => $model->id]);

//            $passwd = $data['CoreStudent']['new_pass'];
//            $model->password_expiry_date = date('Y-m-d', strtotime('+1 years'));
//            $model->setPassword($passwd);
//            if ($model->save(false)) {
//                Logs::logEvent("User password reset(" . $model->id . "): " . $model->student_code, null, null);
//                Yii::$app->session->setFlash('success', 'Thank you for using schoolsuite. Your password has been successfully reset.');
//                return $this->redirect(['/site/index']);
//            }
        } else {
            return ($request->isAjax) ? $this->renderAjax('_exclude_student_code', ['model' => $model, 'id' => $id]) :
                $this->render('_exclude_student_code', ['model' => $model, 'id' => $id]);
        }

    }

    public function actionTheClassReport($id)
    {
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model =Report::findOne($id);
        $connection = Yii::$app->db;
//        try {
      //  $reportTermAss = ReportTermAssociation::findOne([''])
          // $term =CoreTerm::findOne(['id'=>]);
            $grading = [];

            $classId = Yii::$app->db->createCommand('SELECT class_id, school_id FROM report WHERE id =:reportId')
                ->bindValue(':reportId', $id)
                ->queryOne();
            Yii::trace($classId);



        $term = Yii::$app->db->createCommand('SELECT ct.term_name, ct.term_ends FROM report_term_association rp  inner join core_term ct on ct.id =rp.term_id WHERE rp.report_id =:reportId')
            ->bindValue(':reportId', $id)
            ->queryOne();

        $staff = Yii::$app->db->createCommand('SELECT sf.first_name, sf.last_name, bk.image_base64 FROM core_staff sf inner join 
    core_school_class cl on cl.class_teacher = sf.id inner join image_bank bk on bk.id = sf.signature WHERE cl.id =:classId')
            ->bindValue(':classId',$classId['class_id'] )
            ->queryOne();
        Yii::trace($staff);

        $reportDetails =Yii::$app->db->createCommand('SELECT report_level, exam_sets  FROM report  WHERE id =:Id')
            ->bindValue(':Id',$id )
            ->queryOne();

        $headTeacher = Yii::$app->db->createCommand('SELECT ss.* FROM core_staff ss WHERE ss.school_id =:schoolId AND ss.user_level =:userType')
            ->bindValue(':schoolId', $classId['school_id'])
            ->bindValue(':userType', 'sch_headteacher')
            ->queryOne();
            //select test results

        //get results to display


//Yii::trace($finalResults);
//
        $schDetails = Yii::$app->db->createCommand('select distinct(cs2.school_name) ,csc.id as class_id, cs2.school_code ,
                cs2.box_no,cs2.district,cs2.phone_contact_1,cs2.contact_email_1, cs.first_name , cs.middle_name ,cs.student_code, cs.last_name ,
                csc.class_code ,csc.class_description, cs.id as student_id,crgr.report_id
from cumulative_marks_for_subject_per_semester crgr inner join core_student cs
on cs.id = crgr.student_id
inner join core_school cs2 on cs2.id=cs.school_id
inner join core_subject cs3 on cs3.id = crgr.subject_id
inner join core_school_class csc  on csc.id =cs.class_id
where crgr.report_id =:reportId')
            ->bindValue(':reportId', $id)
            ->queryAll();

        //get updated results and save average perfomance perclass



            return ($request->isAjax) ? $this->renderAjax('export_student_report_pdf', ['schDetails'=>$schDetails, 'model' => $model, 'term'=>$term, 'staff'=>$staff, 'hm'=>$headTeacher,
                       'grading' => $grading]) :
                $this->render('export_student_report_pdf',  ['schDetails'=>$schDetails, 'model' => $model, 'term'=>$term, 'staff'=>$staff,'hm'=>$headTeacher,
                    'grading' => $grading]);


    }


    public function actionTheClassReportComposition($id)
    {
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new Report();
        $connection = Yii::$app->db;

        $grading = [];


        $classId = Yii::$app->db->createCommand('SELECT class_id FROM report WHERE id =:reportId')
            ->bindValue(':reportId', $id)
            ->queryOne();
        Yii::trace($classId);



        $term = Yii::$app->db->createCommand('SELECT ct.term_name, ct.term_ends FROM report rp  inner join core_term ct on ct.id =rp.term_id WHERE rp.id =:reportId')
            ->bindValue(':reportId', $id)
            ->queryOne();

        $staff = Yii::$app->db->createCommand('SELECT sf.first_name, sf.last_name, bk.image_base64 FROM core_staff sf inner join 
    core_school_class cl on cl.class_teacher = sf.id inner join image_bank bk on bk.id = sf.signature WHERE cl.id =:classId')
            ->bindValue(':classId',$classId['class_id'] )
            ->queryOne();
        // Yii::trace($staff);



        //select test results

        $reportTests = Yii::$app->db->createCommand('select rgt.type, rgt.group_id , cs.class_id,mks.student_id ,rsc.composition,  mks.marks_obtained, mks.created_by,rgt.test_id,ct.subject_id, cs2.subject_name 
from report_group_test rgt  inner join core_marks mks on mks.test_id = rgt.test_id 
inner join core_student cs on cs.id = mks.student_id
inner join core_test ct on ct.id= mks.test_id 
inner join report_subject_composition rsc on rsc.test_id =rgt.test_id 

inner join core_subject cs2 on cs2.id = ct.subject_id 
where group_id =:reportId')
            ->bindValue(':reportId', $id)
            ->queryAll();

        yii::trace($reportTests);

        //compare types
        foreach ($reportTests as $kv=>$vk){
//            yii::trace($kv);
//            yii::trace($vk);
            //check if record exists;
            $testDetails = Yii::$app->db->createCommand('SELECT *  FROM core_test  WHERE id  =:test_id')
                ->bindValue(':test_id', $vk['test_id'])
                ->queryOne();
            // Yii::trace($testDetails);


            $existingRecord = Yii::$app->db->createCommand('SELECT *  FROM class_report_gp_results rpr WHERE rpr.report_id =:reportId and rpr.subject_id =:subject_id and rpr.student_id=:stdId')
                ->bindValue(':reportId', $id)
                ->bindValue(':subject_id', $testDetails['subject_id'])
                ->bindValue(':stdId', $vk['student_id'])
                ->queryOne();

            Yii::trace($existingRecord);
            if(!$existingRecord){

                if($vk['type'] =='BOT'){
                    $sql2 = "INSERT INTO class_report_gp_results (
                            student_id, 
                              subject_id, 
                              report_id,
                                     bot
                              )
                                VALUES (
                              :student, 
                              :subject, 
                              :report, 
                              :bot
                                )";
                    $fileQuery = $connection->createCommand($sql2);
                    $fileQuery->bindValue(':student', $vk['student_id']);
                    $fileQuery->bindValue(':subject', $vk['subject_id']);
                    $fileQuery->bindValue(':report', $id);
                    $fileQuery->bindValue(':bot', round($vk['marks_obtained']*($vk['composition']/100)));
                    //Insert file
                    $fileQuery->execute();
                    Yii::trace('Saved results ');

                }
//                else{
//
//                }
                if($vk['type'] =='MOT'){
                    $sql2 = "INSERT INTO class_report_gp_results (
                            student_id, 
                              subject_id, 
                              report_id,
                                     mot
                              )
                                VALUES (
                              :student, 
                              :subject, 
                              :report, 
                              :mot
                                )";
                    $fileQuery = $connection->createCommand($sql2);
                    $fileQuery->bindValue(':student', $vk['student_id']);
                    $fileQuery->bindValue(':subject', $vk['subject_id']);
                    $fileQuery->bindValue(':report', $id);
                    $fileQuery->bindValue(':mot',round($vk['marks_obtained']*($vk['composition']/100)));
                    //Insert file
                    $fileQuery->execute();
                    Yii::trace('Saved results ');

                }
//                else{
//
//                }


                if($vk['type'] =='EOT'){
                    $sql2 = "INSERT INTO class_report_gp_results (
                            student_id, 
                              subject_id, 
                              report_id,
                                     eot
                              )
                                VALUES (
                              :student, 
                              :subject, 
                              :report, 
                              :eot
                                )";
                    $fileQuery = $connection->createCommand($sql2);
                    $fileQuery->bindValue(':student', $vk['student_id']);
                    $fileQuery->bindValue(':subject', $vk['subject_id']);
                    $fileQuery->bindValue(':report', $id);
                    $fileQuery->bindValue(':eot', round($vk['marks_obtained']*($vk['composition']/100)));
                    //Insert file
                    $fileQuery->execute();
                    Yii::trace('Saved results ');

                }
//                else{
//
//                }

            }else{
                if($vk['type'] =='BOT'){
                    $connection->createCommand("update class_report_gp_results set bot = :bot where student_id = :stdId and report_id =:report and subject_id =:subject")
                        ->bindValue(':bot', round($vk['marks_obtained']*($vk['composition']/100)))
                        ->bindValue(':report', $id)
                        ->bindValue(':subject', $testDetails['subject_id'])
                        ->bindValue(':stdId', $vk['student_id'])
                        ->execute();
                }
                if($vk['type'] =='MOT'){

                    $connection->createCommand("update class_report_gp_results set mot = :mot where student_id = :stdId and report_id =:report and subject_id =:subject")
                        ->bindValue(':mot', round($vk['marks_obtained']*($vk['composition']/100)))
                        ->bindValue(':report', $id)
                        ->bindValue(':subject', $testDetails['subject_id'])
                        ->bindValue(':stdId', $vk['student_id'])
                        ->execute();
                }
                if($vk['type'] =='EOT'){

                    $connection->createCommand("update class_report_gp_results set eot = :eot where student_id = :stdId and report_id =:report and subject_id =:subject")
                        ->bindValue(':eot', round($vk['marks_obtained']*($vk['composition']/100)))
                        ->bindValue(':report', $id)
                        ->bindValue(':subject', $testDetails['subject_id'])
                        ->bindValue(':stdId', $vk['student_id'])
                        ->execute();
                }

            }

        }


        //get results to display


        $schDetails = Yii::$app->db->createCommand('select distinct(cs2.school_name) ,csc.id as class_id, cs2.school_code ,cs2.box_no,cs2.district,cs2.phone_contact_1,cs2.contact_email_1, cs.first_name , cs.middle_name ,cs.student_code, cs.last_name ,  csc.class_code ,csc.class_description, cs.id as student_id,crgr.report_id
from class_report_gp_results crgr inner join core_student cs 
on cs.id = crgr.student_id 
inner join core_school cs2 on cs2.id=cs.school_id 
inner join core_subject cs3 on cs3.id = crgr.subject_id 
inner join core_school_class csc  on csc.id =cs.class_id 
where crgr.report_id =:reportId')
            ->bindValue(':reportId', $id)
            ->queryAll();



        return ($request->isAjax) ? $this->renderAjax('export_student_report_pdf_composition', ['schDetails'=>$schDetails, 'model' => $model, 'term'=>$term, 'staffInfo'=>$staff,
            'grading' => $grading]) :
            $this->render('export_student_report_pdf_composition',  ['schDetails'=>$schDetails, 'model' => $model, 'term'=>$term, 'staffInfo'=>$staff,
                'grading' => $grading]);



    }





        public function actionClassReportCardGenerator($classes = null, $student_codes = null)
        {

            $model = new Rollcall();
            $request = Yii::$app->request;
            $studentIds = [];
    //        try {
            if ($model->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();
                Yii::trace($data);
                if (isset($data['Rollcall']['classes'])) {
                    Yii::trace("..... classses.........");
                    //get student classes
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->andWhere(['in', 'class_id', array_values($data['Rollcall']['classes'])])
                        ->all();

                    if (isset($_POST['exclude_student_codes'])) {
                        Yii::trace($_POST['exclude_student_codes']);
                        //get student codes
                        $codes = $_POST['exclude_student_codes'];
                        $p_codes = preg_split("/[\s,]+/", $codes);
                        $p_codes = array_unique(array_filter($p_codes));
                        $query = (new Query())->select(['id'])->from('core_student')
                            ->andWhere(['in', 'class_id', array_values($data['Rollcall']['classes'])])
                            ->andWhere(['not in', 'student_code', $p_codes])->all();
                    }

                    foreach ($query as $v) {
                        array_push($studentIds, $v['id']);
                    }
                } else if (isset($_POST['student_codes'])) {
                    Yii::trace("..... codes.........");
                    Yii::trace($_POST['student_codes']);
    //                    get student codes
                    $codes = $_POST['student_codes'];
                    $p_codes = preg_split("/[\s,]+/", $codes);
                    $p_codes = array_unique(array_filter($p_codes));
                    $query = (new Query())->select(['id'])->from('core_student')
                        ->where(['in', 'student_code', $p_codes])->all();
                    foreach ($query as $k => $v) {
                        array_push($studentIds, $v['id']);
                    }
                }


                // get signatures attached
                /*$db = Yii::$app->db;
                $signature = $db->createCommand('select cs.* from core_signatures cs left join circular_signatures  css on (cs.id=css.signature_id) where css.circular_id=:circular_id',
                    [':circular_id' => $circular_id])->queryOne();
                Yii::trace($signature);*/
                //now generator the PDF's
                $student_query = new Query();
                $student_query->select(['c_s.first_name', 'c_s.last_name','cl.class_description', 'c_s.student_code', 'cs.school_name', 'cs.box_no', 'cs.contact_email_1', 'cs.contact_email_2', 'phone_contact_1', 'phone_contact_2', 'cs.vision', 'cs.school_reference_no'])
                    ->from('core_student c_s')
                    ->innerJoin('core_school cs', 'c_s.school_id=cs.id')
                    ->innerJoin('core_school_class cl', 'cl.school_id=cs.id')
                    ->where(['in', 'c_s.id', $studentIds]);
                $command = $student_query->createCommand();
                $student_data = $command->queryAll();


                $circular_data = [];

                foreach ($student_data as $v) {
                    ini_set('max_execution_time', '300');
                    ini_set("pcre.backtrack_limit", "10000000");
                    $circular_data[] = array(
                        'school_name' => $v['school_name'],
                        'box_no' => $v['box_no'],
                        'phone_contact_1' => $v['phone_contact_1'],
                        'phone_contact_2' => $v['phone_contact_2'],

                        'contact_email_1' => $v['contact_email_1'],

                        'first_name' => $v['first_name'],
                        'last_name' => $v['last_name'],
                        'student_code' => $v['student_code'],
                        'class_name' => $v['class_description'],


                        'vision' => $v['vision']
                    );
                }
    //
    //                return $this->render('export_circular_pdf', ['circular_data' => $circular_data
    //                ]);
    /*
                Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
                $pdf = new Pdf([
                    'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                    'destination' => Pdf::DEST_BROWSER,
                    'content' => $this->renderPartial('export_qr_pdf', ['circular_data' => $circular_data
                    ]),
                    'options' => [
                        // any mpdf options you wish to set
                    ],
                    'methods' => [
                        'SetTitle' => 'Print student QR codes',
                        'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
    //                    'SetHeader' => ['Reports||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                        'SetFooter' => [],
                        'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                    ]
                ]);
                return $pdf->render();*/

                $pdf = new Pdf([
                    'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
                    'destination' => Pdf::DEST_BROWSER,
                    'content' => $this->renderPartial('export_qr_pdf', ['circular_data' => $circular_data
                    ]),
    //                'cssFile' => '@css',
                    'cssInline' => 'body {
        background: #eee
    }

    .ratings i {
        font-size: 16px;
        color: red
    }

    .strike-text {
        color: red;
        text-decoration: line-through
    }

    .product-image {
        width: 100%
    }

    .dot {
        height: 7px;
        width: 7px;
        margin-left: 6px;
        margin-right: 6px;
        margin-top: 3px;
        background-color: blue;
        border-radius: 50%;
        display: inline-block
    }

    .spec-1 {
        color: #938787;
        font-size: 15px
    }

    h5 {
        font-weight: 400
    }

    .para {
        font-size: 16px
    }',
                    'options' => [
                        // any mpdf options you wish to set
                    ],
                    'methods' => [
                        'SetTitle' => 'Print students',
                        'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                        'SetHeader' => ['Students||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                        'SetFooter' => ['|Page {PAGENO}|'],
                        'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
                    ]
                ]);
                return $pdf->render();

            }
            /*   } catch (\Exception $e) {
                   $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                   Yii::trace('Error: ' . $error, 'Generate circular callback');
               }*/
            $res = ['student_codes' => $student_codes,  'classes' => $classes, 'model' => $model];
            return ($request->isAjax) ? $this->renderAjax('circular_selector', $res) : $this->render('circular_selector', $res);

        }



    public
    function actionExportPdf($model)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $data = $model::getExportQuery();
        $subSessionIndex = isset($data['sessionIndex']) ? $data['sessionIndex'] : null;
        $query = $data['data'];
        $query = $query->limit(200)->all(Yii::$app->db);
        $type = 'Pdf';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('exportPdfExcel', ['query' => $query, 'type' => $type, 'subSessionIndex' => $subSessionIndex
            ]),
            'options' => [
                // any mpdf options you wish to set
            ],
            'methods' => [
                'SetTitle' => 'Print student report',
                'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
                'SetHeader' => ['Reports||Generated On: ' . date("r") . '   by ' . Yii::$app->user->identity->fullname],
                'SetFooter' => ['|Page {PAGENO}|'],
                'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
            ]
        ]);
        return $pdf->render();
    }

    public
    function actionAddAverage($id, $average)
    {

        $connection = Yii::$app->db;

        //$transaction = $connection->beginTransaction();

        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $searchModel = new ReportSearch();
        //$members = ReportGroupSubject::find()->where(['group_id' => $id])->all();
        $members = $searchModel->findClassSubject($id);
        Yii::trace($members);


        if ($members) {
            try {
                $transaction = $connection->beginTransaction();
                foreach ($members as $k => $v) {

                   // $existingRecord = ReportSubjectAssociation::findOne(['report_id=>'])

                    //save report subject Association
                    //check exisiting
                    //$exisitingRecord =  ReportSubjectAssociation::findOne(['report_id'=>$id, 'subject_id' => $v['subject_id']]);

                    $exisitingRecord = Yii::$app->db->createCommand('SELECT * FROM report_subjects_association WHERE report_id =:reportId AND subject_id= :subjectId')
                        ->bindValue(':reportId', $id)
                        ->bindValue(':subjectId', $v['subject_id'])
                        ->queryone();
                    Yii::trace($exisitingRecord);

                    if(!$exisitingRecord) {
                        $reportSubjectAss = new ReportSubjectAssociation();

                        $reportSubjectAss->report_id = $id;
                        $reportSubjectAss->subject_id = $v['subject_id'];
                        $reportSubjectAss->save(false);

                        $reportSubComposition = new ReportSubjectComposition();
                        $reportSubComposition->test_id = $v['id'];
                        $reportSubComposition->report_subject = $reportSubjectAss->id;
                        $reportSubComposition->save(false);
                    }


                        //save report subject test composition
                        //check if subject already exists

                        if($exisitingRecord){
                            $rptSubId =$exisitingRecord['id'];
                            Yii::trace($rptSubId);
                            $exisitingTestRecord = Yii::$app->db->createCommand('SELECT * FROM report_subject_composition WHERE report_subject =:reportId AND test_id= :testId')
                                ->bindValue(':reportId', $rptSubId)
                                ->bindValue(':testId',  $v['id'])
                                ->queryone();
                            Yii::trace($exisitingTestRecord);
                            if(!$exisitingTestRecord){
                                $reportSubComposition = new ReportSubjectComposition();
                                $reportSubComposition->test_id = $v['id'];
                                $reportSubComposition->report_subject = $rptSubId;
                                $reportSubComposition->save(false);
                            }

                        }








                }
                $transaction->commit();

                Logs::logEvent("Added Average Compo" . $model->id, null, null);


                return $this->runAction('show-composition', ['id' => $id, 'average' => $average,'data'=>$members]);

            } catch (\Exception $e) {
                $transaction->rollBack();
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();

                \Yii::$app->session->setFlash('Failed', "Failed.' '.$error <br>");

                Yii::trace('Error: ' . $error, 'CREATE  COMPOSITION ROLLBACK');
                Logs::logEvent("Error creating Test Composition: ", $error, null, null);
                \Yii::$app->session->setFlash('actionFailed', $error);
                Yii::trace($error);
                return \Yii::$app->runAction('schoolcore/core-staff/error');
            }
        }
        return ($request->isAjax) ? $this->renderAjax('assign', ['model' => $model, 'id' => $id, 'average' => $average]) : $this->render('assign', ['model' => $model, 'id' => $id, 'average' => $average]);
    }

    public function actionAssignTerms($id, $action)
    {
        $post = Yii::$app->request->post();
        $perms = $post['selected'];
        $error = [];

        $model =Report::findOne([$id]);

        if ($action == 'assign') {
            foreach ($perms as $k => $v) {
                $term = CoreTerm::findOne($v);
                Yii::trace($v);
                try {
                    $item = new ReportTermAssociation();
                    $item->report_id = $id;
                    $item->class_id = $model->class_id;
                    $item->term_id = $v;
                    $item->school_id = $model->school_id;
                    $item->created_by = Yii::$app->user->identity->getId();
                    $item->description = $model->report_name. ' - '. $term->term_name;
                    $item->save(false);
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }
            }
        } else {
            foreach ($perms as $k => $v) {
                try {
                    $item = ReportTermAssociation::find()->where(['report_id' => $id, 'term_id' => $v])->one();
                    $item->delete();
                } catch (\Exception $exc) {
                    $error[] = $exc->getMessage();
                }

            }
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [$this->actionTermSearch($id, 'available', ''),
            $this->actionTermSearch($id, 'assigned', ''),
            $error];
    }


    public function actionTermSearch($id, $target, $term = '')
    {
        $model= Report::findOne([$id]);

        $permissions = CoreTerm::find()->where(['school_id' => $model->school_id])->all();
       // $model = CoreTerm::find()->where(['name' => $name])->limit(1)->one();
        $available = [];
        $assigned = [];

        foreach ($model->assignedTerms as $perms) {

            $assigned[$perms->term_id] = $perms->child0->term_name;

        }

        foreach ($permissions as $perm) {

            if (!isset($assigned[$perm->id])) {
                $available[$perm->id] = $perm->term_name;

            }

        }

        $result = [];
        $var = ${$target};
        if (!empty($term)) {
            foreach ($var as $i => $descr) {
                if (stripos($descr, $term) !== false) {
                    $result[$i] = $descr;
                }
            }
        } else {
            $result = $var;
        }

        return Html::renderSelectOptions('', $result);
    }


    public function actionTerms($id)
    {
//        rw_role
        if (Yii::$app->user->can('own_sch') && Yii::$app->user->can('rw_terms_reports')) {
            $request = Yii::$app->request;
            $model = Report::find()->where(['id' => $id])->limit(1)->one();


            $available = array();
            $assigned = array();
            $permissions = CoreTerm::find()->where(['school_id' => $model->school_id])->all();

            foreach ($model->assignedTerms as $perms) {

                $assigned[$perms->term_id] = $perms->child0->term_name;

            }

            foreach ($permissions as $perm) {

                if (!isset($assigned[$perm->id])) {
                    $available[$perm->id] = $perm->term_name;

                }

            }
            return ($request->isAjax) ? $this->renderAjax('terms', ['model' => $model,'id'=>$id,
                'available' => $available, 'assigned' => $assigned]) :
                $this->render('terms', ['model' => $model, 'available' => $available,'id'=>$id,
                    'assigned' => $assigned]);

        } else {
            throw new ForbiddenHttpException('No permissions to update or create roles.');
        }
    }

}

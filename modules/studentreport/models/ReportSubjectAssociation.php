<?php

namespace app\modules\studentreport\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id

 * @property integer $report_id
 * @property integer $subject_id

 */
class ReportSubjectAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_subjects_association';
    }



    /**
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'report_id', 'subject_id'], 'required'],
            [['id', 'subject_id','subject_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_id' => 'Class ID',
            'grade' => 'grade',
            'min_mark' => 'Min',
            'max_mark' => 'Max',
        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }
    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }


    public function searchStudentClassGrades($class_id)
    {
        $query = (new Query())
            ->select(['cm.id','cm.min_mark', 'cm.max_mark','cm.grades'])
            ->from('core_grades cm')
            ->innerJoin('core_school sch','sch.id =cm.school_id' )
            ->innerJoin('core_school_class csc','csc.id =cm.class_id' )
            ->where(['cm.class_id'=>$class_id]);

        $query->orderBy('cm.id desc');

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [

            'min_mark',
            'max_mark',
            'grades',

            ////
        ];
        $searchForm= 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider,'searchForm'=>$searchForm];
    }
}

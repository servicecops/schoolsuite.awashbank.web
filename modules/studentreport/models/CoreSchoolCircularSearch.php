<?php

namespace app\modules\studentreport\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\studentreport\models\CoreSchoolCircular;
use yii\db\Query;

/**
 * CoreSchoolCircularSearch represents the model behind the search form of `app\modules\studentreport\models\CoreSchoolCircular`.
 */
class CoreSchoolCircularSearch extends CoreSchoolCircular
{
    public $classes, $circular_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated', 'circular_description', 'date_modified', 'attachment_file', 'subject', 'the_body', 'ccc_description'], 'safe'],
            [['created_by', 'id', 'school_id', 'term_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = new Query();
        $query->select(['sc.*', 'ct.term_name', 'csc.school_name'])
            ->from('core_school_circular sc')
            ->innerJoin('core_term ct', 'sc.term_id= ct.id')
            ->innerJoin('core_school csc', 'sc.school_id= csc.id')
            ->Where(['sc.circular_type' => 'Generic']);

        $query->orderBy('sc.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
        } else if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'id' => $this->id,
            'school_id' => $this->school_id,
            'term_id' => $this->term_id,
            'date_modified' => $this->date_modified,
        ]);

       // $query->andFilterWhere(['ilike', 'circular_description', $this->circular_description]);


        return $dataProvider;
    }

    public function searchCustomised($params)
    {
        $query = new Query();
        $query->select(['sc.*', 'ct.term_name', 'csc.school_name'])
            ->from('core_school_circular sc')
            ->innerJoin('core_term ct', 'sc.term_id= ct.id')
            ->innerJoin('core_school csc', 'sc.school_id= csc.id')
            ->Where(['sc.circular_type' => 'Customised']);
        $query->orderBy('sc.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
        } else if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'id' => $this->id,
            'school_id' => $this->school_id,
            'term_id' => $this->term_id,
            'date_modified' => $this->date_modified,
        ]);

        // $query->andFilterWhere(['ilike', 'circular_description', $this->circular_description]);


        return $dataProvider;
    }
    public function searchAllCustomised($params)
    {
        $query = new Query();
        $query->select(['sc.*', 'ct.term_name', 'csc.school_name', 'msg.payment_code','msg.student_name','msg.student_class'])
            ->from('core_school_circular sc')
            ->innerJoin('core_term ct', 'sc.term_id= ct.id')
            ->innerJoin('core_school csc', 'sc.school_id= csc.id')
            ->innerJoin('circular_customised_std_msg msg', 'sc.id=msg.circular_id')
            ->Where(['sc.circular_type' => 'Customised'])
            ->andWhere(['sc.id' => $params]);
        $query->orderBy('sc.id desc');


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
        } else if (\app\components\ToWords::isSchoolUser()) {
            $query->andWhere(['sc.school_id' => Yii::$app->user->identity->school_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query, 'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'id' => $this->id,
            'school_id' => $this->school_id,
            'term_id' => $this->term_id,
            'date_modified' => $this->date_modified,
        ]);

        // $query->andFilterWhere(['ilike', 'circular_description', $this->circular_description]);


        return $dataProvider;
    }
}

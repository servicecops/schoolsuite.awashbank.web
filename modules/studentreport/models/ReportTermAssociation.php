<?php

namespace app\modules\studentreport\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreTerm;
use Yii;

/**
 * This is the model class for table "auth_item_child".
 *
 * @property string $parent
 * @property string $child
 *
 * @property AuthItem $parent0
 * @property AuthItem $child0
 */
class ReportTermAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_term_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'term_id'], 'required'],
            [['report_id', 'term_id', 'school_id','class_id'], 'integer', 'max' => 64],
            [['created_by'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getParent0()
//    {
//        return $this->hasOne(AuthItem::className(), ['name' => 'parent']);
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChild0()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }

    public function getSchool()
    {
        return CoreSchool::find()->where(['id' => $this->school_id])->one();
    }

}

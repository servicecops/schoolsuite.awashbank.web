<?php

namespace app\modules\studentreport\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreSchoolClass;
use app\modules\schoolcore\models\CoreTerm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "institution_fee_class_association".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $fee_id
 * @property integer $class_id
 * @property boolean $applied
 * @property integer $created_by
 * @property boolean $average
 *
 * @property Report[] $assignedTerms
 *

 */
class Report extends \yii\db\ActiveRecord
{
    public $classes, $composition, $test_id, $subject_id, $studentNumber, $student_codes;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'report_name','report_description','class_id',], 'required'],
            [['school_id', 'class_id', 'term_id'], 'integer'],
            [['date_created','exam_sets', 'date_modified', 'classes', 'student_codes','term_type'], 'safe'],
            [['active', 'average'], 'boolean'],
            [['report_name', 'report_description','report_level','exam_sets'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'class_id' => 'Class ID',
            'term_id' => 'Term ID',
            'report_description' => 'Descrition',

        ];
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getClassName()
    {
        return $this->hasOne(CoreSchoolClass::className(), ['id' => 'class_id']);
    }

    public function getTermName()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }


    public function searchStudentClassGrades($class_id)
    {
        $query = (new Query())
            ->select(['cm.id', 'cm.min_mark', 'cm.max_mark', 'cm.grades'])
            ->from('core_grades cm')
            ->innerJoin('core_school sch', 'sch.id =cm.school_id')
            ->innerJoin('core_school_class csc', 'csc.id =cm.class_id')
            ->where(['cm.class_id' => $class_id]);

        $query->orderBy('cm.id desc');

        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [

            'min_mark',
            'max_mark',
            'grades',

            ////
        ];
        $searchForm = 'subject_name';
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm];
    }

    public function getSchool()
    {
        return CoreSchool::find()->where(['id' => $this->school_id])->one();
    }


    public function getGrades($average, $subjectId)
    {
        $classId = Yii::$app->db->createCommand('SELECT class_id FROM core_subject WHERE id=:subjectId')
            ->bindValue(':subjectId', $subjectId)
            ->queryOne();
        Yii::trace($classId);

        $grade = Yii::$app->db->createCommand('SELECT grades FROM core_grades WHERE (min_mark <= :average and max_mark >=:average )and class_id =:class_id')
            ->bindValue(':average', $average)
            ->bindValue(':class_id', $classId['class_id'])
            ->queryOne();
        Yii::trace($grade);
        $grade = $grade['grades'];
        return $grade;
    }

    public function getComments($average, $subjectId)
    {
        $classId = Yii::$app->db->createCommand('SELECT class_id FROM core_subject WHERE id=:subjectId')
            ->bindValue(':subjectId', $subjectId)
            ->queryOne();
        Yii::trace($classId);

        $comments = Yii::$app->db->createCommand('SELECT comment FROM core_grades WHERE (min_mark <= :average and max_mark >=:average )and class_id =:class_id')
            ->bindValue(':average', $average)
            ->bindValue(':class_id', $classId['class_id'])
            ->queryOne();
        Yii::trace($comments);
        return $comments['comment'];
    }


    public function getReportTerms()
    {
        return $this->hasMany(ReportTermAssociation::className(), ['report_id' => 'id']);
    }

    public function getAuthItemChildren()
    {
        return $this->hasMany(ReportTermAssociation::className(), ['report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignedTerms()
    {
        return $this->hasMany(ReportTermAssociation::className(), ['report_id' => 'id']);
    }

    public function getChild0()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }


}

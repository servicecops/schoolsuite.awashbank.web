<?php

namespace app\modules\studentreport\models;

use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use app\modules\schoolcore\models\CoreSubject;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class ReportSearch extends Report
{
    public $gender, $student_class, $searchTerm, $class_subject;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id'], 'integer'],
            [['school_id', 'report_name', 'report_description', 'date_created', 'date_modified', 'subject_name','class_id', 'subject_description'], 'safe'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

  public function search($params)
  {
      $this->load($params);
      $query = new Query();
      $query->select(['gr.id', 'report_name', 'report_description', 'gr.date_created', 'sch.school_name', 'gr.active','cl.class_code' ])
          ->from('report gr')
          ->innerJoin('core_school sch', 'sch.id=gr.school_id')
          ->innerJoin('core_school_class cl', 'cl.id=gr.class_id')
          ->andFilterWhere(['gr.school_id'=>$this->school_id]);
      if(\app\components\ToWords::isSchoolUser()){
          $query->where(['gr.school_id'=>Yii::$app->user->identity->school_id]);
      }


      if ( Yii::$app->user->can('is_teacher')) {
            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);

            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
          Yii::trace($data);

          foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
          Yii::trace($the_classId);

          if ($this->class_id && $this->class_id != 0) {
              $query->andWhere(['gr.class_id' => intVal($this->class_id)]);
          } else {
              // $query->andWhere(['tca.class_id' => 18]);
              $query->andwhere(['in', 'gr.class_id', $the_classId]);
          }
        }



      $pages = new Pagination(['totalCount' => $query->count()]);
      $sort = new Sort([
          'attributes' => [
              'date_created', 'report_name', 'report_description',  'school_name' ]
      ]);
      ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('date_created DESC');
      unset($_SESSION['findData']);
      $_SESSION['findData'] = $query;
      $query->offset($pages->offset)->limit($pages->limit);
      return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
  }


    public function searchSemReports($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['ass.id', 'report_name', 'ass.description', 'gr.date_created', 'sch.school_name', 'gr.active','cl.class_code' ])
            ->from('report gr')
            ->innerJoin('report_term_association ass', 'ass.report_id=gr.id')
            ->innerJoin('core_school sch', 'sch.id=ass.school_id')
            ->innerJoin('core_school_class cl', 'cl.id=ass.class_id')
            ->andFilterWhere(['gr.school_id'=>$this->school_id]);
        if(\app\components\ToWords::isSchoolUser()){
            $query->where(['gr.school_id'=>Yii::$app->user->identity->school_id]);
        }


        if ( Yii::$app->user->can('is_teacher')) {
            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);

            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            Yii::trace($data);

            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['gr.class_id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'gr.class_id', $the_classId]);
            }
        }



        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'date_created', 'report_name', 'description',  'school_name' ]
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];
    }

    public function findSubjects($grp, $params)
    {
        Yii::trace("tuli wawelu");
        $query1 = ReportGroupSubject::find()->select(['subject_id'])->where(['group_id'=>$grp['grId']]);
        $query = new Query();
        $query->select(['st.id as test_id', 'st.test_description','st.subject_id',  'st.start_date','sb.subject_code'])
            ->from('core_test st')
            ->innerJoin('core_subject sb', 'sb.id=st.subject_id')
            ->andFilterWhere([
                'st.subject_id'=>$params['class_subject'],
                'st.school_id'=>$grp['sch'],
                'st.class_id'=>$grp['cls']]);
//            ->andWhere(['in', 'st.id', $query1]);


        if(!(empty($params['searchTerm']))){
            $searchTerm = $params['searchTerm'];
            if (is_numeric($searchTerm)) {
                $query->andWhere(['student_code' => $searchTerm]);
            }
            else
            {
                $search_words = explode(' ', $searchTerm);
                $search_words = array_filter($search_words);
                $words = array();
                if(count($search_words) > 1) {
                    foreach($search_words as $word) {
                        $words[] = $word;
                    }
                    if(isset($words[0])){
                        $name = str_replace(' ', '', $words[0]);
                        $query->andFilterWhere(['or',
                            ['ilike', 'subject_name', $name],
                            ['ilike', 'subjct_code', $name],
                        ]);
                    }
                    if(isset($words[1])){
                        $name = str_replace(' ', '', $words[1]);
                        $query->andFilterWhere(['or',
                            ['ilike', 'subject_name', $name],
                            ['ilike', 'subjct_code', $name],
                        ]);
                    }
                    if(isset($words[2])){
                        $name = str_replace(' ', '', $words[3]);
                        $query->andFilterWhere(['or',
                            ['ilike', 'subject_name', $name],
                            ['ilike', 'subjct_code', $name],
                        ]);
                    }

                }
                else{
                    $name = str_replace(' ', '', $searchTerm);
                    $query->andFilterWhere(['or',
                        ['ilike', 'subject_name', $name],
                        ['ilike', 'subjct_code', $name],

                    ]);
                }
            }
        }

        $query->orderBy( 'st.start_date');
        $query->limit(500);
        return $query->all();


    }

    public static function findClassSubject($id){
        $query = (new Query())->select(['si.id', 'si.test_description', 'sb.subject_code','sb.id as subject_id', 'si.start_date','sgr.semester_type_id','sgr.assessment_composition'])
            ->from('subject_semester_assesment_association sgr')
            ->innerJoin('core_test si', 'si.id=sgr.assessment_id')
            ->innerJoin('core_subject sb', 'sb.id=si.subject_id')
            ->andWhere(['sgr.report_id'=>$id])
            ->orderBy('sb.subject_code', 'si.test_description');
        return $query->all();
    }

    public function getTestComposition($params)
    {
        $this->load($params);
        $query = new Query();
        $query->select(['gr.id', 'report_name', ])
            ->from('report rp')
            ->innerJoin('report_subjects_association ass', 'rp.id=ass.report_id')
            ->innerJoin('report_subject_composition comp', 'ass.id=comp.report_subject')
            ->andFilterWhere(['rp.id'=>$params]);

        $pages = new Pagination(['totalCount' => $query->count()]);
        $sort = new Sort([
            'attributes' => [
                'report_name', 'report_description',  'school_name' ]
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('date_created DESC');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query'=>$query->all(), 'pages'=>$pages, 'sort'=>$sort, 'cpage'=>$pages->page];

    }



    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData'],
            'fileName'=>'Reports',
            'title'=>' Student Report Datasheet',
            'exportFile'=>'@app/modules/studentreport/views/student-report/exportPdfExcel',
        ];

        return $data;
    }
}

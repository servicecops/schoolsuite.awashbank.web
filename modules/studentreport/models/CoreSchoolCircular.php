<?php

namespace app\modules\studentreport\models;

use app\modules\schoolcore\models\CoreTerm;
use Yii;
use yii\db\Exception;
use yii\db\Query;

/**
 * This is the model class for table "core_school_circular".
 *
 * @property string|null $date_created
 * @property string|null $date_updated
 * @property int|null $created_by
 * @property int $id
 * @property int|null $school_id
 * @property int|null $term_id
 * @property string|null $circular_name
 * @property string|null $date_modified
 * @property string|null $attachment_file
 * @property string|null $citation
 * @property string|null $subject
 * @property string|null $the_body
 * @property string|null $ccc_description
 * @property string|null $circular_type
 */
class CoreSchoolCircular extends \yii\db\ActiveRecord
{
    public $classes, $signatories, $circular_print_out,$msg_type,$message_type,$recipient_group, $message_template, $student_class;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'core_school_circular';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['circular_subject',  'the_body', 'term_id', 'ccc_description','circular_name'], 'required'],
            [['date_created', 'date_updated', 'date_modified', 'classes'], 'safe'],
            [['circular_print_out'], 'file', 'maxSize' => '100000', 'skipOnEmpty' => true, 'extensions' => 'docx, doc, pdf'],
            [['created_by', 'school_id', 'term_id'], 'default', 'value' => null],
            [['created_by', 'school_id', 'term_id','signature_id'], 'integer'],
            [[ 'attachment_file', 'circular_subject', 'ccc_description','circular_name','circular_ref_no','message_template','circular_type','circular_type'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'id' => 'ID',
            'school_id' => 'School ID',
            'term_id' => 'Term ID',
            'date_modified' => 'Date Modified',
            'attachment_file' => 'Attachment File',
            'citation' => 'Citation',
            'subject' => 'Subject',
            'the_body' => 'Body',
            'ccc_description' => 'Ccc Description',
            'circular_print_out' => 'Attach the print out of the circular(optional)'
        ];
    }

    public function getTermName()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }

    public function Signatures()
    {
        return Yii::$app->db->createCommand('select * from core_signatures')
            ->queryAll();
    }

    public function getPlaceholders()
    {
        $query = (new Query())->from('place_holders')->where(['type'=>'CORE'])->orderBy('type, name')->all();
        return $query;
    }
}

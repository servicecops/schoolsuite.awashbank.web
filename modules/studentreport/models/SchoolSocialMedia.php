<?php

namespace app\modules\studentreport\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreTerm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "core_school_circular".
 *
 * @property string|null $date_created
 * @property string|null $date_updated
 * @property int|null $created_by
 * @property int $id
 * @property int|null $school_id
 * @property int|null $term_id
 * @property string|null $circular_name
 * @property string|null $date_modified
 * @property string|null $attachment_file
 * @property string|null $citation
 * @property string|null $subject
 * @property string|null $the_body
 * @property string|null $ccc_description
 */
class SchoolSocialMedia extends \yii\db\ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_social_media_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'school_id', 'facebook_link','twitter_link','linkedin_link'], 'required'],
            [['facebook_link','twitter_link','linkedin_link','telegram_link'], 'url'],
            [['date_created',  'date_modified', 'school_id', 'facebook_link','twitter_link','linkedin_link','telegram_link'], 'safe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'id' => 'ID',
            'school_id' => 'School ID',
            'date_modified' => 'Date Modified',
            'social_media_name'=>'Social Media Name',
            'social_media_link'=>'Social Media Link',
        ];
    }

    public function getTermName()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }

    public function Signatures()
    {
        return Yii::$app->db->createCommand('select * from core_signatures')
            ->queryAll();
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }




    public function search($params)
    {
        $query = new Query();
        $query->select(['sml.id', 'sml.facebook_link', 'sml.linkedin_link', 'sml.twitter_link',  'csc.school_name', 'sml.date_created', ])
            ->from('school_social_media_links sml')
            ->innerJoin('core_school csc', 'sml.school_id= csc.id');
        $query->orderBy('sml.id desc');


        if (\app\components\ToWords::isSchoolUser()) {

            $query->andWhere(['sml.school_id' => Yii::$app->user->identity->school_id]);
            // }
        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {

            $query->andWhere(['csc.id' => $this->schsearch]);

        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'created_by' => $this->created_by,
            'school_id' => $this->school_id,
        ]);


        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        return $dataProvider;
    }



}

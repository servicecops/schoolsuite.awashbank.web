<?php

namespace app\modules\studentreport\models;

use Yii;

/**
 * This is the model class for table "student_group_student".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $student_id
 * @property string $date_added
 * @property string $type
 */
class ReportGroupSubject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_group_test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'test_id','type'], 'required'],
            [['group_id', 'test_id'], 'integer'],
            [['date_added'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'test_id' => 'Test ID',
            'date_added' => 'Date Added',
        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData'],
            'fileName'=>'StudentGroup',
            'title'=>'Grouped Student Datasheet',
            'exportFile'=>'@app/modules/studentreport/views/student-report/exportPdfExcel',
        ];

        return $data;
    }
}

<?php

namespace app\modules\studentreport\models;

use app\modules\e_learning\models\Elearning;
use app\modules\gradingcore\models\Grading;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * FeeClassSearch represents the model behind the search form about `app\models\FeeClass`.
 */
class ReportSubjectCompositionSearch extends ReportSubjectComposition
{
    public $modelSearch, $schsearch, $pc_rgnumber, $phoneSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'class_id'], 'integer'],
            [['date_created', 'created_by'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function getExportQuery()
    {
        $data = [
            'data' => $_SESSION['findData'],
            'labels' => ['grades','class_id','school_id','min_mark','max_mark'],
            'fileName' => 'Grades',
            'title' => 'student Datasheet',
            'exportFile' => '/grading/exportPdfExcel',
        ];

        return $data;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new Query());
        $query = Grading::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $columns = [
            'id',
            'min_mark',
            'max_mark',
            'grades',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],



        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

            'school_id' => $this->school_id,
            'class_id' => $this->class_id,
            'grades' => $this->grades,
        ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $searchForm = 'grades';


        $searchModel = new GradingSearch();
        return ['columns' => $columns, 'dataProvider' => $dataProvider, 'searchForm' => $searchForm, 'searchModel' => $searchModel];
    }

    /**
     * @param $params
     * @return array
     * Return attributes and model for a view
     */

    public function viewModel($params)
    {
        $attributes = [
            'id',
            'min_mark',
            'max_mark',
            'grades'

        ];

        if (($models = Grading::findOne($params)) !== null) {
            return ['attributes' => $attributes, 'models' => $models];
        }

        throw new NotFoundHttpException('The requested page does not exist.');


    }

    public function searchIndex($params)
    {

        $query = (new Query())
            ->select(['cg.id', 'cg.grades', 'cg.min_mark', 'cg.max_mark','csc.school_name','cc.class_description as class','cc.class_code'
             ])
            ->from('core_grades cg')
            ->innerJoin('core_school_class cc', 'cg.class_id= cc.id')
            ->innerJoin('core_school csc', 'cg.school_id= csc.id');
        $query->orderBy('cg.grades asc');
        //  ->where(['si.active'=>true, 'si.archived'=>false]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (\app\components\ToWords::isSchoolUser() && Yii::$app->user->can('is_teacher')) {
            $query->innerJoin('teacher_class_subject_association tca', 'cc.id=tca.class_id');

            $trQuery = new Query();
            $trQuery->select(['tc.class_id'])
                ->from('teacher_class_subject_association tc')
                ->innerJoin('core_staff cs', 'cs.id=tc.teacher_id')
                ->innerJoin('user uz', 'cs.id=uz.school_user_id')
                ->Where(['uz.id' => Yii::$app->user->identity->getId()]);


            $commandt = $trQuery->createCommand();
            $data = $commandt->queryAll();


            $the_classId = [];
            foreach($data as $k =>$v){
                array_push($the_classId, $v['class_id']);
            }
            Yii::trace($the_classId);

            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['tca.class_id' => intVal($this->class_id)]);
            } else {
                // $query->andWhere(['tca.class_id' => 18]);
                $query->andwhere(['in', 'tca.class_id', $the_classId]);
            }
        } elseif (\app\components\ToWords::isSchoolUser()) {
            if ($this->class_id && $this->class_id != 0) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } else {
                $query->andWhere(['csc.id' => Yii::$app->user->identity->school_id]);
            }
        } elseif (Yii::$app->user->can('schoolsuite_admin') && $this->schsearch) {
            if (!empty($this->class_id)) {
                $query->andWhere(['csc.id' => intVal($this->id)]);
            } elseif ($this->schsearch) {
                $query->andWhere(['csc.id' => $this->schsearch]);
            }
        }

        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'grades',
                'min_mark',
                'max_mark',
                'school_name',
                'class',
                'id'

            ],
        ]);
        $columns = [
            'id',
            'min_mark',
            'max_mark',
            'grades',

            [
                'label' => 'School Name',
                'value' => function ($model) {
                    return $model->schoolName->school_name;
                },
            ],
            [
                'label' => 'Class Name',
                'value' => function ($model) {
                    return $model->className->class_description;
                },
            ],



        ];
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('cg.grades');
        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page, 'columns'=>$columns];

    }


}

<?php

namespace app\modules\studentreport\models;

use Yii;

/**
 * This is the model class for table "student_group_student".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $student_id
 * @property string $date_added
 * @property string $type
 */
class SubjectSemesterAssementAssocaition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject_semester_assesment_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id', 'assessment_id','subject_id','composition'], 'required'],
            [['school_id', 'test_id'], 'integer'],
            [['date_added','school_id','class_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'test_id' => 'Test ID',
            'date_added' => 'Date Added',
        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData'],
            'fileName'=>'StudentGroup',
            'title'=>'Grouped Student Datasheet',
            'exportFile'=>'@app/modules/studentreport/views/student-report/exportPdfExcel',
        ];

        return $data;
    }
}

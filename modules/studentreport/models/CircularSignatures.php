<?php

namespace app\modules\studentreport\models;

use Yii;

/**
 * This is the model class for table "circular_signatures".
 *
 * @property string|null $date_created
 * @property int|null $circular_id
 * @property int|null $signature_id
 * @property int $id
 * @property string|null $date_updated
 * @property int|null $created_by
 */
class CircularSignatures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'circular_signatures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created', 'date_updated'], 'safe'],
            [['circular_id', 'signature_id', 'created_by'], 'default', 'value' => null],
            [['circular_id', 'signature_id', 'created_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date_created' => 'Date Created',
            'circular_id' => 'Circular ID',
            'signature_id' => 'Signature ID',
            'id' => 'ID',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
        ];
    }
}

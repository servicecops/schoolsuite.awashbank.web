<?php

namespace app\modules\studentreport\models;

use app\modules\e_learning\models\TeacherUploads;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreTerm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "core_school_circular".
 *
 * @property string|null $date_created
 * @property string|null $date_updated
 * @property int|null $created_by
 * @property int $id
 * @property int|null $school_id
 * @property int|null $term_id
 * @property string|null $circular_name
 * @property string|null $date_modified
 * @property string|null $attachment_file
 * @property string|null $citation
 * @property string|null $subject
 * @property string|null $the_body
 * @property string|null $ccc_description
 */
class UploadedCircular extends \yii\db\ActiveRecord
{
    public $uploads;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_uploaded_circulars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','circular_name','circular_description','school_id','date_created','created_by'], 'safe'],
            [['uploads'], 'file', 'extensions' => 'png, jpg,pdf, doc, docx, xls, xlsx, mp3,mp4', 'maxFiles' => 10],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'created_by' => 'Created By',
            'id' => 'ID',
            'school_id' => 'School ID',
            'date_modified' => 'Date Modified',
            'social_media_name'=>'Social Media Name',
            'social_media_link'=>'Social Media Link',
        ];
    }

    public function getTermName()
    {
        return $this->hasOne(CoreTerm::className(), ['id' => 'term_id']);
    }

    public function Signatures()
    {
        return Yii::$app->db->createCommand('select * from core_signatures')
            ->queryAll();
    }

    public function getSchoolName()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }




    public function search($params)
    {
        $query = new Query();
        $query->select(['sds.id', 'sds.circular_name', 'sds.circular_description',  'csc.school_name', 'sds.date_created', ])
            ->from('school_uploaded_circulars sds')
            ->innerJoin('core_school csc', 'sds.school_id= csc.id');
        $query->orderBy('sds.id desc');


        if (\app\components\ToWords::isSchoolUser()) {

            $query->andWhere(['sds.school_id' => Yii::$app->user->identity->school_id]);
            // }
        } elseif (Yii::$app->user->can('schoolpay_admin') && $this->schsearch) {

            $query->andWhere(['csc.id' => $this->schsearch]);

        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pagesize' => 20 // in case you want a default pagesize
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'school_id' => $this->school_id,
        ]);


        unset($_SESSION['findData']);
        $_SESSION['findData'] = $query;
        return $dataProvider;
    }

    public function getStoredFiles(){
        $files = (new Query())->select(['id', 'file_name'])->from('circular_uploads')
            ->where(['submission_id'=>$this->id]);
        return $files->all();
    }

    public function saveUploads()
    {
        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {

            foreach ($this->uploads as $file) {
//                $saved = Yii::getAlias('@uploads').'/' . $file->baseName . '.' . $file->extension;
                $saved = Yii::$app->basePath.'/web/uploads/' . $file->baseName . '.' . $file->extension;
                $upload = new CircularUploads();
                $fname = $file->baseName . '.' . $file->extension;
                if ($file->extension == 'ts' || $file->extension == 'mp3' || $file->extension == 'mp4') {
                    Yii::trace("file extension" . $file->extension);
                    $file->saveAs($saved);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_path = $fname;
                    $upload->save(false);
                } else {

                    $tmpfile_contents = file_get_contents($file->tempName);
                    Yii::trace($tmpfile_contents);
                    $upload->submission_id = $this->id;
                    $upload->file_name = $fname;
                    $upload->file_data = base64_encode($tmpfile_contents);
                    $upload->save(false);
                    Yii::trace('Saved file ' . $file->baseName);
                    return $upload->id;
                }
            }
        }
    }

    public function saveUploadUpdate($id)
    {

        Yii::trace($this->uploads);
        if (is_array($this->uploads) && $this->uploads) {
            foreach ($this->uploads as $file) {

                $fname = $file->baseName . '.' . $file->extension;
                $tmpfile_contents = file_get_contents( $file->tempName );
                $upload = new CircularUploads();
                Yii::trace('The Id'.$id);
                $upload->submission_id = $id;
                $upload->file_name = $fname;
                $upload->file_data = base64_encode($tmpfile_contents);
                $upload->save(false);
                Yii::trace('Saved file ' . $file->baseName);
            }
        }
    }


}

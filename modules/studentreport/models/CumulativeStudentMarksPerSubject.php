<?php

namespace app\modules\studentreport\models;

use Yii;

/**
 * This is the model class for table "student_group_student".
 *
 * @property integer $id
 * @property integer $group_id
 * @property integer $student_id
 * @property string $date_added
 * @property string $type
 */
class CumulativeStudentMarksPerSubject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cumulative_marks_for_subject_per_semester';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id','subject_id','final_mark','report_term_association'], 'required'],
            [['school_id', 'test_id', 'student_id','semester_id','final_mark_quarter_one','final_mark_quarter_two','final_mark_quarter_three','final_mark_quarter_four',
            'final_mark_semester_one','final_mark_semester_two',
            ], 'integer'],
            [['date_added','school_id','class_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Group ID',
            'test_id' => 'Test ID',
            'date_added' => 'Date Added',
        ];
    }

    public static function getExportQuery()
    {
        $data = [
            'data'=>$_SESSION['findData'],
            'fileName'=>'StudentGroup',
            'title'=>'Grouped Student Datasheet',
            'exportFile'=>'@app/modules/studentreport/views/student-report/exportPdfExcel',
        ];

        return $data;
    }
}

<?php
namespace app\modules\studentreport;

class StudentReportModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\studentreport\controllers';
    public function init() {
        parent::init();
    }
}

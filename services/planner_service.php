<?php

use app\modules\logs\models\Logs;
use yii\db\Expression;
use yii\db\Query;

class PlannerService{

    public function actionRun(){
        Yii::info("Planner  service is now starting");

        while(true){
            try {
                $this->processEmailDueAlerts();
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace($error);
            }
        }

    }

    /**
     * Will send the email alerts for the due tasks
     */
    public function processEmailDueAlerts(){
          try {
               return \Yii::$app->runAction('/planner/planner-sub-activity/send-task-due-email-alerts');
          } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
          }
    }
}

?>


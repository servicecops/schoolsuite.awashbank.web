<?php

use app\modules\logs\models\Logs;
use yii\db\Expression;
use yii\db\Query;

class TimetableService{

    public function actionRun(){
        Yii::info("Timetable  service is now starting");

        while(true){
            try {
                $this->processScheduledEventReminderAlerts();
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace($error);
            }
        }

    }

    /**
     * Will send the email alerts for the scheduled events
     */
    public function processScheduledEventReminderAlerts(){
        try {
            return \Yii::$app->runAction('/timetable/mailing-list/send-event-reminders');
        } catch (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
        }
    }
}

?>


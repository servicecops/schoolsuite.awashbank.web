<?php

use app\modules\logs\models\Logs;use PhpOffice\PhpSpreadsheet\Shared\Date;
use yii\db\Expression;use yii\db\Query;

class fees_service
{


    public  function actionRun() {
        Yii::info("Fee associations processor service is now starting");
        while (true) {
            try {
                $this->processPendingFeeClassAssociations();
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                Yii::trace($error);
            }

            }
        }
      //  Yii::trace("Fee associations processor service is now exiting");


    /**
     * @throws \yii\db\Exception
     * Will process due penalties
     */
    public function processPenalties()
    {
        $query = (new Query())
            ->select(['assoc.id as associd ',])
            ->from('institution_fee_student_association assoc')
            ->innerJoin('core_student student', 'assoc.student_id=student.id')
            ->innerJoin('institution_fees_due fee', 'assoc.fee_id=fee.id')
            ->innerJoin('institution_fees_due_penalty penalty', 'fee.penalty_id = penalty.id ')
            ->where(['assoc.penalties_closed' => false, 'assoc.next_penalty_apply_date <= current_date', 'fee_outstanding_balance' < 0, 'assoc.has_penalty ' => true]);

        foreach ($query as $value) {
            try {
                $assocId = $value;
                $result = \Yii::$app->db->createCommand("CALL apply_student_penalty(:associd)")
                    ->bindValue(':associd', $assocId)
                    ->execute();
            } catch (\Exception $e) {
                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
                return $error;
            }
        }

    }

    /**
     * Will process pending fee associations and pass debits to the respentive students
     * This will only apply fees that are not recurrent
     */
    public function processRecurringFees()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {

            //Get pending recurring associations

            $feesDueList = Yii::$app->db->createCommand('SELECT * FROM institution_fees_due WHERE approval_status=true AND recurrent=true AND effective_date<=:currentTimeStamp AND end_date>=:currentTimeStamp AND next_apply_date<=:currentTimeStamp ')
                ->bindValue(':currentTimeStamp', new Expression('NOW()'))
                ->bindValue(':status', 1)
                ->queryAll();

            Yii::trace($feesDueList);
            if (!$feesDueList) {
                Yii::trace("No due recurring fee associations to process");
                return;
            }

            foreach ($feesDueList as $feesDue) {
                Yii::trace($feesDue);
                //Determine periods
//
                $nextApplyDate = NULL;
                $final = NULL;
                $thisFeeStartDate = $feesDue['next_apply_date'];
                $applyFreq = $feesDue['apply_frequency'];
                if ($feesDue['apply_frequency_unit'] == 'MONTHLY') {

                    $time = strtotime($thisFeeStartDate);
                    $final = date("Y-m-d", strtotime('+' . $applyFreq . 'month', $time));

                } else {
                    $time = strtotime($thisFeeStartDate);
                    $final = date("Y-m-d", strtotime('+' . $applyFreq . 'week', $time));
                }
                $nextApplyDate = $final;
                Yii::trace($final);

                $formattedPeriodStart = strval($thisFeeStartDate);
                $formattedPeriodEnd = strval($nextApplyDate);

//                Get associated classes for this fees due
                $result = Yii::$app->db->createCommand('SELECT fees.id as feeId, classes.id as classId              
                    from institution_fees_due fees 
                    inner join institution_fee_class_association assoc on fees.id = assoc.fee_id 
                    inner Join core_school_class classes on classes.id=assoc.class_id
                    where fees.id = :feeId')
                    ->bindValue(':feeId', $feesDue['id'])
                    ->queryAll();

                Yii::trace($result);
                //If associations list is empty, no point in proceeding


                if (!$result) {
                    Yii::trace("Recurrent fee " . $feesDue['id'] . " has no classes. Abandoning fee application for now");
                    continue;
                }


                $description = ($feesDue['description'] . " " . $formattedPeriodStart . " to " . $formattedPeriodEnd);
                //    Create child fee due for this period
                //   And shift the next application date
                $connection->createCommand("INSERT INTO institution_fees_due (recurrent,
                        parent_fee_id ,
                        approval_status,
                         ready_for_approval,
                         effective_date,
                         end_date,
                         date_created,
                         delete_flag,
                         description,
                         date_approved,
                         due_amount,
                         school_id,
                         child_of_recurrent,
                         created_by,
                         priority,
                         has_penalty,
                         arrears_after_units,
                         arrears_after_count,
                         arrears_start_date,
                         penalty_id) 
                    VALUES(false  ,
                          :parent_fee_id ,
                          true,
                          true,
                          :formattedPeriodStart,
                          :formattedPeriodEnd,
                          NOW(),
                           false,
                           :description,
                           NOW(),
                           :due_amount,
                           :school_id,
                           true,
                           :created_by,
                           :priority,
                           :has_penalty,
                           :arrears_after_units,
                          :arrears_after_count,
                          :arrears_start_date,
                          :penalty_id
                          )")
                    ->bindParam("parent_fee_id", $feesDue['id'])
                    ->bindParam("formattedPeriodStart", $formattedPeriodStart)
                    ->bindParam("formattedPeriodEnd", $formattedPeriodEnd)
                    ->bindParam("description", $description)
                    ->bindParam("due_amount", $feesDue['due_amount'])
                    ->bindParam("school_id", $feesDue['school_id'])
                    ->bindParam("created_by", $feesDue['created_by'])
                    ->bindParam("priority", $feesDue['priority'])
                    ->bindParam("has_penalty", $feesDue['has_penalty'])
                    ->bindParam("arrears_after_count", $feesDue['arrears_after_count'])
                    ->bindParam("arrears_after_units", $feesDue['arrears_after_units'])
                    ->bindParam("arrears_start_date", $feesDue['arrears_start_date'])
                    ->bindParam("penalty_id", $feesDue['penalty_id'])
                    //                save this new fee
                    ->execute();
                //Now apply this child fee to every class
                $id = Yii::$app->db->getLastInsertID();
                Yii::trace($id);
                $currentTimestamp = new Expression('NOW()');
                foreach ($result as $results) {
                    Yii::trace($results);
                    // Apply to class
                    $connection->createCommand("INSERT INTO institution_fee_class_association  (applied,class_id,created_by,fee_id,date_created) 
                              VALUES(false,:class_id,'',:feeId,:dates
                              )")
                        ->bindParam("class_id", $results['classid'])
                        ->bindParam("feeId", $id)
                        ->bindParam("dates", $currentTimestamp)
                        ->execute();
                }
            }

            Yii::trace("Recursive Fee with ID " . $feesDue['id'] . " Processed  for period " . $formattedPeriodStart . " to " . $formattedPeriodEnd);

            //Shift the feeDue dates
            $sql2 = "UPDATE institution_fees_due SET last_apply_date =:last_apply_date ,next_apply_date = :nextApplyDate WHERE  id= :feeId";
            $connection->createCommand($sql2)
                ->bindValue(':last_apply_date', new Expression('NOW()'))
                ->bindValue(':feeId', $feesDue['id'])
                ->bindValue(':nextApplyDate', $formattedPeriodEnd)
                ->execute();


            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            return $error;
        }
    }

    public function processBookedStudentFees()
    {
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {


            $bookedFeesList = Yii::$app->db->createCommand('SELECT * FROM institution_fee_student_association WHERE applied=:applied AND due_date=:dueDate')
                ->bindValue(':applied', false)
                ->bindValue(':dueDate', new Expression('NOW()'))
                ->queryAll();

            if (!$bookedFeesList)
                return;

            Yii::trace($bookedFeesList);
            foreach ($bookedFeesList as $studentAssociation) {
                $sqlQuery = Yii::$app->db->createCommand("SELECT * FROM apply_fee_to_student(:student_id, :fee_id, 0, 'SYSTEM', '') AS result")
                    ->bindValue(':student_id', $studentAssociation['student_id'])
                    ->bindValue(':fee_id', $studentAssociation['fee_id'])
                    ->queryOne();


                Yii::trace($sqlQuery);
                $aBoolean = $sqlQuery;

                Yii::trace("Return: " . " " . " - Booked Fee applied ok for  student " . $studentAssociation['student_id']);
            }

            $transaction->commit();
        } catch
        (\Exception $e) {
            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Yii::trace($error);
            $transaction->rollBack();
        }
    }

//    private function enableSchoolpayMessages()
//    {
//        return Yii::info("YES", getProperty("EnableSchoolpayMessagesService", "NO"));
//    }
//
//    private function processSchoolpayMessages()
//    {
//        if (!enableSchoolpayMessages()) return;
//
//        $now = new Date();
//
//        $query = Yii::$app->db->createCommand('SELECT * FROM msg_school_messages WHERE schedule_date=:schedule_date AND processed=:processed')
//            ->bindValue(':schedule_date', new Expression('NOW()'))
//            ->bindValue(':processed', false)
//            ->queryOne();
//
//        if (!$query) return;
//        //SchoolpayMessageProcessingHelper helper = new SchoolpayMessageProcessingHelper(getContext());
//        foreach ($query as $msg) {
//            try {
//                processScheduledMessage($msg);
//            } catch (\Exception $e) {
//                $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
//                return $error;
//            }
//        }
//    }


     /**
     * Will process pending fee associations and pass debits to the respective students

     */
    public function processPendingFeeClassAssociations()
    {

        $connection = Yii::$app->db;
        try {

            //Get pending fee associations

            $feeAssociationsList = Yii::$app->db->createCommand('SELECT fees.id as feeId, classes.id as classId,classes.class_description, fees.child_of_recurrent, fees.due_amount           
                    , fees.description
                    from institution_fees_due fees 
                    inner join institution_fee_class_association assoc on fees.id = assoc.fee_id 
                    inner Join core_school_class classes on classes.id=assoc.class_id
                    WHERE assoc.fee_id = fees.id AND assoc.applied = false and fees.approval_status=true AND fees.effective_date<=:currentTimeStamp AND assoc.class_id=classes.id AND fees.recurrent=false')
                ->bindValue('currentTimeStamp', new Expression('NOW()'))
                ->queryAll();


            if (!$feeAssociationsList) {
                Yii::trace("No due fee associations to process");
                return;
            }


            foreach ($feeAssociationsList as $object) {
                Yii::trace($object);


                //Get all students in class

                $studentsList = Yii::$app->db->createCommand('SELECT * FROM core_student WHERE class_id=:sclass ')
                    ->bindValue(':sclass', $object['classid'])
                    ->queryAll();

                //Determine parent


                if ($object['child_of_recurrent']) {
                    $parentRecurrentFeeDue = $object['feeid'];
                }

                //Fetch them
                $amountLong = $object['due_amount'];


                Yii::trace("Going to process fee due " . $object['description'] . " " . $amountLong);
                $processedRecords = 0;

                foreach ($studentsList as $student) {
                    $transaction = $connection->beginTransaction();

                    Yii::trace($student);
                    //Check exemption for this student and fee
                    //If fee is child of recurrent, check exemption from parent fee
                    //Criteria feeExemption = session.createCriteria(Entity_FeesDueStudentExemption.class);
                    $feeIdToConsiderForExemption = $object['feeid'];
                    if ($object['child_of_recurrent']) {
                        $feeIdToConsiderForExemption = $parentRecurrentFeeDue;
                    }

                    $feeExemption = Yii::$app->db->createCommand('SELECT * FROM fees_due_student_exemption WHERE student_number=:studentNumber and fee_id =:feeId  ')
                        ->bindValue(':studentNumber', $student['student_code'])
                        ->bindValue(':feeId', $feeIdToConsiderForExemption)
                        ->queryOne();

                    //Check exemption now

                    if ($feeExemption) {
                        Yii::trace("Student " . $student['first_name'] . " " . $student['last_name'] . " is exempted from fee " . $object['feeid']);
                        continue;
                    }


                    $connection->createCommand("select apply_fee_to_student(
                        :student_id, 
                        :fee, 
                        :userid, 
                        :username, 
                        :userip)", [
                        ':student_id' => $student['id'],
                        ':fee' => $object['feeid'],
                        ':userid' => 0,
                        ':username' => 'SYSTEM',
                        ':userip' => '',
                    ])->execute();
                    $transaction->commit();

                    Logs::logEvent("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['id'] . " ", null, $student['id']);


                    Yii::trace("Attaching " . ucwords($object['description']) . " (" . number_format($object['due_amount'], 2) . ") to -" . $student['id']);
                    //Yii::trace("Return:  - Fee applied ok for  student " . $student['last_name'] . " " . $student['first_name']);
                }

                //Update status of fee association
                $sql2 = "UPDATE institution_fee_class_association SET applied = true,date_applied=:date WHERE fee_id = :feeId";
                $connection->createCommand($sql2)
                    ->bindValue(':date', new Expression('NOW()'))
                    ->bindValue(':feeId', $object['feeid'])
                    ->execute();

                Yii::trace("Fee association applied to class " . $object['description']);
            }
            Yii::trace("Fee association applied to all classes ");

        } catch (\Exception $e) {
            $transaction->rollback();

            $error = isset($e->errorInfo[2]) ? $e->errorInfo[2] : $e->getMessage();
            Logs::logEvent("Error on assigning a fee: " . $student['last_name'].' '.$student['first_name'] . "(" . $student['student_code'] . ")", $error, $student['id']);

            Yii::trace($error);
            return $error;
            //Yii::trace($error)
        }
    }


}

?>

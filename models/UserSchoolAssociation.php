<?php

namespace app\models;

use app\modules\schoolcore\models\CoreSchool;
use Yii;

/**
 * This is the model class for table "user_school_association".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $school_id
 * @property string $date_created
 *
 * @property CoreSchool $school
 * @property User $user
 */
class UserSchoolAssociation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_school_association';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'school_id'], 'integer'],
            [['date_created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'school_id' => 'School ID',
            'date_created' => 'Date Created',
        ];
    }


    public static function setUserSchools($array, $user_id){
        foreach($array as $k=>$v){
            $model = new UserSchoolAssociation();
            $model->user_id = $user_id;
            $model->school_id = $v;
            $model->save();
        }
    }

    public static function delAssc($user_id){
            $model = UserSchoolAssociation::find()->where(['user_id'=>$user_id])->all();
            foreach($model as $v){
                $v->delete();
            }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

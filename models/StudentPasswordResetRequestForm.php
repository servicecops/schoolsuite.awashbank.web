<?php
namespace app\models;

use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Password reset request form
 */
class StudentPasswordResetRequestForm extends Model
{
    /**
     * @var string
     */
    public $student_email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['student_email', 'trim'],
            ['student_email', 'required'],
            ['student_email', 'email'],
            ['student_email', 'exist',
                'targetClass' => '\app\modules\schoolcore\models\CoreStudent',
                'filter' => ['locked' => false, 'archived'=>false],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * Sends confirmation email to user
     *
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        $user = CoreStudent::findOne([
            'student_email' => $this->student_email,
            'locked' => false,
            'archived'=>false
        ]);

        if ($user === null) {
            return false;
        }
        if (!CoreStudent::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'studentPasswordResetToken-html', 'text' => 'studentPasswordResetToken-html'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] =>'Schoolsuite'])
            ->setTo($this->student_email)
            ->setSubject('Student Password reset for schoolsuite ')
            ->send();
    }
}

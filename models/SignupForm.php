<?php
namespace app\models;

use app\components\Helpers;
use app\modules\logs\models\Logs;
use app\modules\schoolcore\models\CoreSchool;
use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * Signup form
 * @property CoreSchool $school
 *  @property integer $school_id
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $firstname;
    public $mobile_phone;
    public $date_created;
    public $locked;
    public $school_id;
    public $user_level;
    public $user_permissions;
    public $date_updated;
    public $lastname;
    public $middle_name;
    public $created_by;
    public $bank_id;


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_by',
                'updatedAtAttribute' => 'updated_by',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['firstname','lastname'], 'trim'],
            [['firstname','lastname','mobile_phone'], 'required'],

            [['locked'], 'boolean'],

            ['bank_id', 'selectBank', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['school_id', 'selectBank', 'skipOnEmpty' => false, 'skipOnError' => false],

            [['date_created', 'date_updated'], 'safe'],

            ['user_level', 'required'],
            [['user_level', 'user_permissions'],'string'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],

            [['username', 'firstname', 'lastname', 'mobile_phone', 'user_level', 'email'], 'required', 'on'=>'update'],

        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['change'] = ['current_pass', 'new_pass', 'password', 'password2'];
        $scenarios['count'] = ['invalid_login_count', 'locked'];
        $scenarios['update'] = ['username', 'firstname', 'lastname', 'mobile_phone', 'user_level', 'school_id', 'bank_id', 'email', 'locked'];

        return $scenarios;
    }


    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        $user = new User();
        $user->generateAuthKey();
       return $user->save(false);


    }

    /*
     * get available user roles
     */
    public function getUsertype()
    {
        $desc = AuthItem::find()->where(['name'=>$this->user_level])->limit(1)->one();
        return $desc->description;
    }
    /*
     * attach user level to user
     */

    public function setAuthAssignment($role, $id)
    {
        $authAssign = AuthAssignment::find()->where(['user_id'=>$id])->limit(1)->one();
        if($authAssign !== null){
            $authAssign->item_name = $role;
            $authAssign->user_id = $id;
            return $authAssign->save(false);
        }else
        {
            $authAssign = new AuthAssignment();
            $authAssign->item_name = $role;
            $authAssign->user_id = $id;
            return $authAssign->save(false);
        }

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
//     */

    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
   }

    public function selectSch($attribute,$params)
    {
//        $selectedSch = false;
//        if(is_array($this->school_id) && !empty($this->school_id)){
//            Yii::trace("This is school array", "SCHOOL_ARRAY");
//            $selectedSch = true;
//        }
//        if($this->school_id){
//            $selectedSch = true;
//        }
        if(Helpers::IsSchoolUser($this->user_level) && !$this->school_id ) {
            $this->addError($attribute, 'Please select school');
        }
    }

    public function selectBank($attribute,$params)
    {

        if($this->user_level == 'bank_user'  && !$this->bank_id ) {
            $this->addError($attribute, 'Please select Bank');
        }
    }
}

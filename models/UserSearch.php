<?php

namespace app\models;

use app\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;

if (!Yii::$app->session->isActive){
          session_start();
      }

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    public $modelSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['username', 'firstname', 'mobile_phone', 'email', 'school_id', 'created_at', 'user_level', 'updated_at', 'lastname'], 'safe'],
            [['locked'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

//        Yii::trace($this->school_id);
//        $query = User::find();


        $query = new Query();
        $query->select(['uz.id','uz.username','sch.school_name', 'uz.firstname', 'uz.lastname','uz.branch_id','uz.mobile_phone', 'uz.user_level','uz.region_id', 'uz.school_id', 'uz.bank_id', 'uz.email', ])
            ->from('user uz');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->leftJoin('core_school sch','sch.id =uz.school_id');

        $query->andFilterWhere([
            'id' => $this->id,
            'invalid_login_count' => $this->invalid_login_count,
            'created_at' => $this->created_at,
            'locked' => $this->locked,
            'updated_at' => $this->updated_at,
        ]);
        //Search by name
        $query->andFilterWhere(['ilike', 'username', $this->username]);
        $query->andFilterWhere(['archived' => false]);

        if($this->school_id) $query->andFilterWhere(['school_id' => $this->school_id]);
        if($this->user_level) {
            $query->andFilterWhere(['user_level' => $this->user_level]);
        } else {
            //Only retrieve users i can administer
            $usersICanAdminister = ArrayHelper::map(AuthItem::getUserLevelsICanAdminister(), 'name', 'description');
            Yii::trace($usersICanAdminister);
            $query->andFilterWhere(['in', 'user_level', array_keys($usersICanAdminister)]);
        }

        if (\app\components\ToWords::isSchoolUser()) {

            $query->andWhere(['school_id' => Yii::$app->user->identity->school_id]);

        }

        if(Yii::$app->user->can('view_by_branch')){
            //If school user, limit and find by id and school id
            $query->andWhere(['sch.branch_id' => Yii::$app->user->identity->branch_id]);

        }


        $pages = new Pagination(['totalCount' => $query->count('*', Yii::$app->db)]);
        $sort = new Sort([
            'attributes' => [
                'student_code', 'first_name', 'last_name', 'school_name', 'class_code', 'student_email', 'student_phone', 'school_student_registration_number'
            ],
        ]);
        ($sort->orders) ? $query->orderBy($sort->orders) : $query->orderBy('uz.created_at desc');
        unset($_SESSION['findData']);
        $this->setSession('Schools Transaction Summary', $query, $this->getColT(), 'school_transaction_report');

        $query->offset($pages->offset)->limit($pages->limit);
        return ['query' => $query->all(Yii::$app->db), 'pages' => $pages, 'sort' => $sort, 'cpage' => $pages->page];
    }
    public static function getExportQuery()
    {


        $data = [
            'data' => $_SESSION['findData']['query'],
            'labels' => $_SESSION['findData']['cols'],
            'title' => $_SESSION['findData']['title'],
            'fileName' => $_SESSION['findData']['file_name'],
            'exportFile' => $_SESSION['findData']['exportFile'],
        ];
        return $data;
    }

    protected function setSession($title, $query, $cols, $filename)
    {
        unset($_SESSION['findData']);
        $_SESSION['findData']['title'] = $title;
        $_SESSION['findData']['query'] = $query;
        $_SESSION['findData']['cols'] = $cols;
        $_SESSION['findData']['file_name'] = $filename;


        $_SESSION['findData']['exportFile'] = '@app/views/common/exportPdfExcel';

//        if(isset($_SESSION['PENALTY_MODE']=='penalty')) {
//            $_SESSION['findData']['exportFile'] = '@app/modules/schoolcore/views/school-information/exportPenaltyPdfExcel';
//
//        }
    }

    public function getColT()
    {
        return [
            'username' => ['label' => 'Username'],
            'email' => ['label' => 'Email '],
            'user_level' => ['label' => 'User Level '],
            'school_name' => ['label' => 'School Name '],

        ];
    }

    public function searchArchived($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('school');

        $query->andFilterWhere([
            'id' => $this->id,
            'invalid_login_count' => $this->invalid_login_count,
            'created_at' => $this->created_at,
            'locked' => $this->locked,
            'updated_at' => $this->updated_at,
        ]);
        //Search by name
        $query->andFilterWhere(['ilike', 'username', $this->username]);
        $query->andFilterWhere(['archived' => true]);

        if($this->school_id) $query->andFilterWhere(['school_id' => $this->school_id]);
        if($this->user_level) {
            $query->andFilterWhere(['user_level' => $this->user_level]);
        } else {
            //Only retrieve users i can administer
            $usersICanAdminister = ArrayHelper::map(AuthItem::getUserLevelsICanAdminister(), 'name', 'description');
            $query->andFilterWhere(['in', 'user_level', array_keys($usersICanAdminister)]);
        }

        if (Yii::$app->user->can('own_sch')) {

            $query->andWhere(['school_id' => Yii::$app->user->identity->school_id]);

        }

        return $dataProvider;

    }

}

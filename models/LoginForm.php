<?php

namespace app\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\Model;
use yii\db\Query;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $captcha;
    public $recaptcha;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password','captcha'], 'required'],
            ['captcha', 'captcha'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
//    public function validatePassword($attribute, $params)
//    {
//        if (!$this->hasErrors()) {
//            $user = $this->getUser();
//            if (!$user || !$user->validatePassword($this->password)) {
//                $this->addError($attribute, 'Incorrect username or password.');
//            }
//        }
//    }
    public function validatePassword($attribute, $params)
    {

        if (!$this->hasErrors()) {
            $errMsg = 'Wrong username or password. Your account will be locked after the 5th attempt';
            $user = $this->getUser();
            $stdObject= false;
           // yii::trace($user);
//            if($user->student_code){
//                $stdObject = true;
//            }
//            foreach($user as $k){
//                yii::trace($k);
//                yii::trace($k->username);
//
//                //if($)
//            }

            $errModel = (new Query())->select(['locked', 'archived'])->from('user')->where(['username' => strtolower($this->username)])->limit(1)->one();
            $errModel2 = (new Query())->select(['archived','locked'])->from('core_student')->where(['student_code' => intval($this->username)])->limit(1)->one();

            if(isset($errModel['locked']) || isset($errModel['archive'])){
                if ($errModel['archived'] == true || $errModel['locked'] == true) {
                    $errMsg = "Your account is archived or locked. Contact the administration for assistance";
                }
            }


            if(isset($errModel2['locked']) || isset($errModel2['archive'])) {
               if ($errModel2['locked'] == true || $errModel2['archived'] == true) {
                $errMsg = "Your account has been locked or archived. Please contact your school administrator for assistance";
            }
            }





            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, $errMsg);
                return;
            }
            if ($user->school_id != null) {
                $school = CoreSchool::find()->where(['id' => $user->school_id])
                    ->limit(1)->one();
                Yii::trace($school);
                if (!$school->active) {
                    $this->addError($attribute, "The associated school is not enabled. Contact the schoolsuite administrator for assistance");
                    return;
                }
            }


        }
    }
    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
//    public function login()
//    {
//        if ($this->validate()) {
//            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
//        }
//
//        return false;
//    }
    public function login()
    {
        $user = $this->getUser();
        if ($this->validate()) {
            $user->scenario = 'count';
            $user->invalid_login_count = 0;
            $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
            Yii::$app->user->login($user,$duration);

//            if($user->hasAttribute('student_code')){
//                Yii::trace('student');
//                    Yii::$app->student->login($user,$duration);
//                }else{
//                Yii::trace('other');
//
//                Yii::$app->user->login($user,$duration);
//                }
            $user->save(false);
            return $user;
        } else {
            $user = $this->getUser();
            if ($user !== null) {
                $user->scenario = 'count';
                $user->invalid_login_count += 1;
                if ($user->invalid_login_count >= 5) {
                    $user->locked = true;
                    $user->invalid_login_count = 0;
                }
                $user->save(false);
            }
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|CoreStudent
     */
//    public function getUser()
//    {
//        if ($this->_user === false) {
//            $this->_user = User::findByUsername($this->username);
//        }
//
//        return $this->_user;
//    }
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}

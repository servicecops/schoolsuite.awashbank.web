<?php

namespace app\models;

use app\components\Helpers;
use app\components\ToWords;
use app\modules\schoolcore\models\CoreBankDetails;
use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\base\NotSupportedException;
use yii\db\Query;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $lastname
 * @property string $middle_name
 * @property boolean $locked
 * @property string $user_level
 * @property integer $school_id
 * @property integer $bank_id
 * @property string $firstname
 * @property string $profile_pic
 * @property string $password_expiry_date
 * @property string $mobile_phone
 * @property integer $invalid_login_count
 * @property CoreSchool $school
 * @property CoreBankDetails $bank
 * @property CoreSchool $school_user_id
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $password2, $new_pass, $current_pass;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'firstname', 'lastname', 'mobile_phone', 'email', 'user_level', 'password', 'auth_key', 'password2'], 'required', 'on' => 'create'],
            [['username', 'firstname', 'lastname', 'mobile_phone', 'email'], 'required', 'on' => 'update'],
            [['username', 'firstname', 'mobile_phone', 'email', 'user_level', 'user_permissions', 'gender', 'lastname', 'password2', 'unarchive_reason'], 'string'],
            [['invalid_login_count', 'bank_id', 'profile_pic', 'school_user_id','branch_id','region_id'], 'integer'],
            [['password2'], 'safe', 'on' => 'create'],
            [['created_at', 'updated_at'], 'safe'],
            ['password2', 'compare', 'compareAttribute' => 'new_pass', 'on' => ['change', 'reset']],
            ['password2', 'compare', 'compareAttribute' => 'password', 'on' => ['create'], 'message' => 'The two passwords should be matching'],
            ['image', 'required', 'on' => 'picture'],
            [['username', 'email'], 'trim'],
            [['username'], 'unique', 'message' => 'This Username has already been taken'],
            [['email'], 'email'],

            ['current_pass', 'checkOldPassword', 'on' => 'change', 'message' => '', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['new_pass', 'diffPassword', 'on' => 'change', 'message' => '', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['school_id', 'selectSch', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['bank_id', 'selectBank', 'skipOnEmpty' => false, 'skipOnError' => false],
          //  ['region_id', 'selectRegion', 'skipOnEmpty' => false, 'skipOnError' => false],
            ['branch_id', 'selectBranch', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['locked'], 'boolean'],
            [['password_reset_token', 'auth_key'], 'string', 'max' => 2044],
            ['password', 'string', 'min' => 6],
            [['mobile_phone'], 'validePhone', 'skipOnEmpty' => true, 'skipOnError' => false],
            [['username'], 'valideUsername',],


        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['username', 'firstname', 'lastname','branch_id','mobile_phone', 'user_level','region_id', 'school_id', 'bank_id', 'email', 'password', 'password2'];

        $scenarios['update'] = ['username', 'firstname', 'lastname','branch_id', 'mobile_phone', 'user_level', 'region_id','school_id', 'bank_id', 'email', 'locked'];
        $scenarios['change'] = ['current_pass', 'new_pass', 'password', 'password2'];
        $scenarios['reset'] = ['new_pass', 'password', 'password2'];
        $scenarios['count'] = ['invalid_login_count', 'locked'];
        $scenarios['photo'] = ['profile_pic'];
        return $scenarios;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
                $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();


                //Correct phone numbers if user model has mobile_phone field
                if ($this->hasAttribute('mobile_phone')) {
                    $validatedPhone = ToWords::validatePhoneNumber($this->mobile_phone);
                    if ($validatedPhone) {
                        $this->mobile_phone = $validatedPhone;
                    }
                }


            }
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
//    public static function findIdentity($id)
//    {
//        return static::findOne(['id' => $id, 'locked' => false]);
//    }

    public static function findIdentity($id)
    {


        $model = User::findOne(['id' => $id, 'locked' => false, 'archived' => false]);


        if (!$model) {
            return false;
        }

        return $model;

    }

    public static function setSchoolId($id)
    {
        $user = User::find()->where(['id' => Yii::$app->user->identity->id])->limit(1)->one();
        $user->school_id = $id;
        $user->save(false);
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return User|null
     */
//    public static function findByUsername($username)
//    {
//       return static::findOne(['username' => $username, 'locked'=>false]);
//   }

    public static function findByUsername($username)
    {
        $model = User::findOne(['username' => $username, 'locked' => false, 'archived' => false]);
        Yii::trace($model);
        if (!$model && is_numeric($username)) {
            $model = User::find()->where(['schpay_student_code' => $username, 'archived' => false, 'locked' => false])->one();

        }
//        else{
//            return var_dump($model);
//        }

        return $model;

    }

    public function getFullname()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function setAuthAssignment($role, $id)
    {
        $authAssign = AuthAssignment::find()->where(['user_id' => $id])->limit(1)->one();
        if ($authAssign !== null) {
            $authAssign->item_name = $role;
            $authAssign->user_id = $id;
            return $authAssign->save(false);
        } else {
            $authAssign = new AuthAssignment();
            $authAssign->item_name = $role;
            $authAssign->user_id = $id;
            return $authAssign->save(false);
        }

    }

    public static function setUserSchools($shid, $user_id, $mult)
    {
Yii::trace($shid);

//
        if($mult){
            foreach($shid as $k=>$v){
                $model = new UserSchoolAssociation();
                $model->user_id = $user_id;
                $model->school_id = $v;
                $model->save();
            }
        }else{
            $model = new UserSchoolAssociation();
        $model->user_id = $user_id;
        $model->school_id = $shid;
        $model->save();
        }


    }

    public function assign($role, $id)
    {
        $manager = Yii::$app->authManager;
        $item = $manager->getRole($role);
//    $item = $item ? : $manager->getPermission($role);
        $manager->revokeAll($id);
        $authorRole = $manager->getRole($role);
        $manager->assign($authorRole, $id);
    }

    public static function asscSchools($id)
    {
        $query = (new Query())->select(['ass.school_id', 'si.school_name'])
            ->from('user_school_association ass')
            ->innerJoin('core_school si', 'si.id=ass.school_id')
            ->where(['ass.user_id' => $id])
            ->all();
        return $query ? $query : null;
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'locked' => false,
            'archived' => false
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'locked' => false, 'archived' => false
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public static function randomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }


    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

//    public function validatePasswords($password)
//    {
//        $user = new CoreStudent;
//        return Yii::$app->security->validatePassword($password,$user->password);
//    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function getPassword()

    {

        return '';

    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

//    get username
    public function getUsername()
    {
        return Yii::$app->user->identity->id;
    }

    public function diffPassword($attribute, $params)
    {

        if ($this->current_pass == $this->new_pass) {
            $this->addError($attribute, 'You have entered an old password. Please provide a different password');
        }

    }


    public function checkOldPassword($attribute, $params)
    {
        $id = !Yii::$app->user->isGuest ? Yii::$app->user->id : $this->id;
        $user = $this->find()->where(['id' => $id])->limit(1)->one();

        if (!$user->validatePassword($this->current_pass)) {
            $this->addError($attribute, 'Invalid or Wrong password');
        }
    }

    public function selectSch($attribute, $params)
    {
//        $selectedSch = false;
//        if(is_array($this->school_id) && !empty($this->school_id)){
//            Yii::trace("This is school array", "SCHOOL_ARRAY");
//            $selectedSch = true;
//        }
//        if($this->school_id){
//            $selectedSch = true;
//        }
        if (Helpers::IsSchoolUser($this->user_level) && !$this->school_id) {
            $this->addError($attribute, 'Please select school');
        }
    }

    public function selectBank($attribute, $params)
    {

        if ($this->user_level == 'bank_user' && !$this->bank_id) {
            $this->addError($attribute, 'Please select Bank');
        }
    }

    public function selectBranch($attribute, $params)
    {

        if (Helpers::IsBranchUser($this->user_level) && !$this->branch_id) {
            $this->addError($attribute, 'Please select branch');
        }
    }
    public function selectRegion($attribute, $params)
    {

        if (Helpers::IsRegionalUser($this->user_level) && !$this->region_id) {
            $this->addError($attribute, 'Please select region');
        }
    }


    /**
     * @return ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(CoreSchool::className(), ['id' => 'school_id']);
    }

    public function getImage()
    {
        return $this->hasOne(ImageBank::className(), ['id' => 'profile_pic']);
    }

    /**
     * Checks when a user's password was last updated and returns
     * an error code.
     */


    public function validatePasswordFreshness($id)

    {
        $model = User::find()->where(['id' => $id])->limit(1)->one();
        $model2 = CoreStudent::find()->where(['id' => $id])->limit(1)->one();
        if (Yii::$app->user->can('non_student')) {
            $lastUpdateTime = strtotime($model->password_expiry_date);

            $now = time();


            $elapsedTime = abs($now - $lastUpdateTime);


            $elapsedDays = round($elapsedTime / 86400);
        } else {
            $lastUpdateTime = strtotime($model2->password_expiry_date);

            $now = time();


            $elapsedTime = abs($now - $lastUpdateTime);


            $elapsedDays = round($elapsedTime / 86400);
        }


        return $elapsedDays;

    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsernameMob($username)
    {
        return static::findOne(['username' => strtolower($username)]);
    }


    public function validePhone($attribute, $params)
    {
        if (!preg_match('/^(0|251)(9|1)\d{8}$/i', $this->$attribute)) {
            $this->addError($attribute, 'Please Enter a valid Phone Number');
        }
    }

    public function valideUsername($attribute, $externalschId)
    {
        if (Helpers::IsSchoolUser($this->user_level) && $this->isNewRecord) {



            $schs = $this->school_id;
            $schDetails = CoreSchool::findOne(['id' => $schs[0]]);

            $externalschId = preg_quote($schDetails->external_school_code,'/');

            $re = '/^'.$externalschId.'+\.[A-Za-z0-9]{2,}$/i';
            if (!preg_match($re, $this->$attribute)) {
                $this->addError($attribute, 'Please Enter a valid username, starting with school ID eg 223.john');
            }
        }
    }


}

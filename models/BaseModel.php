<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the base model class for all entities
 *
 */
class BaseModel extends ActiveRecord
{

    /**
     * Returns the form attributes and their types
     */
    public function formAttributeConfig() {
        $config = [];
        $names = array_keys($this->attributes);
        foreach ($names as $attribute) {
            array_push($config, ['attributeName'=>$attribute, 'controlType'=>'text']);
        }

        return $config;
    }

    public function updateAttributeConfig() {
    $config = [];
    $names = array_keys($this->attributes);
    foreach ($names as $attribute) {
        array_push($config, ['attributeName'=>$attribute, 'controlType'=>'text']);
    }

    return $config;
    }
}

<?php

namespace app\models;

use app\modules\schoolcore\models\CoreStudent;
use yii\base\BaseObject;
use yii\base\InvalidCallException;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

class Identity  implements IdentityInterface
{
    private $_user;


    public static function findIdentity($id)
    {
        // $id will be something like "user-3" or "employee-2"
        list($type, $pk) = explode('-', $id);

        switch ($type) {
            case 'admin': return User:: findOne([$pk, 'locked' => '0']);
            case 'employee' : return CoreStudent::findOne([$pk, 'archived' => false, 'locked'=>false]);
        }
        if (isset($model)) {
                        return new static(['user' => $model]);
     }
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }




    public function getId()
    {
        $prefix = $this->_user instanceof User ? 'admin' : 'employee';
        return $prefix . '-' . $this->_user->getPrimaryKey();
    }
    public function setUser(\yii\db\BaseActiveRecord $user)
    {
       $this->_user = $user;
  }
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    public function getUsername()
    {
        return $this->_user->username;
   }
}

<?php
namespace app\models;

use app\modules\schoolcore\models\CoreSchool;
use app\modules\schoolcore\models\CoreStudent;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class StudentLoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $errMsg = 'Wrong username or password';
            $user = $this->getUser();
            $errModel = (new Query())->select(['archived'])->from('core_student')->where(['student_code' => $this->username])->limit(1)->one();
            $errModel2 = (new Query())->select(['locked'])->from('core_student')->where(['student_code' => strtolower($this->username)])->limit(1)->one();
            if ($errModel['archived'] == true) {
                $errMsg = "Your account is locked for some reasons. Please contact the school administration for assistance";
            }
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, $errMsg);
                return;
            }

        }
    }
//    public function validatePassword($attribute, $params)
//    {
//        if (!$this->hasErrors()) {
//            $user = $this->getUser();
//
//            if (!$user || !$user->validatePassword($this->password)) {
//                $this->addError($attribute, 'Incorrect username or password.');
//            }
//        }
//    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
//    public function login()
//    {
//        if ($this->validate()) {
//            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
//        }
//        return false;
//    }
    public function login()
    {
        $user = $this->getUser();
        if ($this->validate()) {
            $user->scenario = 'count';
            $user->invalid_login_count = 0;
            $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
            Yii::$app->user->login($user,$duration);
            $user->save(false);
            return $user;
        } else {
            $user = $this->getUser();
            if ($user !== null) {
                $user->scenario = 'count';
                $user->invalid_login_count += 1;
                if ($user->invalid_login_count >= 3) {
                    $user->locked = true;
                    $user->invalid_login_count = 0;
                }
                $user->save(false);
            }
            return false;
        }
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }


}

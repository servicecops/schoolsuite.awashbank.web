<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 */
class AuthItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * Returns user levels that this user can administer,
     * if super_admin, then return all
     * if sys_admin then return all except super_admin and sys_admin
     */
    public static function getUserLevelsICanAdminister()
    {
        if(!Yii::$app->user->can('schoolsuite_admin') && !Yii::$app->user->can('sch_admin')) {
            //Return nothing
            return [];
        }
        else if (Yii::$app->user->can('super_admin')) {
            return AuthItem::find()->where(['type' => '1'])->andWhere(['<>', 'name', 'sch_teacher'])->all();
        } else if (Yii::$app->user->can('branch_makers' )||Yii::$app->user->can('branch_chekers' )) {
            return AuthItem::find()->where(['type' => '1'])->andWhere(['<>', 'name', 'sys_admin'])->andWhere(['<>', 'name', 'sch_teacher'])
                ->andWhere(['<>', 'name', 'super_admin'])
                ->andWhere(['<>', 'name', 'branch_chekers'])
                ->andWhere(['<>', 'name', 'DCO_Branch'])
                ->andWhere(['<>', 'name', 'DCO_Region'])
                ->andWhere(['<>', 'name', 'Management_ViewAccess'])
                ->andWhere(['<>', 'name', 'bank_staff'])
                ->andWhere(['<>', 'name', 'branch_makers'])->andWhere(['<>', 'name', 'bank_user'])->all();
        }
        else {
            //return all except super_admin and sys_admin
            return AuthItem::find()->where(['type' => '1'])->andWhere(['<>', 'name', 'sys_admin'])->andWhere(['<>', 'name', 'sch_teacher'])->andWhere(['<>', 'name', 'super_admin'])->andWhere(['<>', 'name', 'bank_user'])->all();
        }
    }
}

<?php
namespace app\models;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $username;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            ['email', 'trim'],
            ['username', 'required'],
            ['username', 'string'],
            ['username', 'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['locked' => false],
                'message' => 'There is no user with this username.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            //'locked' => false,
            'username' => $this->username,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
        Yii::trace('user b4 email');

        $email= Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'Schoolsuite'])
            ->setTo($user->email)
            ->setSubject('User Password reset for Schoolsuite ')
            ->send();
        Yii::trace('kikyo');
        if($email){
            return true;
        }else{
            return false;
        }
    }

    public function getEmail(){
        /* @var $user User */
        $user = User::findOne([
            //'locked' => false,
            'username' => $this->username,
        ]);
        return $user->email;
    }
}
